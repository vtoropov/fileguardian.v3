(*
;-----------------------------------------------------------------------------
;   Created by Tech_dog (VToropov) on 1-Feb-2016 at 5:01:47pm, GMT+7, Phuket, Rawai, Monday;
;   This is Common Code of the File Watcher Applications Installation Program.
;-----------------------------------------------------------------------------
*)

[Code]
/////////////////////////////////////////////////////////////////////////////
//  Global variables and constants                                         //
/////////////////////////////////////////////////////////////////////////////

const
  _FW_CONSOLE_CODE_SUCCESS        = $0;
  _FW_CONSOLE_CODE_PASSED         = $1;
  _FW_CONSOLE_CODE_FAILURE        = $2;

/////////////////////////////////////////////////////////////////////////////
//  Custom functions and procedures                                        //
/////////////////////////////////////////////////////////////////////////////

// fg agent utility writes last error in order to show a message to a user via installer interface

procedure Custom_LastErrorDelete();
begin
  RegDeleteValue(
	HKEY_USERS,
	'S-1-5-18\Software\FG_Service\LastError',
	'details'
  );
end;

procedure Custom_LastErrorShow(sDefault: String);
var
  details :string;
begin
  if RegQueryStringValue(
	HKEY_USERS,
	'S-1-5-18\Software\FG_Service\LastError',
	'details',
	details
    ) then
  begin
    SuppressibleMsgBox(
	details ,
	mbError ,
	MB_OK   ,
	MB_OK
    );
  end
  else begin
    SuppressibleMsgBox(
	sDefault,
	mbError ,
	MB_OK   ,
	MB_OK
    );
  end;
end;


/////////////////////////////////////////////////////////////////////////////

function Custom_ArcExist(bThoroughly: Boolean): Boolean;
var
  FindRec: TFindRec;
begin
  Result := False;
  if FindFirst(ExpandConstant('{app}\Archives\*.db'), FindRec) then begin
    Log('Custom Code: Archive(s) exist(s)');
    Result := True;
    FindClose(FindRec);
  end;
    
  if not Result then begin
    if bThoroughly then begin
      if DirExists(ExpandConstant('{app}\Archives')) then  Result := True;
    end;
  end;
end;

procedure Custom_RemoveArc();
begin
  DelTree(ExpandConstant('{app}\Archives\*.*'), False, True, False);
  Log('Custom Code: Archives folder content is deleted');
  RemoveDir(ExpandConstant('{app}\Archives\'));
end;

function Custom_IsUpgrade(): Boolean;
var
  sPrevPath: String;
begin
  sPrevPath := WizardForm.PrevAppDir;
  Result := (sPrevPath <> '');
end;

function Custom_DbExist(): Boolean;
var
  FindRec: TFindRec;
begin
  Result := False;
  if FindFirst(ExpandConstant('{app}\fw_watch_log.db'), FindRec) then begin
    Log('Custom Code: Storage exists');
    Result := True;
    FindClose(FindRec);
  end;
end;

procedure Custom_RemoveDb();
begin
  DelTree(ExpandConstant('{app}\fw_watch_log.db'), False, True, False);
  Log('Custom Code: event database is deleted');
end;

function Custom_ServiceCreate(): Boolean;
var
  exe_path :String;
  cmd_line :String;
  ret_code :Integer;
begin
  exe_path:= ExpandConstant('{app}\FG_Service.exe'); 
  Log('Custom Code: exe_path=' + exe_path);
  cmd_line:= '/service install /exe_path "' + exe_path + '"';

  Result := False;

  if ShellExec(
	'runas',
	ExpandConstant('{app}\FG_Agent.exe'),
	cmd_line,
	'',
	SW_HIDE,
	ewWaitUntilTerminated,
	ret_code
    ) then
  begin
    if (_FW_CONSOLE_CODE_SUCCESS = ret_code) then Log('Custom Code: File Watcher service is created successfully');
    if (_FW_CONSOLE_CODE_FAILURE = ret_code) then Log('Custom Code: File Watcher service creation has failed');

    Result := (_FW_CONSOLE_CODE_SUCCESS = ret_code);
  end;
end;

function Custom_ServiceStart(): Boolean;
var
  ret_code :Integer;
begin
  Result := False;

  if ShellExec(
	'runas',
	ExpandConstant('{app}\FG_Agent.exe'),
	'/service start',
	'',
	SW_HIDE,
	ewWaitUntilTerminated,
	ret_code
    ) then
  begin
    if (_FW_CONSOLE_CODE_SUCCESS = ret_code) then Log('Custom Code: File Watcher service is started successfully');
    if (_FW_CONSOLE_CODE_FAILURE = ret_code) then Log('Custom Code: File Watcher service start has failed');

    Result := (_FW_CONSOLE_CODE_SUCCESS = ret_code);
  end;
end;

function Custom_ServiceStop(): Boolean;
var
  ret_code :Integer;
begin
  Result := False;

  if ShellExec(
	'runas',
	ExpandConstant('{app}\FG_Agent.exe'),
	'/service stop',
	'',
	SW_HIDE,
	ewWaitUntilTerminated,
	ret_code
    ) then
  begin
    if (_FW_CONSOLE_CODE_SUCCESS = ret_code) then Log('Custom Code: File Watcher service is stopped successfully');
    if (_FW_CONSOLE_CODE_FAILURE = ret_code) then Log('Custom Code: File Watcher service stop has failed');

    Result := (_FW_CONSOLE_CODE_SUCCESS = ret_code);
  end;
end;

function Custom_ServiceRemove(): Boolean;
var
  ret_code :Integer;
begin
  Result := False;

  if ShellExec(
	'runas',
	ExpandConstant('{app}\FG_Agent.exe'),
	'/service remove',
	'',
	SW_HIDE,
	ewWaitUntilTerminated,
	ret_code
    ) then
  begin
    if (_FW_CONSOLE_CODE_SUCCESS = ret_code) then Log('Custom Code: File Watcher service is removed successfully');
    if (_FW_CONSOLE_CODE_FAILURE = ret_code) then Log('Custom Code: File Watcher service removal has failed');

    Result := (_FW_CONSOLE_CODE_SUCCESS = ret_code);
  end;
end;

function Custom_StartupCheck(): Boolean;
var
  ret_code :Integer;
begin
  Result := False;

  if ShellExec(
	'runas',
	ExpandConstant('{app}\FG_Agent.exe'),
	'/startup_task check',
	'',
	SW_HIDE,
	ewWaitUntilTerminated,
	ret_code
    ) then
  begin
    Result := (_FW_CONSOLE_CODE_SUCCESS = ret_code);
  end;
end;

function Custom_StartupCreate(): Boolean;
var
  exe_path :String;
  cmd_line :String;
  ret_code :Integer;
begin
  exe_path:= ExpandConstant('{app}\FG_Agent.exe'); 
  Log('Custom Code: exe_path=' + exe_path);
  cmd_line:= '/startup_task create /exe_path "' + exe_path + '"';
  Result  := False;

  if ShellExec(
	'runas',
	ExpandConstant('{app}\FG_Agent.exe'),
	cmd_line,
	'',
	SW_HIDE,
	ewWaitUntilTerminated,
	ret_code
    ) then
  begin
    if (_FW_CONSOLE_CODE_PASSED  = ret_code) then Log('Custom Code: File Watcher startup task is already created');
    if (_FW_CONSOLE_CODE_SUCCESS = ret_code) then Log('Custom Code: File Watcher startup task has been successfully created');
    if (_FW_CONSOLE_CODE_FAILURE = ret_code) then Log('Custom Code: File Watcher startup task creation failed');
    Result := (_FW_CONSOLE_CODE_FAILURE <> ret_code);
  end
  else
  begin
    Log('Custom Code: Creating startup scheduler task is failed');
  end;
end;

function Custom_StartupRemove(): Boolean;
var
  ret_code :Integer;
begin
  Result := False;
  if ShellExec(
	'runas',
	ExpandConstant('{app}\FG_Agent.exe'),
	'/startup_task remove',
	'',
	SW_HIDE,
	ewWaitUntilTerminated,
	ret_code
    ) then
  begin
    if (_FW_CONSOLE_CODE_PASSED  = ret_code) then Log('Custom Code: File Watcher startup task is already removed');
    if (_FW_CONSOLE_CODE_SUCCESS = ret_code) then Log('Custom Code: File Watcher startup task has been successfully removed');
    if (_FW_CONSOLE_CODE_FAILURE = ret_code) then Log('Custom Code: File Watcher startup task removal failed');
    Result := True;
  end
  else
  begin
    Log('Custom Code: Removing startup scheduler task is failed');
  end;
end;

function Custom_CloseAgent(): Boolean;
var
  ret_code :Integer;
begin
  Result := True;
    Exec(
	'cmd.exe',
	'/C "taskkill /F /IM FG_Agent.exe"',
	'',
	SW_HIDE,
	ewWaitUntilTerminated,
	ret_code
      );
end;

function  Custom_StartAgent(): Boolean;
var
  ret_code :Integer;
begin
  ShellExec('open', ExpandConstant('{app}\FG_Agent.exe'), '', '', SW_HIDE, ewNoWait, ret_code);
end;

function Custom_SetDefaultWatchFolder(): Boolean;
var
  ret_code :Integer;
begin
  Result := False;

  if ShellExec(
	'runas',
	ExpandConstant('{app}\FG_Agent.exe'),
	'/def_folder',
	'',
	SW_HIDE,
	ewWaitUntilTerminated,
	ret_code
    ) then
  begin
    Result := (0 <> ret_code);
  end;
end;
