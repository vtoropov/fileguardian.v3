::
:: Created by Tech_dog (ebontrop@gmail.com) on 25-Apr-2018 at 0:22:28a, UTC+7, Novosibirsk, Rodniki, Wednesday;
:: This is batch file for creating and signing File Guardian minifilter driver for both architectures: x86 and x64.
::

set v_source_base=%1
set v_settings=%2\VC\Auxiliary\Build

set v_bin_x86=%v_source_base%\!bin__\x86\Release
set v_bin_x64=%v_source_base%\!bin__\x64\Release

::
:: Step#1: building File Guardian driver for x86 platform;
::

@echo.
@echo [INFO] Step (#1): Building FG driver for x86 platform...
@echo.

set v_current=%~dp0

call %v_settings%\vcvarsall.bat x86

cd /d %v_current%

@echo [INFO] Current folder is %v_current%
@echo [INFO] Target folder is %v_driver_base%\fg_filter

cd %v_source_base%

msbuild /t:clean /t:build FG_FsUI_v15.sln /p:Configuration=Release /p:Platform=x86

cd /d %v_current%
::
:: Step#2: building File Guardian driver for x64 platform
::

@echo.
@echo [INFO] Step (#2): Building FG driver for x64 platform...
@echo.

call %v_settings%\vcvarsall.bat x64

cd /d %v_current%

cd %v_source_base%

msbuild /t:clean /t:build FG_FsUI_v15.sln /p:Configuration=Release /p:Platform=x64

cd /d %v_current%


set v_project=%v_source_base%
set v_agent_exe=FG_Agent.exe
set v_desktop_exe=FG_Desktop.exe
set v_service_exe=FG_Service.exe
set v_filter_sys=FG_FsFilter.sys

set v_ver_path=%v_source_base%\fg.um.shared\fg.generic.shared\interface
set v_ver_file=FG_Generic_Version.h

set v_hlp_path=%v_source_base%\fg.um.help
set v_hlp_file=Fg_HelpSystem.chm
set v_hlp_wind=Fg_HelpSystem.chw

set v_cert_root=%v_project%\fg.km\fg.cert\Go Daddy Root Certificate Authority - G2.crt
set v_cert_owner=Bit Sphere Incorporated

@echo.
@echo [INFO] Step (#3): Signing executable files for x64 platform...
@echo.

.\cert\signtool.exe sign /v /ac "%v_cert_root%" /s my /n "%v_cert_owner%" /t http://timestamp.verisign.com/scripts/timstamp.dll %v_bin_x64%\%v_filter_sys%
.\cert\signtool.exe sign /v /ac "%v_cert_root%" /s my /n "%v_cert_owner%" /t http://timestamp.verisign.com/scripts/timstamp.dll %v_bin_x64%\%v_agent_exe%
.\cert\signtool.exe sign /v /ac "%v_cert_root%" /s my /n "%v_cert_owner%" /t http://timestamp.verisign.com/scripts/timstamp.dll %v_bin_x64%\%v_desktop_exe%
.\cert\signtool.exe sign /v /ac "%v_cert_root%" /s my /n "%v_cert_owner%" /t http://timestamp.verisign.com/scripts/timstamp.dll %v_bin_x64%\%v_service_exe%

@echo.
@echo [INFO] Step (#4): Signing executable files for x86 platform...
@echo.

.\cert\signtool.exe sign /v /ac "%v_cert_root%" /s my /n "%v_cert_owner%" /t http://timestamp.verisign.com/scripts/timstamp.dll %v_bin_x86%\%v_filter_sys%
.\cert\signtool.exe sign /v /ac "%v_cert_root%" /s my /n "%v_cert_owner%" /t http://timestamp.verisign.com/scripts/timstamp.dll %v_bin_x86%\%v_agent_exe%
.\cert\signtool.exe sign /v /ac "%v_cert_root%" /s my /n "%v_cert_owner%" /t http://timestamp.verisign.com/scripts/timstamp.dll %v_bin_x86%\%v_desktop_exe%
.\cert\signtool.exe sign /v /ac "%v_cert_root%" /s my /n "%v_cert_owner%" /t http://timestamp.verisign.com/scripts/timstamp.dll %v_bin_x86%\%v_service_exe%

pause
::
:: Step#3: copying executable files for building installer;
::     File Guardian version file is also copied for synchronizing the software version with installer one;
::
set v_output=.\output
set v_out_x86=%v_output%\x86
set v_out_x64=%v_output%\x64

set v_out_ver=%v_output%\version
set v_out_hlp=%v_output%\help

@echo.
@echo [INFO] Step (#5): Copying executable files to setup binary output folder...
@echo.

if not exist %v_output%  mkdir %v_output%
if not exist %v_out_x86% mkdir %v_out_x86% 
if not exist %v_out_x64% mkdir %v_out_x64%
if not exist %v_out_ver% mkdir %v_out_ver%
if not exist %v_out_hlp% mkdir %v_out_hlp%

::pause

if exist %v_out_x86%\%v_agent_exe% del %v_out_x86%\%v_agent_exe%
if exist %v_out_x64%\%v_agent_exe% del %v_out_x64%\%v_agent_exe%

if exist %v_out_x86%\%v_desktop_exe% del %v_out_x86%\%v_desktop_exe%
if exist %v_out_x64%\%v_desktop_exe% del %v_out_x64%\%v_desktop_exe%

if exist %v_out_x86%\%v_service_exe% del %v_out_x86%\%v_service_exe%
if exist %v_out_x64%\%v_service_exe% del %v_out_x64%\%v_service_exe%

if exist %v_out_x86%\%v_filter_sys% del %v_out_x86%\%v_filter_sys%
if exist %v_out_x64%\%v_filter_sys% del %v_out_x64%\%v_filter_sys%

::pause

::echo %v_drv_x86%\%v_filter_sys%
::echo %v_out_x86%\%v_filter_sys%

copy %v_bin_x86%\%v_filter_sys% %v_out_x86%\%v_filter_sys% /y
copy %v_bin_x64%\%v_filter_sys% %v_out_x64%\%v_filter_sys% /y

copy %v_bin_x86%\%v_agent_exe% %v_out_x86%\%v_agent_exe% /y
copy %v_bin_x64%\%v_agent_exe% %v_out_x64%\%v_agent_exe% /y

copy %v_bin_x86%\%v_desktop_exe% %v_out_x86%\%v_desktop_exe% /y
copy %v_bin_x64%\%v_desktop_exe% %v_out_x64%\%v_desktop_exe% /y

copy %v_bin_x86%\%v_service_exe% %v_out_x86%\%v_service_exe% /y
copy %v_bin_x64%\%v_service_exe% %v_out_x64%\%v_service_exe% /y

@echo.
@echo [INFO] Step (#6): Copying software version and help files to output folder...
@echo.

if exist %v_out_ver%\%v_ver_file% del %v_out_ver%\%v_ver_file%
if exist %v_out_hlp%\%v_hlp_file% del %v_out_hlp%\%v_hlp_file%
if exist %v_out_hlp%\%v_hlp_wind% del %v_out_hlp%\%v_hlp_wind%

copy %v_ver_path%\%v_ver_file% %v_out_ver%\%v_ver_file%  /y
copy %v_hlp_path%\%v_hlp_file% %v_out_hlp%\%v_hlp_file%  /y
copy %v_hlp_path%\%v_hlp_wind% %v_out_hlp%\%v_hlp_wind%  /y

@echo.
@echo [WAIT] Press any key to continue or Ctrl+C to interrupt the execution...
@echo.

pause

::
:: (k) sets inno setup compiler full path;
::     it depends on installation folder of Inno Setup;
::
set v_inno_comp="C:\Program Files (x86)\Inno Setup 5\Compil32.exe"
set v_inno_proj=.\fw_project_main.iss

@echo.
@echo [INFO] Step (#7): Compiling setup...
@echo.

call %v_inno_comp% /CC %v_inno_proj%
::
:: (l) signing output setup file
::
set v_output=.\output\fg-setup_v1.5.0.1_release.exe

@echo.
@echo [INFO] Step (#8): Signing installer...
@echo.

set v_cert_exe=.\cert\signtool.exe
set v_cert_obj=.\cert\fg_sign.pfx
set v_cert_pwd=%3
set v_cert_tim=http://timestamp.verisign.com/scripts/timstamp.dll

%v_cert_exe% sign /v /a /f %v_cert_obj% /p %v_cert_pwd%  /t %v_cert_tim%  %v_output%
%v_cert_exe% verify /v /kp %v_output%

@echo.
@echo Build precess has been completed.


