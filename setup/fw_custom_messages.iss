;
; Created by Tech_dog (VToropov) on 1-Feb-2016 at 7:28:05pm, GMT+7, Phuket, Rawai, Monday;
; This is custom messages for File Watcher software product installation program;
;

[CustomMessages]
; msg=Message, tsk=Task
; English
en.msg_client_setup_is_running    =File Guardian application setup is already running.
en.msg_client_app_is_running      =Setup has detected File Guardian application is running. Please close the application in order to proceed or press [Cancel] to exit.
en.msg_service_not_available      =The service manager is not available.
en.msg_service_not_supported      =Only NT based systems support services.
en.msg_client_delete_data         =Do you want to delete File Guardian database and configuration file?
en.run_visit_website              =Visit File Guardian software vendor website
en.run_desktop_app                =Run File Guardian
en.tsk_other                      =Other tasks:
en.tsk_client_reset               =Delete File Guardian application files:
en.tsk_delete_arc                 =Archives
en.tsk_delete_db                  =File System Event Database
en.tsk_delete_cfg                 =Service configuration file
en.tsk_startup                    =Startup option:
