(*
;-----------------------------------------------------------------------------
;   Created by Tech_dog (VToropov) on 28-Apr-2016 at 3:54:24pm, GMT+7, Phuket, Rawai, Thursday;
;   This is Custom Code of the File Guardian Installation Program for serial number manipulation tasks.
;-----------------------------------------------------------------------------
*)

[Code]
/////////////////////////////////////////////////////////////////////////////
//  Global variables and constants                                         //
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//  Custom functions and procedures                                        //
/////////////////////////////////////////////////////////////////////////////

procedure Custom_SN_ShowLastError();
var
  details :string;
begin
  if RegQueryStringValue(
	HKEY_USERS,
	'S-1-5-18\Software\FG_Service\LastError',
	'details',
	details
    ) then
  begin
    SuppressibleMsgBox(
	details,
	mbError,
	MB_OK  ,
	MB_OK
    );
  end
  else begin
    SuppressibleMsgBox(
	'The error occurred while registering serial number',
	mbError,
	MB_OK  ,
	MB_OK
    );
  end;
end;

function Custom_SN_Register(): Boolean;
var
  ret_code :Integer;
  cmd_line :String;
  app_path :String;
begin
  cmd_line := '/license register /serial "' + SN_GetNumber() + '" /desc "' + SN_GetDescription() + '"';
  Result   := False;
  app_path := ExpandConstant('{tmp}\FG_Agent.exe');
  Log('Custom Code: app path is ' + app_path);
  Log('Custom Code: cmd line is ' + cmd_line);

  ExtractTemporaryFile('FG_Agent.exe');

  RegDeleteValue(
    HKEY_USERS,
	'S-1-5-18\Software\FG_Service\LastError',
	'details'
  );

  if ShellExec(
	'runas' ,
	app_path,
	cmd_line,
	'',
	SW_HIDE ,
	ewWaitUntilTerminated,
	ret_code
    ) then
  begin
    if (0 = ret_code) then Log('Custom Code: Serial number is valid; license is registered');
    if (2 = ret_code) then begin
      Custom_SN_ShowLastError();
      Log('Custom Code: License registration fails');
    end;
    Result := (0 = ret_code);
  end
  else
  begin
    Log('Custom Code: ShellExec has returned the error; cannot run the agent');
    SuppressibleMsgBox(
	'The error occurred while starting the agent to register serial number',
	mbError,
	MB_OK  ,
	MB_OK
    );
  end;
end;

function Custom_SN_Remove(): Boolean;
var
  ret_code :Integer;
  cmd_line :String;
  app_path :String;
begin
  cmd_line := '/license remove';
  Result   := False;
  app_path := ExpandConstant('{app}\FG_Agent.exe');
  Log('Custom Code: app folder is ' + app_path);

  if ShellExec(
	'runas' ,
	app_path,
	cmd_line,
	'',
	SW_HIDE ,
	ewWaitUntilTerminated,
	ret_code
    ) then
  begin
    if (0 = ret_code) then Log('Custom Code: Serial number is removed');
    if (2 = ret_code) then Log('Custom Code: Serial number removal fails');
    Result := (0 = ret_code);
  end
  else
  begin
    Log('Custom Code: ShellExec has returned the error; cannot run the agent');
  end;
end;
