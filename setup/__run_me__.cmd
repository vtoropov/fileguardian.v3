::
:: This is the entry point of File Guardian installer build process:
:: the main batch file is called in order to initiate a process.
::
:: Created by Tech_dog (ebontrop@gmail.com) on 24-Feb-2018 at 11:10:57p, UTC+7, Phuket, Rawai, Saturday;
:: -----------------------------------------------------------------------------
:: Adopted to v15 on 13-Jun-2018 at 0:07:38a, UTC+7, Phuket, Rawai, Wednesday;
::
@echo off

set /p v_password=Enter the certificate password: 

if "%v_password%" == "" (

  @echo.
  @echo You need to enter the password in order to proceed.
  @echo.
  exit

)

@echo.
@echo   Enter the File Guardian solution root directory path;
@echo   by default, it is .\..\_src
@echo   Please note:
@echo    a) the path must be specified as a full one or relative to this command file;
@echo    b) no back slash at the end;
@echo.

set /p v_project=Please enter File Guardian solution folder: 

if "%v_project%" == "" set v_project=.\..\_src

@echo.
@echo   Enter VS2017 installation directory;
@echo   by default, it is C:\VS15 
@echo   Please note:
@echo    a) the path must be specified as absolute path to VS;
@echo    b) no back slash at the end;
@echo.

set /p v_vstudio=Please enter VS 2017 installtion folder path:

if "%v_vstudio%" == "" set v_vstudio=C:\VS15

call _build_fg_all.v3.bat %v_project% %v_vstudio% %v_password%