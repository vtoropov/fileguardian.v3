;-----------------------------------------------------------------------------
; Created by Tech_dog (ebontrop@gmail.com) on 1-Feb-2016 at 7:59:54pm, GMT+7, Phuket, Rawai, Monday;
; This is File Watcher Application Project Setup (x86 & x64).
;-----------------------------------------------------------------------------

#if VER < 0x05040200
  #error Update your Inno Setup version
#endif

#define base_path  GetEnv('Path')

#include ".\output\version\FG_Generic_Version.h"

#define app_version            str(FG_VER_MAJOR) + "." + str(FG_VER_MINOR) + "." + str(FG_VER_SUBMINOR) + "." + str(FG_VER_BUGFIX)
#define simple_app_version     str(FG_VER_MAJOR) + "." + str(FG_VER_MINOR) + "." + str(FG_VER_SUBMINOR)
#define copyright              "Copyright � 2016, Bit Sphere Inc."
#define installer_build_date   GetDateTimeString('mmm, d yyyy', '', '')
#define quick_launch           "{userappdata}\Microsoft\Internet Explorer\Quick Launch\FileWatcher"
#define bindir_x86             ".\output\x86"
#define bindir_x64             ".\output\x64"
#define setdir                 "."
#define helpdir                ".\output\help"

[Setup]
AppId                          ={{89DF3392-FCBE-4fc7-BA6F-EB2DDE06D0A3}
AppName                        =File Guardian
AppVersion                     ={#app_version}
AppVerName                     =File Guardian {#app_version}
AppPublisher                   =Bit Sphere Inc.
AppCopyright                   ={#copyright}
AppPublisherURL                =http://www.TheFileGuardian.com
AppSupportURL                  =http://www.TheFileGuardian.com
AppUpdatesURL                  =http://www.TheFileGuardian.com
AppContact                     =http://www.TheFileGuardian.com
VersionInfoCompany             =Bit Sphere Inc.
VersionInfoCopyright           ={#copyright}
VersionInfoDescription         =File Guardian Installer
VersionInfoProductName         =File Guardian
VersionInfoProductVersion      ={#app_version}
VersionInfoTextVersion         ={#app_version}
VersionInfoVersion             ={#app_version}
DefaultDirName                 ={pf}\FileGuardian
;DefaultGroupName               =File Watcher
LicenseFile                    =fw_project_license.txt
InfoBeforeFile                 =fw_project_readme_before.rtf
OutputDir                      =.\output
OutputBaseFilename             =fg-setup_v{#app_version}_release
; "ArchitecturesInstallIn64BitMode=x64" requests that the install be
; done in "64-bit mode" on x64, meaning it should use the native
; 64-bit Program Files directory and the 64-bit view of the registry.
; On all other architectures it will install in "32-bit mode".
ArchitecturesInstallIn64BitMode=x64
; Note: We don't set ProcessorsAllowed because we want this
; installation to run on all architectures (including Itanium,
; since it's capable of running 32-bit code too).
Compression                    =lzma2/max
SolidCompression               =yes
MinVersion                     =0,5.1
UninstallDisplayName           =File Guardian {#app_version}
UninstallDisplayIcon           ={app}\fg_service.exe
AppReadmeFile                  ={app}\fw_project_readme_after.rtf
WizardImageFile                =.\fg_img\FG_Installer_LeftSide_Banner.bmp
WizardSmallImageFile           =.\fg_img\FG_Installer_Top_Side_Banner.bmp
SetupIconFile                  =.\fg_img\FG_installer_ui2.ico
AllowNoIcons                   =yes
ShowTasksTreeLines             =yes
AlwaysShowDirOnReadyPage       =yes
AlwaysShowGroupOnReadyPage     =no
PrivilegesRequired             =admin
DisableDirPage                 =no
DisableProgramGroupPage        =yes
AppMutex                       =Global\file_watcher_sft
WizardImageAlphaFormat         =premultiplied

[Languages]
Name: en; MessagesFile: compiler:Default.isl
#include "fw_custom_messages.iss"

[Messages]
BeveledLabel                   =File Guardian {#app_version} built on {#installer_build_date}
SetupAppTitle                  =Setup - File Guardian
SetupWindowTitle               =Setup - File Guardian

[Tasks]
;Name: desktopicon;             Description: {cm:CreateDesktopIcon};       GroupDescription: {cm:AdditionalIcons}
;Name: quicklaunchicon;         Description: {cm:CreateQuickLaunchIcon};   GroupDescription: {cm:AdditionalIcons};                              Flags: unchecked; OnlyBelowVersion: 0,6.01

Name: delete_arc;               Description: {cm:tsk_delete_arc};          GroupDescription: {cm:tsk_client_reset};  Check: Custom_ArcExist(True); Flags: checkedonce unchecked
Name: delete_db;                Description: {cm:tsk_delete_db};           GroupDescription: {cm:tsk_client_reset};  Check: Custom_DbExist();      Flags: checkedonce unchecked

[Files]
Source: .\fg_img\FG_Security_048px.bmp;               Flags: dontcopy
Source: .\common\common_update_page.rtf;              Flags: dontcopy
;-----------------------------------------------------------------------------
; installer helper executable;
;-----------------------------------------------------------------------------
Source: {#bindir_x86}\FG_Agent.exe;                   DestDir: {app};            DestName:    "FG_Agent.exe";      Check: IsWin32();
Source: {#bindir_x64}\FG_Agent.exe;                   DestDir: {app};            DestName:    "FG_Agent.exe";      Check: IsWin64();

;-----------------------------------------------------------------------------
; File Watcher application files;
;-----------------------------------------------------------------------------
Source: {#bindir_x86}\FG_Service.exe;                 DestDir: {app};            DestName:    "FG_Service.exe";    Check: IsWin32(); Flags: ignoreversion
Source: {#bindir_x86}\FG_Desktop.exe;                 DestDir: {app};            DestName:    "FG_Desktop.exe";    Check: IsWin32(); Flags: ignoreversion
Source: {#bindir_x86}\FG_FsFilter.sys;                DestDir: {app};            DestName:    "FG_FsFilter.sys";   Check: IsWin32(); Flags: ignoreversion
Source: {#bindir_x64}\FG_Service.exe;                 DestDir: {app};            DestName:    "FG_Service.exe";    Check: IsWin64(); Flags: ignoreversion
Source: {#bindir_x64}\FG_Desktop.exe;                 DestDir: {app};            DestName:    "FG_Desktop.exe";    Check: IsWin64(); Flags: ignoreversion
Source: {#bindir_x64}\FG_FsFilter.sys;                DestDir: {app};            DestName:    "FG_FsFilter.sys";   Check: IsWin64(); Flags: ignoreversion
Source: fw_project_license.txt;                       DestDir: {app};            DestName:    "license.txt";       Flags: ignoreversion
Source: fw_project_readme_after.rtf;                  DestDir: {app};            DestName:    "readme.rtf";        Flags: ignoreversion
Source: fg_readme_arc.txt;                            DestDir: {app}\Archives;   DestName:    "readme.txt";        Flags: ignoreversion
Source: {#helpdir}\Fg_HelpSystem.chm;                 DestDir: {app};                                              Flags: ignoreversion
SOurce: {#helpdir}\Fg_HelpSystem.chw;                 DestDir: {app};                                              Flags: ignoreversion

[Icons]
;Name: {group}\Uninstall File Watcher;                 Filename: {uninstallexe};       WorkingDir: {app};   IconFilename: {app}\fw_desktop.exe; IconIndex: 1; Comment: {cm:UninstallProgram,File Watcher}
;Name: {group}\Help and Support\ReadMe;                Filename: {app}\readme.rtf;     WorkingDir: {app};                                                     Comment: File Watcher ReadMe

[Run]
Filename: http://www.TheFileGuardian.com;            Description: {cm:run_visit_website};                          Flags: nowait postinstall shellexec skipifsilent unchecked
Filename: {app}\fg_desktop.exe;                      Description: {cm:run_desktop_app};                            Flags: nowait postinstall shellexec

[InstallDelete]
; During installation, delete old files in install folder
Name: {app}\license.txt;                             Type: files

[Code]
#include ".\common\common_generic_win32.iss"
#include ".\common\common_services.iss"
#include ".\common\common_serial_number.iss"
#include ".\common\common_update_page.iss"
#include "fw_custom_code.iss"
#include "fw_custom_code_sn.iss"
#include ".\common\common_wmi_service.iss"

var 
  is_silent: Boolean;

/////////////////////////////////////////////////////////////////////////////
//  Inno Setup functions and procedures                                    //
/////////////////////////////////////////////////////////////////////////////
const
  installer_mutex_name = 'fw_setup_mutex';

function IsWin32(): Boolean; begin Result := not IsWin64(); end;
function IsWinVista(): Boolean; begin Result := (6 = (GetWindowsVersion shr 24)); end;
function IsWin7(): Boolean; begin Result := (7 = (GetWindowsVersion shr 24 )); end;
function IsWin8(): Boolean; begin Result := (8 = (GetWindowsVersion shr 24 )); end;
function IsWin10(): Boolean; begin Result := (10 = (GetWindowsVersion shr 24 )); end;

procedure InitializeWizard();
var
    nPageId : cardinal;
begin
    nPageId := wpWelcome;
    
    if not Is64BitInstallMode() then
        begin WizardForm.Caption := 'File Guardian {#app_version} (32-bit)'; end else
        begin WizardForm.Caption := 'File Guardian {#app_version} (64-bit)';
    end;
//  if IsWin10() then begin
//      if wmi_IsUpdateInstalled('KB4074595') then
    if IsWin7() then begin
        if wmi_IsUpdateInstalled('KB3033929') then
        begin
            Update_CreatePage('FG_Security_048px.bmp');
            nPageId := g_UpdatePage.ID;
        end;
    end;
    SN_CreatePage(nPageId);
end;

function InitializeSetup(): Boolean;
begin
  is_silent := WizardSilent();

  // Creates a mutex for the installer.
  // If it's already running display a message and stop the installation
  if CheckForMutexes(installer_mutex_name) then begin
      Log('Custom Code: Installer is already running');
      SuppressibleMsgBox(CustomMessage('msg_client_setup_is_running'), mbError, MB_OK, MB_OK);
      Result := False;
  end
  else begin
    Result := True;
    
    Log('Custom Code: Creating installer`s mutex');
    CreateMutex(installer_mutex_name);

  end;
end;

function InitializeUninstall(): Boolean;
begin
  if CheckForMutexes(installer_mutex_name) then begin
    Log('Custom Code: Installer mutex is detected');
    SuppressibleMsgBox(CustomMessage('msg_client_setup_is_running'), mbError, MB_OK, MB_OK);
    Result := False;
  end
  else begin
    CreateMutex(installer_mutex_name);
    
    Log('Custom Code: Trying to close fw_agent app');
    Custom_CloseAgent();
    
    Result := True;
  end;
end;

function ShouldSkipPage(PageID: Integer): Boolean;
begin
    Result := False;
    if IsWin7() then begin
        if (PageID = g_UpdatePage.ID) then begin
            Result := is_silent;
            Exit;
        end;
    end;

    if Custom_IsUpgrade() then begin
        if (PageID = wpLicense) or (PageID = wpInfoBefore) then begin
            Result := True;
        end else begin
            Result := False;
        end;
    end;

    if (PageID = g_SerialPage.ID) then begin
        if ExpandConstant('{param:skipserial|false}') = 'true' then
            Result := True;
        if is_silent then
            Result := True;
    end;
end;

procedure CurPageChanged(CurPageID: Integer);
begin
    if IsWin7() then
    begin
        if CurPageID = g_UpdatePage.ID then WizardForm.NextButton.Enabled := True;
    end;
    
    if CurPageID = g_SerialPage.ID then
        WizardForm.NextButton.Enabled := False;
    
    if CurPageID = wpLicense then begin
        WizardForm.BackButton.Enabled := False;
    end else begin
        WizardForm.BackButton.Enabled := True;
    end;
end;

procedure CurStepChanged(CurStep: TSetupStep);
begin 
  if CurStep = ssInstall then begin
  //  if Custom_StartupRemove() = False then Custom_LastErrorShow('Error occurred while removing startup task');
  //  if Custom_ServiceRemove() = False then Custom_LastErrorShow('Error occurred while removing background service');
      Custom_StartupRemove();
      Custom_ServiceRemove();
      Log('Custom Code: Trying to close agent that runs in background mode');
      Custom_CloseAgent();
  end;

  if CurStep = ssPostInstall then begin

    Log('Custom Code: Creating startup task');
    if Custom_StartupCreate() = False then Custom_LastErrorShow('Error occurred while creating startup task');
    if Custom_ServiceCreate() = False then Custom_LastErrorShow('Error occurred while creating background service');
    Custom_SetDefaultWatchFolder();
    
    if IsTaskSelected('delete_arc') and not is_silent then begin
       Log('Custom Code: User selected the "delete_arc" task, calling RemoveArc()');
       Custom_RemoveArc;
    end;

    if IsTaskSelected('delete_db') and not is_silent then begin
       Log('Custom Code: User selected the "delete_db" task, calling RemoveDb()');
       Custom_RemoveDb;
    end;

    if Custom_ServiceStart() = False then Custom_LastErrorShow('Error occurred while staring background service');
    Log('Custom Code: Starting agent');
    Custom_StartAgent();
  end;
end;

function NextButtonClick(CurPageID: Integer): Boolean;
begin
    if IsWin7() then begin
        if CurPageID = g_UpdatePage.ID then Result:= True;
    end;
    if CurPageID = g_SerialPage.ID then begin
        Result:= Custom_SN_Register();
    end else begin
        Result:=True;
   end;
end;

procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
  
    if CurUninstallStep = usUninstall then begin
        if (Custom_DbExist() or Custom_ArcExist(true)) then begin
            if SuppressibleMsgBox(CustomMessage('msg_client_delete_data'), mbConfirmation, MB_YESNO or MB_DEFBUTTON2, IDNO) = IDYES then begin
               Custom_RemoveDb();
               Custom_RemoveArc();
            end;
        end;

        if Custom_StartupRemove() = False then Custom_LastErrorShow('Error occurred while removing startup task');
        Log('Custom Code: Closing agent');
        Custom_CloseAgent();

        // Always stop and remove background service just in case
        if Custom_ServiceStop() = False then Custom_LastErrorShow('Error occurred while stopping background service');
        if Custom_ServiceRemove() = false then Custom_LastErrorShow('Error occurred while removing background service');

        // removes license from web licensing service
        if not is_silent then Custom_SN_Remove();
        RemoveDir(ExpandConstant('{app}'));
    end;
end;

