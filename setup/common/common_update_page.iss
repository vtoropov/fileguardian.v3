(*
; Created by Tech_dog (ebontrop@gmail.com) on 26-Apr-2018 at 7:38:25p, UTC+7, Novosibirsk, Rodniki, Thursday;
; This is File Guardian windows and system software update setup page implementation file.
*)

[Code]

var
  g_UpdatePage : TWizardPage;

procedure Update_OpenLink(sLink: string);
var
    nErrorCode  : Integer;
begin
    ShellExec('open', sLink, '', '', SW_SHOWNORMAL, ewNoWait, nErrorCode);
end;

procedure Update_GoToAdvisory(Sender: TObject);
begin 
    Update_OpenLink('https://support.microsoft.com/en-us/help/3033929/microsoft-security-advisory-availability-of-sha-2-code-signing-support');
end;

procedure Update_GoToDownload(Sender: TObject);
begin 
    Update_OpenLink('https://www.microsoft.com/en-us/download/details.aspx?id=46148');
end;

procedure Update_CreatePage(sWarnIcoFile: string);
var
    DescLabel   : TLabel ;
    Link_Advy   : TLabel ;
    Link_Down   : TLabel ;
begin
    g_UpdatePage := CreateCustomPage(
        wpWelcome, 'MS Windows Update', 'Download and install Windows security package'
    );

    DescLabel         := TLabel.Create(g_UpdatePage);
    DescLabel.Top     := 10;
    DescLabel.Left    := 10;
    DescLabel.WordWrap:= True;
    DescLabel.AutoSize:= False;
    DescLabel.Parent  := g_UpdatePage.Surface;
    DescLabel.Caption := 'Windows 7 requires a security update for availability of SHA-2 code signing support.' + #13#10
                       + 'This is necessary for loading digitally signed driver in 64-bit environment. The original info can be found here:';
    DescLabel.Width   := g_UpdatePage.Surface.Width;
    DescLabel.Height  := 40;

    Link_Advy         := TLabel.Create(g_UpdatePage);
    Link_Advy.Top     := DescLabel.Top + DescLabel.Height;
    Link_Advy.Left    := DescLabel.Left;
    Link_Advy.WordWrap:= True;
    Link_Advy.AutoSize:= False;
    Link_Advy.Parent  := g_UpdatePage.Surface;
    Link_Advy.Caption := 'Microsoft security advisory: Availability of SHA-2 code signing support for Windows 7 and Windows Server 2008 R2: March 10, 2015';
    Link_Advy.OnClick := @Update_GoToAdvisory;
    Link_Advy.ParentFont := True;
    Link_Advy.Font.Style := Link_Advy.Font.Style + [fsUnderline];
    Link_Advy.Font.Color := clBlue;
    Link_Advy.Cursor  := crHand;
    Link_Advy.Width   := DescLabel.Width - 30;
    Link_Advy.Height  := 40;

    DescLabel         := TLabel.Create(g_UpdatePage);
    DescLabel.Top     := Link_Advy.Top + Link_Advy.Height;
    DescLabel.Left    := Link_Advy.Left;
    DescLabel.WordWrap:= True;
    DescLabel.AutoSize:= False;
    DescLabel.Parent  := g_UpdatePage.Surface;
    DescLabel.Caption := 'Download link:' + #13#10
                       + '';
    DescLabel.Width   := Link_Advy.Width;
    DescLabel.Height  := 15;

    Link_Down         := TLabel.Create(g_UpdatePage);
    Link_Down.Top     := DescLabel.Top + DescLabel.Height;
    Link_Down.Left    := DescLabel.Left;
    Link_Down.WordWrap:= True;
    Link_Down.AutoSize:= False;
    Link_Down.Parent  := g_UpdatePage.Surface;
    Link_Down.Caption := 'Security Update for Windows 7 for x64-based Systems (KB3033929)';
    Link_Down.OnClick := @Update_GoToDownload;
    Link_Down.ParentFont := True;
    Link_Down.Font.Style := Link_Advy.Font.Style + [fsUnderline];
    Link_Down.Font.Color := clBlue;
    Link_Down.Cursor  := crHand;
    Link_Down.Width   := DescLabel.Width - 30;
    Link_Down.Height  := 15;
end;



