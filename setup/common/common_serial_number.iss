(*
; Created by vtoropov on 24-Dec-2012 at 6:46:23pm, GMT+3, Rostov-on-Don, Monday;
; This is serial number input script for creating serial number page of setup wizard;
*)

[Code]
function WinAPI_SetFocus(hWnd: HWND): HWND;                  external 'SetFocus@user32.dll stdcall';
function WinAPI_OpenClipboard(hWndNewOwner: HWND): BOOL;     external 'OpenClipboard@user32.dll stdcall';
function WinAPI_GetClipboardData(uFormat: UINT): THandle;    external 'GetClipboardData@user32.dll stdcall';
function WinAPI_CloseClipboard: BOOL;                        external 'CloseClipboard@user32.dll stdcall';
function WinAPI_GlobalLock(hMem: THandle): PAnsiChar;        external 'GlobalLock@kernel32.dll stdcall';
function WinAPI_GlobalUnlock(hMem: THandle): BOOL;           external 'GlobalUnlock@kernel32.dll stdcall';

var
  g_SerialPage : TWizardPage;
  g_SerialEdits: array of TEdit;
  g_DescEdit   : TEdit;

const
  CF_TEXT = 1;
  VK_BACK = 8;
  SC_EDITCOUNT = 4;
  SC_CHARCOUNT = 4;

function SN_GetClipboardText: string;
var
  Data: THandle;
begin
  Result := '';
  if WinAPI_OpenClipboard(0) then
  try
    Data := WinAPI_GetClipboardData(CF_TEXT);
    if Data <> 0 then
      Result := String(WinAPI_GlobalLock(Data));
  finally
    if Data <> 0 then
      WinAPI_GlobalUnlock(Data);
    WinAPI_CloseClipboard;
  end;
end;

function SN_TryPasteNumber: Boolean;
var  
  S: string;
  I: Integer;
  J: Integer;
  Delimiter: string;
begin
  Result := True;
  Delimiter := '-';
  S := SN_GetClipboardText();    
  if Length(S) <> ((SC_EDITCOUNT * SC_CHARCOUNT) + 
    ((SC_EDITCOUNT - 1) * Length(Delimiter))) then
  begin
    SuppressibleMsgBox(
        'There is incorrect data in the clipboard. ' +
        'The data is expected in the format: XXXX-XXXX-XXXX-XXXX',
        mbError,
        MB_OK,
        MB_OK
      );
    Exit;
  end;

  WinAPI_SetFocus(g_SerialEdits[0].Handle);

  for I := 0 to GetArrayLength(g_SerialEdits) - 1 do
  begin
    J := (I * SC_CHARCOUNT) + (I * Length(Delimiter)) + 1;
    g_SerialEdits[I].Text := Copy(S, J, SC_CHARCOUNT);
  end;
end;

procedure SN_TryPasteOnClick(Sender: TObject);
begin
  SN_TryPasteNumber();
end;

procedure SN_OnEditChange(Sender: TObject);
var
  I: Integer;
  CanContinue: Boolean;
begin
  CanContinue := True;
  for I := 0 to GetArrayLength(g_SerialEdits) - 1 do
    if Length(g_SerialEdits[I].Text) < SC_CHARCOUNT then
    begin
      CanContinue := False;
      Break;
    end;
  WizardForm.NextButton.Enabled := CanContinue;
end;

procedure SN_OnEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Edit: TEdit;
  EditIndex: Integer;
begin
  Edit := TEdit(Sender);
  EditIndex := Edit.TabOrder - g_SerialEdits[0].TabOrder;
  if (EditIndex = 0) and (Key = Ord('V')) and (Shift = [ssCtrl]) then
  begin
    if SN_TryPasteNumber() then
      Key := 0;
  end
  else
  if (Key >= 32) and (Key <= 255) then
  begin
    if Length(Edit.Text) = SC_CHARCOUNT - 1 then
    begin
      if EditIndex < GetArrayLength(g_SerialEdits) - 1 then
        WinAPI_SetFocus(g_SerialEdits[EditIndex + 1].Handle)
      else
        WinAPI_SetFocus(WizardForm.NextButton.Handle);
    end;
  end
  else
  if Key = VK_BACK then
    if (EditIndex > 0) and (Edit.Text = '') and (Edit.SelStart = 0) then
      WinAPI_SetFocus(g_SerialEdits[EditIndex - 1].Handle);
end;


procedure SN_CreatePage(nPriorPageID: cardinal);
var
  i_          : Integer;
  Edit        : TEdit  ;
  SerialLabel : TLabel ;
  EditWidth   : Integer;
  PasteButton : TNewButton;
  DescLabel   : TLabel ;
begin
  g_SerialPage := CreateCustomPage(
		nPriorPageID, 'Serial number validation', 'Enter the valid serial number'
	);

  SerialLabel := TLabel.Create(g_SerialPage);
  SerialLabel.Top := 16;
  SerialLabel.Left := 0;
  SerialLabel.Parent := g_SerialPage.Surface;
  SerialLabel.Caption := 'Enter the valid serial number and continue with the installation';

  SetArrayLength(g_SerialEdits, SC_EDITCOUNT);
  EditWidth  := (g_SerialPage.SurfaceWidth - ((SC_EDITCOUNT - 1) * 8)) div SC_EDITCOUNT;

  for i_ := 0 to SC_EDITCOUNT - 1 do
  begin
    Edit            := TEdit.Create(g_SerialPage);
    Edit.Top        := 40;
    Edit.Left       := i_ * (EditWidth + 8);       
    Edit.Width      := EditWidth;
    Edit.CharCase   := ecUpperCase;
    Edit.MaxLength  := SC_CHARCOUNT;
    Edit.Parent     := g_SerialPage.Surface;
    Edit.OnChange   := @SN_OnEditChange;
    Edit.OnKeyDown  := @SN_OnEditKeyDown;
  g_SerialEdits[i_] := Edit;
  end;

  PasteButton         := TNewButton.Create(g_SerialPage);
  PasteButton.Parent  := g_SerialPage.Surface;
  PasteButton.Width   := g_SerialEdits[3].Width    + g_SerialEdits[3].Width    /  2;
  PasteButton.Left    := g_SerialEdits[3].Left     + g_SerialEdits[3].Width    - PasteButton.Width;
  PasteButton.Top     := g_SerialEdits[3].Top      + g_SerialEdits[3].Height   *  2;
  PasteButton.Caption := 'Paste from Clipboard';
  PasteButton.OnClick := @SN_TryPasteOnClick;

  DescLabel         := TLabel.Create(g_SerialPage);
  DescLabel.Top     := PasteButton.Top + PasteButton.Height + 10;
  DescLabel.Left    := 0;
  DescLabel.Parent  := g_SerialPage.Surface;
  DescLabel.Caption := 'Computer Description (Optional)';

  EditWidth := (g_SerialEdits[3].Left + g_SerialEdits[3].Width) - DescLabel.Left;

  Edit              := TEdit.Create(g_SerialPage);
  Edit.Top          := DescLabel.Top  + DescLabel.Height + 10;
  Edit.Left         := DescLabel.Left;       
  Edit.Width        := EditWidth;
  Edit.Parent       := g_SerialPage.Surface;
  g_DescEdit        := Edit;

  DescLabel         := TLabel.Create(g_SerialPage);
  DescLabel.Top     := g_DescEdit.Top + g_DescEdit.Height + 10;
  DescLabel.Left    := 0;
  DescLabel.WordWrap:= True;
  DescLabel.AutoSize:= False;
  DescLabel.Parent  := g_SerialPage.Surface;
  DescLabel.Height  := g_DescEdit.Height *3;
  DescLabel.Width   := g_DescEdit.Width;
  DescLabel.Caption := 'Please enter a user friendly description of this workstation. '
                     + 'This description will make it simpler to identify which computer, monitored files system events belong to on your cloud services account. '
                     + 'It is recommended that you enter a description if you monitor or plan to monitor more than one computer.';
  Win32_SetCueBannerText(g_DescEdit.Handle, 'For example: Sales Computer 1');
end;

procedure SN_SetControlCursor(control: TWinControl; cursor: TCursor);
var i:Integer;
    wc: TWinControl;
begin
  if (not (control = nil)) then begin
    control.Cursor := cursor;
    try
      for i:=0 to control.ControlCount-1 do begin
        wc := TWinControl(control.Controls[i]);
        if (NOT(wc = nil)) then
          SN_SetControlCursor(wc, cursor)
        else
          control.Controls[i].Cursor := cursor;
      end;
    finally

    end;
  end;
end;
/////////////////////////////////////////////////////////////////////////////

function SN_GetDescription(): String;
begin
   Result := g_DescEdit.Text;
end;

function SN_GetNumber(): String;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to GetArrayLength(g_SerialEdits) - 1 do
    Result := Result + g_SerialEdits[I].Text;
end;

