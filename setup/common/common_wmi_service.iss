(*
;-----------------------------------------------------------------------------
; Created by Tech_dog (ebontrop@gmail.com) on 19-Aug-2016 at 6:05:20p, GMT+7, Phuket, Rawai, Friday;
; This is MS Windows Management Instrumentation (WMI) service related code;
;-----------------------------------------------------------------------------
*)

[Code]

// #############################################################################
// Checks the specific process is running by its name
// #############################################################################

function wmi_IsProcRunning(const FileName : string): Boolean;
var
    FSWbemLocator : Variant;
    FWMIService   : Variant;
    FWbemObjectSet: Variant;
begin
    Result := false;
    try
        FSWbemLocator  := CreateOleObject('WBEMScripting.SWBEMLocator');
        FWMIService    := FSWbemLocator.ConnectServer('', 'root\CIMV2', '', '');
        FWbemObjectSet := FWMIService.ExecQuery(Format('SELECT Name FROM Win32_Process Where Name="%s"',[FileName]));
        Result := (FWbemObjectSet.Count > 0);
    except
    end;
    FWbemObjectSet := Unassigned;
    FWMIService    := Unassigned;
    FSWbemLocator  := Unassigned;
end;

// #############################################################################
// Checks the specific KBXXXXXXXX update, which may be installed on the system;
// #############################################################################

function wmi_IsUpdateInstalled(const KbName : string): Boolean;
var
    FSWbemLocator : Variant;
    FWMIService   : Variant;
    FWbemObjectSet: Variant;
begin
    Result := false;
    try
        FSWbemLocator  := CreateOleObject('WBEMScripting.SWBEMLocator');
        FWMIService    := FSWbemLocator.ConnectServer('', 'root\CIMV2', '', '');
        FWbemObjectSet := FWMIService.ExecQuery(Format('SELECT HotFixID FROM Win32_QuickFixEngineering Where HotFixID="%s"',[KbName]));
        Result := (FWbemObjectSet.Count > 0);
    except
    end;
    FWbemObjectSet := Unassigned;
    FWMIService    := Unassigned;
    FSWbemLocator  := Unassigned;
end;