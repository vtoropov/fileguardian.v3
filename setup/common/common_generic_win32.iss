(*
;-----------------------------------------------------------------------------
; Created by Tech_dog (VToropov) on 5-Nov-2016 at 2:24:29pm, GMT+7, Phuket, Rawai, Saturday;
; This is MS Windows Common Win32 API wrapper(s) related code for application installer(s);
;-----------------------------------------------------------------------------
*)


[Code]

function Win32_GetLastError() : LongInt; external 'GetLastError@kernel32.dll stdcall';


const ECM_FIRST       = $1500;
const EM_SETCUEBANNER = ECM_FIRST + $1;

function Win32_SetCueBannerText(hWnd : HWND; BannerText : String) : boolean;
var
  err_code: LongInt;
begin
  Result := (0 <> SendMessage(hWnd, EM_SETCUEBANNER, 1, CastStringToInteger(BannerText)));
  if     Result then Log('Custom Code: cue banner text is set');
  if not Result then 
  begin
     err_code := Win32_GetLastError();
     Log('Custom Code: cue banner text error=' + IntToStr(err_code));
  end;
end;



