(*
;-----------------------------------------------------------------------------
; Created by Tech_dog (VToropov) on 30-Apr-2014 at 6:40:11pm, GMT+4, Saint-Petersburg, Wednesday;
; This is MS Windows Service Control Manager related code for application installer(s);
;-----------------------------------------------------------------------------
*)


[Code]
// Various service related functions

type
  SERVICE_STATUS = record
    dwServiceType             : cardinal;
    dwCurrentState            : cardinal;
    dwControlsAccepted        : cardinal;
    dwWin32ExitCode           : cardinal;
    dwServiceSpecificExitCode : cardinal;
    dwCheckPoint              : cardinal;
    dwWaitHint                : cardinal;
  end;
  HANDLE = cardinal;

const
  SERVICE_QUERY_CONFIG        = $1;
  SERVICE_CHANGE_CONFIG       = $2;
  SERVICE_QUERY_STATUS        = $4;
  SERVICE_START               = $10;
  SERVICE_STOP                = $20;
  SERVICE_ALL_ACCESS          = $f01ff;
  SC_MANAGER_ALL_ACCESS       = $f003f;
  SERVICE_KERNEL_DRIVER       = $1;
  SERVICE_WIN32_OWN_PROCESS   = $10;
  SERVICE_WIN32_SHARE_PROCESS = $20;
  SERVICE_WIN32               = $30;
  SERVICE_INTERACTIVE_PROCESS = $100;
  SERVICE_BOOT_START          = $0;
  SERVICE_SYSTEM_START        = $1;
  SERVICE_AUTO_START          = $2;
  SERVICE_DEMAND_START        = $3;
  SERVICE_DISABLED            = $4;
  SERVICE_DELETE              = $10000;
  SERVICE_CONTROL_STOP        = $1;
  SERVICE_CONTROL_PAUSE       = $2;
  SERVICE_CONTROL_CONTINUE    = $3;
  SERVICE_CONTROL_INTERROGATE = $4;
  SERVICE_STOPPED             = $1;
  SERVICE_START_PENDING       = $2;
  SERVICE_STOP_PENDING        = $3;
  SERVICE_RUNNING             = $4;
  SERVICE_CONTINUE_PENDING    = $5;
  SERVICE_PAUSE_PENDING       = $6;
  SERVICE_PAUSED              = $7;

// #######################################################################################
// NT based service utilities
// #######################################################################################
function WinAPI__OpenSCManager(lpMachineName, lpDatabaseName: AnsiString; dwDesiredAccess: cardinal): HANDLE;
external 'OpenSCManagerA@advapi32.dll stdcall';

function WinAPI__OpenService(hSCManager: HANDLE; lpServiceName: AnsiString; dwDesiredAccess: cardinal): HANDLE;
external 'OpenServiceA@advapi32.dll stdcall';

function WinAPI__CloseServiceHandle(hSCObject: HANDLE): Boolean;
external 'CloseServiceHandle@advapi32.dll stdcall';

function WinAPI__CreateService(hSCManager: HANDLE; lpServiceName, lpDisplayName: AnsiString; dwDesiredAccess,dwServiceType,dwStartType,dwErrorControl: cardinal; lpBinaryPathName,lpLoadOrderGroup: AnsiString; lpdwTagId: cardinal; lpDependencies,lpServiceStartName,lpPassword: AnsiString): cardinal;
external 'CreateServiceA@advapi32.dll stdcall';

function WinAPI__DeleteService(hService: HANDLE): Boolean;
external 'DeleteService@advapi32.dll stdcall';

function WinAPI__StartNTService(hService: HANDLE; dwNumServiceArgs: cardinal; lpServiceArgVectors: cardinal): Boolean;
external 'StartServiceA@advapi32.dll stdcall';

function WinAPI__ControlService(hService: HANDLE; dwControl: cardinal; var ServiceStatus: SERVICE_STATUS): Boolean;
external 'ControlService@advapi32.dll stdcall';

function WinAPI__QueryServiceStatus(hService: HANDLE; var ServiceStatus: SERVICE_STATUS): Boolean;
external 'QueryServiceStatus@advapi32.dll stdcall';

function WinAPI__QueryServiceStatusEx(hService: HANDLE; ServiceStatus: SERVICE_STATUS): Boolean;
external 'QueryServiceStatus@advapi32.dll stdcall';


function Services_OpenServiceManager(): HANDLE;
begin
  if UsingWinNT() = True then begin
    Result := WinAPI__OpenSCManager('','ServicesActive',SC_MANAGER_ALL_ACCESS);
    if Result = 0 then
      SuppressibleMsgBox(CustomMessage('msg_ServiceManager'), mbError, MB_OK, MB_OK)
  end
  else begin
    SuppressibleMsgBox(CustomMessage('msg_ServiceManager2'), mbError, MB_OK, MB_OK)
    Result := 0;
  end;
end;


function Services_InstallService(FileName, ServiceName, DisplayName, Description: AnsiString; ServiceType,StartType: cardinal): Boolean;
var
  hSCM    : HANDLE;
  hService: HANDLE;
begin
  hSCM   := Services_OpenServiceManager();
  Result := False;
  if hSCM <> 0 then begin
    hService := WinAPI__CreateService(hSCM,ServiceName,DisplayName,SERVICE_ALL_ACCESS,ServiceType,StartType,0,FileName,'',0,'','','');
    if hService <> 0 then begin
      Result := True;
      // Win2K & WinXP supports aditional description text for services
      if Description<> '' then
        RegWriteStringValue(HKLM,'System\CurrentControlSet\Services\' + ServiceName,'Description',Description);
      WinAPI__CloseServiceHandle(hService)
    end;
    WinAPI__CloseServiceHandle(hSCM)
  end;
end;


function Services_RemoveService(ServiceName: String): Boolean;
var
  hSCM    : HANDLE;
  hService: HANDLE;
begin
  hSCM   := Services_OpenServiceManager();
  Result := False;
  if hSCM <> 0 then begin
    hService := WinAPI__OpenService(hSCM,ServiceName,SERVICE_DELETE);
    if hService <> 0 then begin
      Result := WinAPI__DeleteService(hService);
      WinAPI__CloseServiceHandle(hService)
    end;
    WinAPI__CloseServiceHandle(hSCM)
  end;
end;


function Services_StartService(ServiceName: String): Boolean;
var
  hSCM    : HANDLE;
  hService: HANDLE;
begin
  hSCM   := Services_OpenServiceManager();
  Result := False;
  if hSCM <> 0 then begin
    hService := WinAPI__OpenService(hSCM,ServiceName,SERVICE_START);
    if hService <> 0 then begin
      Result := WinAPI__StartNTService(hService,0,0);
      WinAPI__CloseServiceHandle(hService)
    end;
    WinAPI__CloseServiceHandle(hSCM)
  end;
end;


function Services_StopService(ServiceName: String): Boolean;
var
  hSCM    : HANDLE;
  hService: HANDLE;
  Status  : SERVICE_STATUS;
begin
  hSCM   := Services_OpenServiceManager();
  Result := False;
  if hSCM <> 0 then begin
    hService := WinAPI__OpenService(hSCM,ServiceName,SERVICE_STOP);
    if hService <> 0 then begin
      Result := WinAPI__ControlService(hService,SERVICE_CONTROL_STOP,Status);
      WinAPI__CloseServiceHandle(hService)
    end;
    WinAPI__CloseServiceHandle(hSCM)
  end;
end;


function Services_IsServiceRunning(ServiceName: String): Boolean;
var
  hSCM    : HANDLE;
  hService: HANDLE;
  Status  : SERVICE_STATUS;
begin
  hSCM   := Services_OpenServiceManager();
  Result := False;
  if hSCM <> 0 then begin
    hService := WinAPI__OpenService(hSCM,ServiceName,SERVICE_QUERY_STATUS);
    if hService <> 0 then begin
      if WinAPI__QueryServiceStatus(hService,Status) then begin
        Result := (Status.dwCurrentState = SERVICE_RUNNING)
      end;
      WinAPI__CloseServiceHandle(hService)
      end;
    WinAPI__CloseServiceHandle(hSCM)
  end;
end;
