#ifndef _FGFSEVTSPYIFACE_H_BCCFC8D1_375E_4DD9_8807_3EF0E19E406C_INCLUDED
#define _FGFSEVTSPYIFACE_H_BCCFC8D1_375E_4DD9_8807_3EF0E19E406C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jun-2018 at 4:26:47p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver event spy interface declaration file.
*/
#include "fg.fs.evt.spy.kernel.h"

namespace shared { namespace km { namespace spy {

	//
	//  FltMgr's IRP major codes
	//
	class eFsMajorFuncType {
	public:
		enum _irp {
			eSectionSyncAcuire     = ((UCHAR)-01),
			eSectionSyncRelease    = ((UCHAR)-02),
			eModeWriteAquire       = ((UCHAR)-03),
			eModeWriteRelease      = ((UCHAR)-04),
			eCCFlushAquire         = ((UCHAR)-05),
			eCCFlushRelease        = ((UCHAR)-06),
			eStreamFoCreateNotify  = ((UCHAR)-07),
			eFastIoIfPossibleCheck = ((UCHAR)-13),
			eNetworkQueryOpen      = ((UCHAR)-14),
			eMdlRead               = ((UCHAR)-15),
			eMdlReadComplete       = ((UCHAR)-16),
			eMdlWritePrepare       = ((UCHAR)-17),
			eMdlWriteComplete      = ((UCHAR)-18),
			eVolumwMount           = ((UCHAR)-19),
			eVolumeDismount        = ((UCHAR)-20),
			eTransactNotify        = ((UCHAR)-40),
		};
	};

	class eFsTransactNotifyCode {
	public:
		enum _e {
			eBegin              =  0,
			ePrePrepare         =  1,
			ePrepare                ,
			eCommit                 ,
			eRollback               ,
			ePrePrepareComplete     ,
			ePrepareComplete        ,
			eCommitComplete         ,
			eRollbackComplete       ,
			eRecover                ,
			eSinglePhaseCommit      ,
			eDelegateCommit         ,
			eRecoverQuery           ,
			eEnlistPrepare          ,
			eRecoverLast            ,
			eIndoubt                ,
			ePropagatePull          ,
			ePropagatePush          ,
			eMarshal                ,
			eEnlistMask             ,
			eCommitFinalize     = 31,
		};
	};

#ifndef _COMP_PATH
#define _COMP_PATH (512)
#endif

	typedef struct _t_fs_evt_data
	{
		ULONG uFuncMajor;                 // predefined major function type
		ULONG uFuncMinor;                 // predefined minor function type

		LARGE_INTEGER luTimestamp;
		ULONG luProcessId;        // a process identifier; this process originates an event;

		WCHAR wFileAffected[_COMP_PATH];  // file affected by an event, if any
		WCHAR wProcApplied [_COMP_PATH];  // process image path that initiates an event
	}
	TFs_EvtData, *PTFs_EvtData;

	class CEvtSpyDriver_deprecated {
	public:
		static PWCHAR Port(void);
	};

#define	_EVT_SPY_PORT_RAW_NAME _T("\\Fg_log_spy")

#define _EVT_RECORD_BUFFER_CAP    100     // defines user mode buffer capacity

	//
	// structure of user mode application buffer for data exchange; 
	//
	typedef struct _t_fs_evt_buffer
	{
		ULONG        uRecCount;           // the counter of the records: on driver input the counter indicates how many records a buffer can accomodate;
										  //                             on driver output the counter contains actual number of records in the buffer;
		PTFs_EvtData  pRecords;           // points to the records' buffer that is allocated on the client side;
	}
	TFs_EvtBuffer, *PTFs_EvtBuffer;

	//
	//  Defines the commands between the user mode app and the filter
	//

	class eFsCommandType {
	public:
		enum _e {
			eNone            = 0x00, // command type is not defined;
			eGetEvents       = 0x01, // command type for getting file events;
		};
	};

	class eFsCommandOpt {
	public:
		enum _e {
			eWDataLen = _COMP_PATH,
		};
	};

	//
	//  Defines a command structure between a user mode app and a filter;
	//  command structure can be used in both roles simultaneously: as input and output buffer;
	//
	typedef struct _t_fs_command
	{
		eFsCommandType::_e
		          eCmdType;   // eNone, 0 by default;
		ULONG     nFlags;     // flags, 0 by default;
	}
	TFs_Command, *PTFs_Command;
}}}

#endif/*_FGFSEVTSPYIFACE_H_BCCFC8D1_375E_4DD9_8807_3EF0E19E406C_INCLUDED*/