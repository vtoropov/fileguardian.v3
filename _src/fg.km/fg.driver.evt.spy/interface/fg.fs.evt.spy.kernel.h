#ifndef _FGFSEVTSPYWMF_H_BB7874E1_7C6E_4413_B694_5BC77AF0B407_INCLUDED
#define _FGFSEVTSPYWMF_H_BB7874E1_7C6E_4413_B694_5BC77AF0B407_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jun-2018 at 1:54:39a, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver event spy kernel mode definition file;
*/

//
// Define the major function codes for IRPs.
//
// copied from <wdm.h>
//

#define IRP_MJ_CREATE                       0x00
#define IRP_MJ_CREATE_NAMED_PIPE            0x01
#define IRP_MJ_CLOSE                        0x02
#define IRP_MJ_READ                         0x03
#define IRP_MJ_WRITE                        0x04
#define IRP_MJ_QUERY_INFORMATION            0x05
#define IRP_MJ_SET_INFORMATION              0x06
#define IRP_MJ_QUERY_EA                     0x07
#define IRP_MJ_SET_EA                       0x08
#define IRP_MJ_FLUSH_BUFFERS                0x09
#define IRP_MJ_QUERY_VOLUME_INFORMATION     0x0a
#define IRP_MJ_SET_VOLUME_INFORMATION       0x0b
#define IRP_MJ_DIRECTORY_CONTROL            0x0c
#define IRP_MJ_FILE_SYSTEM_CONTROL          0x0d
#define IRP_MJ_DEVICE_CONTROL               0x0e
#define IRP_MJ_INTERNAL_DEVICE_CONTROL      0x0f
#define IRP_MJ_SHUTDOWN                     0x10
#define IRP_MJ_LOCK_CONTROL                 0x11
#define IRP_MJ_CLEANUP                      0x12
#define IRP_MJ_CREATE_MAILSLOT              0x13
#define IRP_MJ_QUERY_SECURITY               0x14
#define IRP_MJ_SET_SECURITY                 0x15
#define IRP_MJ_POWER                        0x16
#define IRP_MJ_SYSTEM_CONTROL               0x17
#define IRP_MJ_DEVICE_CHANGE                0x18
#define IRP_MJ_QUERY_QUOTA                  0x19
#define IRP_MJ_SET_QUOTA                    0x1a
#define IRP_MJ_PNP                          0x1b
#define IRP_MJ_PNP_POWER                    IRP_MJ_PNP      // Obsolete....
#define IRP_MJ_MAXIMUM_FUNCTION             0x1b

// ntddk.h

#define IRP_MN_NORMAL                       0x00
#define IRP_MN_DPC                          0x01
#define IRP_MN_MDL                          0x02
#define IRP_MN_COMPLETE                     0x04
#define IRP_MN_COMPRESSED                   0x08

#define IRP_MN_MDL_DPC                      (IRP_MN_MDL | IRP_MN_DPC)
#define IRP_MN_COMPLETE_MDL                 (IRP_MN_COMPLETE | IRP_MN_MDL)
#define IRP_MN_COMPLETE_MDL_DPC             (IRP_MN_COMPLETE_MDL | IRP_MN_DPC)

#define IRP_MN_QUERY_DIRECTORY              0x01
#define IRP_MN_NOTIFY_CHANGE_DIRECTORY      0x02

#define IRP_MN_USER_FS_REQUEST              0x00
#define IRP_MN_MOUNT_VOLUME                 0x01
#define IRP_MN_VERIFY_VOLUME                0x02
#define IRP_MN_LOAD_FILE_SYSTEM             0x03
#define IRP_MN_TRACK_LINK                   0x04    // To be obsoleted soon
#define IRP_MN_KERNEL_CALL                  0x04

#define IRP_MN_SCSI_CLASS                   0x01

#define IRP_MN_LOCK                         0x01
#define IRP_MN_UNLOCK_SINGLE                0x02
#define IRP_MN_UNLOCK_ALL                   0x03
#define IRP_MN_UNLOCK_ALL_BY_KEY            0x04

#define IRP_MN_WAIT_WAKE                    0x00
#define IRP_MN_POWER_SEQUENCE               0x01
#define IRP_MN_SET_POWER                    0x02
#define IRP_MN_QUERY_POWER                  0x03

#define IRP_MN_QUERY_ALL_DATA               0x00
#define IRP_MN_QUERY_SINGLE_INSTANCE        0x01
#define IRP_MN_CHANGE_SINGLE_INSTANCE       0x02
#define IRP_MN_CHANGE_SINGLE_ITEM           0x03
#define IRP_MN_ENABLE_EVENTS                0x04
#define IRP_MN_DISABLE_EVENTS               0x05
#define IRP_MN_ENABLE_COLLECTION            0x06
#define IRP_MN_DISABLE_COLLECTION           0x07
#define IRP_MN_REGINFO                      0x08
#define IRP_MN_EXECUTE_METHOD               0x09

#define IRP_MN_START_DEVICE                 0x00
#define IRP_MN_QUERY_REMOVE_DEVICE          0x01
#define IRP_MN_REMOVE_DEVICE                0x02
#define IRP_MN_CANCEL_REMOVE_DEVICE         0x03
#define IRP_MN_STOP_DEVICE                  0x04
#define IRP_MN_QUERY_STOP_DEVICE            0x05
#define IRP_MN_CANCEL_STOP_DEVICE           0x06

#define IRP_MN_QUERY_DEVICE_RELATIONS       0x07
#define IRP_MN_QUERY_INTERFACE              0x08
#define IRP_MN_QUERY_CAPABILITIES           0x09
#define IRP_MN_QUERY_RESOURCES              0x0A
#define IRP_MN_QUERY_RESOURCE_REQUIREMENTS  0x0B
#define IRP_MN_QUERY_DEVICE_TEXT            0x0C
#define IRP_MN_FILTER_RESOURCE_REQUIREMENTS 0x0D

#define IRP_MN_READ_CONFIG                  0x0F
#define IRP_MN_WRITE_CONFIG                 0x10
#define IRP_MN_EJECT                        0x11
#define IRP_MN_SET_LOCK                     0x12
#define IRP_MN_QUERY_ID                     0x13
#define IRP_MN_QUERY_PNP_DEVICE_STATE       0x14
#define IRP_MN_QUERY_BUS_INFORMATION        0x15
#define IRP_MN_DEVICE_USAGE_NOTIFICATION    0x16
#define IRP_MN_SURPRISE_REMOVAL             0x17

#define IRP_MN_QUERY_LEGACY_BUS_INFORMATION 0x18

//
// Define I/O Request Packet (IRP) flags
//

#define IRP_NOCACHE                   0x00000001
#define IRP_PAGING_IO                 0x00000002
#define IRP_MOUNT_COMPLETION          0x00000002
#define IRP_SYNCHRONOUS_API           0x00000004
#define IRP_ASSOCIATED_IRP            0x00000008
#define IRP_BUFFERED_IO               0x00000010
#define IRP_DEALLOCATE_BUFFER         0x00000020
#define IRP_INPUT_OPERATION           0x00000040
#define IRP_SYNCHRONOUS_PAGING_IO     0x00000040
#define IRP_CREATE_OPERATION          0x00000080
#define IRP_READ_OPERATION            0x00000100
#define IRP_WRITE_OPERATION           0x00000200
#define IRP_CLOSE_OPERATION           0x00000400
#define IRP_DEFER_IO_COMPLETION       0x00000800
#define IRP_OB_QUERY_NAME             0x00001000
#define IRP_HOLD_DEVICE_QUEUE         0x00002000


//
//  FltMgr's IRP major codes
//

#define IRP_MJ_ACQUIRE_FOR_SECTION_SYNCHRONIZATION  ((UCHAR)-1)
#define IRP_MJ_RELEASE_FOR_SECTION_SYNCHRONIZATION  ((UCHAR)-2)
#define IRP_MJ_ACQUIRE_FOR_MOD_WRITE                ((UCHAR)-3)
#define IRP_MJ_RELEASE_FOR_MOD_WRITE                ((UCHAR)-4)
#define IRP_MJ_ACQUIRE_FOR_CC_FLUSH                 ((UCHAR)-5)
#define IRP_MJ_RELEASE_FOR_CC_FLUSH                 ((UCHAR)-6)
#define IRP_MJ_NOTIFY_STREAM_FO_CREATION            ((UCHAR)-7)

#define IRP_MJ_FAST_IO_CHECK_IF_POSSIBLE            ((UCHAR)-13)
#define IRP_MJ_NETWORK_QUERY_OPEN                   ((UCHAR)-14)
#define IRP_MJ_MDL_READ                             ((UCHAR)-15)
#define IRP_MJ_MDL_READ_COMPLETE                    ((UCHAR)-16)
#define IRP_MJ_PREPARE_MDL_WRITE                    ((UCHAR)-17)
#define IRP_MJ_MDL_WRITE_COMPLETE                   ((UCHAR)-18)
#define IRP_MJ_VOLUME_MOUNT                         ((UCHAR)-19)
#define IRP_MJ_VOLUME_DISMOUNT                      ((UCHAR)-20)

#define IRP_MJ_TRANSACTION_NOTIFY                   ((UCHAR)-40)

typedef enum {
	TRANSACTION_NOTIFY_BEGIN                 = 0,
	TRANSACTION_NOTIFY_PREPREPARE_CODE       = 1,
	TRANSACTION_NOTIFY_PREPARE_CODE             ,
	TRANSACTION_NOTIFY_COMMIT_CODE              ,
	TRANSACTION_NOTIFY_ROLLBACK_CODE            ,
	TRANSACTION_NOTIFY_PREPREPARE_COMPLETE_CODE ,
	TRANSACTION_NOTIFY_PREPARE_COMPLETE_CODE    ,
	TRANSACTION_NOTIFY_COMMIT_COMPLETE_CODE     ,
	TRANSACTION_NOTIFY_ROLLBACK_COMPLETE_CODE   ,
	TRANSACTION_NOTIFY_RECOVER_CODE             ,
	TRANSACTION_NOTIFY_SINGLE_PHASE_COMMIT_CODE ,
	TRANSACTION_NOTIFY_DELEGATE_COMMIT_CODE     ,
	TRANSACTION_NOTIFY_RECOVER_QUERY_CODE       ,
	TRANSACTION_NOTIFY_ENLIST_PREPREPARE_CODE   ,
	TRANSACTION_NOTIFY_LAST_RECOVER_CODE        ,
	TRANSACTION_NOTIFY_INDOUBT_CODE             ,
	TRANSACTION_NOTIFY_PROPAGATE_PULL_CODE      ,
	TRANSACTION_NOTIFY_PROPAGATE_PUSH_CODE      ,
	TRANSACTION_NOTIFY_MARSHAL_CODE             ,
	TRANSACTION_NOTIFY_ENLIST_MASK_CODE         ,
	TRANSACTION_NOTIFY_COMMIT_FINALIZE_CODE = 31,
} TRANSACTION_NOTIFICATION_CODES;

#endif/*_FGFSEVTSPYWMF_H_BB7874E1_7C6E_4413_B694_5BC77AF0B407_INCLUDED*/