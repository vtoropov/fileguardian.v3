/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jun-2018 at 4:32:28p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver event spy interface implementation file.
*/
#include "StdAfx.h"
#include "fg.fs.evt.spy.iface.h"

using namespace shared::km::spy;

/////////////////////////////////////////////////////////////////////////////

PWCHAR CEvtSpyDriver_deprecated::Port(void) { static PWCHAR lpsz_port = _T("\\Fg_log_spy"); return lpsz_port; }