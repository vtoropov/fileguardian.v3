#ifndef _FGFSLOGSPYEVTHANDLER_H_331939B7_1B51_4036_8EAD_F54C809A3BFF_INCLUDED
#define _FGFSLOGSPYEVTHANDLER_H_331939B7_1B51_4036_8EAD_F54C809A3BFF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jun-2018 at 14:42:01p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardain minifilter driver generic event handler interface declaration file. 
*/

FLT_POSTOP_CALLBACK_STATUS FLTAPI
PostEventCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData         ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects      ,
	__in        PVOID                     _pCompletionCtx   ,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags
);

FLT_PREOP_CALLBACK_STATUS FLTAPI
PreEventCallback(
	__inout     PFLT_CALLBACK_DATA        _pFltData         ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects      ,
	__inout_opt PVOID*                    _ppCompletionCtx
);

#endif/*_FGFSLOGSPYEVTHANDLER_H_331939B7_1B51_4036_8EAD_F54C809A3BFF_INCLUDED*/