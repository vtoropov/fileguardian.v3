/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jun-2016 at 11:46:22a, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian system event log minifilter driver driver precompiled headers implementation file.
*/
#include "StdAfx.h"

namespace global
{
	TDriverData&   DriverData(void)
	{
		static TDriverData drv_data = {0};
		return drv_data;
	}
}

#define AUX_STRING_POOL_TAG  'auxs'

using namespace utils;
