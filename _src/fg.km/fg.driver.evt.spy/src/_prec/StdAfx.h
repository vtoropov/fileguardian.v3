#ifndef _STDAFX_H_4BE97360_F93F_ABCD_BBA9_1131455A19E1_INCLUDED
#define _STDAFX_H_4BE97360_F93F_ABCD_BBA9_1131455A19E1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Jun-2016 at 11:44:04, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian file system event log minifilter driver precompiled headers definition file.
*/

#ifndef WINVER                 // Specifies that the minimum required platform is Windows Vista.
#define WINVER         0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT           // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINNT   0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINDOWS         // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINDOWS 0x0600  // Change this to the appropriate value to target other platforms.
#endif

extern "C"
{
#pragma warning(push, 0)
	#include <fltKernel.h>
	#include <dontuse.h>
	#include <suppress.h>
	#include <Wdmsec.h>
	#include <windef.h>
	#include <ntimage.h>
	#include <stdarg.h>
	#define NTSTRSAFE_NO_CB_FUNCTIONS
	#include <ntstrsafe.h>
	#include <ntddstor.h>
	#include <mountdev.h>
	#include <ntddvol.h>
	#include <Aux_klib.h>
	#include <bcrypt.h>
	#include <FltUserStructures.h>
#pragma warning(pop)
}

#pragma prefast(disable:__WARNING_ENCODE_MEMBER_FUNCTION_POINTER, "Not valid for kernel mode drivers")
/////////////////////////////////////////////////////////////////////////////
//
// macro utilities
//

#define FlagOnAll( F, T )  \
    (FlagOn( F, T ) == T)

#define SetFlagInterlocked(_ptrFlags,_flagToSet) \
    ((VOID)InterlockedOr(((volatile LONG*)(_ptrFlags)),_flagToSet))

#define PAGE_CODE_EX(_return)                            \
		if (PASSIVE_LEVEL != KeGetCurrentIrql()) {       \
			return _return;                              \
		}
/////////////////////////////////////////////////////////////////////////////
//
// constants and structures
//

#define UNUSED(x) (void)(x)
#define LOG_DEBUG_PARSE_NAMES  0x00000001

typedef LIST_ENTRY             LIST_HEAD;
typedef NPAGED_LOOKASIDE_LIST  LIST_ALLOC;

#define LIST_FOR_EACH_SAFE(pCurr  , pNext, pHead) \
		for (pCurr = pHead->Flink , pNext = pCurr->Flink ; pCurr != pHead; \
		     pCurr = pNext        , pNext = pCurr->Flink)

typedef struct _DRIVER_DATA
{    
	PDRIVER_OBJECT   pDriverObject ;     // the object that identifies this driver
	PFLT_FILTER      pFilter       ;     // filter handle that results from a call to FltRegisterFilter
	PFLT_PORT        pServerPort   ;     // listens for incoming connection(s)
	PEPROCESS        pUserProcess  ;     // user(mode) process that connected to the server port
	PFLT_PORT        pClientPort   ;     // client port for a connection to user-mode
	ULONG            uDebugFlags   ;     // global debug flags;
}
TDriverData, *PTDriverData;

#define CPLUSPLUS

#ifdef CPLUSPLUS
namespace global
{
	TDriverData&     DriverData(void);
}
#else
	PTDriverData     DriverDataPtr(void);
#endif

/////////////////////////////////////////////////////////////////////////////
//
// helper(s)
//

#pragma comment (lib, "fg.driver.common_v15.lib")

#include "fg.driver.common.str.h"

namespace utils
{
	typedef shared::km::aux::CStringW CStringW;
}

#endif/*_STDAFX_H_4BE97360_F93F_ABCD_BBA9_1131455A19E1_INCLUDED*/