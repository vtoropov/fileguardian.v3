#ifndef _FGFSLOGSPYUMMHANDLER_H_707253BF_82F0_497a_82B4_B88FBA399853_INCLUDED
#define _FGFSLOGSPYUMMHANDLER_H_707253BF_82F0_497a_82B4_B88FBA399853_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jun-2018 at 4:40:41p, UTC+7, Phuket, Rawai, Frisday;
	This is File Guardian minifilter driver event spy user mode message handler interface declaration file.
*/

namespace shared { namespace km { namespace spy {

	class CEvtSpyUmHandler {
	public:
		static
		NTSTATUS
		Connect(
			__in PFLT_PORT pClientPort,
			__in_opt PVOID pServerPortCookie,
			__in_bcount_opt(SizeOfContext) PVOID pConnectionContext,
			__in ULONG nSizeOfContext,
			__deref_out_opt PVOID* ppConnectionCookie
		);
		static
		VOID
		Disconnect(
			__in_opt PVOID pConnectionCookie
		);
		static
		NTSTATUS
		Handle(
			__in PVOID       _pConnectionCookie,
			__in_bcount_opt( _nInputBufferSize ) PVOID _pInputBuffer,
			__in ULONG       _nInputBufferSize ,
			__out_bcount_part_opt(_nOutBufferSize,*_pReturnOutBufferSize) PVOID _pOutBuffer,
			__in ULONG       _nOutBufferSize   ,
			__out PULONG     _pReturnOutBufferSize
		);
	};
}}}
#endif/*_FGFSLOGSPYUMMHANDLER_H_707253BF_82F0_497a_82B4_B88FBA399853_INCLUDED*/