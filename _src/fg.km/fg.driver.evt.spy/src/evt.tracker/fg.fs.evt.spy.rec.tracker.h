#ifndef _FGFSLOGSPYEVTTRACKER_H_8A433DAD_535D_44F2_9C6C_D731FB3ACA13_INCLUDED
#define _FGFSLOGSPYEVTTRACKER_H_8A433DAD_535D_44F2_9C6C_D731FB3ACA13_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jun-2018 at 5:22:36p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver event spy event tracker interface declaration file.
*/
#include "fg.fs.evt.spy.iface.h"

namespace shared { namespace km { namespace spy {

	class CEvtTracker {
	public:
		static
			NTSTATUS
			FillUserBuffer (
				__inout PTFs_EvtBuffer _pUserBuffer
			);
	};
}}}

#endif/*_FGFSLOGSPYEVTTRACKER_H_8A433DAD_535D_44F2_9C6C_D731FB3ACA13_INCLUDED*/