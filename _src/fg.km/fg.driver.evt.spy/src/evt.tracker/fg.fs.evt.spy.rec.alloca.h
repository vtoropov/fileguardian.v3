#ifndef _FGFSEVTSPYRECALLOCA_H_748A0C74_1B5A_4DB2_AF24_E2A95C5D3FD4_INCLUDED
#define _FGFSEVTSPYRECALLOCA_H_748A0C74_1B5A_4DB2_AF24_E2A95C5D3FD4_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jun-2018 at 7:07:08p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver event spy event allocator interface declaration file.
*/
#include "fg.fs.evt.spy.iface.h"

namespace shared { namespace km { namespace spy {

	typedef struct _t_fs_evt_entry
	{
		LIST_ENTRY      lst_ndx;                // index of record entry in the list;
		TFs_EvtData     lst_data;
	}
	TFs_EvtEntry, *PTFs_EvtEntry;

	typedef struct _t_fs_evt_storage
	{
		LIST_HEAD       lst_head;               // head of the list of record entries;
		KSPIN_LOCK      lst_lock;               // lock object of record entry lists;
		LIST_ALLOC      lst_alloca;             // lookaside list used for allocating list items;
		LONG            n_max_recs;             // maximum records allowed to keep in the storage;
		__volatile LONG n_allocated;            // currently aloccated records to track buffer overflow;
		ULONG           n_qry_name;             
	}
	TFs_EvtStorage, *PTFs_EvtStorage;

	TFs_EvtStorage&    EvtStorage(void);

#define DEFAULT_EVT_STORAGE_CAP    1000
#define DEFAULT_EVT_NAME_METHOD    FLT_FILE_NAME_QUERY_ALWAYS_ALLOW_CACHE_LOOKUP

	class CEvtAlloca {
	public:
		//
		// allocates a record entry
		//
		static
			PTFs_EvtEntry
			AllocateEntry(
				VOID
			);
		//
		// clones one entry to other;
		//
		static
			BOOLEAN
			CloneData(
				__in CONST TFs_EvtData& _tSource,
				__inout    TFs_EvtData& _tTarget
			);
		//
		// creates record entry from record data provider;
		//
		static
			PTFs_EvtEntry
			CreateEntryFromData(
				__in CONST TFs_EvtData& _tData
			);
		//
		// frees a record entry
		//
		static
			VOID
			FreeEntry(
				__in PTFs_EvtEntry _pEntry
			);
	};

	class CEvtMan{
	public:
		//
		// appends an entry to the record list; if the list reaches the maximum capacity,
		// the head item(s) will be discarded;
		//
		static
			BOOLEAN
			AppendEntry(
				__in PTFs_EvtEntry _pEntry
			);
		//
		// creates/initializes global record entry list and sync access object(s);
		// this routine must be called from driver entry;
		//
		static
			VOID
			CreateStorage(
				VOID
			);
		//
		// destroys global record entry list;
		// this procedure must be invoked in driver unload routine;
		//
		static
			VOID
			DestroyStorage(
				VOID
			);
		//
		// gets the first available entry; if record list is empty or error occurs, NULL is returned;
		// a caller is responsible for destroying the entry object that is received through this method;
		//
		static
			PTFs_EvtEntry
			GetFirstEntry(
				VOID
			);
		//
		// clears record entry list
		//
		static
			VOID
			RemoveAllEntries(
				__in LIST_HEAD&   _list,         // list reference to deal with
				__in KSPIN_LOCK&  _lock          // list access sync object reference
			);
		//
		// sets affected file name/path from string provided;
		//
		static
			BOOLEAN
			SetAffected(
				__in PTFs_EvtEntry   _pEntry,
				__in PUNICODE_STRING _pPathOrName
			);
	};
}}}

#endif/*_FGFSEVTSPYRECALLOCA_H_748A0C74_1B5A_4DB2_AF24_E2A95C5D3FD4_INCLUDED*/