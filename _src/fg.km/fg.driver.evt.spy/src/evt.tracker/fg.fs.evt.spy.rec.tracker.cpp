/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jun-2018 at 8:56:33p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver event spy event tracker interface implementation file.
*/
#include "StdAfx.h"
#include "fg.fs.evt.spy.rec.tracker.h"
#include "fg.fs.evt.spy.rec.alloca.h"

using namespace shared::km::spy;

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CEvtTracker::FillUserBuffer (
	__inout PTFs_EvtBuffer _pUserBuffer
) {
	static
	TFs_EvtStorage& stg_   = shared::km::spy::EvtStorage();
	NTSTATUS     status_   = STATUS_NO_MORE_ENTRIES;
	PTFs_EvtEntry pEntry   = NULL;
	ULONG         lCount   = 0; 

	if (NULL == _pUserBuffer)
		return ( status_ = STATUS_INVALID_PARAMETER );
	else {
		lCount = _pUserBuffer->uRecCount;
		_pUserBuffer->uRecCount = 0;      // this counter is erased and will contain the number of processed events;
	}

	for (ULONG l_ = 0; l_ < lCount; l_++){

		pEntry = CEvtMan::GetFirstEntry();
		if (NULL == pEntry)
			return status_;

		_pUserBuffer->uRecCount += 1;
		CEvtAlloca::CloneData(
			pEntry->lst_data, _pUserBuffer->pRecords[l_]
		);

		CEvtAlloca::FreeEntry(pEntry);
	}
	if (_pUserBuffer->uRecCount > 0) // some entries have been processed;
		status_ = STATUS_SUCCESS;

	return status_;
}