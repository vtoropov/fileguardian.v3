/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jun-2018 at 7:27:36p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver event spy event allocator interface implementation file.
*/
#include "StdAfx.h"
#include "fg.fs.evt.spy.rec.alloca.h"

using namespace shared::km::spy;

static const ULONG FG_SPY_ENTRY_TAG_NAME = 'salo';

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace km { namespace spy {

	TFs_EvtStorage&   EvtStorage(void)
	{
		static TFs_EvtStorage stg;
		return stg;
	}

}}}

/////////////////////////////////////////////////////////////////////////////

PTFs_EvtEntry
CEvtAlloca::AllocateEntry(
	VOID
)
{
	static TFs_EvtStorage& stg_ = shared::km::spy::EvtStorage();

	PTFs_EvtEntry pEntry = (PTFs_EvtEntry)ExAllocateFromNPagedLookasideList(&stg_.lst_alloca);
	if (NULL == pEntry) {
#if DBG
		DbgPrint(
			"__fg_sp: [ERROR] CEvtAlloca::AllocateEntry(): event entry allocation failed;\n"
		);
#endif
	}
	else {
		RtlZeroMemory( pEntry, sizeof( TFs_EvtEntry ) );
	}

	return pEntry;
}

BOOLEAN
CEvtAlloca::CloneData(
	__in CONST TFs_EvtData& _tSource,
	__inout    TFs_EvtData& _tTarget
)
{
	_tTarget.uFuncMajor     = _tSource.uFuncMajor;
	_tTarget.uFuncMinor     = _tSource.uFuncMinor;
	_tTarget.luTimestamp    = _tSource.luTimestamp;

	RtlCopyMemory(&_tTarget.wFileAffected[0], &_tSource.wFileAffected[0], _COMP_PATH);

	return TRUE;
}

PTFs_EvtEntry
CEvtAlloca::CreateEntryFromData(
	__in CONST TFs_EvtData& _tData
)
{
	PTFs_EvtEntry pEntry = CEvtAlloca::AllocateEntry();
	if (NULL != pEntry){
		CEvtAlloca::CloneData(_tData, pEntry->lst_data);
	}

	return pEntry;
}

VOID
CEvtAlloca::FreeEntry(
	__in PTFs_EvtEntry _pEntry
)
{
	static TFs_EvtStorage& stg_ = shared::km::spy::EvtStorage();

	if (NULL == _pEntry)
		return;
	ExFreeToNPagedLookasideList(&stg_.lst_alloca, _pEntry);
}

/////////////////////////////////////////////////////////////////////////////

BOOLEAN
CEvtMan::AppendEntry(
	__in PTFs_EvtEntry _pEntry
)
{
	BOOLEAN b_result       = FALSE;
	KIRQL   prevIrqLevel   = 0;
	PLIST_ENTRY    pIter   = NULL;
	PTFs_EvtEntry pEntry   = NULL;

	static TFs_EvtStorage& stg_ = shared::km::spy::EvtStorage();

	if (NULL == _pEntry)
		return b_result;

	//
	// checks for list overflow; drops item(s) if necessary;
	// we need a room at least for one record entry;
	//

	while ((stg_.n_max_recs - stg_.n_allocated) < 1){
		InterlockedDecrement( &stg_.n_allocated);

		KeAcquireSpinLock(&stg_.lst_lock, &prevIrqLevel);

		pIter =  RemoveHeadList (&stg_.lst_head);

		KeReleaseSpinLock(&stg_.lst_lock,  prevIrqLevel);

		pEntry = CONTAINING_RECORD(pIter, TFs_EvtEntry, lst_ndx);
		CEvtAlloca::FreeEntry(pEntry);
	}

	//
	// adds new entry to the list
	//
	InterlockedIncrement( &stg_.n_allocated);
	KeAcquireSpinLock(&stg_.lst_lock, &prevIrqLevel);

	InsertTailList (&stg_.lst_head, &_pEntry->lst_ndx);
	KeReleaseSpinLock(&stg_.lst_lock,  prevIrqLevel);

	b_result = TRUE;

	return b_result;
}

VOID
CEvtMan::CreateStorage(
	VOID
)
{
	TFs_EvtStorage& stg_ = shared::km::spy::EvtStorage();

	InitializeListHead  ( &stg_.lst_head );
	KeInitializeSpinLock( &stg_.lst_lock );

	ExInitializeNPagedLookasideList(
		&stg_.lst_alloca,
		NULL,
		NULL,
		0,
		sizeof(TFs_EvtEntry),
		FG_SPY_ENTRY_TAG_NAME   ,
		0
	);
	stg_.n_max_recs   = DEFAULT_EVT_STORAGE_CAP;
	stg_.n_allocated  = 0;
	stg_.n_qry_name   = DEFAULT_EVT_NAME_METHOD;
}

VOID
CEvtMan::DestroyStorage(
	VOID
)
{
	TFs_EvtStorage& stg_ = shared::km::spy::EvtStorage();

	CEvtMan::RemoveAllEntries(stg_.lst_head, stg_.lst_lock);

	ExDeleteNPagedLookasideList( &stg_.lst_alloca );
}

PTFs_EvtEntry
CEvtMan::GetFirstEntry(
	VOID
)
{
	static
	TFs_EvtStorage& stg_   = shared::km::spy::EvtStorage();
	PTFs_EvtEntry pEntry   = NULL;
	PLIST_ENTRY   pIter_   = NULL;
	KIRQL   prevIrqLevel   = 0;

	if (IsListEmpty(&stg_.lst_head))
		return pEntry;

	InterlockedDecrement( &stg_.n_allocated);
	KeAcquireSpinLock(&stg_.lst_lock, &prevIrqLevel);

	pIter_ = RemoveTailList(&stg_.lst_head);
	KeReleaseSpinLock(&stg_.lst_lock,  prevIrqLevel);

	if (NULL == pIter_)
		return pEntry;

	pEntry = CONTAINING_RECORD(pIter_, TFs_EvtEntry, lst_ndx);
	return pEntry;
}

VOID
CEvtMan::RemoveAllEntries(
	__in LIST_HEAD&   _list,         // list reference to deal with
	__in KSPIN_LOCK&  _lock          // list access sync object reference
)
{
	PLIST_ENTRY    pHead    = &_list;
	PLIST_ENTRY    pIter    = pHead->Flink;
	PTFs_EvtEntry  pEntry   = NULL;
	KIRQL    prevIrqLevel   = 0;

	KeAcquireSpinLock(&_lock, & prevIrqLevel);

	while(!IsListEmpty(pHead))
	{
		pIter  = RemoveHeadList( pHead );
		KeReleaseSpinLock( &_lock, prevIrqLevel );

		pEntry = CONTAINING_RECORD(pIter, TFs_EvtEntry, lst_ndx);
		CEvtAlloca::FreeEntry(pEntry);
		KeAcquireSpinLock(&_lock, & prevIrqLevel);
	}

	KeReleaseSpinLock( &_lock, prevIrqLevel );
}

BOOLEAN
CEvtMan::SetAffected(
	__in PTFs_EvtEntry   _pEntry,
	__in PUNICODE_STRING _pPathOrName
){
	if (NULL == _pEntry || NULL == _pPathOrName)
		return FALSE;

	RtlCopyMemory(
		&_pEntry->lst_data.wFileAffected[0], _pPathOrName->Buffer, _pPathOrName->Length
	);
	return TRUE;
}