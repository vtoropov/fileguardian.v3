#ifndef _FGFSLOGSPYMODULE_H_DD0894F4_8E36_4110_9EA7_2A43DBD66545_INCLUDED
#define _FGFSLOGSPYMODULE_H_DD0894F4_8E36_4110_9EA7_2A43DBD66545_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jun-2018 at 12:21:48p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian log spy mini-filter driver module declaration file.
*/
#include "fg.fs.evt.spy.iface.h"

EXTERN_C DRIVER_INITIALIZE DriverEntry;

EXTERN_C static NTSTATUS FLTAPI DriverUnload(
	__in        ULONG                     _uFlags
);

#pragma alloc_text(PAGE, DriverEntry)
#pragma alloc_text(PAGE, DriverUnload)

#endif/*_FGFSLOGSPYMODULE_H_DD0894F4_8E36_4110_9EA7_2A43DBD66545_INCLUDED*/