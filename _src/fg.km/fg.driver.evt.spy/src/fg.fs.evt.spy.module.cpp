/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jun-2017 at 12:25:06p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian log spy mini-filter driver module implementation file.
*/
#include "StdAfx.h"

#define FLTFL_OPERATION_REGISTRATION_NONE_OPTS  0
#define IRP_MJ_FILE_CTRL IRP_MJ_FILE_SYSTEM_CONTROL

#include "fg.fs.evt.spy.module.h"
#include "fg.fs.evt.spy.rec.alloca.h"
#include "fg.fs.evt.spy.gen.handler.h"
#include "fg.fs.evt.spy.umm.handler.h"

using shared::km::spy::CEvtSpyUmHandler;
using shared::km::spy::CEvtMan;

/////////////////////////////////////////////////////////////////////////////

EXTERN_C NTSTATUS DriverEntry(__in PDRIVER_OBJECT DriverObject ,
                              __in PUNICODE_STRING RegistryPath)
{
	UNUSED(DriverObject);
	UNUSED(RegistryPath);

#if DBG
	DbgPrint(
		"__fg_sp: [INFO] LogSpy::DriverEntry()::entering to driver entry...;\n"
			);
#endif

	const FLT_CONTEXT_REGISTRATION evtCtxRegNone[] = {
		{ FLT_CONTEXT_END }
	};

	const FLT_OPERATION_REGISTRATION evtCallbacks[] = {
	{ IRP_MJ_CREATE                               , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_CREATE_NAMED_PIPE                    , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_CLOSE                                , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_READ                                 , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_WRITE                                , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_QUERY_INFORMATION                    , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_SET_INFORMATION                      , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_QUERY_EA                             , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_SET_EA                               , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_FLUSH_BUFFERS                        , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_QUERY_VOLUME_INFORMATION             , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_SET_VOLUME_INFORMATION               , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_DIRECTORY_CONTROL                    , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_FILE_SYSTEM_CONTROL                  , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_DEVICE_CONTROL                       , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_INTERNAL_DEVICE_CONTROL              , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_SHUTDOWN                             , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, NULL             },
	{ IRP_MJ_LOCK_CONTROL                         , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_CLEANUP                              , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_CREATE_MAILSLOT                      , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_QUERY_SECURITY                       , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_SET_SECURITY                         , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_QUERY_QUOTA                          , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_SET_QUOTA                            , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_PNP                                  , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_ACQUIRE_FOR_SECTION_SYNCHRONIZATION  , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_RELEASE_FOR_SECTION_SYNCHRONIZATION  , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_ACQUIRE_FOR_MOD_WRITE                , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_RELEASE_FOR_MOD_WRITE                , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_ACQUIRE_FOR_CC_FLUSH                 , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_RELEASE_FOR_CC_FLUSH                 , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_FAST_IO_CHECK_IF_POSSIBLE            , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_NETWORK_QUERY_OPEN                   , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_MDL_READ                             , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_MDL_READ_COMPLETE                    , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_PREPARE_MDL_WRITE                    , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_MDL_WRITE_COMPLETE                   , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_VOLUME_MOUNT                         , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_VOLUME_DISMOUNT                      , FLTFL_OPERATION_REGISTRATION_NONE_OPTS, PreEventCallback, PostEventCallback},
	{ IRP_MJ_OPERATION_END   }
	};

	const FLT_REGISTRATION registry_ = {
		sizeof(registry_)        , // size of structure
		FLT_REGISTRATION_VERSION , // revision level of the structure
		0                        , // bitmask of minifilter registration flags
		evtCtxRegNone            , // context registration info
		evtCallbacks             , // operation callbacks
		DriverUnload             , // minifilter unload routine
		NULL                     , // instance setup callback routine
		NULL                     , // instance query teardown callback routine
		NULL                     , // instance start teardown callback routine
		NULL                     ,
		NULL                     , // generate file name callback routine
		NULL                     , // normalize name component callback routine
		NULL                     , // normalize context cleanup callback routine
#ifdef FLT_MGR_LONGHORN
		NULL                     ,
		NULL                     , // normalize name component extended callback
#endif
	};

#if DBG
#ifdef FLT_MGR_LONGHORN
	DbgPrint(
		"__fg_fs: [INFO] LogSpy::DriverEntry()::TxF support is enabled;\n"
		);
#else
	DbgPrint(
		"__fg_fs: [INFO] LogSpy::DriverEntry()::TxF support is disabled;\n"
		);
#endif
#endif

	PSECURITY_DESCRIPTOR sd           = NULL;
	UNICODE_STRING       drv_port     = {0};
	OBJECT_ATTRIBUTES    obj_a        = {0};
	const LONG           nMaxConnects =  1 ; // accept one connection only;
	NTSTATUS             nt_status    = STATUS_SUCCESS;

	TDriverData& drv_data  = global::DriverData();
	drv_data.pDriverObject = DriverObject;

	__try {
		//
		// initializes global data structures;
		//
		::RtlInitUnicodeString(&drv_port, _EVT_SPY_PORT_RAW_NAME);
		CEvtMan::CreateStorage();
#if DBG
		DbgPrint( "__fg_sp: [INFO] LogSpy::DriverEntry()::registering log:status=%X;\n", nt_status );
		DbgPrint( "__fg_sp: [INFO] LogSpy::DriverEntry()::registering filter...;\n" );
#endif
		nt_status = FltRegisterFilter(
				DriverObject, &registry_, &drv_data.pFilter
			);

		if (!NT_SUCCESS(nt_status)){
#if DBG
			DbgPrint(
				"__fg_sp: [ERR] LogSpy::DriverEntry()::registering filter failed with code=%X;\n", nt_status
				);
#endif
			__leave;
		}
		//
		// secures the port, so administrators or local system (services) can access it;
		//
#if DBG
		DbgPrint(
			"__fg_sp: [INFO] LogSpy::DriverEntry()::creating security descriptor...;\n"
			);
#endif
		nt_status = FltBuildDefaultSecurityDescriptor(&sd, FLT_PORT_ALL_ACCESS);

		if (!NT_SUCCESS(nt_status)){
#if DBG
			DbgPrint(
				"__fg_sp: [ERR] LogSpy::DriverEntry()::creating security descriptor failed with code=%X;\n", nt_status
				);
#endif
			__leave;
		}

		InitializeObjectAttributes(
			&obj_a,
			&drv_port,
			OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
			NULL,
			sd
		);
#if DBG
		DbgPrint(
			"__fg_sp: [INFO] LogSpy::DriverEntry()::creating communication port...;\n"
		);
#endif
		nt_status = FltCreateCommunicationPort(
		            drv_data.pFilter, &drv_data.pServerPort, &obj_a, NULL,
		            CEvtSpyUmHandler::Connect   ,
		            CEvtSpyUmHandler::Disconnect,
		            CEvtSpyUmHandler::Handle    , nMaxConnects
		         );

		FltFreeSecurityDescriptor(sd); sd = NULL;

		if (!NT_SUCCESS(nt_status)){
#if DBG
			DbgPrint(
				"__fg_sp: [ERR] LogSpy::DriverEntry()::creating communication port failed with code=%X;\n", nt_status
				);
#endif
			__leave;
		}

#if DBG
		DbgPrint(
			"__fg_sp: [INFO] LogSpy::DriverEntry()::starting the filtering...;\n"
			);
#endif
		nt_status = FltStartFiltering(drv_data.pFilter);

		if (!NT_SUCCESS(nt_status)){
#if DBG
			DbgPrint(
				"__fg_sp: [ERR] LogSpy::DriverEntry()::start filtering failed with code=%X;\n", nt_status
				);
#endif
		} else {
#if DBG
			DbgPrint(
				"__fg_sp: [INFO] LogSpy::DriverEntry()::waiting for incomming connection(s)...;\n"
				);
#endif
		}
	}
	__finally
	{
		if (!NT_SUCCESS(nt_status))
		{
			if (NULL != drv_data.pServerPort){
				FltCloseCommunicationPort(drv_data.pServerPort);
				drv_data.pServerPort = NULL;
			}
			if (NULL != drv_data.pFilter) {
				FltUnregisterFilter(drv_data.pFilter);
				drv_data.pFilter = NULL;
			}
			CEvtMan::DestroyStorage();
		}
	}

#if DBG
	DbgPrint(
		"__fg_sp: [INFO] LogSpy::DriverEntry()::exiting driver entry;\n"
			);
#endif

	return nt_status;
}

EXTERN_C static NTSTATUS FLTAPI DriverUnload(
	__in        FLT_FILTER_UNLOAD_FLAGS   _uFlags
)
{
	UNUSED(_uFlags);

	PAGED_CODE();

#if DBG
	DbgPrint(
		"__fg_sp: [INFO] LogSpy::DriverUnload()::entering log spy unload...;\n"
		);
#endif

	TDriverData& drv_data  = global::DriverData();

	if (NULL != drv_data.pClientPort){

#if DBG
		DbgPrint(
			"__fg_sp: [INFO] LogSpy::DriverUnload()::disconnecting client(s)...;\n"
			);
#endif

		FltCloseClientPort(
				 drv_data.pFilter, &drv_data.pClientPort
			);
		drv_data.pClientPort = NULL;
	}

#if DBG
	DbgPrint(
		"__fg_sp: [INFO] LogSpy::DriverUnload()::closing communication port...;\n"
		);
#endif

	FltCloseCommunicationPort(drv_data.pServerPort);
	drv_data.pServerPort = NULL;

#if DBG
	DbgPrint(
		"__fg_sp: [INFO] LogSpy::DriverUnload()::unregistering filter...;\n"
		);
#endif

	FltUnregisterFilter(drv_data.pFilter); drv_data.pFilter = NULL;

	CEvtMan::DestroyStorage();
#if DBG
	DbgPrint(
		"__fg_sp: [INFO] LogSpy::DriverUnload()::exiting filter unload...;\n"
		);
#endif
	if (drv_data.pDriverObject) {
		if (drv_data.pDriverObject->DeviceObject) {
			IoDeleteDevice( drv_data.pDriverObject->DeviceObject );
#if DBG
	DbgPrint(
		"__fg_sp: [INFO] LogSpy::DriverUnload()::device object has been deleted...;\n"
		);
#endif
		}
	}
	NTSTATUS nt_status = STATUS_SUCCESS;
	return   nt_status;
}