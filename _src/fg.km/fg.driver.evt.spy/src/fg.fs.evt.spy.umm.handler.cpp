/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jun-2018 at 4:58:57p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver event spy user mode message handler interface implementation file.
*/
#include "StdAfx.h"
#include "fg.fs.evt.spy.umm.handler.h"
#include "fg.fs.evt.spy.iface.h"
#include "fg.fs.evt.spy.rec.tracker.h"

using namespace shared::km::spy;
/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CEvtSpyUmHandler::Connect(
	__in PFLT_PORT pClientPort,
	__in_opt PVOID pServerPortCookie,
	__in_bcount_opt(SizeOfContext) PVOID pConnectionContext,
	__in ULONG nSizeOfContext,
	__deref_out_opt PVOID* ppConnectionCookie
	)
{
	UNUSED(pClientPort);
	UNUSED(pServerPortCookie);
	UNUSED(pConnectionContext);
	UNUSED(nSizeOfContext);
	UNUSED(ppConnectionCookie);

	PAGED_CODE();

#if DBG
	DbgPrint(
		"__fg_sp: [INFO] CEvtSpyUmHandler::Connect()::accepting user mode connection...;\n"
		);
#endif

	global::DriverData().pClientPort  = pClientPort;
	global::DriverData().pUserProcess = PsGetCurrentProcess();

#if DBG
	DbgPrint(
		"__fg_sp: [INFO] CEvtSpyUmHandler::Connect()::the client connection is accepted;\n"
		);
#endif

	return STATUS_SUCCESS;
}

VOID
CEvtSpyUmHandler::Disconnect(
	__in_opt PVOID pConnectionCookie
	)
{
	UNUSED(pConnectionCookie);
	TDriverData& drv_data = global::DriverData();

	PAGED_CODE();

#if DBG
	DbgPrint(
		"__fg_sp: [INFO] CEvtSpyUmHandler::Disconnect()::disconnecting from port...;\n"
		);
#endif

	::FltCloseClientPort(
			drv_data.pFilter, &drv_data.pClientPort
		);
	drv_data.pClientPort  = NULL;
	drv_data.pUserProcess = NULL;

#if DBG
	DbgPrint(
		"__fg_sp: [INFO] CEvtSpyUmHandler::Disconnect()::client is disconnected from filter port;\n"
		);
#endif
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace km { namespace spy { namespace details {

	using namespace shared::km::spy;

	class CDriverCommand {
	private:
		mutable
		NTSTATUS        m_status;
		PVOID           m_pInput;
		ULONG           m_in_sz;
		PVOID           m_pOutput;
		ULONG           m_out_sz;
	public:
		CDriverCommand(const PVOID pInput, const ULONG _in_sz, const PVOID pOut, const ULONG _out_sz) :
			m_pInput(pInput), m_in_sz(_in_sz), m_pOutput(pOut), m_out_sz(_out_sz), m_status(STATUS_SUCCESS)
		{}
	public:
		BOOLEAN     IsValid(VOID)CONST {
			return ((m_pInput != NULL) &&
			        (m_in_sz >= (FIELD_OFFSET(TFs_Command, eCmdType) + sizeof(eFsCommandType::eNone))));
		}

		NTSTATUS    LastResult(VOID)CONST { return  m_status; }

		eFsCommandType::_e
		            Type(VOID)CONST {

			eFsCommandType::_e eType = eFsCommandType::eNone;
			if (!this->IsValid()) {
				m_status = STATUS_INVALID_PARAMETER;
				return eType;
			}

			__try {

				eType = ((PTFs_Command)m_pInput)->eCmdType;

			} __except(EXCEPTION_EXECUTE_HANDLER) {
				m_status = GetExceptionCode();
			}

			return eType;
		}
	};

}}}}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CEvtSpyUmHandler::Handle(
	__in PVOID       _pConnectionCookie,
	__in_bcount_opt( _nInputBufferSize ) PVOID _pInputBuffer,
	__in ULONG       _nInputBufferSize ,
	__out_bcount_part_opt(_nOutBufferSize,*_pReturnOutBufferSize) PVOID _pOutBuffer,
	__in ULONG       _nOutBufferSize   ,
	__out PULONG     _pReturnOutBufferSize
	)
{
	UNUSED(_pReturnOutBufferSize);

	eFsCommandType::_e
	             type_  = eFsCommandType::eNone;
	NTSTATUS  nt_status = STATUS_SUCCESS;

	PAGED_CODE();

	UNUSED(_pConnectionCookie);

	details::CDriverCommand command_(_pInputBuffer, _nInputBufferSize, _pOutBuffer, _nOutBufferSize);

	if (!command_.IsValid())
		return command_.LastResult();
	
	type_ = command_.Type();

	switch (type_)
	{
	case eFsCommandType::eGetEvents:
		{
			//
			// returns as many log records as can fit into the OutputBuffer
			//
			if ((_pOutBuffer == NULL) || (_nOutBufferSize == 0))
			{
				nt_status = STATUS_INVALID_PARAMETER;
				break;
			}
			//
			// checks the given buffer is POINTER aligned;
			//
#if defined(_WIN64)
			if (IoIs32bitProcess( NULL )){
				//
				//  validates alignment for the 32bit process on a 64bit system;
				//
				if (!IS_ALIGNED(_pOutBuffer, sizeof(ULONG)))
				{
					nt_status = STATUS_DATATYPE_MISALIGNMENT;
					break;
				}
			} else {
#endif
				if (!IS_ALIGNED(_pOutBuffer, sizeof(PVOID)))
				{
					nt_status = STATUS_DATATYPE_MISALIGNMENT;
					break;
				}
#if defined(_WIN64)
			}
#endif
			//
			//  gets the log record;
			//
			nt_status = CEvtTracker::FillUserBuffer(
							(PTFs_EvtBuffer)_pOutBuffer
						);
			if (nt_status == STATUS_NO_MORE_ENTRIES)
				nt_status =  STATUS_SUCCESS;
		} break;
	default:
		nt_status = STATUS_INVALID_PARAMETER;
	}

	return nt_status;
}