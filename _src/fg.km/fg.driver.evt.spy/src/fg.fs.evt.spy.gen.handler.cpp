/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jun-2018 at 14:50:57p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardain minifilter driver generic event handler interface implementation file. 
*/
#include "StdAfx.h"
#include "fg.fs.evt.spy.gen.handler.h"
#include "fg.fs.evt.spy.iface.h"
#include "fg.fs.evt.spy.rec.alloca.h"
#include "fg.driver.common.str.h"
#include "fg.driver.proc.object.h"

using namespace shared::km::aux;
using namespace shared::km::spy;
using namespace shared::km::proc_sys;

/////////////////////////////////////////////////////////////////////////////

FLT_POSTOP_CALLBACK_STATUS FLTAPI
PostEventCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData         ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects      ,
	__in        PVOID                     _pCompletionCtx   ,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags
) {
	FLT_POSTOP_CALLBACK_STATUS flt_return = FLT_POSTOP_FINISHED_PROCESSING;

	UNUSED(_pFltData);
	UNUSED(_pFltObjects);
	UNUSED(_pCompletionCtx);
	UNUSED(_uFlags);

	PAGED_CODE();

	return flt_return;
}

FLT_PREOP_CALLBACK_STATUS FLTAPI
PreEventCallback(
	__inout     PFLT_CALLBACK_DATA        _pFltData         ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects      ,
	__inout_opt PVOID*                    _ppCompletionCtx
) {
	FLT_PREOP_CALLBACK_STATUS
	           ft_status = FLT_PREOP_SUCCESS_NO_CALLBACK;
	NTSTATUS   nt_status = STATUS_SUCCESS;
	PTFs_EvtEntry pEntry = NULL;
	BOOLEAN   bPathAvail = FALSE;
	PFLT_FILE_NAME_INFORMATION
	       pFileNameInfo = NULL;
	LARGE_INTEGER liSysTime = {0};
	LARGE_INTEGER liLocTime = {0};
	HANDLE    hProcessId = NULL;

	UNUSED(_pFltData);
	UNUSED(_pFltObjects);
	UNUSED(_ppCompletionCtx);
	UNUSED(hProcessId);

	PAGED_CODE();

	if(FLT_IS_FS_FILTER_OPERATION(_pFltData)) {
		return ft_status;
	}

	pEntry = CEvtAlloca::AllocateEntry();
	if (NULL == pEntry)
		return (ft_status);

	if (NULL != _pFltObjects->FileObject && NULL != _pFltData) {

		nt_status = ::FltGetFileNameInformation(
				_pFltData, FLT_FILE_NAME_NORMALIZED | FLT_FILE_NAME_QUERY_ALWAYS_ALLOW_CACHE_LOOKUP, &pFileNameInfo
		);

		if (TRUE == NT_SUCCESS(nt_status)) {

			if (0 < pFileNameInfo->Name.Length) {
				nt_status  = CEvtMan::SetAffected(pEntry, &pFileNameInfo->Name);
				bPathAvail = NT_SUCCESS(nt_status);
			}
		}
	}
	else {

		nt_status = ::FltGetFileNameInformation(
			_pFltData, FLT_FILE_NAME_OPENED | FLT_FILE_NAME_QUERY_ALWAYS_ALLOW_CACHE_LOOKUP, &pFileNameInfo
		);

		if (TRUE == NT_SUCCESS(nt_status)) {

			if (0 < pFileNameInfo->Name.Length) {
				nt_status  = CEvtMan::SetAffected(pEntry, &pFileNameInfo->Name);
				bPathAvail = NT_SUCCESS(nt_status);
			}
		}
	}

	if (NULL != pFileNameInfo) {
		::FltReleaseFileNameInformation(pFileNameInfo); pFileNameInfo = NULL;
	}

	if (FALSE == bPathAvail) {
		// does not copy anything as affected file for this time;
	}

	pEntry->lst_data.uFuncMajor = _pFltData->Iopb->MajorFunction;
	pEntry->lst_data.uFuncMinor = _pFltData->Iopb->MinorFunction;

	KeQuerySystemTime(&liSysTime);
	::ExSystemTimeToLocalTime(
			&liSysTime, &liLocTime
		);

	pEntry->lst_data.luTimestamp = liLocTime;
	if (FALSE == CEvtMan::AppendEntry(pEntry)) {
		CEvtAlloca::FreeEntry(pEntry); pEntry = NULL;
	}

	//
	// TODO: this code leads to BSD; it requires more attention for getting work perfectly;
	//
	nt_status = CProcess::Id(_pFltData, hProcessId);
	pEntry->lst_data.luProcessId = FltGetRequestorProcessId(_pFltData);
#if (0)
	if (NT_SUCCESS(nt_status)) {

		UNICODE_STRING proc_name_ = {0};

		BIND_UNICODE_STRING(proc_name_, pEntry->lst_data.wProcApplied);

		nt_status = CProcess::ImageName(hProcessId, &proc_name_);
	}
#endif
	if (_pFltData->Iopb->MajorFunction == IRP_MJ_SHUTDOWN) ft_status = FLT_PREOP_SUCCESS_NO_CALLBACK;

	return ft_status;
}