/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jun-2018 at 6:27:15p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian driver common library generic process interface implementation file.
*/
#include "StdAfx.h"
#include "fg.driver.proc.object.h"

using namespace shared::km::proc_sys;

/////////////////////////////////////////////////////////////////////////////

typedef NTSTATUS (*QUERY_INFO_PROCESS) (
	__in HANDLE           ProcessHandle,
	__in PROCESSINFOCLASS ProcessInformationClass,
	__out_bcount(ProcessInformationLength) PVOID ProcessInformation,
	__in ULONG            ProcessInformationLength,
	__out_opt PULONG      ReturnLength
	);

QUERY_INFO_PROCESS ZwQueryInformationProcess;

/////////////////////////////////////////////////////////////////////////////

NTSTATUS   CProcess::Id(PFLT_CALLBACK_DATA  _p_data, HANDLE& _proc_id) {

	NTSTATUS nt_status = STATUS_SUCCESS;
	if (NULL == _p_data)
		return (nt_status = STATUS_INVALID_PARAMETER);

	if (NULL == _p_data->Thread) {
		_proc_id = PsGetCurrentProcessId();
	}
	else {
		PEPROCESS pProcess = IoThreadToProcess( _p_data->Thread );
		_proc_id = PsGetProcessId(pProcess);
	}
#if DBG
	DbgPrint( "__fg_fs: [INFO] CProcess::Id()=%x;\n", _proc_id );
#endif
	return   nt_status;
}

ULONG      CProcess::IdAsLong(PFLT_CALLBACK_DATA  _p_data) {

	HANDLE handle_ = NULL;
	NTSTATUS nt_status = CProcess::Id(_p_data, handle_);
	ULONG  pid_ = 0;

	if (NT_SUCCESS(nt_status))
		pid_ = HandleToUlong(handle_);
#if DBG
	DbgPrint( "__fg_fs: [INFO] CProcess::IdAsLong()=%d;\n", pid_ );
#endif

	return pid_;
}

NTSTATUS   CProcess::ImageName(HANDLE _proc_id, PUNICODE_STRING _p_name) {

	_proc_id; _p_name;

	NTSTATUS nt_status  = STATUS_SUCCESS;
	PEPROCESS p_proc_   = NULL;
	HANDLE    h_proc_   = NULL;
	ULONG     n_len_req = 0;

	PAGED_CODE();

	if (NULL == _proc_id)
		return (nt_status = STATUS_INVALID_PARAMETER_1);
	if (NULL == _p_name)
		return (nt_status = STATUS_INVALID_PARAMETER_2);

	nt_status = PsLookupProcessByProcessId(_proc_id, &p_proc_);

	if (FALSE == NT_SUCCESS(nt_status)) {
		return nt_status;
	}

	nt_status = ObOpenObjectByPointer(p_proc_, 0, NULL, 0, 0, KernelMode, &h_proc_);
	ObDereferenceObject(h_proc_);

	if (!NT_SUCCESS(nt_status))
		return nt_status;

	if (NULL == ZwQueryInformationProcess) {

		UNICODE_STRING routine_ = RTL_CONSTANT_STRING(_T("ZwQueryInformationProcess"));

		ZwQueryInformationProcess =
			(QUERY_INFO_PROCESS) MmGetSystemRoutineAddress(&routine_);

		if (NULL == ZwQueryInformationProcess) {
			nt_status = STATUS_INVALID_ADDRESS; goto proc_clean__;
		}
	}
	// (1) querying an actual size that is required for string buffer;
	nt_status = ZwQueryInformationProcess(
		h_proc_, ProcessImageFileName, NULL,  0,  &n_len_req
	);

	if (STATUS_INFO_LENGTH_MISMATCH != nt_status) {
		goto proc_clean__;
	}

	// (2) confirms buffer size;
	if (_p_name->MaximumLength < n_len_req) {
		nt_status = STATUS_INFO_LENGTH_MISMATCH; goto proc_clean__;
	}

	// (3) gets a process image path;
	nt_status = ZwQueryInformationProcess(
		h_proc_, ProcessImageFileName, _p_name->Buffer, _p_name->MaximumLength, &n_len_req
	);

proc_clean__:
	ZwClose(h_proc_);

	return   nt_status;
}
#if (0)
NTSTATUS   CProcess::ImageName_Ex(HANDLE _proc_id, PUNICODE_STRING _p_name) {
	_proc_id; _p_name;

	NTSTATUS nt_status  = STATUS_SUCCESS;
	PEPROCESS p_proc_   = NULL;
	HANDLE    h_proc_   = NULL;
	ULONG     n_len_req = 0;

	PAGE_CODE_EX(nt_status);

	if (NULL == _proc_id) return (nt_status = STATUS_INVALID_PARAMETER_1);
	if (NULL == _p_name ) return (nt_status = STATUS_INVALID_PARAMETER_2);

	nt_status = PsLookupProcessByProcessId(_proc_id, &p_proc_);
	if (!NT_SUCCESS(nt_status)) { return nt_status; }

	ObDereferenceObject(h_proc_); h_proc_ = NULL;

	return   nt_status;
}
#endif