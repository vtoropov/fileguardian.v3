/*
	Created by Tech_dog (ebontrop@gmail.com) on 31-Jan-2018 at 2:18:27p, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian driver common library volume object wrapper interface implementation file.
*/
#include "StdAfx.h"
#include "fg.driver.volume.object.h"
#include "fg.driver.common.ctx.h"
#include "fg.driver.common.str.h"

using namespace shared::km::aux;
using namespace shared::km::fl_sys;

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CVolume::CName::GuidName (
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects  ,
	__inout     PUNICODE_STRING           _pVolumeName
	)
{
	NTSTATUS        nt_status      = STATUS_SUCCESS;
	PUNICODE_STRING pSrcGuidName   = NULL;
	PTInstanceCtx   pInstanceCtx   = NULL;

	//
	//  Obtains an instance context. Target is NULL for instance context, as
	//  the FLT_INSTANCE can be obtained from the FltObjects.
	//
	nt_status = CCtxInstanceMan::GetContext(
						_pFltObjects, TRUE, (PFLT_CONTEXT*)&pInstanceCtx
					);
	if (NT_SUCCESS( nt_status )) {
		//
		//  sourceGuidName is the source from where we'll copy the volume
		//  GUID name. Hopefully the name is present in the instance context
		//  already (buffer is not NULL) so we'll try to use that.
		//
		pSrcGuidName = &pInstanceCtx->VolumeGuidName;
		if (NULL == pSrcGuidName->Buffer){
			//
			//  The volume GUID name is not cached in the instance context
			//  yet, so we will have to query the volume for it and put it
			//  in the instance context, so future queries can get it directly
			//  from the context.
			//
			UNICODE_STRING place_holder = {0};
			//
			//  adds sizeof(WCHAR) for appending a trailing backslash.
			//
			place_holder.MaximumLength = FILE_VOLUME_GUID_NAME_SIZE * sizeof(WCHAR) + sizeof(WCHAR);

			nt_status = CStringW::Allocate( &place_holder );
			if (!NT_SUCCESS( nt_status ))
				return nt_status; // unicode string allocation error;

			//  while there is no guid name, don't do the open by id deletion logic.
			// (it's actually better to defer obtaining the volume GUID name up to
			//  the point when we actually need it, in the open by ID scenario.)
			nt_status = FltGetVolumeGuidName(
							_pFltObjects->Volume, &place_holder, NULL
						);
			if (!NT_SUCCESS( nt_status ))
			{
				CStringW::Free(&place_holder);
				return nt_status;
			}

			RtlAppendUnicodeToString( &place_holder, _T("\\") );

			//
			//  Now set the sourceGuidName to the place_holder. It is okay to
			//  set Length and MaximumLength with no synchronization, because
			//  those will always be the same value (size of a volume GUID
			//  name with an extra trailing backslash).
			//
			pSrcGuidName->Length        = place_holder.Length;
			pSrcGuidName->MaximumLength = place_holder.MaximumLength;

			//
			//  Setting the buffer, however, requires some synchronization,
			//  because another thread might be attempting to do the same,
			//  and even though they're exactly the same string, they're
			//  different allocations (buffers) so if the other thread we're
			//  racing with manages to set the buffer before us, we need to
			//  free our temporary string buffer.
			//
			InterlockedCompareExchangePointer(
							(PVOID*)&(pSrcGuidName->Buffer), place_holder.Buffer, NULL
						);
			if (pSrcGuidName->Buffer != place_holder.Buffer){
				//
				//  We didn't manage to set the buffer, so let's free the
				//  place_holder buffer.
				//
				CStringW::Free( &place_holder );
			}
		}
		//
		//  pSrcGuidName now contains the correct GUID name, so copy that
		//  to the caller string.
		//
		RtlCopyUnicodeString( _pVolumeName, pSrcGuidName );

		FltReleaseContext( pInstanceCtx );
	}
	return nt_status;
}

NTSTATUS
CVolume::CName::DosName (
	__in        PFLT_VOLUME               _pFltVolume   ,
	__inout     PUNICODE_STRING           _pDosName
	)
{
	NTSTATUS        nt_status      = STATUS_SUCCESS;
	PDEVICE_OBJECT  pDeviceObject  = NULL;

	if (KeAreAllApcsDisabled())
		return (nt_status = STATUS_INVALID_DEVICE_REQUEST);

	nt_status = FltGetDiskDeviceObject(
					_pFltVolume, &pDeviceObject
				);
	if (NT_SUCCESS(nt_status)){
		nt_status = IoVolumeDeviceToDosName(
				pDeviceObject, _pDosName
			);
	}
	return  nt_status;
}

NTSTATUS
CVolume::CName::UncName (
	__in        PFLT_VOLUME               _pFltVolume   ,
	__inout     PUNICODE_STRING           _pName
	)
{
	ULONG      uRequired = 0;
	NTSTATUS   nt_status = FltGetVolumeName(
							_pFltVolume, NULL, &uRequired
						);
	if (STATUS_BUFFER_TOO_SMALL != nt_status)
		return nt_status;

	CStringW unc_name;

	nt_status = unc_name.Allocate(
						static_cast<USHORT>(uRequired)
					);
	if (!NT_SUCCESS(nt_status))
		return nt_status;

	if (NULL == _pName)
		return (nt_status = STATUS_INVALID_PARAMETER);

	nt_status = FltGetVolumeName(
					_pFltVolume, unc_name, NULL
				);
	if (NT_SUCCESS(nt_status)){
		unc_name.CopyTo(*_pName);
	}

	return  nt_status;
}

/////////////////////////////////////////////////////////////////////////////

CVolume::CProperties::CProperties(void)
{
	RtlZeroMemory(&m_enum, sizeof(FLT_VOLUME_PROPERTIES));
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CVolume::CProperties::Characters(PUNICODE_STRING _chars) const {
	NTSTATUS nt_status = STATUS_SUCCESS;

	if (NULL == _chars || NULL == _chars->Buffer || 0 == _chars->MaximumLength)
		return (nt_status = STATUS_INVALID_PARAMETER);

	if (m_enum.DeviceCharacteristics & FILE_READ_ONLY_DEVICE ) ::RtlAppendUnicodeToString(_chars, _T("ReadOnly;"));
	if (m_enum.DeviceCharacteristics & FILE_REMOVABLE_MEDIA  ) ::RtlAppendUnicodeToString(_chars, _T("Removable;"));
	if (m_enum.DeviceCharacteristics & FILE_REMOTE_DEVICE    ) ::RtlAppendUnicodeToString(_chars, _T("Remote;"));
	if (m_enum.DeviceCharacteristics & FILE_DEVICE_IS_MOUNTED) ::RtlAppendUnicodeToString(_chars, _T("Storage;"));
	if (m_enum.DeviceCharacteristics & FILE_VIRTUAL_VOLUME   ) ::RtlAppendUnicodeToString(_chars, _T("Virtual;"));
	if (m_enum.DeviceCharacteristics & FILE_PORTABLE_DEVICE  ) ::RtlAppendUnicodeToString(_chars, _T("Portable;"));

	return nt_status;
}

NTSTATUS
CVolume::CProperties::Enumerate (
	__in        PFLT_VOLUME               _pFltVolume
	)
{
	ULONG      uRequired = 0;
	NTSTATUS   nt_status = FltGetVolumeProperties(
					_pFltVolume, &m_enum, sizeof(FLT_VOLUME_PROPERTIES), &uRequired
				);
	//
	//  the buffer is large enough to hold the fixed portion of the FLT_VOLUME_PROPERTIES structure;
	//  no names are provided in this case;
	//
	if (STATUS_BUFFER_OVERFLOW == nt_status)
		return (nt_status = STATUS_SUCCESS);

	return nt_status;
}

NTSTATUS
CVolume::CProperties::Type(PUNICODE_STRING _type)const {

	NTSTATUS nt_status = STATUS_SUCCESS;

	if (NULL == _type || NULL == _type->Buffer || 0 == _type->MaximumLength)
		return (nt_status = STATUS_INVALID_PARAMETER);

	if (this->IsCdRom())    ::RtlAppendUnicodeToString(_type, _T("CdRom;"));
	if (this->IsHardDisk()) ::RtlAppendUnicodeToString(_type, _T("MassStorage;"));

	return nt_status;
}

const
FLT_VOLUME_PROPERTIES&
CVolume::CProperties::Values(void) const { return m_enum; }

/////////////////////////////////////////////////////////////////////////////

BOOLEAN    CVolume::CProperties::IsCdRom    (void) const { return (m_enum.DeviceType & FILE_DEVICE_CD_ROM); }
BOOLEAN    CVolume::CProperties::IsHardDisk (void) const { return (m_enum.DeviceType & FILE_DEVICE_DISK);   }

BOOLEAN    CVolume::CProperties::IsReadOnly (void) const { return (m_enum.DeviceCharacteristics & FILE_READ_ONLY_DEVICE); }
BOOLEAN    CVolume::CProperties::IsRemovable(void) const { return (m_enum.DeviceCharacteristics & FILE_REMOVABLE_MEDIA)||
                                                                  (m_enum.DeviceCharacteristics & FILE_FLOPPY_DISKETTE);  }
/////////////////////////////////////////////////////////////////////////////

NTSTATUS   CVolume::VolumeFromPath(PFLT_FILTER _p_filter, PUNICODE_STRING _uc_path, PFLT_VOLUME& _pFltVolume) {

	NTSTATUS nt_status = STATUS_SUCCESS;

	if (NULL == _uc_path)
		return (nt_status = STATUS_INVALID_PARAMETER);

	nt_status = FltGetVolumeFromName(_p_filter, _uc_path, &_pFltVolume);

	return nt_status;
}