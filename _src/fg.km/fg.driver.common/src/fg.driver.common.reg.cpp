/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Jul-2017 at 10:04:03a, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian shared library common driver registry interface implementation file.
*/
#include "StdAfx.h"
#include "fg.driver.common.reg.h"

using namespace shared::km::reg;
/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CRegistry::CloseKey(
	__inout HANDLE&      _pKeyHandle                 // registry key handle reference
	)
{
	NTSTATUS  nt_status = STATUS_SUCCESS;

	nt_status = ZwClose(_pKeyHandle);

	if (NT_SUCCESS(nt_status)){

		_pKeyHandle = NULL;
	}
	return nt_status;
}

NTSTATUS
CRegistry::OpenKey(
	__in PUNICODE_STRING _pRegPath  ,                // registry path
	__inout HANDLE&      _pKeyHandle,                // registry key handle reference
	__in_opt ACCESS_MASK _nDesiredAccess             // desired access to the key being opened
	)
{
	NTSTATUS          nt_status    =  STATUS_SUCCESS;
	OBJECT_ATTRIBUTES obj_atts     = {0};

	InitializeObjectAttributes(
			&obj_atts,
			_pRegPath,
			OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
			NULL,
			NULL
		);

	nt_status = ZwOpenKey(
					&_pKeyHandle,
					_nDesiredAccess,
					&obj_atts
				);

	return nt_status;
}

NTSTATUS
CRegistry::ReadDwordValue(
	__in const HANDLE    _pKeyHandle,                // registry key handle
	__in PUNICODE_STRING _pValueName,                // registry value name
	__inout DWORD&       _dwValue   ,                // actual value that is returned to a caller
	__in    DWORD        _dwDefault                  // a default value that is returned in case of failure
	)
{
	NTSTATUS        nt_status    = STATUS_SUCCESS;
	ULONG           uBytesRead   =  0;
	const UINT      nRawInfoSize = sizeof( KEY_VALUE_PARTIAL_INFORMATION ) + sizeof( LONG );
	PKEY_VALUE_PARTIAL_INFORMATION pValueInfo = NULL;
	UCHAR           raw_info[nRawInfoSize] = {0};
	UNICODE_STRING  uConvertable = {0};

	nt_status = ZwQueryValueKey(
					_pKeyHandle ,
					_pValueName ,
					KeyValuePartialInformation,
					raw_info    ,
					nRawInfoSize,
					&uBytesRead
				);
	if (!NT_SUCCESS(nt_status))
	{
		_dwValue = _dwDefault;
		return nt_status;
	}

	pValueInfo = (PKEY_VALUE_PARTIAL_INFORMATION) raw_info;

	if (REG_DWORD == pValueInfo->Type){
		_dwValue= *((PDWORD)&(pValueInfo->Data));
	}
	else
	if (REG_SZ == pValueInfo->Type)
	{
		uConvertable.Buffer = reinterpret_cast<PWCH>(&pValueInfo->Data[0]);
		uConvertable.Length = uConvertable.MaximumLength = static_cast<USHORT>(pValueInfo->DataLength);

		nt_status = RtlUnicodeStringToInteger(
				&uConvertable, 0, &uBytesRead
			);
		if (NT_SUCCESS(nt_status))
			_dwValue = uBytesRead;
	}
	else{
#if DBG
		DbgPrint(
			"__fg_fs: [ERROR] RegReadDwordValue::type(%d): value is not a number;\n", pValueInfo->Type
			);
#endif
	}
	return nt_status;
}

NTSTATUS
CRegistry::ReadStringValue(
	__in const HANDLE    _pKeyHandle,                // registry key handle
	__in PUNICODE_STRING _pValueName,                // registry value name
	__in PUNICODE_STRING _pValue                     // a pointer that is set to a result
	)
{
	NTSTATUS        nt_status    = STATUS_SUCCESS;
	ULONG           uRequireSize = 0;
	const UINT      nRawInfoSize = sizeof( KEY_VALUE_PARTIAL_INFORMATION )
	                             + sizeof( LONG )
	                             + CRegOption::eDefaultStringBufferLen * sizeof(WCHAR);
	PKEY_VALUE_PARTIAL_INFORMATION pValueInfo = NULL;
	UCHAR           raw_info[nRawInfoSize] = {0};

	nt_status = ZwQueryValueKey(
					_pKeyHandle ,
					_pValueName ,
					KeyValuePartialInformation,
					raw_info    ,
					nRawInfoSize,
					&uRequireSize
				);
	if (!NT_SUCCESS(nt_status)){
#if DBG
		if (STATUS_BUFFER_OVERFLOW == nt_status){
			DbgPrint(
			"__fg_fs: [ERROR] RegReadStringValue::size(): info buffer is small;\n"
			);
		}
#endif
		return nt_status;
	}

	pValueInfo = (PKEY_VALUE_PARTIAL_INFORMATION) raw_info;
	if (REG_SZ != pValueInfo->Type){
#if DBG
		DbgPrint(
			"__fg_fs: [ERROR] RegReadStringValue::type(%d): value is not a string;\n", pValueInfo->Type
			);
#endif
	}
	//
	// the documentation doesn't mention anyting about including 0-terminator to the buffer size;
	// it can lead that we allocate a buffer for sizeof(WCHAR) more than necessary;
	//
	else if (_pValue->MaximumLength >= (USHORT)pValueInfo->DataLength
		&& pValueInfo->DataLength > sizeof(WCHAR)){ // checks for empty string

		uRequireSize = pValueInfo->DataLength - sizeof(WCHAR);

		RtlCopyMemory(
			_pValue->Buffer, pValueInfo->Data, uRequireSize
			);

		_pValue->Length = static_cast<USHORT>( uRequireSize );
	}
	else {
#if DBG
		DbgPrint(
			"__fg_fs: [ERROR] RegReadStringValue::size(): target buffer is small;\n"
			);
#endif
	}

	return nt_status;
}