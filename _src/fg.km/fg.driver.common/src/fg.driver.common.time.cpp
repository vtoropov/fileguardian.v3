/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Jun-2018 at 8:38:16p, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian driver common library generic time interface declaration file.
*/
#include "StdAfx.h"
#include "fg.driver.common.time.h"

using namespace shared::km::aux;

/////////////////////////////////////////////////////////////////////////////

LARGE_INTEGER       CSystemTime::Current(const bool _b_local) {

	LARGE_INTEGER sys_tm = {0};

	KeQuerySystemTime(&sys_tm);

	if (_b_local) {
	
		LARGE_INTEGER local_  = {0};
		ExSystemTimeToLocalTime(&sys_tm, &local_);
		return local_;
	}
	else
		return sys_tm;
}

const TIME_FIELDS   CSystemTime::Timestamp(const bool _b_local) {

	TIME_FIELDS tm_flds = {0};

	LARGE_INTEGER sys_loc_time = CSystemTime::Current(_b_local);
	::RtlTimeToTimeFields(&sys_loc_time, &tm_flds);

	return tm_flds;
}

const TIME_FIELDS   CSystemTime::Timestamp(const LARGE_INTEGER _time) {

	TIME_FIELDS tm_flds = {0};
	LARGE_INTEGER sys_loc_time = _time;
	::RtlTimeToTimeFields(&sys_loc_time, &tm_flds);

	return tm_flds;
}