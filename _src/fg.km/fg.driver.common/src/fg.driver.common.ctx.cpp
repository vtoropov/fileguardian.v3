/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-May-2018 at 5:02:09p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian driver common library generic context interface implementation file. 
*/
#include "StdAfx.h"
#include "fg.driver.common.ctx.h"

using namespace shared::km::fl_sys;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace km { namespace fl_sys { namespace details {

	//
	// this class is for creating/destroying instance modification context;
	//
	class CInstanceCtxAlloca {
	public:
		static NTSTATUS Allocate (__in PFLT_FILTER _pFilter, __out_opt PFLT_CONTEXT* _ppContext ) {

			NTSTATUS nt_status = STATUS_SUCCESS;

			PAGE_CODE_EX(nt_status);

			nt_status = FltAllocateContext(
				_pFilter, FLT_INSTANCE_CONTEXT, sizeof(TInstanceCtx), NonPagedPool, _ppContext
			);

			if (NT_SUCCESS( nt_status )) {
				RtlZeroMemory( *_ppContext, sizeof(TInstanceCtx) );
			}

			return nt_status;
		}
		static VOID FLTAPI Free  (__in PFLT_CONTEXT _pContext ) {

			PTInstanceCtx pInstanceCtx = (PTInstanceCtx)_pContext;

			if (NULL == pInstanceCtx)
				return;

			PAGED_CODE();

			pInstanceCtx->VolumeGuidName.Length = 0;

			if ( NULL != pInstanceCtx->VolumeGuidName.Buffer )
			{
				ExFreePool( pInstanceCtx->VolumeGuidName.Buffer );
				pInstanceCtx->VolumeGuidName.Buffer        = NULL;
				pInstanceCtx->VolumeGuidName.MaximumLength = 0;
			}
		}
	};

}}}}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS CCtxInstanceMan::Create(
	__in        PFLT_FILTER   _pFilter,
	__inout_opt PFLT_CONTEXT* _ppContext
) {
	NTSTATUS nt_status = STATUS_SUCCESS;

	PAGED_CODE();

	if (NULL == _pFilter)
		return (nt_status = STATUS_INVALID_PARAMETER);
	if (NULL == _ppContext)
		return (nt_status = STATUS_INVALID_PARAMETER);

	nt_status = details::CInstanceCtxAlloca::Allocate(_pFilter, _ppContext);

	return  nt_status;
}

NTSTATUS  CCtxInstanceMan::SetContext(
	__in        PCFLT_RELATED_OBJECTS _pFltObjects ,
	__in        PFLT_CONTEXT          _pNewContext ,
	__inout_opt PFLT_CONTEXT*         _ppOldContext
) {
	PAGED_CODE();

	if (NULL == _pFltObjects)
		return STATUS_INVALID_PARAMETER;

	return FltSetInstanceContext( _pFltObjects->Instance, FLT_SET_CONTEXT_KEEP_IF_EXISTS, _pNewContext, _ppOldContext );
}

/////////////////////////////////////////////////////////////////////////////

VOID FLTAPI
CCtxInstanceMan::Destroy (
	__in        PFLT_CONTEXT _pContext
) {
	if (NULL == _pContext)
		return;
	details::CInstanceCtxAlloca::Free(_pContext); _pContext = NULL;
}

NTSTATUS
CCtxInstanceMan::GetContext(
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects ,
	__in        BOOL                      _bCreate     ,
	__inout_opt PFLT_CONTEXT*             _ppContext
) {
	NTSTATUS     nt_status    = STATUS_SUCCESS;
	if (NULL == _ppContext)
		return  (nt_status    = STATUS_INVALID_PARAMETER);

	nt_status = FltGetInstanceContext(_pFltObjects->Instance, _ppContext);

	if (STATUS_NOT_FOUND == nt_status && _bCreate) {
		nt_status = CCtxInstanceMan::Create(_pFltObjects->Filter, _ppContext);

		if (!NT_SUCCESS(nt_status))
			return nt_status;

		nt_status = CCtxInstanceMan::SetContext(_pFltObjects, _ppContext, NULL);

		if (!NT_SUCCESS(nt_status))
			*_ppContext = NULL;             // sets to NULL the pointer; nothing is returned to caller;
		else if (NULL != *_ppContext)
			FltReleaseContext(*_ppContext); // releases context after create procedure;
	}

	return nt_status;
}