/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-May-2018 at 7:59:35p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian shared library common driver string management interface implementation file.
*/
#include "StdAfx.h"
#include "fg.driver.common.str.h"

using namespace shared::km::aux;

#define AUX_STRING_POOL_TAG  'auxs'

namespace shared { namespace km { namespace aux {
/////////////////////////////////////////////////////////////////////////////

CStringW::CStringW(void)
{
	m_str.Buffer = NULL; m_str.Length = m_str.MaximumLength = 0;
}

CStringW::~CStringW(void)
{
	CStringW::Free(&m_str);
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS CStringW::Allocate(const USHORT _szBytes)
{
	NTSTATUS nt_status = STATUS_SUCCESS;
	if (!_szBytes)
		return (nt_status = STATUS_INVALID_PARAMETER);

	if (NULL != m_str.Buffer)
		CStringW::Free(&m_str);

	m_str.MaximumLength = _szBytes;
	nt_status = CStringW::Allocate(&m_str);

	return nt_status;
}

bool     CStringW::IsValid (void)const
{
	return (NULL != m_str.Buffer);
}

CStringW::operator PUNICODE_STRING(void) { return &m_str; }
CStringW::operator const PUNICODE_STRING(void)const { return (PUNICODE_STRING)&m_str; }

NTSTATUS CStringW::CopyFrom(CONST WCHAR* lpszSource, CONST UINT uLength)
{
	NTSTATUS nt_status = STATUS_SUCCESS;

	if (NULL == lpszSource || uLength < 1)
		return (nt_status = STATUS_INVALID_PARAMETER);

	__try {

		nt_status = this->Allocate(static_cast<USHORT>(uLength) * sizeof(WCHAR));
		if (!NT_SUCCESS(nt_status))
			return nt_status;

		RtlCopyMemory(&this->m_str.Buffer, lpszSource, uLength  * sizeof(WCHAR));

	}
	__except(EXCEPTION_EXECUTE_HANDLER) {
		nt_status = GetExceptionCode();
	}

	return  nt_status;
}

NTSTATUS CStringW::CopyFrom(PUNICODE_STRING pSource)
{
	if (NULL == pSource)
		return STATUS_INVALID_PARAMETER;

	NTSTATUS nt_status = this->Allocate(pSource->Length + sizeof(WCHAR));
	if (!NT_SUCCESS(nt_status))
		return nt_status;

	RtlCopyUnicodeString(&this->m_str, pSource);

	return nt_status;
}

NTSTATUS CStringW::CopyTo(UNICODE_STRING& _string)
{
	if (!this->IsValid())
		return STATUS_OBJECT_NAME_INVALID;

	RtlCopyUnicodeString(&_string, &this->m_str);
	return STATUS_SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CStringW::Allocate (
	__inout PUNICODE_STRING _pString
)
{
	PAGED_CODE();

	if (NULL == _pString)
		return STATUS_INVALID_PARAMETER;

	if (!_pString->MaximumLength)
		return STATUS_INVALID_PARAMETER;

	_pString->Length = 0;
	_pString->Buffer = (PWCH)ExAllocatePoolWithTag(
		PagedPool,
		_pString->MaximumLength,
		AUX_STRING_POOL_TAG
	);
	if (NULL == _pString->Buffer)
		return STATUS_INSUFFICIENT_RESOURCES;
	else {
		RtlZeroMemory(
			_pString->Buffer, _pString->MaximumLength
		);
		return STATUS_SUCCESS;
	}
}

INT
CStringW::Find (
	__inout PUNICODE_STRING _pWhere,
	__inout PUNICODE_STRING _pWhat
)
{
	BOOL bMatched = FALSE;
	INT n_ptr =  0;         // a pointer of current character in target (what ) string
	INT n_itr =  0;         // a pointer of current character in source (where) string
	INT n_pos = -1;
	INT n_src =  0;
	INT n_tgt =  0;

#if DBG
	WCHAR wSrc = 0;
	WCHAR wTgt = 0;
#endif

	if (NULL == _pWhere ||
		NULL == _pWhat )
		return n_pos;

	n_src = _pWhere->Length / sizeof(WCHAR);
	n_tgt = _pWhat->Length  / sizeof(WCHAR);

	if (_pWhat->Length > _pWhere->Length)
		return n_pos;

	while (n_itr < n_src &&
		n_ptr < n_tgt)
	{
#if DBG
		wSrc = _pWhere->Buffer[n_itr];
		wTgt = _pWhat->Buffer[n_ptr];
#endif
		bMatched = (_pWhere->Buffer[n_itr] == _pWhat->Buffer[n_ptr]);
		if (bMatched){
			if (n_pos == -1)
				n_pos = n_itr; // sets start position for the first time
			n_ptr += 1;
		}
		else if (n_pos != -1){ // resets a position in case of mismatch
			n_pos = -1;
			n_ptr =  0;
			continue;
		}

		n_itr += 1;
	}
	if (n_pos != -1 && (n_pos + n_tgt) > n_src) // the string is found partially
		n_pos  = -1;

	return n_pos;
}

VOID
CStringW::Free (
	__inout PUNICODE_STRING _pString
)
{
	PAGED_CODE();

	if (NULL == _pString)
		return;

	_pString->Length = 0;

	if ( NULL != _pString->Buffer )
	{
		ExFreePool( _pString->Buffer );
		_pString->Buffer        = NULL;
		_pString->MaximumLength = 0;
	}
}

VOID
CStringW::DigitToString(UNICODE_STRING& _str, const LONG _digit){

	RtlUnicodeStringPrintf(&_str, _T("%d"), _digit);
}

}}}