#ifndef _FGFSVOLUMEOBJECT_H_A3356492_9BDC_4ed9_BADB_4B3F1DD87FE7_INCLUDED
#define _FGFSVOLUMEOBJECT_H_A3356492_9BDC_4ed9_BADB_4B3F1DD87FE7_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 31-Jan-2018 at 1:36:17p, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian shared library common driver volume object wrapper interface declaration file.
*/
#define FILE_VOLUME_GUID_NAME_SIZE (48)

namespace shared { namespace km { namespace fl_sys {

	class CVolume {
	public:

		class CName {
		public:
			/*
			Routine Description:
				This helper routine returns a volume GUID name (with an added trailing
				backslash for convenience) in the _pVolumeName string passed by the
				caller.
				The volume GUID name is cached in the instance context for the instance
				attached to the volume, and this function will set up an instance context
				with the cached name on it if there isn't one already attached to the
				instance.

			Arguments:
				_pFltObjects - Pointer to the FLT_RELATED_OBJECTS data structure containing
							   opaque handles to this filter, instance, its associated volume and
							   file object.
				_pVolumeName - Pointer to UNICODE_STRING, returning the volume GUID name.

			Return Value:
				Return statuses forwarded by utils::CStringW::Allocate() or FltGetVolumeGuidName().
				On error, caller needs to invoke utils::CStringW::Free() on _pVolumeName.
			*/
			static
			NTSTATUS
			GuidName (
				__in        PCFLT_RELATED_OBJECTS     _pFltObjects  ,
				__inout     PUNICODE_STRING           _pVolumeName
				);
			/*
			Routine Description:
				This routine returns the MS-DOS path for a specified device object
				that represents a file system volume.
			Arguments:
				_pFltVolume  - Pointer to storage volume object;
				_pDosName    - Pointer to UNICODE_STRING, returning volume DOS name;
				 ***IMPORTANT***
					 a caller must free this value by calling ExFreePool() function;
			*/
			static
			NTSTATUS
			DosName (
				__in        PFLT_VOLUME               _pFltVolume   ,
				__inout     PUNICODE_STRING           _pDosName
				);

			/*
			Routine Description:
				The routine gets the volume name for a given volume;
			*/
			static
			NTSTATUS
			UncName (
				__in        PFLT_VOLUME               _pFltVolume   ,
				__inout     PUNICODE_STRING           _pName
				);
		};

		class CProperties {
		private:
			FLT_VOLUME_PROPERTIES  m_enum;

		public:
			CProperties(void);
		public:
			NTSTATUS   Characters(PUNICODE_STRING _chars)const;
			/*
			Routine description:
				This routine returns volume property information for the given volume.
			*/
			NTSTATUS   Enumerate(PFLT_VOLUME _pFltVolume);
			NTSTATUS   Type(PUNICODE_STRING _type)const;
			const
			FLT_VOLUME_PROPERTIES& Values(void) const;
		public:
			BOOLEAN    IsCdRom    (void) const;
			BOOLEAN    IsHardDisk (void) const;
			BOOLEAN    IsReadOnly (void) const;
			BOOLEAN    IsRemovable(void) const;
		};

		public:
		static
			NTSTATUS   VolumeFromPath(PFLT_FILTER _p_filter, PUNICODE_STRING _uc_path, PFLT_VOLUME& _pFltVolume);
	};
}}}

#endif/*_FGFSVOLUMEOBJECT_H_A3356492_9BDC_4ed9_BADB_4B3F1DD87FE7_INCLUDED*/