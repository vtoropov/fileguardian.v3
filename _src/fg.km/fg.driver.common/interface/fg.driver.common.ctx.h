#ifndef _FGDRIVERCOMMONCTX_H_82AD4FC3_B529_466F_A376_EB1014FB5A02_INCLUDED
#define _FGDRIVERCOMMONCTX_H_82AD4FC3_B529_466F_A376_EB1014FB5A02_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-May-2018 at 4:16:34p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian driver common library generic context interface declaration file. 
*/

namespace shared { namespace km { namespace fl_sys {

	//
	//  This is the instance context, it stores the volume's GUID name.
	//
	typedef struct _CTX_INSTANCE_CONTEXT {
		UNICODE_STRING  VolumeGuidName;
	}
	TInstanceCtx, *PTInstanceCtx;

	class CCtxInstanceMan {
	private:
		static
			NTSTATUS
			Create(
				__in        PFLT_FILTER               _pFilter  ,
				__inout_opt PFLT_CONTEXT*             _ppContext
			);

		static
			NTSTATUS
			SetContext(
				__in        PCFLT_RELATED_OBJECTS     _pFltObjects ,
				__in        PFLT_CONTEXT              _pNewContext ,
				__inout_opt PFLT_CONTEXT*             _ppOldContext
			);

	public:
		static
			VOID FLTAPI
			Destroy (
				__in        PFLT_CONTEXT              _pContext
			);

		static
			NTSTATUS
			GetContext(
				__in        PCFLT_RELATED_OBJECTS     _pFltObjects ,
				__in        BOOL                      _bCreate     ,
				__inout_opt PFLT_CONTEXT*             _ppContext
			);
	};
}}}

#endif/*_FGDRIVERCOMMONCTX_H_82AD4FC3_B529_466F_A376_EB1014FB5A02_INCLUDED*/