#ifndef _FGFSREGISTRYIFACE_H_A74BEE61_1303_4dd5_BC3B_30051A50F02B_INCLUDED
#define _FGFSREGISTRYIFACE_H_A74BEE61_1303_4dd5_BC3B_30051A50F02B_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Jul-2017 at 9:52:48a, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian sahred library common driver registry interface declration file.
*/
namespace shared { namespace km { namespace reg {

	class CRegOption {
	public:
		enum _e {
			eDefaultStringBufferLen = 1024,   // default length of string buffer in characters;
		};
	};

	class CRegistry {
	public:
		//
		// closes key and destroys the handle provided;
		//
		static
		NTSTATUS
		CloseKey(
			__inout HANDLE&      _pKeyHandle                 // registry key handle reference
			);
		//
		// opens registry key by specified path with read-only access (by default);
		// NOTE: caller is responsible for closing the registry key;
		//
		static
		NTSTATUS
		OpenKey(
			__in PUNICODE_STRING _pRegPath  ,                // registry path
			__inout HANDLE&      _pKeyHandle,                // registry key handle reference
			__in_opt ACCESS_MASK _nDesiredAccess = KEY_READ  // desired access to the key being opened
			);
		//
		// reads a DWORD value; if data type of the value is not DWORD, an error code is returned;
		//
		static
		NTSTATUS
		ReadDwordValue(
			__in const HANDLE    _pKeyHandle,                // registry key handle
			__in PUNICODE_STRING _pValueName,                // registry value name
			__inout DWORD&       _dwValue   ,                // a value that is returned to a caller
			__in    DWORD        _dwDefault  = 0             // a default value that is returned in case of failure
			);
		//
		// reads a DWORD value; if data type of the value is not DWORD, an error code is returned;
		//
		static
		NTSTATUS
		ReadStringValue(
			__in const HANDLE    _pKeyHandle,                // registry key handle
			__in PUNICODE_STRING _pValueName,                // registry value name
			__in PUNICODE_STRING _pValue                     // a pointer that is set to a result
			);
	};
}}}

#endif/*_FGFSREGISTRYIFACE_H_A74BEE61_1303_4dd5_BC3B_30051A50F02B_INCLUDED*/