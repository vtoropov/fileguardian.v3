#ifndef _FGDRIVERCOMMONPROC_H_0D3ACF2E_DC70_4444_9894_F446DFB1CDFC_INCLUDED
#define _FGDRIVERCOMMONPROC_H_0D3ACF2E_DC70_4444_9894_F446DFB1CDFC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jun-2018 at 2:24:48p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian driver common library generic process interface declaration file.
*/

namespace shared { namespace km { namespace proc_sys {

	class CProcess {
	public:
		static NTSTATUS   Id(PFLT_CALLBACK_DATA  _p_data, HANDLE& _proc_id);
		static ULONG      IdAsLong(PFLT_CALLBACK_DATA  _p_data);
		static NTSTATUS   ImageName(HANDLE _proc_id, PUNICODE_STRING _p_name);
#if (0)
		static NTSTATUS   ImageName_Ex(HANDLE _proc_id, PUNICODE_STRING _p_name);
#endif
	};
}}}

#endif/*_FGDRIVERCOMMONPROC_H_0D3ACF2E_DC70_4444_9894_F446DFB1CDFC_INCLUDED*/