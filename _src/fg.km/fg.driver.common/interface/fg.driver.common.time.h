#ifndef _FGDRIVERCOMMONTM_H_32BCED2F_AC61_4B82_B11E_701E9E6181EF_INCLUDED
#define _FGDRIVERCOMMONTM_H_32BCED2F_AC61_4B82_B11E_701E9E6181EF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Jun-2018 at 7:50:36p, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian driver common library generic time interface declaration file.
*/

namespace shared { namespace km { namespace aux {

	class CSystemTime {

	public:
		static LARGE_INTEGER       Current(const bool _b_local = true); // returns current system time, if local is true, local time is returned;
		static const TIME_FIELDS   Timestamp(const bool _b_local = true); // returns current system timestamps, if local is true, local timestamp is returned;
		static const TIME_FIELDS   Timestamp(const LARGE_INTEGER _time); // return timestamp from a time provided;
	};
}}}

#endif/*_FGDRIVERCOMMONTM_H_32BCED2F_AC61_4B82_B11E_701E9E6181EF_INCLUDED*/