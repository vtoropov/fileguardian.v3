#ifndef _FGDRIVERCOMMONLOG_H_4E3F1026_CE41_4198_9260_1D9DF582CC9C_INCLUDED
#define _FGDRIVERCOMMONLOG_H_4E3F1026_CE41_4198_9260_1D9DF582CC9C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-May-2018 at 2:00:29p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian common driver log interface declaration file.
*/
#include "fg.driver.log.defs.h"
#include "fg.driver.common.str.h"

namespace shared { namespace km { namespace log {

	using shared::km::aux::CStringW;

	class CLogOptions {
	public:
		static ULONG    Get(void);
		static BOOLEAN  Has(const CLogOption::_e);
		static BOOLEAN  Modify(const CLogOption::_e, const BOOLEAN bSet);
		static BOOLEAN  Set(const ULONG);
	};

	class CLogSettings {
	public:
		static NTSTATUS        Path(CONST UNICODE_STRING&, const ULONG _dummy);  // sets log file path;
		static NTSTATUS        Path(UNICODE_STRING& _result);                    // copies log file path to string provided; max buffer length must be specified;
		static CLogVerbose::_e Verbose(void);
		static NTSTATUS        Verbose(const CLogVerbose::_e);
	};

	class CLogger {
	public:
		static NTSTATUS   Initialize(__in_opt PDEVICE_OBJECT);
		static NTSTATUS   Terminate (__in_opt PDEVICE_OBJECT);
	public:
		static VOID       HandleIrpShutdown(VOID);
	public:
		static VOID       Put(__in ULONG uLevel, __in_opt CONST PWCHAR lpszFunction, __in_opt CONST PWCHAR lpszFormat, ...);
	};

	class CLogger_simplified {
	public:
		static NTSTATUS   Initialize(VOID);
		static NTSTATUS   Terminate (VOID);
	public:
		static VOID   Put(PUNICODE_STRING _lpszLine);
		static VOID   Put(PUNICODE_STRING _lpszTargetObject, PUNICODE_STRING _lpszMatched);
	};
}}}

#endif/*_FGDRIVERCOMMONLOG_H_4E3F1026_CE41_4198_9260_1D9DF582CC9C_INCLUDED*/