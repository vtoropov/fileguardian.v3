#ifndef _FGDRIVERCOMMONLOGDEFS_H_A4392FE3_2B95_4F02_A2B4_1DEE07C79849_INCLUDED
#define _FGDRIVERCOMMONLOGDEFS_H_A4392FE3_2B95_4F02_A2B4_1DEE07C79849_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-May-2018 at 8:49:36a, UTC+7, Phuket, Rawai, Thursday;
	This is File Guardian shared library common driver log interface definition file.
*/

namespace shared { namespace km { namespace log {

	class CLogLevel {
	public:
		enum _e : ULONG{
			eError      = 0x01,    // exception level;
			eWarning    = 0x02,    // warning level;
			eInfo       = 0x04,    // informative;
			eDebug      = 0x08,    // development level;
		};
	};

	class CLogVerbose {
	public:
		enum _e : ULONG {
			eDisabled   =  0x0,    // no information is available for log operation;
			eError      =  CLogLevel::eError ,
			eWarning    =  CLogLevel::eError | CLogLevel::eWarning ,
			eInfo       =  CLogLevel::eError | CLogLevel::eWarning | CLogLevel::eInfo ,
			eDebug      =  CLogLevel::eError | CLogLevel::eWarning | CLogLevel::eInfo | CLogLevel::eDebug ,
		};
	};

	class CLogOption {
	public:
		enum _e : ULONG {
			eNoTimestamp = 0x100,
			eNoFunction  = 0x200,
			eUseSafeMode = 0x400,
		};
	};

	class CLogDefault {
	public:
		enum _e {
			eFilePathLen = 512, // default log file path length in characters;
		};
	};
}}}

#endif/*_FGDRIVERCOMMONLOGDEFS_H_A4392FE3_2B95_4F02_A2B4_1DEE07C79849_INCLUDED*/