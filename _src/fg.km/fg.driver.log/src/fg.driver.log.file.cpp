/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Jun-2018 at 12:20:57p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian driver common log library file interface implementation file.
*/
#include "StdAfx.h"
#include "fg.driver.log.file.h"

using namespace shared::km::log::_imp;

#include "fg.driver.common.str.h"

using namespace shared::km::aux;

/////////////////////////////////////////////////////////////////////////////

NTSTATUS   CLogFile::Create(void) {

	NTSTATUS nt_status = STATUS_SUCCESS;
	//
	// a log file path must be absolute, at least it must contain a destination folder,
	// otherwise, we'll get STATUS_OBJECT_PATH_SYNTAX_BAD;
	//
	UNICODE_STRING path_= RTL_CONSTANT_STRING(_T("\\??\\Global\\c:\\fg_driver_log.log"));

	OBJECT_ATTRIBUTES atts_ = {0};
	InitializeObjectAttributes(
		&atts_, &path_, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, NULL
	);
	IO_STATUS_BLOCK io_status = {0};
	nt_status = ::ZwCreateFile(
		&CLogFile::_Handle_Ref(),
		FILE_APPEND_DATA | SYNCHRONIZE,
		&atts_    ,
		&io_status,
		NULL      ,
		FILE_ATTRIBUTE_NORMAL,
		FILE_SHARE_READ      ,
		FILE_OPEN_IF         ,
		FILE_SYNCHRONOUS_IO_NONALERT | FILE_NON_DIRECTORY_FILE, NULL, 0
	);

	return nt_status;
}

NTSTATUS   CLogFile::Close (void) {

	NTSTATUS nt_status = STATUS_SUCCESS;
	HANDLE&  handle_   = CLogFile::_Handle_Ref();

	if (NULL != handle_) {
		nt_status = ::ZwClose(handle_); handle_ = NULL;
	}
	return nt_status;
}

HANDLE     CLogFile::Handle(void) { return CLogFile::_Handle_Ref(); }

/////////////////////////////////////////////////////////////////////////////

HANDLE&     CLogFile::_Handle_Ref(void) { static HANDLE handle_ = NULL; return handle_; }