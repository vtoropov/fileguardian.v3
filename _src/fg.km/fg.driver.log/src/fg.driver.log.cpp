/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-May-2018 at 2:27:47p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian common driver log interface implementation file.
*/
#include "StdAfx.h"
#include "fg.driver.log.h"
#include "fg.driver.log.rec.h"
#include "fg.driver.log.writer.h"
#include "fg.driver.log.format.h"

using namespace shared::km::log;
using namespace shared::km::log::_imp;

#include "fg.driver.common.time.h"

using namespace shared::km::aux;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace km { namespace log { namespace details {

	class CLogLocker {
	public:
		static KSPIN_LOCK&   Handle(void) {
			static KSPIN_LOCK  pDataLock = NULL; // lock object of options/setting data
			return pDataLock;
		}
		static BOOLEAN       IsValid(void) { return (_Inited()); }
		static BOOLEAN       Lock(KIRQL& _prevLevel) {
			BOOLEAN result_ = TRUE;
			if (FALSE == CLogLocker::IsValid())
				return (result_ =  FALSE);
			KeAcquireSpinLock(
				&CLogLocker::Handle(), &_prevLevel
			);
			return result_;
		}
		static BOOLEAN       Release(KIRQL& _prevLevel) {
			if (FALSE == CLogLocker::IsValid())
				return FALSE;
			KeReleaseSpinLock(
				&CLogLocker::Handle(), _prevLevel
			);
			return TRUE;
		}
	public:
		static NTSTATUS      Intialize(void){

			NTSTATUS nt_status = STATUS_SUCCESS;

			if (FALSE == CLogLocker::IsValid()) {
				KeInitializeSpinLock(&CLogLocker::Handle());
				_Inited() = TRUE;
			}
			else nt_status = STATUS_ALREADY_INITIALIZED;
#if DBG
			DbgPrint(
				"__fg_fs: [INFO] CLogLocker::Intialize(): status=%X;\n", nt_status
			);
#endif
			return nt_status;
		}
		static NTSTATUS      Terminate(void){

			NTSTATUS nt_status = STATUS_SUCCESS;
			_Inited() = FALSE;
			return nt_status;
		}
	private:
		static BOOLEAN& _Inited(void) {
			static BOOLEAN inited_ = FALSE;
			return inited_;
		}
	};

	ULONG&        CLogHelper_OptionsRef(void) {
		static ULONG m_options = CLogOption::eUseSafeMode;
		return m_options;
	}

	UNICODE_STRING&
	              CLogHelper_FilePath(void) {
		static WCHAR m_buffer[CLogDefault::eFilePathLen] = {0};
		static UNICODE_STRING m_log_path = {0, CLogDefault::eFilePathLen * sizeof(WCHAR), m_buffer};
		if (0 == m_buffer[0]) {
			static UNICODE_STRING cs_default = RTL_CONSTANT_STRING(_T("fg_fs_filter.default.log"));
			::RtlCopyMemory(
					&m_buffer[0], &cs_default.Buffer[0], cs_default.Length
				);
		}
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] CLogHelper_FilePath() name=%wZ; size=%d;\n", &m_log_path, m_log_path.Length
		);
#endif
		return m_log_path;
	}

	CLogVerbose::_e&
	              CLogHelper_Verbose(void) {
#if DBG
		static CLogVerbose::_e m_verbose = CLogVerbose::eDebug;
#else
		static CLogVerbose::_e m_verbose = CLogVerbose::eDisabled;
#endif
		return m_verbose;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

ULONG         CLogOptions::Get(void) {
	ULONG ul_opts = CLogOption::eUseSafeMode;
	KIRQL prv_lvl = 0;
	if (FALSE == details::CLogLocker::Lock(prv_lvl))
		return ul_opts;
	ul_opts = details::CLogHelper_OptionsRef();

	details::CLogLocker::Release(prv_lvl);
	return ul_opts;
}

BOOLEAN       CLogOptions::Has(const CLogOption::_e _val) {
	BOOLEAN bResult = FALSE;
	KIRQL prv_lvl = 0;
	if (FALSE == details::CLogLocker::Lock(prv_lvl))
		return bResult;

	bResult = (0 != (_val & details::CLogHelper_OptionsRef()));

	details::CLogLocker::Release(prv_lvl);
	return bResult;
}

BOOLEAN       CLogOptions::Modify(const CLogOption::_e _val, const BOOLEAN bSet) {
	BOOLEAN bResult = FALSE;
	KIRQL prv_lvl = 0;
	if (FALSE == details::CLogLocker::Lock(prv_lvl))
		return bResult;

	if (bSet) details::CLogHelper_OptionsRef() |=  _val;
	else      details::CLogHelper_OptionsRef() &= ~_val;

	details::CLogLocker::Release(prv_lvl);
	return (bResult = TRUE);
}

BOOLEAN       CLogOptions::Set(const ULONG _val) {
	BOOLEAN bResult = FALSE;
	KIRQL prv_lvl = 0;
	if (FALSE == details::CLogLocker::Lock(prv_lvl))
		return bResult;

	details::CLogHelper_OptionsRef() = _val;

	details::CLogLocker::Release(prv_lvl);
	return (bResult = TRUE);
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS      CLogSettings::Path(CONST UNICODE_STRING& _path, const ULONG) {
#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CLogSettings::Path() name=%wZ; size=%d;\n", &_path, _path.Length
	);
#endif
	if (NULL == _path.Buffer) return STATUS_INVALID_PARAMETER;
	if (1 > _path.Length ||
	        _path.Length * sizeof(WCHAR) > CLogDefault::eFilePathLen)
		return STATUS_INVALID_PARAMETER;

	KIRQL prev_lvl = 0;

	if (FALSE == details::CLogLocker::Lock(prev_lvl))
		return STATUS_INVALID_LOCK_SEQUENCE;

	NTSTATUS nt_status = STATUS_SUCCESS;
#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CLogSettings::Path() name=%wZ; size=%d;\n", &_path, _path.Length
		);
#endif
	// FIXED: unicode string length must be in bytes;
	__try {
		RtlZeroMemory(&details::CLogHelper_FilePath().Buffer[0], CLogDefault::eFilePathLen * sizeof(WCHAR));
		RtlCopyMemory(&details::CLogHelper_FilePath().Buffer[0], &_path.Buffer, _path.Length );
		details::CLogHelper_FilePath().Length = _path.Length;
	}
	__except (EXCEPTION_EXECUTE_HANDLER) {
		nt_status = GetExceptionCode();
	}

	details::CLogLocker::Release(prev_lvl);

	return nt_status;
}

NTSTATUS      CLogSettings::Path(UNICODE_STRING& _result) {

	if (sizeof(WCHAR) > _result.MaximumLength)
		return STATUS_INVALID_PARAMETER;

	KIRQL prev_lvl = 0;

	if (FALSE == details::CLogLocker::Lock(prev_lvl))
		return STATUS_INVALID_LOCK_SEQUENCE;

	const USHORT uRequired = details::CLogHelper_FilePath().Length;
	if (uRequired > _result.MaximumLength)
		return STATUS_BUFFER_TOO_SMALL;

	NTSTATUS nt_status = STATUS_SUCCESS;

	__try {
		RtlZeroMemory( _result.Buffer, _result.MaximumLength * sizeof(WCHAR));
		RtlCopyMemory( _result.Buffer, details::CLogHelper_FilePath().Buffer, uRequired);
		_result.Length = uRequired;
	}
	__except (EXCEPTION_EXECUTE_HANDLER) {
		nt_status = GetExceptionCode();
	}

	details::CLogLocker::Release(prev_lvl);

	return nt_status;
}

CLogVerbose::_e
              CLogSettings::Verbose(void) {

	CLogVerbose::_e verbose_ = CLogVerbose::eDisabled;

	KIRQL prev_lvl = 0;

	if (FALSE == details::CLogLocker::Lock(prev_lvl))
		return verbose_;

	verbose_ = details::CLogHelper_Verbose();

	details::CLogLocker::Release(prev_lvl);

	return verbose_;
}

NTSTATUS      CLogSettings::Verbose(const CLogVerbose::_e _val) {
	
	KIRQL prev_lvl = 0;

	if (FALSE == details::CLogLocker::Lock(prev_lvl))
		return STATUS_INVALID_LOCK_SEQUENCE;

	details::CLogHelper_Verbose() = _val;
	details::CLogLocker::Release(prev_lvl);

	return STATUS_SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace km { namespace log { namespace details {

	ULONG& CLogHelper_Flags(void) {
		static ULONG m_flags = CLogVerbose::eDisabled;
		return m_flags;
	}

}}}}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS CLogger::Initialize(__in_opt PDEVICE_OBJECT pDevice) {

	NTSTATUS nt_status = STATUS_SUCCESS; pDevice;

	PAGE_CODE_EX(nt_status);

	if (NULL == pDevice && FALSE) return (nt_status = STATUS_INVALID_PARAMETER);

	nt_status = details::CLogLocker::Intialize();
	if (FALSE == NT_SUCCESS(nt_status))
		return nt_status;
	
	nt_status = CRecordStorage::Initialize();
#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CLogger::Intialize(): storage status=%X;\n", nt_status
	);
#endif
	if (FALSE == NT_SUCCESS(nt_status))
		return nt_status;

	nt_status = CLogWriter::Initialize(pDevice);

	if (FALSE == NT_SUCCESS(nt_status))
		return nt_status;

	return nt_status;
}

NTSTATUS CLogger::Terminate (__in_opt PDEVICE_OBJECT pDevice) {

	NTSTATUS nt_status = STATUS_SUCCESS; pDevice;

	PAGE_CODE_EX(nt_status);

	nt_status = CLogWriter::Terminate(pDevice);
	nt_status = CRecordStorage::Terminate();
	nt_status = details::CLogLocker::Terminate();

	return nt_status;
}
/////////////////////////////////////////////////////////////////////////////

#include "fg.driver.log.file.h"

using namespace shared::km::log::_imp;

NTSTATUS   CLogger_simplified::Initialize(VOID) {

	NTSTATUS nt_status = CLogFile::Create();
	if (NT_SUCCESS(nt_status)) {

		UNICODE_STRING cap_ = RTL_CONSTANT_STRING(_T("Target Object Path;\tMatched Folder Path;"));
		CLogger_simplified::Put(
			&cap_, NULL
		);
	}
	return nt_status;

}

NTSTATUS   CLogger_simplified::Terminate (VOID) {

	NTSTATUS nt_status = CLogFile::Close();
	return nt_status;

}

/////////////////////////////////////////////////////////////////////////////

VOID   CLogger_simplified::Put(PUNICODE_STRING _lpszLine) {

	if (CLogFile::Handle() == NULL)
		return;

	if (NULL == _lpszLine || NULL == _lpszLine->Buffer)
		return;

	IO_STATUS_BLOCK io_stat_ = {0};

	LARGE_INTEGER sys_tm = CSystemTime::Current(true);
	DECLARE_UNICODE_STRING(cur_tm, CLogFormat_Spec::eTimestampLen);

	NTSTATUS nt_status = CLogFormat::Format(sys_tm, cur_tm);
	if (NT_SUCCESS(nt_status)) {
		DECLARE_UNICODE_STRING(line_, 0x800);

		RtlAppendUnicodeStringToString(&line_, &cur_tm);
		RtlAppendUnicodeToString(&line_, _T(" "));
		RtlAppendUnicodeStringToString(&line_, _lpszLine);

		nt_status = ZwWriteFile(
			CLogFile::Handle(), NULL, NULL, NULL, &io_stat_, _lpszLine->Buffer, _lpszLine->Length, NULL, NULL
		);
	}
	else
		nt_status = ZwWriteFile(
			CLogFile::Handle(), NULL, NULL, NULL, &io_stat_, _lpszLine->Buffer, _lpszLine->Length, NULL, NULL
		);
	nt_status;
}

VOID   CLogger_simplified::Put(PUNICODE_STRING _lpszTargetObject, PUNICODE_STRING _lpszMatched) {

	if (CLogFile::Handle() == NULL)
		return;

	DECLARE_UNICODE_STRING(rec_, 0x800);
	UNICODE_STRING tab_ln = RTL_CONSTANT_STRING(_T("\t"));

	LARGE_INTEGER sys_tm = CSystemTime::Current(true);
	DECLARE_UNICODE_STRING(cur_tm, CLogFormat_Spec::eTimestampLen);

	NTSTATUS nt_status = CLogFormat::Format(sys_tm, cur_tm);
	if (NT_SUCCESS(nt_status)) {
		RtlAppendUnicodeStringToString(&rec_, &cur_tm);
		RtlAppendUnicodeToString(&rec_, _T(" "));
	}

	if (NULL != _lpszTargetObject && NULL != _lpszTargetObject->Buffer && _lpszTargetObject->Length > 0) {
		::RtlAppendUnicodeStringToString(
			&rec_, _lpszTargetObject
		);
		::RtlAppendUnicodeStringToString(
			&rec_, &tab_ln
		);
	}

	if (NULL != _lpszMatched && NULL != _lpszMatched->Buffer && _lpszMatched->Length > 0) {
		::RtlAppendUnicodeStringToString(
			&rec_, _lpszMatched
		);
	}

	UNICODE_STRING new_ln = RTL_CONSTANT_STRING(_T("\n\n"));
	::RtlAppendUnicodeStringToString(
		&rec_, &new_ln
	);

	IO_STATUS_BLOCK io_stat_ = {0};

	nt_status = ZwWriteFile(
			CLogFile::Handle(), NULL, NULL, NULL, &io_stat_, rec_.Buffer, rec_.Length, NULL, NULL
		);
	nt_status;
}