#ifndef _FGDRIVERCOMMONLOGREC_H_777FD35A_2AC0_4E49_9BA0_0F68EB7972CB_INCLUDED
#define _FGDRIVERCOMMONLOGREC_H_777FD35A_2AC0_4E49_9BA0_0F68EB7972CB_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-May-2018 at 12:50:48p, UTC+7, Phuket, Rawai, Friday;
	This File Guardian shared library common driver log record interface declaration file.
*/
#include "fg.driver.log.defs.h"
#include "fg.driver.log.spec.h"

#ifndef LIST_HEAD
#define LIST_HEAD LIST_ENTRY
#endif

namespace shared { namespace km { namespace log { namespace _imp {

	typedef struct CLogRecord_Entry {
		LIST_ENTRY         m_index;
		TLogRecordData     m_data;
	}   TLogRecordEntry,
	  *PTLogRecordEntry;

	typedef struct CLogRecord_List {
		LIST_HEAD          m_head;     // a start/first record of a storage/list;
		LONG               m_allowed;  // a capacity of a storage in records count;
		__volatile LONG    m_current;  // a current amount/number of records in a storage;
	}   TLogRecordList,
	  *PTLogRecordList;

	class CRecordStorage {
	public:
		enum _e {
			eMaxRecAllowed = 0xFF,     // an amount of records that is allowed in log record storage/list; 
		};
	public:
		class CRecords {
		public:
			static PTLogRecordEntry
			                  Available(void);
			static NTSTATUS   Append(const CLogLevel::_e, const PUNICODE_STRING pwFunc, const PUNICODE_STRING pwDescript);
			static NTSTATUS   Normalize(void);
			static NTSTATUS   Remove(PTLogRecordEntry);
			static NTSTATUS   RemoveAll (void);
		};
	public:
		static NTSTATUS   Initialize(void);
		static NTSTATUS   Terminate (void);
	};

}}}}

#endif/*_FGDRIVERCOMMONLOGREC_H_777FD35A_2AC0_4E49_9BA0_0F68EB7972CB_INCLUDED*/