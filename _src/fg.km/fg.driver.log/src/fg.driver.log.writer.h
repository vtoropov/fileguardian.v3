#ifndef _FGDRIVERCOMMONLOGWRITER_H_0CB6F303_7B25_4A0F_9D4E_7B4279AE5C48_INCLUDED
#define _FGDRIVERCOMMONLOGWRITER_H_0CB6F303_7B25_4A0F_9D4E_7B4279AE5C48_INCLUDED
	/*
		Created by Tech_dog (ebontrop@gmail.com) on 25-May-2018 at 7:36:45p, UTC+7, Phuket, Rawai, Friday;
		This is File Guardian shared library common driver log writer interface declaration file.
	*/

namespace shared { namespace km { namespace log { namespace _imp {

	class CLogWriter {
	public:
		static NTSTATUS   Initialize(PDEVICE_OBJECT pDevice); // initializes a writer; log file path is used from log settings;
		static NTSTATUS   Terminate (PDEVICE_OBJECT pDevice);
	};

}}}}

#endif/*_FGDRIVERCOMMONLOGWRITER_H_0CB6F303_7B25_4A0F_9D4E_7B4279AE5C48_INCLUDED*/