#ifndef _FGDRIVERLOGFILE_H_C5E2EEF0_1F7F_418C_BD4B_CC1B9D68043C_INCLUDED
#define _FGDRIVERLOGFILE_H_C5E2EEF0_1F7F_418C_BD4B_CC1B9D68043C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Jun-2018 at 12:13:28p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian driver common log library file interface declaration file.
*/

namespace shared { namespace km { namespace log { namespace _imp {

	class CLogFile {
	public:
		static NTSTATUS   Create(void); // creates/opens a file with default name;
		static NTSTATUS   Close (void);
		static HANDLE     Handle(void);
	private:
		static HANDLE&   _Handle_Ref(void);
	};

}}}}

#endif/*_FGDRIVERLOGFILE_H_C5E2EEF0_1F7F_418C_BD4B_CC1B9D68043C_INCLUDED*/