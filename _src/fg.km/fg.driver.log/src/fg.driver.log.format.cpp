/*
	Created by Tech_dog (ebontrop@gmail.com) on 26-May-2018 at 1:46:41p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian shared library common driver log data format interface implementation file.
*/
#include "StdAfx.h"
#include "fg.driver.log.format.h"
#include "fg.driver.log.rec.h"
#include "fg.driver.common.str.h"

using namespace shared::km::log;
using namespace shared::km::log::_imp;

#include "fg.driver.common.time.h"

using namespace shared::km::aux;

//////////////////////////////////////////////////////////////////////////////

NTSTATUS   CLogFormat::Format(const LARGE_INTEGER _time , UNICODE_STRING& _result) {
	return CLogFormat::_Time(_time, _result);
}

NTSTATUS   CLogFormat::Format(const TLogRecordData& _rec, UNICODE_STRING& _line) {

	NTSTATUS nt_status = STATUS_SUCCESS; _line;
	if (NULL == _line.Buffer ||
	    FALSE == CLogFormat_Spec::Compare(CLogFormat_Spec::uTotalLineLen, _line))
		return (nt_status = STATUS_INVALID_PARAMETER);

	DECLARE_UNICODE_STRING(level_, CLogFormat_Spec::eLevelLen);
	nt_status = CLogFormat::_Level(_rec.m_level, level_);   if (FALSE == NT_SUCCESS(nt_status)) return nt_status;

	DECLARE_UNICODE_STRING(time_, CLogFormat_Spec::eTimestampLen);
	nt_status = CLogFormat::_Time(_rec.m_timestamp, time_); if (FALSE == NT_SUCCESS(nt_status)) return nt_status;

	DECLARE_UNICODE_STRING(func_, CLogFormat_Spec::eFuncNameLen);
	nt_status = CLogFormat::_Func((PWCHAR)&_rec.m_fun_path[0], func_); if (FALSE == NT_SUCCESS(nt_status)) return nt_status;

	UNICODE_STRING desc_ = {
			CLogRecord_Spec::eDescriptLen,
			CLogRecord_Spec::eDescriptLen,
			const_cast<PWCHAR>(_rec.m_descript)
		};

	::RtlAppendUnicodeStringToString(&_line, &level_);
	::RtlAppendUnicodeStringToString(&_line, &time_ );
	::RtlAppendUnicodeStringToString(&_line, &func_ );
	::RtlAppendUnicodeStringToString(&_line, &desc_ );

	return nt_status;
}

//////////////////////////////////////////////////////////////////////////////

NTSTATUS   CLogFormat::_Func (const PWCHAR _fun_complete, UNICODE_STRING& _result) {

	NTSTATUS nt_status = STATUS_SUCCESS;

	if (NULL == _fun_complete)
		return (nt_status = STATUS_INVALID_PARAMETER);

	if (NULL == _result.Buffer ||
	    FALSE == CLogFormat_Spec::Compare(CLogFormat_Spec::eFuncNameLen, _result))
		return (nt_status = STATUS_INVALID_PARAMETER);

	PWCHAR iter_ =  _fun_complete;
	PWCHAR name_ =  _fun_complete;

	// (1) finds the root name of a function, i.e. function clean/base name;
	while (*(iter_++)) {
		if (*iter_ == _T(':')) {
			name_ = iter_ + 1;   // double colon are expected, like this: ::;
		}
	}

	// (2) evaluates a length of function name in characters;
	SHORT len_ = 0; iter_ = name_;

	while (*(iter_++)) {
		len_ += 1;
	}

	if (NULL == len_ || len_ > CLogFormat_Spec::eFuncNameLen)
		return (nt_status = STATUS_INVALID_PARAMETER);

	RtlCopyMemory(
		_result.Buffer, name_, len_ * sizeof(WCHAR) // specifies a length in bytes;
	);
	_result.Length = len_;

	return nt_status;
}

NTSTATUS   CLogFormat::_Level(const CLogLevel::_e _lvl, UNICODE_STRING& _result) {

	NTSTATUS nt_status = STATUS_SUCCESS;

	if (NULL == _result.Buffer ||
	    FALSE == CLogFormat_Spec::Compare(CLogFormat_Spec::eLevelLen, _result))
		return (nt_status = STATUS_INVALID_PARAMETER);

	switch (_lvl) {
	case CLogLevel::eDebug:   RtlUnicodeStringCopyString(&_result, _T("[dbg]")); break;
	case CLogLevel::eError:   RtlUnicodeStringCopyString(&_result, _T("[err]")); break;
	case CLogLevel::eInfo :   RtlUnicodeStringCopyString(&_result, _T("[inf]")); break;
	case CLogLevel::eWarning: RtlUnicodeStringCopyString(&_result, _T("[wrn]")); break;
	default:
		return (nt_status = STATUS_INVALID_PARAMETER);
	}

	return nt_status;
}

NTSTATUS   CLogFormat::_Time (const LARGE_INTEGER _time, UNICODE_STRING& _result) {

	NTSTATUS nt_status = STATUS_SUCCESS;

	if (NULL == _time.QuadPart)
		return (nt_status = STATUS_INVALID_PARAMETER);

	if (NULL == _result.Buffer ||
	    FALSE == CLogFormat_Spec::Compare(CLogFormat_Spec::eTimestampLen, _result)) {
		return (nt_status = STATUS_INVALID_PARAMETER);
	}

	WCHAR w_buf[CLogFormat_Spec::eTimestampLen] = {0};
	TIME_FIELDS timestamp_ = CSystemTime::Timestamp(_time);

	nt_status = ::RtlStringCchPrintfW(
		w_buf, CLogFormat_Spec::eTimestampLen, _T("[%d-%d-%d %d:%d:%d.%d]"),
		timestamp_.Year    ,
		timestamp_.Month   ,
		timestamp_.Day     ,
		timestamp_.Hour    ,
		timestamp_.Minute  ,
		timestamp_.Second  ,
		timestamp_.Milliseconds
	);
	if (NT_SUCCESS(nt_status)) {
		RtlAppendUnicodeToString(&_result, w_buf);
	}
	return nt_status;
}