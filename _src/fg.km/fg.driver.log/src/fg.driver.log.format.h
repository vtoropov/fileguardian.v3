#ifndef _FGDRIVERCOMMONLOGFORMAT_H_8755D3F1_A447_4460_9BB9_F3F2EBB5B1A8_INCLUDED
#define _FGDRIVERCOMMONLOGFORMAT_H_8755D3F1_A447_4460_9BB9_F3F2EBB5B1A8_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 26-May-2018 at 1:39:07p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian shared library common driver log data format interface declaration file.
*/
#include "fg.driver.log.defs.h"
#include "fg.driver.log.spec.h"

namespace shared { namespace km { namespace log { namespace _imp {

	class     CLogFormat {
	public:
		static NTSTATUS   Format(const LARGE_INTEGER _time , UNICODE_STRING& _result);
		static NTSTATUS   Format(const TLogRecordData& _dt , UNICODE_STRING& _line);
	protected:
		static NTSTATUS   _Func (const PWCHAR _fun_complete, UNICODE_STRING& _result);
		static NTSTATUS   _Level(const CLogLevel::_e _lvl  , UNICODE_STRING& _result);
		static NTSTATUS   _Time (const LARGE_INTEGER _time , UNICODE_STRING& _result);
	};

}}}}

#endif/*_FGDRIVERCOMMONLOGFORMAT_H_8755D3F1_A447_4460_9BB9_F3F2EBB5B1A8_INCLUDED*/