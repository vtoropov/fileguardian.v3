/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-May-2018 at 7:41:55p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian shared library common driver log writer interface implementation file.
*/
#include "StdAfx.h"
#include "fg.driver.log.h"
#include "fg.driver.log.writer.h"
#include "fg.driver.log.defs.h"
#include "fg.driver.log.rec.h"
#include "fg.driver.log.format.h"

using namespace shared::km::log;
using namespace shared::km::log::_imp;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace km { namespace log { namespace _imp { namespace details {

	volatile
	BOOLEAN&  CLogWriter_Accepted(void) {
		static volatile BOOLEAN m_accept = FALSE;
		return m_accept;
	}

	class     CLogWriter_File {
	public:
		static NTSTATUS   Close(void) {

			NTSTATUS nt_status = STATUS_SUCCESS;
			HANDLE&  handle_   = CLogWriter_File::_Handle();

			if (NULL != handle_) {
				nt_status = ::ZwClose(handle_); handle_ = NULL;
			}
			return nt_status;
		}

		static HANDLE     Handle(void) { return CLogWriter_File::_Handle(); }

		static NTSTATUS   Open(void) {
			NTSTATUS nt_status = STATUS_SUCCESS;

			DECLARE_UNICODE_STRING(path_, CLogDefault::eFilePathLen);

			if (NULL != CLogWriter_File::_Handle())
				return (nt_status = STATUS_ALREADY_COMPLETE);

			nt_status = CLogSettings::Path(path_);
			if (FALSE == NT_SUCCESS(nt_status))
				return nt_status;

			if (NULL == path_.Buffer)
				return (nt_status = STATUS_INVALID_PARAMETER);

			OBJECT_ATTRIBUTES atts_ = {0};
			InitializeObjectAttributes(
				&atts_, &path_, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, NULL
			);
			IO_STATUS_BLOCK io_status = {0};
			nt_status = ::ZwCreateFile(
				&CLogWriter_File::_Handle(),
				FILE_APPEND_DATA | SYNCHRONIZE,
				&atts_    ,
				&io_status,
				NULL      ,
				FILE_ATTRIBUTE_NORMAL,
				FILE_SHARE_READ      ,
				FILE_OPEN_IF         ,
				FILE_SYNCHRONOUS_IO_NONALERT | FILE_NON_DIRECTORY_FILE, NULL, 0
			);

			return nt_status;
		}

	private:
		static HANDLE&   _Handle(void) { static HANDLE m_file = NULL; return m_file; }
	};

	class     CLogWriter_Worker {
	private:
		enum _e {
			eAwaitNextStep = 100, // sleeping for specified period of time, in milliseconds, for processing next record, if any;
		};
	public:
		static NTSTATUS   Initialize(void) {

			NTSTATUS nt_status = STATUS_SUCCESS;
			HANDLE&  handle_   = CLogWriter_Worker::_Handle();

			if (NULL != handle_)
				return (nt_status = STATUS_ALREADY_INITIALIZED);

			details::CLogWriter_Accepted() = TRUE;
			nt_status = ::PsCreateSystemThread(
				&handle_, GENERIC_ALL, NULL, NULL, NULL, CLogWriter_Worker::_Func, NULL
			);

			return nt_status;
		}

		static NTSTATUS   Terminate (void) {

			NTSTATUS nt_status = STATUS_SUCCESS;
			HANDLE&  handle_   = CLogWriter_Worker::_Handle();

			if (NULL == handle_)
				return (nt_status = STATUS_ALREADY_COMPLETE);

			if (details::CLogWriter_Accepted() == TRUE) {
				details::CLogWriter_Accepted()  = FALSE;

				nt_status = ::ZwWaitForSingleObject(
					handle_, FALSE, NULL
				);
#ifdef DBG
				if (FALSE == NT_SUCCESS(nt_status)) { __fg_debug_break(); }
#endif
				nt_status = ::ZwClose(handle_); handle_ = NULL;
			}

			return nt_status;
		}

	private:
		static NTSTATUS   _Await(const ULONG _msec) {
			PAGED_CODE();

			LARGE_INTEGER wait_for_ = {0};
			wait_for_.QuadPart = RELATIVE(MILLISECONDS(_msec));

			return ::KeDelayExecutionThread(KernelMode, FALSE, &wait_for_);
		}

		static VOID _Func(PVOID pCtx) {

			NTSTATUS nt_status = STATUS_SUCCESS;
			PAGED_CODE();

			IO_STATUS_BLOCK io_block_ = {0};

			while (TRUE == details::CLogWriter_Accepted()) {

				PTLogRecordEntry entry_ = CRecordStorage::CRecords::Available();
				if (NULL != entry_) {
					DECLARE_UNICODE_STRING(
						line_, CLogFormat_Spec::uTotalLineLen
					);
					nt_status = CLogFormat::Format(entry_->m_data, line_);
					if (TRUE == NT_SUCCESS(nt_status)) {
						nt_status = ::ZwWriteFile(
								CLogWriter_File::Handle(),
								NULL, NULL, NULL, &io_block_, line_.Buffer, line_.Length, NULL, NULL
							);
					}
					nt_status = CRecordStorage::CRecords::Remove(entry_);
				}

				CLogWriter_Worker::_Await(CLogWriter_Worker::eAwaitNextStep);
			}

			if (NULL == pCtx) {
				PsTerminateSystemThread(nt_status);
				return;
			}
		}

		static HANDLE&   _Handle(void) { static HANDLE m_thread = NULL; return m_thread; }
	};

}}}}}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS   CLogWriter::Initialize(PDEVICE_OBJECT pDevice) {

	PAGED_CODE();

	NTSTATUS nt_status = details::CLogWriter_File::Open();
#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CLogWriter::Intialize(): open file status=%X;\n", nt_status
	);
#endif
	if (FALSE == NT_SUCCESS(nt_status)) goto __err_handler;
	//
	// TODO: it's necessary to check what actually occurs during receiving shutdown notification; 
	//
	if (NULL != pDevice) {
		nt_status = IoRegisterShutdownNotification(pDevice);
		if (FALSE == NT_SUCCESS(nt_status)) {
			CLogWriter::Terminate(pDevice);
			return nt_status;
		}
	}
	nt_status = details::CLogWriter_Worker::Initialize();
#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CLogWriter::Intialize(): worker start status=%X;\n", nt_status
	);
#endif	
__err_handler:
	if (FALSE == NT_SUCCESS(nt_status)) {
		CLogWriter::Terminate(pDevice);
		return nt_status;
	}

	return nt_status;
}

NTSTATUS   CLogWriter::Terminate (PDEVICE_OBJECT pDevice) {

	NTSTATUS nt_status = STATUS_SUCCESS;

	PAGED_CODE();

	nt_status = details::CLogWriter_Worker::Terminate();
	nt_status = details::CLogWriter_File::Close();

	if (NULL != pDevice) {
		IoUnregisterShutdownNotification(pDevice);
	}

	return nt_status;
}
/////////////////////////////////////////////////////////////////////////////