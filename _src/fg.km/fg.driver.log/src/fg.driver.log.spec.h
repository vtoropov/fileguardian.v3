#ifndef _FGDRIVERCOMMONLOGSPEC_H_18DDA5BB_BC5C_4399_B336_F6F6A3B2C799_INCLUDED
#define _FGDRIVERCOMMONLOGSPEC_H_18DDA5BB_BC5C_4399_B336_F6F6A3B2C799_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 26-May-2018 at 4:17:51p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian shared library common driver log data type declaration file.
*/

namespace shared { namespace km { namespace log { namespace _imp {

	class CLogRecord_Spec {
	public:
		//
		// TODO: zero terminator is not included yet (for some defs); 
		//
		enum _e {
			eFuncPathLen   = 0xFF,  // function name full path length of 255 characters;
			eDescriptLen   = 0xFF,  // record message/details/description length of 255 characters;
		};
	};

	typedef struct CLogRecord_Data {
		CLogLevel::_e      m_level;
		LARGE_INTEGER      m_timestamp;
		WCHAR              m_fun_path[CLogRecord_Spec::eFuncPathLen];
		WCHAR              m_descript[CLogRecord_Spec::eDescriptLen];
	}   TLogRecordData,
	  *PTLogRecordData;

	class CLogFormat_Spec {
	public:
		//
		// TODO: zero terminator is not included yet (for some defs); 
		//
		enum _e {
			eLevelLen      = 0x07,  // record level length of format [xxx], i.e. in 7 characters;
			eTimestampLen  = 0x1A,  // timestamp in format [yyyy-mm-dd hh:mm:ss.sss] (25 chars + \0);
			eFuncNameLen   = 0x40,  // function base name length of 64 characters like this {a-z}:;
			eDescriptLen   = 0xFF,  // record message/details/description length of 255 characters;
		};
	public:
		static const USHORT  uTotalLineLen = CLogFormat_Spec::eLevelLen
		                                   + CLogFormat_Spec::eTimestampLen
		                                   + CLogFormat_Spec::eFuncNameLen
	                                       + CLogRecord_Spec::eDescriptLen;
	public:
		static BOOLEAN   Compare(const ULONG _what, const UNICODE_STRING& _with) { // if _what equals to or less than _with.length, returns true;
			return (_what * sizeof(WCHAR) <= _with.MaximumLength);
		}
	};

}}}}

#endif/*_FGDRIVERCOMMONLOGSPEC_H_18DDA5BB_BC5C_4399_B336_F6F6A3B2C799_INCLUDED*/