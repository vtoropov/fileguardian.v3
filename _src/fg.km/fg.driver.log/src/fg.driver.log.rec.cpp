/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-May-2018 at 1:40:44p, UTC+7, Phuket, Rawai, Friday;
	This File Guardian shared library common driver log record interface implementation file.
*/
#include "StdAfx.h"
#include "fg.driver.log.rec.h"

using namespace shared::km::log;
using namespace shared::km::log::_imp;

#ifndef LIST_ALLOC
#define NPAGED_LOOKASIDE_LIST LIST_ALLOC
#endif

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace km { namespace log { namespace _imp { namespace details {

	TLogRecordList& CRecordStg_Data(void) {
		static TLogRecordList m_data;
		return m_data;
	}

	BOOLEAN&        CRecordStg_Inited(void) {
		static BOOLEAN m_inited = FALSE;
		return m_inited;
	}

	KSPIN_LOCK&     CRecordStg_Locker(void) {
		static KSPIN_LOCK m_locker;   // safe guard/locker object for sharing data;
		return m_locker;
	}

	class CRecordStg_Alloca {
	public:
		static NTSTATUS   Allocate(PTLogRecordEntry& _rec) {

			NTSTATUS nt_status = STATUS_SUCCESS;
			if (NULL != _rec)
				return (nt_status = STATUS_INVALID_PARAMETER);

			_rec = reinterpret_cast<PTLogRecordEntry>(ExAllocateFromNPagedLookasideList(
											&CRecordStg_Alloca::_Alloca()
										));
			if (NULL == _rec)
				return (nt_status = STATUS_INSUFFICIENT_RESOURCES);
			else
				RtlZeroMemory(_rec, sizeof(PTLogRecordEntry));

			return nt_status;
		}

		static NTSTATUS   Release(PTLogRecordEntry _rec) {

			NTSTATUS nt_status = STATUS_SUCCESS;
			if (NULL == _rec)
				return (nt_status = STATUS_INVALID_PARAMETER);

			ExFreeToNPagedLookasideList(
					&CRecordStg_Alloca::_Alloca(), _rec
				);
			return nt_status;
		}
	public:
		static NTSTATUS   Initialize(void) {
			NTSTATUS nt_status = STATUS_SUCCESS;

			ExInitializeNPagedLookasideList(
					&CRecordStg_Alloca::_Alloca(), NULL, NULL, 0, sizeof(TLogRecordEntry), (ULONG)'loga', 0
				);

			return nt_status;
		}

		static NTSTATUS   Terminate(void) {
			ExDeleteNPagedLookasideList(
				&CRecordStg_Alloca::_Alloca()
			);
			return STATUS_SUCCESS;
		}

	private:
		static LIST_ALLOC&  _Alloca(void) {
			static LIST_ALLOC m_alloca;
			return m_alloca;
		}
	};

}}}}}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS   CRecordStorage::Initialize(void) {

	NTSTATUS nt_status = STATUS_SUCCESS;

	if (TRUE == details::CRecordStg_Inited())
		return (nt_status = STATUS_ALREADY_COMPLETE);

	TLogRecordList& list_ = details::CRecordStg_Data();

	InitializeListHead(&list_.m_head);
	KeInitializeSpinLock(&details::CRecordStg_Locker());

	nt_status = details::CRecordStg_Alloca::Initialize();
	list_.m_allowed = CRecordStorage::eMaxRecAllowed;
	list_.m_current = NULL;

	details::CRecordStg_Inited() = NT_SUCCESS(nt_status);

	return nt_status;
}

NTSTATUS   CRecordStorage::Terminate (void) {

	NTSTATUS nt_status = STATUS_SUCCESS;

	if (FALSE == details::CRecordStg_Inited())
		return (nt_status = STATUS_ALREADY_COMPLETE);

	nt_status = CRecordStorage::CRecords::RemoveAll();
	nt_status = details::CRecordStg_Alloca::Terminate();

	details::CRecordStg_Inited() = FALSE;

	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

PTLogRecordEntry
           CRecordStorage::CRecords::Available(void) {
	KIRQL          prev_lvl = 0;
	PLIST_ENTRY      pIter_ = NULL;
	PTLogRecordEntry pEntry = NULL;
	TLogRecordList&   list_ = details::CRecordStg_Data();

	if (IsListEmpty(&list_.m_head))
		return pEntry;

	KeAcquireSpinLock(&details::CRecordStg_Locker(), &prev_lvl);
	pIter_ = ::RemoveHeadList(&list_.m_head);
	KeReleaseSpinLock(&details::CRecordStg_Locker(),  prev_lvl);

	if (NULL != pIter_) {
		pEntry = CONTAINING_RECORD(pIter_, TLogRecordEntry, m_index);
	}
	return pEntry;
}

NTSTATUS   CRecordStorage::CRecords::Append(
	const  CLogLevel::_e _lvl, const PUNICODE_STRING pwFunc, const PUNICODE_STRING pwDescript) {

	KIRQL          prev_lvl = 0;
	PTLogRecordEntry pEntry = NULL;

	NTSTATUS nt_status = details::CRecordStg_Alloca::Allocate(pEntry);
	if (FALSE == NT_SUCCESS(nt_status))
		return nt_status;

	// (0) assigns current local time
	LARGE_INTEGER sys_cur_  = {0};
	KeQuerySystemTime(&sys_cur_);
	ExSystemTimeToLocalTime(&sys_cur_, &pEntry->m_data.m_timestamp);

	// (1) assigns level;
	pEntry->m_data.m_level = _lvl;
	// (2) assigns function name is any;
	if (NULL != pwFunc &&
	    NULL != pwFunc->Buffer &&
	    pwFunc->Length < CLogRecord_Spec::eFuncPathLen)
	{
		RtlCopyMemory(
			pEntry->m_data.m_fun_path, pwFunc->Buffer, pwFunc->Length
		);
	}
	// (3) assign details/description if any
	if (NULL != pwDescript && 
	    NULL != pwDescript->Buffer && 
	    pwDescript->Length < CLogRecord_Spec::eDescriptLen)
	{
		RtlCopyMemory(
			pEntry->m_data.m_descript, pwDescript->Buffer, pwDescript->Length
		);
	}
	// (4) adds new record/entry to log list;
	TLogRecordList& list_ = details::CRecordStg_Data();

	InterlockedIncrement( &list_.m_current);
	KeAcquireSpinLock(&details::CRecordStg_Locker(), &prev_lvl);

	InsertTailList (&list_.m_head, &pEntry->m_index);
	KeReleaseSpinLock(&details::CRecordStg_Locker(),  prev_lvl);

	return nt_status;
}

NTSTATUS   CRecordStorage::CRecords::Normalize(void) {

	NTSTATUS      nt_status = STATUS_SUCCESS;
	KIRQL          prev_lvl = 0;
	PLIST_ENTRY       pIter = NULL;
	PTLogRecordEntry pEntry = NULL;

	KeAcquireSpinLock(&details::CRecordStg_Locker(), &prev_lvl);

	TLogRecordList& list_ = details::CRecordStg_Data();

	//
	// checks for list overflow; drops item(s) if necessary;
	// we need a room at least for one record entry;
	//

	while ((list_.m_allowed - list_.m_current) < 1){
		InterlockedDecrement(&list_.m_current);

		KeAcquireSpinLock(&details::CRecordStg_Locker(), &prev_lvl);

		pIter =  RemoveHeadList (&list_.m_head);

		KeReleaseSpinLock(&details::CRecordStg_Locker(),  prev_lvl);

		pEntry = CONTAINING_RECORD(pIter, TLogRecordEntry, m_index);
		details::CRecordStg_Alloca::Release(pEntry);
	}

	KeReleaseSpinLock(&details::CRecordStg_Locker(),  prev_lvl);

	return nt_status;
}

NTSTATUS   CRecordStorage::CRecords::Remove(PTLogRecordEntry _p_entry) {
	NTSTATUS nt_status = STATUS_SUCCESS;
	if (NULL == _p_entry) {
		return (nt_status = STATUS_INVALID_PARAMETER);
	}

	nt_status = details::CRecordStg_Alloca::Release(_p_entry);

	return nt_status;
}

NTSTATUS   CRecordStorage::CRecords::RemoveAll (void) {

	NTSTATUS nt_status = STATUS_SUCCESS;
	KIRQL     prev_lvl = 0;

	KeAcquireSpinLock(&details::CRecordStg_Locker(), &prev_lvl);

	PLIST_ENTRY      pHead    = &details::CRecordStg_Data().m_head;
	PLIST_ENTRY      pIter    = pHead->Flink;
	PTLogRecordEntry pEntry   = NULL;

	while(!IsListEmpty(pHead)) {
		pIter  = RemoveHeadList( pHead );
		KeReleaseSpinLock( &details::CRecordStg_Locker(), prev_lvl );

		pEntry = CONTAINING_RECORD(pIter, TLogRecordEntry, m_index);
		details::CRecordStg_Alloca::Release(pEntry);

		KeAcquireSpinLock(&details::CRecordStg_Locker(), &prev_lvl);
	}
	KeReleaseSpinLock( &details::CRecordStg_Locker(), prev_lvl );

	return nt_status;
}