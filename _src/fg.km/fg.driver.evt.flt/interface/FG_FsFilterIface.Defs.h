#ifndef _FGFSFILTERIFACEDEFS_H_7A37A5B7_13AD_4658_8ABD_736858FEA709_INCLUDED
#define _FGFSFILTERIFACEDEFS_H_7A37A5B7_13AD_4658_8ABD_736858FEA709_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-Apr-2018 at 00:00:24a, UTC+7, Novosibirsk, Rodniki, Tuesday;
	This is File Guardian kernel mode minifilter driver common interface declaration file.
*/

class eFsEventType{
public:
	enum _e {
		eFsEventUnknown   = 0x0000,              // 0 - is not included to count value;
		eFsEventCreated   = 0x0001,              // 1
		eFsEventCopied    = 0x0002,              // 2
		eFsEventAltered   = 0x0004,              // 3
		eFsEventRenamed   = 0x0008,              // 4
		eFsEventDeleted   = 0x0010,              // 5
		eFsEventMoved     = 0x0020,              // 6
		eFsEventReplaced  = 0x0040,              // 7
		eFsEventMovedToRecycle      = 0x0080,    // 8
		eFsEventRestoredFromRecycle = 0x0100,    // 9
	};
	static const INT nTypes = 9;
public:
	static INT        Count(void) { return eFsEventType::nTypes; }
	static INT        Index(const eFsEventType::_e _type) {
		switch(_type) {
			case eFsEventCreated   : return 0;
			case eFsEventCopied    : return 1;
			case eFsEventAltered   : return 2;
			case eFsEventRenamed   : return 3;
			case eFsEventDeleted   : return 4;
			case eFsEventMoved     : return 5;
			case eFsEventReplaced  : return 6;
			case eFsEventMovedToRecycle: return 7;
			case eFsEventRestoredFromRecycle: return 8;
		}
		return -1;
	}
	static  _e        Type (const INT nIndex) {
		switch (nIndex) {
			case 0: return eFsEventType::eFsEventCreated ;
			case 1: return eFsEventType::eFsEventCopied  ;
			case 2: return eFsEventType::eFsEventAltered ;
			case 3: return eFsEventType::eFsEventRenamed ;
			case 4: return eFsEventType::eFsEventDeleted ;
			case 5: return eFsEventType::eFsEventMoved   ;
			case 6: return eFsEventType::eFsEventReplaced;
			case 7: return eFsEventType::eFsEventMovedToRecycle;
			case 8: return eFsEventType::eFsEventRestoredFromRecycle;
		}
		return eFsEventType::eFsEventUnknown;
	}
};

#endif/*_FGFSFILTERIFACEDEFS_H_7A37A5B7_13AD_4658_8ABD_736858FEA709_INCLUDED*/