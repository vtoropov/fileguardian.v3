#ifndef _FGFSFILTERIFACE_H_1A6AD0DC_DE5A_40de_A139_885CEB3EB402_INCLUDED
#define _FGFSFILTERIFACE_H_1A6AD0DC_DE5A_40de_A139_885CEB3EB402_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Jun-2017 at 03:28:30a, UTC+7, Phuket, Rawai, Thursday;
	This is File Guardian file system minifilter driver interface declaration file.
*/
#include "FG_FsFilterIface.Defs.h"

#ifndef _MAX_DRIVE
#define _MAX_DRIVE    3
#endif

#ifndef _MAX_DRIVE_ALIGNED
#define _MAX_DRIVE_ALIGNED    4
#endif

#ifndef _MAX_DIR
#define _MAX_DIR    256
#endif

#ifndef _MAX_FNAME
#define _MAX_FNAME  256
#endif

#ifndef _MAX_EXT
#define _MAX_EXT    256
#endif

#ifndef _LONG_PATH
#define _LONG_PATH (_MAX_DIR + _MAX_FNAME + _MAX_DRIVE_ALIGNED + _MAX_EXT)
#endif
#define _COMP_PATH (_MAX_DIR + _MAX_FNAME)          // used for trancating paths if they are larger than 512 bytes
                                                    // to a form such as, drive_letter:\\...the_rest_of_the_path

//
//  Local definitions for passing parameters between the kernel and user mode
//
typedef ULONG_PTR FILE_ID;
typedef __success(return >= 0) LONG NTSTATUS;

//
//  actual information that is logged.
//
#ifndef CPLUSPLUS
#define CPLUSPLUS
#endif

#ifdef CPLUSPLUS
#else

typedef enum {
		eFsEventUnknown   = 0x0000,
		eFsEventCreated   = 0x0001,
		eFsEventCopied    = 0x0002,
		eFsEventAltered   = 0x0004,
		eFsEventRenamed   = 0x0008,
		eFsEventDeleted   = 0x0010,
		eFsEventMoved     = 0x0020,
		eFsEventReplaced  = 0x0040,
		eFsEventMovedToRecycle      = 0x0080,
		eFsEventRestoredFromRecycle = 0x0100,
} eFsEventType;

#endif

#ifdef CPLUSPLUS
class eFsObjectType{
public:
	enum _e {
		eFsUnknownType    = 0x00,
		eFsFileObject     = 0x01,
		eFsFolderObject   = 0x02,
		eFsStreamObject   = 0x03,
	};
};
#else

typedef enum {
	eFsUnknownType    = 0x00,
	eFsFileObject     = 0x01,
	eFsFolderObject   = 0x02,
	eFsStreamObject   = 0x03,
} eFsObjectType;

#endif

//
// Tech_dog:
//    The object file path buffer has been changed from dynamic allocation to fixed size;
//    such approach consumes more memory, but simplifies pointer arithmetic;
//

typedef struct _t_fs_record
{
	ULONG uFsEventType;               // predefined event type
	ULONG uFsObjectType;              // predefined file system object type

	LARGE_INTEGER OriginatingTime;
	LARGE_INTEGER CompletionTime;

	LARGE_INTEGER liSize;
	ULONG uProcId;                    // identifier on a process, which initiates an event;

	WCHAR SourceObject[_COMP_PATH];   // This is a null terminated unicode string
	WCHAR TargetObject[_COMP_PATH];   // This is a null terminated unicode string
}
TFs_Record, *PTFs_Record;

#define _LOG_RECORD_BUFFER_CAP   100  // defines user mode buffer capacity

//
// structure of user mode application buffer for data exchange; 
//
typedef struct _t_fs_rec_buffer
{
	ULONG        uRecCount;           // the counter of the records: on driver input the counter indicates how many records a buffer can accomodate;
	                                  //                             on driver output the counter contains actual number of records in the buffer;
	PTFs_Record  pRecords;            // points to the records' buffer that is allocated on the client side;
}
TFs_Buffer, *PTFs_Buffer;

//
//  Defines the commands between the user mode app and the filter
//

class eFsCommandType {
	//https://stackoverflow.com/questions/5084542/forcing-enum-to-be-of-unsigned-long-type
public:
	enum _e : ULONG {
		eNone            = 0x0000, // command type is not defined;
		eGetEvents       = 0x0001, // command type for getting file events;
		eUpdateFilter    = 0x0002, // command type for updating filter from registry;
		eRemovable       = 0x0004, // command type for adding/deleting removable path;
		eLogFilePath     = 0x0008, // command type for get/set log file absolute path;
		eLogOptions      = 0x0010, // command type for modifying log options;
		eLogVerbose      = 0x0020, // command type for modifying log verbose level;
		eRemovableAppend = 0x0040, // this is old command type, it is actual for this time;
		eRemovableDelete = 0x0080, // this is old command type, it is actual for this time;
		eComparePath     = 0x0100, // this command type for making a comparison of folder paths provided;
	};
};

class eFsCommandOpt {
public:
	enum _e : ULONG {
		eWDataLen = _COMP_PATH,
	};
};

//
//  Defines a command structure between a user mode app and a filter;
//  command structure can be used in both roles simultaneously: as input and output buffer;
//
typedef struct _t_fs_command
{
	eFsCommandType::_e
	          eCmdType;           // acts as ULONG data type, please see the definition of the enumeration;
	ULONG     nFlags;             // data buffer size in bytes deprecated, because a buffer has a fixed size; this member is used for command flags now;
	ULONG     nNumeric;           // alignment on IA64 is removed from supporting; numeric data can be exchanged now through this member;
	WCHAR     wData[_COMP_PATH];  // this is a null terminated unicode string; the fixed length can be changed in the following versions;
	ULONG     nDataLen;           // size of data in bytes;
}
 TFs_Command, *PTFs_Command;

#define DriverPortName _T("\\Fg_fs_filter")

#endif/*_FGFSFILTERIFACE_H_1A6AD0DC_DE5A_40de_A139_885CEB3EB402_INCLUDED*/