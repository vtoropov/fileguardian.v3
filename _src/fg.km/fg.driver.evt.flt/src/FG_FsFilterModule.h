#ifndef _FGFSFILTERMODULE_H_DD0894F4_8E36_4110_9EA7_2A43DBD66545_INCLUDED
#define _FGFSFILTERMODULE_H_DD0894F4_8E36_4110_9EA7_2A43DBD66545_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Jun-2017 at 11:13:57p, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian mini-filter driver module declaration file.
*/
EXTERN_C DRIVER_INITIALIZE DriverEntry;

// does not work and leads to a lot of linking errors (in Win32 configuration);
// #pragma comment(lib, "fltMgr.lib")
/////////////////////////////////////////////////////////////////////////////

#if (0)
EXTERN_C static NTSTATUS FLTAPI DriverInstanceSetup (
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects,
	__in        FLT_INSTANCE_SETUP_FLAGS  _uFlags     ,
	__in        DEVICE_TYPE               _uDeviceType,
	__in        FLT_FILESYSTEM_TYPE       _uFileSysType
	);

EXTERN_C static VOID FLTAPI DriverTeardownComplete (
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects,
	__in        ULONG                     _uFlags
	);
#endif
EXTERN_C static NTSTATUS FLTAPI DriverUnload(
	__in        ULONG                     _uFlags
);

#pragma alloc_text(PAGE, DriverEntry)
#if (0)
#pragma alloc_text(PAGE, DriverTeardownComplete)
#endif
#pragma alloc_text(PAGE, DriverUnload)

#endif/*_FGFSFILTERMODULE_H_DD0894F4_8E36_4110_9EA7_2A43DBD66545_INCLUDED*/