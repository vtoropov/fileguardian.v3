/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jul-2017 at 2:55:48p, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian minifilter driver FS (file system) event logger interface implementation file.
*/
#include "StdAfx.h"
#include "fg.fs.filter.event.tracker.h"
#include "fg.fs.filter.event.rec.proc.h"
#include "FG_FsCtxIface.h"

using namespace fg::filter::_imp;
/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace filter { namespace _imp { namespace details {
	/*
		This routine has been written to convert a transaction notification code
		to an Irp minor code. This function is needed because RECORD_DATA has a
		UCHAR field for the Irp minor code whereas TxNotification is ULONG. As
		of now all this function does is compute log_base_2(TxNotification) + 1.
		This function is intricately tied with the enumeration TRANSACTION_NOTIFICATION_CODES
	*/
	UCHAR TxNotificationToMinorCode (
		__in ULONG _code
		)
	{
		UCHAR uCount = 0;

		if (!_code)
			return uCount;
		//
		//  This condition verifies if no more than one flag is set
		//  in the notification code. TxNotification flags are
		//  supposed to be mutually exclusive. The condition below verifies
		//  if the value of TxNotification is a power of 2. If it is not
		//  then we will break the execution.
		//
		if (( _code ) & ( _code - 1 ))
			return uCount;

		while (_code)
		{
			uCount += 1;
			_code >>= 1;
		}
		return uCount;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CEvtTracker::FillUserBuffer (
	__inout PTFs_Buffer _pUserBuffer
	)
{
	static
	TFs_Evt_Storage& stg_  = global::EventStorage();
	NTSTATUS     status_   = STATUS_NO_MORE_ENTRIES;
	PTFs_Evt_Record pEntry = NULL;
	ULONG         lCount   = 0; 

	if (NULL == _pUserBuffer)
		return ( status_ = STATUS_INVALID_PARAMETER );
	else {
		lCount = _pUserBuffer->uRecCount;
		_pUserBuffer->uRecCount = 0;      // this counter is erased and will contain the number of processed events;
	}

	for (ULONG l_ = 0; l_ < lCount; l_++){

		pEntry = CRecManager::GetFirstEntry();
		if (NULL == pEntry)
			return status_;

		_pUserBuffer->uRecCount += 1;
		CRecAllocator::CloneData(
				pEntry->Data, _pUserBuffer->pRecords[l_]
			);

			CRecAllocator::FreeEntry(pEntry);
	}
	if (_pUserBuffer->uRecCount > 0) // some entries have been processed;
		status_ = STATUS_SUCCESS;

	return status_;
}

/////////////////////////////////////////////////////////////////////////////

VOID
CEvtTracker::LogPostOperationData (
	__in PFLT_CALLBACK_DATA _pData,
	__inout TFs_Record&     _tRecord
	)
{
	UNUSED(_pData);
	_tRecord.CompletionTime = ctx::CContextTime::GetCurrentTimestamp();
}

VOID
CEvtTracker::LogPreOperationData (
	__in PFLT_CALLBACK_DATA    _pData,
	__in PCFLT_RELATED_OBJECTS _pFltObjects,
	__inout TFs_Record&        _tRecord
	)
{
	UNUSED(_pData);
	UNUSED(_pFltObjects);
	_tRecord.OriginatingTime = ctx::CContextTime::GetCurrentTimestamp();
}

VOID
CEvtTracker::LogTransactionNotify (
	__in PCFLT_RELATED_OBJECTS _pFltObjects,
	__inout TFs_Record&        _tRecord    ,
	__in ULONG                 _uNotifyCode
	)
{
	UNUSED(_pFltObjects);
	UNUSED(_uNotifyCode);
	_tRecord.OriginatingTime = ctx::CContextTime::GetCurrentTimestamp();
}