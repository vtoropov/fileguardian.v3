#ifndef _FGFSRECORDPROC_H_3B1C8722_FC82_47d5_8389_8E535798D4D2_INCLUDED
#define _FGFSRECORDPROC_H_3B1C8722_FC82_47d5_8389_8E535798D4D2_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Jul-2017 at 12:15:04p, UTC+7, Phuket, Rawai, Monday;
	This is File Guardian minifilter driver event record processing interface declaration file.
*/
#include "FG_FsFilterIface.h"

typedef struct _t_fs_evt_record
{
	LIST_ENTRY      Index;                // index of record entry in the list;
	TFs_Record      Data;
}
TFs_Evt_Record, *PTFs_Evt_Record;

typedef struct _t_fs_evt_storage
{
	LIST_HEAD       EntryHead;            // head of the list of record entries;
	KSPIN_LOCK      pListLock;            // lock object of record entry lists;
	LIST_ALLOC      EntryAllocator;       // lookaside list used for allocating list items;
	LONG            nMaxRecords;          // maximum records allowed to keep in the storage;
	__volatile LONG nRecAllocated;        // currently aloccated records to track buffer overflow;
	ULONG           uNameQueryMethod;     // the name query method to use; by default is FLT_FILE_NAME_QUERY_ALWAYS_ALLOW_CACHE_LOOKUP;
}
TFs_Evt_Storage, *PTFs_Evt_Storage;

namespace global
{
	TFs_Evt_Storage&   EventStorage(void);
}

#define DEFAULT_REC_STORAGE_CAP    1000
#define DEFAULT_NAME_QUERY_METHOD  FLT_FILE_NAME_QUERY_ALWAYS_ALLOW_CACHE_LOOKUP
/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace filter { namespace _imp {

	class CRecAllocator {
	public:
		//
		// allocates a record entry
		//
		static
		PTFs_Evt_Record
		AllocateEntry(
			VOID
			);
		//
		// clones one entry to other;
		//
		static
		BOOL
		CloneData(
			__in CONST TFs_Record& _tSource,
			__inout    TFs_Record& _tTarget
			);
		//
		// creates record entry from record data provider;
		//
		static
		PTFs_Evt_Record
		CreateEntryFromData(
			__in CONST TFs_Record& _tData
			);
		//
		// frees a record entry
		//
		static
		VOID
		FreeEntry(
			__in PTFs_Evt_Record _pEntry
			);
	};

	class CRecManager{
	public:
		//
		// appends an entry to the record list; if the list reaches the maximum capacity,
		// the head item(s) will be discarded;
		//
		static
		BOOL
		AppendEntry(
			__in PTFs_Evt_Record _pEntry
			);
		//
		// creates/initializes global record entry list and sync access object(s);
		// this routine must be called from driver entry;
		//
		static
		VOID
		CreateStorage(
			VOID
			);
		//
		// destroys global record entry list;
		// this procedure must be invoked in driver unload routine;
		//
		static
		VOID
		DestroyStorage(
			VOID
			);
		//
		// gets the first available entry; if record list is empty or error occurs, NULL is returned;
		// a caller is responsible for destroying the entry object that is received through this method;
		//
		static
		PTFs_Evt_Record
		GetFirstEntry(
			VOID
			);
		//
		// clears record entry list
		//
		static
		VOID
		RemoveAllEntries(
			__in LIST_HEAD&   _list,         // list reference to deal with
			__in KSPIN_LOCK&  _lock          // list access sync object reference
			);
		//
		// sets file system source object name/path from string provided;
		//
		static
		BOOL
		SetEntrySource(
			__in PTFs_Evt_Record _pEntry,
			__in PUNICODE_STRING _pPathOrName
			);
		//
		// sets file system target object name/path from string provided;
		//
		static
		BOOL
		SetEntryTarget(
			__in PTFs_Evt_Record _pEntry,
			__in PUNICODE_STRING _pPathOrName
			);
	};
}}}

#endif/*_FGFSRECORDPROC_H_3B1C8722_FC82_47d5_838y9_8E535798D4D2_INCLUDED*/