/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Jul-2017 at 1:16:31p, UTC+7, Phuket, Rawai, Monday;
	This is File Guardian minifilter driver log record processing interface implementation file.
*/
#include "StdAfx.h"
#include "fg.fs.filter.event.rec.proc.h"

static const ULONG FG_ENTRY_TAG_NAME = 'fgnt';

/////////////////////////////////////////////////////////////////////////////

namespace global
{
	TFs_Evt_Storage&   EventStorage(void)
	{
		static TFs_Evt_Storage stg;
		return stg;
	}
}

using namespace fg::filter::_imp;
/////////////////////////////////////////////////////////////////////////////

PTFs_Evt_Record
CRecAllocator::AllocateEntry(
	VOID
	)
{
	static TFs_Evt_Storage& stg_ = global::EventStorage();

	PTFs_Evt_Record pEntry = (PTFs_Evt_Record)ExAllocateFromNPagedLookasideList(&stg_.EntryAllocator);
	if (NULL == pEntry) {
#if DBG
		DbgPrint(
			"__fg_fs: [ERROR] RecAllocateEntry::alloc(): record entry allocation failed;\n"
			);
#endif
	}
	else {
		RtlZeroMemory( pEntry, sizeof( TFs_Evt_Record ) );
	}

	return pEntry;
}

BOOL
CRecAllocator::CloneData(
	__in CONST TFs_Record& _tSource,
	__inout    TFs_Record& _tTarget
	)
{
	_tTarget.CompletionTime  = _tSource.CompletionTime;
	_tTarget.OriginatingTime = _tSource.OriginatingTime;
	_tTarget.uFsEventType    = _tSource.uFsEventType;
	_tTarget.uFsObjectType   = _tSource.uFsObjectType;
	_tTarget.liSize          = _tSource.liSize;
	_tTarget.uProcId         = _tSource.uProcId;

	RtlCopyMemory(&_tTarget.SourceObject[0], &_tSource.SourceObject[0], _COMP_PATH);
	RtlCopyMemory(&_tTarget.TargetObject[0], &_tSource.TargetObject[0], _COMP_PATH);

	return TRUE;
}

PTFs_Evt_Record
CRecAllocator::CreateEntryFromData(
	__in CONST TFs_Record& _tData
	)
{
	PTFs_Evt_Record pEntry = CRecAllocator::AllocateEntry();
	if (NULL != pEntry){
		CRecAllocator::CloneData(_tData, pEntry->Data);
	}

	return pEntry;
}

VOID
CRecAllocator::FreeEntry(
	__in PTFs_Evt_Record _pEntry
	)
{
	static TFs_Evt_Storage& stg_ = global::EventStorage();

	if (NULL == _pEntry)
		return;
	ExFreeToNPagedLookasideList(&stg_.EntryAllocator, _pEntry);
}

/////////////////////////////////////////////////////////////////////////////

BOOL
CRecManager::AppendEntry(
	__in PTFs_Evt_Record _pEntry
	)
{
	BOOL    b_result       = FALSE;
	KIRQL   prevIrqLevel   = 0;
	PLIST_ENTRY    pIter   = NULL;
	PTFs_Evt_Record pEntry = NULL;

	static TFs_Evt_Storage& stg_ = global::EventStorage();

	if (NULL == _pEntry)
		return b_result;

	//
	// checks for list overflow; drops item(s) if necessary;
	// we need a room at least for one record entry;
	//

	while ((stg_.nMaxRecords - stg_.nRecAllocated) < 1){
		InterlockedDecrement( &stg_.nRecAllocated);

		KeAcquireSpinLock(&stg_.pListLock, &prevIrqLevel);

		pIter =  RemoveHeadList (&stg_.EntryHead);

		KeReleaseSpinLock(&stg_.pListLock,  prevIrqLevel);

		pEntry = CONTAINING_RECORD(pIter, TFs_Evt_Record, Index);
		CRecAllocator::FreeEntry(pEntry);
	}

	//
	// adds new entry to the list
	//
	InterlockedIncrement( &stg_.nRecAllocated);
	KeAcquireSpinLock(&stg_.pListLock, &prevIrqLevel);

	InsertTailList (&stg_.EntryHead, &_pEntry->Index);
	KeReleaseSpinLock(&stg_.pListLock,  prevIrqLevel);

	b_result = TRUE;

	return b_result;
}

VOID
CRecManager::CreateStorage(
	VOID
	)
{
	TFs_Evt_Storage& stg_ = global::EventStorage();

	InitializeListHead  ( &stg_.EntryHead );
	KeInitializeSpinLock( &stg_.pListLock );

	ExInitializeNPagedLookasideList(
				&stg_.EntryAllocator,
				NULL,
				NULL,
				0,
				sizeof(TFs_Evt_Record),
				FG_ENTRY_TAG_NAME   ,
				0
			);
	stg_.nMaxRecords   = DEFAULT_REC_STORAGE_CAP;
	stg_.nRecAllocated = 0;
	stg_.uNameQueryMethod = DEFAULT_NAME_QUERY_METHOD;
}

VOID
CRecManager::DestroyStorage(
	VOID
	)
{
	TFs_Evt_Storage& stg_ = global::EventStorage();

	CRecManager::RemoveAllEntries(stg_.EntryHead, stg_.pListLock);

	ExDeleteNPagedLookasideList( &stg_.EntryAllocator );
}

PTFs_Evt_Record
CRecManager::GetFirstEntry(
	VOID
	)
{
	static
	TFs_Evt_Storage& stg_  = global::EventStorage();
	PTFs_Evt_Record pEntry = NULL;
	PLIST_ENTRY   pIter_   = NULL;
	KIRQL   prevIrqLevel   = 0;

	if (IsListEmpty(&stg_.EntryHead))
		return pEntry;

	InterlockedDecrement( &stg_.nRecAllocated);
	KeAcquireSpinLock(&stg_.pListLock, &prevIrqLevel);

	pIter_ = RemoveTailList(&stg_.EntryHead);
	KeReleaseSpinLock(&stg_.pListLock,  prevIrqLevel);

	if (NULL == pIter_)
		return pEntry;

	pEntry = CONTAINING_RECORD(pIter_, TFs_Evt_Record, Index);
	return pEntry;
}

VOID
CRecManager::RemoveAllEntries(
	__in LIST_HEAD&   _list,         // list reference to deal with
	__in KSPIN_LOCK&  _lock          // list access sync object reference
	)
{
	PLIST_ENTRY    pHead    = &_list;
	PLIST_ENTRY    pIter    = pHead->Flink;
	PTFs_Evt_Record  pEntry = NULL;
	KIRQL    prevIrqLevel   = 0;

	KeAcquireSpinLock(&_lock, & prevIrqLevel);

	while(!IsListEmpty(pHead))
	{
		pIter  = RemoveHeadList( pHead );
		KeReleaseSpinLock( &_lock, prevIrqLevel );

		pEntry = CONTAINING_RECORD(pIter, TFs_Evt_Record, Index);
		CRecAllocator::FreeEntry(pEntry);
		KeAcquireSpinLock(&_lock, & prevIrqLevel);
	}

	KeReleaseSpinLock( &_lock, prevIrqLevel );
}

BOOL
CRecManager::SetEntrySource(
	__in PTFs_Evt_Record _pEntry,
	__in PUNICODE_STRING _pPathOrName
	)
{
	if (NULL == _pEntry || NULL == _pPathOrName)
		return FALSE;

	RtlCopyMemory(
		&_pEntry->Data.SourceObject[0], _pPathOrName->Buffer, _pPathOrName->Length
	);

	return TRUE;
}

BOOL
CRecManager::SetEntryTarget(
	__in PTFs_Evt_Record _pEntry,
	__in PUNICODE_STRING _pPathOrName
	)
{
	if (NULL == _pEntry || NULL == _pPathOrName)
		return FALSE;

	RtlCopyMemory(
		&_pEntry->Data.TargetObject[0], _pPathOrName->Buffer, _pPathOrName->Length
	);

	return TRUE;
}