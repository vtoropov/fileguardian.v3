#ifndef _FGFSEVENTLOGGER_H_3B1C8722_FC82_47d5_8389_8E535798D4D2_INCLUDED
#define _FGFSEVENTLOGGER_H_3B1C8722_FC82_47d5_8389_8E535798D4D2_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jul-2017 at 2:44:19p, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian minifilter driver FS (file system) event logger interface declaration file.
*/
#include "FG_FsFilterIface.h"

namespace fg { namespace filter { namespace _imp {

	class CEvtTracker {
	public:
	/*
	Routine Description:
		This function fills OutputBuffer with as many LOG_RECORDs as possible.
		LOG_RECORDs are variable sizes and are tightly packed in the OutputBuffer.
		NOTE:  This code must be NON-PAGED because it uses a spin-lock.

	Arguments:
		pOutputBuffer             - The user's buffer to fill with the log data we have collected;
		nOutputBufferLength       - The size in bytes of OutputBuffer;
		pReturnOutputBufferLength - The amount of data actually written into the OutputBuffer.
	*/
	static
	NTSTATUS
	FillUserBuffer (
		__inout PTFs_Buffer _pUserBuffer
		);

	/*
		This is called from the post-operation callback routine to copy the
		necessary information into the log record.
		NOTE:  This code must be NON-PAGED because it can be called on the
		       paging path or at DPC level.
	*/
	static
	VOID
	LogPostOperationData (
		__in PFLT_CALLBACK_DATA _pData,
		__inout TFs_Record&     _tRecord
		);

	/*
		This is called from the pre-operation callback routine to copy the
		necessary information into the log record.
		NOTE:  This code must be NON-PAGED because it can be called on the
		       paging path.
	*/
	static
	VOID
	LogPreOperationData (
		__in PFLT_CALLBACK_DATA    _pData,
		__in PCFLT_RELATED_OBJECTS _pFltObjects,
		__inout TFs_Record&        _tRecord
		);

	/*
		This routine logs the transaction notification;
	*/
	static
	VOID
	LogTransactionNotify (
		__in PCFLT_RELATED_OBJECTS _pFltObjects,
		__inout TFs_Record&        _tRecord    ,
		__in ULONG                 _uNotifyCode
		);
	};
}}}

#endif/*_FGFSEVENTLOGGER_H_3B1C8722_FC82_47d5_8389_8E535798D4D2_INCLUDED*/