#ifndef _STDAFX_H_4BE97360_F93F_4e5e_BBA9_1131455A19E1_INCLUDED
#define _STDAFX_H_4BE97360_F93F_4e5e_BBA9_1131455A19E1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Jun-2016 at 10:47:59pm, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian file system minifilter driver precompiled headers definition file.
*/

#ifndef WINVER                 // Specifies that the minimum required platform is Windows Vista.
#define WINVER         0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT           // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINNT   0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINDOWS         // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINDOWS 0x0600  // Change this to the appropriate value to target other platforms.
#endif

extern "C"
{
#pragma warning(push, 0)
	#include <fltKernel.h>
	#include <dontuse.h>
	#include <suppress.h>
	#include <Wdmsec.h>
	#include <windef.h>
	#include <ntimage.h>
	#include <stdarg.h>
	#define NTSTRSAFE_NO_CB_FUNCTIONS
	#include <ntstrsafe.h>
	#include <FltUserStructures.h>
#pragma warning(pop)
}

#pragma prefast(disable:__WARNING_ENCODE_MEMBER_FUNCTION_POINTER, "Not valid for kernel mode drivers")
/////////////////////////////////////////////////////////////////////////////
//
// macro utilities
//

#ifndef Add2Ptr
#define Add2Ptr(P,I)   ((PVOID)((PUCHAR)(P) + (I)))
#endif

#ifndef FlagOnAll
#define FlagOnAll( F, T )  \
    (FlagOn( F, T ) == T)
#endif

#ifndef SetFlagInterlocked
#define SetFlagInterlocked(_ptrFlags,_flagToSet) \
    ((VOID)InterlockedOr(((volatile LONG*)(_ptrFlags)),_flagToSet))
#endif

#define PAGE_CODE_EX(_return)                            \
		if (PASSIVE_LEVEL != KeGetCurrentIrql()) {       \
			return _return;                              \
		}

/////////////////////////////////////////////////////////////////////////////
//
// constants and structures
//

#define UNUSED(x) (void)(x)

typedef LIST_ENTRY             LIST_HEAD;
typedef NPAGED_LOOKASIDE_LIST  LIST_ALLOC;

#define LIST_FOR_EACH_SAFE(pCurr  , pNext, pHead) \
		for (pCurr = pHead->Flink , pNext = pCurr->Flink ; pCurr != pHead; \
		     pCurr = pNext        , pNext = pCurr->Flink)

typedef struct _DRIVER_DATA
{    
	PDRIVER_OBJECT   pDriverObject ;     // the object that identifies this driver
	PFLT_FILTER      pFilter       ;     // filter handle that results from a call to FltRegisterFilter
	PFLT_PORT        pServerPort   ;     // listens for incoming connection(s)
	PEPROCESS        pUserProcess  ;     // user(mode) process that connected to the server port
	PFLT_PORT        pClientPort   ;     // client port for a connection to user-mode
	ULONG            uDebugFlags   ;     // global debug flags;
}
TDriverData, *PTDriverData;

#define CPLUSPLUS

#ifdef CPLUSPLUS
namespace global
{
	TDriverData&     DriverData(void);
}
#else
	PTDriverData     DriverDataPtr(void);
#endif

/////////////////////////////////////////////////////////////////////////////
//
// helper(s)
//

#ifdef CPLUSPLUS

//
//	pragma does not work as expacted; these libraries must be added throu Linker>>Input>>Additional Dependencies;
//

#pragma comment (lib, "fg.driver.common_v15.lib")
#pragma comment (lib, "fg.driver.evt.cmp_v15.lib")
#pragma comment (lib, "fg.driver.log_v15.lib")

#include "fg.driver.common.str.h"

namespace utils
{
	typedef shared::km::aux::CStringW CStringW;
}
#else

	VOID     CStringW_Initialize(PUNICODE_STRING);
	VOID     CStringW_Terminate (PUNICODE_STRING);
	NTSTATUS CStringW_Allocate  (PUNICODE_STRING, const USHORT _szBytes);
	BOOLEAN  CStringW_IsValid   (PUNICODE_STRING  CONST );
	NTSTATUS CStringW_CopyFrom  (PUNICODE_STRING, CONST WCHAR* lpszSource, CONST UINT uLength);
	NTSTATUS CStringW_Copy      (PUNICODE_STRING, CONST PUNICODE_STRING _pSource);
	NTSTATUS CStringW_CopyTo    (PUNICODE_STRING  CONST , PUNICODE_STRING _pTo  );
	INT      CStringW_Find      (PUNICODE_STRING _pWhere, PUNICODE_STRING _pWhat);

#endif

#endif/*_STDAFX_H_4BE97360_F93F_4e5e_BBA9_1131455A19E1_INCLUDED*/