/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Jun-2016 at 10:59:34p, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian file system minifilter driver precompiled headers implementation file.
*/
#include "StdAfx.h"

#ifdef CPLUSPLUS
namespace global
{
	TDriverData&   DriverData(void)
	{
		static TDriverData drv_data = {0};
		return drv_data;
	}
}
#else
	PTDriverData     DriverDataPtr(void)
	{
		static TDriverData drv_data = {0};
		return &drv_data;
	}
#endif

#define AUX_STRING_POOL_TAG  'auxs'

#ifdef CPLUSPLUS

	using namespace utils;

#else

	VOID     CStringW_Initialize(PUNICODE_STRING _pString)
	{
		if (NULL != _pString){
			(*_pString).Buffer = NULL; (*_pString).Length = (*_pString).MaximumLength = 0;
		}
	}

	VOID     CStringW_Terminate (PUNICODE_STRING _pString)
	{
		PAGED_CODE();
		if (NULL != _pString){

			_pString->Length = 0;

			if ( NULL != _pString->Buffer )
			{
				ExFreePool( _pString->Buffer );
				_pString->Buffer        = NULL;
				_pString->MaximumLength = 0;
			}
		}
	}

	NTSTATUS CStringW_Allocate  (PUNICODE_STRING _pString, const USHORT _szBytes)
	{
		PAGED_CODE();

		if (NULL == _pString)
			return STATUS_INVALID_PARAMETER;

		if (!_pString->MaximumLength)
			return STATUS_INVALID_PARAMETER;

		_pString->Length = 0;
		_pString->MaximumLength = _szBytes;
		_pString->Buffer = (PWCH)ExAllocatePoolWithTag(
								PagedPool,
								_pString->MaximumLength,
								AUX_STRING_POOL_TAG
							);
		if (NULL == _pString->Buffer)
			return STATUS_INSUFFICIENT_RESOURCES;

		RtlZeroMemory(
				_pString->Buffer, _pString->MaximumLength
			);
		return STATUS_SUCCESS;
	}

	BOOLEAN  CStringW_IsValid   (PUNICODE_STRING CONST _pString)
	{
		return (NULL != _pString && NULL != _pString->Buffer);
	}

	NTSTATUS CStringW_CopyFrom  (PUNICODE_STRING _pString, CONST WCHAR* lpszSource, CONST UINT uLength)
	{
		NTSTATUS nt_status = STATUS_SUCCESS;

		if (NULL == _pString)
			return (nt_status = STATUS_INVALID_PARAMETER);

		if (NULL == lpszSource || uLength < 1)
			return (nt_status = STATUS_INVALID_PARAMETER);

		__try
		{

			nt_status = CStringW_Allocate(_pString, (USHORT)uLength * sizeof(WCHAR));
			if (!NT_SUCCESS(nt_status))
				return nt_status;

			RtlCopyMemory(_pString->Buffer, lpszSource, uLength  * sizeof(WCHAR));

		}
		__except (EXCEPTION_EXECUTE_HANDLER) {
			nt_status = GetExceptionCode();
		}

		return  nt_status;
	}

	NTSTATUS CStringW_Copy      (PUNICODE_STRING _pString, CONST PUNICODE_STRING _pSource)
	{
		NTSTATUS nt_status = STATUS_SUCCESS;

		if (NULL == _pString)
			return (nt_status = STATUS_INVALID_PARAMETER);
		if (NULL == _pSource)
			return (nt_status = STATUS_INVALID_PARAMETER);

		nt_status = CStringW_Allocate(_pString, _pSource->Length + sizeof(WCHAR));
		if (!NT_SUCCESS(nt_status))
			return nt_status;

		RtlCopyUnicodeString(_pString, _pSource);

		return nt_status;
	}

	NTSTATUS CStringW_CopyTo  (PUNICODE_STRING  CONST _pString, PUNICODE_STRING _pTo  )
	{
		if (!CStringW_IsValid(_pString))
			return STATUS_INVALID_PARAMETER;

		if (NULL == _pTo)
			return STATUS_INVALID_PARAMETER;

		RtlCopyUnicodeString(_pString, _pTo);
		return STATUS_SUCCESS;
	}

	INT      CStringW_Find    (PUNICODE_STRING _pWhere, PUNICODE_STRING _pWhat)
	{
		BOOL bMatched = FALSE;
		INT n_ptr =  0;         // a pointer of current character in target (what ) string
		INT n_itr =  0;         // a pointer of current character in source (where) string
		INT n_pos = -1;
		INT n_src =  0;
		INT n_tgt =  0;

#ifdef _DEBUG
		WCHAR wSrc = 0;
		WCHAR wTgt = 0;
#endif

		if (NULL == _pWhere ||
			NULL == _pWhat )
			return n_pos;

		n_src = _pWhere->Length / sizeof(WCHAR);
		n_tgt = _pWhat->Length  / sizeof(WCHAR);

		if (_pWhat->Length > _pWhere->Length)
			return n_pos;

		while (n_itr < n_src &&
			   n_ptr < n_tgt)
		{
#ifdef _DEBUG
			wSrc = _pWhere->Buffer[n_itr];
			wTgt = _pWhat->Buffer[n_ptr];
#endif
			bMatched = (_pWhere->Buffer[n_itr] == _pWhat->Buffer[n_ptr]);

			if (bMatched){
				if (n_pos == -1)
					n_pos = n_itr; // sets start position for the first time
				n_ptr += 1;
			}
			else if (n_pos != -1){ // resets a position in case of mismatch
				n_pos = -1;
				n_ptr =  0;
				continue;
			}
			n_itr += 1;
		}

		if (n_pos != -1 && (n_pos + n_tgt) > n_src) // the string is found partially
			n_pos  = -1;

		return n_pos;
	}


#endif