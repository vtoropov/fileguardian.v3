/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Aug-2017 at 2:54:27a, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian mini-filter driver file object wrapper interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsFileObject.h"
#include "FG_FsCtxMans.h"
#include "FG_Fs_SDK_ext_need_to_be_removed_in_future.h"
#include "fg.fs.evt.cmp.iface.h"

/* CRC-32C (iSCSI) polynomial in reversed bit order. */
#define CRC32_POLY 0x82f63b78

/* CRC-32 (Ethernet, ZIP, etc.) polynomial in reversed bit order. */
/* #define CRC32_POLY 0xedb88320 */

using namespace fl_sys;

#include "fg.driver.volume.object.h"

using namespace shared::km::fl_sys;

/////////////////////////////////////////////////////////////////////////////

UINT
CCrc32::CalculateOnBuffer(
	__in  const PBYTE   _pBuffer,
	__in  const UINT    _nBufferLen
	)
{
	UINT  crc_  = 0;
	UINT  iter_ = _nBufferLen;
	PBYTE ptr_  = _pBuffer;

	if (NULL == _pBuffer || !_nBufferLen)
		return crc_;

	while (iter_--) {

		crc_ ^= *ptr_++;

		for (UINT i_ = 0; i_ < 8; i_++)
			crc_ = crc_ & 1 ? (crc_ >> 1) ^ CRC32_POLY : crc_ >> 1;

	}

	return crc_;
}

UINT
CCrc32::CalculateOnFile(
	__in        PFLT_INSTANCE             _pInstance    ,
	__in        PFILE_OBJECT              _pFileObject    // file object, which CRC is calculated for;
	)
{
	NTSTATUS   nt_status  = STATUS_SUCCESS;
	BYTE       buffer_[CRC_CHUNK_SIZE] = {0};
	UINT       crc_check  = 0;
	ULONG      nBytesRead = 0;
	
	nt_status = CFile::ReadContent(
						_pInstance, _pFileObject, &buffer_[0], CRC_CHUNK_SIZE, &nBytesRead
					);
	if (!NT_SUCCESS(nt_status))
		return crc_check;

	crc_check = CCrc32::CalculateOnBuffer(&buffer_[0], CRC_CHUNK_SIZE);

	return crc_check;
}

/////////////////////////////////////////////////////////////////////////////

BOOLEAN
CComparator::CompareNames(
	__in       PFLT_FILE_NAME_INFORMATION _pSource,
	__in       PFLT_FILE_NAME_INFORMATION _pTarget,
	__in       CONST BOOL                 _bParseComponents
	)
{
	NTSTATUS      nt_status  = STATUS_SUCCESS;
	BOOLEAN       bResult    = FALSE;
	//
	// two NULLs are not equals because such case (two or one NULL) indicates an error;
	//
	if (NULL == _pSource ||
	    NULL == _pTarget)
		return bResult;

	if (_bParseComponents) {

		nt_status = FltParseFileNameInformation(_pSource);
		if (!NT_SUCCESS(nt_status) )
			return bResult;

		nt_status = FltParseFileNameInformation(_pTarget);
		if (!NT_SUCCESS(nt_status) )
			return bResult;

	}

	bResult = RtlEqualUnicodeString(&_pSource->FinalComponent, &_pTarget->FinalComponent, TRUE);

	return bResult;
}

BOOLEAN
CComparator::ComparePaths(
	__in       PFLT_FILE_NAME_INFORMATION _pSource,
	__in       PFLT_FILE_NAME_INFORMATION _pTarget,
	__in       CONST BOOL                 _bParseComponents
	)
{
	NTSTATUS      nt_status  = STATUS_SUCCESS;
	BOOLEAN       bResult    = FALSE;
	//
	// two NULLs are not equals because such case (two or one NULL) indicates an error;
	//
	if (NULL == _pSource ||
	    NULL == _pTarget)
		return bResult;

	if (_bParseComponents) {

		nt_status = FltParseFileNameInformation(_pSource);
		if (!NT_SUCCESS(nt_status) )
			return bResult;

		nt_status = FltParseFileNameInformation(_pTarget);
		if (!NT_SUCCESS(nt_status) )
			return bResult;

	}

	bResult = RtlEqualUnicodeString(&_pSource->Volume, &_pTarget->Volume, TRUE) &&
	          RtlEqualUnicodeString(&_pSource->ParentDir, &_pTarget->ParentDir, TRUE);

	return bResult;
}
/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CFile::ConvertUncToDos(
	__in        PFILE_OBJECT CONST        _pFileObject,
	__inout     utils::CStringW&          _pDosPath
	)
{
	POBJECT_NAME_INFORMATION
	         pInfo     = NULL;
	NTSTATUS nt_status = STATUS_SUCCESS;

	if (NULL == _pFileObject)
		return (nt_status = STATUS_INVALID_PARAMETER); // this is mandatory argument and cannot be NULL

	if (_pDosPath.IsValid())
		return (nt_status = STATUS_INVALID_PARAMETER); // it seems the string is already allocated

	nt_status = IoQueryFileDosDeviceName(_pFileObject, &pInfo);
	if (!NT_SUCCESS(nt_status)){
		return nt_status;
	}

	nt_status = _pDosPath.Allocate(pInfo->Name.Length + sizeof(WCHAR));
	if ( NT_SUCCESS(nt_status)){
		RtlCopyUnicodeString(_pDosPath, &pInfo->Name);
	}

	if (NULL != pInfo){
		ExFreePool(pInfo); pInfo = NULL;
	}

	return nt_status;
}

NTSTATUS
CFile::FileName (
	__inout     PFLT_CALLBACK_DATA        _pFltData     ,
	__inout     utils::CStringW&          _pFileName
	)
{
	PFLT_FILE_NAME_INFORMATION
	            nm_Info    = NULL;
	NTSTATUS    nt_status  = STATUS_SUCCESS;

	if (NULL == _pFltData)
		return (nt_status  = STATUS_INVALID_PARAMETER);

	if (_pFileName.IsValid())
		return (nt_status  = STATUS_INVALID_PARAMETER);

	PAGED_CODE();
	//
	//  FltGetFileNameInformation - this is enough for getting a file name.
	//
	nt_status = FltGetFileNameInformation(
					_pFltData, (FLT_FILE_NAME_OPENED|FLT_FILE_NAME_QUERY_DEFAULT|FLT_FILE_NAME_REQUEST_FROM_CURRENT_PROVIDER), &nm_Info
				);
	if (!NT_SUCCESS( nt_status )){
		return nt_status;
	}

	nt_status = FltParseFileNameInformation( nm_Info );

	if (!NT_SUCCESS(nt_status))
	{
		FltReleaseFileNameInformation(nm_Info);
		return nt_status;
	}

	_pFileName.Allocate(&nm_Info->Name);

	if (NULL != nm_Info){
		FltReleaseFileNameInformation(nm_Info); nm_Info = NULL;
	}

	return nt_status;
}

NTSTATUS
CFile::FileNameUnsafe (
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects,
	__inout     utils::CStringW&          _refFileName
	)
{
	PFLT_FILE_NAME_INFORMATION
	            pFileName  = NULL;
	NTSTATUS    nt_status  = STATUS_SUCCESS;

	PAGED_CODE();

	if (NULL == _pFltObjects)
		return (nt_status = STATUS_INVALID_PARAMETER);

	nt_status = FltGetFileNameInformationUnsafe(
			_pFltObjects->FileObject, _pFltObjects->Instance, FLT_FILE_NAME_NORMALIZED, &pFileName
		);

	if (!NT_SUCCESS(nt_status))
		return nt_status;
	else{
		_refFileName.CopyFrom(
					&pFileName->Name
				);
		FltReleaseFileNameInformation(pFileName); pFileName = NULL;
	}
	return nt_status;
}

NTSTATUS
CFile::FileNameUnsafe (
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects,
	__inout     UNICODE_STRING&           _refFileName
)
{
	PFLT_FILE_NAME_INFORMATION
	            pFileName  = NULL;
	NTSTATUS    nt_status  = STATUS_SUCCESS;
	USHORT      us_req_    = 0;

	PAGED_CODE();

	if (NULL == _pFltObjects)
		return (nt_status = STATUS_INVALID_PARAMETER);

	nt_status = FltGetFileNameInformationUnsafe(
			_pFltObjects->FileObject, _pFltObjects->Instance, FLT_FILE_NAME_NORMALIZED, &pFileName
		);

	if (!NT_SUCCESS(nt_status))
		return nt_status;
	else{
#if (1)
		us_req_ = pFileName->Name.Length;
		if (us_req_ > _refFileName.MaximumLength - sizeof(WCHAR))
			us_req_ = _refFileName.MaximumLength - sizeof(WCHAR);

		::RtlStringCbCopyUnicodeString(
			_refFileName.Buffer, us_req_, &pFileName->Name
		);
#else
		us_req_ = pFileName->Name.Length;
		if (us_req_ > _refFileName.MaximumLength)
			us_req_ = _refFileName.MaximumLength;

		::RtlCopyMemory(
			&_refFileName.Buffer[0], pFileName->Name.Buffer, us_req_
		);
#endif
		FltReleaseFileNameInformation(pFileName); pFileName = NULL;
	}
	return nt_status;
}

NTSTATUS
CFile::GetId (
	__in        PFLT_INSTANCE             _pInstance    ,
	__in        PFILE_OBJECT              _pFileObject  ,
	__inout     TFileReference&           _File_Ref
	)
{
	NTSTATUS      nt_status  = STATUS_SUCCESS;
	FILE_INTERNAL_INFORMATION  fileInternalInformation = {0};
	FILE_ID_INFORMATION        fileIdInformation       = {0};

	PAGED_CODE();

	//
	//  queries FileInternalInformation for getting file ID.
	//
	nt_status = FltQueryInformationFile(
					_pInstance  ,
					_pFileObject,
					&fileInternalInformation         ,
					sizeof(FILE_INTERNAL_INFORMATION),
					FileInternalInformation,
					NULL
				);
	if (NT_SUCCESS( nt_status )) {
		return nt_status;
	}
	//
	//  ReFS uses 128-bit file IDs.  FileInternalInformation supports 64-
	//  bit file IDs.  ReFS signals that a particular file ID can only 
	//  be represented in 128 bits by returning FILE_INVALID_FILE_ID as
	//  the file ID.  In that case we need to use FileIdInformation.
	//
	if (fileInternalInformation.IndexNumber.QuadPart == FILE_INVALID_FILE_ID) {
		// TODO: Win7 SDK doesn't support ReFS data types (it became available since Win Server 2012);
		nt_status = FltQueryInformationFile(
						_pInstance  ,
						_pFileObject,
						&fileIdInformation,
						sizeof(FILE_ID_INFORMATION),
						(FILE_INFORMATION_CLASS)sdk_ext::FileIdInformation,
						NULL
					);
		if (NT_SUCCESS( nt_status )){
			//
			//  We don't use SizeofFileId() here because we are not
			//  measuring the size of a FILE_REFERENCE.  We know we have
			//  a 128-bit value.
			//
			RtlCopyMemory(
				&_File_Ref, &fileIdInformation.FileId, sizeof(_File_Ref)
			);
		}
	}
	if (fileInternalInformation.IndexNumber.QuadPart != FILE_INVALID_FILE_ID
		|| !NT_SUCCESS( nt_status ))
	{
		_File_Ref.FileId64.Value = fileInternalInformation.IndexNumber.QuadPart;
		_File_Ref.FileId64.UpperZeroes = 0ll;

		nt_status = STATUS_SUCCESS;
	}
	return nt_status;
}

NTSTATUS
CFile::GetIdAsText (
	__in        PFLT_INSTANCE             _pInstance    ,
	__in        PFILE_OBJECT              _pFileObject  ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects  ,
	__inout     PUNICODE_STRING           _pFieldIdString
	)
{
	NTSTATUS       nt_status  = STATUS_SUCCESS;
	TFileReference fileRef;

	PAGED_CODE();

	if (NULL == _pFieldIdString)
		return (nt_status = STATUS_INVALID_PARAMETER);
	//
	//  composes the string with: (1) the volume GUID name + (2) a backslash + (3) the file ID.
	//

	//
	//  It is assumed the file ID is *loaded* in the StreamContext. Note that if the
	//  file has been deleted GetFileId will return STATUS_FILE_DELETED.
	//  Since we're interested in detecting whether the file has been deleted
	//  that's fine; the open-by-ID will not actually take place.  We have to
	//  ensure it is loaded before building the string length below since we
	//  may get either a 64-bit or 128-bit file ID back.
	//
	nt_status = CFile::GetId( _pInstance, _pFileObject, fileRef );

	if (!NT_SUCCESS( nt_status ))
		return nt_status;

	//
	//  First calculate the required buffer length.
	//  Note that ReFS understands both 64- and 128-bit file IDs when opening
	//  by ID, so whichever size we get back from GetFileId(), it will work.
	//
	_pFieldIdString->MaximumLength = FILE_VOLUME_GUID_NAME_SIZE * sizeof(WCHAR)
	                        + sizeof(WCHAR)
	                        + SizeofFileId( fileRef );

	nt_status = utils::CStringW::Allocate( _pFieldIdString );
	if (!NT_SUCCESS( nt_status ))
		return nt_status;

	// obtains volume GUID name here and caches it in the InstanceContext.
	nt_status = CVolume::CName::GuidName( _pFltObjects, _pFieldIdString );
	if (!NT_SUCCESS( nt_status ))
	{
		utils::CStringW::Free(_pFieldIdString);
		return nt_status;
	}
	//  appends the file ID to the end of the string;
	RtlCopyMemory(
		Add2Ptr(_pFieldIdString->Buffer, _pFieldIdString->Length ),
		&fileRef,
		SizeofFileId( fileRef )
	);

	_pFieldIdString->Length += SizeofFileId( fileRef );

	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

ULONG
CFile::GetDispose (
	__in        CONST FLT_PARAMETERS      _nParameters
	)
{
	return (_nParameters.Create.Options >> 24);
}

VOID
CFile::GetOptions (
	__in        CONST FLT_PARAMETERS      _nParameters  ,
	__inout     ULONG&                    _nDispose     ,
	__inout     ULONG&                    _nOptions
	)
{
	_nDispose   = CFile::GetDispose(_nParameters);
	_nOptions   = (_nParameters.Create.Options &  0x00FFFFFF);
}

/////////////////////////////////////////////////////////////////////////////

LARGE_INTEGER
CFile::GetSize (
	__in        PFLT_INSTANCE             _pInstance    ,
	__in        PFILE_OBJECT              _pFileObject
	)
{
	FILE_STANDARD_INFORMATION
	               stdInfo        = {0};
	NTSTATUS       nt_status      = STATUS_SUCCESS;
	LARGE_INTEGER  ll_size        = {0};

	if (NULL == _pInstance ||
	    NULL == _pFileObject)
		return ll_size;

	if (CFile::IsDirectory(_pInstance, _pFileObject))
		return ll_size;

	nt_status = FltQueryInformationFile(
						_pInstance, _pFileObject, &stdInfo, sizeof(stdInfo), FileStandardInformation, NULL
					);
	if (!NT_SUCCESS(nt_status))
		return ll_size;

	return ll_size;
}

NTSTATUS
CFile::IsDeleted (
	__in        PFLT_INSTANCE             _pInstance    ,
	__in        PFILE_OBJECT              _pFileObject  ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects  ,
	__in        PFLT_CONTEXT              _pContext     , // actually, stream context pointer
	__in        BOOLEAN                   _bIsTransaction
	)
{
	NTSTATUS             nt_status = STATUS_SUCCESS;
	FILE_OBJECTID_BUFFER id_buffer = {0};
	FLT_FILESYSTEM_TYPE  fileSystemType = FLT_FSTYPE_UNKNOWN;

	PAGED_CODE();

	//
	//  We need to know whether we're on ReFS or NTFS.
	//
	nt_status = FltGetFileSystemType(
					_pFltObjects->Instance, &fileSystemType
				);
	if (STATUS_SUCCESS != nt_status){
#if DBG
		DbgPrint(
			"__fg_fs: [WARN] CFile::IsDeleted(): FltGetFileSystemType() returns=%X;\n", nt_status
			);
#endif
		return nt_status;
	}
	//
	//  FSCTL_GET_OBJECT_ID does not return STATUS_FILE_DELETED if the
	//  file was deleted in a transaction, and this is why we need another
	//  method for detecting if the file is still present: opening by ID.
	//
	//  If we're on ReFS we also need to open by file ID because ReFS does not
	//  support object IDs.
	//
	if (_bIsTransaction ||
		(fileSystemType == FLT_FSTYPE_REFS)){

			nt_status = CFile::OpenById(
							_pInstance, _pFileObject, _pFltObjects, _pContext
						);
		switch (nt_status)
		{
		case STATUS_INVALID_PARAMETER:
			//
			//  The file was deleted. In this case, trying to open it
			//  by ID returns STATUS_INVALID_PARAMETER.
			//
			return (nt_status = STATUS_FILE_DELETED);

		case STATUS_DELETE_PENDING:
			//
			//  In this case, the main file still exists, but is in
			//  a delete pending state, so we return STATUS_SUCCESS,
			//  signaling it still exists and wasn't deleted by this
			//  operation.
			//
			return (nt_status = STATUS_SUCCESS);
		default:
#if DBG
			if (!NT_SUCCESS(nt_status)){
				DbgPrint(
					"__fg_fs: [WARN] CFile::IsDeleted(): CFile::OpenById() returns=%X;\n", nt_status
					);
			}
#endif
			return  nt_status;
		}
	}
	else {
		//
		//  When not in a transaction, attempting to get the object ID of the
		//  file is a cheaper alternative compared to opening the file by ID.
		//
		nt_status = FltFsControlFile(
						_pInstance,
						_pFileObject,
						FSCTL_GET_OBJECT_ID,
						NULL,
						0,
						&id_buffer,
						sizeof(FILE_OBJECTID_BUFFER),
						NULL
					);
		switch (nt_status)
		{
		case STATUS_OBJECTID_NOT_FOUND:
			//
			//  Getting back STATUS_OBJECTID_NOT_FOUND means the file
			//  still exists, it just doesn't have an object ID.
			//
			return (nt_status = STATUS_SUCCESS);
		default:
			//
			//  Else we just get back STATUS_FILE_DELETED if the file
			//  doesn't exist anymore, or some error status, so no
			//  status conversion is necessary.
			//
#if DBG
			if (!NT_SUCCESS(nt_status) && STATUS_FILE_DELETED != nt_status){
				DbgPrint(
					"__fg_fs: [WARN] CFile::IsDeleted(): FltFsControlFile() returns=%X;\n", nt_status
					);
			}
#endif
			NOTHING;
		}
	}

	return nt_status;
}

BOOLEAN
CFile::IsDirectory (
	__in        PFLT_INSTANCE             _pInstance    ,
	__in        PFILE_OBJECT              _pFileObject
	)
{
	NTSTATUS       nt_status    = STATUS_SUCCESS;
	BOOLEAN        b_directory  = FALSE;

	if (NULL == _pInstance ||
	    NULL == _pFileObject)
		return b_directory;

	nt_status = FltIsDirectory(
					_pFileObject, _pInstance, &b_directory
				);
	if (!NT_SUCCESS(nt_status))
		b_directory = FALSE;

	return b_directory;
}

BOOLEAN
CFile::IsExist (
	__in        PUNICODE_STRING           _pObjectPath
	)
{
	HANDLE         fl_handle    = NULL;
	NTSTATUS       nt_status    = STATUS_SUCCESS;
	BOOLEAN        bl_return    = FALSE;

	OBJECT_ATTRIBUTES obj_a = {0};
	IO_STATUS_BLOCK   io_bl = {0};

	if (NULL == _pObjectPath ||
		NULL == _pObjectPath->Buffer ||
		NULL == _pObjectPath->Length)
		return bl_return;

	InitializeObjectAttributes(
					&obj_a, _pObjectPath, (OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE), NULL, NULL
				);

	nt_status = ZwOpenFile(
					&fl_handle, GENERIC_READ | SYNCHRONIZE, &obj_a, &io_bl, FILE_SHARE_READ, 0
				);
	if (NT_SUCCESS(nt_status)) {

		::ZwClose(fl_handle);
		bl_return = (FILE_EXISTS == io_bl.Status ? TRUE : FALSE);
	}
	return bl_return;
}


NTSTATUS
CFile::OpenById (
	__in        PFLT_INSTANCE             _pInstance    ,
	__in        PFILE_OBJECT              _pFileObject  ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects  ,
	__inout     HANDLE&                   _hFile
	)
{
	NTSTATUS          nt_status    = STATUS_SUCCESS;
	UNICODE_STRING    fileIdString = {0};
	OBJECT_ATTRIBUTES obj_atts     = {0};
	IO_STATUS_BLOCK   ioBlock      = {0};

	IO_DRIVER_CREATE_CONTEXT drvCreateContext = {0};
	static TDriverData&      drv_data = global::DriverData();

	PAGED_CODE();

	//
	//  First get the file ID string. Note that this may fail with STATUS_FILE_DELETED
	//  and short-circuit our open-by-ID. Since we're really trying to see if
	//  the file is deleted, that's perfectly okay.
	//
	nt_status = CFile::GetIdAsText(
					_pInstance, _pFileObject, _pFltObjects, &fileIdString
				);
	if (!NT_SUCCESS( nt_status ))
		return nt_status;

	InitializeObjectAttributes(
			&obj_atts, &fileIdString, OBJ_KERNEL_HANDLE, NULL, NULL
		);
	//
	//  It is important to initialize the IO_DRIVER_CREATE_CONTEXT structure's
	//  TxnParameters. We'll always want to do this open on behalf of a
	//  transaction because opening the file by ID is the method we use to
	//  detect if the whole file still exists when we're in a transaction.
	//
	IoInitializeDriverCreateContext( &drvCreateContext );

	drvCreateContext.TxnParameters =
        IoGetTransactionParameterBlock( _pFileObject );

	nt_status = FltCreateFileEx2(
						drv_data.pFilter,
						_pInstance,
						&_hFile,
						NULL,
						FILE_READ_ATTRIBUTES,
						&obj_atts,
						&ioBlock ,
						(PLARGE_INTEGER) NULL,
						0L,
						FILE_SHARE_VALID_FLAGS,
						FILE_OPEN,
						FILE_OPEN_REPARSE_POINT | FILE_OPEN_BY_FILE_ID,
						(PVOID)NULL,
						0L,
						IO_IGNORE_SHARE_ACCESS_CHECK,
						&drvCreateContext
					);
	if (NT_SUCCESS( nt_status )) {
		nt_status = FltClose(_hFile ); // TODO: closing file here is for test purposes only; will be removed in the future;
	}

	utils::CStringW::Free( &fileIdString );

	return nt_status;
}

NTSTATUS
CFile::ReadContent (
	__in        PFLT_INSTANCE             _pInstance    ,
	__in        PFILE_OBJECT              _pFileObject  , // contains file info, which data will be read from;
	__inout     PBYTE                     _pBuffer      , // a buffer for data, it is already allocated by a caller
	__in CONST  UINT                      _pBufferLen   , // a buffer size
	__inout     PULONG                    _nBytesRead     // number of bytes that are actually read from the file;
	)
{
	NTSTATUS      nt_status  = STATUS_SUCCESS;
	LARGE_INTEGER byteOffset = { 0 };
	UINT          bufferSize = _pBufferLen;

	LARGE_INTEGER act_size   = CFile::GetSize(_pInstance, _pFileObject);

	if (bufferSize > act_size.LowPart)
		bufferSize = act_size.LowPart;

	static const
	FLT_IO_OPERATION_FLAGS read_ops = FLTFL_IO_OPERATION_NON_CACHED|FLTFL_IO_OPERATION_DO_NOT_UPDATE_BYTE_OFFSET;

	nt_status = FltReadFile(
					_pInstance  ,
					_pFileObject, &byteOffset, bufferSize, _pBuffer, read_ops, _nBytesRead, NULL, NULL
				);

	return nt_status;
}

NTSTATUS
CFile::SetInfoToCtx (
	__in        PFLT_CALLBACK_DATA        _pFltData     ,
	__in        PFLT_CONTEXT              _pContext     , // actually, stream context pointer
	__in        BOOLEAN                   _bCheckPass
	)
{
	NTSTATUS      nt_status  = STATUS_SUCCESS;
	PTStreamCtx   pStreamCtx = (PTStreamCtx)_pContext;

	PFLT_FILE_NAME_INFORMATION oldNameInfo = {0};
	PFLT_FILE_NAME_INFORMATION newNameInfo = {0};

	PAGED_CODE();
	//
	//  FltGetFileNameInformation - this is enough for getting a file name.
	//
	nt_status = FltGetFileNameInformation(
					_pFltData, (FLT_FILE_NAME_OPENED | FLT_FILE_NAME_QUERY_DEFAULT), &newNameInfo
				);
	if (!NT_SUCCESS( nt_status )){
		return nt_status;
	}

	//
	// checks passing file object name through the list of included folders for watching;
	//
	if (_bCheckPass && FALSE == CFltCompare::IsIncludedToList(&newNameInfo->Name)) {
		if (NULL != newNameInfo)
			FltReleaseFileNameInformation( newNameInfo );
		return nt_status = STATUS_UNSUCCESSFUL;
	}
	//
	//  FltParseFileNameInformation - this fills in the other gaps, like the
	//  stream name, if present.
	//
	nt_status = FltParseFileNameInformation( newNameInfo );

	if (!NT_SUCCESS(nt_status))
	{
		FltReleaseFileNameInformation(newNameInfo);
		return nt_status;
	}
	//
	//  sets new info to the context, replacing the previous one.
	//
	oldNameInfo = (PFLT_FILE_NAME_INFORMATION) InterlockedExchangePointer( (PVOID*) (&pStreamCtx->pNameInfo), newNameInfo );
	if (NULL != oldNameInfo) {
		FltReleaseFileNameInformation( oldNameInfo );
	}

	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

CFileInfoPtr::CFileInfoPtr(void) : m_pInfo(NULL)
{
}

CFileInfoPtr::CFileInfoPtr(const PFLT_FILE_NAME_INFORMATION pInfo) : m_pInfo(pInfo)
{
}

CFileInfoPtr::~CFileInfoPtr(void)
{
	if (m_pInfo){
		FltReleaseFileNameInformation(m_pInfo);
		m_pInfo = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

CFileInfoPtr::operator PFLT_FILE_NAME_INFORMATION (void)const { return  m_pInfo; }
CFileInfoPtr::operator PFLT_FILE_NAME_INFORMATION*(void)      { return &m_pInfo; }
CFileInfoPtr::operator PFLT_FILE_NAME_INFORMATION&(void)      { return  m_pInfo; }

CFileInfoPtr& CFileInfoPtr::operator=(const PFLT_FILE_NAME_INFORMATION pInfo)
{
	if (m_pInfo){
		FltReleaseFileNameInformation(m_pInfo);
		m_pInfo = NULL;
	}
	m_pInfo = pInfo;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS    CFileInfoPtr::Attach(const PFLT_FILE_NAME_INFORMATION pInfo)
{
	if (m_pInfo){
		FltReleaseFileNameInformation(m_pInfo);
		m_pInfo = NULL;
	}
	m_pInfo = pInfo;
	NTSTATUS nt_status = STATUS_SUCCESS;
	return   nt_status;
}

PFLT_FILE_NAME_INFORMATION
            CFileInfoPtr::Detach(void)
{
	PFLT_FILE_NAME_INFORMATION pInfo = m_pInfo;
	m_pInfo = NULL;
	return pInfo;
}

bool        CFileInfoPtr::IsValid(void)const
{
	return (NULL != m_pInfo);
}

PFLT_FILE_NAME_INFORMATION
		    CFileInfoPtr::Ptr(void)const
{
	return m_pInfo;
}