#ifndef _FGFSN0TIFYDATA_H_302056E6_435D_42c1_9BE1_35844850B6FD_INCLUDED
#define _FGFSN0TIFYDATA_H_302056E6_435D_42c1_9BE1_35844850B6FD_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Aug-2017 at 6:40:22p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian mini-filter driver notification data structure(s) declaration file.
*/
#include "FG_FsFilterIface.h"
#include "FG_FsCtxIface.h"

//
//  This structure represents pending delete notifications for files that have
//  been deleted in an open transaction.
//
typedef struct _DELETE_NOTIFY_DATA {

	LIST_ENTRY    links;             //  Links to other _DELETE_NOTIFY_DATA structures in the notification list;
	PFLT_CONTEXT  pContext;          //  Pointer to the stream context for the deleted stream/file;
	BOOLEAN       bFileDelete;       //  TRUE for a deleted file, FALSE for a stream.
}
TDeleteNotifyData, *PTDeleteNotifyData;

/////////////////////////////////////////////////////////////////////////////

namespace notify {

	class CNotifier {
	public:
		class _CRemovable {
		public:
			static NTSTATUS Append(__in PUNICODE_STRING _pDevicePath);
			static NTSTATUS Delete(__in PUNICODE_STRING _pDevicePath);
		};

		class CChangeEvent {
		public:
			/*
			Routine Description:
				This routine is called by pre-post-handler in cases related to file
				object change operation;
			*/
			static
			VOID
			Notify (
				__in   TFileReference_Ex&         _pTarget, // TODO: to return back CONST specifier;
				__in   ULONG                      _uProcId = 0
				);
			/**/
			static
			VOID
			Notify (
				__in   PCFLT_RELATED_OBJECTS      _pFltObjects,
				__in   ULONG                      _uProcId = 0
				);
		};
		/*
		Routine Description:
			This routine is called by pre-create handler in cases related to file
			object create operation;
		*/
		static
		VOID
		OnCreate (
			__in       PFLT_FILE_NAME_INFORMATION _pTarget,
			__in       PCFLT_RELATED_OBJECTS      _pFltObjects,
			__in       ULONG                      _uProcId = 0
			);
		/*
		Routine Description:
			This routine does the processing after it is verified, in the post-cleanup
			callback, that a file or stream were deleted. It sorts out whether it's a
			file or a stream delete, whether this is in a transacted context or not,
			and issues the appropriate notifications.

		Arguments:
			StreamContext - Pointer to the stream context of the deleted file/stream.
			IsFile        - TRUE if deleting a file, FALSE for an alternate data stream.

			TransactionContext - The transaction context. Present if in a transaction,
			                     NULL otherwise.
		*/
		static
		VOID
		OnDelete (
			__in       PFLT_CONTEXT               _pStreamCtx,
			__in       BOOLEAN                    _bIsFile   ,
			__inout    PFLT_CONTEXT               _pTransCtx ,
			__in       PCFLT_RELATED_OBJECTS      _pFltObjects
			);

		static
		VOID
		OnDeleteNotTransacted (
			__in       PFLT_FILE_NAME_INFORMATION _pTarget,
			__in       PCFLT_RELATED_OBJECTS      _pFltObjects,
			__in       ULONG                      _uProcId = 0
			);

		/*
		Routine Description:
			This routine is called by pre-create handler in cases related to file
			object rename operation;
		*/
		static
		VOID
		OnRename (
			__in       PFLT_FILE_NAME_INFORMATION _pSource,
			__in       PFLT_FILE_NAME_INFORMATION _pTarget,
			__in       PCFLT_RELATED_OBJECTS      _pFltObjects,
			__in       ULONG                      _uProcId = 0
			);
		/*
		Routine Description:
			This routine is called by PreSetInfo_OnFileRename handler in cases related to file
			object rename operation that leads to replace target object;
		*/
		static
		VOID
		OnReplace (
			__in       PFLT_FILE_NAME_INFORMATION _pSource,
			__in       PFLT_FILE_NAME_INFORMATION _pTarget,
			__in       PCFLT_RELATED_OBJECTS      _pFltObjects,
			__in       ULONG                      _uProcId = 0
			);
		/*
		Routine Description:
			This routine adds a pending deletion notification (TDeleteNotifyData)
			object to the transaction context DeleteNotifyList. It is called from
			NotifyDelete when a file or stream gets deleted in a transaction.

		Arguments:
			StreamContext      - Pointer to the stream context.
			TransactionContext - Pointer to the transaction context.
			FileDelete         - TRUE if this is a FILE deletion, FALSE if it's a STREAM
		                         deletion.
		Return Value:
			STATUS_SUCCESS.
		*/
		static
		NTSTATUS
		OnTransDeleteBegin (
			__inout    PFLT_CONTEXT               _pStreamCtx,
			__inout    PFLT_CONTEXT               _pTransCtx ,
			__in       BOOLEAN                    _bFileDelete
			);
		/*
		Routine Description:
			This routine is called by the transaction notification callback to issue
			the proper notifications for a file that has been deleted in the context
			of that transaction.
			The file will be reported as finally deleted, if the transaction was
			committed, or "saved" if the transaction was rolled back.

		Arguments:
			_pNotify     - Pointer to the TDeleteNotifyData object that contains the
			               data necessary for issuing this notification.
			_bCommitted  - TRUE if the transaction was committed, FALSE if it was
			               rolled back.
		*/
		static
		VOID
		OnTransDeleteEnd (
			__in       PTDeleteNotifyData         _pNotify,
			__in       BOOLEAN                    _bCommitted
			);
	};
}

#endif/*_FGFSN0TIFYDATA_H_302056E6_435D_42c1_9BE1_35844850B6FD_INCLUDED*/