#ifndef _FGFSTXOBJECT_H_62A319F7_2655_47ca_A1DB_E585C4E60D53_INCLUDED
#define _FGFSTXOBJECT_H_62A319F7_2655_47ca_A1DB_E585C4E60D53_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Oct-2017 at 9:48:54a, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian mini-filter driver transaction object interface declaration file.
*/
#include "FG_FsCtxIface.h"

namespace root {

	class CTxObject {
	public:
		static
		BOOLEAN
		IsDirty(
			__in        PFLT_CALLBACK_DATA        _pFltData
			);

		/*
			This function qeuries the KTM that how trasnaction was ended.
		*/
		static
		NTSTATUS
		QueryOutcome(
			__in        PKTRANSACTION             _pTxObject  ,
			__inout     ULONG&                    _uOutcome
			);
	};

	class CTxModifyFile : public CTxObject {
	private:
		/*
			For internal use: querying transaction context for given file context,
			if the file context belongs to transaction, enlist the transaction if not yet;
		*/
		static
		NTSTATUS
		Enlist(
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects,
			__in        PTFileObjectCtx           _pFileCtx   ,
			__inout     PTTransactCtx&            _pTxEnlisted
			);
		/*
			For internal use: propagate transaction dirty flag to the file context;
		*/
		static
		VOID
		PropagateDirty(
			__in        PTFileObjectCtx           _pFileCtx   ,
			__in        const ULONG               _uTxOutcome
			);
	public:
		static
		NTSTATUS
		GetInstance(
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects,
			__inout     PFLT_CONTEXT*             _ppTxContext
		);

		/*
			This routine is expected to be invoked at post-create callback. Note that this function will enlist
			the newly allocated transaction context via FltEnlistInTransaction if it needs to.
		*/
		static
		NTSTATUS
		Process(
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects,
			__in        PTFileObjectCtx           _pFileCtx
			);

		/*
			This is a helper function that processes transaction commitment or rollback (outcome); 
		*/
		static
		NTSTATUS
		ProcessOutcome(
			__inout     PTTransactCtx             _pTxContext ,
			__in        const ULONG               _uTxOutcome
			);
	};
}

#endif/*_FGFSTXOBJECT_H_62A319F7_2655_47ca_A1DB_E585C4E60D53_INCLUDED*/