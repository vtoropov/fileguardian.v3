/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Oct-2017 at 10:07:12a, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian mini-filter driver transaction object interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsTxObject.h"
#include "FG_FsCtxMans.h"
#include "FG_FsCtxAlloca.h"
#include "FG_FsNotifyData.h"

using namespace root;
using namespace ctx;
using namespace notify;

/////////////////////////////////////////////////////////////////////////////

#include "FG_Fs_SDK_ext_need_to_be_removed_in_future.h" // FSCTL_OFFLOAD_WRITE

BOOLEAN
CTxObject::IsDirty(
	__in        PFLT_CALLBACK_DATA        _pFltData
	)
{
	BOOLEAN bNeed =  FALSE;

	if (NULL == _pFltData)
		return bNeed;

	PFLT_IO_PARAMETER_BLOCK iopb = _pFltData->Iopb;

	switch(iopb->MajorFunction)
	{
	case IRP_MJ_WRITE               : bNeed = TRUE; break;
	case IRP_MJ_FILE_SYSTEM_CONTROL :
		switch ( iopb->Parameters.FileSystemControl.Common.FsControlCode ){
			case FSCTL_OFFLOAD_WRITE:
			case FSCTL_WRITE_RAW_ENCRYPTED:
			case FSCTL_SET_ZERO_DATA: bNeed = TRUE; break;
			default:;
		}   break;
	case IRP_MJ_SET_INFORMATION     :
		switch ( iopb->Parameters.SetFileInformation.FileInformationClass ){
			case FileEndOfFileInformation:
			case FileValidDataLengthInformation: bNeed = TRUE; break;
			default:;
		}   break;
	}
	return bNeed;
}

NTSTATUS
CTxObject::QueryOutcome(
	__in        PKTRANSACTION             _pTxObject  ,
	__inout     ULONG&                    _uOutcome
	)
{
	TRANSACTION_BASIC_INFORMATION
	         txInfo    ={0};
	NTSTATUS nt_status = STATUS_SUCCESS;
	HANDLE   hTxHandle = NULL;

	PAGED_CODE();

	if (NULL == _pTxObject)
		return (nt_status = STATUS_INVALID_PARAMETER);

	nt_status = ObOpenObjectByPointer(
						_pTxObject, OBJ_KERNEL_HANDLE, NULL, GENERIC_READ, *TmTransactionObjectType, KernelMode, &hTxHandle
					);
	if (!NT_SUCCESS(nt_status)){
#if DBG
		DbgPrint(
				"__fg_fs: [ERROR] CTxObject::QueryOutcome() Getting TX outcome failed with err=%X;\n", nt_status
			);
#endif
		return nt_status;
	}

	nt_status = ZwQueryInformationTransaction(
						hTxHandle, TransactionBasicInformation, &txInfo, sizeof(TRANSACTION_BASIC_INFORMATION), NULL
					);
	if ( NT_SUCCESS(nt_status)){
		_uOutcome = txInfo.Outcome;
	}
	else {
#if DBG
		DbgPrint(
				"__fg_fs: [ERROR] CTxObject::QueryOutcome() Getting TX Info failed with err=%X;\n", nt_status
			);
#endif
	}

	ZwClose(hTxHandle); hTxHandle = NULL;

	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CTxModifyFile::Enlist(
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects,
	__in        PTFileObjectCtx           _pFileCtx   ,
	__inout     PTTransactCtx&            _pTxEnlisted
	)
{
	NTSTATUS nt_status = STATUS_SUCCESS;

	PAGED_CODE();

	if (NULL == _pFltObjects ||
	    NULL == _pFileCtx)
		return (nt_status = STATUS_INVALID_PARAMETER);

	if (NULL != _pFltObjects->Transaction){        // we have a transacted file
		nt_status = ctx::CCtxTxModifyMan::GetContext(
						_pFltObjects, (PFLT_CONTEXT*)&_pTxEnlisted
					);
		if (!NT_SUCCESS(nt_status)){
				return nt_status;
		}
		else if (!_pTxEnlisted->ModifyData.bEnlisted){

			_pTxEnlisted->ModifyData.bEnlisted = TRUE;
			nt_status = FltEnlistInTransaction(
							_pFltObjects->Instance, _pFltObjects->Transaction, _pTxEnlisted, CTX_NOTIFICATION_MASK
						);
			if (nt_status == STATUS_FLT_ALREADY_ENLISTED)
				nt_status  = STATUS_SUCCESS;

			if (!NT_SUCCESS( nt_status )){
				FltReleaseContext(_pTxEnlisted); _pTxEnlisted = NULL;
				return nt_status;
			}
			else
				_pTxEnlisted->ModifyData.bEnlisted = TRUE;
		}
	}

	return nt_status;
}

VOID
CTxModifyFile::PropagateDirty(
	__in        PTFileObjectCtx           _pFileCtx   ,
	__in        const ULONG               _uTxOutcome
	)
{
	if (NULL == _pFileCtx)
		return;
	if (TransactionOutcomeCommitted == _uTxOutcome){
	//
	//  The 'or' operator here handles the case below:
	//
	//  It is possible that the user only read the file even if the transacted file is opened for read-write access.
	//  Thus, transaction dirty flag (TxDirty) can be set to FALSE.
	//  
	//  Since KTM callback is asynchrounous, notifications are not necessarily received in order.
	//  It is possible that file dirty information will be wiped out, if we would use direct assignment,
	//  like this:
	//
	//  pFileCtx->Dirty = pFileCtx->TxDirty;
	//
		_pFileCtx->ModifyData.bDirty |= _pFileCtx->ModifyData.bTxDirty;
	}
	//
	//  clears transaction dirty flag (TxDirty) regardless of transaction outcome;
	//
	_pFileCtx->ModifyData.bTxDirty = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CTxModifyFile::GetInstance(
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects,
	__inout     PFLT_CONTEXT*             _ppTxContext
)
{
	NTSTATUS    nt_status = STATUS_SUCCESS;
	PFLT_CONTEXT pCurCtx  = NULL; // a context that is already set

	PAGED_CODE();

	if (NULL == _ppTxContext)
		return (nt_status = STATUS_INVALID_PARAMETER);

	if (NULL !=*_ppTxContext)
		return (nt_status = STATUS_INVALID_PARAMETER);

	nt_status = FltGetTransactionContext(
						_pFltObjects->Instance, _pFltObjects->Transaction, &pCurCtx
					);

	if (NT_SUCCESS( nt_status )) {
		*_ppTxContext = pCurCtx;
		return nt_status;
	}
	else if (STATUS_NOT_FOUND != nt_status) {
		return nt_status;
	}

	nt_status = CTxModifyCtxAlloca::Allocate(&pCurCtx);
	if (!NT_SUCCESS(nt_status))
		return nt_status;

	CTxContextPtr    pTxContext;
	pTxContext.Attach(pCurCtx); pCurCtx = NULL;

	ObReferenceObject(_pFltObjects->Transaction);
	pTxContext->ModifyData.pTransaction = _pFltObjects->Transaction;

	nt_status = FltSetTransactionContext(
						_pFltObjects->Instance, _pFltObjects->Transaction, FLT_SET_CONTEXT_KEEP_IF_EXISTS, pTxContext.Ptr(), &pCurCtx
					);

	if (NT_SUCCESS(nt_status)){
		*_ppTxContext = pTxContext.Detach();
		return nt_status;
	}

	ObDereferenceObject(pTxContext->ModifyData.pTransaction); // otherwise; reference count of the transaction will be increased

	pTxContext.Free();
	if (nt_status != STATUS_FLT_CONTEXT_ALREADY_DEFINED){
		return nt_status;
	}
	else
		nt_status = STATUS_SUCCESS; // the context is already assigned and we need to update the status to success
	*_ppTxContext = pCurCtx;

	return nt_status;
}

NTSTATUS
CTxModifyFile::Process(
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects,
	__in        PTFileObjectCtx           _pFileCtx
	)
{
	NTSTATUS nt_status = STATUS_SUCCESS;
	ULONG    uTxOut    = TransactionOutcomeUndetermined;
	CTxContextPtr pPrvTxCtx = NULL; // previous/old context
	CTxContextPtr pCurTxCtx = NULL; // current/new context

	PAGED_CODE();

	if (NULL != _pFltObjects->Transaction){
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] CTxModifyFile::Process(): file context belongs to a transaction;\n"
			);
#endif
		nt_status = CTxModifyFile::GetInstance(
							_pFltObjects, pCurTxCtx
						);
		if (!NT_SUCCESS(nt_status)){
			pCurTxCtx.Free();
#if DBG
		DbgPrint(
			"__fg_fs: [ERR] CTxModifyFile::Process(): getting transaction context failed;\n"
			);
#endif
			return nt_status;
		}
		else if (FALSE == pCurTxCtx->ModifyData.bEnlisted) {

			nt_status = FltEnlistInTransaction(
								_pFltObjects->Instance, _pFltObjects->Transaction, pCurTxCtx.Ptr(), CTX_NOTIFICATION_MASK
							);
			if (STATUS_FLT_ALREADY_ENLISTED == nt_status) {
				nt_status = STATUS_SUCCESS;
			}
			else if (!NT_SUCCESS(nt_status)){
#if DBG
		DbgPrint(
			"__fg_fs: [ERR] CTxModifyFile::Process(): enlisting transaction is failed [code=%X];\n", nt_status
			);
#endif
				return nt_status;
			}
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] CTxModifyFile::Process(): transaction context is successfully enlisted;\n"
			);
#endif
			pCurTxCtx->ModifyData.bEnlisted = TRUE;
		}
	}
	else {
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] CTxModifyFile::Process(): file object does *not* belong to a transaction;\n"
			);
#endif
	}

	//
	// we have the following possible outcome cases:
	//     1) pPrvTxCtx = NULL; pCurTxCtx = NULL; (not)        nothing changed; the file being modified is not transacted;
	//     2) pPrvTxCtx =    A; pCurTxCtx =    A; (not)        nothing changed; the same transaction is open
	//     3) pPrvTxCtx =    A; pCurTxCtx = NULL; (interested) the previously open transaction is closed
	//     4) pPrvTxCtx = NULL; pCurTxCtx =    B; (interested) a transaction is opened for modifying this file
	//     5) pPrvTxCtx =    A; pCurTxCtx =    B; (interested) new simultaneous transaction is created
	//
	// synchronizes the replacement of FileContext->TxContext with KTM callback;
	//
	// hopefully, the proper alignment of the data, which is referred to, will be made by compiler with
	// optimizing speed option turned on.
	//
	pPrvTxCtx.Ref() = (PTTransactCtx)InterlockedExchangePointer( (PVOID*) &_pFileCtx->ModifyData.pTxCtx, pCurTxCtx.Ptr() );

	// Case #1
	// pCurTxCtx == NULL; _pFileCtx->ModifyData.pTxCtx == NULL; will result to pPrvTxCtx.Ref() == NULL;
	// This means the file that is being modified is not transacted;
	//
	// Case #2
	// pCurTxCtx == Tx_A; _pFileCtx->ModifyData.pTxCtx == Tx_A; will result to pPrvTxCtx.Ref() == Tx_A;
	// This means the file modification is still being made by the same transaction;
	//
	// In both cases, we need to release the transaction current context only once;
	// context pointer takes care about proper releasing a raw data including handling NULL values;
	//

	if (pPrvTxCtx == pCurTxCtx){
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] CTxModifyFile::Process(): comparing case(s) ##1/2;\n"
			);
#endif
		pPrvTxCtx.Detach(); // otherwise we will release the same data twice;
		return nt_status;
	}

	// Case #3
	// pCurTxCtx == NULL; _pFileCtx->ModifyData.pTxCtx == Tx_A; will result to pPrvTxCtx.Ref() == Tx_A;
	// In this case current transaction context is not assigned, that means we need to process the previously open transaction,
	// because it is being closed;
	//
	if (!pCurTxCtx.IsValid() &&
	     pPrvTxCtx.IsValid()) {
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] CTxModifyFile::Process(): comparing case(s) #3;\n"
			);
#endif
		nt_status = CTxObject::QueryOutcome(
						pPrvTxCtx.Ptr()->ModifyData.pTransaction, uTxOut
					);
		//
		// disconnecting file context with transaction one;
		//
		ExAcquireFastMutex( pPrvTxCtx->ModifyData.pSyncMutex );
		RemoveEntryList   (&_pFileCtx->ModifyData.pThis );
		ExReleaseFastMutex( pPrvTxCtx->ModifyData.pSyncMutex );

		CTxModifyFile::PropagateDirty(
				_pFileCtx, uTxOut
			);
	}

	// Case #4
	// pCurTxCtx == Tx_B; _pFileCtx->ModifyData.pTxCtx == NULL; will result to pPrvTxCtx.Ref() == NULL;
	// we connect file context list with this trunsaction; 

	if ( pCurTxCtx.IsValid() &&
	    !pPrvTxCtx.IsValid()) {
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] CTxModifyFile::Process(): comparing case(s) #4;\n"
			);
#endif
		ExAcquireFastMutex( pCurTxCtx->ModifyData.pSyncMutex );
		{
			if (!pCurTxCtx->ModifyData.bListDrained) {
				InsertTailList(
							&pCurTxCtx->ModifyData.FileCtxList, &_pFileCtx->ModifyData.pThis
						);
			}
		}
		ExReleaseFastMutex( pCurTxCtx->ModifyData.pSyncMutex );
	}

	// Case #5
	// pCurTxCtx == Tx_B; _pFileCtx->ModifyData.pTxCtx == Tx_A; will result to pPrvTxCtx.Ref() == Tx_A;
	// new transaction has arrived; we need to close old transaction and to assign new one;

	if ( pCurTxCtx.IsValid() &&
	     pPrvTxCtx.IsValid()) {
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] CTxModifyFile::Process(): comparing case(s) #5;\n"
			);
#endif
		// closes the old transaction
		{
			nt_status = CTxObject::QueryOutcome(
						pPrvTxCtx.Ptr()->ModifyData.pTransaction, uTxOut
					);
		//
		// disconnecting file context with transaction one;
		//
		ExAcquireFastMutex( pPrvTxCtx->ModifyData.pSyncMutex );
		RemoveEntryList   (&_pFileCtx->ModifyData.pThis );
		ExReleaseFastMutex( pPrvTxCtx->ModifyData.pSyncMutex );

		CTxModifyFile::PropagateDirty(
				_pFileCtx, uTxOut
			);
		}

		// assigns new transaction
		ExAcquireFastMutex( pCurTxCtx->ModifyData.pSyncMutex );
		{
			if (!pCurTxCtx->ModifyData.bListDrained) {
				InsertTailList(
							&pCurTxCtx->ModifyData.FileCtxList, &_pFileCtx->ModifyData.pThis
						);
			}
		}
		ExReleaseFastMutex( pCurTxCtx->ModifyData.pSyncMutex );
	}

	return nt_status;
}

NTSTATUS
CTxModifyFile::ProcessOutcome(
	__inout     PTTransactCtx             _pTxContext ,
	__in        const ULONG               _uTxOutcome        
	)
{
	PLIST_ENTRY  pCurr = NULL;
	PLIST_ENTRY  pNext = NULL;
	NTSTATUS nt_status = STATUS_SUCCESS;

	if (NULL == _pTxContext)
		return (nt_status = STATUS_INVALID_PARAMETER);

	PAGED_CODE();

	ExAcquireFastMutex( _pTxContext->ModifyData.pSyncMutex );
	{
		PLIST_ENTRY pHead = &_pTxContext->ModifyData.FileCtxList;

		LIST_FOR_EACH_SAFE( pCurr, pNext, pHead){

			PTFileObjectCtx pFileCtx = (PTFileObjectCtx)
			                            CONTAINING_RECORD(pCurr, TFileObjectCtx, ModifyData);

			PTTransactCtx pTxCtx = (PTTransactCtx)
			                        InterlockedCompareExchangePointer( (PVOID*) &(pFileCtx->ModifyData.pTxCtx), NULL, &_pTxContext );

			if (pTxCtx == _pTxContext){
				//
				//  when pTxCtx and _pTxContext are equal, that means that 
				//  pFileCtx->ModifyData.pTxCtx has been successfully set to NULL.
				//

				RemoveEntryList(pCurr);
				CTxModifyFile::PropagateDirty(
						pFileCtx, _uTxOutcome
					);
				FltReleaseContext( pTxCtx ); pTxCtx = NULL;

				//
				//  we have just propagated TxDirty to Dirty.
				//  if the file becomes dirty, we put a notification out here.
				//
				if (pFileCtx->ModifyData.bDirty){
					notify::CNotifier::CChangeEvent::Notify(
								pFileCtx->ModifyData.FileRefEx
							);
				}
			}
			FltReleaseContext( pFileCtx ); pFileCtx = NULL;
		}
	}
	_pTxContext->ModifyData.bListDrained = TRUE;

	ExReleaseFastMutex( _pTxContext->ModifyData.pSyncMutex );

	return nt_status;
}