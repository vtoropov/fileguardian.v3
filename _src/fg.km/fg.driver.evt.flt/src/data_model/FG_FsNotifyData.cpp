/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Aug-2017 at 11:34:29p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian mini-filter driver notification data structure(s) implementation file.
*/
#include "StdAfx.h"
#include "FG_FsNotifyData.h"
#include "FG_FsCtxMans.h"
#include "fg.fs.filter.event.rec.proc.h"
#include "FG_FsFileObject.h"

#define  DELETE_NOTIFY_POOL_TAG       'nfyd'

using namespace notify;
/////////////////////////////////////////////////////////////////////////////

namespace notify { namespace details {

	NTSTATUS CNotifier_Removable_Operate(__in PUNICODE_STRING _pDevicePath, __in BOOL _bAppend) {

		NTSTATUS      nt_status     = STATUS_SUCCESS;
		PTFs_Evt_Record  pEntry     =  NULL;
		BOOL           bRecorded    =  FALSE;
		eFsObjectType::_e eType     = eFsObjectType::eFsFolderObject; // expected;
		DECLARE_UNICODE_STRING(uc_result, _LONG_PATH);

		if (NULL == _pDevicePath || NULL == _pDevicePath->Buffer || 0 == _pDevicePath->Length)
			return (nt_status = STATUS_INVALID_PARAMETER);

		if (_bAppend) ::RtlUnicodeStringPrintf(&uc_result, _T("External device is appended: %ws"), _pDevicePath->Buffer);
		else          ::RtlUnicodeStringPrintf(&uc_result, _T("External device is removed: %ws") , _pDevicePath->Buffer);

		pEntry = fg::filter::_imp::CRecAllocator::AllocateEntry();

		if (NULL != pEntry){
			if (fg::filter::_imp::CRecManager::SetEntryTarget(pEntry, &uc_result)){ // only target name is available for this type of event

				pEntry->Data.OriginatingTime = ctx::CContextTime::GetCurrentTimestamp();
				if (_bAppend)
				pEntry->Data.uFsEventType    = eFsEventType::eFsEventCreated;
				else
				pEntry->Data.uFsEventType    = eFsEventType::eFsEventReplaced;
				pEntry->Data.uFsObjectType   = eType;
				pEntry->Data.CompletionTime  = pEntry->Data.OriginatingTime;

				bRecorded = fg::filter::_imp::CRecManager::AppendEntry(pEntry);
			}
			if (!bRecorded)
				fg::filter::_imp::CRecAllocator::FreeEntry(pEntry);
		}


		return nt_status;
	}

}}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS CNotifier::_CRemovable::Append(__in PUNICODE_STRING _pDevicePath) {

	NTSTATUS nt_status = details::CNotifier_Removable_Operate(_pDevicePath, TRUE);
	if (NT_SUCCESS(nt_status)) {
#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CNotifier::_CRemovable::Append(): target=%wZ;\n", _pDevicePath
	);
#endif
	}
	return nt_status;
}

NTSTATUS CNotifier::_CRemovable::Delete(__in PUNICODE_STRING _pDevicePath) {

	NTSTATUS nt_status = details::CNotifier_Removable_Operate(_pDevicePath, FALSE);
	if (NT_SUCCESS(nt_status)) {
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] CNotifier::_CRemovable::Delete(): target=%wZ;\n", _pDevicePath
		);
#endif
	}
	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

VOID
CNotifier::CChangeEvent::Notify (
	__in       TFileReference_Ex&     _pTarget, // TODO: to return back CONST specifier;
	__in       ULONG                  _uProcId
	)
{
	PTFs_Evt_Record  pEntry     =  NULL;
	BOOL           bRecorded    = {0};
	eFsObjectType::_e eType     = eFsObjectType::eFsUnknownType;


#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CNotifier::OnChange(): target=%wZ;\n", &_pTarget.FileName
		);
#endif
	pEntry = fg::filter::_imp::CRecAllocator::AllocateEntry();
	if (NULL != pEntry){
		if (fg::filter::_imp::CRecManager::SetEntryTarget(pEntry, &_pTarget.FileName)){ // only target name is available for this type of event

			if (_pTarget.IsFolder)
				eType = eFsObjectType::eFsFolderObject;
			else
				eType = eFsObjectType::eFsFileObject;

			pEntry->Data.OriginatingTime = ctx::CContextTime::GetCurrentTimestamp();
			pEntry->Data.uFsEventType    = eFsEventType::eFsEventAltered;
			pEntry->Data.uFsObjectType   = eType;
			pEntry->Data.CompletionTime  = pEntry->Data.OriginatingTime;
			pEntry->Data.uProcId         = _uProcId;

			bRecorded = fg::filter::_imp::CRecManager::AppendEntry(pEntry);
		}
		if (!bRecorded)
			fg::filter::_imp::CRecAllocator::FreeEntry(pEntry);
	}
}

VOID
CNotifier::CChangeEvent::Notify (
	__in   PCFLT_RELATED_OBJECTS      _pFltObjects,
	__in   ULONG                      _uProcId
	)
{
	PTFs_Evt_Record   pEntry    =  NULL;
	BOOL              bRecorded = {0};
	BOOL              bIsFolder = {0};
	eFsObjectType::_e eType     = eFsObjectType::eFsUnknownType;
	NTSTATUS          nt_status = STATUS_SUCCESS;
	utils::CStringW   u_file_name_or_path_;
#if (0)
	DECLARE_UNICODE_STRING(u_file_name_or_path_, _COMP_PATH);
#endif
	PAGED_CODE();

	pEntry = fg::filter::_imp::CRecAllocator::AllocateEntry();
	if (NULL == pEntry)
		return;

	nt_status = fl_sys::CFile::FileNameUnsafe(_pFltObjects, u_file_name_or_path_);
	if (!NT_SUCCESS(nt_status))
		return;

#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CNotifier::CChangeEvent::Notify(): target=%wZ;\n",(PUNICODE_STRING)u_file_name_or_path_
		);
#endif
	if (fg::filter::_imp::CRecManager::SetEntryTarget(pEntry, u_file_name_or_path_)){

		bIsFolder = fl_sys::CFile::IsDirectory(
							_pFltObjects->Instance, _pFltObjects->FileObject
						);

		if (bIsFolder)
			eType = eFsObjectType::eFsFolderObject;
		else {
			eType = eFsObjectType::eFsFileObject;
			if (NULL != _pFltObjects) {
				pEntry->Data.liSize
				      = fl_sys::CFile::GetSize(_pFltObjects->Instance, _pFltObjects->FileObject);
			}
		}
		pEntry->Data.OriginatingTime = ctx::CContextTime::GetCurrentTimestamp();
		pEntry->Data.uFsEventType    = eFsEventType::eFsEventAltered;
		pEntry->Data.uFsObjectType   = eType;
		pEntry->Data.CompletionTime  = pEntry->Data.OriginatingTime;
		pEntry->Data.uProcId         = _uProcId;

		bRecorded = fg::filter::_imp::CRecManager::AppendEntry(pEntry);
	}
	if (!bRecorded)
		fg::filter::_imp::CRecAllocator::FreeEntry(pEntry);
}

/////////////////////////////////////////////////////////////////////////////

VOID
CNotifier::OnCreate (
	__in       PFLT_FILE_NAME_INFORMATION _pTarget,
	__in       PCFLT_RELATED_OBJECTS      _pFltObjects,
	__in       ULONG                      _uProcId
	)
{
	PTFs_Evt_Record  pEntry     =  NULL;
	BOOL           bRecorded    = {0};
	eFsObjectType::_e eType     = eFsObjectType::eFsUnknownType;

	if (NULL == _pTarget)
		return;

#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CNotifier::OnCreate(): target=%wZ;\n", &_pTarget->Name
		);
#endif
	pEntry = fg::filter::_imp::CRecAllocator::AllocateEntry();
	if (NULL != pEntry){
		if (fg::filter::_imp::CRecManager::SetEntryTarget(pEntry, &_pTarget->Name)){ // only target name is available for this type of event

			if (NULL != _pFltObjects &&
				fl_sys::CFile::IsDirectory(
						_pFltObjects->Instance, _pFltObjects->FileObject
					)){
				eType = eFsObjectType::eFsFolderObject;
			}
			else if (NULL != _pFltObjects) {
				eType = eFsObjectType::eFsFileObject;
				pEntry->Data.liSize
				      = fl_sys::CFile::GetSize(_pFltObjects->Instance, _pFltObjects->FileObject);
			}
			else
				eType = eFsObjectType::eFsFileObject;

			pEntry->Data.OriginatingTime = ctx::CContextTime::GetCurrentTimestamp();
			pEntry->Data.uFsEventType    = eFsEventType::eFsEventCreated;
			pEntry->Data.uFsObjectType   = eType;
			pEntry->Data.CompletionTime  = pEntry->Data.OriginatingTime;
			pEntry->Data.uProcId         = _uProcId;

			bRecorded = fg::filter::_imp::CRecManager::AppendEntry(pEntry);
		}
		if (!bRecorded)
			fg::filter::_imp::CRecAllocator::FreeEntry(pEntry);
	}
}

VOID
CNotifier::OnDelete (
	__in       PFLT_CONTEXT               _pStreamCtx,
	__in       BOOLEAN                    _bIsFile   ,
	__inout    PFLT_CONTEXT               _pTransCtx ,
	__in       PCFLT_RELATED_OBJECTS      _pFltObjects
	)
{
	PTStreamCtx     pStreamCtx = (PTStreamCtx)_pStreamCtx;
	PTTransactCtx   pTransCtx  = (PTTransactCtx)_pTransCtx;
	PTFs_Evt_Record pEntry     =  NULL;
	BOOL            bRecorded  = FALSE;
	eFsObjectType::_e  eType   = eFsObjectType::eFsUnknownType;

	PAGED_CODE();

	if (InterlockedIncrement( &pStreamCtx->IsNotified ) > 1)
		return;

	//
	// adding new entry to log record storage 
	//
	if (_bIsFile){
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] CNotifier::OnDelete(): file name=%wZ;\n", &pStreamCtx->pNameInfo->Name
			);
#endif
		pEntry = fg::filter::_imp::CRecAllocator::AllocateEntry();
		if (NULL != pEntry){
			if (fg::filter::_imp::CRecManager::SetEntryTarget(pEntry, &pStreamCtx->pNameInfo->Name)){ // only target name is available for this type of event

				if (NULL != _pFltObjects &&
					fl_sys::CFile::IsDirectory(
						_pFltObjects->Instance, _pFltObjects->FileObject
					)){
					eType = eFsObjectType::eFsFolderObject;
				}
				else
					eType = eFsObjectType::eFsFileObject;

				pEntry->Data.OriginatingTime = pStreamCtx->CreateTime;
				pEntry->Data.uFsEventType    = eFsEventType::eFsEventDeleted;
				pEntry->Data.uFsObjectType   = eType;
				pEntry->Data.CompletionTime  = ctx::CContextTime::GetCurrentTimestamp();
				pEntry->Data.uProcId         = 0;

				bRecorded = fg::filter::_imp::CRecManager::AppendEntry(pEntry);
			}
			if (!bRecorded)
				fg::filter::_imp::CRecAllocator::FreeEntry(pEntry);
		}
	}
	else{
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] CNotifier::OnDelete(): stream name=%wZ;\n", &pStreamCtx->pNameInfo->Name
			);
#endif
	}
	//
	//  Flag that a delete has been notified on this file/stream.
	//
	if (NULL != pTransCtx){

		CNotifier::OnTransDeleteBegin(
			_pStreamCtx, _pTransCtx, _bIsFile
		);
	}
}

VOID
CNotifier::OnDeleteNotTransacted (
	__in       PFLT_FILE_NAME_INFORMATION _pTarget,
	__in       PCFLT_RELATED_OBJECTS      _pFltObjects,
	__in       ULONG                      _uProcId
	)
{
	PTFs_Evt_Record  pEntry     =  NULL;
	BOOL           bRecorded    = {0};
	ULONG          lEvtType     = eFsEventType::eFsEventUnknown;
	eFsObjectType::_e eType     = eFsObjectType::eFsUnknownType;

	if (NULL == _pTarget)
		return;

#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CNotifier::OnDeleteNotTransacted(): target=%wZ;\n", &_pTarget->Name
		);
#endif
	pEntry = fg::filter::_imp::CRecAllocator::AllocateEntry();
	if (NULL != pEntry){
		if (fg::filter::_imp::CRecManager::SetEntryTarget(pEntry, &_pTarget->Name)){

			if (NULL != _pFltObjects &&
				fl_sys::CFile::IsDirectory(
						_pFltObjects->Instance, _pFltObjects->FileObject
					)){
				eType = eFsObjectType::eFsFolderObject;
			}
			else
				eType = eFsObjectType::eFsFileObject;

			lEvtType  = eFsEventType::eFsEventDeleted;

			pEntry->Data.CompletionTime  = ctx::CContextTime::GetCurrentTimestamp();
			pEntry->Data.OriginatingTime = pEntry->Data.CompletionTime;
			pEntry->Data.uFsEventType    = lEvtType;
			pEntry->Data.uFsObjectType   = eType;
			pEntry->Data.uProcId         = _uProcId;

			bRecorded = fg::filter::_imp::CRecManager::AppendEntry(pEntry);
		}
		if (!bRecorded)
			fg::filter::_imp::CRecAllocator::FreeEntry(pEntry);
	}
}

VOID
CNotifier::OnRename (
	__in       PFLT_FILE_NAME_INFORMATION _pSource,
	__in       PFLT_FILE_NAME_INFORMATION _pTarget,
	__in       PCFLT_RELATED_OBJECTS      _pFltObjects,
	__in       ULONG                      _uProcId
	)
{
	static
	UNICODE_STRING  pRecycle     = RTL_CONSTANT_STRING(L"$Recycle");
	PTFs_Evt_Record pEntry       =  NULL;
	BOOL            bRecorded    = {0};
	ULONG           lEvtType     = eFsEventType::eFsEventUnknown;
	eFsObjectType::_e  eType     = eFsObjectType::eFsUnknownType;

	if (NULL == _pSource ||
	    NULL == _pTarget)
		return;

#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CNotifier::OnRename(): src=%wZ; tgt=%wZ;\n", &_pSource->Name, &_pTarget->Name
		);
#endif
	pEntry = fg::filter::_imp::CRecAllocator::AllocateEntry();
	if (NULL != pEntry){

		if (fg::filter::_imp::CRecManager::SetEntryTarget(pEntry, &_pTarget->Name)){
			fg::filter::_imp::CRecManager::SetEntrySource(pEntry, &_pSource->Name);

			if (!fl_sys::CComparator::ComparePaths(_pSource, _pTarget, TRUE )) lEvtType |= eFsEventType::eFsEventMoved;
			if (!fl_sys::CComparator::CompareNames(_pSource, _pTarget, FALSE)) lEvtType |= eFsEventType::eFsEventRenamed;
			
			if (-1 != utils::CStringW::Find(&_pTarget->Name, &pRecycle)){
				lEvtType &=~eFsEventType::eFsEventMoved;
				lEvtType |= eFsEventType::eFsEventMovedToRecycle;
			}
			else
			if (-1 != utils::CStringW::Find(&_pSource->Name, &pRecycle)){
				lEvtType &=~eFsEventType::eFsEventMoved;
				lEvtType |= eFsEventType::eFsEventRestoredFromRecycle;
			}

			if (NULL != _pFltObjects &&
				fl_sys::CFile::IsDirectory(
						_pFltObjects->Instance, _pFltObjects->FileObject
					)){
				eType = eFsObjectType::eFsFolderObject;
			}
			else if (NULL != _pFltObjects) {
				eType = eFsObjectType::eFsFileObject;
				pEntry->Data.liSize
				      = fl_sys::CFile::GetSize(_pFltObjects->Instance, _pFltObjects->FileObject);
			}
			else
				eType = eFsObjectType::eFsFileObject;

			pEntry->Data.CompletionTime  = ctx::CContextTime::GetCurrentTimestamp();
			pEntry->Data.OriginatingTime = pEntry->Data.CompletionTime;
			pEntry->Data.uFsEventType    = lEvtType;
			pEntry->Data.uFsObjectType   = eType;
			pEntry->Data.uProcId         = _uProcId;

			bRecorded = fg::filter::_imp::CRecManager::AppendEntry(pEntry);
		}
		if (!bRecorded)
			fg::filter::_imp::CRecAllocator::FreeEntry(pEntry);
	}
}

VOID
CNotifier::OnReplace (
	__in       PFLT_FILE_NAME_INFORMATION _pSource,
	__in       PFLT_FILE_NAME_INFORMATION _pTarget,
	__in       PCFLT_RELATED_OBJECTS      _pFltObjects,
	__in       ULONG                      _uProcId
	)
{
	PTFs_Evt_Record pEntry       =  NULL;
	BOOL            bRecorded    = {0};
	ULONG           lEvtType     = eFsEventType::eFsEventUnknown;
	eFsObjectType::_e  eType     = eFsObjectType::eFsUnknownType;

	if (NULL == _pSource ||
		NULL == _pTarget)
		return;

#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CNotifier::OnReplace(): tgt=%wZ by src=%wZ;\n", &_pTarget->Name, &_pSource->Name
		);
#endif
	pEntry = fg::filter::_imp::CRecAllocator::AllocateEntry();

	if (NULL != pEntry){
		if (fg::filter::_imp::CRecManager::SetEntryTarget(pEntry, &_pTarget->Name)){
			fg::filter::_imp::CRecManager::SetEntrySource(pEntry, &_pSource->Name);

			lEvtType = eFsEventType::eFsEventReplaced;

			if (NULL != _pFltObjects &&
				fl_sys::CFile::IsDirectory(
						_pFltObjects->Instance, _pFltObjects->FileObject
					)){
				eType = eFsObjectType::eFsFolderObject;
			}
			else if (NULL != _pFltObjects) {
				eType = eFsObjectType::eFsFileObject;
				pEntry->Data.liSize
				      = fl_sys::CFile::GetSize(_pFltObjects->Instance, _pFltObjects->FileObject);
			}
			else
				eType = eFsObjectType::eFsFileObject;

			pEntry->Data.CompletionTime  = ctx::CContextTime::GetCurrentTimestamp();
			pEntry->Data.OriginatingTime = pEntry->Data.CompletionTime;
			pEntry->Data.uFsEventType    = lEvtType;
			pEntry->Data.uFsObjectType   = eType;
			pEntry->Data.uProcId         = _uProcId;

			bRecorded = fg::filter::_imp::CRecManager::AppendEntry(pEntry);
		}
		if (!bRecorded)
			fg::filter::_imp::CRecAllocator::FreeEntry(pEntry);
	}
}

NTSTATUS
CNotifier::OnTransDeleteBegin (
	__inout    PFLT_CONTEXT               _pStreamCtx,
	__inout    PFLT_CONTEXT               _pTransCtx ,
	__in       BOOLEAN                    _bFileDelete
	)
{
	PTDeleteNotifyData pNotify     = NULL;
	PTStreamCtx        pStreamCtx  = (PTStreamCtx)_pStreamCtx;
	PTTransactCtx      pTransCtx   = (PTTransactCtx)_pTransCtx;
	NTSTATUS           nt_status   = STATUS_SUCCESS;

	PAGED_CODE();

	if (NULL == pTransCtx
	 || NULL == pTransCtx->DeleteData.pSyncResource)
		return (nt_status = STATUS_INVALID_PARAMETER);

	if (NULL == pStreamCtx)
		return (nt_status = STATUS_INVALID_PARAMETER);

	pNotify  = (PTDeleteNotifyData)ExAllocatePoolWithTag(
					PagedPool, sizeof(TDeleteNotifyData), DELETE_NOTIFY_POOL_TAG
				);

	if (NULL == pNotify)
		return (nt_status = STATUS_INSUFFICIENT_RESOURCES);

	RtlZeroMemory( pNotify, sizeof(TDeleteNotifyData) );

	FltReferenceContext( pStreamCtx );
	pNotify->pContext      = pStreamCtx;
	pNotify->bFileDelete   = _bFileDelete;

	FltAcquireResourceExclusive( pTransCtx->DeleteData.pSyncResource );

	InsertTailList( &pTransCtx->DeleteData.NotifyList, &pNotify->links );

	FltReleaseResource( pTransCtx->DeleteData.pSyncResource );

	return nt_status;
}

VOID
CNotifier::OnTransDeleteEnd (
	__in       PTDeleteNotifyData         _pNotify,
	__in       BOOLEAN                    _bCommitted
	)
{
	PTStreamCtx    pStreamCtx = NULL;

	PAGED_CODE();

	if (NULL == _pNotify)
		return;

	pStreamCtx = (PTStreamCtx)_pNotify->pContext;
	//
	// TO DO: adding new entry to log record storage 
	//
	if (_bCommitted && _pNotify->bFileDelete){
#if DBG
		DbgPrint( "__fg_fs: [INFO] CNotifier::OnTransDeleteEnd::commit(): file name=%wZ;\n", &pStreamCtx->pNameInfo->Name );
#endif
	}
	else if (_bCommitted){
#if DBG
		DbgPrint( "__fg_fs: [INFO] CNotifier::OnTransDeleteEnd::commit(): stream name=%wZ;\n", &pStreamCtx->pNameInfo->Name );
#endif
	}
	else if (!_bCommitted){
#if DBG
		DbgPrint( "__fg_fs: [INFO] CNotifier::OnTransDeleteEnd::rollback(): cancelled=%wZ;\n", &pStreamCtx->pNameInfo->Name );
#endif
	}
}