#ifndef _FGFSFILEOBJECT_H_95A58A4D_236D_4ca1_9D87_E95E755EE7CD_INCLUDED
#define _FGFSFILEOBJECT_H_95A58A4D_236D_4ca1_9D87_E95E755EE7CD_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Aug-2017 at 2:43:57a, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian mini-filter driver file object wrapper interface declaration file.
*/
#include "FG_FsFilterIface.h"

//
//  This helps us deal with ReFS 128-bit file IDs and NTFS 64-bit file IDs.
//
typedef union _FILE_REFERENCE
{
	struct
	{
		ULONGLONG   Value;          //  The 64-bit file ID lives here.
		ULONGLONG   UpperZeroes;    //  In a 64-bit file ID this will be 0.
	} FileId64;

	UCHAR   FileId128[16];          //  The 128-bit file ID lives here.
}
TFileReference, *PTFileReference;

typedef struct _FILE_REFERENCE_EX
{
	TFileReference  FileId;
	UNICODE_STRING  FileName;
	BOOLEAN         IsFolder;
}
TFileReference_Ex, *PTFileReference_Ex;

#define SizeofFileId(FID) (                 \
	((FID).FileId64.UpperZeroes == 0ll) ?   \
		sizeof((FID).FileId64.Value)    :   \
		sizeof((FID).FileId128)             \
	)

#define CPLUSPLUS

#ifdef CPLUSPLUS
namespace fl_sys {
#endif
/////////////////////////////////////////////////////////////////////////////

//
// original idea is here:
// https://stackoverflow.com/questions/27939882/fast-crc-algorithm
//

#define CRC_CHUNK_SIZE 1024

#ifdef CPLUSPLUS
	class CCrc32{
	public:
		/*
		Routine Description:
			This routine calculates fast CRC32 on the buffer provided;
			the length of a buffer part being calculated is limited to CRC_CHUNK_SIZE;
			if a buffer has a smaller size than CRC_CHUNK_SIZE, it will be calculated entirely;
		*/
		static
		UINT
		CalculateOnBuffer(
			__in  const PBYTE     _pBuffer,
			__in  const UINT      _nBufferLen
			);
		/*
		Routine Description:
			This routine calculates fast CRC32 on the file content;
			the length of a file content part being calculated is limited to CRC_CHUNK_SIZE;
			if a file has a smaller size than CRC_CHUNK_SIZE, the file will be calculated entirely;

			This routine is intended for calculating CRC on files being moved across different volumes
			in order to identify the file between two events, 'delete' and 'create', that are generated in
			such cases by file system;
		*/
		static
		UINT
		CalculateOnFile(
			__in        PFLT_INSTANCE             _pInstance    ,
			__in        PFILE_OBJECT              _pFileObject    // file object, which CRC is calculated for;
			);
	};
#endif

#ifdef CPLUSPLUS

	class CComparator{
	public:
		/*
		Routine Description:
			This routine compares two object/file names provided (w/o object path);
			returns TRUE if the same, otherwise, FALSE;
		*/
		static
		BOOLEAN
		CompareNames(
			__in       PFLT_FILE_NAME_INFORMATION _pSource,
			__in       PFLT_FILE_NAME_INFORMATION _pTarget,
			__in       CONST BOOL                 _bParseComponents
			);
		/*
		Routine Description:
			This routine compares two paths provided (w/o file name);
			returns TRUE if the same, otherwise, FALSE;
		*/
		static
		BOOLEAN
		ComparePaths(
			__in       PFLT_FILE_NAME_INFORMATION _pSource,
			__in       PFLT_FILE_NAME_INFORMATION _pTarget,
			__in       CONST BOOL                 _bParseComponents
			);
	};

#endif

#ifdef CPLUSPLUS

	class CFile {
	public:
		/*
			Converts UNC path to DOS one;
		*/
		static
		NTSTATUS
		ConvertUncToDos(
			__in        PFILE_OBJECT CONST        _pFileObject,
			__inout     utils::CStringW&          _pDosPath
			);

		/*
			Get file name if possible (in UNC by default);
		*/
		static
		NTSTATUS
		FileName (
			__in        PFLT_CALLBACK_DATA        _pFltData ,
			__inout     utils::CStringW&          _pFileName
			);

		/*
			Get file name if possible (in UNC by default);
		*/
		static
		NTSTATUS
		FileNameUnsafe (
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects,
			__inout     utils::CStringW&          _refFileName
			);

		/*
			Get file name if possible (in UNC by default);
		*/
		static
		NTSTATUS
		FileNameUnsafe (
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects,
			__inout     UNICODE_STRING&           _refFileName
			);


		/*
		Routine Description:
			This routine obtains the File ID and saves it in the stream context.

		Arguments:
			_pFltData     - Pointer to FLT_CALLBACK_DATA.
			_pContext     - Pointer to stream context that will receive the file ID.
		Return Value:
			Returns statuses forwarded from FltQueryInformationFile, including
			STATUS_FILE_DELETED.
		*/
		static
		NTSTATUS
		GetId (
			__in        PFLT_INSTANCE             _pInstance  ,
			__in        PFILE_OBJECT              _pFileObject,
			__inout     TFileReference&           _File_Ref
			);
		/*
		Routine Description:
			This helper routine builds a string used to open a file by its ID.
			It will assume the file ID is properly loaded in the stream context
			(StreamContext->FileId).

		Arguments:
			_pFltData    - Pointer to FLT_CALLBACK_DATA.
			_pFltObjects - Pointer to the FLT_RELATED_OBJECTS data structure containing
			               opaque handles to this filter, instance, its associated volume and
			               file object.
			_pContext    - Pointer to the stream context.
			_pString     - Pointer to UNICODE_STRING (output).

		Return Value:
			Return statuses forwarded by utils::AllocateUnicodeString or
			FltGetInstanceContext.
		*/
		static
		NTSTATUS
		GetIdAsText (
			__in        PFLT_INSTANCE             _pInstance    ,
			__in        PFILE_OBJECT              _pFileObject  ,
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects  ,
			__inout     PUNICODE_STRING           _pFieldIdString
			);
		/*
			Gets values for file open event
		*/
		static
		ULONG
		GetDispose (
			__in        CONST FLT_PARAMETERS      _nParameters
			);

		static
		VOID
		GetOptions (
			__in        CONST FLT_PARAMETERS      _nParameters  ,
			__inout     ULONG&                    _nDispose     ,
			__inout     ULONG&                    _nOptions
			);
		/*
		Routine Description:
			This routine returns a file size; if file object is a directory
			or an error occurs, 0 size is returned;
		*/
		static
		LARGE_INTEGER
		GetSize(
			__in        PFLT_INSTANCE             _pInstance    ,
			__in        PFILE_OBJECT              _pFileObject
			);
		/*
		Routine Description:
			This routine returns whether a file was deleted. It is called
			after an alternate data stream is deleted. This needs to
			be done for the case when the last outstanding handle to a delete-pending
			file is a handle to a delete-pending alternate data stream. When that
			handle is closed, the whole file goes away, and we want to report a whole
			file deletion, not just an alternate data stream deletion.

		Arguments:
			_pFltData       - Pointer to the filter callbackData that is passed to us.
			_pFltObjects    - Pointer to the FLT_RELATED_OBJECTS data structure containing
							  opaque handles to this filter, instance, its associated volume and
							  file object.
			_pStreamCtx     - Pointer to the stream context.
			_bIsTransaction - TRUE if in a transaction, FALSE otherwise.

		Return Value:
			STATUS_FILE_DELETED - The whole file was deleted.
			Successful status   - The file still exists, this was probably just a named
			                      data stream being deleted.
			Anything else       - Failure in finding out if the file was deleted.
		*/
		static
		NTSTATUS
		IsDeleted (
			__in        PFLT_INSTANCE             _pInstance    ,
			__in        PFILE_OBJECT              _pFileObject  ,
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects  ,
			__in        PFLT_CONTEXT              _pContext     , // actually, stream context pointer
			__in        BOOLEAN                   _bIsTransaction
			);
		/*
		Routine Description:
			This routine checks the file object provided against directory type;
			returns TRUE if the object is a directory, otherwise, FALSE;
		*/
		static
		BOOLEAN
		IsDirectory (
			__in        PFLT_INSTANCE             _pInstance    ,
			__in        PFILE_OBJECT              _pFileObject
			);
		/*
		Routine Description:
			This routine checks the file object existence;
			returns TRUE if the object exists, otherwise, FALSE;
		*/
		static
		BOOLEAN
		IsExist (
			__in        PUNICODE_STRING           _pObjectPath
			);
		/*
		Routine Description:
			This helper routine detects a deleted file by attempting to open it using 
			its file ID.
			If the file is successfully opened this routine closes the file before returning.

		Arguments:
			_pFltData    - Pointer to FLT_CALLBACK_DATA.
			_pFltObjects - Pointer to the FLT_RELATED_OBJECTS data structure containing
						   opaque handles to this filter, instance, its associated volume and
						   file object.
			_pStreamCtx  - Pointer to the stream context.

		Return Value:
			STATUS_FILE_DELETED      - Returned through DfBuildFileIdString if the file has
									   been deleted.
			STATUS_INVALID_PARAMETER - Returned from FltCreateFileEx2 when opening by ID
									   a file that doesn't exist.
			STATUS_DELETE_PENDING    - The file has been set to be deleted when the last handle
									   goes away, but there are still open handles.

			Also any other NTSTATUS is returned from FltCreateFileEx2,
			or FltClose.
		*/
		static
		NTSTATUS
		OpenById (
			__in        PFLT_INSTANCE             _pInstance    ,
			__in        PFILE_OBJECT              _pFileObject  ,
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects  ,
			__inout     HANDLE&                   _hFile
			);
		/*
		Routine Description:
			This routine reads data of the specified length from a file;
		*/
		static
		NTSTATUS
		ReadContent (
			__in        PFLT_INSTANCE             _pInstance    ,
			__in        PFILE_OBJECT              _pFileObject  , // contains file info, which data will be read from;
			__inout     PBYTE                     _pBuffer      , // a buffer for data, it is already allocated by a caller
			__in CONST  UINT                      _pBufferLen   , // a buffer size
			__inout     PULONG                    _nBytesRead     // number of bytes that are actually read from the file;
			);
		/*
		Routine Description:
			This routine gets and parses the file name information, obtains the File
			ID and saves them in the stream context.

		Arguments:
			Data          - Pointer to FLT_CALLBACK_DATA.
			StreamContext - Pointer to stream context that will receive the file
							information.

		Return Value:
			Returns statuses forwarded from Flt(Get|Parse)FileNameInformation or
			FltQueryInformationFile.
		*/
		static
		NTSTATUS
		SetInfoToCtx (
			__in        PFLT_CALLBACK_DATA        _pFltData     ,
			__in        PFLT_CONTEXT              _pContext     , // actually, stream context pointer
			__in        BOOLEAN                   _bCheckPass     // if TRUE, the routine checks the file object name in the list for watching
			);
	};

#endif

#ifdef CPLUSPLUS
	//
	// smart pointer for file information structure
	//
	class CFileInfoPtr{
	private:
		PFLT_FILE_NAME_INFORMATION  m_pInfo;
	public:
		CFileInfoPtr(void);
		CFileInfoPtr(const PFLT_FILE_NAME_INFORMATION);
		~CFileInfoPtr(void);
	public:
		operator PFLT_FILE_NAME_INFORMATION (void)const;
		operator PFLT_FILE_NAME_INFORMATION*(void);
		operator PFLT_FILE_NAME_INFORMATION&(void);
	public:
		CFileInfoPtr& operator=(const PFLT_FILE_NAME_INFORMATION);
	public:
		NTSTATUS    Attach(const PFLT_FILE_NAME_INFORMATION);
		PFLT_FILE_NAME_INFORMATION
		            Detach(void);
		bool        IsValid(void)const;
		PFLT_FILE_NAME_INFORMATION
		            Ptr(void)const;
	private:
		CFileInfoPtr(const CFileInfoPtr&);
		CFileInfoPtr& operator=(const CFileInfoPtr&);
	};
#endif

#ifdef CPLUSPLUS
}
#endif

#endif/*_FGFSFILEOBJECT_H_95A58A4D_236D_4ca1_9D87_E95E755EE7CD_INCLUDED*/