#ifndef _FGFSCTXMANAGER_H_C6BE2C6E_68D8_410e_9032_7CC1BAAA6A72_INCLUDED
#define _FGFSCTXMANAGER_H_C6BE2C6E_68D8_410e_9032_7CC1BAAA6A72_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail,com) on 6-Dec-2017 at 7:56:13p, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian mini-filter driver event handler context data manager interface declaration file.
*/
#include "FG_FsCtxIface.h"

namespace ctx {

	class CCtxFileModifyMan {
	private:
		static
		NTSTATUS
		Create(
			__in        PFLT_CALLBACK_DATA        _pClbTarget  , // target instance and file object are mandatory;
			__inout_opt PFLT_CONTEXT*             _ppContext
		);

		static
		NTSTATUS
		SetContext(
			__in        PFLT_CALLBACK_DATA        _pClbTarget  ,
			__in        PFLT_CONTEXT              _pNewContext ,
			__inout_opt PFLT_CONTEXT*             _ppOldContext
		);

	public:
		static
		VOID FLTAPI
		Destroy (
			__in        PFLT_CONTEXT              _pContext
		);

		/*
			A context will be created if not found;
		*/
		static
		NTSTATUS
		GetContext(
			__in        PFLT_CALLBACK_DATA        _pClbTarget  ,
			__inout_opt PFLT_CONTEXT*             _ppContext
		);
	};

	class CCtxFileOperateMan {
	private:
		static
		NTSTATUS
		Create(
			__inout_opt PFLT_CONTEXT*             _ppContext
		);

		static
		NTSTATUS
		SetContext(
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects ,
			__in        PFLT_CONTEXT              _pNewContext ,
			__inout_opt PFLT_CONTEXT*             _ppOldContext
		);

	public:
		static
		VOID FLTAPI
		Destroy (
			__in        PFLT_CONTEXT              _pContext
		);

		static
		NTSTATUS
		GetContext(
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects ,
			__in        BOOL                      _bCreate     ,
			__inout_opt PFLT_CONTEXT*             _ppContext
		);

	};

	class CCtxStreamMan {
	private:
		static
		NTSTATUS
		Create(
			__inout_opt PFLT_CONTEXT*             _ppContext
		);

		static
		NTSTATUS
		Replace(
			__in        PFLT_CONTEXT              _pNewContext , // new context
			__in        PFLT_CONTEXT              _pPrvContext   // provided one
		);

	public:
		static
		VOID FLTAPI
		Destroy (
			__in        PFLT_CONTEXT              _pContext
		);

		static
		NTSTATUS
		GetContext(
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects , // driver instance is required
			__in        PFILE_OBJECT              _pTarget     , // target file object
			__in        BOOL                      _bCreate     ,
			__inout_opt PFLT_CONTEXT*             _ppContext
		);

		static
		NTSTATUS
		SetContext(
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects , // driver instance is required
			__in        PFILE_OBJECT              _pTarget     , // target file object
			__inout_opt PFLT_CONTEXT*             _ppContext     // will be replaced with existing one if necessary
		);
	};

	//
	// This class is called from PreCreate and PreSetInfo fore enlisting to transaction object,
	// which is taken from callback data;
	//
	class CCtxTxDeleteMan {
	private:
		static
		NTSTATUS
		Create(
			__inout_opt PFLT_CONTEXT*             _ppContext
		);

		static
		NTSTATUS
		SetContext(
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects ,
			__in        PKTRANSACTION             _pTransaction, // target object, which will get a context
			__in        PFLT_CONTEXT              _pNewContext ,
			__inout_opt PFLT_CONTEXT*             _ppOldContext
		);

	public:
		static
		VOID FLTAPI
		Destroy (
			__in        PFLT_CONTEXT              _pContext
		);

		static
		NTSTATUS
		GetContext(
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects , // must contain driver instance and transaction values;
			__in        PKTRANSACTION             _pTransaction,
			__inout_opt PFLT_CONTEXT*             _ppContext
		);
	};

	class CCtxTxModifyMan {
	private:
		static
		NTSTATUS
		Create(
			__inout_opt PFLT_CONTEXT*             _ppContext
		);

		static
		NTSTATUS
		SetContext(
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects , // must contain driver instance and transaction values;
			__in        PFLT_CONTEXT              _pNewContext ,
			__inout_opt PFLT_CONTEXT*             _ppOldContext
		);

	public:
		static
		VOID FLTAPI
		Destroy (
			__in        PFLT_CONTEXT              _pContext
		);

		static
		NTSTATUS
		GetContext(
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects , // must contain driver instance and transaction values;
			__inout_opt PFLT_CONTEXT*             _ppContext
		);
	};
}

#endif/*_FGFSCTXMANAGER_H_C6BE2C6E_68D8_410e_9032_7CC1BAAA6A72_INCLUDED*/