/*
	Created by Tech_dog (ebontrop@gmail,com) on 7-Dec-2017 at 10:32:08p, UTC+7, Phuket, Rawai, Thursday;
	This is File Guardian mini-filter driver event handler context data manager interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsCtxMans.h"
#include "FG_FsCtxAlloca.h"

using namespace ctx;

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CCtxFileModifyMan::Create(
	__in        PFLT_CALLBACK_DATA        _pClbTarget  ,
	__inout_opt PFLT_CONTEXT*             _ppContext
)
{
	NTSTATUS nt_status = STATUS_SUCCESS;

	if (NULL == _ppContext)
		return STATUS_INVALID_PARAMETER;

	nt_status = ctx::CFileModifyCtxAlloca::Allocate(
					_pClbTarget, _ppContext
				);

	return nt_status;
}

NTSTATUS
CCtxFileModifyMan::SetContext(
	__in        PFLT_CALLBACK_DATA        _pClbTarget  ,
	__in        PFLT_CONTEXT              _pNewContext ,
	__inout_opt PFLT_CONTEXT*             _ppOldContext
)
{
	NTSTATUS nt_status = STATUS_SUCCESS;

	if (NULL == _pClbTarget  ||
	    NULL == _pNewContext ||
	    NULL == _ppOldContext)
		return STATUS_INVALID_PARAMETER;

	nt_status = FltSetFileContext(
					_pClbTarget->Iopb->TargetInstance, _pClbTarget->Iopb->TargetFileObject, FLT_SET_CONTEXT_KEEP_IF_EXISTS, _pNewContext, _ppOldContext
				);
	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

VOID FLTAPI
CCtxFileModifyMan::Destroy (
	__in        PFLT_CONTEXT              _pContext
)
{
	if (NULL == _pContext)
		return NOTHING;
	else
		return ctx::CFileModifyCtxAlloca::Free( _pContext );
}

NTSTATUS
CCtxFileModifyMan::GetContext(
	__in        PFLT_CALLBACK_DATA        _pClbTarget  ,
	__inout_opt PFLT_CONTEXT*             _ppContext
)
{
	NTSTATUS    nt_status = STATUS_SUCCESS;

	if (NULL == _pClbTarget)
		return (nt_status = STATUS_INVALID_PARAMETER);

	nt_status = FltGetFileContext(
					_pClbTarget->Iopb->TargetInstance, _pClbTarget->Iopb->TargetFileObject, _ppContext
				);

	if (STATUS_NOT_FOUND == nt_status) {

		nt_status = CCtxFileModifyMan::Create(_pClbTarget, _ppContext);

		if (!NT_SUCCESS(nt_status))
			return nt_status;

		PFLT_CONTEXT pAssigned = NULL;

		nt_status = CCtxFileModifyMan::SetContext(_pClbTarget, *_ppContext, &pAssigned);

		if (STATUS_FLT_CONTEXT_ALREADY_DEFINED == nt_status){
			nt_status = STATUS_SUCCESS;
			FltReleaseContext(*_ppContext); *_ppContext = pAssigned;
		}
		else if (!NT_SUCCESS( nt_status )) {
			FltReleaseContext(*_ppContext); *_ppContext = NULL;
		}
	}

	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CCtxFileOperateMan::Create(
	__inout_opt PFLT_CONTEXT*             _ppContext
)
{
	NTSTATUS nt_status = STATUS_SUCCESS;

	if (NULL == _ppContext)
		return STATUS_INVALID_PARAMETER;

	nt_status = ctx::CFileOperCtxAlloca::Allocate(_ppContext);

	return nt_status;
}

NTSTATUS
CCtxFileOperateMan::SetContext(
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects ,
	__in        PFLT_CONTEXT              _pNewContext ,
	__inout_opt PFLT_CONTEXT*             _ppOldContext
)
{
	if (NULL == _pFltObjects)
		return STATUS_INVALID_PARAMETER;

	return FltSetFileContext(
				_pFltObjects->Instance, _pFltObjects->FileObject, FLT_SET_CONTEXT_KEEP_IF_EXISTS, _pNewContext, _ppOldContext
			);
}

/////////////////////////////////////////////////////////////////////////////

VOID FLTAPI
CCtxFileOperateMan::Destroy (
	__in        PFLT_CONTEXT              _pContext
)
{
	if (NULL == _pContext)
		return NOTHING;
	else
		return ctx::CFileOperCtxAlloca::Free( _pContext );
}

NTSTATUS
CCtxFileOperateMan::GetContext(
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects ,
	__in        BOOL                      _bCreate     ,
	__inout_opt PFLT_CONTEXT*             _ppContext
)
{
	NTSTATUS    nt_status = STATUS_SUCCESS;

	if (NULL == _pFltObjects)
		return (nt_status = STATUS_INVALID_PARAMETER);

	nt_status = FltGetFileContext(
					_pFltObjects->Instance, _pFltObjects->FileObject, _ppContext
				);

	if (STATUS_NOT_FOUND == nt_status && _bCreate) {

		nt_status = CCtxFileOperateMan::Create(_ppContext);

		if (!NT_SUCCESS(nt_status))
			return nt_status;

		PFLT_CONTEXT pAssigned = NULL;

		nt_status = CCtxFileOperateMan::SetContext(_pFltObjects, *_ppContext, &pAssigned);

		if (NT_SUCCESS( nt_status ) && NULL != pAssigned) {
			FltReleaseContext(*_ppContext); *_ppContext = pAssigned; // returns an existing context that is associated with the file object;
		}
	}

	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CCtxStreamMan::Create(
	__inout_opt PFLT_CONTEXT*             _ppContext
)
{
	NTSTATUS nt_status = STATUS_SUCCESS;
	
	PAGED_CODE();
	if (NULL == _ppContext)
		return (nt_status = STATUS_INVALID_PARAMETER);

	nt_status = ctx::CStreamCtxAlloca::Allocate(_ppContext);

	return nt_status;
}

NTSTATUS
CCtxStreamMan::Replace(
	__in        PFLT_CONTEXT              _pNewContext , // new context
	__in        PFLT_CONTEXT              _pPrvContext   // provided one
)
{
	if (NULL == _pNewContext)
		return STATUS_SUCCESS;

	if (_pPrvContext == _pNewContext){

		FltReleaseContext(
				_pNewContext
			);
		return STATUS_SUCCESS;
	}

	if (NULL != _pPrvContext)
		FltReleaseContext(_pPrvContext);

	_pPrvContext = _pNewContext;
	return STATUS_SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////

VOID FLTAPI
CCtxStreamMan::Destroy (
	__in        PFLT_CONTEXT              _pContext
)
{
	if (NULL == _pContext)
		return;

	ctx::CStreamCtxAlloca::Free(_pContext); _pContext = NULL;
}

NTSTATUS
CCtxStreamMan::GetContext(
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects ,
	__in        PFILE_OBJECT              _pTarget     ,
	__in        BOOL                      _bCreate     ,
	__inout_opt PFLT_CONTEXT*             _ppContext
)
{
	PFLT_CONTEXT pContext     = NULL;
	NTSTATUS     nt_status    = STATUS_SUCCESS;
	if (NULL == _ppContext)
		return  (nt_status    = STATUS_INVALID_PARAMETER);

	if (NULL == _pTarget)
		return  (nt_status    = STATUS_INVALID_PARAMETER);

	//
	// tries to get existing context;
	//
	nt_status = FltGetStreamContext(_pFltObjects->Instance, _pTarget, &pContext );
	if (NT_SUCCESS(nt_status)){
		
		CCtxStreamMan::Replace(pContext, *_ppContext);
		return nt_status;
	}

	if (STATUS_NOT_FOUND == nt_status && _bCreate) {
		nt_status = ctx::CStreamCtxAlloca::Allocate(&pContext);

		if (!NT_SUCCESS(nt_status))
			return nt_status;

		PFLT_CONTEXT pAssigned = NULL;

		nt_status = CCtxStreamMan::SetContext(_pFltObjects, _pTarget, &pContext);

		if (!NT_SUCCESS( nt_status )) {

			FltReleaseContext(pContext); pContext = NULL;

			if (STATUS_FLT_CONTEXT_ALREADY_DEFINED == nt_status){

				CCtxStreamMan::Replace(pAssigned, *_ppContext);

				nt_status = STATUS_SUCCESS;
			}
		}
	}

	return nt_status;
}

NTSTATUS
CCtxStreamMan::SetContext(
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects ,
	__in        PFILE_OBJECT              _pTarget     ,
	__inout_opt PFLT_CONTEXT*             _ppContext
)
{
	NTSTATUS    nt_status = STATUS_SUCCESS;

#pragma region __things_that_never_should_occur__

	if (NULL == _pFltObjects)
		return STATUS_INVALID_PARAMETER;

	if (NULL == _pTarget)
		return STATUS_INVALID_PARAMETER;

	if (NULL == _ppContext ||
	    NULL ==*_ppContext )
		return STATUS_INVALID_PARAMETER;

#pragma endregion __seems_everything_is_fine__

	PFLT_CONTEXT pAssigned = NULL;

	nt_status = FltSetStreamContext(
				_pFltObjects->Instance, _pTarget, FLT_SET_CONTEXT_KEEP_IF_EXISTS, *_ppContext, &pAssigned
			);

	if (STATUS_FLT_CONTEXT_ALREADY_DEFINED == nt_status) {

		FltReleaseContext(*_ppContext);
		*_ppContext = pAssigned;

		nt_status = STATUS_SUCCESS;
	}

	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CCtxTxDeleteMan::Create(
	__inout_opt PFLT_CONTEXT*             _ppContext
)
{
	NTSTATUS nt_status = STATUS_SUCCESS;
	
	PAGED_CODE();
	if (NULL == _ppContext)
		return (nt_status = STATUS_INVALID_PARAMETER);

	nt_status = ctx::CTxDeleteCtxAlloca::Allocate(_ppContext);

	return nt_status;
}

NTSTATUS
CCtxTxDeleteMan::SetContext(
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects ,
	__in        PKTRANSACTION             _pTransaction,
	__in        PFLT_CONTEXT              _pNewContext ,
	__inout_opt PFLT_CONTEXT*             _ppOldContext
)
{
	NTSTATUS nt_status = STATUS_SUCCESS;

	PAGED_CODE();

	if (NULL == _pFltObjects ||
	    NULL == _pTransaction )
		return STATUS_INVALID_PARAMETER;

	if (NULL == _pNewContext)
		return STATUS_INVALID_PARAMETER;

	nt_status = FltSetTransactionContext(
					_pFltObjects->Instance, _pTransaction, FLT_SET_CONTEXT_KEEP_IF_EXISTS, _pNewContext, _ppOldContext
				);

	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

VOID FLTAPI
CCtxTxDeleteMan::Destroy (
	__in        PFLT_CONTEXT              _pContext
)
{
	if (NULL == _pContext)
		return;

	PTTransactCtx pTxContext  = (PTTransactCtx)_pContext;

	if (NULL != pTxContext->ModifyData.pTransaction){

		ObDereferenceObject(pTxContext->ModifyData.pTransaction);
		pTxContext->ModifyData.pTransaction = NULL;
	}
	//
	// TODO: needs to review transaction list destruction! Now it is destructed on processing a transaction;
	//
	pTxContext->ModifyData.bIsInitialized = FALSE;

	ctx::CTxDeleteCtxAlloca::Free(_pContext); _pContext = NULL;

	return NOTHING;
}

NTSTATUS
CCtxTxDeleteMan::GetContext(
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects ,
	__in        PKTRANSACTION             _pTransaction,
	__inout_opt PFLT_CONTEXT*             _ppContext
)
{
	NTSTATUS     nt_status    = STATUS_SUCCESS;

	if (NULL == _ppContext   ||
	    NULL == _pFltObjects ||
	    NULL == _pTransaction )
		return  (nt_status    = STATUS_INVALID_PARAMETER);

	nt_status = FltGetTransactionContext(_pFltObjects->Instance, _pTransaction, _ppContext );

	if (STATUS_NOT_FOUND == nt_status) {
		nt_status = ctx::CTxDeleteCtxAlloca::Allocate(_ppContext);

		if (!NT_SUCCESS(nt_status))
			return nt_status;

		PFLT_CONTEXT pAssigned = NULL;

		nt_status = CCtxTxDeleteMan::SetContext(_pFltObjects, _pTransaction, _ppContext, &pAssigned);

		if (!NT_SUCCESS( nt_status )) {

			if (STATUS_FLT_CONTEXT_ALREADY_DEFINED == nt_status){
				//
				// TODO: it needs to be handled for multi-threaded environment; i.e. returning existing object;
				//
				nt_status = STATUS_SUCCESS;
				*_ppContext = pAssigned;    pAssigned = NULL;
			}
			else {
				ctx::CTxDeleteCtxAlloca::Free(*_ppContext); *_ppContext = NULL;
			}
		}
	}

	return nt_status;
}


/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CCtxTxModifyMan::Create(
	__inout_opt PFLT_CONTEXT*             _ppContext
)
{
	NTSTATUS nt_status = STATUS_SUCCESS;
	
	PAGED_CODE();
	if (NULL == _ppContext)
		return (nt_status = STATUS_INVALID_PARAMETER);

	nt_status = ctx::CTxModifyCtxAlloca::Allocate(_ppContext);

	return nt_status;
}

NTSTATUS
CCtxTxModifyMan::SetContext(
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects ,
	__in        PFLT_CONTEXT              _pNewContext ,
	__inout_opt PFLT_CONTEXT*             _ppOldContext
)
{
	NTSTATUS nt_status = STATUS_SUCCESS;
	
	PAGED_CODE();
	if (NULL == _pFltObjects ||
	    NULL == _pNewContext ||
	    NULL == _ppOldContext)
		return (nt_status = STATUS_INVALID_PARAMETER);

	 nt_status = FltSetTransactionContext( 
					_pFltObjects->Instance, _pFltObjects->Transaction, FLT_SET_CONTEXT_KEEP_IF_EXISTS, _pNewContext, _ppOldContext
				);
	 return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

VOID FLTAPI
CCtxTxModifyMan::Destroy (
	__in        PFLT_CONTEXT              _pContext
)
{
	if (NULL == _pContext)
		return;

	ctx::CTxModifyCtxAlloca::Free(_pContext); _pContext = NULL;
}

NTSTATUS
CCtxTxModifyMan::GetContext(
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects ,
	__inout_opt PFLT_CONTEXT*             _ppContext
)
{
	NTSTATUS nt_status = STATUS_SUCCESS;

	if (NULL == _pFltObjects || 
		NULL == _pFltObjects->Transaction)
		return (nt_status = STATUS_INVALID_PARAMETER);

	if (NULL == _ppContext)
		return (nt_status = STATUS_INVALID_PARAMETER);

	nt_status = FltGetTransactionContext(
					_pFltObjects->Instance, _pFltObjects->Transaction, _ppContext
					);

	if (STATUS_NOT_FOUND == nt_status) {

		nt_status = CCtxTxModifyMan::Create(_ppContext);
		if (!NT_SUCCESS(nt_status))
			return nt_status;
		else {
			CTxContextPtr    pTxContext;
			nt_status = pTxContext.Attach(*_ppContext);
			
			ObReferenceObject(_pFltObjects->Transaction);
			pTxContext->ModifyData.pTransaction = _pFltObjects->Transaction;

			PFLT_CONTEXT pAssigned = NULL;

			nt_status = CCtxTxModifyMan::SetContext(
								_pFltObjects, pTxContext.Ptr(), &pAssigned
							);

			if (NT_SUCCESS(nt_status))
				*_ppContext = pTxContext.Detach();
			else {
				CCtxTxModifyMan::Destroy(*_ppContext); *_ppContext = NULL;
			}
		}
	}

	return nt_status;
}