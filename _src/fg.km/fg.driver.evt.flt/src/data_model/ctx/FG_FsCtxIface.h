#ifndef _FGFSCONTEXTDATA_H_C6BE2C6E_68D8_410e_9032_7CC1BAAA6A72_INCLUDED
#define _FGFSCONTEXTDATA_H_C6BE2C6E_68D8_410e_9032_7CC1BAAA6A72_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail,com) on 14-Aug-2017 at 8:04:35p, UTC+7, Phuket, Rawai, Monday;
	This is File Guardian mini-filter driver event handler context data interface declaration file.
*/
#include "FG_FsFilterIface.h"
#include "FG_FsFileObject.h"
#include "fg.driver.common.ctx.h"

#define CTX_NOTIFICATION_MASK           (TRANSACTION_NOTIFY_COMMIT_FINALIZE | \
                                         TRANSACTION_NOTIFY_ROLLBACK)
typedef USHORT FLT_CONTEXT_TYPE_EX;

#define FLT_TX_DELETE_CTX_EX             0x0040
#define FLT_TX_MODIFY_CTX_EX             0x0080
#define FLT_FILE_OPER_CTX_EX             0x0100
#define FLT_FILE_MODIFY_CTX_EX           0x0200

#if defined(WIN64)
#define ALIGN_BOUND 64
#else
#define ALIGN_BOUND 32
#endif

//
//  This is the stream context, attached whenever a stream
//  becomes a candidate for deletion.
//
typedef struct _CTX_STREAM_CONTEXT {
	LARGE_INTEGER   CreateTime;
	//
	//  FLT_FILE_NAME_INFORMATION structure with the names for this stream
	//  and file. This is only used for printing out the opened name when
	//  notifying deletes. This will be the result of an opened query name
	//  done at the last pre-cleanup on the file/stream.
	//
	PFLT_FILE_NAME_INFORMATION  pNameInfo;
	//
	//  File ID, obtained from querying the file system for FileInternalInformation.
	//  If the File ID is 128 bits (as in ReFS) we get it via FileIdInformation.
	//
	TFileReference  FileRef;
	volatile LONG   NumOps;              //  Number of SetDisp operations in flight.
	volatile LONG   IsNotified;          //  IsNotified == 1 means a file/stream deletion was already notified.
	BOOLEAN         bFileIdSet;          //  Whether or not we've already queried the file ID.
	BOOLEAN         bDeleteDispose;      //  Delete a disposition for this stream.
	BOOLEAN         bDeleteOnClose;      //  Delete-on-Close state for this stream.
}
TStreamCtx, *PTStreamCtx;

//
//  This is the transaction context, attached at post-
//  -cleanup when notifying a delete within a transaction.
//
typedef struct _CTX_TX_DELETE_CONTEXT
{
	BOOLEAN         bIsInitialized;      // flags this structure is initialized or not, because there are cases when this context is not used;
	BOOLEAN         bEnlisted;           // a flag that tracks if it has been enlisted in transaction;
	LIST_HEAD       NotifyList;          // a list of DF_DELETE_NOTIFY structures representing pending delete notifications.
	//
	//  ERESOURCE for synchronized access to the DeleteNotifyList.
	//
	//  ERESOURCEs must be allocated from NonPagedPool. If an ERESOURCE was
	//  declared here as a direct member of a structure, instead of just a
	//  pointer, then the whole transaction context would need to be allocated
	//  out of NonPagedPool.
	//
	//  Therefore, declaring it as a pointer and only allocating at context
	//  initialization time helps us save some NonPagedPool. This is
	//  particularly important in larger context structures.
	//
	PERESOURCE      pSyncResource;
}
TTxDeleteCtx, *PTTxDeleteCtx;

//
// this is a context that is specific for file modification within a transaction
//
typedef struct _CTX_TX_MODIFY_CONTEXT
{
	BOOLEAN         bIsInitialized;      // a flag indicates if this structure is initialized or not;
	PKTRANSACTION   pTransaction;        // owning transaction object pointer;
	BOOLEAN         bEnlisted;           // a flag that tracks if this context has been enlisted in transaction;
	BOOLEAN         bListDrained;        // a flag that indicates if the file context list is drained (in other words, it is processed and cleared);
	LIST_HEAD       FileCtxList;         // this list contains all file contexts, which is likely to be modified within a transaction (multiple writers);
	PFAST_MUTEX     pSyncMutex;          // protects list from simultaneous access from different threads;
}
TTxModifyCtx, *PTTxModifyCtx;

//
// this context for all type of actions that are monitored in transactions
//
// https://msdn.microsoft.com/en-us/library/windows/desktop/ms683609(v=vs.85).aspx
// take a look at remark section;
// this structure participates in pointer exchange;
//
// typedef __declspec(align(ALIGN_BOUND)) struct _CTX_TRANSACTION_CONTEXT
typedef struct _CTX_TRANSACTION_CONTEXT
{
	TTxDeleteCtx    DeleteData;          // specific data for deleting context
	TTxModifyCtx    ModifyData;          // specific data for modifying context
}
TTransactCtx, *PTTransactCtx;

//
// this is generic context for all file opeations
//
typedef struct _CTX_FILE_OPERATE_CONTEXT
{
	BOOL            bIsInitialized;      // a flag indicates if this structure is initialized or not;
	PFLT_FILE_NAME_INFORMATION
	                pSourceInfo;         // source file object name information pointer;
	PFLT_FILE_NAME_INFORMATION
	                pTargetInfo;         // target file object name information pointer;
	BOOL            bIsTargetReplaced;   // this flag indicates that the target file is replaced durring curren operation, for example, file copy/move/rename;
}
TFileOperateCtx, *PTFileOperateCtx;

//
// this is context for file modification monitor
//

typedef struct _CTX_FILE_MODIFY_CONTEXT
{
	BOOLEAN         bIsInitialized;      // a flag indicates if this structure is initialized or not;
	TFileReference_Ex
	                FileRefEx;           // an informational data of the file being modified;
	BOOLEAN         bDirty;              // the flag is used to indicate if the file is dirty;
	BOOLEAN         bTxDirty;            // file is preliminary marked as dirty within particular transaction (before transaction is committed/rolled back);
	PTTransactCtx   pTxCtx;              // pointer to an owning transaction object context;
	LIST_HEAD       pThis;               // this file context list entry that is included to parent transaction file context list;
}
TFileModifyCtx, *PTFileModifyCtx;

//
// File object compound context;
//
// https://msdn.microsoft.com/en-us/library/windows/desktop/ms683609(v=vs.85).aspx
// take a look at remark section;
// this structure participates in pointer exchange;
//
// TODO: Actually, compound structures can contain a generic VOID pointer and size of the allocated data;
//       Void pointer makes context structure indifferent to the types, but needs more attention to memory management;
//
// Other approach:
// https://stackoverflow.com/questions/38788137/structure-padding-on-64bit-machine
//
// typedef __declspec(align(ALIGN_BOUND)) struct _CTX_FILE_OBJECT_CONTEXT
typedef struct _CTX_FILE_OBJECT_CONTEXT
{
	TFileOperateCtx Operate;
	TFileModifyCtx  ModifyData;
}
TFileObjectCtx, *PTFileObjectCtx;

/////////////////////////////////////////////////////////////////////////////

namespace ctx {

	CONST PFLT_CONTEXT_REGISTRATION
	GetRegistrationInfo(
		VOID
		);

	class CContextTime {

	public:
		static
		LARGE_INTEGER GetCurrentTimestamp(VOID);
	};

	using shared::km::fl_sys::TInstanceCtx;

	template<typename T>
	class CCtxPtr {
	private:
		T*              m_pCtx;
	public:
		CCtxPtr(void) : m_pCtx(NULL){}
		CCtxPtr(T* _pCtx) : m_pCtx(_pCtx){}
		~CCtxPtr(void) {
			this->Free();
		}
	public:
		NTSTATUS        Attach(T* _pCtx){
			if (NULL == _pCtx)
				return STATUS_INVALID_PARAMETER;

			this->Free();
			m_pCtx = _pCtx;
			return STATUS_SUCCESS;
		}

		NTSTATUS        Attach(PFLT_CONTEXT _pCtx){
			return this->Attach((T*)_pCtx);
		}

		T*              Detach(void){
			T* pCtx = m_pCtx;
			m_pCtx = NULL;
			return pCtx;
		}

		NTSTATUS        Free(void) {
			if (NULL != m_pCtx){
				FltReleaseContext(m_pCtx); m_pCtx = NULL;
			}
			return STATUS_SUCCESS;
		}

		bool            IsValid(void)const {
			return (NULL != m_pCtx);
		}

		T*              Ptr(void) { return  m_pCtx; }
		T*&             Ref(void) { return  m_pCtx; }
		VOID            Zero(void){ if (NULL != m_pCtx) RtlZeroMemory( m_pCtx, sizeof(T) ); }
	public:
		operator T* (void)const   { return  m_pCtx; }
		operator T**(void)        { return &m_pCtx; }
		operator T*&(void)        { return  m_pCtx; }
	public:
		operator PFLT_CONTEXT*
		            (void)        { return (PFLT_CONTEXT*)&m_pCtx; }
	public:
		CCtxPtr& operator=(T* _pCtx) {
			this->Attach(_pCtx);
			return *this;
		}

		bool operator==(const CCtxPtr<T>& _ptr) { return (m_pCtx == _ptr.m_pCtx); }
		T*   operator->(void) const             { return  m_pCtx; }
	};

	typedef CCtxPtr<PFLT_CONTEXT>    CFltContextPtr ;
	typedef CCtxPtr<TTransactCtx>    CTxContextPtr  ;
	typedef CCtxPtr<TInstanceCtx>    CInstanceCtxPtr;
	typedef CCtxPtr<TStreamCtx>      CStreamCtxPtr  ;
	typedef CCtxPtr<TFileObjectCtx>  CFileObjCtxPtr ;
}

#endif/*_FGFSCONTEXTDATA_H_C6BE2C6E_68D8_410e_9032_7CC1BAAA6A72_INCLUDED*/