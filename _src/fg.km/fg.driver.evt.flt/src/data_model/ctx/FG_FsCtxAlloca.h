#ifndef _FGFSCONTEXTALLOCA_H_9E0663FC_2511_4fa6_B39A_14A1376E46E7_INCLUDED
#define _FGFSCONTEXTALLOCA_H_9E0663FC_2511_4fa6_B39A_14A1376E46E7_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Oct-2017 at 2:35:10p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian mini-filter driver event handler context allocator interface declaration file.
*/

#define CTX_ERESOURCE_POOL_TAG           'eres'
#define CTX_FASTMUTEX_POOL_TAG           'fmtx'

namespace ctx {
	class CCtxBaseAlloca {
	public:
		static
		NTSTATUS
		Allocate (
			__in        FLT_CONTEXT_TYPE           _nCtxType  ,
			__out_opt   PFLT_CONTEXT*             _ppContext
			);
	};
	//
	// this class is for creating/destroying file generic/operate operation context;
	//
	class CFileOperCtxAlloca {
	public:
		//
		// allocates all resources that are required for file generic context;
		//
		static
		NTSTATUS
		Allocate (
			__out_opt   PFLT_CONTEXT*             _ppContext
			);
		//
		// frees all allocated resources;
		//
		static
		VOID FLTAPI
		Free (
			__in        PFLT_CONTEXT              _pContext
			);
	};
	//
	// this class is for creating/destroying file modification context;
	//
	class CFileModifyCtxAlloca {
	public:
		//
		// allocates all resources that are required for file modification context;
		//
		static
		NTSTATUS
		Allocate (
			__in        PFLT_CALLBACK_DATA        _pClbTarget,
			__out_opt   PFLT_CONTEXT*             _ppContext
			);
		static
		VOID FLTAPI
		Free (
			__in        PFLT_CONTEXT              _pContext
			);
	};
	//
	// this class is for creating/destroying stream context;
	//
	class CStreamCtxAlloca {
	public:
		/*
		Routine Description:
			This routine allocates and initializes stream context.
		Arguments:
			_pContext     - Pointer to a context pointer.
		Return Value:
			A status forwarded from FltAllocateContext.
		*/
		static
		NTSTATUS
		Allocate (
			__out_opt   PFLT_CONTEXT*             _ppContext
			);
		/*
		Routine Description:
			This routine cleans up stream context provided in accordance with its type.
		Arguments:
			_pContext     - Pointer to stream context object being cleaned up.
		*/
		static
		VOID FLTAPI
		Free (
			__in        PFLT_CONTEXT              _pContext
			);
	};

	//
	// this class is for creating/destroying file deletion transaction context;
	//
	class CTxDeleteCtxAlloca {
	public:
		//
		//	This routine allocates and initializes a transaction context for file delete operation.
		//
		static
		NTSTATUS
		Allocate (
			__out_opt   PFLT_CONTEXT*             _ppContext
			);
		//
		//	This routine frees all allocated resources.
		//
		static
		VOID FLTAPI
		Free (
			__in        PFLT_CONTEXT              _pContext
			);
	};

	//
	// this class is for creating/destroying file modification transaction context;
	//
	class CTxModifyCtxAlloca {
	public:
		//
		//	This routine allocates and initializes a transaction context.
		//
		static
		NTSTATUS
		Allocate (
			__out_opt   PFLT_CONTEXT*             _ppContext
			);
		//
		//	This routine frees all allocated resources.
		//
		static
		VOID FLTAPI
		Free (
			__in        PFLT_CONTEXT              _pContext
			);
	};
}

#endif/*_FGFSCONTEXTALLOCA_H_9E0663FC_2511_4fa6_B39A_14A1376E46E7_INCLUDED*/