/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Aug-2017 at 8:08:14p, UTC+7, Phuket, Rawai, Monday;
	This is File Guardian mini-filter driver event handler context data interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsCtxIface.h"
#include "FG_FsCtxMans.h"

#define CTX_INSTANCE_OBJ_POOL_TAG        'ictx'
#define CTX_TRANSACT_OBJ_POOL_TAG        'tctx'
#define CTX_FLSTREAM_OBJ_POOL_TAG        'sctx'
#define CTX_FILEOPER_OBJ_POOL_TAG        'fctx'

#define CTX_PRESERVE_OBJ                 FLT_SET_CONTEXT_KEEP_IF_EXISTS

#include "fg.driver.common.ctx.h"

/////////////////////////////////////////////////////////////////////////////

namespace ctx {

	typedef shared::km::fl_sys::CCtxInstanceMan CCtxInstanceMan;

	VOID FLTAPI private_clean_fun (
	__in PFLT_CONTEXT     pContext,
	__in FLT_CONTEXT_TYPE uContextType
	)
	{
		switch(uContextType){
			case FLT_INSTANCE_CONTEXT    : ctx::CCtxInstanceMan::Destroy(pContext); break;
			case FLT_STREAM_CONTEXT      : ctx::CCtxStreamMan::Destroy(pContext); break;
			case FLT_TRANSACTION_CONTEXT : {
			                               ctx::CCtxTxDeleteMan::Destroy(pContext);
			                               ctx::CCtxTxModifyMan::Destroy(pContext);
			                               } break;
			case FLT_FILE_CONTEXT        : {
			                               ctx::CCtxFileModifyMan::Destroy(pContext);
			                               ctx::CCtxFileOperateMan::Destroy(pContext);
										   } break;
			default:
				NOTHING;
		}
	}

	CONST PFLT_CONTEXT_REGISTRATION
	GetRegistrationInfo(
		VOID
	)
	{
		static FLT_CONTEXT_REGISTRATION reg_info[] = {

			{ FLT_INSTANCE_CONTEXT   , 0, private_clean_fun, sizeof(TInstanceCtx)   , CTX_INSTANCE_OBJ_POOL_TAG, NULL, NULL, NULL },
			{ FLT_STREAM_CONTEXT     , 0, private_clean_fun, sizeof(TStreamCtx)     , CTX_FLSTREAM_OBJ_POOL_TAG, NULL, NULL, NULL },
			{ FLT_TRANSACTION_CONTEXT, 0, private_clean_fun, sizeof(TTransactCtx)   , CTX_TRANSACT_OBJ_POOL_TAG, NULL, NULL, NULL },
			{ FLT_FILE_CONTEXT       , 0, private_clean_fun, sizeof(TFileObjectCtx) , CTX_FILEOPER_OBJ_POOL_TAG, NULL, NULL, NULL },
			{ FLT_CONTEXT_END }
		};

		return &reg_info[0];
	}

}

/////////////////////////////////////////////////////////////////////////////


LARGE_INTEGER ctx::CContextTime::GetCurrentTimestamp(VOID)
{
	LARGE_INTEGER system_time_ = {0};

	KeQuerySystemTime(&system_time_);

	return system_time_;
}