/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Oct-2017 at 2:42:05p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian mini-filter driver event handler context allocator interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsCtxAlloca.h"
#include "FG_FsNotifyData.h"
#include "FG_FsCtxIface.h"

using namespace ctx;

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CCtxBaseAlloca::Allocate (
	__in        FLT_CONTEXT_TYPE           _nCtxType  ,
	__out_opt   PFLT_CONTEXT*             _ppContext
	)
{
	UNUSED(_nCtxType);
	UNUSED(_ppContext);

	return STATUS_SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CFileOperCtxAlloca::Allocate (
	__out_opt   PFLT_CONTEXT*             _ppContext
	)
{
	static
	TDriverData&     drv_data    = global::DriverData();
	NTSTATUS         nt_status   = STATUS_SUCCESS;
	PTFileObjectCtx  pFileCtx    = NULL;

	nt_status = FltAllocateContext(
					drv_data.pFilter, FLT_FILE_CONTEXT, sizeof(TFileObjectCtx), PagedPool, _ppContext
				);
	if (NT_SUCCESS( nt_status )) {
		RtlZeroMemory( *_ppContext, sizeof(TFileObjectCtx) );
	}

	pFileCtx = (PTFileObjectCtx) *_ppContext;

	pFileCtx->Operate.bIsTargetReplaced = TRUE;

	return nt_status;
}

VOID FLTAPI
CFileOperCtxAlloca::Free (
	__in        PFLT_CONTEXT              _pContext
	)
{
	PTFileObjectCtx pFileObjectCtx   = (PTFileObjectCtx)_pContext;
	if (NULL == pFileObjectCtx)
		return;
	if (!pFileObjectCtx->Operate.bIsInitialized)
		return;

	pFileObjectCtx->Operate.bIsInitialized    = FALSE;
	pFileObjectCtx->Operate.bIsTargetReplaced = FALSE;

	if (NULL != pFileObjectCtx->Operate.pSourceInfo){

		FltReleaseFileNameInformation(pFileObjectCtx->Operate.pSourceInfo);
		pFileObjectCtx->Operate.pSourceInfo = NULL;
	}

	if (NULL != pFileObjectCtx->Operate.pTargetInfo){

		FltReleaseFileNameInformation(pFileObjectCtx->Operate.pTargetInfo);
		pFileObjectCtx->Operate.pTargetInfo = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CFileModifyCtxAlloca::Allocate (
	__in        PFLT_CALLBACK_DATA        _pClbTarget,
	__out_opt   PFLT_CONTEXT*             _ppContext
	)
{
	static
	TDriverData&     drv_data    = global::DriverData();
	NTSTATUS         nt_status   = STATUS_SUCCESS;
	CFileObjCtxPtr   pFileCtx;

	PAGED_CODE();

	nt_status = FltAllocateContext(
					drv_data.pFilter, FLT_FILE_CONTEXT, sizeof(TFileObjectCtx), PagedPool, _ppContext
				);
	if (!NT_SUCCESS( nt_status ))
		return nt_status;

	nt_status = pFileCtx.Attach(*_ppContext);
	if (!NT_SUCCESS( nt_status ))
		return nt_status;
	
	*_ppContext = NULL; // the pointer above controls memory block

	pFileCtx.Zero();
	pFileCtx->ModifyData.bIsInitialized = TRUE;

	nt_status = fl_sys::CFile::GetId(
						_pClbTarget->Iopb->TargetInstance,
						_pClbTarget->Iopb->TargetFileObject, pFileCtx->ModifyData.FileRefEx.FileId
					);
	if (!NT_SUCCESS( nt_status ))
		return nt_status;

	utils::CStringW cs_name;

	nt_status = fl_sys::CFile::FileName(
						_pClbTarget, cs_name
					);
	if (!NT_SUCCESS( nt_status ))
		return nt_status;

	cs_name.CopyTo( pFileCtx->ModifyData.FileRefEx.FileName );
	pFileCtx->ModifyData.FileRefEx.IsFolder = fl_sys::CFile::IsDirectory(
							_pClbTarget->Iopb->TargetInstance, _pClbTarget->Iopb->TargetFileObject
						);

	*_ppContext = pFileCtx.Detach(); // frees memory control by the pointer
	return nt_status;
}

VOID FLTAPI
CFileModifyCtxAlloca::Free (
	__in        PFLT_CONTEXT              _pContext
	)
{
	PTFileObjectCtx pFileCtx = NULL;
	if (NULL == _pContext)
		return;

	pFileCtx = (PTFileObjectCtx) _pContext;

#if DBG
	if (NULL != pFileCtx->ModifyData.pTxCtx){
		DbgPrint("__fg_fs: [WARN] CFileModifyCxt::Free(): File context must be out of transaction one;\n");
	}
#endif
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CStreamCtxAlloca::Allocate (
	__out_opt   PFLT_CONTEXT*             _ppContext
	)
{
	static
	TDriverData&     drv_data    = global::DriverData();
	NTSTATUS         nt_status   = STATUS_SUCCESS;

	PAGED_CODE();

	if (NULL != *_ppContext){
		//
		// completely foolish state, but nevertheless, we need a protection from unexpectedly incoming context; 
		//
		FltReleaseContext(*_ppContext); *_ppContext = NULL;
	}

	nt_status = FltAllocateContext(
					drv_data.pFilter, FLT_STREAM_CONTEXT, sizeof(TStreamCtx), PagedPool, _ppContext
				);
	if (NT_SUCCESS( nt_status )) {

		PTStreamCtx pStreamCtx = (PTStreamCtx)(*_ppContext);

		RtlZeroMemory( *_ppContext, sizeof(TStreamCtx) );
		pStreamCtx->CreateTime = ctx::CContextTime::GetCurrentTimestamp();
	}
	else {
#if DBG
		DbgPrint(
			"__fg_fs: [WARN] CStreamCtxAlloca::Allocate(): failed with err=%X;\n", nt_status
			);
#endif
	}
	return nt_status;
}

VOID FLTAPI
CStreamCtxAlloca::Free (
	__in        PFLT_CONTEXT              _pContext
	)
{
	PTStreamCtx pStreamCtx   = (PTStreamCtx)_pContext;
	if (NULL == pStreamCtx)
		return;

	if (NULL != pStreamCtx->pNameInfo) {
		FltReleaseFileNameInformation(pStreamCtx->pNameInfo);
		pStreamCtx->pNameInfo = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CTxDeleteCtxAlloca::Allocate (
	__out_opt   PFLT_CONTEXT*             _ppContext
	)
{
	static
	TDriverData&     drv_data    = global::DriverData();
	NTSTATUS         nt_status   = STATUS_SUCCESS;
	CTxContextPtr    pTxContext;

	nt_status = FltAllocateContext(
					drv_data.pFilter, FLT_TRANSACTION_CONTEXT, sizeof(TTransactCtx), PagedPool, _ppContext
				);
	if (!NT_SUCCESS( nt_status ))
		return nt_status;

	nt_status = pTxContext.Attach(*_ppContext);
	if (!NT_SUCCESS( nt_status ))
		return nt_status;

	*_ppContext = NULL; // the pointer above takes control over allocated memory;

	pTxContext.Zero();
	pTxContext->DeleteData.bIsInitialized = TRUE;
	pTxContext->DeleteData.pSyncResource  = (PERESOURCE)ExAllocatePoolWithTag(
									NonPagedPool, sizeof(ERESOURCE), CTX_ERESOURCE_POOL_TAG
								);
	if (NULL == pTxContext->DeleteData.pSyncResource){
		return (nt_status = STATUS_INSUFFICIENT_RESOURCES);
	}
	ExInitializeResourceLite( pTxContext->DeleteData.pSyncResource );
	InitializeListHead( &pTxContext->DeleteData.NotifyList );

	*_ppContext = pTxContext.Detach(); // returns memory control

	return nt_status;
}

VOID FLTAPI
CTxDeleteCtxAlloca::Free (
	__in        PFLT_CONTEXT              _pContext
	)
{
	PTDeleteNotifyData pNotify   = NULL;
	PTTransactCtx    pTnxContext = (PTTransactCtx)_pContext;
	if (NULL == pTnxContext)
		return;

	if (NULL != pTnxContext->DeleteData.pSyncResource){

		FltAcquireResourceExclusive( pTnxContext->DeleteData.pSyncResource );

		while (!IsListEmpty( &pTnxContext->DeleteData.NotifyList )) {

			pNotify = CONTAINING_RECORD(
							RemoveHeadList( &pTnxContext->DeleteData.NotifyList ), TDeleteNotifyData, links
						);
			FltReleaseContext( pNotify->pContext );
			ExFreePool( pNotify );
		}
		FltReleaseResource( pTnxContext->DeleteData.pSyncResource );
		//
		//  Delete and free the DeleteNotifyList synchronization resource.
		//
		ExDeleteResourceLite( pTnxContext->DeleteData.pSyncResource );
		ExFreePool( pTnxContext->DeleteData.pSyncResource );
	}
	return NOTHING;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CTxModifyCtxAlloca::Allocate (
	__out_opt   PFLT_CONTEXT*             _ppContext
	)
{
	static
	TDriverData&     drv_data    = global::DriverData();
	NTSTATUS         nt_status   = STATUS_SUCCESS;
	CTxContextPtr    pTxCtxPtr;

	nt_status = FltAllocateContext(
					drv_data.pFilter, FLT_TRANSACTION_CONTEXT, sizeof(TTransactCtx), PagedPool, _ppContext
				);
	if (!NT_SUCCESS( nt_status ))
		return nt_status;

	nt_status = pTxCtxPtr.Attach(*_ppContext);
	if (!NT_SUCCESS( nt_status ))
		return nt_status;

	*_ppContext = NULL; // the pointer takes control over memory block;

	pTxCtxPtr.Zero();
	pTxCtxPtr->ModifyData.bIsInitialized = TRUE;
	pTxCtxPtr->ModifyData.pSyncMutex = (PFAST_MUTEX)ExAllocatePoolWithTag(
									NonPagedPool, sizeof(PFAST_MUTEX), CTX_FASTMUTEX_POOL_TAG
								);
	if (NULL == pTxCtxPtr->ModifyData.pSyncMutex){
		return (nt_status = STATUS_INSUFFICIENT_RESOURCES);
	}

	InitializeListHead(&pTxCtxPtr->ModifyData.FileCtxList);
	ExInitializeFastMutex(pTxCtxPtr->ModifyData.pSyncMutex);

	*_ppContext = pTxCtxPtr.Detach(); // removes the pointer control;

	return nt_status;
}

VOID FLTAPI
CTxModifyCtxAlloca::Free (
	__in        PFLT_CONTEXT              _pContext
	)
{
	PTTransactCtx    pTxContext  = (PTTransactCtx)_pContext;
	if (NULL == pTxContext)
		return;

	if (!pTxContext->ModifyData.bIsInitialized)
		return;

	ExFreePoolWithTag( pTxContext->ModifyData.pSyncMutex, CTX_FASTMUTEX_POOL_TAG );
	pTxContext->ModifyData.pSyncMutex = NULL;

	if (NULL != pTxContext->ModifyData.pTransaction) {

		ObDereferenceObject(pTxContext->ModifyData.pTransaction);
		pTxContext->ModifyData.pTransaction = NULL;
	}

	return NOTHING;
}