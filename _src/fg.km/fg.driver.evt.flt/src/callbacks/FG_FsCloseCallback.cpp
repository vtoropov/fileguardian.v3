/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Nov-2017 at 10:24:26a, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian mini-filter driver file system close callback interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsCloseCallback.h"
#include "FG_FsModifyEvtHandler.h"
#include "FG_FsFileObject.h"
/////////////////////////////////////////////////////////////////////////////

FLT_PREOP_CALLBACK_STATUS
PreCloseCallback(
	__inout     PFLT_CALLBACK_DATA        _pFltData      ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects   ,
	__out       PVOID*                    _ppCompletionCtx
	)
{
	FLT_PREOP_CALLBACK_STATUS
	      fl_status = FLT_PREOP_SUCCESS_NO_CALLBACK;
	BOOL   bHandled = FALSE;

	UNUSED(_ppCompletionCtx);
	UNUSED(_pFltObjects);

	PAGED_CODE();
	//
	// TODO: there is a requirement of checking target file name before doing anything;
	//
	utils::CStringW fileName;
	fl_sys::CFile::FileName(_pFltData, fileName);

	if (!fileName.IsValid())
		return fl_status;

#if defined(_USE_COMPLEX_MODIFY)
	fl_status = handlers::CModifyEvtHandler::CClose::OnBefore(
						_pFltData, _pFltObjects, _ppCompletionCtx, bHandled
					);
#endif

	if (bHandled)
		return fl_status;
	else {
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] PreCloseCallback::callback() has been passed for %wZ;\n", (PUNICODE_STRING)fileName
			);
#endif
	}

	return fl_status;
}