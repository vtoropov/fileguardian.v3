#ifndef _FGFSTRANSHANDLER_H_3B1C8722_FC82_47d5_8389_8E535798D4D2_INCLUDED
#define _FGFSTRANSHANDLER_H_3B1C8722_FC82_47d5_8389_8E535798D4D2_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jul-2017 at 9:29:11p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian minifilter driver transaction callback interface declaration file.
*/
#include "FG_FsFilterIface.h"
//
// filter manager transaction support related functionality
//

typedef NTSTATUS
(*PFLT_SET_TRANSACTION_CONTEXT)(
	__in PFLT_INSTANCE  _pInstance   ,
	__in PKTRANSACTION  _pTransaction,
	__in FLT_SET_CONTEXT_OPERATION eOperation,
	__in PFLT_CONTEXT   _pNewContext ,
	__deref_opt_out PFLT_CONTEXT*  pPtrToPrevCtx
	);

typedef NTSTATUS
(*PFLT_GET_TRANSACTION_CONTEXT)(
	__in PFLT_INSTANCE  _pInstance   ,
	__in PKTRANSACTION  _pTransaction,
	__deref_out PFLT_CONTEXT* pPtrToActiveCtx
	);

typedef NTSTATUS
(*PFLT_ENLIST_IN_TRANSACTION)(
	__in PFLT_INSTANCE  _pInstance   ,
	__in PKTRANSACTION  _pTransaction,
	__in PFLT_CONTEXT   _pTransactionCtx,
	__in NOTIFICATION_MASK  uNotifyCode
	);

typedef struct _TRANSACTION_VTABLE
{
	//
	//  transaction support; started from Vista;
	//
	PFLT_SET_TRANSACTION_CONTEXT   pFltSetTransactionCtx;
	PFLT_GET_TRANSACTION_CONTEXT   pFltGetTransactionCtx;
	PFLT_ENLIST_IN_TRANSACTION     pFltEnlistInTransaction;
}
TTransactionVTable, *PTTransactionVTable;

#define FLAG_ENLISTED_IN_TRANSACTION 0x01

namespace global
{
	TTransactionVTable&  TransaxVTable(void);
}

/////////////////////////////////////////////////////////////////////////////

/*
Routine Description:
    This routine is the transaction notification callback for this minifilter.
    It is called when a transaction we're enlisted in is committed or rolled
    back so that it's possible to emit notifications about files that were
    deleted in that transaction.

Arguments:
    FltObjects  - Pointer to the FLT_RELATED_OBJECTS data structure containing
                  opaque handles to this filter, instance, its associated volume and
                  file object.

    TransactCtx - The transaction context, set/modified when a delete
                  is detected.

    NotifyMask  - A mask of flags indicating the notifications received
                  from FltMgr. Should be either TRANSACTION_NOTIFY_COMMIT or
                  TRANSACTION_NOTIFY_ROLLBACK.
Return Value:
    STATUS_SUCCESS - This operation is never pended.
*/
NTSTATUS
TxNotificationCallback (
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects,
	__in        PFLT_CONTEXT              _pTransCtx  ,
	__in        ULONG                     _uNotifyMask
	);

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
KtmInitializeTransactionVTable(
	__in    VOID
	);

#endif/*_FGFSTRANSHANDLER_H_3B1C8722_FC82_47d5_8389_8E535798D4D2_INCLUDED*/