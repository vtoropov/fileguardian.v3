#ifndef _FGFSSETINFOHANDLER_H_E71C53C8_D580_4370_B0D3_6CD840FAD521_INCLUDED
#define _FGFSSETINFOHANDLER_H_E71C53C8_D580_4370_B0D3_6CD840FAD521_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Aug-2017 at 11:17:45a, UTC+7, Phuket, Rawai, Monday;
	This is File Guardian mini-filter driver setting information callback interface file declaration. 
*/
#include "FG_FsFilterIface.h"

FLT_POSTOP_CALLBACK_STATUS FLTAPI
PostSetInfoCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData         ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects      ,
	__in        PVOID                     _pCompletionCtx   ,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags
	);

FLT_PREOP_CALLBACK_STATUS FLTAPI
PreSetInfoCallback(
	__inout     PFLT_CALLBACK_DATA        _pFltData         ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects      ,
	__inout_opt PVOID*                    _ppCompletionCtx
);

#endif/*_FGFSSETINFOHANDLER_H_E71C53C8_D580_4370_B0D3_6CD840FAD521_INCLUDED*/