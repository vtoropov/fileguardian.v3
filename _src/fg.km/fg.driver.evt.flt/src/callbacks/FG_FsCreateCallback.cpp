/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Aug-2017 at 4:10:34p, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian mini-filter driver create event callback interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsCreateCallback.h"
#include "FG_FsDelNoTxEvtHandler.h"
#include "Fg_FsCreateEvtHandler.h"
#include "FG_FsModifyEvtHandler.h"

/////////////////////////////////////////////////////////////////////////////

FLT_POSTOP_CALLBACK_STATUS FLTAPI
PostCreateCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData      ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects   ,
	__in        PVOID                     _pCompletionCtx,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags
	)
{
	FLT_POSTOP_CALLBACK_STATUS
	            fl_status   = FLT_POSTOP_FINISHED_PROCESSING;
	BOOL        bHandled    = FALSE;

	PAGE_CODE_EX(fl_status);

	//
	// checks for draining scenario;
	//
	if (FlagOn( _uFlags, FLTFL_POST_OPERATION_DRAINING ))
		return fl_status;

	fl_status = handlers::CCreateEvtHandler::OnAfter(_pFltData, _pFltObjects, _pCompletionCtx, _uFlags, bHandled);
	if (bHandled)
		return fl_status;

	//
	// If Before::Event is handled by CDeleteNonTxEvtHandler::CFileInfoEvent::OnBefore();
	// the specified above procedure will be followed by OnAfter() one of the same event handler;
	//
#if defined(_USE_COMPLEX_MODIFY)
	if (handlers::CModifyEvtHandler::CWriteEvent::IsWritable(_pFltData)){
		fl_status = handlers::CModifyEvtHandler::CCreateEvent::OnAfter(
						_pFltData, _pFltObjects, _pCompletionCtx, _uFlags, bHandled
					);
		return fl_status;
	}
#endif
	return fl_status;
}

FLT_PREOP_CALLBACK_STATUS
PreCreateCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData      ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects   ,
	__out       PVOID*                    _ppCompletionCtx
	)
{
	FLT_PREOP_CALLBACK_STATUS
	            fl_status   = FLT_PREOP_SUCCESS_NO_CALLBACK;
	BOOL        bHandled    = FALSE;

	PAGE_CODE_EX(fl_status);

	//
	// the 1st level of filtering events (by system thread)
	//
	if (PsIsSystemThread(PsGetCurrentThread()))
		return fl_status;
	if (FLT_IS_FS_FILTER_OPERATION(_pFltData))
		return fl_status;

	//
	// the 2nd level of filtering events (by class of the event)
	//
	fl_status =  handlers::CCreateEvtHandler::OnBefore(_pFltData, _pFltObjects, _ppCompletionCtx, bHandled);
	if (bHandled){
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] PreCreateCallback() is handled by handlers::CCreateEvtHandler::OnPreCreate();\n"
			);
#endif
		return fl_status;
	}

	if (FlagOn( _pFltData->Iopb->Parameters.Create.Options, FILE_DELETE_ON_CLOSE )){
		fl_status =  handlers::CDeleteNonTxEvtHandler::CFileInfoEvent::OnBefore(_pFltData, _pFltObjects, _ppCompletionCtx, bHandled);
		if (bHandled){
#if DBG
			DbgPrint(
				"__fg_fs: [INFO] PreCreateCallback() is handled by handlers::CDeleteNonTxEvtHandler::CFileInfoEvent()::OnBefore;\n"
				);
#endif
			return fl_status;
		}
	}
#if defined(_USE_COMPLEX_MODIFY)
	fl_status = handlers::CModifyEvtHandler::CCreateEvent::OnBefore(_pFltData, bHandled);
#endif
	return fl_status;
}