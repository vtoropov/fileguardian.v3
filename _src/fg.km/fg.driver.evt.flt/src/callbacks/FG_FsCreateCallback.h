#ifndef _FGFSCREATEHANDLER_H_4D390DF7_EA0A_45b9_9BF9_5D64164AB821_INCLUDED
#define _FGFSCREATEHANDLER_H_4D390DF7_EA0A_45b9_9BF9_5D64164AB821_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Aug-2017 at 3:33:01p, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian mini-filter driver create event callback interface declaration file.
*/
#include "FG_FsFilterIface.h"

FLT_POSTOP_CALLBACK_STATUS FLTAPI
PostCreateCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData      ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects   ,
	__in        PVOID                     _pCompletionCtx,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags
	);

FLT_PREOP_CALLBACK_STATUS
PreCreateCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData      ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects   ,
	__out       PVOID*                    _ppCompletionCtx
	);

#endif/*_FGFSCREATEHANDLER_H_4D390DF7_EA0A_45b9_9BF9_5D64164AB821_INCLUDED*/