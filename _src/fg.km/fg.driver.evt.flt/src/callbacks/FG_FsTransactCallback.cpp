/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jul-2017 at 9:33:46p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian minifilter driver transaction callback interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsTransactCallback.h"
#include "FG_FsCtxMans.h"
#include "FG_FsDeleteEvtHandler.h"
#include "FG_FsModifyEvtHandler.h"

#define CTX_ALREADY_EXISTS (STATUS_FLT_CONTEXT_ALREADY_DEFINED)
/////////////////////////////////////////////////////////////////////////////

namespace global
{
	TTransactionVTable&  TransaxVTable(void)
	{
		static TTransactionVTable vTable = {0};
		return vTable;
	}
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
TxNotificationCallback (
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects,
	__in        PFLT_CONTEXT              _pTransCtx  ,
	__in        ULONG                     _uNotifyMask
	)
{
	NTSTATUS      nt_status    = STATUS_SUCCESS;
	BOOLEAN       bCommitted   = BooleanFlagOn( _uNotifyMask, TRANSACTION_NOTIFY_COMMIT_FINALIZE );
	BOOL          bHandled     = FALSE;

	UNUSED(_pTransCtx);
	UNUSED(_pFltObjects);
	UNUSED(bHandled);
	UNUSED(bCommitted);
	//
	//  TODO: we need to use only one boolean macro; especially which is developed for drivers (BOOLEAN)
	//
	PAGED_CODE();
	//
	//  There is no such thing as a simultaneous commit and rollback, nor
	//  should we get notifications for events other than a commit or a
	//  rollback.
	//
	if (FlagOnAll( _uNotifyMask, (CTX_NOTIFICATION_MASK))){
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] TxNotificationCallback() has received all flags on;\n"
			);
#endif
		return nt_status;
	}

	if (!FlagOn  ( _uNotifyMask, (CTX_NOTIFICATION_MASK))){
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] TxNotificationCallback() has received all flags off;\n"
			);
#endif
		return nt_status;
	}
#if (0)	
	nt_status = handlers::CDeleteEvtHandler::OnTransactionEnd(
					_pFltObjects, _pTransCtx, bCommitted, bHandled
				);
	if (bHandled){
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] TxNotificationCallback() is handled by handlers::CDeleteEvtHandler::OnTransactionEnd();\n"
			);
#endif
		return nt_status;
	}
#endif

#if defined(_USE_COMPLEX_MODIFY)
	nt_status = handlers::CModifyEvtHandler::OnTransactionEnd(
					_pFltObjects, _pTransCtx, _uNotifyMask, bHandled
				);

	if (bHandled){
#if DBG
		DbgPrint(
			"__fg_fs: [info] TxNotificationCallback() is handled by handlers::CModifyEvtHandler::OnTransactionEnd();\n"
			);
#endif
		return nt_status;
	}
#endif

	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
KtmInitializeTransactionVTable(
	__in    VOID
	)
{
	TTransactionVTable& v_table = global::TransaxVTable();
#pragma warning(push)
#pragma warning(disable:4055) // type cast from data pointer to function pointer
	v_table.pFltSetTransactionCtx   = (PFLT_SET_TRANSACTION_CONTEXT) FltGetRoutineAddress( "FltSetTransactionContext" );
	v_table.pFltGetTransactionCtx   = (PFLT_GET_TRANSACTION_CONTEXT) FltGetRoutineAddress( "FltGetTransactionContext" );
	v_table.pFltEnlistInTransaction = (PFLT_ENLIST_IN_TRANSACTION)   FltGetRoutineAddress( "FltEnlistInTransaction" );
#pragma warning(pop)
	return STATUS_SUCCESS;
}