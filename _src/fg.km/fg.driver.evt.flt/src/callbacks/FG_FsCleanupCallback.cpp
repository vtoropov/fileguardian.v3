/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Aug-2017 at 2:20:15a, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian mini-filter driver file system cleaning up callback interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsCleanupCallback.h"
#include "FG_FsDelNoTxEvtHandler.h"
#include "FG_FsChangeEvtHandler.h"

/////////////////////////////////////////////////////////////////////////////

FLT_POSTOP_CALLBACK_STATUS FLTAPI
PostCleanupCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__in        PVOID                     _pCompletionCtx ,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags
	)
{
	FLT_POSTOP_CALLBACK_STATUS
	      fl_status = FLT_POSTOP_FINISHED_PROCESSING;
	BOOL   bHandled = FALSE;

	PAGE_CODE_EX(fl_status);
	//
	// checks for draining scenario;
	//
	if (FlagOn( _uFlags, FLTFL_POST_OPERATION_DRAINING ))
		return fl_status;

#if DBG
	DbgPrint(
		"__fg_fs: [INFO] PostCleanupCallback() is invoked;\n"
		);
#endif
	//
	// if pre-cleanup event is handled by CDeleteNonTxEvtHandler::CFileInfoEvent::OnBefore()
	// the further execution of the callback is not proceeded;
	//
	fl_status = handlers::CChangeEvtHandler::CWriteEvent::OnAfter(
						_pFltData, _pFltObjects, _pCompletionCtx, _uFlags, bHandled
					);
	
	return fl_status;
}

FLT_PREOP_CALLBACK_STATUS
PreCleanupCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__out       PVOID*                    _ppCompletionCtx
	)
{
	FLT_PREOP_CALLBACK_STATUS
	      fl_status = FLT_PREOP_SUCCESS_NO_CALLBACK;
	BOOL   bHandled = FALSE;

	PAGE_CODE_EX(fl_status);
	//
	// the 1st level of filtering events (by system thread)
	//
	if (PsIsSystemThread(PsGetCurrentThread()))
		return fl_status;
	if (FLT_IS_FS_FILTER_OPERATION(_pFltData))
		return fl_status;

	fl_status = handlers::CDeleteNonTxEvtHandler::CFileInfoEvent::OnBefore(
						_pFltData, _pFltObjects, _ppCompletionCtx, bHandled
					);
	if (bHandled)
		return fl_status;

	return fl_status;
}