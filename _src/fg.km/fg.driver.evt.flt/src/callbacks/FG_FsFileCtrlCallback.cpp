/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Nov-2017 at 11:43:55a, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian mini-filter driver file system control callback interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsFileCtrlCallback.h"
#include "FG_FsModifyEvtHandler.h"

/////////////////////////////////////////////////////////////////////////////

FLT_PREOP_CALLBACK_STATUS
PreFileCtrlCallback(
	__inout     PFLT_CALLBACK_DATA        _pFltData      ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects   ,
	__out       PVOID*                    _ppCompletionCtx
	)
{
	FLT_PREOP_CALLBACK_STATUS fl_status = FLT_PREOP_COMPLETE;
	BOOL bHandled = FALSE;

	UNUSED(_pFltData);
	UNUSED(_pFltObjects);
	UNUSED(_ppCompletionCtx);
	UNUSED(bHandled);

	PAGED_CODE();

#if defined(_USE_COMPLEX_MODIFY)
	fl_status = handlers::CModifyEvtHandler::CFileCtrl::OnBefore(
						_pFltData, _pFltObjects, _ppCompletionCtx, bHandled
					);
#endif
	return fl_status;
}