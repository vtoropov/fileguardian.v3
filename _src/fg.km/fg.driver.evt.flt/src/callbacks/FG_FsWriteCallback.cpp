/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Nov-2017 at 11:30:13a, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian mini-filter driver file system close callback interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsWriteCallback.h"
#include "FG_FsModifyEvtHandler.h"

/////////////////////////////////////////////////////////////////////////////


FLT_PREOP_CALLBACK_STATUS
PreWriteCallback(
	__inout     PFLT_CALLBACK_DATA        _pFltData      ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects   ,
	__out       PVOID*                    _ppCompletionCtx
	)
{
	FLT_PREOP_CALLBACK_STATUS
	      fl_status = FLT_PREOP_SUCCESS_NO_CALLBACK;
	BOOL   bHandled = FALSE;

	UNUSED(_pFltData);
	UNUSED(_pFltObjects);
	UNUSED(_ppCompletionCtx);

	PAGED_CODE();
#if defined(_USE_COMPLEX_MODIFY)
	fl_status = handlers::CModifyEvtHandler::CWriteEvent::OnBefore(
						_pFltData, _pFltObjects, _ppCompletionCtx, bHandled
					);
#else
	fl_status= handlers::CModifyEvtHandler::CWriteEvent::OnBeforeNonTransacted(
		_pFltData, _pFltObjects, _ppCompletionCtx, bHandled
	);
#endif
	if (bHandled)
		return fl_status;

	return fl_status;
}