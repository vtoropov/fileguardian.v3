/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Aug-2017 at 11:17:45a, UTC+7, Phuket, Rawai, Monday;
	This is File Guardian mini-filter driver setting information callback interface file implementation. 
*/
#include "StdAfx.h"
#include "FG_FsSetInfoCallback.h"
#include "FG_FsRenameEvtHandler.h"
#include "FG_FsDelNoTxEvtHandler.h"
#include "FG_FsModifyEvtHandler.h"
#include "FG_FsChangeEvtHandler.h"

/////////////////////////////////////////////////////////////////////////////

FLT_POSTOP_CALLBACK_STATUS  FLTAPI
PostSetInfoCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData         ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects      ,
	__in        PVOID                     _pCompletionCtx   ,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags
	)
{
	UNUSED (_pFltObjects);
	UNUSED (_uFlags);

	FLT_POSTOP_CALLBACK_STATUS
	            fl_status    = FLT_POSTOP_FINISHED_PROCESSING;
	BOOL        bHandled     = FALSE;

	PAGE_CODE_EX(fl_status);
	//
	// checks for draining scenario;
	//
	if (FlagOn( _uFlags, FLTFL_POST_OPERATION_DRAINING ))
		return fl_status;

	if (NULL == _pCompletionCtx)
		return fl_status;

	fl_status = handlers::CRenameEvtHandler::CFileInfoEvent::OnAfter(_pFltData, _pFltObjects, _pCompletionCtx, _uFlags, bHandled);
	if (bHandled)
		return fl_status;

	fl_status = handlers::CDeleteNonTxEvtHandler::CFileInfoEvent::OnAfter(_pFltData, _pFltObjects, _pCompletionCtx, _uFlags, bHandled);
	if (bHandled)
		return fl_status;

	return fl_status;
}

FLT_PREOP_CALLBACK_STATUS  FLTAPI
PreSetInfoCallback(
	__inout     PFLT_CALLBACK_DATA        _pFltData         ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects      ,
	__inout_opt PVOID*                    _ppCompletionCtx
)
{
	FLT_PREOP_CALLBACK_STATUS
	            fl_status     = FLT_PREOP_SUCCESS_NO_CALLBACK;
	BOOL        bHandled      = FALSE;

	PAGE_CODE_EX(fl_status);

	//
	// the 1st level of filtering events (by system thread)
	//
	if (PsIsSystemThread(PsGetCurrentThread()))
		return fl_status;
	if (FLT_IS_FS_FILTER_OPERATION(_pFltData))
		return fl_status;

	//
	// the 2nd level of filtering events (by class of the event)
	//

	fl_status = handlers::CRenameEvtHandler::CFileInfoEvent::OnBefore(_pFltData, _pFltObjects, _ppCompletionCtx, bHandled);
	if (bHandled)
		return fl_status;
	
	fl_status = handlers::CDeleteNonTxEvtHandler::CFileInfoEvent::OnBefore(_pFltData, _pFltObjects, _ppCompletionCtx, bHandled);
	if (bHandled)
		return fl_status;

#if defined(_USE_COMPLEX_MODIFY)
	fl_status = handlers::CModifyEvtHandler::CWriteEvent::OnBefore(
						_pFltData, _pFltObjects, _ppCompletionCtx, bHandled
					);
#else
	fl_status = handlers::CChangeEvtHandler::CFileInfoEvent::OnBefore(
						_pFltData, _pFltObjects, _ppCompletionCtx, bHandled
					);
#endif

	return fl_status;
}