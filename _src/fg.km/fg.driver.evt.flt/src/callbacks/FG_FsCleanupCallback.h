#ifndef _FGFSCLEANUPHANDLER_H_90C5B54E_77AD_44a1_A5F9_7FA2CE75DB2A_INCLUDED
#define _FGFSCLEANUPHANDLER_H_90C5B54E_77AD_44a1_A5F9_7FA2CE75DB2A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Aug-2017 at 2:10:15a, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian mini-filter driver file system cleaning up callback interface declaration file.
*/
#include "FG_FsFilterIface.h"

/*
Routine Description:
    This routine is the post-operation completion routine for
    IRP_MJ_CLEANUP in this miniFilter.
    Post-cleanup is the core of the file/directory deletion event handling.
    Here we check to see if the stream or file were deleted and report that through record entry storage.

Arguments:
    _pFltData       - Pointer to the filter callbackData that is passed to us.
    _pFltObjects    - Pointer to the FLT_RELATED_OBJECTS data structure containing
                      opaque handles to this filter, instance, its associated volume and
                      file object.
    _pCompletionCtx - The completion context set in the pre-operation routine.
    _uFlags         - Denotes whether the completion is successful or is being drained.

Return Value:
    FLT_POSTOP_FINISHED_PROCESSING - we never do any sort of asynchronous
                                     processing here.
*/
FLT_POSTOP_CALLBACK_STATUS FLTAPI
PostCleanupCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData      ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects   ,
	__in        PVOID                     _pCompletionCtx,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags
	);

/*
Routine Description:
    This routine is the pre-operation completion routine for
    IRP_MJ_CLEANUP in this miniFilter.
    In the preop callback for cleanup, we obtain the file information and
    save it in the stream context, just so we have a name to use when
    reporting file deletions.
    That is done for every stream with an attached stream context because
    those will be deletion candidates most of the time.

Arguments:
    Data              - Pointer to the filter callbackData that is passed to us.
    FltObjects        - Pointer to the FLT_RELATED_OBJECTS data structure containing
                        opaque handles to this filter, instance, its associated volume and
                        file object.
    CompletionContext - The context for the completion routine for this
                        operation.
Return Value:
    FLT_PREOP_SYNCHRONIZE - we never do any sort of asynchronous processing
                            here, and we want to synchronize the postop.
    FLT_PREOP_SUCCESS_NO_CALLBACK - when we don't manage to get a stream
                            context.
*/
FLT_PREOP_CALLBACK_STATUS
PreCleanupCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData      ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects   ,
	__out       PVOID*                    _ppCompletionCtx
	);

#endif/*_FGFSCLEANUPHANDLER_H_90C5B54E_77AD_44a1_A5F9_7FA2CE75DB2A_INCLUDED*/