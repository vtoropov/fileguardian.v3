#ifndef _FGFSWRITEHANDLER_H_6D77EADC_7488_44e2_BA41_B4F61CEF1F72_INCLUDED
#define _FGFSWRITEHANDLER_H_6D77EADC_7488_44e2_BA41_B4F61CEF1F72_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Nov-2017 at 10:46:23a, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian mini-filter driver file system close callback interface declaration file.
*/
#include "FG_FsFilterIface.h"

/*
*/
FLT_PREOP_CALLBACK_STATUS
PreWriteCallback(
	__inout     PFLT_CALLBACK_DATA        _pFltData      ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects   ,
	__out       PVOID*                    _ppCompletionCtx
	);

#endif/*_FGFSWRITEHANDLER_H_6D77EADC_7488_44e2_BA41_B4F61CEF1F72_INCLUDED*/