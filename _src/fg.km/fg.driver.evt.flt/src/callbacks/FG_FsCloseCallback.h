#ifndef _FGFSCLOSEHANDLER_H_10E863D1_9499_4c41_AD30_923B908C783A_INCLUDED
#define _FGFSCLOSEHANDLER_H_10E863D1_9499_4c41_AD30_923B908C783A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Nov-2017 at 10:12:52a, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian mini-filter driver file system close callback interface declaration file.
*/
#include "FG_FsFilterIface.h"

/*
	Pre-close callback makes a file context persistent in the volatile cache;
    If the file is transacted, it will be synchronized at KTM notification callback 
    at commit event.
*/
FLT_PREOP_CALLBACK_STATUS
PreCloseCallback(
	__inout     PFLT_CALLBACK_DATA        _pFltData      ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects   ,
	__out       PVOID*                    _ppCompletionCtx
	);

#endif/*_FGFSCLOSEHANDLER_H_10E863D1_9499_4c41_AD30_923B908C783A_INCLUDED*/