/*
	Created by Tech_dog (ebontrop@gmail.com) on 31-Dec-2017 at 1:01:18p, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian minifilter driver flush data callback interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsFlushDataCallback.h"
#include "FG_FsChangeEvtHandler.h"

FLT_POSTOP_CALLBACK_STATUS FLTAPI
PostFlushDataCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData      ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects   ,
	__in        PVOID                     _pCompletionCtx,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags
	)
{
	FLT_POSTOP_CALLBACK_STATUS
	     fl_status = FLT_POSTOP_FINISHED_PROCESSING;
	BOOL bHandled  = FALSE;
	//
	// passed code macro produces asserts here and leads to blue screen of death;
	//
	PAGE_CODE_EX(FLT_POSTOP_FINISHED_PROCESSING);
	//
	// checks for draining scenario;
	//
	if (FlagOn( _uFlags, FLTFL_POST_OPERATION_DRAINING ))
		return fl_status;

	fl_status = handlers::CChangeEvtHandler::CWriteEvent::OnAfter(
					_pFltData, _pFltObjects, _pCompletionCtx, _uFlags, bHandled
				);

	return fl_status;
}