#ifndef _FGFSFLUSHDATACALLBACK_H_045F8D88_20AB_44ef_8811_7292CEF6469E_INCLUDED
#define _FGFSFLUSHDATACALLBACK_H_045F8D88_20AB_44ef_8811_7292CEF6469E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 31-Dec-2017 at 12:38:47p, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian minifilter driver flush data callback interface declaration file.
*/
#include "FG_FsFilterIface.h"

FLT_POSTOP_CALLBACK_STATUS FLTAPI
PostFlushDataCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData      ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects   ,
	__in        PVOID                     _pCompletionCtx,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags
	);

#endif/*_FGFSFLUSHDATACALLBACK_H_045F8D88_20AB_44ef_8811_7292CEF6469E_INCLUDED*/