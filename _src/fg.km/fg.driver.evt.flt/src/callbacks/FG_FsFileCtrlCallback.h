#ifndef _FGFSFILECTRLHANDLER_H_CB25F949_CBDB_46a2_9B32_F819B7D2EFCB_INCLUDED
#define _FGFSFILECTRLHANDLER_H_CB25F949_CBDB_46a2_9B32_F819B7D2EFCB_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Nov-2017 at 11:39:22a, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian mini-filter driver file system control callback interface declaration file.
*/
#include "FG_FsFilterIface.h"

/*
	Pre-file system control event callback.
*/
FLT_PREOP_CALLBACK_STATUS
PreFileCtrlCallback(
	__inout     PFLT_CALLBACK_DATA        _pFltData      ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects   ,
	__out       PVOID*                    _ppCompletionCtx
	);


#endif/*_FGFSFILECTRLHANDLER_H_CB25F949_CBDB_46a2_9B32_F819B7D2EFCB_INCLUDED*/