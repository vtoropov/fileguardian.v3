#ifndef _FGFSMODIFYEVTHANDLER_H_07B879C5_F116_4189_8807_12442E7A22F2_INCLUDED
#define _FGFSMODIFYEVTHANDLER_H_07B879C5_F116_4189_8807_12442E7A22F2_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Oct-2017 at 9:32:59p, UTC+7, Phuket, Rawai, Thursday;
	This is File Guardian minifilter driver file content modify event interface declaration file.
*/
#include "FG_FsFilterIface.h"

namespace handlers {

	/*
		This handler is transaction-aware; it tracks if the files are 'dirty' by intercepting write I/O requests;
		such approach provides a way to track modifications to a file, additionally, it handles the case where 
		the transaction commits or rollbacks.
	*/
	class CModifyEvtHandler {
	public:

		class CWriteEvent {
		public:
			/*
				This operation for events with *exclusive write* attribute;
				such as, IRP_MJ_SET_INFORMATION | IRP_MJ_WRITE (pre-condition);
			*/
			static
			FLT_PREOP_CALLBACK_STATUS
			OnBefore(
				__inout PFLT_CALLBACK_DATA        _pFltData       ,
				__in    PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__in    PVOID*                    _ppCompletionCtx,
				__inout BOOL&                     _bHandled
				);

			static
			FLT_PREOP_CALLBACK_STATUS
			OnBeforeNonTransacted(
				__inout PFLT_CALLBACK_DATA        _pFltData       ,
				__in    PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__in    PVOID*                    _ppCompletionCtx,
				__inout BOOL&                     _bHandled
				);
			//
			// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/content/fltkernel/ns-fltkernel-_flt_parameters
			//

			static BOOLEAN
				IsWritable(
					__inout PFLT_CALLBACK_DATA        _pFltData
				);

			static BOOLEAN
			IsWritable_OnCreate(
				__inout PFLT_CALLBACK_DATA        _pFltData
				);
		};

		class CCreateEvent {
		public:
			/*
				This is the post-create completion routine.
				In this routine, file context and/or transaction context shall be
				created if not exists.
			*/
			static
			FLT_POSTOP_CALLBACK_STATUS FLTAPI
			OnAfter (
				__inout PFLT_CALLBACK_DATA        _pFltData       ,
				__in    PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__in    PVOID                     _pCompletionCtx ,
				__in    FLT_POST_OPERATION_FLAGS  _uFlags         ,
				__inout BOOL&                     _bHandled
				);

			/*
				This routine is for synchronous calling post-create procedure from pre-create ones.
				Return Value:
				FLT_PREOP_SYNCHRONIZE
			*/
			static
			FLT_PREOP_CALLBACK_STATUS FLTAPI
			OnBefore (
				__inout PFLT_CALLBACK_DATA        _pFltData       ,
				__inout BOOL&                     _bHandled
				);
		};

		class CClose {
		public:
			/*
				Pre-close callback; it makes a file context persistent in volatile cache;
				If the file is transacted, it will be synchronized at KTM notification callback 
				if committed.
			*/
			static
			FLT_PREOP_CALLBACK_STATUS FLTAPI
			OnBefore (
				__inout PFLT_CALLBACK_DATA        _pFltData       ,
				__in    PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__out   PVOID*                    _ppCompletionCtx,
				__inout BOOL&                     _bHandled
				);
		};

		class CFileCtrl {
		public:
			/*
				This routine blocks save point features;
			*/
			static
			FLT_PREOP_CALLBACK_STATUS FLTAPI
			OnBefore (
				__inout PFLT_CALLBACK_DATA        _pFltData       ,
				__in    PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__inout PVOID*                    _ppCompletionCtx,
				__inout BOOL&                     _bHandled
			);
		};

	public:
		/*
			This is transaction callback that is called for transaction commitment/rollback;
		*/
		static
		NTSTATUS
		OnTransactionEnd (
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
			__in        PFLT_CONTEXT              _pTxContext     ,
			__in        ULONG                     _uTxNotifyCode  ,
			__inout     BOOL&                     _bHandled
			);
	};
}

#endif/*_FGFSMODIFYEVTHANDLER_H_07B879C5_F116_4189_8807_12442E7A22F2_INCLUDED*/