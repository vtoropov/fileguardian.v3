#ifndef _FGFSGENERICHANDLER_H_3B1C8722_FC82_47d5_8389_8E535798D4D2_INCLUDED
#define _FGFSGENERICHANDLER_H_3B1C8722_FC82_47d5_8389_8E535798D4D2_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) 11-Jul-2017 at 8:08:35p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian minifilter driver IRP generic handler interface declaration file.
*/

FLT_POSTOP_CALLBACK_STATUS FLTAPI PostGenericOperCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData         ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects      ,
	__in        PVOID                     _pCompletionCtx   ,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags
	);

/*
	This routine receives ALL pre-operation callbacks for this filter. It then
	tries to log information about the given operation. If it is able
	to log information then post-operation callback routine is called.

	NOTE:  This routine must be NON-PAGED because it can be called on the
	       paging path.
*/
FLT_PREOP_CALLBACK_STATUS  FLTAPI PreGenericOperCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData         ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects      ,
	__deref_out_opt PVOID*                _pPtrCompletionCtx
	);

#endif/*_FGFSGENERICHANDLER_H_3B1C8722_FC82_47d5_8389_8E535798D4D2_INCLUDED*/