/*
	Created by Tech_dog (ebontrop@gmail.com) 11-Jul-2017 at 8:08:35p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian minifilter driver IRP generic handler interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsGenericHandler.h"
#include "FG_FsRecordProc.h"
#include "FG_FsEventLogger.h"
#include "FG_FsTransactHandler.h"
#include "FG_FsDebugObject.h"
#include "FG_FsFilterData.h"

/////////////////////////////////////////////////////////////////////////////

FLT_POSTOP_CALLBACK_STATUS FLTAPI PostGenericOperCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData         ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects      ,
	__in        PVOID                     _pCompletionCtx   ,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags
	)
{
	UNUSED(_pFltData);
	UNUSED(_pFltObjects);
	UNUSED(_uFlags);

	PTRecordEntry pEntry = NULL;

	if (PsIsSystemThread(PsGetCurrentThread()))
	{
		RecFreeEntry( pEntry );
		pEntry = NULL;
		return FLT_POSTOP_FINISHED_PROCESSING;
	}

	pEntry = (PTRecordEntry)_pCompletionCtx;

	//
	//  if this instance is in the process of being torn down, don't bother to
	//  log this record, free it now.
	//
	if (FlagOn(_uFlags, FLTFL_POST_OPERATION_DRAINING))
	{
		RecFreeEntry( pEntry );
		pEntry = NULL;
		return FLT_POSTOP_FINISHED_PROCESSING;
	}

	if (NULL != pEntry) {

		LogPostOperationData( _pFltData, pEntry->Data );
		RecAppendEntry(pEntry);
	}
	return FLT_POSTOP_FINISHED_PROCESSING;
}

FLT_PREOP_CALLBACK_STATUS  FLTAPI PreGenericOperCallback (
	__inout     PFLT_CALLBACK_DATA        _pFltData         ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects      ,
	__deref_out_opt PVOID*                _pPtrCompletionCtx
	)
{
	UNUSED(_pFltData);
	UNUSED(_pFltObjects);
	UNUSED(_pPtrCompletionCtx);

	FLT_PREOP_CALLBACK_STATUS   clbk_status  = FLT_PREOP_SUCCESS_NO_CALLBACK;
	PFLT_FILE_NAME_INFORMATION  pNameInfo    = NULL;
	UNICODE_STRING*             pNameToUse   = NULL;
	NTSTATUS                    nt_status    = STATUS_SUCCESS;
	PTRecordEntry               pEntry       = NULL;
	static TDriverData&         drv_data     = global::DriverData();
	static TRecordStorage&      stg_data     = global::RecordStorage();
	static UNICODE_STRING       def_name     = RTL_CONSTANT_STRING(_T("#err_name"));

	//
	// the filter is not interested in kernel file operations;
	// only user file operations are considered;
	//
	if (PsIsSystemThread(PsGetCurrentThread()))
	{
		return FLT_PREOP_SUCCESS_NO_CALLBACK;
	}

	if (_pFltObjects->FileObject){
		//
		//  NOTE: By default, the query method FLT_FILE_NAME_QUERY_ALWAYS_ALLOW_CACHE_LOOKUP is used,
		//  because the filter would like to get the name as much as possible, but can cope if a name
		//  cannot be retrieved;
		//  TODO: for debugging mode, this is reasonable, but for production mode, the names should be
		//  queried reliably at times when it is known to be safe and use the query method
		//  FLT_FILE_NAME_QUERY_DEFAULT.
		//
		nt_status = FltGetFileNameInformation(
				_pFltData,
				FLT_FILE_NAME_NORMALIZED | stg_data.uNameQueryMethod,
				&pNameInfo
			);
		//
		// if no success with previous call to getting normalized file name,
		// tries to retrieve information for 'opened' file name;
		//
		if (!NT_SUCCESS(nt_status))
		{
			nt_status = FltGetFileNameInformation(
				_pFltData,
				FLT_FILE_NAME_OPENED | FLT_FILE_NAME_NORMALIZED,
				&pNameInfo
			);
		}
	}
	else
		nt_status = STATUS_UNSUCCESSFUL;

	if (NT_SUCCESS(nt_status))
	{
		pNameToUse = &pNameInfo->Name;
		if (FlagOn(
				drv_data.uDebugFlags, LOG_DEBUG_PARSE_NAMES
			))
			FltParseFileNameInformation( pNameInfo );
	}
	else {
		pNameToUse = &def_name;
	}

	if (FldIsIncludedToList(pNameToUse))
	{
		pEntry = RecAllocateEntry();

		if (pEntry)
		{
			RecSetEntryTarget(pEntry, pNameToUse);
			LogPreOperationData( _pFltData, _pFltObjects , pEntry->Data );

			if (_pFltData->Iopb->MajorFunction == IRP_MJ_SHUTDOWN) {
				PostGenericOperCallback(
					_pFltData, _pFltObjects, pEntry, 0
				);
			}
			else {
				*_pPtrCompletionCtx = pEntry;
				clbk_status = FLT_PREOP_SUCCESS_WITH_CALLBACK;
			}
		}
	}
	if (pNameInfo){
		FltReleaseFileNameInformation(pNameInfo);
		pNameInfo = NULL;
	}
	return clbk_status;
}