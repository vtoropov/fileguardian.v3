#ifndef _FGFSCHANGEEVTHANDLER_H_C53F556B_9026_4252_9DA7_06EF95B5F97F_INCLUDED
#define _FGFSCHANGEEVTHANDLER_H_C53F556B_9026_4252_9DA7_06EF95B5F97F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Dec-2017 at 7:43:40p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver file content modify (simplified) event interface declaration file.
*/
#include "FG_FsFilterIface.h"

namespace handlers {

	class CChangeEvtHandler {
	public:
		class CWriteEvent {
		public:
			/*
				This routine is called from the following callbacks:
				(a) IRP_MJ_CLEANUP;
				(b) IRP_MJ_FLUSH_BUFFERS;
				In all cases, this routine must be called from post-event handler;
			*/
			static
			FLT_POSTOP_CALLBACK_STATUS FLTAPI
			OnAfter (
				__inout     PFLT_CALLBACK_DATA        _pFltData       ,
				__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__in        PVOID                     _pCompletionCtx ,
				__in        FLT_POST_OPERATION_FLAGS  _uFlags         ,
				__inout     BOOL&                     _bHandled
				);
		};

		class CFileInfoEvent {
		public:
			/*
				This routine is specifically created for IRP_MJ_SET_INFORMATION pre-event handler;
			*/
			static
			FLT_PREOP_CALLBACK_STATUS FLTAPI
			OnBefore (
				__inout     PFLT_CALLBACK_DATA        _pFltData       ,
				__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__inout_opt PVOID*                    _ppCompletionCtx,
				__inout     BOOL&                     _bHandled
				);
		};
	};

}

#endif/*_FGFSCHANGEEVTHANDLER_H_C53F556B_9026_4252_9DA7_06EF95B5F97F_INCLUDED*/