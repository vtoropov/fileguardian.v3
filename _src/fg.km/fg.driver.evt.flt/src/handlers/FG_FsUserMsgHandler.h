#ifndef _FGFSMESSAGEHANDLER_H_707253BF_82F0_497a_82B4_B88FBA399853_INCLUDED
#define _FGFSMESSAGEHANDLER_H_707253BF_82F0_497a_82B4_B88FBA399853_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jul-2017 at 10:09:28p, UTC+7, Phuket, Rawai, Thursday;
	This is File Guardian minifilter driver message communication channel handler interface declaration file.
*/

NTSTATUS
DriverPortConnect(
	__in PFLT_PORT pClientPort,
	__in_opt PVOID pServerPortCookie,
	__in_bcount_opt(SizeOfContext) PVOID pConnectionContext,
	__in ULONG nSizeOfContext,
	__deref_out_opt PVOID* ppConnectionCookie
	);

VOID
DriverPortDisconnect(
	__in_opt PVOID pConnectionCookie
	);

NTSTATUS
DriverMessageHandler(
	__in PVOID       _pConnectionCookie,
	__in_bcount_opt( _nInputBufferSize ) PVOID _pInputBuffer,
	__in ULONG       _nInputBufferSize ,
	__out_bcount_part_opt(_nOutBufferSize,*_pReturnOutBufferSize) PVOID _pOutBuffer,
	__in ULONG       _nOutBufferSize   ,
	__out PULONG     _pReturnOutBufferSize
	);

#endif/*_FGFSMESSAGEHANDLER_H_707253BF_82F0_497a_82B4_B88FBA399853_INCLUDED*/