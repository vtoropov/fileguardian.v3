/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Feb-2018 at 10:02:44a, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver file object non-transactive delete event interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsDelNoTxEvtHandler.h"
#include "FG_FsNotifyData.h"
#include "fg.fs.evt.cmp.iface.h"
#include "FG_FsCtxMans.h"

using namespace handlers;
using namespace shared::km::fl_sys;

#include "fg.driver.proc.object.h"

using shared::km::proc_sys::CProcess;

/////////////////////////////////////////////////////////////////////////////

namespace handlers { namespace details {
	static const
	FLT_FILE_NAME_OPTIONS AnyDelete_GetTargetNameOps = FLT_FILE_NAME_OPENED|FLT_FILE_NAME_QUERY_DEFAULT|FLT_FILE_NAME_REQUEST_FROM_CURRENT_PROVIDER;

}}

/////////////////////////////////////////////////////////////////////////////

FLT_POSTOP_CALLBACK_STATUS FLTAPI
CDeleteNonTxEvtHandler::CFileInfoEvent::OnAfter (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__in        PVOID                     _pCompletionCtx ,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags         ,
	__inout     BOOL&                     _bHandled
	)
{
	FLT_POSTOP_CALLBACK_STATUS
	                fl_status = FLT_POSTOP_FINISHED_PROCESSING;
	PTFileObjectCtx  pFileCtx = (PTFileObjectCtx)_pCompletionCtx;
	ULONG          ui_proc_id = 0; ui_proc_id;

	UNUSED(_uFlags);

	_bHandled = FALSE;

	if (FileDispositionInformation != 
		_pFltData->Iopb->Parameters.SetFileInformation.FileInformationClass)
		return fl_status;
#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CDeleteNonTxEvtHandler::CFileInfoEvent::OnAfter() handles the event;\n"
		);
#endif
	_bHandled = TRUE;

	if (NULL == pFileCtx)
		return fl_status;

	ui_proc_id = CProcess::IdAsLong(_pFltData);

	notify::CNotifier::OnDeleteNotTransacted(
			pFileCtx->Operate.pSourceInfo, _pFltObjects, ui_proc_id
		);

	FltReleaseContext(pFileCtx); pFileCtx = NULL; // we do not need the file context at this point;

	return fl_status;
}

FLT_PREOP_CALLBACK_STATUS FLTAPI
CDeleteNonTxEvtHandler::CFileInfoEvent::OnBefore (
		__inout     PFLT_CALLBACK_DATA        _pFltData       ,
		__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
		__inout_opt PVOID*                    _ppCompletionCtx,
		__inout     BOOL&                     _bHandled
	)
{
	FLT_PREOP_CALLBACK_STATUS
	          fl_status  = FLT_PREOP_SUCCESS_NO_CALLBACK;
	NTSTATUS  nt_status  = STATUS_SUCCESS;

	fl_sys::CFileInfoPtr
	                 pSrcFileInfo;
	PTFileObjectCtx pFileObjectCtx = NULL;

	_bHandled = FALSE;

	if (FileDispositionInformation != 
		_pFltData->Iopb->Parameters.SetFileInformation.FileInformationClass)
		return fl_status;
#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CDeleteNonTxEvtHandler::CFileInfoEvent::OnBefore() handles the event;\n"
		);
#endif
	_bHandled = TRUE;

	//
	// queries source file info
	//
	nt_status = FltGetFileNameInformation(
					_pFltData, details::AnyDelete_GetTargetNameOps, pSrcFileInfo
				);
	if (!NT_SUCCESS( nt_status )||
		!pSrcFileInfo.IsValid()) {
		return fl_status;
	}

	if (!CFltCompare::IsIncludedToList(&pSrcFileInfo.Ptr()->Name)){  // filters out all events on object, which we are not watching for;

		return fl_status;
	}

	nt_status = ctx::CCtxFileOperateMan::GetContext(
						_pFltObjects, TRUE, (PFLT_CONTEXT*)&pFileObjectCtx // the context is allocated or is gotten as existing one;
					);

	if (!NT_SUCCESS(nt_status)){
		//
		// checks for context existence and destroys it if necessary;
		//
		if (NULL != pFileObjectCtx){
			ctx::CCtxFileOperateMan::Destroy(pFileObjectCtx);
			FltReleaseContext(pFileObjectCtx);
		}
		return fl_status;
	}

	if (NULL != pFileObjectCtx->Operate.pSourceInfo){
		FltReleaseFileNameInformation(pFileObjectCtx->Operate.pSourceInfo); // actually should never occur
	}

	pFileObjectCtx->Operate.pSourceInfo = pSrcFileInfo.Detach();

	*_ppCompletionCtx = (PVOID)pFileObjectCtx;

	return (fl_status = FLT_PREOP_SYNCHRONIZE);
}