#ifndef _FGFSCREATEEVTHANDLER_H_A7ECEA66_041D_484e_AB3E_EBB298BC2D48_INCLUDED
#define _FGFSCREATEEVTHANDLER_H_A7ECEA66_041D_484e_AB3E_EBB298BC2D48_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Sep-2017 at 7:08:30p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver file object create event handler interface declaration file.
*/
#include "FG_FsFilterIface.h"

namespace handlers {

	class CCreateEvtHandler {
	public:
		static
		FLT_POSTOP_CALLBACK_STATUS FLTAPI
		OnAfter (
			__inout     PFLT_CALLBACK_DATA        _pFltData       ,
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
			__in        PVOID                     _pCompletionCtx ,
			__in        FLT_POST_OPERATION_FLAGS  _uFlags         ,
			__inout     BOOL&                     _bHandled
			);

		static
		FLT_PREOP_CALLBACK_STATUS FLTAPI
		OnBefore (
			__inout     PFLT_CALLBACK_DATA        _pFltData       ,
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
			__out       PVOID*                    _ppCompletionCtx,
			__inout     BOOL&                     _bHandled
			);
	};
}

#endif/*_FGFSCREATEEVTHANDLER_H_A7ECEA66_041D_484e_AB3E_EBB298BC2D48_INCLUDED*/