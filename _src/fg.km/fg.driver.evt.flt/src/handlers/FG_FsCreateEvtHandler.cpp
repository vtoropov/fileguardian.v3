/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Sep-2017 at 0:57:53a, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian minifilter driver file object create event handler interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsCreateEvtHandler.h"
#include "FG_FsCtxMans.h"
#include "FG_FsNotifyData.h"
#include "fg.fs.evt.cmp.iface.h"
#include "FG_FsFileObject.h"

using namespace handlers;
using namespace shared::km::fl_sys;

#include "fg.driver.proc.object.h"

using shared::km::proc_sys::CProcess;

/////////////////////////////////////////////////////////////////////////////

namespace handlers { namespace details {

	static const
	FLT_FILE_NAME_OPTIONS AnyCreate_GetTargetNameOps = FLT_FILE_NAME_OPENED|FLT_FILE_NAME_QUERY_DEFAULT|FLT_FILE_NAME_REQUEST_FROM_CURRENT_PROVIDER;

	bool AnyCreate_GetPriorityLevel(void)
	{
#if (1)
		return true;
#else
#if DBG
		DbgPrint( "__fg_fs: [INFO] AnyCreate_GetPriorityLevel() refuses a request;\n" );
#endif
		return false; // no pre-condition is defined for returning 'true';
#endif
	}

}}

/////////////////////////////////////////////////////////////////////////////

FLT_POSTOP_CALLBACK_STATUS FLTAPI
CCreateEvtHandler::OnAfter (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__in        PVOID                     _pCompletionCtx ,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags         , /*FLTFL_POST_OPERATION_DRAINING*/
	__inout     BOOL&                     _bHandled
	)
{
	PFLT_FILE_NAME_INFORMATION pTargetInfo = NULL;
	FLT_POSTOP_CALLBACK_STATUS
	          fl_status  = FLT_POSTOP_FINISHED_PROCESSING;
	NTSTATUS  nt_status  = STATUS_SUCCESS;
	ULONG     ul_disp    = 0;
	ULONG     ul_opts    = 0;
	ULONG     ul_flags   = 0;
	BOOL      bAlertable = FALSE;
	ULONG     ui_proc_id = 0; ui_proc_id;

	UNUSED(_pFltObjects);
	UNUSED(_uFlags);
	UNUSED(_pCompletionCtx);

	_bHandled = FALSE;

	fl_sys::CFile::GetOptions(
			_pFltData->Iopb->Parameters, ul_disp, ul_opts
		);
	ul_flags  = (_pFltData->Iopb->IrpFlags);

	switch (ul_disp)
	{
	case FILE_CREATE:
		{
			//
			// queries target file info
			//
			nt_status = FltGetFileNameInformation(
							_pFltData, details::AnyCreate_GetTargetNameOps, &pTargetInfo
						);

			if (NT_SUCCESS( nt_status )
				&& CFltCompare::IsIncludedToList(&pTargetInfo->Name)) // filters out all events on objects which we do not watch for;
			{
				bAlertable = (0 != (FILE_OPEN_REPARSE_POINT & ul_opts));

				if (!(bAlertable)){
#if(0)
					ui_proc_id = FltGetRequestorProcessId(_pFltData);
#else
					ui_proc_id = CProcess::IdAsLong(_pFltData);
#endif
					notify::CNotifier::OnCreate(pTargetInfo, _pFltObjects, ui_proc_id);
				}
			}
			if (NULL != pTargetInfo)
				FltReleaseFileNameInformation(pTargetInfo);

			_bHandled = TRUE;
		} break;
	}

	return fl_status;
}

FLT_PREOP_CALLBACK_STATUS FLTAPI
CCreateEvtHandler::OnBefore (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__out       PVOID*                    _ppCompletionCtx,
	__inout     BOOL&                     _bHandled
	)
{
	FLT_PREOP_CALLBACK_STATUS
	          fl_status  = FLT_PREOP_SUCCESS_NO_CALLBACK;
	ULONG     ul_disp    = 0;
	NTSTATUS  nt_status  = STATUS_SUCCESS;

	UNUSED(_pFltObjects);
	UNUSED(_ppCompletionCtx);

	_bHandled = FALSE;
	nt_status = _pFltData->IoStatus.Status;
	if (!NT_SUCCESS( nt_status ) || (STATUS_REPARSE == nt_status))
		return fl_status;

	ul_disp   = fl_sys::CFile::GetDispose(_pFltData->Iopb->Parameters);

	switch (ul_disp)
	{
	case FILE_CREATE :
		{
			if (NULL != _pFltData->Iopb->TargetFileObject &&
			    NULL != _pFltData->Iopb->TargetFileObject->FileName.Buffer) {

				//
				// TODO: opening the check condition leads to hanging up on unregistering the filter;
				//       the hanging case must be reviewed: AnyCreate_GetPriorityLevel() returns true for now;
				//
				if (details::AnyCreate_GetPriorityLevel() ||
				    CFltCompare::IsIncludedToList(&_pFltData->Iopb->TargetFileObject->FileName)) {
					fl_status = FLT_PREOP_SUCCESS_WITH_CALLBACK;
					_bHandled = TRUE;
				}
			}

		} break;
	}
	return fl_status;
}