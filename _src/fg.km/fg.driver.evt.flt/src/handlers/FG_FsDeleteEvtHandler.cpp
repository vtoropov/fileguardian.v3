/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Sep-2017 at 5:58:59p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver file object delete event interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsDeleteEvtHandler.h"
#include "FG_FsCtxMans.h"
#include "FG_FsNotifyData.h"
#include "FG_FsCtxAlloca.h"
#include "fg.fs.evt.cmp.iface.h"

using namespace handlers;
using namespace shared::km::fl_sys;

/////////////////////////////////////////////////////////////////////////////

namespace handlers { namespace details {

	class CDeleteStreamCtx {
	public:
		CDeleteStreamCtx(void){}
		~CDeleteStreamCtx(void){}
	public:
		NTSTATUS
		ManageContext(
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects , // driver instance is required
			__in        PVOID                     _pTarget     , // target stream/transaction object
			__inout_opt PFLT_CONTEXT*             _ppContext   , // will be replaced with existing one if necessary
			__in        BOOLEAN                   _bTransact     // points to transaction type of the context
		)
		{
			NTSTATUS   nt_status = STATUS_SUCCESS;
			PFLT_CONTEXT pCurCtx = NULL;
			PFLT_CONTEXT pNewCtx = *_ppContext;                  // assigns to the context provided;

			PAGED_CODE();

			if (_bTransact)
				nt_status = FltGetTransactionContext( _pFltObjects->Instance, (PKTRANSACTION)_pTarget, &pCurCtx );
			else
				nt_status = FltGetStreamContext( _pFltObjects->Instance, (PFILE_OBJECT)_pTarget, &pCurCtx );

			if (STATUS_NOT_FOUND == nt_status) {
				//
				// there is no input context provided; it is very seldom case;
				//
				if (NULL == pNewCtx){

					nt_status = ctx::CStreamCtxAlloca::Allocate(&pNewCtx);
					if (!NT_SUCCESS(nt_status))
						return nt_status;
					else
						*_ppContext = pNewCtx;
				}
			}
			else if (!NT_SUCCESS(nt_status))
				return nt_status;
			else if (pNewCtx != pCurCtx) {

				if (NULL != pNewCtx)
					FltReleaseContext(pNewCtx);

				*_ppContext = pCurCtx; // returns the assigned context;

				return nt_status;
			}

			// sets income context to target object;
			if (_bTransact){
				nt_status = FltSetTransactionContext(
						_pFltObjects->Instance, (PKTRANSACTION)_pTarget, FLT_SET_CONTEXT_KEEP_IF_EXISTS, pNewCtx, &pCurCtx
					);
			}
			else{
				nt_status = FltSetStreamContext(
						_pFltObjects->Instance,  (PFILE_OBJECT)_pTarget, FLT_SET_CONTEXT_KEEP_IF_EXISTS, pNewCtx, &pCurCtx
					);
			}

			if (!NT_SUCCESS(nt_status)){

				if (STATUS_FLT_CONTEXT_ALREADY_DEFINED == nt_status){
					//
					// racing condition; we need to free the provided context and return the existing one;
					//
					FltReleaseContext(pNewCtx);    // frees the context provided/allocated;
					*_ppContext = pCurCtx;         // assigns the existing one;
					nt_status   = STATUS_SUCCESS;
				}
				else {
#if DBG
		DbgPrint(
			"__fg_fs: [WARN] CDeleteStreamCtx::ManageContext(): cannot set a context, err=%X;\n", nt_status
			);
#endif
				}
			}
			else if (_bTransact){
				nt_status = FltEnlistInTransaction(
						_pFltObjects->Instance, (PKTRANSACTION)_pTarget, pNewCtx, CTX_NOTIFICATION_MASK
					);
			}
			//
			// everything is fine; income context does not need to be changed at this point;
			// the context being returned must be released;
			//
			return nt_status;
		}
	};

}}

/////////////////////////////////////////////////////////////////////////////

FLT_POSTOP_CALLBACK_STATUS FLTAPI
CDeleteEvtHandler::CCreateEvent::OnAfter (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__in        PVOID                     _pCompletionCtx ,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags         ,
	__inout     BOOL&                     _bHandled
	)
{
	FLT_POSTOP_CALLBACK_STATUS
	            fl_status   = FLT_POSTOP_FINISHED_PROCESSING;
	NTSTATUS    nt_status   = STATUS_SUCCESS;
	PTStreamCtx pStreamCtx  = NULL;

	PAGED_CODE();

	_bHandled = FALSE;
	if (NULL == _pFltData)       // must be provided by filter manager;
		return fl_status;

	if (NULL == _pCompletionCtx) // must be provided by pre-create procedure;
		return fl_status;

	nt_status = _pFltData->IoStatus.Status;

	if (NT_SUCCESS( nt_status )
		&& (STATUS_REPARSE != nt_status)
		&& !FlagOn( _uFlags, FLTFL_POST_OPERATION_DRAINING )) {

		_bHandled = TRUE;
		//
		//  Flag the stream as a deletion candidate: try setting the context
		//  to the stream; the context is allocated by pre-create event handler;
		//
		nt_status = details::CDeleteStreamCtx().ManageContext(
									_pFltObjects, _pFltData->Iopb->TargetFileObject, &_pCompletionCtx, FALSE
								);

		if (NT_SUCCESS( nt_status )) {

			pStreamCtx  = (PTStreamCtx)_pCompletionCtx;
			//
			//  Set DeleteOnClose on the stream context: a delete-on-close stream will
			//  always be checked for deletion on cleanup.
			//
			pStreamCtx->bDeleteOnClose = BooleanFlagOn(
											_pFltData->Iopb->Parameters.Create.Options, FILE_DELETE_ON_CLOSE
										);
#if defined (_DEBUG)
			DbgPrint(
				"__fg_fs: [INFO] CDeleteEvtHandler::CCreateEvent::OnAfter(): Stream context is set and analyzed successfully;\n"
				);
#endif
		}
	}
	//
	// the context must be released due to it has been allocated or been gotten as existing one;
	//
	if (NULL != _pCompletionCtx)
		FltReleaseContext( _pCompletionCtx );

	return fl_status;
}

FLT_PREOP_CALLBACK_STATUS FLTAPI
CDeleteEvtHandler::CCreateEvent::OnBefore (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__out       PVOID*                    _ppCompletionCtx,
	__inout     BOOL&                     _bHandled
	)
{
	FLT_PREOP_CALLBACK_STATUS
	            fl_status   = FLT_PREOP_SUCCESS_NO_CALLBACK;
	NTSTATUS    nt_status   = STATUS_SUCCESS;

	UNUSED(_pFltObjects);

	PAGED_CODE();

	_bHandled = FALSE;
	if (NULL == _pFltData)
		return fl_status;
	//
	//  We're interested when the file is open for further deletion;
	//
	if (!FlagOn( _pFltData->Iopb->Parameters.Create.Options, FILE_DELETE_ON_CLOSE ))
		return fl_status;

	_bHandled = TRUE;

	//
	// creating a context, but not set it to the target object;
	//
	nt_status = ctx::CStreamCtxAlloca::Allocate(
									_ppCompletionCtx
								);

	if (!NT_SUCCESS( nt_status )){
		*_ppCompletionCtx = NULL;
		return (fl_status = FLT_PREOP_SUCCESS_NO_CALLBACK);
	}
	else {
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] CDeleteEvtHandler::CCreateEvent::OnBefore(): a context is created, post operation is synchronized;\n"
			);
#endif
		return (fl_status = FLT_PREOP_SYNCHRONIZE);
	}
}

/////////////////////////////////////////////////////////////////////////////

FLT_POSTOP_CALLBACK_STATUS FLTAPI
CDeleteEvtHandler::CCleanUpEvent::OnAfter (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__in        PVOID                     _pCompletionCtx ,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags         ,
	__inout     BOOL&                     _bHandled
	)
{
	FLT_POSTOP_CALLBACK_STATUS
	            flt_status    = FLT_POSTOP_FINISHED_PROCESSING;
	PTStreamCtx pStreamCtx    = NULL;
	NTSTATUS    nt_status     = STATUS_SUCCESS;
	FILE_STANDARD_INFORMATION fileInfo = {0};

	PAGED_CODE();

	if (NULL == _pCompletionCtx)
		return flt_status;

	pStreamCtx = (PTStreamCtx) _pCompletionCtx;

	if (FlagOn( _uFlags, FLTFL_POST_OPERATION_DRAINING )){
		FltReleaseContext(pStreamCtx);
		return flt_status;
	}

	_bHandled = TRUE;

	if (NT_SUCCESS( _pFltData->IoStatus.Status )) {
		//
		//  Determine whether or not we should check for deletion. What
		//  flags a file as a deletion candidate is one or more of the following:
		//
		// (1) NumOps > 0: this means there are or were racing changes to
		//     the file delete disposition state, and, in that case,
		//     we don't know what that state is. So, let's err to the side of
		//     caution and check if it was deleted.
		//
		// (2) bDeleteDispose: if this is TRUE and we haven't raced in setting delete
		//     disposition, this reflects the true delete disposition state of the
		//     file, meaning we must check for deletes if it is set to TRUE.
		//
		// (3) DeleteOnClose: if the file was ever opened with
		//     FILE_DELETE_ON_CLOSE, we must check to see if it was deleted.
		//
		//  Also, if a deletion of this stream was already notified, there is no
		//  point notifying it again.
		//
		if (((pStreamCtx->NumOps > 0) || (pStreamCtx->bDeleteDispose) || (pStreamCtx->bDeleteOnClose))
		     &&
		    (0 == pStreamCtx->IsNotified))
		{
			if (pStreamCtx->bDeleteOnClose && 0 == pStreamCtx->IsNotified) {

				notify::CNotifier::OnDelete(pStreamCtx, TRUE, NULL, _pFltObjects);
				return flt_status;
			}
			//
			//  The check for deletion is done via a query to
			//  FileStandardInformation. If that returns STATUS_FILE_DELETED
			//  it means the stream was deleted.
			//
			nt_status = FltQueryInformationFile(
							_pFltData->Iopb->TargetInstance, _pFltData->Iopb->TargetFileObject, &fileInfo, sizeof(fileInfo), FileStandardInformation, NULL
						);
			if (STATUS_FILE_DELETED == nt_status) {

				nt_status = CDeleteEvtHandler::CCleanUpEvent::Finalize(
								_pFltData, _pFltObjects, pStreamCtx
							);
			}
			else {
#if DBG
				DbgPrint(
					"__fg_fs: [WARN] CDeleteEvtHandler::CCleanUpEvent::OnAfter(): a stream is not being deleted [status=%X, ops=%d, disp=%d, close=%d, notified=%d];\n",
						nt_status, pStreamCtx->NumOps, pStreamCtx->bDeleteDispose, pStreamCtx->bDeleteOnClose, pStreamCtx->IsNotified
					);
#endif
			}
		}
	}
	FltReleaseContext( pStreamCtx ); pStreamCtx = NULL;
	return flt_status;
}

FLT_PREOP_CALLBACK_STATUS FLTAPI
CDeleteEvtHandler::CCleanUpEvent::OnBefore(
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__out       PVOID*                    _ppCompletionCtx,
	__inout     BOOL&                     _bHandled
	)
{
	FLT_PREOP_CALLBACK_STATUS
	              fl_status      = FLT_PREOP_SUCCESS_NO_CALLBACK;
	NTSTATUS      nt_status      = STATUS_SUCCESS;
	ctx::CStreamCtxPtr
	              pStreamCtxPtr;

	PAGED_CODE();

	nt_status = FltGetStreamContext(
					_pFltData->Iopb->TargetInstance, _pFltData->Iopb->TargetFileObject, _ppCompletionCtx
				);

	if (NT_SUCCESS( nt_status )) {

		_bHandled = TRUE;
		nt_status = pStreamCtxPtr.Attach(*_ppCompletionCtx);

		*_ppCompletionCtx = NULL;
		if (!NT_SUCCESS(nt_status))
			return FLT_PREOP_SUCCESS_NO_CALLBACK;
		//
        //  Only streams with stream context will be sent for deletion check
        //  in post-cleanup, which makes sense because they would only ever
        //  have one if they were flagged as candidates at some point.
        //
        //  Gather file information here so that we have a name to report.
        //  The name will be accurate most of the times, and in the cases it
        //  won't, it serves as a good clue and the stream context pointer
        //  value should offer a way to disambiguate that in case of renames
        //  etc.
        //
		//  passes from pre-callback to post-callback
		//  TODO:
		//      detaching must be made later for auto-release memory on failure by the pointer;
		*_ppCompletionCtx = pStreamCtxPtr.Detach();
		nt_status = fl_sys::CFile::SetInfoToCtx( _pFltData, *_ppCompletionCtx, TRUE );

		if (NT_SUCCESS( nt_status )) {
			//
			// this is the last point where we can access the file object being deleted:
			// the file is open at this time (we have a handle to it) and is accessible;
			// after exiting this pre-event handler, the object will not be valid anymore;
			//
			if (!fl_sys::CFile::IsDirectory(_pFltObjects->Instance, _pFltObjects->FileObject)){
			}

			return (fl_status = FLT_PREOP_SYNCHRONIZE);
		}
	}
	else {
		//
		// there is no reason to call post-event handler because there is no stream context;
		//
		fl_status = FLT_PREOP_SUCCESS_NO_CALLBACK;
	}
	
	if (NULL != *_ppCompletionCtx){
		FltReleaseContext(*_ppCompletionCtx); *_ppCompletionCtx = NULL;
	}

	return fl_status;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CDeleteEvtHandler::CCleanUpEvent::Finalize (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__in        PFLT_CONTEXT              _pStreamCtx
	)
{
	BOOLEAN       bTransaction   = FALSE;
	BOOLEAN       bFileDeleted   = FALSE;
	NTSTATUS      nt_status      = STATUS_SUCCESS;
	PTStreamCtx   pStreamCtx     =(PTStreamCtx)_pStreamCtx;
	PTTransactCtx pTransCtx      = NULL;

	PAGED_CODE();

	bTransaction = (NULL != _pFltObjects->Transaction);
#if (1)
	bTransaction = FALSE; // removes transacted deletion;
#endif

	if (bTransaction){

		nt_status = details::CDeleteStreamCtx().ManageContext(
						_pFltObjects, _pFltObjects->Transaction, (PFLT_CONTEXT*)&pTransCtx, bTransaction
					);
		if (!NT_SUCCESS( nt_status )) {
			if (NULL != pTransCtx){
				FltReleaseContext(pTransCtx); pTransCtx = NULL;
			}
			return nt_status;
		}
	}

	//
	//  Notify deletion. If this is an alternate data stream is being deleted,
	//  check if the whole file was deleted (by calling file::IsFileDeleted) as
	//  this could be the last handle to a delete-pending file.
	//
	nt_status = fl_sys::CFile::IsDeleted(
						_pFltData->Iopb->TargetInstance, _pFltData->Iopb->TargetFileObject, _pFltObjects, _pStreamCtx, bTransaction
					);
	if (STATUS_FILE_DELETED == nt_status){

		bFileDeleted = TRUE;
		nt_status    = STATUS_SUCCESS;;
	}

	if (NT_SUCCESS( nt_status )){
		notify::CNotifier::OnDelete(pStreamCtx, bFileDeleted, pTransCtx, _pFltObjects);
	}
	else {
#if DBG
		DbgPrint(
			"__fg_fs: [WARN] CDeleteEvtHandler::CCleanUpEvent::Finalize(): error occurred [%X];\n", nt_status
			);
#endif
	}

	if (NULL != pTransCtx)
		FltReleaseContext( pTransCtx );

	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

FLT_POSTOP_CALLBACK_STATUS FLTAPI
CDeleteEvtHandler::CFileInfoEvent::OnAfter (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__in        PVOID                     _pCompletionCtx ,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags         ,
	__inout     BOOL&                     _bHandled
	)
{
	FLT_POSTOP_CALLBACK_STATUS
	            fl_status    = FLT_POSTOP_FINISHED_PROCESSING;
	PTStreamCtx pStreamCtx   = NULL;

	UNUSED(_uFlags);
	UNUSED(_pFltObjects);

	PAGED_CODE();

	_bHandled = FALSE;

	if (FileDispositionInformation != 
		_pFltData->Iopb->Parameters.SetFileInformation.FileInformationClass)
		return fl_status;

	_bHandled  = TRUE;
	pStreamCtx = (PTStreamCtx) _pCompletionCtx;

	//
	//  Reaching a postop for FileDispositionInformation means we
	//  MUST have a stream context passed in the _pCompletionCtx.
	//
	if (NT_SUCCESS( _pFltData->IoStatus.Status )){
		//
		//  No synchronization is needed to set the bDeleteDispose field,
		//  because in case of races, the NumOps field will be perpetually
		//  positive, and it being positive is already an indication this
		//  file is a delete candidate, so it will be checked at post-
		//  -cleanup regardless of the value of bDeleteDispose field.
		//
		pStreamCtx->bDeleteDispose = ((PFILE_DISPOSITION_INFORMATION)
									   _pFltData->Iopb->Parameters.SetFileInformation.InfoBuffer)->DeleteFile;
	}
	//
	//  now that the operation is over, decrement NumOps;
	//
	InterlockedDecrement( &pStreamCtx->NumOps );
	FltReleaseContext( pStreamCtx );

	return fl_status;
}

FLT_PREOP_CALLBACK_STATUS FLTAPI
CDeleteEvtHandler::CFileInfoEvent::OnBefore (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__inout_opt PVOID*                    _ppCompletionCtx,
	__inout     BOOL&                     _bHandled
)
{
	FLT_PREOP_CALLBACK_STATUS
	             fl_status     = FLT_PREOP_SUCCESS_NO_CALLBACK;
#if (0)
	PTStreamCtx  pStreamCtx    = NULL;
	BOOLEAN      bIsRaced      = FALSE;
#else
	UNUSED(_ppCompletionCtx);
#endif
	NTSTATUS     nt_status     = STATUS_SUCCESS;
	PFLT_CONTEXT pContext      = NULL; pContext;

	fl_sys::CFileInfoPtr pTgtFileInfo;

	PAGED_CODE();

	_bHandled = FALSE;

	//
	//  we're interested when the file delete disposition changes;
	//

	if (FileDispositionInformation !=
		_pFltData->Iopb->Parameters.SetFileInformation.FileInformationClass)
		return fl_status;

	if (!((PFILE_DISPOSITION_INFORMATION)
		_pFltData->Iopb->Parameters.SetFileInformation.InfoBuffer)->DeleteFile)
		return fl_status;

	_bHandled = TRUE;
#if (0)
	nt_status = details::CDeleteStreamCtx().ManageContext(
					_pFltObjects, _pFltData->Iopb->TargetFileObject, &pContext, FALSE // the context is allocated or is gotten as existing one;
				);

	if (!NT_SUCCESS( nt_status )){
		//
		// we need to release the context if necessary before returning from this procedure;
		//
		if (NULL != pContext){
		//	ctx::CStreamCtxAlloca::Free(pContext);
			FltReleaseContext(pContext);
		}
		return (fl_status = FLT_PREOP_SUCCESS_NO_CALLBACK);
	}
	else
		pStreamCtx =  (PTStreamCtx)(pContext);
	//
	//  Race detection logic. The NumOps field in the StreamContext
	//  counts the number of in-flight changes to delete disposition
	//  on the stream.
	//
	//  If there's already some operations in flight, don't bother
	//  doing postop. Since there will be no postop, this value won't
	//  be decremented, staying forever 2 or more, which is one of
	//  the conditions for checking deletion at post-cleanup.
	//
	bIsRaced = (InterlockedIncrement( &pStreamCtx->NumOps ) > 1);

	if (!bIsRaced) {
		//
		//  This is the only operation in flight, so do a postop on
		//  it because the final outcome of the delete disposition
		//  state of the stream is deterministic.
		//
		*_ppCompletionCtx = (PVOID)pContext;
		fl_status = FLT_PREOP_SYNCHRONIZE;
	}
	else if (NULL != pStreamCtx){
		FltReleaseContext( pStreamCtx );
	}
#else

	nt_status = FltGetFileNameInformation(
					_pFltData, FLT_FILE_NAME_OPENED|FLT_FILE_NAME_QUERY_DEFAULT|FLT_FILE_NAME_REQUEST_FROM_CURRENT_PROVIDER, pTgtFileInfo
				);
	if (!NT_SUCCESS( nt_status )||
		!pTgtFileInfo.IsValid()) {
		return fl_status;
	}
	if (!CFltCompare::IsIncludedToList(&pTgtFileInfo.Ptr()->Name)){  // filters out all events on object, which we are not watching for;
		return fl_status;
	}
	notify::CNotifier::OnDeleteNotTransacted(
			pTgtFileInfo.Ptr(), _pFltObjects
		);

#endif
	return fl_status;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CDeleteEvtHandler::OnTransactionEnd (
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__in        PFLT_CONTEXT              _pTransCtx      ,
	__in        BOOLEAN                   _bCommitted     ,
	__inout     BOOL&                     _bHandled
)
{
	NTSTATUS nt_status         = STATUS_SUCCESS;
	PTDeleteNotifyData pNotify = NULL;
	PTTransactCtx pTransCtx    =(PTTransactCtx) _pTransCtx;

	UNUSED(_pFltObjects);

	PAGED_CODE();

	if (NULL == pTransCtx)
		return nt_status;

	if (!pTransCtx->DeleteData.bIsInitialized)
		return nt_status;

	_bHandled = TRUE;

	if (NULL == pTransCtx->DeleteData.pSyncResource)
		return nt_status;

	FltAcquireResourceExclusive( pTransCtx->DeleteData.pSyncResource );

	while (!IsListEmpty( &pTransCtx->DeleteData.NotifyList )) {

		pNotify = CONTAINING_RECORD(
						RemoveHeadList( &pTransCtx->DeleteData.NotifyList ), TDeleteNotifyData, links
					);
		if (NULL != pNotify->pContext){
			if (!_bCommitted){
				InterlockedDecrement( &((PTStreamCtx)pNotify->pContext)->IsNotified );
			}
			else{
				notify::CNotifier::OnDelete(pNotify->pContext, pNotify->bFileDelete, _pTransCtx, _pFltObjects);
			}
			FltReleaseContext( pNotify->pContext );
		}
		ExFreePool( pNotify );
	}

	FltReleaseResource( pTransCtx->DeleteData.pSyncResource );
	FltReleaseContext ( pTransCtx );

	return nt_status;
}