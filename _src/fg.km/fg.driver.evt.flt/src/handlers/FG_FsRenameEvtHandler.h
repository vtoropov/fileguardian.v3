#ifndef _FGFSRENAMEEVTHANDLER_H_3D71C695_D81E_4df8_93FA_98EDB2BA9861_INCLUDED
#define _FGFSRENAMEEVTHANDLER_H_3D71C695_D81E_4df8_93FA_98EDB2BA9861_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Sep-2017 at 2:25:42a, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian mini-filter driver file object rename/move event handler interface declaration file.
*/
#include "FG_FsFilterIface.h"

namespace handlers {

	class CRenameEvtHandler {
	public:
		class CFileInfoEvent {
		public:
			static
			FLT_POSTOP_CALLBACK_STATUS FLTAPI
			OnAfter (
				__inout     PFLT_CALLBACK_DATA        _pFltData       ,
				__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__in        PVOID                     _pCompletionCtx ,
				__in        FLT_POST_OPERATION_FLAGS  _uFlags         ,
				__inout     BOOL&                     _bHandled
				);

			static
			FLT_PREOP_CALLBACK_STATUS FLTAPI
			OnBefore (
				__inout     PFLT_CALLBACK_DATA        _pFltData       ,
				__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__inout_opt PVOID*                    _ppCompletionCtx,
				__inout     BOOL&                     _bHandled
			);
		};
	};
}

#endif/*_FGFSRENAMEEVTHANDLER_H_3D71C695_D81E_4df8_93FA_98EDB2BA9861_INCLUDED*/