/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Oct-2017 at 9:44:34p, UTC+7, Phuket, Rawai, Thursday;
	This is File Guardian minifilter driver file content modify event interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsModifyEvtHandler.h"
#include "FG_FsCtxMans.h"

using namespace handlers;

#include "FG_FsTxObject.h"
#include "fg.fs.evt.cmp.iface.h"
#include "FG_FsNotifyData.h"
#include "FG_FsCtxMans.h"

using namespace shared::km::fl_sys;

/////////////////////////////////////////////////////////////////////////////

namespace handlers { namespace details {

	class CModifyFileCtx {
	public:
		CModifyFileCtx(void){}
		~CModifyFileCtx(void){}
	public:
		NTSTATUS GetContext(PCFLT_RELATED_OBJECTS _pFltObjects, PFLT_CONTEXT* _ppContext)
		{
			NTSTATUS nt_status = STATUS_SUCCESS;

			PAGED_CODE();

			if (NULL ==  _ppContext)
				return (nt_status = STATUS_INVALID_PARAMETER);
			if (NULL != *_ppContext)
				return (nt_status = STATUS_INVALID_PARAMETER);

			nt_status = FltGetFileContext(
					_pFltObjects->Instance, _pFltObjects->FileObject, _ppContext
				);

			if (!NT_SUCCESS(nt_status))
				return nt_status;

			return nt_status;
		}
	};

	static const
	FLT_FILE_NAME_OPTIONS AnyModify_GetTargetNameOps = FLT_FILE_NAME_OPENED|FLT_FILE_NAME_QUERY_DEFAULT|FLT_FILE_NAME_REQUEST_FROM_CURRENT_PROVIDER;

	
	//
	// TODO: must be considered for all flags; but for now, only changing file data is important;
	//
	static const ULONG AnyModify_WriteFlags =
		FILE_WRITE_DATA | FILE_APPEND_DATA;

	static const ULONG AnyModify_WriteFlagsEx =
		FILE_WRITE_DATA | FILE_APPEND_DATA | DELETE | FILE_WRITE_ATTRIBUTES | FILE_WRITE_EA | WRITE_DAC | WRITE_OWNER | ACCESS_SYSTEM_SECURITY;
}}

/////////////////////////////////////////////////////////////////////////////

FLT_PREOP_CALLBACK_STATUS
CModifyEvtHandler::CWriteEvent::OnBefore(
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__in        PVOID*                    _ppCompletionCtx,
	__inout     BOOL&                     _bHandled
	)
{
	FLT_PREOP_CALLBACK_STATUS
	                    fl_status = FLT_PREOP_SUCCESS_NO_CALLBACK;
	NTSTATUS            nt_status = STATUS_SUCCESS;
	ctx::CFileObjCtxPtr pFileCtx;

	UNUSED(_pFltData);
	UNUSED(_ppCompletionCtx);

	PAGED_CODE();

	_bHandled = TRUE;

	if (!root::CTxObject::IsDirty(_pFltData)){
		return fl_status;
	}

	nt_status = details::CModifyFileCtx().GetContext(
					_pFltObjects, pFileCtx
				);
	if (!NT_SUCCESS(nt_status))
		return fl_status;

	if (NULL != pFileCtx->ModifyData.pTxCtx) {
#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CModifyEvtHandler::CWriteEvent::OnBefore(): file context is changed via transaction;\n"
		);
#endif
		pFileCtx->ModifyData.bTxDirty = TRUE;
	}
	else {
#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CModifyEvtHandler::CWriteEvent::OnBefore(): file context is changed directly;\n"
		);
#endif
		pFileCtx->ModifyData.bDirty = TRUE;
	}

	return fl_status;
}

FLT_PREOP_CALLBACK_STATUS
CModifyEvtHandler::CWriteEvent::OnBeforeNonTransacted(
	__inout PFLT_CALLBACK_DATA        _pFltData       ,
	__in    PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__in    PVOID*                    _ppCompletionCtx,
	__inout BOOL&                     _bHandled
)
{
	FLT_PREOP_CALLBACK_STATUS
	                    fl_status = FLT_PREOP_SUCCESS_NO_CALLBACK;
	NTSTATUS            nt_status = STATUS_SUCCESS;

	UNUSED(_pFltData);
	UNUSED(_pFltObjects);
	UNUSED(_ppCompletionCtx);
	UNUSED(_bHandled);
	UNUSED(nt_status);

	PAGE_CODE_EX(fl_status);

	if (CModifyEvtHandler::CWriteEvent::IsWritable(_pFltData) == FALSE)
		return fl_status;
	else
		_bHandled = TRUE;
#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CModifyEvtHandler::CWriteEvent::OnBeforeNonTransacted();\n"
	);
#endif
	notify::CNotifier::CChangeEvent::Notify(_pFltObjects);

	return fl_status;
}

BOOLEAN 
CModifyEvtHandler::CWriteEvent::IsWritable(
	__inout PFLT_CALLBACK_DATA        _pFltData
)
{
	if (NULL == _pFltData || NULL == _pFltData->Iopb)
		return FALSE;

	PAGE_CODE_EX(FALSE);

	return (0 != (_pFltData->Iopb->IrpFlags & IRP_WRITE_OPERATION));
}

BOOLEAN 
CModifyEvtHandler::CWriteEvent::IsWritable_OnCreate(
	__inout PFLT_CALLBACK_DATA        _pFltData
	)
{
	if (NULL == _pFltData ||
	    NULL == _pFltData->Iopb ||
	    NULL == _pFltData->Iopb->Parameters.Create.SecurityContext)
		return FALSE;

	PAGED_CODE();
	
	return (0 != (_pFltData->Iopb->IrpFlags & IRP_WRITE_OPERATION) &&
	        0 != (_pFltData->Iopb->Parameters.Create.SecurityContext->DesiredAccess &  (details::AnyModify_WriteFlagsEx)));
}

/////////////////////////////////////////////////////////////////////////////

FLT_POSTOP_CALLBACK_STATUS FLTAPI
CModifyEvtHandler::CCreateEvent::OnAfter (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__in        PVOID                     _pCompletionCtx ,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags         ,
	__inout     BOOL&                     _bHandled
	)
{
	PFLT_FILE_NAME_INFORMATION pTargetInfo = NULL;
	FLT_POSTOP_CALLBACK_STATUS
	         fl_status = FLT_POSTOP_FINISHED_PROCESSING;
	NTSTATUS nt_status = STATUS_SUCCESS;

	ctx::CFileObjCtxPtr pFileCtxPtr; 

	UNUSED(_uFlags);
	UNUSED(_bHandled);
	UNUSED(_pCompletionCtx);

	PAGED_CODE();

	if (NULL == _pFltData)
		return (fl_status);

	nt_status = _pFltData->IoStatus.Status;
	if (!NT_SUCCESS(nt_status) || STATUS_REPARSE == nt_status)
		return (fl_status);

	if (!CModifyEvtHandler::CWriteEvent::IsWritable_OnCreate(_pFltData))
		return fl_status;

	//
	// queries target file info
	//
	nt_status = FltGetFileNameInformation(
					_pFltData, details::AnyModify_GetTargetNameOps, &pTargetInfo
				);
	if (!NT_SUCCESS(nt_status)){
		return fl_status;
	}
	if (!CFltCompare::IsIncludedToList(&pTargetInfo->Name)){
		FltReleaseFileNameInformation(pTargetInfo);
		return fl_status;
	}
	else {
		FltReleaseFileNameInformation(pTargetInfo);
		_bHandled = TRUE;
	}

	nt_status = ctx::CCtxFileModifyMan::GetContext(
						_pFltData, pFileCtxPtr
					);

	if (!NT_SUCCESS(nt_status)) {
		//  this is non-intrusive driver; however, if tracking is critical for 
		//  the driver: we need to fail the creating via FltCancelFileOpen(...);
#if DBG
		DbgPrint(
			"__fg_fs: [ERR]  ctx::CCtxFileModifyMan::GetContext(): failed with code=%X;\n", nt_status
			);
#endif
		return (fl_status);
	}
	else {
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] ctx::CCtxFileModifyMan::GetContext(): pFileCtxPtr is created;\n"
			);
#endif
	}

	nt_status = root::CTxModifyFile::Process(
					_pFltObjects, pFileCtxPtr.Ptr()
				);

	if (!NT_SUCCESS(nt_status)){
#if DBG
#endif
	}

	pFileCtxPtr.Free();

	return fl_status;
}

FLT_PREOP_CALLBACK_STATUS FLTAPI
CModifyEvtHandler::CCreateEvent::OnBefore (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__inout     BOOL&                     _bHandled
	)
{
	FLT_PREOP_CALLBACK_STATUS
	          fl_status  = FLT_PREOP_SYNCHRONIZE;

	PAGED_CODE();

	_bHandled = FALSE;

	if (NULL == _pFltData){
		return (fl_status = FLT_PREOP_SUCCESS_NO_CALLBACK);
	}

	_bHandled = TRUE;

	return fl_status;
}

/////////////////////////////////////////////////////////////////////////////

FLT_PREOP_CALLBACK_STATUS FLTAPI
CModifyEvtHandler::CClose::OnBefore (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__out       PVOID*                    _ppCompletionCtx,
	__inout     BOOL&                     _bHandled
	)
{
	FLT_PREOP_CALLBACK_STATUS
	            fl_status = FLT_PREOP_SUCCESS_NO_CALLBACK;
	NTSTATUS    nt_status = STATUS_SUCCESS;

	UNUSED(_ppCompletionCtx);
	UNUSED(_pFltData);

	ctx::CFileObjCtxPtr pFileCtxPtr;
	nt_status = details::CModifyFileCtx().GetContext(
					_pFltObjects, pFileCtxPtr
				);

	if (!NT_SUCCESS(nt_status)){
#if DBG
		DbgPrint(
			"__fg_fs: [WARN] CModifyEvtHandler::CClose::GetContext(): failed with code=%X;\n", nt_status
			);
#endif
		return fl_status;
	}
	else{
		_bHandled = TRUE;
		fl_status = FLT_PREOP_SUCCESS_NO_CALLBACK;
	}

	//
	//  for non-transacted file(s),
	//  we need to generate notification here;
	//
	if (NULL == _pFltObjects->Transaction &&
	    TRUE ==  pFileCtxPtr->ModifyData.bDirty){
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] CModifyEvtHandler::CClose::GetContext(): event notifier is called;\n"
			);
#endif
		notify::CNotifier::CChangeEvent::Notify(
					pFileCtxPtr->ModifyData.FileRefEx
				);
	}

	return fl_status;
}

/////////////////////////////////////////////////////////////////////////////

FLT_PREOP_CALLBACK_STATUS FLTAPI
CModifyEvtHandler::CFileCtrl::OnBefore (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__inout     PVOID*                    _ppCompletionCtx,
	__inout     BOOL&                     _bHandled
)
{
	FLT_PREOP_CALLBACK_STATUS
	            fl_status = FLT_PREOP_COMPLETE;
	NTSTATUS    nt_status = STATUS_SUCCESS;

	UNUSED(_ppCompletionCtx);

	PAGED_CODE();
	if (NULL == _pFltData   ) return fl_status;
	if (NULL == _pFltObjects) return fl_status;

	_bHandled = TRUE;
	if (_pFltData->Iopb->Parameters.FileSystemControl.Common.FsControlCode == FSCTL_TXFS_SAVEPOINT_INFORMATION ) {
		//
		// blocks the save point request
		//
		nt_status = STATUS_NOT_SUPPORTED;
		_pFltData->IoStatus.Status = nt_status;
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] CModifyEvtHandler::CFileCtrl::OnPreAction(): a parse point is refused;\n"
			);
#endif
		return fl_status;
	}

	fl_status = CModifyEvtHandler::CWriteEvent::OnBefore(
						_pFltData, _pFltObjects, _ppCompletionCtx, _bHandled
					);
	return fl_status;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CModifyEvtHandler::OnTransactionEnd (
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__in        PFLT_CONTEXT              _pTxContext     ,
	__in        ULONG                     _uTxNotifyCode  ,
	__inout     BOOL&                     _bHandled
	)
{
	NTSTATUS nt_status = STATUS_SUCCESS;
	ctx::CTxContextPtr pTxCtxPtr;

	UNUSED(_pFltObjects);

	_bHandled = (TRANSACTION_NOTIFY_COMMIT_FINALIZE == _uTxNotifyCode || TRANSACTION_NOTIFY_ROLLBACK == _uTxNotifyCode);
	if (!_bHandled){
#if DBG
		DbgPrint(
			"__fg_fs: [WARN] CModifyEvtHandler::OnNotify()::tx_code=%X is ignored;\n", _uTxNotifyCode
			);
#endif
		return nt_status;
	}

	PAGED_CODE();

	nt_status = pTxCtxPtr.Attach(_pTxContext);
	if (!NT_SUCCESS(nt_status))
		return nt_status;

	if ( FlagOn( _uTxNotifyCode, TRANSACTION_NOTIFY_COMMIT_FINALIZE ) ){

		const ULONG uTxOutcome = TransactionOutcomeCommitted;

		nt_status = root::CTxModifyFile::ProcessOutcome(
					pTxCtxPtr.Ptr(), uTxOutcome
				);
	}
	else {

		const ULONG uTxOutcome = TransactionOutcomeAborted;

		nt_status = root::CTxModifyFile::ProcessOutcome(
					pTxCtxPtr.Ptr(), uTxOutcome
				);
	}

	return nt_status;
}