#ifndef _FGFSDELNOTXEVTHANDLER_H_659BCC9D_1CD4_47f2_9F92_5AB37AAB0AE3_INCLUDED
#define _FGFSDELNOTXEVTHANDLER_H_659BCC9D_1CD4_47f2_9F92_5AB37AAB0AE3_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Feb-2018 at 9:48:49a, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver file object non-transactive delete event interface declaration file.
*/
#include "FG_FsFilterIface.h"

namespace handlers {

	class CDeleteNonTxEvtHandler {

	public:
		class CFileInfoEvent {
		public:
			static
			FLT_POSTOP_CALLBACK_STATUS FLTAPI
			OnAfter (
				__inout     PFLT_CALLBACK_DATA        _pFltData       ,
				__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__in        PVOID                     _pCompletionCtx ,
				__in        FLT_POST_OPERATION_FLAGS  _uFlags         ,
				__inout     BOOL&                     _bHandled
				);

			static
			FLT_PREOP_CALLBACK_STATUS FLTAPI
			OnBefore (
				__inout     PFLT_CALLBACK_DATA        _pFltData       ,
				__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__inout_opt PVOID*                    _ppCompletionCtx,
				__inout     BOOL&                     _bHandled
			);
		};
	};
}


#endif/*_FGFSDELNOTXEVTHANDLER_H_659BCC9D_1CD4_47f2_9F92_5AB37AAB0AE3_INCLUDED*/