/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Dec-2017 at 8:11:28p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver file content modify (simplified) event interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsChangeEvtHandler.h"
#include "FG_FsNotifyData.h"
#include "fg.fs.evt.cmp.iface.h"

using namespace handlers;
using namespace shared::km::fl_sys;

#include "fg.driver.proc.object.h"

using shared::km::proc_sys::CProcess;

/////////////////////////////////////////////////////////////////////////////

FLT_POSTOP_CALLBACK_STATUS FLTAPI
CChangeEvtHandler::CWriteEvent::OnAfter (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__in        PVOID                     _pCompletionCtx ,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags         ,
	__inout     BOOL&                     _bHandled
	)
{
	FLT_POSTOP_CALLBACK_STATUS
	                fl_status  = FLT_POSTOP_FINISHED_PROCESSING;
	utils::CStringW pFileName;
	ULONG           ui_proc_id = 0; ui_proc_id;

	UNUSED(_pFltData);
	UNUSED(_pCompletionCtx);
	UNUSED(_uFlags);

	PAGED_CODE();

	if (PASSIVE_LEVEL != KeGetCurrentIrql())
		return fl_status;

	if (NULL == _pFltObjects)
		return fl_status;

	if (FALSE == _pFltObjects->FileObject->WriteAccess)
		return fl_status;

	fl_sys::CFile::FileName(_pFltData, pFileName);

#if DBG
	DbgPrint( "__fg_fs: [INFO] CChangeEvtHandler::CWriteEvent::OnAfter() target=%wZ;\n", (PUNICODE_STRING)pFileName );
#endif
	if (CFltCompare::IsIncludedToList(pFileName))
	{
		_bHandled  = TRUE;
		ui_proc_id = CProcess::IdAsLong(_pFltData);
		{
			notify::CNotifier::CChangeEvent::Notify(
						_pFltObjects, ui_proc_id
					);
		}
	}
	return fl_status;
}

/////////////////////////////////////////////////////////////////////////////

FLT_PREOP_CALLBACK_STATUS FLTAPI
CChangeEvtHandler::CFileInfoEvent::OnBefore (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__inout_opt PVOID*                    _ppCompletionCtx,
	__inout     BOOL&                     _bHandled
	)
{
	FLT_PREOP_CALLBACK_STATUS
	                fl_status  = FLT_PREOP_SUCCESS_NO_CALLBACK;
	utils::CStringW pFileName;
	ULONG           ui_proc_id = 0; ui_proc_id;

	UNUSED(_ppCompletionCtx);
	UNUSED(_pFltObjects);

	PAGED_CODE();

	if (PASSIVE_LEVEL != KeGetCurrentIrql())
		return fl_status;

	const FILE_INFORMATION_CLASS cls_ = _pFltData->Iopb->Parameters.SetFileInformation.FileInformationClass;

	switch (cls_) {
		case FileAllocationInformation:
		case FileEndOfFileInformation :
		case FileBasicInformation     :
			{
				fl_sys::CFile::FileNameUnsafe(_pFltObjects, pFileName);
#if DBG
	DbgPrint(
		"__fg_fs: [INFO] CChangeEvtHandler::CFileInfoEvent::OnBefore(): param=%d; target=%wZ;\n", cls_, (PUNICODE_STRING)pFileName
		);
#endif
				if (CFltCompare::IsIncludedToList(pFileName)){
					_bHandled  = TRUE;
					ui_proc_id = CProcess::IdAsLong(_pFltData);

					notify::CNotifier::CChangeEvent::Notify(
						_pFltObjects, ui_proc_id
					);
				}
			} break;
		default:
			_bHandled = FALSE;
	}

	return fl_status;
}