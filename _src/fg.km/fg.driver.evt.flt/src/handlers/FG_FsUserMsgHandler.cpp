/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jul-2017 at 11:05:20p, UTC+7, Phuket, Rawai, Thursday;
	This is File Guardian minifilter driver message communication channel handler interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsUserMsgHandler.h"
#include "fg.fs.filter.event.tracker.h"
#include "fg.fs.evt.cmp.iface.h"
#include "fg.fs.evt.cmp.rem.h"
#if(0)
#include "fg.driver.common.log.h"
#endif
using namespace shared::km::fl_sys;

/////////////////////////////////////////////////////////////////////////////

/*
 * DriverPortConnect
 *
 * This is called when user-mode connects to the server port - to establish a
 * connection. See PFLT_CONNECT_NOTIFY under FltCreateCommunicationPort:
 *
 * http://msdn.microsoft.com/en-us/library/windows/hardware/ff541931(v=vs.85).aspx
 *
 * To accept the connection, we return STATUS_CONNECT. Because
 * in FltCreateCommunicationPort we set MaxConnections to 1, we never have
 * to worry about denying this request... if the request makes it here we can
 * just assert there aren't any more.
 */

NTSTATUS
DriverPortConnect(
	__in PFLT_PORT pClientPort,
	__in_opt PVOID pServerPortCookie,
	__in_bcount_opt(SizeOfContext) PVOID pConnectionContext,
	__in ULONG nSizeOfContext,
	__deref_out_opt PVOID* ppConnectionCookie
	)
{
	UNUSED(pClientPort);
	UNUSED(pServerPortCookie);
	UNUSED(pConnectionContext);
	UNUSED(nSizeOfContext);
	UNUSED(ppConnectionCookie);

	PAGED_CODE();

#if DBG
	DbgPrint(
		"__fg_fs: [INFO] accepting connection...;\n"
		);
#endif

	global::DriverData().pClientPort  = pClientPort;
	global::DriverData().pUserProcess = PsGetCurrentProcess();

#if DBG
	DbgPrint(
		"__fg_fs: [INFO] the client connection is accepted;\n"
		);
#endif

	return STATUS_SUCCESS;
}

/*
 * DriverPortDisconnect
 *
 * This is called when user-mode disconnects the server port - to establish a
 * connection. See PFLT_CONNECT_NOTIFY under FltCreateCommunicationPort:
 *
 * http://msdn.microsoft.com/en-us/library/windows/hardware/ff541931(v=vs.85).aspx
 *
 * There is no return value. In our case, we maintain only a single connection
 * to a single client process, so if we get a call we know who it's from.
 */

VOID
DriverPortDisconnect(
	__in_opt PVOID pConnectionCookie
	)
{
	UNUSED(pConnectionCookie);
	TDriverData& drv_data = global::DriverData();

	PAGED_CODE();

#if DBG
	DbgPrint(
		"__fg_fs: [INFO] DriverPortDisconnect()::disconnecting from port...;\n"
		);
#endif

	FltCloseClientPort(
			drv_data.pFilter, &drv_data.pClientPort
		);
	drv_data.pClientPort  = NULL;
	drv_data.pUserProcess = NULL;

#if DBG
	DbgPrint(
		"__fg_fs: [INFO] DriverPortDisconnect()::client is disconnected from filter port;\n"
		);
#endif
}

/////////////////////////////////////////////////////////////////////////////

namespace handlers { namespace details {
#if (0)
	class CDriverCommand {
	private:
		mutable
		NTSTATUS        m_status;
		PVOID           m_pInput;
		ULONG           m_in_sz;
		PVOID           m_pOutput;
		ULONG           m_out_sz;
	public:
		CDriverCommand(const PVOID pInput, const ULONG _in_sz, const PVOID pOut, const ULONG _out_sz) :
			m_pInput(pInput), m_in_sz(_in_sz), m_pOutput(pOut), m_out_sz(_out_sz), m_status(STATUS_SUCCESS)
		{}
	public:
		BOOLEAN     IsValid(VOID)CONST {
			return ((m_pInput != NULL) &&
			        (m_in_sz >= (FIELD_OFFSET(TFs_Command, eCmdType) + sizeof(ULONG))));
		}

		NTSTATUS    ModifyLogFile  (VOID) {

			if (!this->IsValid())
				return (m_status = STATUS_INVALID_PARAMETER);

			DECLARE_UNICODE_STRING(path_, eFsCommandOpt::eWDataLen);

			__try {
				const ULONG  nFlags   = ((PTFs_Command)m_pInput)->nFlags;
				const WCHAR* lpszData = ((PTFs_Command)m_pInput)->wData;
				const ULONG  uDataLen = ((PTFs_Command)m_pInput)->nDataLen;

				if (lpszData && uDataLen) {
					::RtlCopyMemory(&path_, lpszData,  uDataLen);
					path_.Length = static_cast<USHORT>(uDataLen);
					path_.MaximumLength = eFsCommandOpt::eWDataLen;
				}
#if DBG
				DbgPrint(
					"__fg_fs: [INFO] CDriverCommand::ModifyLogFile() name=%wZ; size=%d; flags=%d;\n", &path_, path_.Length, nFlags
				);
#endif
				if (nFlags) {
					m_status = shared::km::log::CLogSettings::Path(path_, 0);
				}
				else if (NULL != m_pOutput) {
					m_status = shared::km::log::CLogSettings::Path(path_);
					if (NT_SUCCESS(m_status))
						::RtlCopyMemory(&((PTFs_Command)m_pOutput)->wData[0], &path_.Buffer[0], eFsCommandOpt::eWDataLen);
				}
				else
					m_status = STATUS_INVALID_PARAMETER;

			} __except(EXCEPTION_EXECUTE_HANDLER) {
				m_status = GetExceptionCode();
			}

			return m_status;
		}

		NTSTATUS    ModifyLogVerbose  (void) {

			if (!this->IsValid())
				return (m_status = STATUS_INVALID_PARAMETER);

			__try {

				const ULONG uFlags = ((PTFs_Command)m_pInput)->nFlags;
				const ULONG uValue = ((PTFs_Command)m_pInput)->nNumeric;

				if (uFlags) {
					shared::km::log::CLogSettings::Verbose(static_cast<shared::km::log::CLogVerbose::_e>(uValue));
				}
				else if (NULL != m_pOutput)
					((PTFs_Command)m_pOutput)->nNumeric = static_cast<ULONG>(shared::km::log::CLogSettings::Verbose());
				else
					m_status = STATUS_INVALID_PARAMETER;

			} __except(EXCEPTION_EXECUTE_HANDLER) {
				m_status = GetExceptionCode();
			}

			return m_status;
		}

		NTSTATUS    ModifyLogOptions  (void) {

			if (!this->IsValid())
				return (m_status = STATUS_INVALID_PARAMETER);

			__try {

				const ULONG uFlags = ((PTFs_Command)m_pInput)->nFlags;
				const ULONG uValue = ((PTFs_Command)m_pInput)->nNumeric;

				if (uFlags) {
					shared::km::log::CLogOptions::Set(uValue);
				}
				else if (NULL != m_pOutput)
					((PTFs_Command)m_pOutput)->nNumeric = shared::km::log::CLogOptions::Get();
				else
					m_status = STATUS_INVALID_PARAMETER;

			} __except(EXCEPTION_EXECUTE_HANDLER) {
				m_status = GetExceptionCode();
			}

			return m_status;
		}

		NTSTATUS    ModifyRemovable(VOID) {

			DECLARE_UNICODE_STRING(path_, _COMP_PATH);
#if DBG
			DbgPrint(
				"__fg_fs: [INFO] CDriverCommand::ModifyRemovable is invoked;\n"
			);
#endif
			if (!this->IsValid())
				return (m_status = STATUS_INVALID_PARAMETER);

			__try {

				PTFs_Command p_cmd = reinterpret_cast<PTFs_Command>(m_pInput);
				if (NULL ==  p_cmd)
					return (m_status = STATUS_INVALID_PARAMETER);

				const ULONG  nFlags   = p_cmd->nFlags;
				const WCHAR* lpszData = p_cmd->wData;
				const ULONG  uDataLen = p_cmd->nDataLen;

				if (lpszData && uDataLen) {
					::RtlCopyMemory(
						path_.Buffer, lpszData, uDataLen  // this initialization does not require freeing memory;
					);
					path_.Length = static_cast<USHORT>(uDataLen);
#if DBG
					DbgPrint(
						"__fg_fs: [INFO] CDriverCommand::ModifyRemovable: data-len=%d; nFlags=%d; path=%wZ;\n", uDataLen, nFlags, &path_
					);
#endif
				}

				if (nFlags)  m_status = removable::CRemPersistent::AppendRemovable(&path_);
				else         m_status = removable::CRemPersistent::DeleteRemovable(&path_);

			} __except(EXCEPTION_EXECUTE_HANDLER) {
				m_status = GetExceptionCode();
			}

			return m_status;
		}

		NTSTATUS    LastResult(VOID)CONST { return  m_status; }

		eFsCommandType::_e
		            Type(VOID)CONST {

			eFsCommandType::_e eType = eFsCommandType::eNone;
			if (!this->IsValid()) {
				m_status = STATUS_INVALID_PARAMETER;
				return eType;
			}

			__try {

				eType = ((PTFs_Command)m_pInput)->eCmdType;

			} __except(EXCEPTION_EXECUTE_HANDLER) {
				m_status = GetExceptionCode();
			}

			return eType;
		}
	};
#endif
	class CDriverCommand {
	private:
		mutable
		NTSTATUS        m_status;
		PVOID           m_pData;
		ULONG           m_uSize;
	public:
		CDriverCommand(const PVOID pData, const ULONG uSize) : m_pData(pData), m_uSize(uSize), m_status(STATUS_SUCCESS)
		{}
	public:
		NTSTATUS    AppendRemovable(VOID) {

			UNICODE_STRING us_path = {0};

			m_status = this->_ModifyRemovable(&us_path, TRUE);

			if (NT_SUCCESS(m_status)) {
#if DBG
				DbgPrint ( "__fg_fs: [INFO] CDriverCommand()::appends the removable '%wZ';\n", &us_path );
#endif
			}
			return m_status;
		}

		NTSTATUS    ComparePath(VOID) {

			if (!this->IsValid())
				return (m_status = STATUS_INVALID_PARAMETER);

			UNICODE_STRING us_path = {0};

			__try {

				const WCHAR* lpszPath = ((PTFs_Command)m_pData)->wData;
				::RtlInitUnicodeString(
					&us_path, lpszPath
				);
#if DBG
				DbgPrint ( "__fg_fs: [INFO] CDriverCommand()::compare the path='%wZ';\n", &us_path );
#endif
				if (CFltCompare::IsIncludedToList(&us_path)) {
#if DBG
				DbgPrint ( "__fg_fs: [INFO] CDriverCommand()::compare the path='%wZ' is included;\n", &us_path );
#endif
				}
				else {
#if DBG
				DbgPrint ( "__fg_fs: [INFO] CDriverCommand()::compare the path='%wZ' is not matched;\n", &us_path );
#endif
				}

			} __except(EXCEPTION_EXECUTE_HANDLER) {
				m_status = GetExceptionCode();
			}
			return m_status;
		}

		NTSTATUS    DeleteRemovable(VOID) {

			UNICODE_STRING us_path = {0};

			m_status = this->_ModifyRemovable(&us_path, FALSE);

			if (NT_SUCCESS(m_status)) {
#if DBG
				DbgPrint ( "__fg_fs: [INFO] CDriverCommand()::removes the removable '%wZ';\n", &us_path );
#endif
			}
			return m_status;
		}

		BOOLEAN     IsValid(VOID)CONST {
			return ((m_pData != NULL) &&
				(m_uSize >= (FIELD_OFFSET(TFs_Command, eCmdType) + sizeof(eFsCommandType::eNone))));
		}

		eFsCommandType::_e
			Type(VOID)CONST {

			eFsCommandType::_e eType = eFsCommandType::eNone;
			if (!this->IsValid()) {
				m_status = STATUS_INVALID_PARAMETER;
				return eType;
			}

			__try {

				eType = ((PTFs_Command)m_pData)->eCmdType;

			} __except(EXCEPTION_EXECUTE_HANDLER) {
				m_status = GetExceptionCode();
			}

			return eType;
		}

		NTSTATUS    LastResult(VOID)CONST {
			return  m_status;
		}

	private:
		NTSTATUS   _ModifyRemovable(PUNICODE_STRING _pString, CONST BOOLEAN bAppend) {

			if (!this->IsValid() || NULL == _pString)
				return (m_status = STATUS_INVALID_PARAMETER);

			__try {

				const WCHAR* lpszPath = ((PTFs_Command)m_pData)->wData;

				::RtlInitUnicodeString(
					_pString, lpszPath  // this initialization does not require freeing memory;
				);

				if (bAppend) {
					m_status = CRemPersistent::Append(_pString);
				}
				else {
					m_status = CRemPersistent::Delete(_pString);
				}

			} __except(EXCEPTION_EXECUTE_HANDLER) {
				m_status = GetExceptionCode();
			}

			return m_status;
		}
	};
}}

/////////////////////////////////////////////////////////////////////////////

/*
 * DriverMessageHandler
 * 
 * This is called whenever a user mode application wishes to communicate
 * with this minifilter. See PFLT_MESSAGE_NOTIFY under FltCreateCommunicationPort
 *
 * http://msdn.microsoft.com/en-us/library/windows/hardware/ff541931(v=vs.85).aspx
 *
 * The specific API which is called from user mode that makes a blocking
 * call here is FilterSendMessage:
 *
 * http://msdn.microsoft.com/en-us/library/windows/hardware/ff541513(v=vs.85).aspx
 *
 * Do notice that there are several issues documented in the MSDN regarding
 * the buffer alignments.
 *
 * Returns the status of processing the message.
 */

NTSTATUS
DriverMessageHandler(
	__in PVOID       _pConnectionCookie,
	__in_bcount_opt( _nInputBufferSize ) PVOID _pInputBuffer,
	__in ULONG       _nInputBufferSize ,
	__out_bcount_part_opt(_nOutBufferSize,*_pReturnOutBufferSize) PVOID _pOutBuffer,
	__in ULONG       _nOutBufferSize   ,
	__out PULONG     _pReturnOutBufferSize
	)
{
	UNUSED(_pReturnOutBufferSize);

	eFsCommandType::_e
	             type_  = eFsCommandType::eNone;
	NTSTATUS  nt_status = STATUS_SUCCESS;

	PAGED_CODE();

	//
	//  the input and output buffers are raw user mode addresses;
	//  filter manager has already done a ProbedForRead (on InputBuffer) and
	//  ProbedForWrite (on OutputBuffer) which guarentees they are valid
	//  addresses based on the access (user mode vs. kernel mode);
	//  the minifilter does not need to do their own probe.
	//
	//  but filter manager is not doing any alignment checking on the pointers.
	//  (a) the minifilter must do this itself if they care (see below).
	//
	//  (b) the minifilter must continue to use a try/except around any access to
	//      these buffers.
	//

	UNUSED(_pConnectionCookie);
#if(0)
	handlers::details::CDriverCommand command_(_pInputBuffer, _nInputBufferSize, _pOutBuffer, _nOutBufferSize);
#else
	handlers::details::CDriverCommand command_(_pInputBuffer, _nInputBufferSize);
#endif
	if (!command_.IsValid())
		return command_.LastResult();
	
	type_ = command_.Type();

#if DBG
	/*DbgPrint(
		"__fg_fs: [INFO] DriverMessageHandler()::accepts type=%d;\n", type_
	);*/
#endif

	switch (type_)
	{
	case eFsCommandType::eGetEvents:
		{
			//
			// returns as many log records as can fit into the OutputBuffer
			//
			if ((_pOutBuffer == NULL) || (_nOutBufferSize == 0)) { nt_status = STATUS_INVALID_PARAMETER; break; }
			//
			// checks the given buffer is POINTER aligned;
			//
#if defined(_WIN64)
			if (IoIs32bitProcess( NULL )){
				//
				//  validates alignment for the 32bit process on a 64bit system;
				//
				if (!IS_ALIGNED(_pOutBuffer, sizeof(ULONG)))
				{
					nt_status = STATUS_DATATYPE_MISALIGNMENT;
					break;
				}
			} else {
#endif
				if (!IS_ALIGNED(_pOutBuffer, sizeof(PVOID)))
				{
					nt_status = STATUS_DATATYPE_MISALIGNMENT;
					break;
				}
#if defined(_WIN64)
			}
#endif
			//
			//  gets the log record;
			//
			nt_status = fg::filter::_imp::CEvtTracker::FillUserBuffer(
							(PTFs_Buffer)_pOutBuffer
						);
			if (nt_status == STATUS_NO_MORE_ENTRIES)
				nt_status =  STATUS_SUCCESS;
		} break;
#if (0)
	case eFsCommandType::eLogFilePath    : { nt_status = command_.ModifyLogFile();     } break;
	case eFsCommandType::eLogOptions     : { nt_status = command_.ModifyLogOptions();  } break;
	case eFsCommandType::eLogVerbose     : { nt_status = command_.ModifyLogVerbose();  } break;
#endif
	case eFsCommandType::eRemovableAppend: { nt_status = command_.AppendRemovable();   } break;
	case eFsCommandType::eRemovableDelete: { nt_status = command_.DeleteRemovable();   } break;
	case eFsCommandType::eUpdateFilter   : { CFltAdapter::Update(global::DriverData().pFilter); } break;
	case eFsCommandType::eComparePath    : { nt_status = command_.ComparePath(); } break;
	default:
		nt_status = STATUS_INVALID_PARAMETER;
	}

	return nt_status;
}