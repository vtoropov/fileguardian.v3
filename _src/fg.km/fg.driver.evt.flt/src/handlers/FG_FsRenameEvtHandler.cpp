/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Sep-2017 at 2:47:48a, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian mini-filter driver file object rename/move event handler interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsRenameEvtHandler.h"
#include "FG_FsCtxMans.h"
#include "FG_FsNotifyData.h"
#include "fg.fs.evt.cmp.iface.h"
#include "FG_FsFileObject.h"

using namespace handlers;
using namespace shared::km::fl_sys;

#include "fg.driver.proc.object.h"

using shared::km::proc_sys::CProcess;

/////////////////////////////////////////////////////////////////////////////

namespace handlers { namespace details {
	static const
	FLT_FILE_NAME_OPTIONS AnyRename_GetTargetNameOps = FLT_FILE_NAME_OPENED|FLT_FILE_NAME_QUERY_DEFAULT|FLT_FILE_NAME_REQUEST_FROM_CURRENT_PROVIDER;

	NTSTATUS
	AnyRename_GetTargetNameInfo(
	__inout PFLT_CALLBACK_DATA            _pFltData         ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects      ,
	__inout PFLT_FILE_NAME_INFORMATION*   _ppInfo
	)
	{
		NTSTATUS  nt_status  = STATUS_SUCCESS;
		PFILE_RENAME_INFORMATION   pRenameInfo = NULL;

		if (NULL == _pFltData ||
			NULL == _ppInfo)
			return (nt_status = STATUS_INVALID_PARAMETER);

		pRenameInfo = (PFILE_RENAME_INFORMATION)_pFltData->Iopb->Parameters.SetFileInformation.InfoBuffer;
		if (NULL == pRenameInfo)
			return (nt_status = STATUS_INVALID_PARAMETER);

		//
		// queries target file info
		//
		nt_status = FltGetDestinationFileNameInformation(
					_pFltObjects->Instance  ,
					_pFltObjects->FileObject, pRenameInfo->RootDirectory, pRenameInfo->FileName, pRenameInfo->FileNameLength, AnyRename_GetTargetNameOps, _ppInfo
				);

		return nt_status;
	};
}}

/////////////////////////////////////////////////////////////////////////////

FLT_POSTOP_CALLBACK_STATUS FLTAPI
CRenameEvtHandler::CFileInfoEvent::OnAfter (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__in        PVOID                     _pCompletionCtx ,
	__in        FLT_POST_OPERATION_FLAGS  _uFlags         ,
	__inout     BOOL&                     _bHandled
	)
{
	FLT_POSTOP_CALLBACK_STATUS
	                fl_status = FLT_POSTOP_FINISHED_PROCESSING;
	PTFileObjectCtx  pFileCtx = (PTFileObjectCtx)_pCompletionCtx;
	ULONG          ui_proc_id = 0; ui_proc_id;

	UNUSED(_uFlags);

	_bHandled = FALSE;

	if (FileRenameInformation != 
		_pFltData->Iopb->Parameters.SetFileInformation.FileInformationClass)
		return fl_status;

	_bHandled = TRUE;

	if (NULL == pFileCtx)
		return fl_status;

	ui_proc_id = CProcess::IdAsLong(_pFltData);

	notify::CNotifier::OnRename(
		pFileCtx->Operate.pSourceInfo, pFileCtx->Operate.pTargetInfo, _pFltObjects, ui_proc_id
	);

	if (pFileCtx->Operate.bIsTargetReplaced){
		notify::CNotifier::OnReplace(
			pFileCtx->Operate.pSourceInfo, pFileCtx->Operate.pTargetInfo, _pFltObjects, ui_proc_id
		);
	}

	FltReleaseContext(pFileCtx); pFileCtx = NULL; // we do not need the file context at this point;

	return fl_status;
}

FLT_PREOP_CALLBACK_STATUS FLTAPI
CRenameEvtHandler::CFileInfoEvent::OnBefore (
	__inout     PFLT_CALLBACK_DATA        _pFltData       ,
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
	__inout_opt PVOID*                    _ppCompletionCtx,
	__inout     BOOL&                     _bHandled
)
{
	FLT_PREOP_CALLBACK_STATUS
	          fl_status  = FLT_PREOP_SUCCESS_NO_CALLBACK;
	NTSTATUS  nt_status  = STATUS_SUCCESS;

	fl_sys::CFileInfoPtr
	                 pSrcFileInfo, pTgtFileInfo;
	PTFileObjectCtx pFileObjectCtx = NULL;

	_bHandled = FALSE;

	if (FileRenameInformation != 
		_pFltData->Iopb->Parameters.SetFileInformation.FileInformationClass)
		return fl_status;

	_bHandled = TRUE;

	//
	// queries source file info
	//
	nt_status = FltGetFileNameInformation(
					_pFltData, details::AnyRename_GetTargetNameOps, pSrcFileInfo
				);
	if (!NT_SUCCESS( nt_status )||
		!pSrcFileInfo.IsValid()) {
		return fl_status;
	}
	//
	// queries target file info
	//
	nt_status = details::AnyRename_GetTargetNameInfo(
					_pFltData, _pFltObjects, pTgtFileInfo
				);
	if (!NT_SUCCESS( nt_status )||
		!pTgtFileInfo.IsValid()) {
		return fl_status;
	}

	if (!CFltCompare::IsIncludedToList(&pSrcFileInfo.Ptr()->Name) &&
		!CFltCompare::IsIncludedToList(&pTgtFileInfo.Ptr()->Name)){  // filters out all events on objects, which we are not watching for;

		return fl_status;
	}

	nt_status = ctx::CCtxFileOperateMan::GetContext(
						_pFltObjects, TRUE, (PFLT_CONTEXT*)&pFileObjectCtx // the context is allocated or is gotten as existing one;
					);
	if (!NT_SUCCESS(nt_status)){
		//
		// checks for context existence and destroys it if necessary;
		//
		if (NULL != pFileObjectCtx){
			ctx::CCtxFileOperateMan::Destroy(pFileObjectCtx);
			FltReleaseContext(pFileObjectCtx);
		}
		return fl_status;
	}
	
	if (NULL != pFileObjectCtx->Operate.pSourceInfo){
		FltReleaseFileNameInformation(pFileObjectCtx->Operate.pSourceInfo); // actually should never occur
	}
	if (NULL != pFileObjectCtx->Operate.pTargetInfo){
		FltReleaseFileNameInformation(pFileObjectCtx->Operate.pTargetInfo); // actually should never occur
	}
	pFileObjectCtx->Operate.pSourceInfo = pSrcFileInfo.Detach();
	pFileObjectCtx->Operate.pTargetInfo = pTgtFileInfo.Detach();

	*_ppCompletionCtx = (PVOID)pFileObjectCtx;

	pFileObjectCtx->Operate.bIsTargetReplaced = fl_sys::CFile::IsExist(&pFileObjectCtx->Operate.pTargetInfo->Name);

	return (fl_status = FLT_PREOP_SUCCESS_WITH_CALLBACK);
}