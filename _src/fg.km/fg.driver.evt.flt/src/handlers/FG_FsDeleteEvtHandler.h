#ifndef _FGFSDELETEEVTHANDLER_H_B04DAD19_65C3_4820_8D1A_42BB5F61A737_INCLUDED
#define _FGFSDELETEEVTHANDLER_H_B04DAD19_65C3_4820_8D1A_42BB5F61A737_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Sep-2017 at 5:53:54p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver file object delete event interface declaration file.
*/
#include "FG_FsFilterIface.h"

namespace handlers {

	//
	// The common calling places for this class are:
	//
	// (1) PreCreate callback:
	//     This handler looks for FILE_DELETE_ON_CLOSE attribute of the target object;
	//     If found, a file stream context is created for passing to post create procedure;
	//     [a context is passed via _ppCompletionCtx argument]
	//     [**Important** context is not set to target object]
	//     In case of context creation the handler must return FLT_PREOP_SYNCHRONIZE;
	//
	// (2) PostCreate callback:
	//     This handler must be called only when a file object has FILE_DELETE_ON_CLOSE attribute;
	//     if so, coming stream context is assigned to target object and is marked as a candidate
	//     for deleting a target file;
	//     [**Important** the handler must release the context on success]
	//     Returned value must be FLT_POSTOP_FINISHED_PROCESSING;
	//
	// (3) PreSetInfo callback:
	//     The FileDispositionInformation data is checked, because it is another way for getting
	//     a file being deleted; after getting/creating stream context; the routine checks race
	//     condition; if no racing, assigning the context is made to _ppCompletionCtx and
	//     the routine must return FLT_PREOP_SYNCHRONIZE; otherwise FLT_PREOP_SUCCESS_NO_CALLBACK;
	//
	// (4) PostSetInfo callback:
	//     This routine is called in synchronous mode for pre-set-info; stream context is coming here
	//     via _pCompletionCtx argument and must have not NULL value;
	//     also, delete value field of the context is updated; finally, the routine decreases race 
	//     value and releases context;
	//     FLT_POSTOP_FINISHED_PROCESSING must be returned;
	//
	// (5) PreCleanUp callback:
	//     this routine gets basic information of a target file and update stream context with full
	//     file name that is being deleted; it must be done for each context that is attached to
	//     a stream; because only one stream is considered for real deletion;
	//     In case of successfull getting context for incoming target object [Iopb->TargetFileObject] 
	//     the routine must return FLT_PREOP_SYNCHRONIZE and assign a context to _ppCompletionCtx;
	//     otherwise, FLT_PREOP_SUCCESS_NO_CALLBACK;
	//
	// (6) PostCleanUp callback:
	//     This is the *main* routine for checking a stream as deleting a file;
	//     The routine is a source for sending appropriate notification about deleting a file;
	//     FLT_POSTOP_FINISHED_PROCESSING must be returned;
	//
	class CDeleteEvtHandler {
	public:
		class CCreateEvent {

		public:
			/*
			Routine Description:
				This routine is the post-operation completion routine for
				IRP_MJ_CREATE in this miniFilter.
				The post-create callback will only be called when this is a create with
				FILE_DELETE_ON_CLOSE, meaning we have to flag it as a deletion candidate.
			*/
			static
			FLT_POSTOP_CALLBACK_STATUS FLTAPI
			OnAfter (
				__inout     PFLT_CALLBACK_DATA        _pFltData       ,
				__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__in        PVOID                     _pCompletionCtx ,
				__in        FLT_POST_OPERATION_FLAGS  _uFlags         ,
				__inout     BOOL&                     _bHandled
				);

			/*
			Routine Description:
				This routine is the pre-create completion routine for
				IRP_MJ_CREATE event.
				In the pre-create phase we're concerned with creates which have
				FILE_DELETE_ON_CLOSE attribute set, and in those cases we want to flag
				this stream as a candidate for being deleted.
			Return Value:
				FLT_PREOP_SYNCHRONIZE - we never do any sort of asynchronous processing
										here, and we synchronize post-operation.

				FLT_PREOP_SUCCESS_NO_CALLBACK - if not FileDispositionInformation or we
												can't set a stream context.
			*/
			static
			FLT_PREOP_CALLBACK_STATUS FLTAPI
			OnBefore (
				__inout     PFLT_CALLBACK_DATA        _pFltData       ,
				__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__out       PVOID*                    _ppCompletionCtx,
				__inout     BOOL&                     _bHandled
				);
		};

		class CCleanUpEvent {

		public:
			/*
			*/
			static
			FLT_POSTOP_CALLBACK_STATUS FLTAPI
			OnAfter (
				__inout     PFLT_CALLBACK_DATA        _pFltData       ,
				__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__in        PVOID                     _pCompletionCtx ,
				__in        FLT_POST_OPERATION_FLAGS  _uFlags         ,
				__inout     BOOL&                     _bHandled
				);

			static
			FLT_PREOP_CALLBACK_STATUS FLTAPI
			OnBefore (
				__inout     PFLT_CALLBACK_DATA        _pFltData       ,
				__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__out       PVOID*                    _ppCompletionCtx,
				__inout     BOOL&                     _bHandled
				);

		private:
			/*
			Routine Description:
				This routine does the processing after it is verified, in the post-cleanup
				callback, that a file or stream were deleted. It sorts out whether it's a
				file or a stream delete, whether this is in a transacted context or not,
				and issues the appropriate notifications.
			Return Value:
				STATUS_SUCCESS.
			*/
			static
			NTSTATUS
			Finalize (
				__inout     PFLT_CALLBACK_DATA        _pFltData       ,
				__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__in        PFLT_CONTEXT              _pStreamCtx
				);
			};

		class CFileInfoEvent {

		public:
			/*
			Routine Description:
				This routine is the post-operation completion routine for
				IRP_MJ_SET_INFORMATION major function.

				This postop callback updates the deletion disposition state
				of the stream in the stream context. This callback will only be reached
				when there's a single change to deletion disposition in flight for the
				stream or when this was the first of many racing ops to hit the preop.

				In the latter case, the race is already detected and adequately flagged
				in the other preops, so we're safe just decrementing NumOps, because the
				other operations will never reach postop and NumOps won't ever be
				decremented for them, guaranteeing that NumOps will stay nonzero forever,
				effectively flagging the race.
			Return Value:
				FLT_POSTOP_FINISHED_PROCESSING - we never do any sort of asynchronous
												 processing here.
			*/
			static
			FLT_POSTOP_CALLBACK_STATUS FLTAPI
			OnAfter (
				__inout     PFLT_CALLBACK_DATA        _pFltData       ,
				__in        PCFLT_RELATED_OBJECTS     _pFltObjects    ,
				__in        PVOID                     _pCompletionCtx ,
				__in        FLT_POST_OPERATION_FLAGS  _uFlags         ,
				__inout     BOOL&                     _bHandled
				);

			/*
			Routine Description:
				This routine is the pre-operation completion routine for
				IRP_MJ_SET_INFORMATION in this miniFilter.

				The pre-setinfo callback is important because setting
				FileDispositionInformation is another way of putting the file in a
				delete-pending state.

				Since the delete disposition is a reversible condition, we have to
				make sure to do the right thing when multiple operations are racing:
				we won't be able to tell the final outcome of the delete
				disposition state of the stream, so everytime a race like that happens,
				we assume this stream as a permanent deletion candidate, so it will be
				checked for deletion in the post-cleanup callback.
			Return Value:
				FLT_PREOP_SYNCHRONIZE         - we never do any sort of asynchronous processing
												here, and we synchronize postop.

				FLT_PREOP_SUCCESS_NO_CALLBACK - if not FileDispositionInformation or we
												cannot set a streamcontext.
			*/
			static
			FLT_PREOP_CALLBACK_STATUS FLTAPI
			OnBefore (
				__inout     PFLT_CALLBACK_DATA        _pFltData         ,
				__in        PCFLT_RELATED_OBJECTS     _pFltObjects      ,
				__inout_opt PVOID*                    _ppCompletionCtx  ,
				__inout     BOOL&                     _bHandled
			);
		};

		/*
			This routine is the transaction notification callback for this minifilter.
			It is called when a transaction is committed or rolled back;
			This callback is called for enlisted transaction only;
		*/
		static
		NTSTATUS
		OnTransactionEnd (
			__in        PCFLT_RELATED_OBJECTS     _pFltObjects      ,
			__in        PFLT_CONTEXT              _pTransCtx        ,
			__in        BOOLEAN                   _bCommitted       ,
			__inout     BOOL&                     _bHandled
		);
	};
}

#endif/*_FGFSDELETEEVTHANDLER_H_B04DAD19_65C3_4820_8D1A_42BB5F61A737_INCLUDED*/