/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Jun-2017 at 12:35:15p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian file system filter driver debug routines implementation file.
*/
#include "StdAfx.h"
#include "FG_FsDebugObject.h"

namespace __completely_and_absolutely_deprecated {

NTSTATUS DebugPrintDeviceObj(
	__in PDEVICE_OBJECT _pDevice
    )
{
	NTSTATUS       status = STATUS_SUCCESS;
	UNICODE_STRING drive_;

	if (NULL == _pDevice)
	{
		DbgPrint(
			"__fg_drv: device object pointer is invalid."
			);
		return (RPC_NT_NULL_REF_POINTER);
	}
	else
	{
		status = IoVolumeDeviceToDosName(
				(PVOID)_pDevice,
				&drive_
			);
		if (STATUS_SUCCESS == status){
			DbgPrint(
				"__fg_drv: device_obj::name: %wZ\n", &drive_
				);
			ExFreePool(drive_.Buffer);
		}
		else
			DbgPrint(
				"__fg_drv: cannot retrieve device name: error=%X\n", status
				);
	}

	return (STATUS_SUCCESS);
}

NTSTATUS DebugPrintFileInfo(
	__in PFILE_RENAME_INFORMATION _pInfo
    )
{
	UNICODE_STRING u_str_;

	if (NULL == _pInfo)
	{
		DbgPrint(
			"__fg_drv: file rename info pointer is invalid."
			);
		return (RPC_NT_NULL_REF_POINTER);
	}
	else
	{
		u_str_.Buffer        = _pInfo->FileName;
		u_str_.Length        = (USHORT)_pInfo->FileNameLength;
		u_str_.MaximumLength = (USHORT)_pInfo->FileNameLength;

		if (_pInfo->RootDirectory == NULL)
			DbgPrint(
				"__fg_drv: file rename info::file_name=%wZ;rootdir=NULL\n",
				&u_str_
				);
		else
			DbgPrint(
				"__fg_drv: file rename info::file_name=%wZ;rootdir=Object\n",
				&u_str_
				);
	}

	return (STATUS_SUCCESS);
}

NTSTATUS DebugPrintFileObj(
    __in PFILE_OBJECT _pFile
    )
{
	if (NULL == _pFile)
	{
		DbgPrint(
			"__fg_drv: file object pointer is invalid."
			);
		return (RPC_NT_NULL_REF_POINTER);
	}
	else
	{
		DbgPrint(
			"__fg_drv: file object::file_name=%wZ\n",
			&(_pFile->FileName)
			);
		DbgPrint(
			"__fg_drv: file object::RelatedFileObject::\n"
			);
		DebugPrintFileObj(_pFile->RelatedFileObject);
		DbgPrint(
			"__fg_drv: file object::DeviceObject::\n"
			);
		DebugPrintDeviceObj(_pFile->DeviceObject);
	}

	return (STATUS_SUCCESS);
}

NTSTATUS DebugPrintNameInfo(
	__in PFLT_FILE_NAME_INFORMATION _pInfo
	)
{
	if (NULL == _pInfo)
	{
		DbgPrint(
			"__fg_drv: name object pointer is invalid."
			);
		return (RPC_NT_NULL_REF_POINTER);
	}
	else
	{
		DbgPrint(
			"__fg_drv: file object::full_name=%wZ\n",
			&(_pInfo->Name)
			);
	}

	return (STATUS_SUCCESS);
}

NTSTATUS DebugPrintUnicodeStringAsHex(
	__in PUNICODE_STRING _pString
	)
{
	USHORT i_ = 0;
	if (NULL == _pString)
	{
		DbgPrint(
			"__fg_drv: [PRINT] string object pointer is invalid."
			);
		return (RPC_NT_NULL_REF_POINTER);
	}
	for (i_ = 0; i_ < _pString->Length; i_+= 2)
	{}
	return STATUS_SUCCESS;
}

}