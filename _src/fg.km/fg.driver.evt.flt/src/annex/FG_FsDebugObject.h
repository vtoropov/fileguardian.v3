#ifndef _FGFSDRVDEBUGOBJECT_H_98895EB4_B6C7_4b97_B781_C6FC0DA06AA2_INCLUDED
#define _FGFSDRVDEBUGOBJECT_H_98895EB4_B6C7_4b97_B781_C6FC0DA06AA2_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Jun-2017 at 12:26:13p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian file system filter driver debug routines declafration file.
*/
namespace __completely_and_absolutely_deprecated {

NTSTATUS DebugPrintDeviceObj(
	__in PDEVICE_OBJECT _pDevice
    );

NTSTATUS DebugPrintFileInfo(
	__in PFILE_RENAME_INFORMATION _pInfo
    );

NTSTATUS DebugPrintFileObj(
    __in PFILE_OBJECT _pFile
    );

NTSTATUS DebugPrintNameInfo(
	__in PFLT_FILE_NAME_INFORMATION _pInfo
	);

NTSTATUS DebugPrintUnicodeStringAsHex(
	__in PUNICODE_STRING _pString
	);

}

#endif/*_FGFSDRVDEBUGOBJECT_H_98895EB4_B6C7_4b97_B781_C6FC0DA06AA2_INCLUDED*/