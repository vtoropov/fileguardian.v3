#ifndef _FGFSSDKEXTNEEDTOBEREMOVEDINFUTURE_H_ABB51A5F_3B73_4ccf_A8E2_CE24237F74F0_INCLUDED
#define _FGFSSDKEXTNEEDTOBEREMOVEDINFUTURE_H_ABB51A5F_3B73_4ccf_A8E2_CE24237F74F0_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Aug-2017 at 2:25:29p, UTC+7, Phuket, Rawai, Tuesday;
	This is SDK extension file, it contains all new definitions that do not exist in Sdk Win7 and it
	needs to be removed in the future version of the File Guardian mini-filter driver (porting to VS2015).
*/

#ifndef FLT_FSTYPE_REFS                        // it very looks like this type is not defined for Win7 SDK:
#define FLT_FSTYPE_REFS  (FLT_FSTYPE_GPFS + 4) // Microsoft ReFS file system. File system namespace: \FileSystem\refs.
#endif                                         // https://docs.microsoft.com/en-us/windows-server/storage/refs/refs-overview

#ifndef FILE_INVALID_FILE_ID
#define FILE_INVALID_FILE_ID  ((LONGLONG)-1LL) // winnt
#endif

/*
	MSVC++ 14.1 _MSC_VER == 1912 (Visual Studio 2017)
	MSVC++ 14.1 _MSC_VER == 1911 (Visual Studio 2017)
	MSVC++ 14.1 _MSC_VER == 1910 (Visual Studio 2017)
	MSVC++ 14.0 _MSC_VER == 1900 (Visual Studio 2015)
	MSVC++ 12.0 _MSC_VER == 1800 (Visual Studio 2013)
	MSVC++ 11.0 _MSC_VER == 1700 (Visual Studio 2012)
	MSVC++ 10.0 _MSC_VER == 1600 (Visual Studio 2010)
	MSVC++  9.0 _MSC_FULL_VER == 150030729 (Visual Studio 2008, SP1)
	MSVC++  9.0 _MSC_VER == 1500 (Visual Studio 2008)
	MSVC++  8.0 _MSC_VER == 1400 (Visual Studio 2005)
	MSVC++  7.1 _MSC_VER == 1310 (Visual Studio 2003)
	MSVC++  7.0 _MSC_VER == 1300
	MSVC++  6.0 _MSC_VER == 1200
	MSVC++  5.0 _MSC_VER == 1100
*/

#if defined(_MSC_VER) && (_MSC_VER < 1600)

typedef struct _FILE_ID_128 {                  // winnt
    UCHAR Identifier[16];                      // winnt
} FILE_ID_128, *PFILE_ID_128;                  // winnt

typedef struct _FILE_ID_INFORMATION {
    ULONGLONG VolumeSerialNumber;
    FILE_ID_128 FileId;
} FILE_ID_INFORMATION, *PFILE_ID_INFORMATION;

#endif

#ifndef FSCTL_OFFLOAD_WRITE
#define FSCTL_OFFLOAD_WRITE                 CTL_CODE(FILE_DEVICE_FILE_SYSTEM, 154, METHOD_BUFFERED, FILE_WRITE_ACCESS)
#endif
//
// Define the file information class values
//
// WARNING:  The order of the following values are assumed by the I/O system.
//           Any changes made here should be reflected there as well.
//
namespace sdk_ext{

typedef enum _FILE_INFORMATION_CLASS {
    FileDirectoryInformation         = 1,
    FileFullDirectoryInformation,   // 2
    FileBothDirectoryInformation,   // 3
    FileBasicInformation,           // 4
    FileStandardInformation,        // 5
    FileInternalInformation,        // 6
    FileEaInformation,              // 7
    FileAccessInformation,          // 8
    FileNameInformation,            // 9
    FileRenameInformation,          // 10
    FileLinkInformation,            // 11
    FileNamesInformation,           // 12
    FileDispositionInformation,     // 13
    FilePositionInformation,        // 14
    FileFullEaInformation,          // 15
    FileModeInformation,            // 16
    FileAlignmentInformation,       // 17
    FileAllInformation,             // 18
    FileAllocationInformation,      // 19
    FileEndOfFileInformation,       // 20
    FileAlternateNameInformation,   // 21
    FileStreamInformation,          // 22
    FilePipeInformation,            // 23
    FilePipeLocalInformation,       // 24
    FilePipeRemoteInformation,      // 25
    FileMailslotQueryInformation,   // 26
    FileMailslotSetInformation,     // 27
    FileCompressionInformation,     // 28
    FileObjectIdInformation,        // 29
    FileCompletionInformation,      // 30
    FileMoveClusterInformation,     // 31
    FileQuotaInformation,           // 32
    FileReparsePointInformation,    // 33
    FileNetworkOpenInformation,     // 34
    FileAttributeTagInformation,    // 35
    FileTrackingInformation,        // 36
    FileIdBothDirectoryInformation, // 37
    FileIdFullDirectoryInformation, // 38
    FileValidDataLengthInformation, // 39
    FileShortNameInformation,       // 40
    FileIoCompletionNotificationInformation, // 41
    FileIoStatusBlockRangeInformation,       // 42
    FileIoPriorityHintInformation,           // 43
    FileSfioReserveInformation,              // 44
    FileSfioVolumeInformation,               // 45
    FileHardLinkInformation,                 // 46
    FileProcessIdsUsingFileInformation,      // 47
    FileNormalizedNameInformation,           // 48
    FileNetworkPhysicalNameInformation,      // 49
    FileIdGlobalTxDirectoryInformation,      // 50
    FileIsRemoteDeviceInformation,           // 51
    FileUnusedInformation,                   // 52
    FileNumaNodeInformation,                 // 53
    FileStandardLinkInformation,             // 54
    FileRemoteProtocolInformation,           // 55

        //
        //  These are special versions of these operations (defined earlier)
        //  which can be used by kernel mode drivers only to bypass security
        //  access checks for Rename and HardLink operations.  These operations
        //  are only recognized by the IOManager, a file system should never
        //  receive these.
        //

    FileRenameInformationBypassAccessCheck,  // 56
    FileLinkInformationBypassAccessCheck,    // 57

        //
        // End of special information classes reserved for IOManager.
        //

    FileVolumeNameInformation,               // 58
    FileIdInformation,                       // 59
    FileIdExtdDirectoryInformation,          // 60
    FileReplaceCompletionInformation,        // 61
    FileHardLinkFullIdInformation,           // 62
    FileIdExtdBothDirectoryInformation,      // 63
    FileDispositionInformationEx,            // 64
    FileRenameInformationEx,                 // 65
    FileRenameInformationExBypassAccessCheck, // 66
    FileDesiredStorageClassInformation,      // 67
    FileStatInformation,                     // 68
    FileMaximumInformation
} FILE_INFORMATION_CLASS, *PFILE_INFORMATION_CLASS;

}
#endif/*_FGFSSDKEXTNEEDTOBEREMOVEDINFUTURE_H_ABB51A5F_3B73_4ccf_A8E2_CE24237F74F0_INCLUDED*/