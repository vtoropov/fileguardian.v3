//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ resource include file.
// Used by FG_FsFilter_Resource.rc
//
// Created by Tech_dog (ebontrop@gmail.com) on 7-Apr-2018 at 4:59:27p, UTC+7, Novosibirsk, Rodniki, Saturday; 
// This is File Guardian mini-filter driver resource identifier declaration file.
//
#define IDR_RT_MANIFEST                                     1  // for compatibility; actually, a driver cannot use either a desktop theme nor UAC access;
#define IDR_RT_MAINICON                                     3
