/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Jun-2017 at 11:20:06p, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian mini-filter driver module implementation file.
*/
#include "StdAfx.h"
#include "FG_FsFilterModule.h"
#include "FG_FsFilterIface.h"
#include "fg.fs.evt.cmp.iface.h"
#include "FG_FsUserMsgHandler.h"
#include "fg.fs.filter.event.rec.proc.h"
#include "fg.fs.filter.event.tracker.h"
//
// callbacks are included;
//
#include "FG_FsTransactCallback.h"
#include "FG_FsSetInfoCallback.h"
#include "FG_FsCleanupCallback.h"
#include "FG_FsCreateCallback.h"
#include "FG_FsCloseCallback.h"
#include "FG_FsWriteCallback.h"
#include "FG_FsFileCtrlCallback.h"
#include "FG_FsFlushDataCallback.h"

#define FLTFL_OPERATION_REGISTRATION_NONE_OPTS  0
#define IRP_MJ_FILE_CTRL IRP_MJ_FILE_SYSTEM_CONTROL

#include "FG_FsCtxIface.h"
#include "FG_FsCtxMans.h"
#include "fg.driver.volume.object.h"

using namespace ctx;
using namespace fl_sys;

using fg::filter::_imp::CRecManager;
using shared::km::fl_sys::CFltAdapter;

/////////////////////////////////////////////////////////////////////////////

EXTERN_C NTSTATUS DriverEntry(__in PDRIVER_OBJECT DriverObject ,
                              __in PUNICODE_STRING RegistryPath)
{
	UNUSED(DriverObject);
	UNUSED(RegistryPath);

#if DBG
	DbgPrint( "__fg_fs: [INFO] DriverEntry()::entering to driver entry...;\n" );
#endif

	const FLT_OPERATION_REGISTRATION evtCallbacks[] = {
		{ IRP_MJ_CREATE          , FLTFL_OPERATION_REGISTRATION_NONE_OPTS     , PreCreateCallback     , PostCreateCallback      },
#if defined(_USE_COMPLEX_MODIFY)
		{ IRP_MJ_CLOSE           , FLTFL_OPERATION_REGISTRATION_NONE_OPTS     , PreCloseCallback      , NULL                    },
		{ IRP_MJ_WRITE           , FLTFL_OPERATION_REGISTRATION_NONE_OPTS     , PreWriteCallback      , NULL                    },
#endif
		{ IRP_MJ_SET_INFORMATION , FLTFL_OPERATION_REGISTRATION_SKIP_PAGING_IO, PreSetInfoCallback    , PostSetInfoCallback     },

#if defined(_USE_COMPLEX_MODIFY)
		{ IRP_MJ_FILE_CTRL       , FLTFL_OPERATION_REGISTRATION_NONE_OPTS     , PreFileCtrlCallback   , NULL                    },
#endif
		{ IRP_MJ_FLUSH_BUFFERS   , FLTFL_OPERATION_REGISTRATION_SKIP_PAGING_IO, NULL                  , PostFlushDataCallback   },

		{ IRP_MJ_CLEANUP         , FLTFL_OPERATION_REGISTRATION_NONE_OPTS     , PreCleanupCallback    , PostCleanupCallback     },
		{ IRP_MJ_OPERATION_END   }
	};

	const FLT_REGISTRATION registry_ = {
		sizeof(registry_)        , // size of structure
		FLT_REGISTRATION_VERSION , // revision level of the structure
		0                        , // bitmask of minifilter registration flags
		GetRegistrationInfo()    , // context registration info
		evtCallbacks             , // operation callbacks
		DriverUnload             , // minifilter unload routine
#if (0)
		DriverInstanceSetup      , // instance setup callback routine; this routine is not used; moreover, it can produce bluscreen; must be reviewed;
#else
		NULL                     , // instance setup callback routine
#endif
		NULL                     , // instance query teardown callback routine
		NULL                     , // instance start teardown callback routine
#if (0)
		DriverTeardownComplete   , // instance complete teardown callback routine
#else
		NULL                     ,
#endif
		NULL                     , // generate file name callback routine
		NULL                     , // normalize name component callback routine
		NULL                     , // normalize context cleanup callback routine
#ifdef FLT_MGR_LONGHORN
#if (0)
		TxNotificationCallback   , // transition notification callback
#else
		NULL                     ,
#endif
		NULL                     , // normalize name component extended callback
#endif
	};

#if DBG
#ifdef FLT_MGR_LONGHORN
	DbgPrint( "__fg_fs: [INFO] DriverEntry()::TxF support is enabled;\n" );
#else
	DbgPrint( "__fg_fs: [INFO] DriverEntry()::TxF support is disabled;\n" );
#endif
#endif

	PSECURITY_DESCRIPTOR sd           = NULL;
	UNICODE_STRING       drv_port     = {0};
	OBJECT_ATTRIBUTES    obj_a        = {0};
	const LONG           nMaxConnects =  1 ; // accept one connection only;
	NTSTATUS             nt_status    = STATUS_SUCCESS;

	TDriverData& drv_data  = global::DriverData();
	drv_data.pDriverObject = DriverObject;

	__try {
		//
		// initializes global data structures;
		//
#if (0)
		nt_status = CLogger::Initialize(NULL); // TODO: it leads to system crash; it's necessary to research the issue;
#endif
		RtlInitUnicodeString(&drv_port, DriverPortName);

		CRecManager::CreateStorage();

		//
		// registers with filter manager;
		//
#if DBG
		DbgPrint( "__fg_fs: [INFO] DriverEntry()::registering log:status=%X;\n", nt_status );
		DbgPrint( "__fg_fs: [INFO] DriverEntry()::registering filter...;\n" );
#endif
		nt_status = FltRegisterFilter(
				DriverObject, &registry_, &drv_data.pFilter
			);

		if (!NT_SUCCESS(nt_status)){
#if DBG
		DbgPrint( "__fg_fs: [ERR] DriverEntry()::registering filter failed with code=%X;\n", nt_status );
#endif
			__leave;
		}
		else
		CFltAdapter::Initialize(drv_data.pFilter);
		//
		// secures the port, so administrators or local system (services) can access it;
		//
#if DBG
		DbgPrint( "__fg_fs: [INFO] DriverEntry()::creating security descriptor...;\n" );
#endif
		nt_status = FltBuildDefaultSecurityDescriptor(&sd, FLT_PORT_ALL_ACCESS);

		if (!NT_SUCCESS(nt_status)){
#if DBG
		DbgPrint( "__fg_fs: [ERR] DriverEntry()::creating security descriptor failed with code=%X;\n", nt_status );
#endif
			__leave;
		}

		InitializeObjectAttributes(
			&obj_a,
			&drv_port,
			OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
			NULL,
			sd
		);
#if DBG
		DbgPrint( "__fg_fs: [INFO] DriverEntry()::creating communication port...;\n" );
#endif
		nt_status = FltCreateCommunicationPort(
					 drv_data.pFilter, &drv_data.pServerPort, &obj_a, NULL, DriverPortConnect, DriverPortDisconnect, DriverMessageHandler, nMaxConnects
				);

		FltFreeSecurityDescriptor(sd); sd = NULL;

		if (!NT_SUCCESS(nt_status)){
#if DBG
			DbgPrint( "__fg_fs: [ERR] DriverEntry()::creating communication port failed with code=%X;\n", nt_status );
#endif
			__leave;
		}

#if DBG
		DbgPrint( "__fg_fs: [INFO] DriverEntry()::starting the filtering...;\n" );
#endif
		nt_status = FltStartFiltering(drv_data.pFilter);

		if (!NT_SUCCESS(nt_status)){
#if DBG
		DbgPrint( "__fg_fs: [ERR] DriverEntry()::start filtering failed with code=%X;\n", nt_status );
#endif
		} else {
#if DBG
		DbgPrint( "__fg_fs: [INFO] DriverEntry()::waiting for incomming connection(s)...;\n" );
#endif
		}
	}
	__finally
	{
		if (!NT_SUCCESS(nt_status))
		{
			if (NULL != drv_data.pServerPort){
				FltCloseCommunicationPort(drv_data.pServerPort);
				drv_data.pServerPort = NULL;
			}
			if (NULL != drv_data.pFilter) {
				FltUnregisterFilter(drv_data.pFilter);
				drv_data.pFilter = NULL;
			}
			CRecManager::DestroyStorage();
			CFltAdapter::Terminate();
#if (0)
			CLogger::Terminate(NULL); // TODO: do not call a logger; it crashes the system and is needed further researching the problem;
#endif
		}
	}

#if DBG
	DbgPrint( "__fg_fs: [INFO] DriverEntry()::exiting driver entry;\n" );
#endif

	return nt_status;
}

//////////////////////////////////////////////////////////////////////////////

#if (0)

EXTERN_C static NTSTATUS FLTAPI DriverInstanceSetup (
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects,
	__in        FLT_INSTANCE_SETUP_FLAGS  _uFlags     ,
	__in        DEVICE_TYPE               _uDeviceType,
	__in        FLT_FILESYSTEM_TYPE       _uFileSysType
	)
{
	NTSTATUS   nt_status = STATUS_FLT_DO_NOT_ATTACH;
	CInstanceCtxPtr pCtx = NULL;

	UNUSED(_uFlags);
	UNUSED(_uFileSysType);

	PAGED_CODE();

	switch (_uDeviceType)
	{
	case FILE_DEVICE_CD_ROM_FILE_SYSTEM:
	case FILE_DEVICE_DISK_FILE_SYSTEM  :
		{
			CVolume::CProperties props_;
			nt_status = props_.Enumerate(
								_pFltObjects->Volume
							);
			if (NT_SUCCESS(nt_status) &&
				props_.IsRemovable()) {

					nt_status = CCtxInstanceMan::GetContext(
									_pFltObjects, TRUE, pCtx
									);
					if (NT_SUCCESS(nt_status)){
#if DBG
	DbgPrint(
		"__fg_fs: [INFO] DriverInstanceSetup()::attached device is connected for monitoring;\n"
		);
#endif
						nt_status = STATUS_SUCCESS; // the device volume can be attached;
					}
			}
		} break;
	default:
		nt_status = STATUS_FLT_DO_NOT_ATTACH;
#if DBG
	DbgPrint(
		"__fg_fs: [INFO] DriverInstanceSetup()::attached device has type %d;\n", _uDeviceType
		);
#endif
	}

	return   nt_status;
}

EXTERN_C static VOID FLTAPI DriverTeardownComplete (
	__in        PCFLT_RELATED_OBJECTS     _pFltObjects,
	__in        ULONG                     _uFlags
	)
{
	CInstanceCtxPtr pCtx = NULL;

	UNUSED(_uFlags);

	PAGED_CODE();

	NTSTATUS nt_status = CCtxInstanceMan::GetContext(_pFltObjects, FALSE, pCtx);
	if (NT_SUCCESS(nt_status)){
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] DriverTeardownComplete()::exiting tear down routine;\n"
			);
#endif
	}
	else {
#if DBG
		DbgPrint(
			"__fg_fs: [INFO] DriverTeardownComplete()::cannot release instance context; err code=%X;\n", nt_status
			);
#endif
	}
}
#endif

EXTERN_C static NTSTATUS FLTAPI DriverUnload(
	__in        FLT_FILTER_UNLOAD_FLAGS   _uFlags
)
{
	UNUSED(_uFlags);

	PAGED_CODE();

#if DBG
	DbgPrint( "__fg_fs: [INFO] DriverUnload()::entering filter unload...;\n" );
#endif

	TDriverData& drv_data  = global::DriverData();

	if (NULL != drv_data.pClientPort){

#if DBG
		DbgPrint( "__fg_fs: [INFO] DriverUnload()::disconnecting client(s)...;\n" );
#endif

		FltCloseClientPort(
				 drv_data.pFilter, &drv_data.pClientPort
			);
		drv_data.pClientPort = NULL;
	}

#if DBG
	DbgPrint( "__fg_fs: [INFO] DriverUnload()::closing communication port...;\n" );
#endif

	FltCloseCommunicationPort(drv_data.pServerPort);
	drv_data.pServerPort = NULL;

#if DBG
	DbgPrint( "__fg_fs: [INFO] DriverUnload()::unregistering filter...;\n" );
#endif

	FltUnregisterFilter(drv_data.pFilter); drv_data.pFilter = NULL;
#if DBG
	DbgPrint( "__fg_fs: [INFO] DriverUnload()::destroying storage...;\n" );
#endif
	CRecManager::DestroyStorage();
#if DBG
	DbgPrint( "__fg_fs: [INFO] DriverUnload()::destroying filter...;\n" );
#endif
	CFltAdapter::Terminate();

#if DBG
	DbgPrint( "__fg_fs: [INFO] DriverUnload()::exiting filter unload...;\n" );
#endif
	if (drv_data.pDriverObject) {
		if (drv_data.pDriverObject->DeviceObject) {
			IoDeleteDevice( drv_data.pDriverObject->DeviceObject );
#if DBG
	DbgPrint( "__fg_fs: [INFO] DriverUnload()::device object has been deleted...;\n" );
#endif
		}
	}
	NTSTATUS nt_status = STATUS_SUCCESS;
#if (0)
	nt_status = CLogger::Terminate(NULL); // TODO: logger does not wori=k as expected; it seems an implementation has poor design; 
#endif
	return   nt_status;
}