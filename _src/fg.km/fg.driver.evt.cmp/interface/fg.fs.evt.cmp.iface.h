#ifndef _FGFSEVTCMPDEFS_H_E18E2CEE_F7A5_4FC0_AB41_32F3F32CD0C5_INCLUDED
#define _FGFSEVTCMPDEFS_H_E18E2CEE_F7A5_4FC0_AB41_32F3F32CD0C5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Jun-2018 at 8:26:52a, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian minifilter driver folder/file filter interface declaration file. 
*/

#ifndef _MAX_DRIVE
#define _MAX_DRIVE    3
#endif

#ifndef _MAX_DRIVE_ALIGNED
#define _MAX_DRIVE_ALIGNED    4
#endif

#ifndef _MAX_DIR
#define _MAX_DIR    256
#endif

#ifndef _MAX_FNAME
#define _MAX_FNAME  256
#endif

#ifndef _MAX_EXT
#define _MAX_EXT    256
#endif

#ifndef _LONG_PATH
#define _LONG_PATH (_MAX_DIR + _MAX_FNAME + _MAX_DRIVE_ALIGNED + _MAX_EXT)
#endif

namespace shared { namespace km { namespace fl_sys {

	class CFltCompare {
	public:
		//
		// determines the path/object name provided is included to the filter list or not;
		//
		static
		BOOL
		IsIncludedToList(
			__in PUNICODE_STRING   _pPath         // the path/object name to check
		);

		/*
			FsRtlIsNameInExpression() works fine with statically defined patterns, but doesn't
			work (does not match anything) with dynamically allocated patterns (it's a miracle!).
			The hand written function below is for matching a file object name (i.e. path) to 
			watching folder path, which is defined dynamically;
			Args:
			_pFilePath   � file object path;
			_pFolder     � folder path (from include or exclude list);

			The function expects watching folder path in low case (the file object path is converted to
			low case on the fly), returns true if file object path starts with watching folder one; 
		*/
		static
		BOOL
		MatchFileWithFolder(
			__in PUNICODE_STRING   _pFilePath,    // the string where the finding occurs
			__in PUNICODE_STRING   _pFolder       // the string that is being searched
		);
		/*
			This routine uses RtlCompareMemory for getting comparison result;
		*/
		static
		BOOL
		MatchFileWithFolder_Ex(
			__in PUNICODE_STRING   _pFilePath,    // the string where the finding occurs
			__in PUNICODE_STRING   _pFolder       // the string that is being searched
		);
		static
		BOOL
		MatchFileWithFolderList(
			__in PUNICODE_STRING   _pFilePath,    // the string where the finding occurs
			__in LIST_HEAD&        _list          // list reference to deal with
		);
	};

	class CFltAdapter {
	public:
		static VOID   Initialize(PFLT_FILTER _p_filter);
		static VOID   Update(PFLT_FILTER _p_filter);
		static VOID   Terminate(VOID);
	};
}}}

#endif/*_FGFSEVTCMPDEFS_H_E18E2CEE_F7A5_4FC0_AB41_32F3F32CD0C5_INCLUDED*/