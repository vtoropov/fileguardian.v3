#ifndef _FGFSFILTERREMOVABLE_H_267DBC25_6944_401f_A0FC_E877FB7AA63B_INCLUDED
#define _FGFSFILTERREMOVABLE_H_267DBC25_6944_401f_A0FC_E877FB7AA63B_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Feb-2018 at 11:43:14p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver removable storage data filter interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 17-Jun-2018 at 10:17:34a, UTC+7, Phuket, Rawai, Sunday;
*/
#include "fg.fs.evt.cmp.iface.h"

namespace shared { namespace km { namespace fl_sys {

	typedef struct _t_filter_data_rem
	{
		LIST_HEAD       IncludeDirList;       // list of folder names that are being watched;
		KSPIN_LOCK      pListLock;            // lock object of folder lists
		LIST_ALLOC      ItemAllocator;        // lookaside list used for allocating list items;
	}
	TFilterDataRem, *PTFilterDataRem;

	class CRemStorage {
	public:
		static
		TFilterDataRem&   Data(void);
	};

	class CRemPersistent {
	public:
		static
		NTSTATUS Append(
			__in    PUNICODE_STRING   _pPath
			);

		static
		NTSTATUS Delete(
			__in    PUNICODE_STRING   _pPath
			);
	};

}}}

#endif/*_FGFSFILTERREMOVABLE_H_267DBC25_6944_401f_A0FC_E877FB7AA63B_INCLUDED*/