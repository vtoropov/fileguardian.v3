/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Feb-2018 at 0:19:17a, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian minifilter driver watch folder data interface implementation file.
	----------------------------------------------------------------------
	Adopted to v15 on 17-Jun-2018 at 10:35:234, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "fg.fs.evt.cmp.base.h"
#include "fg.driver.common.str.h"

using namespace shared::km::fl_sys::_imp;

/////////////////////////////////////////////////////////////////////////////

TFilterData& CFltStorage::Data(void) { static TFilterData data_ = {0}; return data_; }

/////////////////////////////////////////////////////////////////////////////

BOOL
CFltStorage::AddItem (
	__in LIST_HEAD&      _list ,     // list head reference to deal with
	__in PUNICODE_STRING _pData      // pointer to unicode string data;
	)
{
	static TFilterData& flt_data = CFltStorage::Data();

	return CFltDataPersistent::AddItemToList(_list, _pData, flt_data.ItemAllocator);
}


VOID
CFltStorage::RemoveAll (
	__in LIST_HEAD&   _list          // list reference to deal with
	)
{
	static
	TFilterData& flt_data   = CFltStorage::Data();
	
	CFltDataPersistent::RemoveAllItems(_list, flt_data.ItemAllocator);
}

BOOL
CFltStorage::RemoveItem (
	__in LIST_HEAD&      _list ,     // list reference to deal with
	__in PUNICODE_STRING _pData      // pointer to data; it is expected the pointer to wchar buffer
	)
{
	static
	TFilterData& flt_data   = CFltStorage::Data();

	return CFltDataPersistent::RemoveItemFromList(_list, _pData, flt_data.ItemAllocator);
}


/////////////////////////////////////////////////////////////////////////////

PTFilterItem
CFltDataFinder::MatchItem(
	__in LIST_HEAD&      _list ,     // list head reference to search in
	__in PUNICODE_STRING _pPath      // pointer to path that is being compared against pattern(s)
	)
{
	PLIST_ENTRY     pHead    = &_list;
	PLIST_ENTRY     pIter    = pHead->Flink;
	PTFilterItem    pItem    = NULL;
	PUNICODE_STRING pPattern = NULL;

	DECLARE_UNICODE_STRING(staticPattern, _LONG_PATH);

	if (IsListEmpty(&_list)){
#if DBG
			DbgPrint( "__fg_fs: [INFO] CFltDataFinder::MatchItem(): a list is empty;\n" );
#endif
		return NULL;
	}

	if (NULL == _pPath
		|| NULL == _pPath->Buffer){
#if DBG
			DbgPrint( "__fg_fs: [INFO] CDataFinder::MatchItem(): args == NULL;\n" );
#endif
		return NULL;
	}

	while(pIter != pHead)
	{
		pItem  = CONTAINING_RECORD(pIter, TFilterItem, Index);
		if (NULL != pItem){

			pPattern = &pItem->Data.wPathPattern;
			::RtlStringCbCopyUnicodeString(
					staticPattern.Buffer, _LONG_PATH * sizeof(WCHAR), &pItem->Data.wPathPattern
				);

			if (::FsRtlIsNameInExpression(&staticPattern, _pPath, TRUE, NULL)){
#if DBG
				DbgPrint( "__fg_fs: [INFO] CDataFinder::MatchItem(): matched pattern=%wZ; path=%wZ;\n", &staticPattern, _pPath );
#endif
				return pItem;
			}
			RtlZeroMemory(
					staticPattern.Buffer, _LONG_PATH * sizeof(WCHAR)
				);
		}
		pIter = pIter->Flink;
	}

	return NULL;
}

PTFilterItem
CFltDataFinder::MatchItem(
	__in LIST_HEAD&   _list,         // list head reference to deal with
	__in const UCHAR* _pData,        // pointer to data; it is expected the pointer to wchar buffer
	__in const UINT   _nDataLen      // buffer length
	)
{
	TFilterPattern tPattern = {0};

	if (IsListEmpty(&_list))
		return NULL;

	if (!CFltDataAlloca::CreateItemData(tPattern, _pData, _nDataLen))
		return NULL;

	return CFltDataFinder::MatchItem(_list, &tPattern.wPathPattern);
}

/////////////////////////////////////////////////////////////////////////////

BOOL
CFltDataPersistent::AddItemToList(
	__in LIST_HEAD&      _list  ,    // list head reference to deal with;
	__in PUNICODE_STRING _pData ,    // pointer to unicode string data;
	__in LIST_ALLOC&     _alloca
	)
{
	PTFilterItem pItem  =  NULL;

	if (NULL == _pData || NULL == _pData->Buffer || 0 == _pData->Length)
		return FALSE;

	pItem = CFltDataAlloca::AllocateItem(_alloca);
	if (NULL == pItem)
		return FALSE;

	if (!CFltDataAlloca::CreateItemData(pItem->Data, _pData))
	{
		CFltDataAlloca::FreeItem(pItem, _alloca); pItem = NULL;
		return FALSE;
	}

	InsertTailList(&_list, &pItem->Index);
#if DBG
		DbgPrint( "__fg_fs: [INFO] CFltDataPersistent::AddItemToList(): data=%wZ;\n", &pItem->Data.wPathPattern );
#endif

	return TRUE;
}

VOID
CFltDataPersistent::RemoveAllItems(
	__in LIST_HEAD&      _list,         // list reference to deal with
	__in LIST_ALLOC&     _alloca
	)
{
	PLIST_ENTRY    pHead    = &_list;
	PLIST_ENTRY    pIter    = pHead->Flink;
	PTFilterItem   pItem    = NULL;

	while(!IsListEmpty(pHead))
	{
		pIter = RemoveHeadList( pHead );
		pItem = CONTAINING_RECORD(pIter, TFilterItem, Index);
		CFltDataAlloca::FreeItem(pItem, _alloca);
	}
}

BOOL
CFltDataPersistent::RemoveItemFromList(
	__in LIST_HEAD&      _list ,     // list reference to deal with
	__in PUNICODE_STRING _pData,
	__in LIST_ALLOC&     _alloca
	)
{
	PTFilterItem pFound = CFltDataFinder::MatchItem(_list, _pData);

	if (NULL == pFound)
		return FALSE;

	RemoveEntryList(&pFound->Index);

	CFltDataAlloca::FreeItem(pFound, _alloca); pFound = NULL;
	return TRUE;
}