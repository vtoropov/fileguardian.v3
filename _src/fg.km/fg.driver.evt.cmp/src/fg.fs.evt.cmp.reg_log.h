#ifndef _FGFSEVTCMPREGLOG_H_C6344F94_C854_468D_B49D_9A2DDA29C295_INCLUDED
#define _FGFSEVTCMPREGLOG_H_C6344F94_C854_468D_B49D_9A2DDA29C295_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Jun-2018 at 5:10:45p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver watching filter data logged registry interface declaration file.
*/
#include "fg.fs.evt.cmp.reg.h"

namespace shared { namespace km { namespace fl_sys { namespace _imp {

	class CFltRegStg_Logged {

	public:
		class CError {
		public:
			static VOID Put (PWCHAR _pFunc, PWCHAR _pDetails);
			static VOID Put (PWCHAR _pFunc, PUNICODE_STRING _pDetails);
			static VOID Put (PWCHAR _pFunc, PWCHAR _pFormat, CONST LONG _lCode);
			static VOID Put (PWCHAR _pFunc, PWCHAR _pFormat, CONST LONG _lCode, CONST LONG _lValue);
			static VOID Put (PWCHAR _pFunc, PWCHAR _pFormat, CONST LONG _lCode, PUNICODE_STRING _pPath);
		};

		class CPreface {
		public:
			static VOID Excluded(PUNICODE_STRING _path);
			static VOID Included(PUNICODE_STRING _path);
			static VOID OnRemove(VOID);
			static VOID OnUpdate(VOID);
		};

		class CContent {
		public:
			static VOID ExcludedEmpty(void);
			static VOID IncludedEmpty(void);
			static VOID RemoveEmpty  (void);
		};

		class CRemovable {
		public:
		};
	public:
		static
		NTSTATUS
		ItemToLog(PFLT_FILTER _p_filter, PUNICODE_STRING _pItem, PUNICODE_STRING _pFolder);
		static
		NTSTATUS
		ListToLog(PFLT_FILTER _p_filter, PLIST_ENTRY _p_list);
	};

}}}}

#endif/*_FGFSEVTCMPREGLOG_H_C6344F94_C854_468D_B49D_9A2DDA29C295_INCLUDED*/