#ifndef _FGFSEVTCMPALLOCA_H_B43FC6FA_D7B0_4A77_9C4A_6FF20CF4E96D_INCLUDED
#define _FGFSEVTCMPALLOCA_H_B43FC6FA_D7B0_4A77_9C4A_6FF20CF4E96D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 17=Jun-2018 at 6:34:18p, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian common driver library file filter allocator interface declaration file.
*/
#include "fg.fs.evt.cmp.iface.h"

#ifndef LIST_HEAD
#define LIST_HEAD LIST_ENTRY
#endif

#ifndef LIST_ALLOC
#define LIST_ALLOC NPAGED_LOOKASIDE_LIST
#endif

namespace shared { namespace km { namespace fl_sys { namespace _imp {

	typedef struct _t_filter_pattern { UNICODE_STRING wPathPattern; } TFilterPattern, *PTFilterPattern;
	typedef struct _t_filter_item {
		LIST_ENTRY      Index;                // index of filter item list;
		TFilterPattern  Data;                 // directory/folder that is looked for during a matching;
	}
	TFilterItem, *PTFilterItem;

	class CFltDataAlloca {
	public:
		//
		// allocates a folder list item
		//
		static
		PTFilterItem
		AllocateItem(
			LIST_ALLOC& _alloca
			);
		//
		// creates folder pattern from unicode string;
		// returns true on success, otherwise, false;
		//
		static
		BOOL
		CreateItemData(
			__inout TFilterPattern&  _pattern,      // folder pattern;
			__in PUNICODE_STRING     _pData         // pointer to data (data from client message); it is expected the pointer to wchar buffer
			);
		//
		// creates folder item data (from data passed through client message);
		// returns true on success, otherwise, false;
		//
		static
		BOOL
		CreateItemData(
			__inout TFilterPattern&  _itemData,     // list item data
			__in const UCHAR*        _pMsgData,     // pointer to data (data from client message); it is expected the pointer to wchar buffer
			__in const UINT          _nDataLen      // buffer length
			);
		//
		// destroys folder item data; returns true on success, otherwise, false;
		//
		static
		BOOL
		DestroyItemData(
			__inout TFilterPattern& _itemData      // list item data that is being destroyed;
			);
		//
		// frees a folder list item
		//
		static
		VOID
		FreeItem(
			__in PTFilterItem _pItem ,
			__in LIST_ALLOC&  _alloca
			);
	};

}}}}

#endif/*_FGFSEVTCMPALLOCA_H_B43FC6FA_D7B0_4A77_9C4A_6FF20CF4E96D_INCLUDED*/