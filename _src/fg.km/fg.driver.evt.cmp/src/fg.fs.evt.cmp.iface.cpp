/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Jun-2018 at 8:44:45a, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian minifilter driver folder/file filter interface implementation file. 
*/
#include "StdAfx.h"
#include "fg.fs.evt.cmp.iface.h"
#include "fg.fs.evt.cmp.base.h"

using namespace shared::km::fl_sys;
using namespace shared::km::fl_sys::_imp;

#include "fg.driver.common.str.h"
#include "fg.fs.evt.cmp.rem.h"
#include "fg.fs.evt.cmp.reg.h"

#if defined(_DEBUG_CMP)
#include "fg.driver.log.h"
using namespace shared::km::log;
#endif

namespace shared { namespace km { namespace fl_sys { namespace _imp {

	BOOL CFltCompare_IsRemovable(
			__in PUNICODE_STRING   _pPath     // the path/object name to check
		)
	{
		static TFilterDataRem& rem_data = CRemStorage::Data();

		return CFltCompare::MatchFileWithFolderList(_pPath, rem_data.IncludeDirList);
	}

	BOOL CFltCompare_IsRegistry(
			__in PUNICODE_STRING   _pPath     // the path/object name to check
		)
	{
		static TFilterData& reg_data = CFltStorage::Data();

		const BOOL bExcluded = CFltCompare::MatchFileWithFolderList(_pPath, reg_data.ExcludeDirList);
		if (bExcluded)
			return FALSE;
		else
			return CFltCompare::MatchFileWithFolderList(_pPath, reg_data.IncludeDirList);
	}

}}}}

BOOL
CFltCompare::IsIncludedToList(
	__in PUNICODE_STRING   _pPath         // the path/object name to check
)
{
	return (_imp::CFltCompare_IsRegistry(_pPath) ||
	        _imp::CFltCompare_IsRemovable(_pPath));
}

BOOL
CFltCompare::MatchFileWithFolder(
	__in PUNICODE_STRING _pFilePath,      // the string where the finding occurs
	__in PUNICODE_STRING _pFolder         // the string that is being searched
)
{
	DECLARE_UNICODE_STRING_STATIC(u_low_case_file_path_buffer, _LONG_PATH);
	NTSTATUS nt_status  = STATUS_SUCCESS;
	BOOL      b_result  = FALSE;
	INT       n_iter    = 0;
#if DBG
	INT       n_left    = 0;
	INT       n_right   = 0;
	WCHAR     w_left    = NULL;
	WCHAR     w_right   = NULL;
#endif

	if (NULL == _pFilePath ||
		NULL == _pFolder)
		return b_result;

	if (_pFilePath->Length < _pFolder->Length)
		return b_result;
	//
	// we need a lowercase file path in order to exclude case-sensitive comparison;
	// please note; registered folder list already contains all paths in lowercase;
	//
	nt_status = RtlDowncaseUnicodeString(
		&u_low_case_file_path_buffer, _pFilePath, FALSE
	);
	if (!NT_SUCCESS(nt_status))
		return b_result;

	n_iter   = (INT)_pFolder->Length/sizeof(WCHAR);
	b_result = TRUE;

	while (0 < n_iter){
#if DBG
		w_left  = u_low_case_file_path_buffer.Buffer[n_iter - 1];
		w_right = _pFolder->Buffer[n_iter - 1];
		n_left  = (INT)w_left;
		n_right = (INT)w_right;
#endif
		// zero base index; it starts from the end of the paths (especially, from the end of the folder path, which is being searched),
		// because the beginnings of them are very often the same;
		if ( (INT)u_low_case_file_path_buffer.Buffer[n_iter - 1] != (INT)_pFolder->Buffer[n_iter - 1] ){
			b_result = FALSE;
			break;
		}
		// the counter is already calculated in chars number; why it should be decreased in 2 chars? only one cell;
		n_iter -= /*sizeof(WCHAR)*/1;
	}

	RtlZeroMemory(
		u_low_case_file_path_buffer.Buffer, u_low_case_file_path_buffer.MaximumLength
	);

	return b_result;
}

BOOL
CFltCompare::MatchFileWithFolder_Ex(
	__in PUNICODE_STRING _pFilePath,  // the string where the finding occurs
	__in PUNICODE_STRING _pFolder     // the string that is being searched
)
{
	DECLARE_UNICODE_STRING_STATIC(u_low_case_file_path_buffer, _LONG_PATH);
	NTSTATUS nt_status  = STATUS_SUCCESS;
	BOOL      b_result  = FALSE;

	if (_pFilePath->Length < _pFolder->Length)
		return b_result;

	nt_status = ::RtlDowncaseUnicodeString(
		&u_low_case_file_path_buffer, _pFilePath, FALSE
	);
	if (!NT_SUCCESS(nt_status))
		return b_result;

	if (::RtlCompareMemory(u_low_case_file_path_buffer.Buffer, _pFolder->Buffer, _pFolder->Length) == _pFolder->Length)
		b_result = TRUE;

	RtlZeroMemory(
		u_low_case_file_path_buffer.Buffer, u_low_case_file_path_buffer.MaximumLength
	);

	return b_result;
}

BOOL
CFltCompare::MatchFileWithFolderList(
	__in PUNICODE_STRING   _pFilePath,    // the string where the finding occurs
	__in LIST_HEAD&        _list          // list reference to deal with
)
{
	static
	TFilterData& ft_data_ref = CFltStorage::Data();

	PLIST_ENTRY     pHead    = &_list;
	PLIST_ENTRY     pIter    = pHead->Flink;
	PTFilterItem    pItem    = NULL;
	PUNICODE_STRING pFolder  = NULL;
	ULONG           n_ext_cmp= ft_data_ref.b_use_ext;

	if (IsListEmpty(&_list)){
		//
		// this is the error of returning 'true' when match folder list is empty;
		// I could not understand for a long time why system files, which are not being watched, come to event list;
		// Thanks God, a reason is simple: I am stupid;
		//
		return FALSE;
	}

	while(pIter != pHead)
	{
		pItem  = CONTAINING_RECORD(pIter, TFilterItem, Index);
		if (NULL != pItem){

			pFolder = &pItem->Data.wPathPattern;
			if (0 == n_ext_cmp) {
				if (CFltCompare::MatchFileWithFolder(_pFilePath, pFolder)){
#if DBG
					DbgPrint( "__fg_fs: [INFO] FldMatchFileWithFolderList::matched(): folder=%wZ; file=%wZ;\n", pFolder, _pFilePath );
#endif
#if defined(_DEBUG_CMP)
					CLogger_simplified::Put(_pFilePath, pFolder);
#endif
					return TRUE;
				}
			}
			else {
				if (CFltCompare::MatchFileWithFolder_Ex(_pFilePath, pFolder)){
#if DBG
					DbgPrint( "__fg_fs: [INFO] FldMatchFileWithFolderList_Ex::matched(): folder=%wZ; file=%wZ;\n", pFolder, _pFilePath );
#endif
#if defined(_DEBUG_CMP)
					CLogger_simplified::Put(_pFilePath, pFolder);
#endif
					return TRUE;
				}
			}
		}
		pIter = pIter->Flink;
	}
	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////


VOID   CFltAdapter::Initialize(PFLT_FILTER _p_filter) {

	UNUSED(_p_filter);

#if defined(_DEBUG_CMP)
	CLogger_simplified::Initialize();
#endif

	CFltRegStorage::CreateFilter();

#if defined(_DEBUG_CMP)
	CFltRegStorage::Read(_p_filter);
#else
	CFltRegStorage::Read();
#endif
}

VOID   CFltAdapter::Update(PFLT_FILTER _p_filter) {

	UNUSED(_p_filter);

#if defined(_DEBUG_CMP)
	CFltRegStorage::UpdateLists(_p_filter);
#else
	CFltRegStorage::UpdateLists();
#endif
}

VOID   CFltAdapter::Terminate(VOID) {

#if defined(_DEBUG_CMP)
	CLogger_simplified::Terminate();
#endif

	CFltRegStorage::DestroyFilter();
}