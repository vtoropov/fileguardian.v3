/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Feb-2018 at 1:06:46a, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian minifilter driver removable storage data filter interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 17-Jun-2018 at 10:23:47a, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "fg.fs.evt.cmp.rem.h"
#include "fg.fs.evt.cmp.base.h"

using namespace shared::km::fl_sys;
using namespace shared::km::fl_sys::_imp;

#if defined(_DEBUG_CMP)
#include "fg.driver.log.h"
using namespace shared::km::log;
#endif

/////////////////////////////////////////////////////////////////////////////

TFilterDataRem& CRemStorage::Data(void){ static TFilterDataRem m_removable = {0}; return m_removable; }

/////////////////////////////////////////////////////////////////////////////

NTSTATUS CRemPersistent::Append(
	__in    PUNICODE_STRING   _pPath
	)
{
	TFilterDataRem& rem_data = CRemStorage::Data();

	PTFilterItem pItem = CFltDataFinder::MatchItem(
							rem_data.IncludeDirList, _pPath
						);
	if (NULL != pItem)
		return STATUS_SUCCESS;

	BOOL bResult = CFltDataPersistent::AddItemToList(rem_data.IncludeDirList, _pPath, rem_data.ItemAllocator);
	if ( bResult ){
//		notify::CNotifier::_CRemovable::Append(_pPath);
#if defined(_DEBUG_CMP)
		UNICODE_STRING us_title = RTL_CONSTANT_STRING(_T("Removable device is included: "));
		CLogger_simplified::Put(&us_title, _pPath);
#endif
	}

	return STATUS_SUCCESS;
}

NTSTATUS CRemPersistent::Delete(
	__in    PUNICODE_STRING   _pPath
	)
{
	TFilterDataRem& rem_data = CRemStorage::Data();

	BOOL bResult = CFltDataPersistent::RemoveItemFromList(rem_data.IncludeDirList, _pPath, rem_data.ItemAllocator);
	if ( bResult ){
//		notify::CNotifier::_CRemovable::Delete(_pPath);
#if defined(_DEBUG_CMP)
		UNICODE_STRING us_title = RTL_CONSTANT_STRING(_T("Removable device is excluded: "));
		CLogger_simplified::Put(&us_title, _pPath);
#endif
	}

	return STATUS_SUCCESS;
}