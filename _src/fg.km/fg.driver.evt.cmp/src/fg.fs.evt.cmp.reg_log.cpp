/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Jun-2018 at 7:50:07p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver watching filter data logged registry interface implementation file.
*/
#include "StdAfx.h"
#include "fg.fs.evt.cmp.reg_log.h"

using namespace shared::km::fl_sys::_imp;

#include "fg.driver.log.h"
#include "fg.driver.volume.object.h"

using namespace shared::km::log;
using namespace shared::km::fl_sys;

/////////////////////////////////////////////////////////////////////////////

VOID CFltRegStg_Logged::CError::Put (PWCHAR _pFunc, PWCHAR _pDetails) {

	if (NULL == _pFunc)    return;
	if (NULL == _pDetails) return;

	DECLARE_UNICODE_STRING(details_, _LONG_PATH);
	::RtlAppendUnicodeToString(&details_, _pDetails);

	CFltRegStg_Logged::CError::Put(
		_pFunc, &details_
	);
}

VOID CFltRegStg_Logged::CError::Put (PWCHAR _pFunc, PUNICODE_STRING _pDetails) {
	if (NULL == _pFunc)
		return;
	if (NULL == _pDetails || NULL == _pDetails->Buffer || 0 == _pDetails->Length)
		return;

	UNICODE_STRING uc_title = RTL_CONSTANT_STRING(_T("Registry Storage Error\n"));

	CLogger_simplified::Put(&uc_title);

	UNICODE_STRING uc_fn_title  = RTL_CONSTANT_STRING(_T("Function: "));

	DECLARE_UNICODE_STRING(uc_fn_name, _LONG_PATH);
	::RtlAppendUnicodeToString(&uc_fn_name, _pFunc);
	::RtlAppendUnicodeToString(&uc_fn_name, _T("\n"));

	CLogger_simplified::Put(&uc_fn_title, &uc_fn_name);

	UNICODE_STRING uc_dt_title  = RTL_CONSTANT_STRING(_T("Details: "));

	CLogger_simplified::Put(&uc_dt_title, _pDetails);
}

VOID CFltRegStg_Logged::CError::Put (PWCHAR _pFunc, PWCHAR _pFormat, CONST LONG _lCode) {
	UNUSED(_pFunc);
	UNUSED(_pFormat);
	UNUSED(_lCode);

	if (NULL == _pFunc) return;
	if (NULL == _pFormat) return;

	DECLARE_UNICODE_STRING(uc_dt_desc, _LONG_PATH);
	RtlUnicodeStringPrintf(&uc_dt_desc, _pFormat, _lCode);

	CFltRegStg_Logged::CError::Put(_pFunc, &uc_dt_desc);
}

VOID CFltRegStg_Logged::CError::Put (PWCHAR _pFunc, PWCHAR _pFormat, CONST LONG _lCode, CONST LONG _lValue) {
	UNUSED(_pFunc);
	UNUSED(_pFormat);
	UNUSED(_lCode);
	UNUSED(_lValue);

	if (NULL == _pFunc) return;
	if (NULL == _pFormat) return;

	DECLARE_UNICODE_STRING( uc_dt_desc, _LONG_PATH);
	RtlUnicodeStringPrintf(&uc_dt_desc, _pFormat, _lCode, _lValue);

	CFltRegStg_Logged::CError::Put(_pFunc, &uc_dt_desc);
}

VOID CFltRegStg_Logged::CError::Put (PWCHAR _pFunc, PWCHAR _pFormat, CONST LONG _lCode, PUNICODE_STRING _pPath) {
	UNUSED(_pFunc);
	UNUSED(_pFormat);
	UNUSED(_lCode);
	UNUSED(_pPath);

	if (NULL == _pFunc) return;
	if (NULL == _pFormat) return;
	if (NULL == _pPath) return;

	DECLARE_UNICODE_STRING( uc_dt_desc, _LONG_PATH);
	RtlUnicodeStringPrintf(&uc_dt_desc, _pFormat, _lCode, _pPath);

	CFltRegStg_Logged::CError::Put(_pFunc, &uc_dt_desc);
}

/////////////////////////////////////////////////////////////////////////////0

VOID CFltRegStg_Logged::CPreface::Excluded(PUNICODE_STRING _path) {
	UNUSED(_path);
#if defined(_DEBUG_CMP)
	UNICODE_STRING cmt_ex = RTL_CONSTANT_STRING(_T("Reading excluded path(s)"));
	CLogger_simplified::Put(&cmt_ex, _path);
#endif
}

VOID CFltRegStg_Logged::CPreface::Included(PUNICODE_STRING _path) {
	UNUSED(_path);
#if defined(_DEBUG_CMP)
	UNICODE_STRING cmt_in = RTL_CONSTANT_STRING(_T("Reading included path(s)"));
	CLogger_simplified::Put(&cmt_in, _path);
#endif
}

VOID CFltRegStg_Logged::CPreface::OnRemove(VOID) {
#if defined(_DEBUG_CMP)
	UNICODE_STRING cmt_rm = RTL_CONSTANT_STRING(_T("Reading removable data\n"));
	CLogger_simplified::Put(&cmt_rm);
#endif
}

VOID CFltRegStg_Logged::CPreface::OnUpdate(VOID) {
#if defined(_DEBUG_CMP)
	UNICODE_STRING cmt_up = RTL_CONSTANT_STRING(_T("Updating data from registry\n"));
	CLogger_simplified::Put(&cmt_up);
#endif
}

/////////////////////////////////////////////////////////////////////////////

VOID CFltRegStg_Logged::CContent::ExcludedEmpty(void) {
#if defined(_DEBUG_CMP)
	UNICODE_STRING cmt_em = RTL_CONSTANT_STRING(_T("Excluded folder list is empty.\n"));
	CLogger_simplified::Put(&cmt_em);
#endif
}

VOID CFltRegStg_Logged::CContent::IncludedEmpty(void) {
#if defined(_DEBUG_CMP)
	UNICODE_STRING cmt_em = RTL_CONSTANT_STRING(_T("Included folder list is empty.\n"));
	CLogger_simplified::Put(&cmt_em);
#endif
}

VOID CFltRegStg_Logged::CContent::RemoveEmpty  (void) {
#if defined(_DEBUG_CMP)
	UNICODE_STRING cmt_em = RTL_CONSTANT_STRING(_T("No removable device(s)\n"));
	CLogger_simplified::Put(&cmt_em);
#endif
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS
CFltRegStg_Logged::ItemToLog(PFLT_FILTER _p_filter, PUNICODE_STRING _pItem, PUNICODE_STRING _pFolder) {

	NTSTATUS   nt_status = STATUS_SUCCESS;

	if (NULL == _p_filter)
		return (nt_status = STATUS_INVALID_PARAMETER);

	PFLT_VOLUME p_volume = NULL;
	nt_status = CVolume::VolumeFromPath(_p_filter, _pFolder, p_volume);
	if (NT_SUCCESS(nt_status)) {

		CVolume::CProperties props_;
		nt_status = props_.Enumerate(p_volume);

		if (NT_SUCCESS(nt_status)) {
			DECLARE_UNICODE_STRING(vol_chars, _LONG_PATH);
			nt_status = props_.Type(&vol_chars);
			nt_status = props_.Characters(&vol_chars);

			if (NT_SUCCESS(nt_status)) {

				nt_status = ::RtlAppendUnicodeStringToString(&vol_chars, _pFolder);
				if (NT_SUCCESS(nt_status)) {
					CLogger_simplified::Put( _pItem, &vol_chars );
				}
			}
		}
		FltObjectDereference(p_volume); p_volume = NULL;
	}
	return nt_status;
}

NTSTATUS
CFltRegStg_Logged::ListToLog(PFLT_FILTER _p_filter, PLIST_ENTRY _p_list) {

	NTSTATUS nt_status = STATUS_SUCCESS;
	UNUSED(_p_filter);
	UNUSED(_p_list);

	if (NULL == _p_list)
		return (nt_status = STATUS_INVALID_PARAMETER);

	if (NULL == _p_filter)
		return (nt_status = STATUS_INVALID_PARAMETER);

	if (::IsListEmpty(_p_list)) {
		UNICODE_STRING preface_ = RTL_CONSTANT_STRING(_T("Folder list content:"));
		UNICODE_STRING empty_   = RTL_CONSTANT_STRING(_T("(empty)"));

		CLogger_simplified::Put( &preface_, &empty_ );

		return nt_status;
	}

	PLIST_ENTRY p_entry     = _p_list->Flink;
	PTFilterItem p_item     = NULL;
	PUNICODE_STRING pFolder = NULL;
	DECLARE_UNICODE_STRING(us_name, _LONG_PATH);
	LONG l_iter = 0;

	while (p_entry != _p_list) {

		p_item  = CONTAINING_RECORD(p_entry, TFilterItem, Index);
		if (NULL == p_item)
			continue;

		pFolder = &p_item->Data.wPathPattern;
		if (NULL == pFolder || NULL == pFolder->Buffer)
			continue;

		::RtlUnicodeStringPrintf(&us_name, _T("Item_%d"), l_iter++);

		CFltRegStg_Logged::ItemToLog(_p_filter, &us_name, pFolder);
	}
	return nt_status;
}