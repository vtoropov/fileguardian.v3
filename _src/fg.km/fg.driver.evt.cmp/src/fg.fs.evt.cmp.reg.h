#ifndef _FGFSFILTERPERS_H_2204D0FD_DCBA_47c9_B76E_99EEF106BC08_INCLUDED
#define _FGFSFILTERPERS_H_2204D0FD_DCBA_47c9_B76E_99EEF106BC08_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Feb-2018 at 11:13:09p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver watching filter data persistent interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 17-Jun-2018 at 8:53:41a, UTC+7, Phuket, Rawai, Sunday;
*/
#include "fg.fs.evt.cmp.iface.h"
#include "fg.fs.evt.cmp.alloca.h"

//
// TODO: common name definition must be made for both user and kernel mode applications;
//
#ifndef _FG_SERVICE_NAME_COMMON
#define _FG_SERVICE_NAME_COMMON _T("FG_Service")
#endif

namespace shared { namespace km { namespace fl_sys { namespace _imp {

	typedef VOID (__stdcall *TCallbackFunc)(PUNICODE_STRING _pItem, PUNICODE_STRING _pFolder);

	class CFltRegStorage {
	public:
		//
		// creates/initializes global filter data;
		// this routine must be called from driver entry;
		//
		static
		VOID
		CreateFilter(
			VOID
			);
		//
		// this routine is for initializating watching folders from registry;
		// desktop UI writes all settings into the predefined registry folders:
		//
		// (the keys is a subject to change, because they were initially intended for using from user mode service);
		//
		// HKEY_USERS\S-1-5-18\Software\{Service Name}\Folders\Excluded
		// HKEY_USERS\S-1-5-18\Software\{Service name}\Folders\Included
		//
		// thus, it is alternative way to set up matching criteria in comparison
		// with direct control over watching folder lists;
		//
		static
		BOOL
		Read(
			PFLT_FILTER _p_filter = NULL
			);

		static
		BOOL
		ReadOpts (
			__in PUNICODE_STRING   _pRegPath,     // registry path, which to read from
			__in DWORD&            _dwOpts        // options value
			);

		//
		// this routine is initializes filter list from the registry
		// by the path provided;
		//
		static
		BOOL
		ReadToList(
			__in PUNICODE_STRING   _pRegPath,     // registry path, which to read from
			__in LIST_HEAD&        _list    ,     // list reference to deal with
			__in PFLT_FILTER       _p_filter = NULL
			);
		//
		// destroys global filter data;
		// this procedure must be invoked in driver unload routine;
		//
		static
		VOID
		DestroyFilter(
			VOID
			);
		//
		// this routine is called from user mode app via command;
		// it is required for updating filter lists when user mode app makes changes in registry;
		//
		static
		VOID
		UpdateLists(__in PFLT_FILTER   _p_filter = NULL);
	};
}}}}

#endif/*_FGFSFILTERPERS_H_2204D0FD_DCBA_47c9_B76E_99EEF106BC08_INCLUDED*/