/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Feb-2018 at 11:18:10p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver watching filter data persistent interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 17-Jun-2018 at 10:08:33a, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "fg.fs.evt.cmp.reg.h"
#include "fg.fs.evt.cmp.base.h"
#include "fg.fs.evt.cmp.rem.h"
#include "fg.driver.common.str.h"

using namespace shared::km::fl_sys::_imp;

#include "fg.driver.common.reg.h"

using namespace shared::km::reg;

static const ULONG FG_LIST_TAG_NAME = 'fgls';

#if (0)
#include "FG_Generic_Defs.Names.h"
#endif

#if defined(_DEBUG_CMP)
#include "fg.fs.evt.cmp.reg_log.h"
#endif

/////////////////////////////////////////////////////////////////////////////

VOID
CFltRegStorage::CreateFilter(
	VOID
	)
{
	//
	// (1) sets up persistent data
	//
	TFilterData& flt_data = CFltStorage::Data();

	InitializeListHead  ( &flt_data.ExcludeDirList );
	InitializeListHead  ( &flt_data.IncludeDirList );

	KeInitializeSpinLock( &flt_data.pListLock );

	ExInitializeNPagedLookasideList(
				&flt_data.ItemAllocator,
				NULL,
				NULL,
				0,
				sizeof(TFilterItem),
				FG_LIST_TAG_NAME   ,
				0
			);
	//
	// (2) sets up removable data
	//

	TFilterDataRem& rem_data = CRemStorage::Data();

	InitializeListHead  ( &rem_data.IncludeDirList );

	KeInitializeSpinLock( &rem_data.pListLock );

	ExInitializeNPagedLookasideList(
				&rem_data.ItemAllocator,
				NULL,
				NULL,
				0,
				sizeof(TFilterItem),
				FG_LIST_TAG_NAME   ,
				0
			);

	flt_data.b_use_ext = FALSE;
}

VOID
CFltRegStorage::DestroyFilter(
	VOID
)
{
	//
	// (1) destroys persistent filter data
	//
	TFilterData& flt_data = CFltStorage::Data();

	CFltDataPersistent::RemoveAllItems(flt_data.ExcludeDirList, flt_data.ItemAllocator);
	CFltDataPersistent::RemoveAllItems(flt_data.IncludeDirList, flt_data.ItemAllocator);

	ExDeleteNPagedLookasideList( &flt_data.ItemAllocator );

	//
	// (2) destroys removable device data
	//
	TFilterDataRem& rem_data = CRemStorage::Data();

	CFltDataPersistent::RemoveAllItems(rem_data.IncludeDirList, rem_data.ItemAllocator);

	ExDeleteNPagedLookasideList( &rem_data.ItemAllocator );
}

/////////////////////////////////////////////////////////////////////////////

BOOL
CFltRegStorage::ReadOpts (
	__in PUNICODE_STRING   _pRegPath,     // registry path, which to read from
	__in DWORD&            _dwOpts        // options value
) {
	UNICODE_STRING    uCompareMode =  RTL_CONSTANT_STRING(_T("CompareMode")); // TODO: value name must be synchronized with user mode app(s);
	HANDLE            hRegKey      =  NULL;
	NTSTATUS          nt_status    =  STATUS_SUCCESS;

	nt_status = CRegistry::OpenKey(
		_pRegPath, hRegKey
	);
	if (FALSE == NT_SUCCESS(nt_status))
		return FALSE;
	//
	// makes 'agressive' comparison option is set to false (0) by default;
	//
	nt_status = CRegistry::ReadDwordValue(
		hRegKey, &uCompareMode, _dwOpts, 0
	);

	CRegistry::CloseKey(hRegKey);

	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

typedef const UNICODE_STRING CUNICODE_STRING;

#define VALUE_NAME_LEN 0xa // length of _T("Item_%04d") + 1 = 10

BOOL
CFltRegStorage::ReadToList(
	__in PUNICODE_STRING   _pRegPath,     // registry path, which to read from
	__in LIST_HEAD&        _list    ,     // list reference to deal with
	__in PFLT_FILTER       _p_filter
	)
{
	UNICODE_STRING    uCountName   =  RTL_CONSTANT_STRING(_T("Count"));
	HANDLE            hRegKey      =  NULL;
	NTSTATUS          nt_status    =  STATUS_SUCCESS;
	DWORD             lItemCount   =  0;

	DECLARE_UNICODE_STRING(uItemName  , VALUE_NAME_LEN);
	DECLARE_UNICODE_STRING(uItemValue , _LONG_PATH);

#if defined(_DEBUG_CMP)
	UNICODE_STRING    uFuncName    =  RTL_CONSTANT_STRING(_T("CFltRegStorage::ReadToList"));
#endif

	UNUSED(_p_filter);
	//
	// (1) opens the registry
	//
	nt_status = CRegistry::OpenKey(
			_pRegPath, hRegKey
		);

	if (!NT_SUCCESS(nt_status)){
#if defined(_DEBUG_CMP)
		CFltRegStg_Logged::CError::Put( uFuncName.Buffer, _T("status=%X; path=%wZ;"), nt_status, _pRegPath );
#endif
		return FALSE;
	}
	//
	// (2) gets count of the list items
	//
	nt_status = CRegistry::ReadDwordValue(
			hRegKey, &uCountName, lItemCount, 0
		);
	if (!NT_SUCCESS(nt_status)){
#if defined(_DEBUG_CMP)
		CFltRegStg_Logged::CError::Put( uFuncName.Buffer, _T("count(); status=%X; path=%wZ;"), nt_status, _pRegPath );
#endif
	}
	//
	// (3) iterates through all items
	//
	else
	{
		for (DWORD l_ = 0; l_ < lItemCount; l_++)
		{
			nt_status = RtlUnicodeStringPrintf(
							&uItemName, _T("Item_%04d"), l_
						);
			if (!NT_SUCCESS(nt_status)) {
#if defined(_DEBUG_CMP)
				CFltRegStg_Logged::CError::Put( uFuncName.Buffer, _T("iter(); status=%X; value=%d;"), nt_status, l_ );
#endif
				continue;
			}

			nt_status = CRegistry::ReadStringValue(
							hRegKey, &uItemName, &uItemValue
						);
			if (!NT_SUCCESS(nt_status)){
#if defined(_DEBUG_CMP)
				CFltRegStg_Logged::CError::Put( uFuncName.Buffer, _T("get(); status=%X; value=%wZ;"), nt_status, &uItemName );
#endif
				continue;
			}

#if defined(_DEBUG_CMP)
		CFltRegStg_Logged::ItemToLog( _p_filter, &uItemName, &uItemValue );
#endif
			if (!CFltStorage::AddItem(_list, &uItemValue)){

#if defined(_DEBUG_CMP)
				CFltRegStg_Logged::CError::Put( uFuncName.Buffer, _T("read(); inserting item is failed;") );
#endif
			}
			RtlZeroMemory(uItemValue.Buffer, _LONG_PATH);
		}
	}
	CRegistry::CloseKey(hRegKey);
	return TRUE;
}

/*
	|     hive root name            |   abbreviate  |
    -------------------------------------------------
	HKEY_CLASSES_ROOT                      HKCR
	HKEY_CURRENT_USER                      HKCU
	HKEY_LOCAL_MACHINE                     HKLM
	HKEY_USERS                             HKU
	HKEY_CURRENT_CONFIG                    HKCC
*/

BOOL
CFltRegStorage::Read(
	__in PFLT_FILTER   _p_filter
)
{
	BOOL b_result = TRUE;
	DECLARE_UNICODE_STRING(uExcludePath, _LONG_PATH);
	DECLARE_UNICODE_STRING(uIncludePath, _LONG_PATH);
	DECLARE_UNICODE_STRING(uOptionsPath, _LONG_PATH);
	{
		RtlUnicodeStringPrintf(&uExcludePath, _T("\\Registry\\User\\S-1-5-18\\Software\\%ws\\Folders\\Excluded"), _FG_SERVICE_NAME_COMMON);
		RtlUnicodeStringPrintf(&uIncludePath, _T("\\Registry\\User\\S-1-5-18\\Software\\%ws\\Folders\\Included"), _FG_SERVICE_NAME_COMMON);
		RtlUnicodeStringPrintf(&uOptionsPath, _T("\\Registry\\User\\S-1-5-18\\Software\\%ws\\Folders\\Options" ), _FG_SERVICE_NAME_COMMON);
	}
#if (0)
	UNICODE_STRING   uExcludePath = RTL_CONSTANT_STRING(_T("\\Registry\\User\\S-1-5-18\\Software\\FileGuardian\\Folders\\Excluded"));
	UNICODE_STRING   uIncludePath = RTL_CONSTANT_STRING(_T("\\Registry\\User\\S-1-5-18\\Software\\FileGuardian\\Folders\\Included"));
#endif

	static TFilterData& flt_data = CFltStorage::Data();

#if defined(_DEBUG_CMP)
	CFltRegStg_Logged::CPreface::Excluded(&uExcludePath);
#endif

	if (!CFltRegStorage::ReadToList(&uExcludePath, flt_data.ExcludeDirList, _p_filter)) b_result = FALSE;

#if defined(_DEBUG_CMP)
	if (IsListEmpty(&flt_data.ExcludeDirList))
	CFltRegStg_Logged::CContent::ExcludedEmpty();
	CFltRegStg_Logged::CPreface::Included(&uIncludePath);
#endif

	if (!CFltRegStorage::ReadToList(&uIncludePath, flt_data.IncludeDirList, _p_filter)) b_result = FALSE;

#if defined(_DEBUG_CMP)
	if (IsListEmpty(&flt_data.IncludeDirList))
	CFltRegStg_Logged::CContent::IncludedEmpty();
#endif

	CFltRegStorage::ReadOpts  (&uOptionsPath, flt_data.b_use_ext);

	return b_result;
}

VOID
CFltRegStorage::UpdateLists(
	__in PFLT_FILTER   _p_filter
	)
{
	TFilterData& flt_data = CFltStorage::Data();

#if defined(_DEBUG_CMP)
	CFltRegStg_Logged::CPreface::OnUpdate();
#endif

	CFltStorage::RemoveAll(flt_data.ExcludeDirList);
	CFltStorage::RemoveAll(flt_data.IncludeDirList);

	CFltRegStorage::Read(_p_filter);

#if defined(_DEBUG_CMP)
	TFilterDataRem& rem_data = CRemStorage::Data();
	CFltRegStg_Logged::CPreface::OnRemove();
	CFltRegStg_Logged::ListToLog(_p_filter, &rem_data.IncludeDirList);
#endif
}