#ifndef _FGFSFILTERBASE_H_CC29C3FC_8ED3_4a97_B2ED_C213BC41CCAA_INCLUDED
#define _FGFSFILTERBASE_H_CC29C3FC_8ED3_4a97_B2ED_C213BC41CCAA_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Feb-2018 at 0:05:35a, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian minifilter driver watch folder data interface declaration file.
	----------------------------------------------------------------------
	Adopted to v15 on 17-Jun-2018 at 9:50:34a, UTC+7, Phuket, Rawai, Sunday;
*/
#include "fg.fs.evt.cmp.iface.h"
#include "fg.fs.evt.cmp.alloca.h"

#ifndef LIST_HEAD
#define LIST_HEAD LIST_ENTRY
#endif

#ifndef LIST_ALLOC
#define LIST_ALLOC NPAGED_LOOKASIDE_LIST
#endif

namespace shared { namespace km { namespace fl_sys { namespace _imp {

	typedef struct _t_filter_data
	{
		LIST_HEAD       ExcludeDirList;       // list of folder names that are excluded from watching;
		LIST_HEAD       IncludeDirList;       // list of folder names that are being watched;
		KSPIN_LOCK      pListLock;            // lock object of folder lists
		LIST_ALLOC      ItemAllocator;        // lookaside list used for allocating list items;
		ULONG           b_use_ext;            // if not equal to zero, agressive memory comparison is used, otherwise, simple looping;
	}
	TFilterData, *PTFilterData;

	class CFltStorage {
	public:
		static
		TFilterData&   Data(void);
	public:
		//
		// adds new to a list; returns true on success, otherwise, false;
		//
		static
		BOOL
		AddItem(
			__in LIST_HEAD&      _list ,     // list head reference to deal with
			__in PUNICODE_STRING _pData      // pointer to unicode string data;
			);
		//
		// clears list
		//
		static
		VOID
		RemoveAll(
			__in LIST_HEAD&   _list          // list reference to deal with
			);
		//
		// removes the entry from list; returns true, if item is removed,
		// otherwise, false;
		//
		static
		BOOL
		RemoveItem(
			__in LIST_HEAD&      _list ,     // list reference to deal with
			__in PUNICODE_STRING _pData      // pointer to data; it is expected the pointer to wchar buffer
			);
	};

	class CFltDataFinder{
	public:
		//
		// finds a list item by full path; if not found, NULL is returned;
		//
		static
		PTFilterItem
		MatchItem(
			__in LIST_HEAD&      _list ,     // list head reference to search in
			__in PUNICODE_STRING _pPath      // pointer to path that is being compared against pattern(s)
			);
		//
		// finds a list item by data, if not found, NULL is returned;
		//
		static
		PTFilterItem
		MatchItem(
			__in LIST_HEAD&   _list ,        // list head reference to deal with
			__in const UCHAR* _pData,        // pointer to data; it is expected the pointer to wchar buffer
			__in const UINT   _nDataLen      // buffer length
			);
	};

	class CFltDataPersistent{
	public:
		//
		// adds new to a list; returns true on success, otherwise, false;
		//
		static
		BOOL
		AddItemToList(
			__in LIST_HEAD&      _list ,     // list head reference to deal with
			__in PUNICODE_STRING _pData,     // pointer to unicode string data;
			__in LIST_ALLOC&     _alloca
			);
		//
		// clears list
		//
		static
		VOID
		RemoveAllItems(
			__in LIST_HEAD&      _list,      // list reference to deal with
			__in LIST_ALLOC&     _alloca
			);
		//
		// removes the entry from list; returns true, if item is removed,
		// otherwise, false;
		//
		static
		BOOL
		RemoveItemFromList(
			__in LIST_HEAD&      _list ,     // list reference to deal with
			__in PUNICODE_STRING _pData,
			__in LIST_ALLOC&     _alloca
			);
	};

}}}}
#endif/*_FGFSFILTERBASE_H_CC29C3FC_8ED3_4a97_B2ED_C213BC41CCAA_INCLUDED*/