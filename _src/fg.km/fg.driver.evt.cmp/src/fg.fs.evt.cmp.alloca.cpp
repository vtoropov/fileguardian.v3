/*
	Created by Tech_dog (ebontrop@gmail.com) on 17=Jun-2018 at 6:34:18p, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian common driver library file filter allocator interface implementation file.
*/
#include "StdAfx.h"
#include "fg.fs.evt.cmp.alloca.h"

using namespace shared::km::fl_sys::_imp;

/////////////////////////////////////////////////////////////////////////////

static const ULONG FG_ITEM_TAG_NAME = 'fgit';

/////////////////////////////////////////////////////////////////////////////

PTFilterItem
CFltDataAlloca::AllocateItem(
	LIST_ALLOC& _alloca
	)
{
	PTFilterItem pItem = (PTFilterItem)ExAllocateFromNPagedLookasideList(&_alloca);

	if (NULL == pItem) {
#if DBG
		DbgPrint( "__fg_fs: [ERROR] CFltDataAlloca::AllocateItem(): failed;\n" );
#endif
	}
	else {
		RtlZeroMemory( pItem, sizeof( TFilterItem ) );
	}
	return pItem;
}

BOOL
CFltDataAlloca::CreateItemData(
	__inout TFilterPattern&  _itemData,     // list item data
	__in PUNICODE_STRING     _pData         // pointer to data (data from client message); it is expected the pointer to wchar buffer
	)
{
	NTSTATUS nt_status = STATUS_SUCCESS;
	BOOL       bResult = FALSE;
	USHORT     nReqLen = 0; 

	if (NULL == _pData || NULL == _pData->Buffer || _pData->Length < 1)
		return bResult;

	nReqLen = _pData->Length + sizeof(WCHAR);

	_itemData.wPathPattern.Buffer = (PWCHAR)ExAllocatePoolWithTag(PagedPool, nReqLen, FG_ITEM_TAG_NAME);
	if (NULL == _itemData.wPathPattern.Buffer){
#if DBG
		DbgPrint( "__fg_fs: [ERROR] CFltDataAlloca::CreateItemData(): string allocation failed;\n" );
#endif
		return bResult;
	}
	RtlZeroMemory(_itemData.wPathPattern.Buffer, nReqLen);

	_itemData.wPathPattern.Length        = _pData->Length;
	_itemData.wPathPattern.MaximumLength = nReqLen;

	//
	// protects an access to raw user-mode buffer with an exception handler
	//
	__try
	{
		RtlCopyMemory(_itemData.wPathPattern.Buffer, _pData->Buffer, _pData->Length);
		//
		// makes the string uppercase for case-insensetive comparison
		//
		nt_status = ::RtlDowncaseUnicodeString(&_itemData.wPathPattern, _pData, FALSE);
		if (!NT_SUCCESS(nt_status)){
#if DBG
			DbgPrint( "__fg_fs: [ERROR] CFltDataAlloca::CreateItemData(): low case error code=%X;\n", nt_status );
#endif
			CFltDataAlloca::DestroyItemData(_itemData);
			return bResult;
		}
		else{
		}
		bResult = TRUE;
	}
	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		CFltDataAlloca::DestroyItemData(_itemData);

#if DBG
		DbgPrint( "__fg_fs: [ERROR] CFltDataAlloca::CreateItemData(): string copying failed;\n" );
#endif
	}
	return bResult;
}

BOOL
CFltDataAlloca::CreateItemData(
	__inout TFilterPattern& _itemData,     // list item data
	__in const UCHAR*        _pMsgData,     // pointer to data (data from client message); it is expected the pointer to wchar buffer
	__in const UINT          _nDataLen      // buffer length
	)
{
	UNICODE_STRING uData = {
		static_cast<USHORT>( _nDataLen ),
		static_cast<USHORT>( _nDataLen ),
		reinterpret_cast<PWCH>(
		      const_cast<UCHAR*>(_pMsgData)
		)
	};
	return CFltDataAlloca::CreateItemData(_itemData, &uData);
}

BOOL
CFltDataAlloca::DestroyItemData(
	__inout TFilterPattern& _itemData      // list item data that is being destroyed;
	)
{
	if (NULL == _itemData.wPathPattern.Buffer)
		return FALSE;

	ExFreePoolWithTag(_itemData.wPathPattern.Buffer, FG_ITEM_TAG_NAME);
	_itemData.wPathPattern.Buffer = NULL;
	_itemData.wPathPattern.Length = _itemData.wPathPattern.MaximumLength = 0;

	return TRUE;
}

VOID
CFltDataAlloca::FreeItem(
	__in PTFilterItem _pItem,
	__in LIST_ALLOC&  _alloca
	)
{
	if (NULL == _pItem)
		return;

	CFltDataAlloca::DestroyItemData(_pItem->Data);

	ExFreeToNPagedLookasideList(&_alloca, _pItem);
}