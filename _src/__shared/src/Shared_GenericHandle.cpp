/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Nov-2014 at 0:42:03am, GMT+3, Taganrog, Saturday;
	This is Shared Lite Library Generic Handle class(es) implementation file.
*/
#include "StdAfx.h"
#include "Shared_GenericHandle.h"

using namespace shared::lite::common;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace common { namespace details
{
	static HRESULT  Shared_CloseHandleSafe(HANDLE& handle_)
	{
		if (!handle_ || INVALID_HANDLE_VALUE == handle_)
			return E_INVALIDARG;
		if (!::CloseHandle(handle_))
		{
			return HRESULT_FROM_WIN32(::GetLastError());
		}
		handle_ = INVALID_HANDLE_VALUE;
		return S_OK;
	}

	static HANDLE&  Shared_GetInvalidHandleRef(void)
	{
		static HANDLE handle = INVALID_HANDLE_VALUE;
		return handle;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CAutoHandle::CAutoHandle(void):
	m_handle(NULL)
{
}

CAutoHandle::CAutoHandle(const HANDLE handle_):
	m_handle(handle_)
{
}

CAutoHandle::~CAutoHandle(void)
{
	details::Shared_CloseHandleSafe(m_handle);
}

/////////////////////////////////////////////////////////////////////////////

CAutoHandle& CAutoHandle::operator=(const HANDLE _handle)
{
	details::Shared_CloseHandleSafe(m_handle);
	m_handle = _handle;
	return *this;
}

CAutoHandle::operator HANDLE(void)
{
	return m_handle;
}

CAutoHandle::operator HANDLE(void) const
{
	return m_handle;
}

/////////////////////////////////////////////////////////////////////////////

PHANDLE    CAutoHandle::operator&(void)
{
	return &m_handle;
}

/////////////////////////////////////////////////////////////////////////////
HANDLE     CAutoHandle::Handle(void) const
{
	return m_handle;
}

bool       CAutoHandle::IsValid(void) const
{
	return (NULL != m_handle && INVALID_HANDLE_VALUE != m_handle);
}

void       CAutoHandle::Reset(void)
{
	details::Shared_CloseHandleSafe(m_handle);
}

/////////////////////////////////////////////////////////////////////////////

CAutoHandleArray::CAutoHandleArray(const DWORD dwSize) : m_handles(NULL), m_size(0)
{
	m_error.Reset();
	if (dwSize)
	{
		try
		{
			m_handles = new HANDLE[dwSize];
			m_size = dwSize;
			m_error.Clear();
		}
		catch(::std::bad_alloc&){ m_error = E_OUTOFMEMORY; }
	}
	else
		m_error = E_INVALIDARG;
}

CAutoHandleArray::~CAutoHandleArray(void)
{
	if (m_handles)
	{
		try { delete [] m_handles; m_handles = NULL; } catch(...){}
	}
	m_size = 0;
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef  CAutoHandleArray::Error(void)const
{
	return m_error;
}

bool       CAutoHandleArray::IsValid(void)const
{
	return (NULL != m_handles && m_size);
}

PHANDLE    CAutoHandleArray::Handles(void)const
{
	return m_handles;
}

DWORD      CAutoHandleArray::Size(void)const
{
	return m_size;
}

HANDLE     CAutoHandleArray::operator[] (const INT nIndex) const
{
	return (!this->IsValid() || 0 > nIndex || INT(m_size - 1) < nIndex ? 0 : m_handles[nIndex]);
}

HANDLE&    CAutoHandleArray::operator[] (const INT nIndex)
{
	return (!this->IsValid() || 0 > nIndex || INT(m_size - 1) < nIndex ? details::Shared_GetInvalidHandleRef() : m_handles[nIndex]);
}

/////////////////////////////////////////////////////////////////////////////

#define SAFE_LOCK_HANDLE() SAFE_LOCK(this->m_sync_obj)
/////////////////////////////////////////////////////////////////////////////

CHandleSafe::CHandleSafe(const HANDLE _handle) : m_handle(_handle)
{
}

CHandleSafe::~CHandleSafe(void)
{
	SAFE_LOCK_HANDLE();
	ATLASSERT(INVALID_HANDLE_VALUE == m_handle);
}

/////////////////////////////////////////////////////////////////////////////

CHandleSafe& CHandleSafe::operator=(const HANDLE _handle)
{
	SAFE_LOCK_HANDLE();
	details::Shared_CloseHandleSafe(m_handle);
	m_handle = _handle;
	return *this;
}

CHandleSafe::operator HANDLE(void)
{
	SAFE_LOCK_HANDLE();
	HANDLE handle_ = m_handle;
	return handle_;
}

CHandleSafe::operator HANDLE(void) const
{
	SAFE_LOCK_HANDLE();
	HANDLE handle_ = m_handle;
	return handle_;
}

/////////////////////////////////////////////////////////////////////////////

PHANDLE    CHandleSafe::operator&(void)
{
	SAFE_LOCK_HANDLE();
	PHANDLE p_handle_ = &m_handle;
	return  p_handle_;
}

/////////////////////////////////////////////////////////////////////////////

HANDLE     CHandleSafe::Handle(void) const
{
	SAFE_LOCK_HANDLE();
	HANDLE handle_ = m_handle;
	return handle_;
}

bool       CHandleSafe::IsValid(void) const
{
	HANDLE handle_ = this->Handle();
	return (NULL != handle_ && INVALID_HANDLE_VALUE != handle_);
}

VOID       CHandleSafe::Reset(void)
{
	SAFE_LOCK_HANDLE();
	details::Shared_CloseHandleSafe(m_handle);
}