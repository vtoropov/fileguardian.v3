/*
	Created by Tech_dog (ebontrop@gmail.com) 22-Jun-2015 at 4:33:06pm, GMT+3, Taganrog, Sunday;
	This is Shared Lite Library Generic Synchronize Object class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 27-May-2018 at 1:20:35p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Shared_GenericSyncObject.h"

using namespace shared::lite;
using namespace shared::lite::sync;

/////////////////////////////////////////////////////////////////////////////

CGenericSyncObject::CGenericSyncObject(void) // throw() // no XP support
{
#if (0)
	__try
	{
#endif
		::InitializeCriticalSection(&m_sec);
#if (0)
	}
	__except(STATUS_NO_MEMORY == ::GetExceptionCode())
	{}
#endif
}

CGenericSyncObject::~CGenericSyncObject(void)
{
	::DeleteCriticalSection(&m_sec);
	::memset((void*)&m_sec, 0, sizeof(CRITICAL_SECTION));
}

/////////////////////////////////////////////////////////////////////////////

VOID CGenericSyncObject::Lock(void) const
{
	__try
	{
		::EnterCriticalSection(&m_sec);
	}
	__except(STATUS_NO_MEMORY == ::GetExceptionCode())
	{}
}

BOOL CGenericSyncObject::TryLock(void) const
{
	const BOOL bResult = ::TryEnterCriticalSection(&m_sec);
	return bResult;
}

VOID CGenericSyncObject::Unlock(void) const
{
	__try
	{
		::LeaveCriticalSection(&m_sec);
	}
	__except(STATUS_NO_MEMORY == ::GetExceptionCode())
	{}
}