/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Mar-2014 at 10:52:48am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Operating System Core class(es) implementation file.
*/
#include "StdAfx.h"
#include "Shared_SystemCore.h"

using namespace shared::lite::sys_core;
/////////////////////////////////////////////////////////////////////////////

CCoInitializer::CCoInitializer(const bool bMultiThreaded)
{
	m_error.Reset();
	if (bMultiThreaded)
	{
		m_error = ::CoInitializeEx(0, COINIT_MULTITHREADED);
	}
	else
	{
		m_error = ::CoInitialize(NULL);
	}
}

CCoInitializer::CCoInitializer(const DWORD _flag)
{
	m_error = ::CoInitializeEx(0, _flag);
}

CCoInitializer::~CCoInitializer(void)
{
	if (IsSuccess())
	{
		m_error.Reset();
		::CoUninitialize();
	}
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef  CCoInitializer::Error(void) const     { return m_error;    }
bool       CCoInitializer::IsSuccess(void) const { return (!m_error); }

/////////////////////////////////////////////////////////////////////////////

CCoApartmentThreaded::CCoApartmentThreaded(void):TBase((DWORD)COINIT_APARTMENTTHREADED)
{
}

/////////////////////////////////////////////////////////////////////////////

CCoSecurityProvider::CCoSecurityProvider(void) {
	m_error.Source(_T("CCoSecurityProvider"));
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CCoSecurityProvider::Error(VOID) CONST { return m_error; }
HRESULT     CCoSecurityProvider::InitDefailt(void) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = ::CoInitializeSecurity(
						NULL,
						-1,
						NULL,
						NULL,
						RPC_C_AUTHN_LEVEL_NONE,
						RPC_C_IMP_LEVEL_IDENTIFY,
						NULL,
						EOAC_NONE,
						NULL
					);
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}