/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Mar-2014 at 7:06:34am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Library System Error class implementation file.
*/
#include "StdAfx.h"
#include "Shared_SystemError.h"

using namespace shared::lite;
using namespace shared::lite::common;

#include <strsafe.h>
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace common { namespace details
{
	LPCTSTR    SysError_NotFoundMessage(void)
	{
		static LPCTSTR pszNotFound = _T("Error details not found");
		return pszNotFound;
	}

	VOID       SysError_FormatMessage(const DWORD dwError , const CLangId& lng_id, ::ATL::CAtlString& buffer_ref)
	{
		TCHAR szBuffer[_MAX_PATH] = {0};
	
		::FormatMessage(
				FORMAT_MESSAGE_FROM_SYSTEM,
				NULL,
				dwError,
				MAKELANGID(lng_id.dwPrimary, lng_id.dwSecond),
				szBuffer,
				_MAX_PATH - 1,
				NULL
			);
		if (ERROR_RESOURCE_LANG_NOT_FOUND == ::GetLastError())
			buffer_ref = SysError_NotFoundMessage();
		else
			buffer_ref = szBuffer;
	}

	VOID       SysError_FormatMessage(const HRESULT hError, const CLangId& lng_id, ::ATL::CAtlString& buffer_ref)
	{
		TCHAR szBuffer[_MAX_PATH] = {0};
	
		::FormatMessage(
				FORMAT_MESSAGE_FROM_SYSTEM,
				NULL,
				hError,
				MAKELANGID(lng_id.dwPrimary, lng_id.dwSecond),
				szBuffer,
				_MAX_PATH - 1,
				NULL
			);
		buffer_ref = szBuffer;
	}

	CAtlString SysError_FormatMessage(LPCTSTR lpszFormat  , va_list& _args)
	{
		CAtlString msg_;
		size_t t_size = 0;

		try {

			HRESULT hr_ = S_OK;

			do
			{
				t_size += 2048;
				TCHAR* pszBuffer = new TCHAR[t_size];

				::memset(pszBuffer, 0, t_size * sizeof(TCHAR));

				hr_ = ::StringCchVPrintfEx(
								pszBuffer,
								t_size,
								NULL,
								NULL,
								0,
								lpszFormat,
								_args
							);
				if (S_OK == hr_)
					msg_ = pszBuffer;
				if (pszBuffer)
				{
					delete pszBuffer; pszBuffer = NULL;
				}
			}
			while(STRSAFE_E_INSUFFICIENT_BUFFER == hr_);
		}
		catch (::std::bad_alloc&)
		{
		}
		return msg_;
	}

	VOID       SysError_NormalizeMessage(::ATL::CAtlString& buffer_ref)
	{
		if (!buffer_ref.IsEmpty())
		{
			buffer_ref.Replace(_T("\r")  , _T(" "));
			buffer_ref.Replace(_T("\n")  , _T(" "));
			buffer_ref.TrimRight();
		}
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CSysError::CSysError(LPCTSTR pszModule) :
	m_dwError(ERROR_SUCCESS),
	m_hrError(S_OK)
{
	if (pszModule && ::_tcslen(pszModule)) m_module = pszModule;
}

CSysError::CSysError(const CSysError& _err)
{
	*this = _err;
}

CSysError::CSysError(const DWORD dwError, const CLangId& lng_id) :
	m_dwError(dwError),
	m_hrError(__HRESULT_FROM_WIN32(dwError)),
	m_lng_id(lng_id)
{
	details::SysError_FormatMessage(m_dwError, m_lng_id, m_buffer);
	details::SysError_NormalizeMessage(m_buffer);
}

CSysError::CSysError(const HRESULT hError, const CLangId& lng_id):
	m_dwError(CSysError::dwEmpty),
	m_hrError(OLE_E_BLANK),
	m_lng_id(lng_id)
{
	this->SetHresult(hError);
}

CSysError::~CSysError(void)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID       CSysError::Clear(void)
{
	m_dwError = ERROR_SUCCESS;
	m_hrError = S_OK;
	m_buffer  = _T("No error");
	//
	// clears the error state; but does not remove source and module values;
	//
	if (m_source.IsEmpty() && false)
		m_source.Empty();
}

DWORD      CSysError::GetCode(void)const
{
	return m_dwError;
}

LPCTSTR    CSysError::GetDescription(void) const
{
	return m_buffer.GetString();
}

CAtlString CSysError::GetFormattedDetails(LPCTSTR lpszSeparator)const
{
	static LPCTSTR pszPattern = _T("Code=0x%x; desc=%s; module=%s; source=%s");

	CAtlString cs_pattern(pszPattern);
	if (lpszSeparator && ::_tcslen(lpszSeparator))
		cs_pattern.Replace(_T("; "), lpszSeparator);

	CAtlString cs_details;
	cs_details.Format(
			cs_pattern,
			this->GetHresult(),
			this->HasDetails() ? this->GetDescription() : _T("#n/a"),
			::_tcslen(this->Module()) ?  this->Module() : _T("#n/a"),
			::_tcslen(this->Source()) ?  this->Source() : _T("#n/a")
		);
	return cs_details;
}

HRESULT    CSysError::GetHresult(void) const
{
	return m_hrError;
}

bool       CSysError::HasDetails(void) const
{
	return !m_buffer.IsEmpty();
}

LPCTSTR    CSysError::Module(void) const
{
	return m_module;
}

VOID       CSysError::Module(LPCTSTR pszModule)
{
	m_module = pszModule;
}

VOID       CSysError::Reset(void)
{
	//
	// resets error object to indicate no initialization;
	// source and module variables are kept;
	//
	m_dwError = CSysError::dwEmpty;
	m_hrError = OLE_E_BLANK;
	details::SysError_FormatMessage(m_hrError, m_lng_id, m_buffer);
}

VOID       CSysError::SetHresult(const HRESULT hResult)
{
	if (hResult == m_hrError)
		return;
	m_hrError = hResult;
	if (S_OK == m_hrError)
	{
		this->Clear();
		return;
	}
	::ATL::CAtlString cs_source;
	::ATL::CComPtr<IErrorInfo> sp;
	if (S_OK == ::GetErrorInfo(0, &sp) && sp)
	{
		_com_error err(m_hrError, sp, true);
		m_buffer  = (LPCTSTR)err.Description();
		cs_source = (LPCTSTR)err.Source();
		if (cs_source.IsEmpty() == false)
			m_source  = cs_source;
	}
	else
	{
		_com_error err(m_hrError);
		m_buffer  = (LPCTSTR)err.Description();
		cs_source = (LPCTSTR)err.Source();
		if (cs_source.IsEmpty() == false)
			m_source  = cs_source;
	}
	if (m_source.IsEmpty())
		m_source = _T("Undefined");
	if (m_buffer.IsEmpty()) details::SysError_FormatMessage(m_hrError, m_lng_id, m_buffer);

	details::SysError_NormalizeMessage(m_buffer);

	m_dwError = (m_hrError & 0x0000FFFF);
}

VOID       CSysError::SetState(const CSysError& _src)
{
	this->SetState(
			_src.GetHresult(),
			_src.GetDescription()
		);
}

VOID       CSysError::SetState(const DWORD  dwError, LPCTSTR lpszDescription, ...)
{
	m_dwError =   dwError;
	m_hrError = __HRESULT_FROM_WIN32(dwError);

	va_list  args_;
	va_start(args_, lpszDescription);

	m_buffer = details::SysError_FormatMessage(lpszDescription, args_);

	va_end(args_);
}

VOID       CSysError::SetState(const DWORD  dwError, const UINT resId)
{
	::ATL::CAtlString cs_desc;
	cs_desc.LoadString(resId);
	this->SetState(dwError, cs_desc.GetString());
}

VOID       CSysError::SetState(const HRESULT hError, LPCTSTR lpszDescription, ...)
{
	m_dwError = (hError & 0x0000FFFF);
	m_hrError =  hError;

	va_list  args_;
	va_start(args_, lpszDescription);

	m_buffer = details::SysError_FormatMessage(lpszDescription, args_);

	va_end(args_);
}

VOID       CSysError::SetState(const HRESULT hError, const UINT resId)
{
	::ATL::CAtlString cs_desc;
	cs_desc.LoadString(resId);
	this->SetState(hError, cs_desc.GetString());
}

LPCTSTR    CSysError::Source(void) const
{
	return m_source.GetString();
}

VOID       CSysError::Source(LPCTSTR pSource)
{
	m_source = pSource;
}

/////////////////////////////////////////////////////////////////////////////

CSysError& CSysError::operator= (const _com_error& err_ref)
{
	m_hrError = err_ref.Error();
	m_dwError = (m_hrError & 0x0000FFFF);
	m_buffer  = (LPCTSTR)err_ref.Description();
	m_source  = (LPCTSTR)err_ref.Source();
	return *this;
}

CSysError& CSysError::operator= (const DWORD dwError)
{
	this->SetHresult(__HRESULT_FROM_WIN32(dwError));
	return *this;
}

CSysError& CSysError::operator= (const HRESULT hResult)
{
	this->SetHresult(hResult);
	return *this;
}

CSysError& CSysError::operator= (const CSysError& _err)
{
	this->m_buffer  = _err.m_buffer;
	this->m_dwError = _err.m_dwError;
	this->m_hrError = _err.m_hrError;
	this->m_lng_id  = _err.m_lng_id;
	this->m_module  = _err.m_module;
	if (_err.m_source.IsEmpty() == false)
	this->m_source  = _err.m_source;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CSysError::operator bool(void)const
{
	return (m_hrError != S_OK || m_dwError != ERROR_SUCCESS);
}

CSysError::operator HRESULT(void) const
{
	return m_hrError;
}

CSysError::operator LPCTSTR(void) const
{
	return this->GetDescription();
}

/////////////////////////////////////////////////////////////////////////////

CSysErrorSafe::CSysErrorSafe(CGenericSyncObject& _sync) : m_sync_obj(_sync), TBase(_T("CSysErrorSafe"))
{
}

/////////////////////////////////////////////////////////////////////////////

CSysErrorSafe& CSysErrorSafe::operator= (const CSysError& _err)
{
	SAFE_LOCK(m_sync_obj);
	((CSysError&) *this) = _err;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

VOID          CSysErrorSafe::Clear(void)
{
	SAFE_LOCK(m_sync_obj);
	TBase::Clear();
}

CAtlString    CSysErrorSafe::GetFormattedDetails(void)const
{
	SAFE_LOCK(m_sync_obj);
	return TBase::GetFormattedDetails();
}

VOID          CSysErrorSafe::Module(LPCTSTR pszModule)
{
	SAFE_LOCK(m_sync_obj);
	TBase::Module(pszModule);
}

VOID          CSysErrorSafe::SetState(const DWORD  dwError, LPCTSTR pszDescription)
{
	SAFE_LOCK(m_sync_obj);
	TBase::SetState(dwError, pszDescription);
}

VOID          CSysErrorSafe::SetState(const HRESULT hError, LPCTSTR pszDescription)
{
	SAFE_LOCK(m_sync_obj);
	TBase::SetState(hError, pszDescription);
}

VOID          CSysErrorSafe::Source(LPCTSTR pszSource)
{
	SAFE_LOCK(m_sync_obj);
	TBase::Source(pszSource);
}

/////////////////////////////////////////////////////////////////////////////

CSysErrorSafe& CSysErrorSafe::operator= (const DWORD _code)
{
	SAFE_LOCK(m_sync_obj);
	TBase::SetHresult(__HRESULT_FROM_WIN32(_code));	
	return *this;
}

CSysErrorSafe& CSysErrorSafe::operator= (const HRESULT hResult)
{
	SAFE_LOCK(m_sync_obj);
	TBase::SetHresult(hResult);	
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CSysErrorSafe::operator bool(void)const
{
	SAFE_LOCK(m_sync_obj);
	return TBase::operator bool();
}

CSysErrorSafe::operator HRESULT(void) const
{
	SAFE_LOCK(m_sync_obj);
	return TBase::GetHresult();
}