/*
	Created by Tech_dog (VToropov) on 17-Oct-2015 at 3:49:47pm, GMT+7, Phuket, Rawai, Saturday;
	This is Shared Lite Library Operating System Information class implementation file.
*/
#include "StdAfx.h"
#include "Shared_SystemInfo.h"

using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

typedef UINT (WINAPI *PGetSystemWow64DirectoryW)(
	__out LPWSTR lpBuffer, __in UINT uSize
	);

typedef PGetSystemWow64DirectoryW LPGetSystemWow64DirectoryW;
#define NGetSystemWow64DirectoryW  "GetSystemWow64DirectoryW"

/////////////////////////////////////////////////////////////////////////////

bool         CSysInfoDataProvider::Is64Bit(void)
{
	LPGetSystemWow64DirectoryW pGetSystemWow64DirectoryW = NULL;
	pGetSystemWow64DirectoryW = (LPGetSystemWow64DirectoryW)GetProcAddress(GetModuleHandle(_T("Kernel32")), NGetSystemWow64DirectoryW);
	if (pGetSystemWow64DirectoryW)
	{
		wchar_t Wow64Directory[MAX_PATH] = {0};
		const UINT n_len = pGetSystemWow64DirectoryW(Wow64Directory, MAX_PATH);
		if (n_len)
				return true;
	}
	return false;
}

CAtlString   CSysInfoDataProvider::WindowsDrive(void)
{
	CAtlString cs_drive;
	{
		TCHAR buffer[32] = {0};
		::GetEnvironmentVariable(_T("SystemDrive"), buffer, _countof(buffer));
		cs_drive = buffer;
	}
	return cs_drive;
}

CAtlString   CSysInfoDataProvider::WindowsDriveId(void)
{
	CAtlString cs_drive = CSysInfoDataProvider::WindowsDrive();
	if (cs_drive.IsEmpty())
		cs_drive = _T("C:\\");
	if (cs_drive.GetAt(cs_drive.GetLength() - 1) != _T('\\'))
		cs_drive+= _T("\\");
	DWORD dwSerial = 0, dword1 = 0, dword2 = 0;
	TCHAR buff1[0xff]  = { 0 };
	TCHAR buff2[0xff]  = { 0 };
	const BOOL bResult = ::GetVolumeInformation(cs_drive, buff1, _countof(buff1), &dwSerial, &dword1, &dword2, buff2, _countof(buff2));
	if (bResult)
	{
		CAtlString cs_id;
		cs_id.Format(_T("%X"), dwSerial);
		return cs_id;
	}
	else
		return CAtlString();
}