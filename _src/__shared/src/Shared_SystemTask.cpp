/*
	Created by Tech_dog (VToropov) on 17-Feb-2013 at 12:35:26pm, GMT+3, Rostov-on-Don, Sunday;
	This is Shared Lite Library System Task Scheduler Wrapper class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 4:27:29p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include <initguid.h>
#include "Shared_SystemTask.h"
#include "Shared_GenericAppObject.h"

using namespace shared::lite::sys_core;
using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace sys_core { namespace details
{
	static CTask&  CTask_GetInvalidObjectRef(void)
	{
		static CTask invalid(_T("[Invalid Task Ref]"));
		return invalid;
	}
	static HRESULT CTask_NormalizeXML(::ATL::CAtlString& xml_ref)
	{
		xml_ref.Replace(_T("\r"), _T(""));
		xml_ref.Replace(_T("\n"), _T(""));
		return S_OK;
	}
	static LPCTSTR CTask_Status_Desc_Disabled = _T("The task is registered but is disabled and no instances of the task are queued or running. The task cannot be run until it is enabled.");
	static LPCTSTR CTask_Status_Desc_Queued   = _T("Instances of the task are queued.");
	static LPCTSTR CTask_Status_Desc_Ready    = _T("The task is ready to be executed, but no instances are queued or running.");
	static LPCTSTR CTask_Status_Desc_Running  = _T("One or more instances of the task are running.");
	static LPCTSTR CTask_Status_Desc_Unknown  = _T("The state of the task is unknown.");
}}}}

/////////////////////////////////////////////////////////////////////////////

CTaskState::CTaskState(void) : m_current(CTaskState::TS_Unknown)
{
}

CTaskState::CTaskState(const CTaskState::_e _state) : m_current(_state)
{
}

CTaskState::~CTaskState(void)
{
}

/////////////////////////////////////////////////////////////////////////////

const 
CTaskState::_e&  CTaskState::Current(void) const
{
	return m_current;
}

CTaskState::_e&  CTaskState::Current(void)
{
	return m_current;
}

LPCTSTR          CTaskState::Description(void) const
{
	switch (m_current)
	{
	case CTaskState::TS_Disabled:   return details::CTask_Status_Desc_Disabled;
	case CTaskState::TS_Queued:     return details::CTask_Status_Desc_Queued;
	case CTaskState::TS_Ready:      return details::CTask_Status_Desc_Ready;
	case CTaskState::TS_Running:    return details::CTask_Status_Desc_Running;
	default:;
	}
	return details::CTask_Status_Desc_Unknown;
}

/////////////////////////////////////////////////////////////////////////////

CTaskState::operator DWORD(void) const
{
	return (DWORD)m_current;
}

/////////////////////////////////////////////////////////////////////////////

CTask::CTask(void) : m_state(CTaskState::TS_Unknown)
{
}

CTask::CTask(LPCTSTR pszName, const CTaskState::_e _state) : m_state(_state), m_name(pszName)
{
}

CTask::~CTask(void)
{
}

/////////////////////////////////////////////////////////////////////////////

const
CAtlString&  CTask::Author(void) const
{
	return m_author;
}

CAtlString&  CTask::Author(void)
{
	return m_author;
}

const
DWORD&       CTask::Delay(void)const
{
	return m_delay;
}

DWORD&       CTask::Delay(void)
{
	return m_delay;
}

const
CAtlString&  CTask::Name(void) const
{
	return m_name;
}

CAtlString&  CTask::Name(void)
{
	return m_name;
}

const
CAtlString&  CTask::Path(void) const
{
	return m_path;
}

CAtlString&  CTask::Path(void)
{
	return m_path;
}

const
CTaskState&  CTask::State(void) const
{
	return m_state;
}

CTaskState&  CTask::State(void)
{
	return m_state;
}

const
CAtlString&  CTask::XML(void) const
{
	return m_xml;
}

CAtlString&  CTask::XML(void)
{
	return m_xml;
}

/////////////////////////////////////////////////////////////////////////////

bool         CTask::IsValid(void)const
{
	if (this->Name().IsEmpty()
		|| this->Name().GetLength() < 1
		|| this->Path().IsEmpty()
		|| this->Path().GetLength() < 4)
		return false;
	else
		return true;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace sys_core
{
	class CTaskCollectionBase : public ::std::vector<CTask>
	{
	public:
		static HRESULT  _CreateObject_Safe(CTaskCollectionBase*& pObject)
		{
			if (NULL != pObject) return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
			try { pObject = new  CTaskCollectionBase(); } catch(::std::bad_alloc&) { ATLASSERT(FALSE); return E_OUTOFMEMORY; }
			return S_OK;
		}

		static HRESULT  _DestroyObject_Safe(CTaskCollectionBase* pObject)
		{
			if (NULL == pObject) return S_FALSE;
			try { delete pObject; pObject = NULL; } catch(...) { ATLASSERT(FALSE); return E_OUTOFMEMORY; }
			return S_OK;
		}
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CTaskCollection::CTaskCollection(void) : m_pTasks(NULL)
{
	CTaskCollectionBase::_CreateObject_Safe(m_pTasks);
}

CTaskCollection::~CTaskCollection(void)
{
	if (NULL != m_pTasks) (*m_pTasks).clear();
	CTaskCollectionBase::_DestroyObject_Safe(m_pTasks);
}

/////////////////////////////////////////////////////////////////////////////

INT     CTaskCollection::Count(void)const
{
	if (NULL == m_pTasks) return 0;
	return (INT)(*m_pTasks).size();
}

const
CTask&  CTaskCollection::Item(const INT nIndex)const
{
	if (nIndex < 0 || nIndex > Count() - 1) return details::CTask_GetInvalidObjectRef();
	if (NULL == m_pTasks) return details::CTask_GetInvalidObjectRef();
	return (*m_pTasks)[nIndex];
}

/////////////////////////////////////////////////////////////////////////////

CTaskEnumerator::CTaskEnumerator(const bool bUpdateOnCreateOption)
{
	if (bUpdateOnCreateOption == true) Update();
}

CTaskEnumerator::~CTaskEnumerator(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CTaskEnumerator::Update(void)
{
	// TODO: possible memory leak (names) if an error occurs and it is caught in catch section
	if (NULL == m_pTasks)
		return OLE_E_BLANK;

	(*m_pTasks).clear();

	HRESULT hr_ = S_OK;
	try
	{
		::ATL::CComPtr<ITaskScheduler> pTS;
		hr_ = pTS.CoCreateInstance(CLSID_CTaskScheduler);

		::ATL::CComPtr<IEnumWorkItems> pEnum;
		hr_ = pTS->Enum(&pEnum);

		DWORD dwFetchedTasks = 0;
		LPWSTR* pNames = NULL;

		while (SUCCEEDED(pEnum->Next(5, &pNames, &dwFetchedTasks)) && 0 != dwFetchedTasks)
		{
			while (dwFetchedTasks)
			{
				CTask task;
				LPWSTR pName = pNames[--dwFetchedTasks];
				task.Name() = pName;
				(*m_pTasks).push_back(task);
				::CoTaskMemFree(pNames[dwFetchedTasks]); pNames[dwFetchedTasks] = NULL;
			}
			::CoTaskMemFree(pNames); pNames = NULL;
		}
	}
	catch (::std::bad_alloc&)
	{
		hr_ = E_OUTOFMEMORY;
	}
	catch (_com_error& error)
	{
		hr_ = error.Error();
	}
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace sys_core { namespace details
{
	CAtlString    Impersonate_GenerateUUID(void)
	{
		::ATL::CAtlString strUUID;
		GUID guid_ = {0};
		HRESULT hr_ = ::CoCreateGuid(&guid_);
		if (FAILED(hr_))
			strUUID = _T("00000000-0000-0000-0000-000000000000");
		else
		{
			TCHAR buffer_[_MAX_PATH] = {0};
			::StringFromGUID2(guid_, buffer_, _countof(buffer_));
			strUUID = buffer_;
		}
		return strUUID;
	}

	CAtlString    Impersonate_GetHostName(void)
	{
		::ATL::CAtlString strHost;
		TCHAR szHost[1024] = {0};
		DWORD dwSize = _countof(szHost);
		if (::GetComputerName(szHost, &dwSize))
		{
			strHost = szHost;
		}
		return strHost;
	}

	CAtlString    Impersonate_GetUserName(void)
	{
		::ATL::CAtlString strUser;
		TCHAR szUser[1024] = {0};
		DWORD dwSize = _countof(szUser);
		if (::GetUserName(szUser, &dwSize))
		{
			strUser = szUser;
		}
		return strUser;
	}

	CAtlString    Impersonate_GetUserId(void)
	{
		::ATL::CAtlString strUserId;
		strUserId.Format(_T("%s\\%s"), Impersonate_GetHostName(), Impersonate_GetUserName());
		return strUserId;
	}

	HRESULT       TaskService_ConnectToObject(CComPtr<ITaskService>& pService)
	{
		HRESULT hr_ = S_OK;
		try
		{
			hr_ = pService.CoCreateInstance(CLSID_TaskScheduler);
			if (FAILED(hr_))
				return hr_;
			hr_ = pService->Connect(_variant_t(), _variant_t(), _variant_t(), _variant_t());
		}
		catch (_com_error& _err)
		{
			return (hr_ = _err.Error());
		}
		return  hr_;
	}

	HRESULT       TaskService_DeleteTaskFromRoot(CComPtr<ITaskService>& pService, LPCTSTR pszTaskName)
	{
		if (!pService)
			return OLE_E_BLANK;

		HRESULT hr_ = S_OK;
		try
		{
			::ATL::CComPtr<ITaskFolder> pRoot;
			hr_ = pService->GetFolder(_bstr_t(L"\\"), &pRoot);
			if (FAILED(hr_))
				return hr_;
			hr_ = pRoot->DeleteTask( _bstr_t(pszTaskName), 0 );
		}
		catch (_com_error& _err)
		{
			return (hr_ = _err.Error());
		}
		return  hr_;
	}

	HRESULT       TaskService_SetTaskPrinciple(CComPtr<ITaskDefinition>& pDefs)
	{
		if (!pDefs)
			return E_INVALIDARG;

		HRESULT hr_ = S_OK;
		::ATL::CComPtr<IPrincipal> pPrincipal;
		hr_ = pDefs->get_Principal(&pPrincipal);
		if (FAILED(hr_))
			return hr_;
		const TASK_LOGON_TYPE logon_type = TASK_LOGON_INTERACTIVE_TOKEN;
		hr_ = pPrincipal->put_LogonType(logon_type);
		if (FAILED(hr_))
			return hr_;
		const TASK_RUNLEVEL_TYPE run_level = TASK_RUNLEVEL_HIGHEST;
		hr_ = pPrincipal->put_RunLevel(run_level);
		if (FAILED(hr_))
			return hr_;
		::ATL::CAtlString strUserId = details::Impersonate_GetUserId();
		hr_ = pPrincipal->put_UserId(_bstr_t(strUserId.GetString()));

		return hr_;
	}

	HRESULT       TaskService_SetTaskRegInfo(CComPtr<ITaskDefinition>& pDefs, const CTask& _task)
	{
		if (!pDefs)
			return OLE_E_BLANK;

		HRESULT hr_ = S_OK;
		try
		{
			::ATL::CComPtr<IRegistrationInfo> pRegInfo;
			hr_ = pDefs->get_RegistrationInfo(&pRegInfo);
			if (FAILED(hr_))
				return hr_;

			::ATL::CAtlString csAuthor(_task.Author().GetString());
			if (csAuthor.IsEmpty())
				csAuthor = _T("shared::lite::sys_core::CTaskService");

			hr_ = pRegInfo->put_Author(_bstr_t(csAuthor.GetString()));
		}
		catch(_com_error& _err)
		{
			hr_ = _err.Error();
		}
		return hr_;
	}

	HRESULT       TaskService_SetTaskSettings(CComPtr<ITaskDefinition>& pDefs, const CTask& _task)
	{
		_task;
		if (!pDefs)
			return OLE_E_BLANK;

		HRESULT hr_ = S_OK;
		try
		{
			::ATL::CComPtr<ITaskSettings> pSettings;
			hr_ = pDefs->get_Settings(&pSettings);
			if (FAILED(hr_))
				return hr_;
			hr_ = pSettings->put_StartWhenAvailable(VARIANT_TRUE);
		}
		catch(_com_error& _err)
		{
			hr_ = _err.Error();
		}
		return hr_;
	}

	HRESULT       TaskService_SetTaskLogonTrigger(CComPtr<ITaskDefinition>& pDefs, const CTask& _task)
	{
		_task;
		if (!pDefs)
			return OLE_E_BLANK;

		HRESULT hr_ = S_OK;
		try
		{
			::ATL::CComPtr<ITriggerCollection> pTriggers;
			hr_ = pDefs->get_Triggers(&pTriggers);
			if (FAILED(hr_))
				return hr_;
			::ATL::CComPtr<ITrigger> pTrigger;
			hr_ = pTriggers->Create(TASK_TRIGGER_LOGON, &pTrigger);
			if (FAILED(hr_))
				return hr_;
			::ATL::CComQIPtr<ILogonTrigger> pLogon;
			hr_ = pTrigger.QueryInterface(&pLogon);
			if (FAILED(hr_))
				return (hr_);
			if (!pLogon)
				return (hr_ = E_NOINTERFACE);
			::ATL::CAtlString strId = details::Impersonate_GenerateUUID();
			hr_ = pLogon->put_Id(_bstr_t(strId.GetString()));
		}
		catch(_com_error& _err)
		{
			hr_ = _err.Error();
		}
		return hr_;
	}

	HRESULT       TaskService_SetTaskTimeTrigger(CComPtr<ITaskDefinition>& pDefs, const CTask& _task)
	{
		_task;
		if (!pDefs)
			return OLE_E_BLANK;

		HRESULT hr_ = S_OK;
		try
		{
			::ATL::CComPtr<ITriggerCollection> pTriggers;
			hr_ = pDefs->get_Triggers(&pTriggers);
			if (FAILED(hr_))
				return hr_;
			::ATL::CComPtr<ITrigger> pTrigger;
			hr_ = pTriggers->Create(TASK_TRIGGER_DAILY, &pTrigger);
			if (FAILED(hr_))
				return hr_;
			::ATL::CComQIPtr<IDailyTrigger> pRepeater;
			hr_ = pTrigger.QueryInterface( &pRepeater );
			if (FAILED(hr_))
				return hr_;
			if (!pRepeater)
				return (hr_ = E_NOINTERFACE);
			::ATL::CAtlString strId = details::Impersonate_GenerateUUID();
			hr_ = pRepeater->put_Id(_bstr_t(strId.GetString()));

			SYSTEMTIME st_ = {0};
			::GetLocalTime(&st_);
		}
		catch(_com_error& _err)
		{
			hr_ = _err.Error();
		}
		return hr_;
	}

	HRESULT       TaskService_SetTaskAction(CComPtr<ITaskDefinition>& pDefs, const CTask& _task)
	{
		_task;
		if (!pDefs)
			return OLE_E_BLANK;

		HRESULT hr_ = S_OK;
		try
		{
			::ATL::CComPtr<IActionCollection> pActions;
			hr_ = pDefs->get_Actions(&pActions);
			if (FAILED(hr_))
				return hr_;
			::ATL::CComPtr<IAction> pAction;
			hr_ = pActions->Create(TASK_ACTION_EXEC, &pAction);
			if (FAILED(hr_))
				return hr_;
			::ATL::CComQIPtr<IExecAction> pExecutable;
			hr_ = pAction.QueryInterface(&pExecutable);
			if (FAILED(hr_))
				return hr_;
			if (!pExecutable)
				return (hr_ = E_NOINTERFACE);
			hr_ = pExecutable->put_Path(_bstr_t(_task.Path().GetString()));
		}
		catch(_com_error& _err)
		{
			hr_ = _err.Error();
		}
		return hr_;
	}

	HRESULT       TaskService_RegisterTask(CComPtr<ITaskDefinition>& pDefs, const CTask& _task, CComPtr<ITaskService>& pService)
	{
		_task;
		if (!pDefs)
			return OLE_E_BLANK;

		HRESULT hr_ = S_OK;
		try
		{
			::ATL::CComPtr<IRegisteredTask> pRegistered;
			::ATL::CComPtr<ITaskFolder> pRoot;
			hr_ = pService->GetFolder(_bstr_t(L"\\"), &pRoot);
			if (FAILED(hr_))
				return hr_;

			hr_ = pRoot->RegisterTaskDefinition(
					_bstr_t(_task.Name().GetString()),
					pDefs,
					TASK_CREATE_OR_UPDATE,
					_variant_t(),
					_variant_t(),
					TASK_LOGON_INTERACTIVE_TOKEN,
					_variant_t(L""),
					&pRegistered
				);
		}
		catch(_com_error& _err)
		{
			hr_ = _err.Error();
		}
		return hr_;
	}

	HRESULT       TaskService_CreateTask_Head(CComPtr<ITaskDefinition>& pDefs, const CTask& _task, CComPtr<ITaskService>& pService)
	{
		HRESULT hr_ = S_OK;
		try
		{
			hr_ = details::TaskService_ConnectToObject(pService);
			if (FAILED(hr_))
				return hr_;

			hr_ = details::TaskService_DeleteTaskFromRoot(pService, _task.Name());

			hr_ = pService->NewTask( 0, &pDefs);
			if (FAILED(hr_))
				return hr_;
			
			hr_ = details::TaskService_SetTaskPrinciple(pDefs);
			if (FAILED(hr_))
				return hr_;

			hr_ = details::TaskService_SetTaskRegInfo(pDefs, _task);
			if (FAILED(hr_))
				return hr_;
			
			hr_ = details::TaskService_SetTaskSettings(pDefs, _task);
			if (FAILED(hr_))
				return hr_;
		}
		catch(_com_error& _err)
		{
			hr_ = _err.Error();
		}
		return hr_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CTaskService::CTaskService(const bool bUpdateOnCreateOption)
{
	if (bUpdateOnCreateOption) Update();
}

CTaskService::~CTaskService(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CTaskService::CreateLogonTask(const CTask& new_ref)
{
	if (!new_ref.IsValid())
		return E_INVALIDARG;

	HRESULT hr_ = S_OK;

	::ATL::CComPtr<ITaskService> pService;
	::ATL::CComPtr<ITaskDefinition> pDefs;

	hr_ = details::TaskService_CreateTask_Head(pDefs, new_ref, pService);
	if (FAILED(hr_))
		return hr_;

	try
	{
		hr_ = details::TaskService_SetTaskLogonTrigger(pDefs, new_ref);
		if (FAILED(hr_))
			return hr_;

		hr_ = details::TaskService_SetTaskAction(pDefs, new_ref);
		if (FAILED(hr_))
			return hr_;

		hr_ = details::TaskService_RegisterTask(pDefs, new_ref, pService);
	}
	catch (_com_error& error)
	{
		hr_ = error.Error();
	}
	return  hr_;
}

HRESULT       CTaskService::CreateRepeateTask(const CTask& new_ref)
{
	HRESULT hr_ = S_OK;

	::ATL::CComPtr<ITaskService> pService;
	::ATL::CComPtr<ITaskDefinition> pDefs;

	hr_ = details::TaskService_CreateTask_Head(pDefs, new_ref, pService);
	if (FAILED(hr_))
		return hr_;

	try
	{
//		hr_ = details::TaskService_SetTaskLogonTrigger(pDefs, new_ref);
		if (FAILED(hr_))
			return hr_;

		hr_ = details::TaskService_SetTaskAction(pDefs, new_ref);
		if (FAILED(hr_))
			return hr_;

		hr_ = details::TaskService_RegisterTask(pDefs, new_ref, pService);
	}
	catch (_com_error& error)
	{
		hr_ = error.Error();
	}
	return  hr_;
}

HRESULT       CTaskService::DeleteTaskOf(LPCTSTR pszPath)
{
	HRESULT hr_ = S_OK;
	try
	{
		::ATL::CComPtr<ITaskService> pService;
		hr_ = details::TaskService_ConnectToObject(pService);
		if (FAILED(hr_))
			return hr_;

		::ATL::CComPtr<ITaskFolder> pRoot;
		hr_ = pService->GetFolder(_bstr_t(L"\\"), &pRoot);
		if (FAILED(hr_))
			return hr_;
		hr_ = pRoot->DeleteTask( _bstr_t(pszPath), 0 );
	}
	catch (_com_error& error)
	{
		hr_ = error.Error();
	}
	return  hr_;
}

HRESULT       CTaskService::GetTaskOf(LPCTSTR pszPath, CTask& result_ref) const
{
	pszPath; result_ref;
	HRESULT hr_ = S_OK;
	try
	{
		::ATL::CComPtr<ITaskService> pService;
		hr_ = details::TaskService_ConnectToObject(pService);
		if (FAILED(hr_))
			return hr_;

		::ATL::CComPtr<ITaskFolder> pRoot;
		hr_ = pService->GetFolder(_bstr_t(L"\\"), &pRoot);
		if (FAILED(hr_))
			return hr_;
		::ATL::CComPtr<IRegisteredTask> pRegestered;
		hr_ = pRoot->GetTask(_bstr_t(pszPath), &pRegestered);
		if (FAILED(hr_))
			return hr_;
		::ATL::CComBSTR sName;
		hr_ = pRegestered->get_Name(&sName);
		result_ref.Name() = sName;
		TASK_STATE taskState = TASK_STATE_UNKNOWN;
		hr_ = pRegestered->get_State(&taskState);
		result_ref.State().Current() = (CTaskState::_e)taskState;
		::ATL::CComBSTR sPath;
		hr_ = pRegestered->get_Path(&sPath);
		result_ref.Path() = sPath;
		::ATL::CComBSTR sXml;
		hr_ = pRegestered->get_Xml(&sXml);
		result_ref.XML() = sXml;
	}
	catch (_com_error& error)
	{
		hr_ = error.Error();
	}
	return  hr_;
}

HRESULT       CTaskService::IsTaskExist(LPCTSTR pszPath)const
{
	HRESULT hr_ = S_OK;
	try
	{
		::ATL::CComPtr<ITaskService> pService;
		hr_ = details::TaskService_ConnectToObject(pService);
		if (FAILED(hr_))
			return hr_;

		::ATL::CComPtr<ITaskFolder> pRoot;
		hr_ = pService->GetFolder(_bstr_t(L"\\"), &pRoot);
		if (FAILED(hr_))
			return hr_;
		::ATL::CComPtr<IRegisteredTask> pRegestered;
		hr_ = pRoot->GetTask(_bstr_t(pszPath), &pRegestered);
		if (FAILED(hr_))
			return hr_;
	}
	catch (_com_error& error)
	{
		ATLASSERT(FALSE); hr_ = error.Error();
	}
	return  hr_;
}

HRESULT       CTaskService::Update(void)
{
	if (NULL == m_pTasks) return OLE_E_BLANK;
	(*m_pTasks).clear();
	HRESULT hr_ = S_OK;
	try
	{
		::ATL::CComPtr<ITaskService> pService;
		hr_ = details::TaskService_ConnectToObject(pService);
		if (FAILED(hr_))
			return hr_;

		::ATL::CComPtr<ITaskFolder> pRoot;
		hr_ = pService->GetFolder(_bstr_t(L"\\"), &pRoot);
		if (FAILED(hr_))
			return hr_;
		::ATL::CComPtr<IRegisteredTaskCollection> pTasks;
		hr_ = pRoot->GetTasks(NULL, &pTasks);
		if (FAILED(hr_))
			return hr_;
		LONG numTasks = 0;
		hr_ = pTasks->get_Count(&numTasks);
		if (FAILED(hr_))
			return hr_;
		for (LONG i_ = 0; i_ < numTasks; i_++)
		{
			::ATL::CComPtr<IRegisteredTask> pTask;
			hr_ = pTasks->get_Item(_variant_t(i_ + 1), &pTask);
			if (FAILED(hr_)) // break on error?
				continue;
			CTask task;
			::ATL::CComBSTR sName;
			hr_ = pTask->get_Name(&sName);
			task.Name() = sName;
			TASK_STATE taskState = TASK_STATE_UNKNOWN;
			hr_ = pTask->get_State(&taskState);
			task.State().Current() = (CTaskState::_e)taskState;
			::ATL::CComBSTR sPath;
			hr_ = pTask->get_Path(&sPath);
			task.Path() = sPath;
			::ATL::CComBSTR sXml;
			hr_ = pTask->get_Xml(&sXml);
			task.XML() = sXml;
			details::CTask_NormalizeXML(task.XML());
			(*m_pTasks).push_back(task);
		}
	}
	catch (_com_error& error)
	{
		hr_ = error.Error();
	}
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////
namespace shared { namespace lite { namespace sys_core { namespace details
{
	static  CAtlString   StartupTask_Author(void)
	{
		::ATL::CAtlString strTaskAuthor(Impersonate_GetUserId());
		return strTaskAuthor;
	}
	static  CAtlString   StartupTask_Name(void)
	{
		::ATL::CAtlString strTaskName(_T("FileGuardianStartupTask"));
		return strTaskName;
	}
	static  CAtlString   StartupTask_URI(void)
	{
		::ATL::CAtlString strTaskURI(_T("\\"));
		strTaskURI += StartupTask_Name();
		return strTaskURI;
	}
	static  HRESULT      StartupTask_Executable(::ATL::CAtlString& _path)
	{
		CApplication& the_app = GetAppObjectRef();
		HRESULT hr_ = the_app.GetPathFromAppFolder(_T(".\\fw_desktop.exe"), _path);
		return  hr_;
	}
}}}}
/////////////////////////////////////////////////////////////////////////////

CStartupTask::CStartupTask(void)
{
}

CStartupTask::CStartupTask(LPCTSTR pszTaskExePath)
{
	m_tsk_exe = pszTaskExePath;
}

CStartupTask::~CStartupTask(void)
{
}

/////////////////////////////////////////////////////////////////////////////

bool        CStartupTask::IsOn(void) const
{
	::ATL::CAtlString strTaskURI = details::StartupTask_URI().GetString();

	const bool bUpdateOnCreateOption = false;
	CTaskService service(bUpdateOnCreateOption);

	const HRESULT hr_ = service.IsTaskExist(strTaskURI);
	return (S_OK == hr_);
}

HRESULT     CStartupTask::Off(void)
{
	::ATL::CAtlString strTaskURI = details::StartupTask_URI().GetString();

	const bool bUpdateOnCreateOption = false;
	CTaskService service(bUpdateOnCreateOption);

	HRESULT hr_ = service.DeleteTaskOf(strTaskURI.GetString());
	return  hr_;
}

HRESULT     CStartupTask::On(void)
{
	::ATL::CAtlString strTaskURI = details::StartupTask_URI().GetString();
	::ATL::CAtlString strExecURL;
	if (!m_tsk_exe.IsEmpty())
		strExecURL = m_tsk_exe;
	else
	{
		HRESULT hr_ = details::StartupTask_Executable(strExecURL);
		if (S_OK != hr_)
			return  hr_;
	}
	CTask task(strTaskURI.GetString());
	{
		task.Path() = strExecURL;
		task.Author() = details::StartupTask_Author();
	}
	const bool bUpdateOnCreateOption = false;
	CTaskService service(bUpdateOnCreateOption);

	HRESULT hr_ = service.CreateLogonTask(task);
	return  hr_;
}