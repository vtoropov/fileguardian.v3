/*
	Created by Tech_dog (VToropov) on 19-Feb-2015 at 5:58:02pm, GMT+3, Taganrog, Thursday;
	This is shared lite library UTF-8/Unicode conversion class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 6:27:51p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "Shared_Unicode.h"

using namespace shared::lite::data;

////////////////////////////////////////////////////////////////////////////

CAtlString  CUTF8::ConvertMbsToWcs(const _variant_t& utf8_raw)
{
	utf8_raw;
	m_error = E_NOTIMPL;
	return CAtlString();
}

_variant_t  CUTF8::ConvertWcsToMbs(LPCTSTR pszData)
{
	CRawData utf8_raw;
	m_error = CUTF8::ConvertWcsToMbsBuffer(pszData, utf8_raw);
	if (m_error)
		return _variant_t((LPCTSTR)NULL);

	_variant_t v_data; v_data.Clear();
	m_error = utf8_raw.CopyToVariantAsUtf16(v_data);
	return v_data;
}

TErrorRef   CUTF8::Error(void) const
{
	return m_error;
}

VOID        CUTF8::Reset(void)
{
	if (m_error)m_error.Clear();
}

////////////////////////////////////////////////////////////////////////////

HRESULT     CUTF8::ConvertWcsToMbsBuffer(LPCTSTR pszData, CRawData& mbs_buffer)
{
	if (!pszData || !::_tcslen(pszData))
		return E_INVALIDARG;

	if (mbs_buffer.IsValid())
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	//
	// providing specific buffer length causes appearing an error in the functions below;
	// it is replaced by applying -1 length for 0-terminated string;
	//
	const INT n_req = ::WideCharToMultiByte(
			CP_UTF8,
			0,
			pszData,
			-1, //static_cast<INT>(::_tcslen(pszData)),
			NULL,
			0,
			NULL,
			NULL
		);
	if (!n_req)
		return HRESULT_FROM_WIN32(::GetLastError());

	HRESULT hr_ = mbs_buffer.Create(static_cast<DWORD>(n_req));
	if (S_OK != hr_)
		return  hr_;

	const INT n_len = ::WideCharToMultiByte(
			CP_UTF8,
			0,
			pszData,
			-1, //static_cast<INT>(::_tcslen(pszData)),
			reinterpret_cast<LPSTR>(mbs_buffer.GetData()),
			static_cast<INT>(mbs_buffer.GetSize()),
			NULL,
			NULL
		);
	if (!n_len)
		return HRESULT_FROM_WIN32(::GetLastError());
	else
		return S_OK;
}

HRESULT     CUTF8::ConvertWcsToAnsiBuffer(const CAtlStringW& wc_string, CAtlStringA& mb_string)
{
	HRESULT hr_ = S_OK;

	if (wc_string.IsEmpty())
		return (hr_ = E_INVALIDARG);

	mb_string = wc_string;

	return  hr_;
}