/*
	Created by Tech_dog (VToropov) on 16-Apr-2016 at 12:06:36am, GMT+7, Phuket, Rawai, Sunday;
	This is Shared Lite library operating system security class(es) implementation file.
*/
#include "StdAfx.h"
#include "Shared_SystemSecurity.h"

using namespace shared::lite::security;

#include "Shared_GenericHandle.h"
#include "Shared_GenericAppObject.h"

using namespace shared::lite::common;
using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace security { namespace details
{
	::ATL::CAtlString UserAccount_GetCurrentName(void)
	{
		TCHAR szUser[_MAX_PATH] = {0};
		DWORD dwSize = _countof(szUser);

		if (::GetUserName(szUser, &dwSize))
			return ::ATL::CAtlString(szUser);
		else
			return ::ATL::CAtlString();
	}
}}}}

////////////////////////////////////////////////////////////////////////////

CUserAccount::CUserAccount(LPCTSTR pszUserName) : m_name(pszUserName)
{
	if (m_name.IsEmpty())
	{
		m_error = E_INVALIDARG;
		return;
	}
	m_error.Clear();

	DWORD cbUserSID = 0;
	DWORD cbDomain  = 0;
	SID_NAME_USE eType = SidTypeUnknown;
	CRawData raw_domain;
	do
	{
		if (cbUserSID)
			m_error = m_raw_sid.Create(cbUserSID);
		if (m_error)
			break;
		if (cbDomain)
			m_error = raw_domain.Create(cbDomain * sizeof(TCHAR));
		if (m_error)
			break;
		const BOOL bRetVal = ::LookupAccountName(
				NULL,
				pszUserName,
				reinterpret_cast<PSID>(m_raw_sid.GetData()),
				&cbUserSID,
				reinterpret_cast<LPWSTR>(raw_domain.GetData()),
				&cbDomain,
				&eType);
		if (!bRetVal)
			m_error = ::GetLastError();
		if (m_error.GetCode() != ERROR_INSUFFICIENT_BUFFER)
			break;
		if (!m_error)
			break;
	} while (true);
}

CUserAccount::~CUserAccount(void)
{
}

////////////////////////////////////////////////////////////////////////////

const PSID     CUserAccount::Descriptor(void)const
{
	return (reinterpret_cast<PSID>(m_raw_sid.GetData()));
}

CAtlString     CUserAccount::DescriptorAsText(void)const
{
	LPTSTR lpszSid = NULL;

	BOOL bSuccess = ::ConvertSidToStringSid(
				this->Descriptor(),
				&lpszSid
			);
	if (!bSuccess)
	{
		m_error = ::GetLastError();
		return (CAtlString());
	}
	else
		m_error = S_OK;

	CAtlString sid_ = lpszSid;

	::LocalFree(lpszSid);
	lpszSid = NULL;

	return sid_;
}

TErrorRef      CUserAccount::Error(void)const
{
	return m_error;
}

bool           CUserAccount::HasAccessTo(const PACL pObjDacl, const DWORD dwAccessLevel)const
{
	if (!pObjDacl || !m_raw_sid.IsValid())
		return false;
	TRUSTEE trustee = {0};
	::BuildTrusteeWithSid(&trustee, reinterpret_cast<PSID>(m_raw_sid.GetData()));

	ACCESS_MASK acc_msk = 0;
	m_error = ::GetEffectiveRightsFromAcl(pObjDacl, &trustee, &acc_msk);
	if (m_error)
		return false;
	else
		return (0 != (dwAccessLevel & acc_msk));
}

LPCTSTR        CUserAccount::Name (void)const
{
	return m_name.GetString();
}

/////////////////////////////////////////////////////////////////////////////

CCurrentUserAccount::CCurrentUserAccount(void) : TBaseAccount(details::UserAccount_GetCurrentName())
{
}

/////////////////////////////////////////////////////////////////////////////

CAtlString     CSystemSIDs::GetSidAsText(const CSystemSIDs::_e _sid)
{
	CAtlString sid_;
	switch (_sid)
	{
	case CSystemSIDs::eLocalSystem: sid_ = _T("S-1-5-18"); break;
	default:                        sid_ = _T("S-0-0-00"); break;
	}
	return sid_;
}

/////////////////////////////////////////////////////////////////////////////

CSecurityProvider::CSecurityProvider(void)
{
}

CSecurityProvider::~CSecurityProvider(void)
{
	m_error.Source(_T("CSecurityProvider"));
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CSecurityProvider::Elevate(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	::ATL::CAtlString cs_full_path;
	HRESULT hr_ = CApplication::GetFullPath(cs_full_path);
	if (FAILED(hr_))
		return (m_error = hr_);

	SHELLEXECUTEINFO info_ = {0};
	info_.cbSize = sizeof(SHELLEXECUTEINFO);
	info_.lpVerb = _T("runas");
	info_.lpFile = cs_full_path;
	info_.hwnd   = HWND_DESKTOP;
	info_.nShow  = SW_NORMAL;

	const BOOL bResult = ::ShellExecuteEx(&info_);
	if (!bResult)
		m_error = ::GetLastError();

	return m_error;
}

TErrorRef CSecurityProvider::Error(void)const
{
	return m_error;
}

bool      CSecurityProvider::IsAdminRole(void)const
{
	BOOL bResult = FALSE;
	SID_IDENTIFIER_AUTHORITY sid_nt_authority = SECURITY_NT_AUTHORITY;
	PSID pAdministratorsGroup = NULL;
	bResult = ::AllocateAndInitializeSid(
			&sid_nt_authority,
			2,
			SECURITY_BUILTIN_DOMAIN_RID,
			DOMAIN_ALIAS_RID_ADMINS,
			0,
			0,
			0,
			0,
			0,
			0,
			&pAdministratorsGroup
		);
	if (bResult)
	{
		if (!::CheckTokenMembership(NULL, pAdministratorsGroup, &bResult))
			bResult = FALSE;
		::FreeSid(pAdministratorsGroup);  pAdministratorsGroup = NULL;
	}
	return !!bResult;
}