#ifndef _SHAREDLITEGENERICHANDLE_H_212F82EC_7E06_468a_8DEA_B2A98F4F2D6F_INCLUDED
#define _SHAREDLITEGENERICHANDLE_H_212F82EC_7E06_468a_8DEA_B2A98F4F2D6F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Nov-2014 at 0:42:43am, GMT+3, Taganrog, Saturday;
	This is Shared Lite Library Generic Handle class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 27-May-2018 at 3:21:42p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "Shared_SystemError.h"
#include "Shared_GenericSyncObject.h"

namespace shared { namespace lite { namespace common
{
	class CAutoHandle
	{
	private:
		HANDLE     m_handle;
	public:
		CAutoHandle(void);
		CAutoHandle(const HANDLE);
		~CAutoHandle(void);
	public:
		CAutoHandle& operator=(const HANDLE);
	public:
		operator   HANDLE(void);
		operator   HANDLE(void) const;
	public:
		PHANDLE    operator&(void);
	private:
		CAutoHandle(const CAutoHandle&);
		CAutoHandle& operator= (const CAutoHandle&);
	public:
		HANDLE     Handle(void) const;
		bool       IsValid(void) const;
		VOID       Reset(void);
	};

	class CAutoHandleArray
	{
	private:
		PHANDLE    m_handles;
		DWORD      m_size;
		CSysError  m_error;
	public:
		CAutoHandleArray(const DWORD dwSize);
		~CAutoHandleArray(void);
	public:
		TErrorRef  Error(void)const;
		bool       IsValid(void)const;
		PHANDLE    Handles(void)const;
		DWORD      Size(void)const;
	public:
		HANDLE     operator[] (const INT) const;
		HANDLE&    operator[] (const INT)      ;
	private:
		CAutoHandleArray(const CAutoHandleArray&);
		CAutoHandleArray& operator= (const CAutoHandleArray&);
	};

	using shared::lite::sync::CGenericSyncObject;

	//
	// TODO: actually we need the handle with reference counting, otherwise, protected access doesn't protect from
	//       destroying the handle while it is still used in one of the worker threads;
	//
	class CHandleSafe
	{
	private:
		HANDLE     m_handle;
		CGenericSyncObject m_sync_obj;
	public:
		CHandleSafe(const HANDLE = INVALID_HANDLE_VALUE);
		~CHandleSafe(void);
	public:
		CHandleSafe& operator=(const HANDLE);
	public:
		operator   HANDLE(void);
		operator   HANDLE(void) const;
	public:
		PHANDLE    operator&(void);
	private:
		CHandleSafe(const CHandleSafe&);
		CHandleSafe& operator= (const CHandleSafe&);
	public:
		HANDLE     Handle(void) const;
		bool       IsValid(void) const;
		VOID       Reset(void);
	};
}}}

#endif/*_SHAREDLITEGENERICHANDLE_H_212F82EC_7E06_468a_8DEA_B2A98F4F2D6F_INCLUDED*/