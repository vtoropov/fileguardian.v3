#ifndef _SHAREDLITEGENERICSYNCOBJECT_H_074A8045_A1F6_4f13_8537_76CDF121AEDF_INCLUDED
#define _SHAREDLITEGENERICSYNCOBJECT_H_074A8045_A1F6_4f13_8537_76CDF121AEDF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Jun-2014 at 4:33:07pm, GMT+3, Taganrog, Monday;
	This is Shared Lite Library Generic Synchronize Object class definition file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 27-May-2018 at 1:20:24p, UTC+7, Phuket, Rawai, Sunday;
*/

namespace shared { namespace lite { namespace sync
{
	class CGenericSyncObject
	{
	protected:
		mutable
		CRITICAL_SECTION m_sec;
	public:
		CGenericSyncObject(void);
		virtual ~CGenericSyncObject(void);
	public:
		VOID Lock(void) const;
		BOOL TryLock(void) const;
		VOID Unlock(void) const;
	private:
		CGenericSyncObject(const CGenericSyncObject&);
		CGenericSyncObject& operator= (const CGenericSyncObject&);
	};

	template<typename TLocker>
	class CGenericAutoLocker
	{
	private:
		TLocker&    m_locker_ref;
	public:
		CGenericAutoLocker(TLocker& locker_ref) : m_locker_ref(locker_ref)
		{
			m_locker_ref.Lock();
		}
		~CGenericAutoLocker(void)
		{
			m_locker_ref.Unlock();
		}
	private:
		CGenericAutoLocker(const CGenericAutoLocker&);
		CGenericAutoLocker& operator= (const CGenericAutoLocker&);
	};

	template<typename TLocker>
	class CGenericAutoLockerEx
	{
	private:
		BOOL        m_locked;
		TLocker&    m_locker_ref;
	public:
		CGenericAutoLockerEx(TLocker& locker_ref) : m_locker_ref(locker_ref), m_locked(FALSE)
		{
			m_locked = m_locker_ref.TryLock();
		}
		~CGenericAutoLockerEx(void)
		{
			if (m_locked)
				m_locker_ref.Unlock();
		}
	public:
		BOOL   IsLocked(void)const { return m_locked; }
	private:
		CGenericAutoLockerEx(const CGenericAutoLockerEx&);
		CGenericAutoLockerEx& operator= (const CGenericAutoLockerEx&);
	};
}}}

typedef const shared::lite::sync::CGenericAutoLocker<const shared::lite::sync::CGenericSyncObject> TAutoLocker;
#define SAFE_LOCK(cs)    TAutoLocker   tAutoLocker(cs);

typedef const shared::lite::sync::CGenericAutoLockerEx<const shared::lite::sync::CGenericSyncObject> TAutoLockerEx;
#define SAFE_LOCK_EX(cs) TAutoLockerEx tAutoLockerEx(cs); if (!tAutoLockerEx.IsLocked()) return S_OK;

#endif/*_SHAREDLITEGENERICSYNCOBJECT_H_074A8045_A1F6_4f13_8537_76CDF121AEDF_INCLUDED*/