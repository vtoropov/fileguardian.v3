#ifndef _SHAREDSYSTEMSECURITY_H_BC991B57_7D9F_454d_AD76_12298B4DF5BA_INCLUDED
#define _SHAREDSYSTEMSECURITY_H_BC991B57_7D9F_454d_AD76_12298B4DF5BA_INCLUDED
/*
	Created by Tech_dog (VToropov) on 16-Apr-2016 at 11:55:49pm, GMT+7, Phuket, Rawai, Saturday;
	This is Shared Lite library operating system security class(es) declaration file.
*/
#include "Shared_SystemError.h"
#include "generic.stg.data.h"

#include <Sddl.h>
#include <Aclapi.h>
#include <psapi.h>

namespace shared { namespace lite { namespace security
{
	using shared::lite::common::CSysError;
	using shared::lite::data::CRawData;

	class CUserAccount
	{
	private:
		mutable
		CSysError            m_error;
		CRawData             m_raw_sid;
		::ATL::CAtlString    m_name;
	public:
		CUserAccount(LPCTSTR pszUserName);
		~CUserAccount(void);
	public:
		const PSID           Descriptor(void)const;
		CAtlString           DescriptorAsText(void)const;
		TErrorRef            Error(void)const;
		bool                 HasAccessTo(const PACL pObjDacl, const DWORD dwAccessLevel)const;
		LPCTSTR              Name (void)const;
	private:
		CUserAccount(const CUserAccount&);
		CUserAccount& operator= (const CUserAccount&);
	};

	class CCurrentUserAccount : public CUserAccount
	{
		typedef CUserAccount TBaseAccount;
	public:
		CCurrentUserAccount(void);
	};

	// https://support.microsoft.com/en-us/kb/243330
	class CSystemSIDs
	{
	public:
		enum _e {
			eUnknown     = 0x0,
			eLocalSystem = 0x1, // A service account that is used by the operating system.
		};
	public:
		static CAtlString GetSidAsText(const CSystemSIDs::_e);
	};

	class CSecurityProvider
	{
	private:
		mutable
		CSysError         m_error;
	public:
		CSecurityProvider(void);
		~CSecurityProvider(void);
	public:
		HRESULT           Elevate(void);
		TErrorRef         Error(void)const;
		bool              IsAdminRole(void)const;
	};
}}}

#endif/*_SHAREDSYSTEMSECURITY_H_BC991B57_7D9F_454d_AD76_12298B4DF5BA_INCLUDED*/