#ifndef _SHAREDLITEUTF8_H_49C8D5ED_9552_4341_A3A2_7E5060AE912A_INCLUDED
#define _SHAREDLITEUTF8_H_49C8D5ED_9552_4341_A3A2_7E5060AE912A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Feb-2015 at 5:49:26pm, GMT+3, Taganrog, Thursday; 
	This is shared lite library UTF-8/Unicode conversion class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 6:25:25p, UTC+7, Phuket, Rawai, Monday;
*/
#include "Shared_SystemError.h"
#include "generic.stg.data.h"

namespace shared { namespace lite { namespace data
{
	using shared::lite::common::CSysError;

	class CUTF8
	{
	private:
		CSysError   m_error;
	public:
		CAtlString  ConvertMbsToWcs(const _variant_t& utf8_raw);
		_variant_t  ConvertWcsToMbs(LPCTSTR pszData);
		TErrorRef   Error(void) const;
		VOID        Reset(void);
	public:
		static
		HRESULT     ConvertWcsToMbsBuffer(LPCTSTR pszData, CRawData& mbs_buffer);
		static
		HRESULT     ConvertWcsToAnsiBuffer(const CAtlStringW& wc_string, CAtlStringA& mb_string);
	};
}}}

#endif/*_SHAREDLITEUTF8_H_49C8D5ED_9552_4341_A3A2_7E5060AE912A_INCLUDED*/