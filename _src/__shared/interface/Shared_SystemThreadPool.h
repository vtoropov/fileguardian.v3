#ifndef _SHAREDSYSTEMTHREADPOOL_H_88D00B0A_6D7B_4a9c_BBDE_3A546C279DEE_INCLUDED
#define _SHAREDSYSTEMTHREADPOOL_H_88D00B0A_6D7B_4a9c_BBDE_3A546C279DEE_INCLUDED
/*
 *******************************************************************************
 * Copyright (c) Microsoft Corporation.                                        *
 *                                                                             *
 * The class was designed by Kenny Kerr. It provides the ability to queue      *
 * simple member functions of a class to the Windows thread pool.              *
 *                                                                             *
 * Kenny Kerr spends most of his time designing and building distributed       *
 * applications for the Microsoft Windows platform. He also has a particular   *
 * passion for C++ and security programming. Reach Kenny at                    *
 * http://weblogs.asp.net/kennykerr/ or visit his Web site:                    *
 * http://www.kennyandkarin.com/Kenny/.                                        *
 *                                                                             *
 * This source is subject to the Microsoft Public License.                     *
 * See http://www.microsoft.com/en-us/openness/resources/licenses.aspx#MPL.    *
 * All other rights reserved.                                                  *
 *                                                                             *
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, * 
 * EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED       *
 * WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.      *
 *                                                                             *
 *-----------------------------------------------------------------------------*
 *  This is Shared Lite Library System Thread Pool class declaration file.     *
 *-----------------------------------------------------------------------------*
 *                                                                             *
 * Adopted and extended by Tech_dog (ebontrop@gmail.com)                       *
 * on 21-Sep-2015 at 12:48:06am, GMT+7, Phuket, Rawai, Monday;                 *
 *-----------------------------------------------------------------------------*
 *                                                                             *
 *******************************************************************************
*/
#pragma warning(push)
#pragma warning(disable:4995)
#include <memory>
#pragma warning(pop)

#include "Shared_GenericSyncObject.h"
#include "Shared_SystemError.h"

namespace shared { namespace lite { namespace sys_core
{
	class CThreadType
	{
	public:
		enum {
			eDefault  = WT_EXECUTEDEFAULT     ,
			eIOThread = WT_EXECUTEINIOTHREAD  ,
			eUIThread = WT_EXECUTEINUITHREAD  ,
			eLongOper = WT_EXECUTELONGFUNCTION,
		};
	};

	class CThreadPool
	{
	public:
		template <typename T>
		static BOOL QueueUserWorkItem(
					void (T::*pfn)(void), 
					T* pObject,
					ULONG flags = CThreadType::eLongOper
				)
		{
			typedef std::pair<void (T::*)(), T*> CallbackType;
			std::auto_ptr<CallbackType> ptr_(new CallbackType(pfn, pObject));

			if (::QueueUserWorkItem(ThreadProc<T>, ptr_.get(), flags))
			{
				ptr_.release();
				return TRUE;
			}
			else
				return FALSE;
		}

		template <typename T>
		static BOOL QueueUserWorkItemEx(
					void (T::*pfn)(T*),
					T* pObject,
					ULONG flags = CThreadType::eLongOper
				)
		{
			typedef std::pair<void (T::*)(T*), T*> CallbackType;
			std::auto_ptr<CallbackType> ptr_(new CallbackType(pfn, pObject));

			if (::QueueUserWorkItem(ThreadProcEx<T>, ptr_.get(), flags))
			{
				ptr_.release();
				return TRUE;
			}
			else
				return FALSE;
		}
	private:
		template <typename T>
		static DWORD WINAPI ThreadProc(PVOID context)
		{
			typedef std::pair<void (T::*)(), T *> CallbackType;

			std::auto_ptr<CallbackType> p(static_cast<CallbackType *>(context));

			(p->second->*p->first)();
			return 0;
		}

		template <typename T>
		static DWORD WINAPI ThreadProcEx(PVOID context)
		{
			typedef std::pair<void (T::*)(T*), T*> CallbackType;

			std::auto_ptr<CallbackType> ptr_(static_cast<CallbackType*>(context));

			(ptr_->second->*ptr_->first)(ptr_->second);
			return 0;
		}
	};

	class CThreadCrtDataBase
	{
	protected:
		volatile 
		bool           m_bStopped;
		HANDLE         m_hStopEvent;
	public:
		CThreadCrtDataBase(void);
		virtual ~CThreadCrtDataBase(void);
	public:
		const
		HANDLE&        EventObject(void) const;
		HANDLE&        EventObject(void);
		HRESULT        Initialize(void);
		bool           IsStopped(void)const;
		VOID           IsStopped(const bool);
		HRESULT        Terminate(void);
	private:
		CThreadCrtDataBase(const CThreadCrtDataBase&);
		CThreadCrtDataBase& operator= (const CThreadCrtDataBase&);
	};

	class CThreadCrtData : public CThreadCrtDataBase
	{
		typedef CThreadCrtDataBase TBase;
	public:
		CThreadCrtData(void);
		~CThreadCrtData(void);
	};

	class CThreadState
	{
	public:
		enum {
			eStopped   =  0x0,
			eError     =  0x1,
			eWorking   =  0x2,
		};
	};

	using shared::lite::common::CSysError;
	using shared::lite::common::CSysErrorSafe;

	using shared::lite::sync::CGenericSyncObject;

	class CThreadBase
	{
	protected:
		CThreadCrtData      m_crt;
		CGenericSyncObject  m_sync_obj;
	protected:
		volatile
		DWORD               m_state;
		CSysErrorSafe       m_error;
	protected:
		CThreadBase(void);
		~CThreadBase(void);
	public:
		virtual bool        IsComplete(void)const;
		virtual HRESULT     MarkToStop(void);
		virtual HRESULT     Start(void);
		virtual HRESULT     Stop (void);
	private:
		virtual VOID        ThreadFunction(VOID)PURE;
	public:
		CSysError           Error(void)const;
		bool                IsRunning(void)const;
		DWORD               State(void)const;
	};
}}}

#endif/*_SHAREDSYSTEMTHREADPOOL_H_88D00B0A_6D7B_4a9c_BBDE_3A546C279DEE_INCLUDED*/