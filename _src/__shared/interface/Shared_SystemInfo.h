#ifndef _SHAREDSYSTEMINFO_H_D3A71679_400C_4845_8FC5_2C9A9AA9F96B_INCLUDED
#define _SHAREDSYSTEMINFO_H_D3A71679_400C_4845_8FC5_2C9A9AA9F96B_INCLUDED
/*
	Created by Tech_dog (VToropov) on 17-Oct-2015 at 3:21:44pm, GMT+7, Phuket, Rawai, Saturday;
	This is Shared Lite Library Operating System Information class declaration file.
*/
#include "Shared_SystemError.h"

namespace shared { namespace lite { namespace data
{
	using shared::lite::common::CSysError;

	class CSysInfoDataProvider
	{
	public:
		static bool         Is64Bit(void);
		static CAtlString   WindowsDrive(void);
		static CAtlString   WindowsDriveId(void);
		static CAtlString   WindowsPath(void);
	};
}}}

#endif/*_SHAREDSYSTEMINFO_H_D3A71679_400C_4845_8FC5_2C9A9AA9F96B_INCLUDED*/