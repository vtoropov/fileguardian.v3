#ifndef _SHAREDLITESYSTEMCORE_H_5D2FFEE1_0785_4c17_ABF2_99598CF8D1FB_INCLUDED
#define _SHAREDLITESYSTEMCORE_H_5D2FFEE1_0785_4c17_ABF2_99598CF8D1FB_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Mar-2014 at 9:47:36am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite library operating system core interface declaration file.
*/
#include "Shared_SystemError.h"

namespace shared { namespace lite { namespace sys_core
{
	using shared::lite::common::CSysError;

	class CCoInitializer
	{
	protected:
		CSysError   m_error;
	public:
		CCoInitializer(const bool bMultiThreaded);
		CCoInitializer(const DWORD _flag);
		~CCoInitializer(void);
	public:
		TErrorRef   Error(VOID) CONST;
		bool        IsSuccess(VOID) CONST;
	private:
		CCoInitializer(const CCoInitializer&);
		CCoInitializer& operator= (const CCoInitializer&);
	};

	class CCoApartmentThreaded : public CCoInitializer
	{
		typedef CCoInitializer TBase;
	public:
		CCoApartmentThreaded(void);
	};

	class CCoSecurityProvider {
	protected:
		CSysError   m_error;
	public:
		CCoSecurityProvider(void);
	public:
		TErrorRef   Error(VOID) CONST;
		HRESULT     InitDefailt(void);
	};
}}}

#endif/*_SHAREDLITESYSTEMCORE_H_5D2FFEE1_0785_4c17_ABF2_99598CF8D1FB_INCLUDED*/