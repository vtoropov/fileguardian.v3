/////////////////////////////////////////////////////////////////////////////
//
// function(s) for internal use of the HTML
//
/////////////////////////////////////////////////////////////////////////////

var colImage = new Image();
var expImage = new Image();

function fn_on_doc_load()
{
    colImage.src = "./images/fg_node_collapse.gif";
    expImage.src = "./images/fg_node_expand.gif";
}

function fn_on_sec_click(parent_id)
{
    var img  = document.getElementById(parent_id + "_img");
    var cnt  = document.getElementById(parent_id + "_cnt");

    if (null != img)
    {
        if (img.className == "section_exp")
            img.className =  "section_col";
        else
            img.className =  "section_exp";
    }
    if (null != cnt)
    {
        if (cnt.style.display == "none")
            cnt.style.display =  "block";
        else
            cnt.style.display =  "none";
    }
    else
        alert("object " + parent_id + "_cnt is not found");
}