#ifndef _SHAREDREGISTRYDWORD_H_39516275_DA8B_4F38_A094_876FFE71487F_INCLUDED
#define _SHAREDREGISTRYDWORD_H_39516275_DA8B_4F38_A094_876FFE71487F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jun-2018 at 6:59:12p, UTC+7, Phuket, Rawai, Friday;
	This is Shared Library registry dword data related interface declaration file.
*/
#include "Shared_Registry.Common.Defs.h"

namespace shared { namespace registry {

	class CRegistryDword : public CRegistryBase {
		typedef CRegistryBase TBase;
	public:
		CRegistryDword(const CRegistryBase&);
		CRegistryDword(const HKEY hRoot, const DWORD dwOptions /*= CRegistryOptions::eAutoCompletePath*/);
		~CRegistryDword(void);
	public:
		HRESULT     Read  (LPCTSTR lpszFolder, LPCTSTR lpszValueName, DWORD& _out_value, const DWORD _default = 0)const; // loads dword data;
		HRESULT     Write (LPCTSTR lpszFolder, LPCTSTR lpszValueName, const DWORD _in_value)const; // saves dword data;
	public:
		CRegistryDword& operator= (const CRegistryBase&);
	};
}}


#endif/*_SHAREDREGISTRYDWORD_H_39516275_DA8B_4F38_A094_876FFE71487F_INCLUDED*/