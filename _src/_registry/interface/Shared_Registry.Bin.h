#ifndef _SHAREDREGISTRBIN_H_4917EE81_8F39_4C69_9395_DF1BEE807B26_INCLUDED
#define _SHAREDREGISTRBIN_H_4917EE81_8F39_4C69_9395_DF1BEE807B26_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jun-2018 at 1:58:55p, UTC+7, Phuket, Rawai, Friday;
	This is Shared Library registry binary data related interface declaration file.
*/
#include "Shared_Registry.Common.Defs.h"

namespace shared { namespace registry {

	class CRegistryBin : public CRegistryBase {
		typedef CRegistryBase TBase;
	public:
		CRegistryBin(const HKEY hRoot);
		~CRegistryBin(void);
	public:
		HRESULT     Read  (LPCTSTR lpszFolder, LPCTSTR lpszValueName, PBYTE& _out_value, ULONG& _out_size)const; // loads binary data;
		HRESULT     Write (LPCTSTR lpszFolder, LPCTSTR lpszValueName, const PBYTE _in__value, const ULONG _in_size)const; // saves binary data;
	};
}}

#endif/*_SHAREDREGISTRBIN_H_4917EE81_8F39_4C69_9395_DF1BEE807B26_INCLUDED*/