#ifndef _SHAREDREGISTRYEXT_H_8E8778D2_8484_4B1C_8160_DEAAE17C2FA3_INCLUDED
#define _SHAREDREGISTRYEXT_H_8E8778D2_8484_4B1C_8160_DEAAE17C2FA3_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jun-2018 at 4:42:52p, UTC+7, Phuket, Rawai, Friday;
	This is Shared Library registry external resource data related interface declaration file.
*/
#include "Shared_Registry.Common.Defs.h"

namespace shared { namespace registry {

	typedef ::std::map<CAtlString, CAtlString>   TRegistryPathChanges; // key is path name; value is replacement/change;

	class CRegistryStorage_Ext : public CRegistryBase {

		typedef CRegistryBase TBase;

	public:
		CRegistryStorage_Ext(void);
		CRegistryStorage_Ext(const HKEY);
		~CRegistryStorage_Ext(void);

	public:
		HRESULT     ImportFromFile(LPCTSTR lpszFilePath, const bool bAssumeUtf8);
		HRESULT     ImportFromResource(const UINT nResId, const TRegistryPathChanges);
		HRESULT     ImportFromText(const CAtlString& _buffer);
	};

}}

#endif/*_SHAREDREGISTRYEXT_H_8E8778D2_8484_4B1C_8160_DEAAE17C2FA3_INCLUDED*/