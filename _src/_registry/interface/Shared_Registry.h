#ifndef _SHAREDLITELIBRARYREGISTRYDATAPROVIDER_H_2F09C567_4655_42e2_9729_6E40697EF2B8_INCLUDED
#define _SHAREDLITELIBRARYREGISTRYDATAPROVIDER_H_2F09C567_4655_42e2_9729_6E40697EF2B8_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Apr-2015 at 1:01:25pm, GMT+3, Taganrog, Wednesday;
	This is Shared Lite Library Registry Data Provider class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 15-Jun-2018 at 1:38:09p, UTC+7, Phuket, Rawai, Friday;
*/
#include "Shared_Registry.Common.Defs.h"
#include "Shared_Registry.Dword.h"
#include "Shared_Registry.Ext.h"

namespace shared { namespace registry {

	class CRegistryStorage : public CRegistryBase {

		typedef CRegistryBase TBase;
	public:
		CRegistryStorage(const HKEY hRoot, const DWORD dwOptions = CRegistryOptions::eAutoCompletePath);
		~CRegistryStorage(void);
	public:
		HRESULT      Load     (LPCTSTR pszFolder, LPCTSTR pszValueName, ::ATL::CAtlString& _out_value) const;
		HRESULT      Load     (LPCTSTR pszFolder, LPCTSTR pszValueName, LONG& _out_value, const LONG _default = 0) const;
		HRESULT      Remove   (LPCTSTR pszFolder);
		HRESULT      Remove   (LPCTSTR pszFolder, LPCTSTR pszValueName);
		HRESULT      Save     (LPCTSTR pszFolder, LPCTSTR pszValueName, const ::ATL::CAtlString& _in_value);
		HRESULT      Save     (LPCTSTR pszFolder, LPCTSTR pszValueName, const DWORD _in_value);             // saves as dword;
		HRESULT      Save     (LPCTSTR pszFolder, LPCTSTR pszValueName, const LONG  _in_value);             // saves as string;
		HRESULT      Save     (LPCTSTR pszFolder, LPCTSTR pszValueName, const TMultiString& _in_value);     // saves string array as a multi-string registry data type;
	private:
		CRegistryStorage(const CRegistryStorage&);
		CRegistryStorage& operator= (const CRegistryStorage&);
	};
}}

#endif/*_SHAREDLITELIBRARYREGISTRYDATAPROVIDER_H_2F09C567_4655_42e2_9729_6E40697EF2B8_INCLUDED*/