#ifndef _SHAREDREGISTRYENUM_H_30220D21_3178_4479_8091_0D217F41DF52_INCLUDED
#define _SHAREDREGISTRYENUM_H_30220D21_3178_4479_8091_0D217F41DF52_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jun-2018 at 4:57:32p, UTC+7, Phuket, Rawai, Friday;
	This is Shared Library registry entry enumerator interface declaration file.
*/
#include "Shared_Registry.Common.Defs.h"

namespace shared { namespace registry {

	typedef ::std::vector<CAtlString> TRegistryKeyEnum;

	class CRegistryEnumerator : public CRegistryBase {

		typedef CRegistryBase TBase;

	public:
		CRegistryEnumerator(const HKEY hRoot);
		~CRegistryEnumerator(void);
	public:
		TRegistryKeyEnum Enumerate(LPCTSTR lpszFolder);
	};

}}

#endif/*_SHAREDREGISTRYENUM_H_30220D21_3178_4479_8091_0D217F41DF52_INCLUDED*/