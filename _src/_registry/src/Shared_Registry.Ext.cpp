/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jun-2018 at 4:42:52p, UTC+7, Phuket, Rawai, Friday;
	This is Shared Library registry external resource data related interface implementation file.
*/
#include "StdAfx.h"
#include "Shared_Registry.Ext.h"
#include "Shared_RegistryParser.h"
#include "Shared_Registry.h"

using namespace shared::registry;
using namespace shared::registry::_impl;

#include "Shared_GenericAppObject.h"
#include "Shared_GenericAppResource.h"

using namespace shared::user32;

#include "Shared_FS_GenericFile.h"

using namespace shared::ntfs;

/////////////////////////////////////////////////////////////////////////////

CRegistryStorage_Ext::CRegistryStorage_Ext(void) : TBase(HKEY_CURRENT_USER, _T("CRegistryStorage_Ext")) {}
CRegistryStorage_Ext::CRegistryStorage_Ext(const HKEY _key) : TBase(_key, _T("CRegistryStorage_Ext")) {}
CRegistryStorage_Ext::~CRegistryStorage_Ext(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CRegistryStorage_Ext::ImportFromFile(LPCTSTR pszFilePath, const bool bAssumeUtf8)
{
	m_error.Source(_T("CRegistryStorage"));
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_buffer;

	CGenericFile file_(pszFilePath);

	HRESULT hr_ = file_.ReadAsText(cs_buffer, bAssumeUtf8);
	if (FAILED(hr_))
		return (m_error = file_.Error());

	m_error = ImportFromText(cs_buffer);

	return m_error;
}

HRESULT     CRegistryStorage_Ext::ImportFromResource(const UINT nResId, const TRegistryPathChanges _changes)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CRawData raw_data;

	CGenericResourceLoader loader_;
	HRESULT hr_ = loader_.LoadRcDataAsUtf8(nResId, raw_data);
	if (FAILED(hr_))
		return (m_error = loader_.Error());

	CAtlStringA cs_buffer_a;

	hr_ = raw_data.ToStringUtf8(cs_buffer_a);
	if (FAILED(hr_))
		return (m_error = raw_data.Error());

	CAtlStringW cs_buffer_w(cs_buffer_a);

	for (TRegistryPathChanges::const_iterator it_ = _changes.begin(); it_ != _changes.end(); ++it_){

		const CAtlString& path_   = it_->first;
		const CAtlString& change_ = it_->second;

		cs_buffer_w.Replace(path_.GetString(), change_.GetString());
	}

	m_error = ImportFromText(cs_buffer_w);

	return m_error;
}

HRESULT     CRegistryStorage_Ext::ImportFromText(const CAtlString& _buffer)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CRegistryFileParser parser_;
	HRESULT hr_ = parser_.Parse(_buffer);
	if (FAILED(hr_))
		return (m_error = parser_.Error());

	const TRegistryElements& elements_ = parser_.ParsedContent();

	CRegistryStorage stg_(HKEY_USERS, CRegistryOptions::eDoNotModifyPath);

	for (size_t i_ = 0; i_ < elements_.size(); i_++){

		const CRegistryFileElement& element_ = elements_[i_];

		stg_.Root(element_.Path().Root());

		switch (element_.NamedValue().Type())
		{
		case REG_SZ:
		case REG_EXPAND_SZ:
		{
			hr_ = stg_.Save(
				element_.Path().Folder(), element_.NamedValue().Name(), element_.NamedValue().ValueAsText()
			);
		} break;
		case REG_DWORD:
		{
			hr_ = stg_.Save(
				element_.Path().Folder(), element_.NamedValue().Name(), element_.NamedValue().ValueAsDword()
			);
		} break;
		case REG_MULTI_SZ:
		{
			const TMultiString& multi_ = element_.NamedValue().ValueAsMultiString();
			hr_ = stg_.Save(
				element_.Path().Folder(), element_.NamedValue().Name(), multi_
			);
		} break;
		default:
		hr_ = DISP_E_TYPEMISMATCH;
		}
		if (FAILED(hr_))
			return (m_error = hr_);
	}

	return m_error;
}