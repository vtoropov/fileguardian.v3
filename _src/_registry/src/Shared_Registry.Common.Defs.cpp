/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jun-2018 at 3:48:55p, UTC+7, Phuket, Rawai, Friday;
	This is shared ms-windows registry wrapper library common definition implementation file.
*/
#include "StdAfx.h"
#include "Shared_Registry.Common.Defs.h"

using namespace shared::registry;

/////////////////////////////////////////////////////////////////////////////

CRegistryOptions::CRegistryOptions(const DWORD _opts) : m_opts(_opts) {}

/////////////////////////////////////////////////////////////////////////////

bool        CRegistryOptions::DoNotCare(void)const      { return (0 != (CRegistryOptions::eDoNotModifyPath & m_opts));  }
bool        CRegistryOptions::IsAutoComplete(void)const { return (0 != (CRegistryOptions::eAutoCompletePath & m_opts)); }
bool        CRegistryOptions::IsModifyPath(void)const   { return (0 == (CRegistryOptions::eDoNotModifyPath & m_opts));  }
DWORD       CRegistryOptions::Value(void)const          { return m_opts; }
VOID        CRegistryOptions::Value(const DWORD _val)   { m_opts = _val; }

/////////////////////////////////////////////////////////////////////////////

CRegistryBase::CRegistryBase(const HKEY hRoot, LPCTSTR lpszModuleName) : m_root(hRoot) {
	m_error.Source(lpszModuleName);
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CRegistryBase::Error(void)const  { return m_error; }
const
CRegistryOptions& CRegistryBase::Options(void)const { return m_options; }
CRegistryOptions& CRegistryBase::Options(void)      { return m_options; }
HKEY        CRegistryBase::Root (void)const  { return m_root;  }
VOID        CRegistryBase::Root (const HKEY _key)   { m_root = _key; }

/////////////////////////////////////////////////////////////////////////////

HRESULT     CRegistryBase::_OpenKey(::ATL::CRegKey& reg_key, LPCTSTR lpszFolder, LPCTSTR lpszValueName) const {

	CRegistryArgs args_(m_options);

	HRESULT hr_ = args_.IsValid(lpszFolder, lpszValueName);
	if (FAILED(hr_))
		return (m_error = hr_);

	::ATL::CAtlString qry__ = args_.Format(lpszFolder);

	LONG nLastResult = reg_key.Open(m_root, qry__);
	if ( nLastResult!= ERROR_SUCCESS)
		return (hr_ = HRESULT_FROM_WIN32(nLastResult));

	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

#include "Shared_GenericAppObject.h"

using namespace shared::user32;

namespace shared { namespace registry { namespace details {

	// not used;
	CApplication& RegistryStorage_AppObject(void) { return GetAppObjectRef(); }

	DWORD  RegistryStorage_Type(const CRegistryBase& _base, LPCTSTR lpszFolderName, LPCTSTR lpszValueName) {

		DWORD dw_type = 0;
		CRegistryArgs args_(_base.Options());

		HRESULT hr_ = args_.IsValid(lpszFolderName, lpszValueName);
		if (FAILED(hr_))
			return dw_type;

		::ATL::CAtlString qry__ = args_.Format(lpszFolderName);

		::ATL::CRegKey  reg_key;
		LONG l_result = reg_key.Open(_base.Root(), qry__, KEY_READ);
		if ( l_result!= ERROR_SUCCESS)
			return dw_type;

		l_result = ::RegQueryValueEx(
				reg_key.m_hKey, lpszValueName, 0, &dw_type, NULL, NULL
			);
		if ( l_result!= ERROR_SUCCESS)
			dw_type = 0;

		return dw_type;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CRegistryArgs::CRegistryArgs(const CRegistryOptions& _opts) : m_options(_opts) {}

/////////////////////////////////////////////////////////////////////////////

CAtlString     CRegistryArgs::Format(LPCTSTR lpszFolder) const {

	CAtlString out_;

	if (m_options.IsAutoComplete())
		out_.Format(_T("Software\\%s\\%s"), GetAppObjectRef().GetName(), lpszFolder);
	else if (m_options.DoNotCare() == true)
		out_ = lpszFolder;
	//
	// this approach does not work for driver/service settings, because nothing must be changed!!!
	//
	else
		out_.Format(_T("Software\\%s"), lpszFolder);

	return out_;
}

HRESULT        CRegistryArgs::IsValid(LPCTSTR lpszFolder, LPCTSTR lpszValue)const {

	HRESULT hr__ = this->IsFolderName(lpszFolder);
	if (S_OK != hr__)
		return  hr__;

	hr__ = this->IsValueName(lpszValue);
	if (S_OK != hr__)
		return  hr__;

	if (m_options.IsAutoComplete()) { hr__ = GetAppObjectRef().GetLastResult(); }
	return hr__;
}

HRESULT        CRegistryArgs::IsFolderName(LPCTSTR lpszFolder)const {

	HRESULT hr_ = S_OK;
	if (NULL == lpszFolder)        return (hr_ = E_INVALIDARG);  // cannot be nothing
	if (1 > ::_tcslen(lpszFolder)) return (hr_ = E_INVALIDARG);  // cannot be zero-length
	else
		return hr_;
}

HRESULT        CRegistryArgs::IsValueName (LPCTSTR lpszValue )const {

	HRESULT hr_ = S_OK;
	if (NULL == lpszValue)  return (hr_ = E_INVALIDARG);  // cannot be nothing
	else                    return (hr_);
}

/////////////////////////////////////////////////////////////////////////////

CRegistryType::CRegistryType(const CRegistryBase& _base, LPCTSTR lpszFolder, LPCTSTR lpszValueName) {

	m_type = details::RegistryStorage_Type(_base, lpszFolder, lpszValueName);
}
CRegistryType::~CRegistryType(void) {}

/////////////////////////////////////////////////////////////////////////////

bool    CRegistryType::IsDword(void)const { return (REG_DWORD == m_type); }
bool    CRegistryType::IsMultiline(void)const {  return (REG_MULTI_SZ == m_type); }
bool    CRegistryType::IsNone (void)const { return (REG_NONE == m_type); }
bool    CRegistryType::IsText (void)const { return (REG_SZ == m_type); }
DWORD   CRegistryType::Type(void)const    { return m_type; }