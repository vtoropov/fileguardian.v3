/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Apr-2015 at 2:24:00pm, GMT+3, Taganrog, Wednesday;
	This is Shared Lite Library Registry Data Provider class implementation file.
*/
#include "StdAfx.h"
#include "Shared_Registry.h"

using namespace shared::registry;

#include "generic.stg.data.h"

using namespace shared::lite::data;

#define Nothing_Happens_Here()
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace registry { namespace details {
}}}

/////////////////////////////////////////////////////////////////////////////

CRegistryStorage::CRegistryStorage(const HKEY hRoot, const DWORD dwOptions): TBase(hRoot, _T("CRegistryStorage")) {
	TBase::m_options.Value(dwOptions);
}
CRegistryStorage::~CRegistryStorage(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT      CRegistryStorage::Load(LPCTSTR lpszFolder, LPCTSTR lpszValueName, ::ATL::CAtlString& _out_value)const
{
	if (!_out_value.IsEmpty())_out_value.Empty();

	CRegistryArgs args_(TBase::m_options);

	HRESULT hr__ = args_.IsValid(lpszFolder, lpszValueName);
	if (S_OK != hr__)
		return  hr__;

	::ATL::CAtlString qry__ = args_.Format(lpszFolder);

	::ATL::CRegKey  reg_key;
	LONG nLastResult = reg_key.Create(TBase::m_root, qry__);
	switch (nLastResult)
	{
	case ERROR_FILE_NOT_FOUND:
	case ERROR_PATH_NOT_FOUND:
		{
			return (hr__ = S_FALSE);
		} break;
	}
	if (ERROR_SUCCESS != nLastResult)
		return __HRESULT_FROM_WIN32(nLastResult);

	static const ULONG n_buff_len = 4096;
	TCHAR local_buffer[n_buff_len] = {0};
	ULONG local_length = n_buff_len;
	nLastResult = reg_key.QueryStringValue(lpszValueName, local_buffer, &local_length);

	switch (nLastResult)
	{
	case ERROR_FILE_NOT_FOUND:
	case ERROR_PATH_NOT_FOUND:
		{
			return (hr__ = S_FALSE);
		} break;
	}

	if (ERROR_SUCCESS != nLastResult)
		return __HRESULT_FROM_WIN32(nLastResult);

	_out_value = local_buffer;
	return  hr__;
}

HRESULT      CRegistryStorage::Load(LPCTSTR lpszFolder, LPCTSTR lpszValueName, LONG& _out_value, const LONG _default)const
{
	CRegistryType type_(*this, lpszFolder, lpszValueName);
	if (type_.IsNone()) {
		_out_value = _default;
		return S_OK;
	}

	const bool bDword = type_.IsDword();
	if (bDword){

		DWORD dw_value = 0;

		CRegistryDword dword_(*this);
		if (SUCCEEDED(dword_.Read(lpszFolder, lpszValueName, dw_value))) {
			_out_value = static_cast<LONG>(dw_value);
			return S_OK;
		}
	}

	CAtlString cs_value;
	HRESULT hr_ = this->Load(lpszFolder, lpszValueName, cs_value);
	if (FAILED( hr_))
	{
		_out_value = _default;
		return  hr_;
	}
	else if (cs_value.IsEmpty())
		_out_value = _default;
	else
		_out_value = ::_tstol(cs_value);

	return  hr_;
}

HRESULT      CRegistryStorage::Remove(LPCTSTR lpszFolder)
{
	CRegistryArgs args_(TBase::m_options);

	HRESULT hr__ = args_.IsFolderName(lpszFolder);
	if (S_OK != hr__)
		return  hr__;

	::ATL::CAtlString qry__ = args_.Format(lpszFolder);

	LONG lResult = ::RegDeleteKey(TBase::m_root, qry__);

	if (ERROR_FILE_NOT_FOUND == lResult) // registry can be clean
		lResult = ERROR_SUCCESS;

	hr__ = __HRESULT_FROM_WIN32(static_cast<DWORD>(lResult));
	return hr__;
}

HRESULT      CRegistryStorage::Remove(LPCTSTR lpszFolder, LPCTSTR lpszValueName)
{
	CRegistryArgs args_(TBase::m_options);

	HRESULT hr__ = args_.IsValid(lpszFolder, lpszValueName);
	if (S_OK != hr__)
		return  hr__;

	::ATL::CAtlString qry__ = args_.Format(lpszFolder);

	LONG lResult = ::RegDeleteKeyValue(TBase::m_root, qry__, lpszValueName);

	if (ERROR_FILE_NOT_FOUND == lResult) // registry can be clean
		lResult = ERROR_SUCCESS;

	hr__ = __HRESULT_FROM_WIN32(static_cast<DWORD>(lResult));
	return hr__;
}

HRESULT      CRegistryStorage::Save(LPCTSTR lpszFolder, LPCTSTR lpszValueName, const ::ATL::CAtlString& _in_value)
{
	CRegistryArgs args_(TBase::m_options);

	HRESULT hr__ = args_.IsValid(lpszFolder, lpszValueName);
	if (S_OK != hr__)
		return  hr__;

	::ATL::CAtlString qry__ = args_.Format(lpszFolder);

	::ATL::CRegKey  reg_key;
	LONG nLastResult = reg_key.Create(TBase::m_root, qry__);
	if (ERROR_SUCCESS != nLastResult)
		return (hr__ = HRESULT_FROM_WIN32(nLastResult));

	nLastResult = reg_key.SetStringValue(lpszValueName, _in_value.GetString());
	if (ERROR_SUCCESS != nLastResult)
		hr__ = HRESULT_FROM_WIN32(nLastResult);
	return  hr__;
}

HRESULT      CRegistryStorage::Save(LPCTSTR lpszFolder, LPCTSTR lpszValueName, const DWORD _in_value)
{
	CRegistryDword dword_(*this);
	HRESULT hr_ = dword_.Write(lpszFolder, lpszValueName, _in_value);
	if (FAILED(hr_))
		m_error = dword_.Error();

	return hr_;
}

HRESULT      CRegistryStorage::Save(LPCTSTR lpszFolder, LPCTSTR lpszValueName, const LONG _in_value)
{
	//
	// TODO: such approach may be inconsistent with registry entry that is used by other programs
    //       (drivers for example) that treat a type of data, which is stored in registry, strictly;
	//
	::ATL::CAtlString cs_value;
	cs_value.Format(
			_T("%d"),
			_in_value
		);
	HRESULT hr_ = this->Save(lpszFolder, lpszValueName, cs_value.GetString());
	return  hr_;
}

HRESULT      CRegistryStorage::Save(LPCTSTR lpszFolder, LPCTSTR lpszValueName, const TMultiString& _in_value)
{
	CRegistryArgs args_(TBase::m_options);

	HRESULT hr_ = args_.IsValid(lpszFolder, lpszValueName);
	if (S_OK != hr_)
		return  hr_;

	::ATL::CAtlString qry__ = args_.Format(lpszFolder);

	WCHAR w_term = 0;

	CRawData buffer_;
	for (size_t t_ = 0; t_ < _in_value.size(); t_++)
	{
		const CAtlString& element_ = _in_value[t_];

		hr_ = buffer_.Append((PBYTE)element_.GetString(), element_.GetLength() * sizeof(WCHAR));
		if (FAILED(hr_))
			return hr_;

		hr_ = buffer_.Append((PBYTE)&w_term, sizeof(WCHAR));
		if (FAILED(hr_))
			return hr_;
	}
	hr_ = buffer_.Append((PBYTE)&w_term, sizeof(WCHAR)); // multistring ends up with double 0;
		if (FAILED(hr_))
			return hr_;

	::ATL::CRegKey  reg_key;
	LONG nLastResult = reg_key.Create(TBase::m_root, qry__);
	if (ERROR_SUCCESS != nLastResult)
		return (hr_ = HRESULT_FROM_WIN32(nLastResult));

	nLastResult = ::RegSetValueEx(
					reg_key.m_hKey, lpszValueName, NULL, REG_MULTI_SZ, buffer_.GetData(), buffer_.GetSize()
				);
	if (ERROR_SUCCESS != nLastResult)
		hr_ = HRESULT_FROM_WIN32(nLastResult);

	return  hr_;
}