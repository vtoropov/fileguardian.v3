#ifndef _SHAREDREGISTRYPARSER_H_5CBDBAE7_A31C_460f_ADC3_4DEA0DBC8865_INCLUDED
#define _SHAREDREGISTRYPARSER_H_5CBDBAE7_A31C_460f_ADC3_4DEA0DBC8865_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Aug-2017 at 3:16:47p, UTC+7, Phuket, Rawai, Friday;
	This is Shared MS Windows registry script file parser interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 5:20:38p, UTC+7, Phuket, Rawai, Monday;
*/
#include "Shared_Registry.Common.Defs.h"
#include "generic.stg.profile.h"
#include "generic.stg.data.h"

namespace shared { namespace registry { namespace _impl
{
	using shared::lite::data::CRawData;
	using shared::lite::data::CPrivateProfile;
	using shared::lite::common::CSysError;

	class CRegistryPath
	{
	private:
		HKEY               m_root;
		CAtlString         m_folder;
	public:
		CRegistryPath(void);
	public:
		LPCTSTR            Folder(void)const;
		VOID               Folder(LPCTSTR);
		VOID               Init(const HKEY _root, LPCTSTR lpszFolder);
		HKEY               Root(void)const;
		VOID               Root(const HKEY);
	};

	class CRegistryValue
	{
	private:
		DWORD              m_type;
		CAtlString         m_name;
		CRawData           m_raw_buffer;
	public:
		CRegistryValue(void);
	public:
		bool               IsUnicode(void)const;
		LPCTSTR            Name(void)const;
		VOID               Name(LPCTSTR);
		const
		CRawData&          RawData(void)const;
		CRawData&          RawData(void);
		DWORD              Type(void)const;
		VOID               Type(const DWORD);
		DWORD              ValueAsDword(void)const;
		TMultiString       ValueAsMultiString(void)const;
		CAtlString         ValueAsText(void)const;
		CAtlString         ValueAsTextExpanded(void)const; // returns a value as an expanded text (applying actual values of environment variable(s))
	};

	class CRegistryFileElement
	{
	private:
		CRegistryPath      m_path;
		CRegistryValue     m_value;
	public:
		CRegistryFileElement(void);
		~CRegistryFileElement(void);
	public:
		const
		CRegistryValue&    NamedValue(void)const;
		CRegistryValue&    NamedValue(void);
		const
		CRegistryPath&     Path(void)const;
		CRegistryPath&     Path(void);
	};

	typedef ::std::vector<CRegistryFileElement> TRegistryElements;

	class CRegistryFileParser
	{
	private:
		CSysError          m_error;
		CPrivateProfile    m_preliminary_parser;
		TRegistryElements  m_elements;
	public:
		CRegistryFileParser(void);
		~CRegistryFileParser(void);
	public:
		TErrorRef          Error(void) const;
		HRESULT            Parse(LPCTSTR lpszFilePath, const bool bAssumeUtf8);
		HRESULT            Parse(const CAtlString& _raw_text);
		const
		TRegistryElements& ParsedContent(void)const;
	};
}}}

#endif/*_SHAREDREGISTRYPARSER_H_5CBDBAE7_A31C_460f_ADC3_4DEA0DBC8865_INCLUDED*/