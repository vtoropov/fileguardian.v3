/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Aug-2017 at 7:36:54p, UTC+7, Phuket, Rawai, Friday;
	This is Shared MS Windows registry script file parser interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 0:39:31a, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "Shared_RegistryParser.h"

using namespace shared::registry;
using namespace shared::registry::_impl;
using namespace shared::lite::data;

#define FILE_HEADER_V4  _T("REGEDIT4")
#define FILE_HEADER_V5  _T("Windows Registry Editor Version 5.00")

#define VALUE_DWORD_PFX       _T("dword:")
#define VALUE_TEXT_EXPAND_PFX _T("hex(2):")
#define VALUE_TEXT_MULTI_PFX  _T("hex(7):")

typedef ::std::vector<BYTE> TByteArray;
/////////////////////////////////////////////////////////////////////////////
/*
 +覧覧覧覧覧覧�+覧覧覧覧覧覧覧覧�+
 | File entry  | Registry type   |
 +覧覧覧覧覧覧�+覧覧覧覧覧覧覧覧�+
 | hex         | REG_BINARY      |
 +覧覧覧覧覧覧�+覧覧覧覧覧覧覧覧�+
 | dword       | REG_DWORD       |
 +覧覧覧覧覧覧�+覧覧覧覧覧覧覧覧�+
 | hex(2)      | REG_EXPAND_SZ   |
 +覧覧覧覧覧覧�+覧覧覧覧覧覧覧覧�+
 | hex(7)      | REG_MULTI_SZ    |
 +覧覧覧覧覧覧�+覧覧覧覧覧覧覧覧�+
 | none        | REG_SZ          |
 +覧覧覧覧覧覧�+覧覧覧覧覧覧覧覧�+
*/
namespace shared { namespace registry { namespace _impl { namespace details
{
	CAtlString RegistryParser_EntryToName  (const CAtlString& _entry)
	{
		CAtlString normalized_(_entry);

		normalized_.Replace(_T("\""), _T(""));

		return normalized_;
	}

	VOID       RegistryParser_ArrToRawData (const TByteArray& _array  , CRawData& _data)
	{
		if (!_array.empty()){

			_data.Create((DWORD)_array.size());
			if (_data.IsValid()) {

				for (size_t i_ = 0; i_ < _array.size() && i_ < (size_t)_data.GetSize(); i_++){
					_data.GetData()[i_] = _array[i_];
				}
			}
		}
	}

	VOID       RegistryParser_SectionToPath(const CAtlString& _section, CRegistryPath& _path)
	{
		CAtlString normalized_(_section);

		normalized_.Replace(_T("["), _T(""));
		normalized_.Replace(_T("]"), _T(""));

		const bool bHasRoot = (0 == normalized_.Find(_T("HKEY_")));
		if (!bHasRoot){
			_path.Folder(normalized_);
			return;
		}
		if (false){}
		else if (0 == normalized_.Find(_T("HKEY_LOCAL_MACHINE"))) _path.Root(HKEY_LOCAL_MACHINE);
		else if (0 == normalized_.Find(_T("HKEY_CLASSES_ROOT" ))) _path.Root(HKEY_CLASSES_ROOT );
		else if (0 == normalized_.Find(_T("HKEY_CURRENT_USER" ))) _path.Root(HKEY_CURRENT_USER );
		else if (0 == normalized_.Find(_T("HKEY_USERS")))         _path.Root(HKEY_USERS);

		// cuts off the root element name from the registry path;
		const INT n_pos = normalized_.Find(_T("\\"));
		if (-1 != n_pos)
			normalized_ = normalized_.Right(normalized_.GetLength() - n_pos - 1);

		_path.Folder(normalized_);
	}

	VOID       RegistryParser_SplitHexToArr(const CAtlString& _value  , TByteArray& _array)
	{
		INT n_pos = 0;
		CAtlString cs_byte = _value.Tokenize(_T(","), n_pos);

		while (!cs_byte.IsEmpty())
		{
			const BYTE byte_ = (BYTE)::_tcstol(cs_byte, NULL, 16);
			_array.push_back(byte_);

			cs_byte = _value.Tokenize(_T(","), n_pos);
		}
	}

	VOID       RegistryParser_ValueToValue (const CAtlString& _value  , CRegistryValue& _named)
	{
		if (_value.IsEmpty())
			return;
		CAtlString normalized_(_value);
		normalized_.Replace(_T("\""), _T(""));
		normalized_.Trim();

		CRawData& raw_data = _named.RawData();

		if (false){}
		else if (0 == normalized_.Find(VALUE_DWORD_PFX)){

			_named.Type(REG_DWORD);

			normalized_ = normalized_.Right(normalized_.GetLength() - static_cast<INT>(::_tcslen(VALUE_DWORD_PFX)));
			const DWORD dwValue = static_cast<DWORD>(::_tstol(normalized_.GetString()));

			raw_data.Create(sizeof(DWORD));
			if (raw_data.IsValid()){

				::memcpy_s(raw_data.GetData(), raw_data.GetSize(), &dwValue, sizeof(DWORD)); 
			}

		}
		else if (0 == normalized_.Find(VALUE_TEXT_EXPAND_PFX)){

			_named.Type(REG_EXPAND_SZ);
			normalized_ = normalized_.Right(normalized_.GetLength() - static_cast<INT>(::_tcslen(VALUE_TEXT_EXPAND_PFX)));

			TByteArray array_;

			RegistryParser_SplitHexToArr(normalized_, array_);
			RegistryParser_ArrToRawData (array_, raw_data);
		}
		else if (0 == normalized_.Find(VALUE_TEXT_MULTI_PFX)){

			_named.Type(REG_MULTI_SZ);
			normalized_ = normalized_.Right(normalized_.GetLength() - static_cast<INT>(::_tcslen(VALUE_TEXT_MULTI_PFX)));

			TByteArray array_;

			RegistryParser_SplitHexToArr(normalized_, array_);
			RegistryParser_ArrToRawData (array_, raw_data);
		}
		else { // REG_SZ by default;
			_named.Type(REG_SZ);
			raw_data.Create(
					reinterpret_cast<PBYTE>(normalized_.GetBuffer()),
					static_cast<DWORD>(normalized_.GetLength() * sizeof(TCHAR)) // zero terminator is not copied!
				);
		}
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CRegistryPath::CRegistryPath(void) : m_root(NULL)
{
}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR            CRegistryPath::Folder(void)const
{
	return m_folder.GetString();
}

VOID               CRegistryPath::Folder(LPCTSTR _folder)
{
	m_folder = _folder;
}

VOID               CRegistryPath::Init(const HKEY _root, LPCTSTR lpszFolder)
{
	m_root = _root;
	m_folder = lpszFolder;
}

HKEY               CRegistryPath::Root(void)const
{
	return m_root;
}

VOID               CRegistryPath::Root(const HKEY _root)
{
	m_root = _root;
}

/////////////////////////////////////////////////////////////////////////////

CRegistryValue::CRegistryValue(void) : m_type(REG_NONE)
{
}

/////////////////////////////////////////////////////////////////////////////

bool               CRegistryValue::IsUnicode(void)const
{
	bool bUnicode = false;

	if (!m_raw_buffer.IsValid())
		return bUnicode;

	switch (m_type)
	{
	case REG_SZ:
		{
			bUnicode = !!::IsTextUnicode(
							m_raw_buffer.GetData(), (INT)m_raw_buffer.GetSize(), NULL
						);
		} break;
	case REG_EXPAND_SZ:
		{
			const DWORD dwSize = m_raw_buffer.GetSize();
			//
			// 0-terminator fills at least 2 bytes for unicode string; these types of registry value include 0-terminator;
			//
			if (1 < dwSize){
				bUnicode = (
						0 == m_raw_buffer.GetData()[dwSize - 1] &&
						0 == m_raw_buffer.GetData()[dwSize - 2]
					);
			}
		} break;
	case REG_MULTI_SZ :
		{
			const DWORD dwSize = m_raw_buffer.GetSize();
			//
			// double 0-terminator fills at least 4 bytes for unicode string;
			//
			if (3 < dwSize){
				bUnicode = (
						0 == m_raw_buffer.GetData()[dwSize - 1] &&
						0 == m_raw_buffer.GetData()[dwSize - 2] &&
						0 == m_raw_buffer.GetData()[dwSize - 3] &&
						0 == m_raw_buffer.GetData()[dwSize - 4]
					);
			}
		} break;
	default:;
	}

	return bUnicode;
}

LPCTSTR            CRegistryValue::Name(void)const
{
	return m_name.GetString();
}

VOID               CRegistryValue::Name(LPCTSTR _name)
{
	m_name = _name;
}

const
CRawData&          CRegistryValue::RawData(void)const
{
	return m_raw_buffer;
}

CRawData&          CRegistryValue::RawData(void)
{
	return m_raw_buffer;
}

DWORD              CRegistryValue::Type(void)const
{
	return m_type;
}

VOID               CRegistryValue::Type(const DWORD _type)
{
	m_type = _type;
}

DWORD              CRegistryValue::ValueAsDword(void)const
{
	const
	DWORD dwRequired = sizeof(DWORD);
	DWORD dwValue    = 0;

	if (!m_raw_buffer.IsValid())
		return dwValue;

	switch(m_type)
	{
	case REG_DWORD:
		{
			if (m_raw_buffer.GetSize() >= dwRequired)
			::memcpy_s(
				&dwValue, sizeof(DWORD), m_raw_buffer.GetData(), dwRequired
			);
		} break;
	default:;
	}
	return dwValue;
}

TMultiString       CRegistryValue::ValueAsMultiString(void)const
{
	TMultiString multi_;

	if (!m_raw_buffer.IsValid())
		return multi_;

	switch(m_type)
	{
	case REG_MULTI_SZ:
		{
			const bool bUnicode = this->IsUnicode();
			if (bUnicode) {

				CAtlString cs_value_w;
				// iterates till the first closing terminator at the end of the buffer (the tail terminator is doubled)
				for (DWORD i_ = 0; i_ < m_raw_buffer.GetSize() - sizeof(WCHAR); i_ += sizeof(WCHAR)){

					const INT n_iter_ = i_ * sizeof(WCHAR);

					WCHAR wchar_ = NULL;
					::memcpy_s(
							&wchar_, sizeof(WCHAR), &m_raw_buffer.GetData()[n_iter_], sizeof(WCHAR)
						);
					if (wchar_){
						cs_value_w.Append(&wchar_, 1);
					}
					else {
						multi_.push_back(cs_value_w);
						cs_value_w.Empty();
					}
				}
			}
			else {

				CAtlStringA cs_value_a;
				// iterates till the first closing terminator at the end of the buffer (the tail terminator is doubled)
				for (DWORD i_ = 0; i_ < m_raw_buffer.GetSize() - sizeof(CHAR) ; i_ += sizeof(CHAR)){

					const INT n_iter_ = i_ * sizeof(CHAR);

					CHAR char_ = (CHAR)m_raw_buffer.GetData()[n_iter_];
					
					if (char_){
						cs_value_a.Append(&char_, 1);
					}
					else {
						CAtlString cs_value_w = cs_value_a.GetString();

						multi_.push_back(cs_value_w);
						cs_value_a.Empty();
					}
				}
			}
		} break;
	default:;
	}

	return multi_;
}

CAtlString         CRegistryValue::ValueAsText(void)const
{
	CAtlString cs_value;

	if (!m_raw_buffer.IsValid())
		return cs_value;

	switch(m_type)
	{
	case REG_SZ:
	case REG_EXPAND_SZ:
		{
			const bool bUnicode = this->IsUnicode();

			if (!bUnicode){
				CAtlStringA cs_utf8;
				if (SUCCEEDED(m_raw_buffer.ToStringUtf8(cs_utf8)))
					cs_value = cs_utf8;
			}
			else {
				m_raw_buffer.ToStringUtf16(cs_value);
			}
		} break;
	case REG_MULTI_SZ:
		{
			TMultiString multi_ =  this->ValueAsMultiString();
			for (size_t i_ = 0; i_ < multi_.size(); i_++)
			{
				cs_value += multi_[i_];
			}
		} break;
	default:;
	}
	return cs_value;
}

CAtlString         CRegistryValue::ValueAsTextExpanded(void)const
{
	CAtlString cs_value;

	switch (m_type)
	{
	case REG_EXPAND_SZ:
		{
			cs_value = this->ValueAsText();

			const INT n_pos = cs_value.ReverseFind(_T('%'));
			if (n_pos > 1)
			{
				CAtlString cs_env = cs_value.Mid(0, n_pos + 1);

				CRawData raw_buffer(sizeof(TCHAR));
				if (raw_buffer.IsValid())
				{
					const INT nRequired = ::ExpandEnvironmentStrings(
												cs_env.GetString(), (PTCHAR)raw_buffer.GetData(), raw_buffer.GetSize()/sizeof(TCHAR)
											);
					if (0 < nRequired)
					{
						raw_buffer.Create(nRequired * sizeof(TCHAR));
						if (raw_buffer.IsValid()){
							if (::ExpandEnvironmentStrings(
												cs_env.GetString(), (PTCHAR)raw_buffer.GetData(), nRequired
											)) {
								raw_buffer.ToStringUtf16(cs_value);
							}
						}
					}
				}
			}
		} break;
	default:;
	}
	return cs_value;
}

/////////////////////////////////////////////////////////////////////////////

CRegistryFileElement::CRegistryFileElement(void)
{
}

CRegistryFileElement::~CRegistryFileElement(void)
{
}

/////////////////////////////////////////////////////////////////////////////

const
CRegistryValue&    CRegistryFileElement::NamedValue(void)const
{
	return m_value;
}

CRegistryValue&    CRegistryFileElement::NamedValue(void)
{
	return m_value;
}

const
CRegistryPath&     CRegistryFileElement::Path(void)const
{
	return m_path;
}

CRegistryPath&     CRegistryFileElement::Path(void)
{
	return m_path;
}

/////////////////////////////////////////////////////////////////////////////

CRegistryFileParser::CRegistryFileParser(void)
{
	m_error.Source(_T("CRegistryFileParser"));
}

CRegistryFileParser::~CRegistryFileParser(void)
{
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef          CRegistryFileParser::Error(void) const
{
	return m_error;
}

HRESULT            CRegistryFileParser::Parse(const CAtlString& _raw_text)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (_raw_text.IsEmpty())
		return (m_error = E_INVALIDARG);

	CAtlString cs_buffer(_raw_text);
	//
	// removes the header of the file, like REGEDIT4/REGEDIT5;
	// the header does not allow to treat the file content as INI data;
	//
	if (-1 != cs_buffer.Find(FILE_HEADER_V4)) cs_buffer.Replace(FILE_HEADER_V4, _T(""));
	if (-1 != cs_buffer.Find(FILE_HEADER_V5)) cs_buffer.Replace(FILE_HEADER_V5, _T(""));

	cs_buffer.Trim();

	if (m_elements.empty() == false)
		m_elements.clear();

	HRESULT hr_ = m_preliminary_parser.CreateFromText(cs_buffer);
	if (FAILED(hr_))
		return (m_error = hr_);

	const INT nSections = m_preliminary_parser.SectionCount();
	for ( INT i_ = 0; i_ < nSections; i_++){

		const CPrivateProfileSection& section_ = m_preliminary_parser.SectionOf(i_);

		CRegistryPath path_;
		details::RegistryParser_SectionToPath(section_.Name(), path_);

		const INT nItems = section_.ItemCount();

		for (INT j_ = 0; j_ < nItems; j_++){

			const CPrivateProfileItem& item_ = section_.ItemOf(j_);

			CAtlString cs_name  = details::RegistryParser_EntryToName(item_.Name());

			CRegistryFileElement element_;
			element_.Path() = path_;
			element_.NamedValue().Name(cs_name);

			details::RegistryParser_ValueToValue(item_.Value(), element_.NamedValue());

			try{
				m_elements.push_back(element_);
			}
			catch(std::bad_alloc&)
			{
				return (m_error = E_OUTOFMEMORY);
			}
		}
	}

	return m_error;
}

const
TRegistryElements& CRegistryFileParser::ParsedContent(void)const
{
	return m_elements;
}