/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jun-2018 at 7:02:46p, UTC+7, Phuket, Rawai, Friday;
	This is Shared Library registry dword data related interface implementation file.
*/
#include "StdAfx.h"
#include "Shared_Registry.Dword.h"

using namespace shared::registry;

/////////////////////////////////////////////////////////////////////////////

CRegistryDword::CRegistryDword(const CRegistryBase& _base)  : TBase(_base.Root(), _T("CRegistryDword")){
	this->m_options.Value(_base.Options().Value());
}
CRegistryDword::CRegistryDword(const HKEY hRoot, const DWORD dwOptions) : TBase(hRoot, _T("CRegistryDword")){
	this->m_options.Value(dwOptions);
}
CRegistryDword::~CRegistryDword(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CRegistryDword::Read  (LPCTSTR lpszFolder, LPCTSTR lpszValueName, DWORD& _out_value, const DWORD _default)const {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	_out_value = _default;

	::ATL::CRegKey  reg_key;

	HRESULT hr_ = TBase::_OpenKey(reg_key, lpszFolder, lpszValueName);
	if (FAILED(hr_))
		return hr_;

	const LONG nLastResult = reg_key.QueryDWORDValue(lpszValueName, _out_value);
	hr_ = HRESULT_FROM_WIN32(nLastResult);
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}

HRESULT     CRegistryDword::Write (LPCTSTR lpszFolder, LPCTSTR lpszValueName, const DWORD _in_value)const {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	::ATL::CRegKey  reg_key;

	HRESULT hr_ = TBase::_OpenKey(reg_key, lpszFolder, lpszValueName);
	if (FAILED(hr_))
		return hr_;

	const LONG nLastResult = reg_key.SetDWORDValue(lpszValueName, _in_value);
	hr_ = HRESULT_FROM_WIN32(nLastResult);
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CRegistryDword& CRegistryDword::operator= (const CRegistryBase& _base) {

	this->m_root = _base.Root();
	this->m_options.Value(_base.Options().Value());

	return *this;
}