/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jun-2018 at 5:11:43p, UTC+7, Phuket, Rawai, Friday;
	This is Shared Library registry entry enumerator interface implementation file.
*/
#include "StdAfx.h"
#include "Shared_Registry.Enum.h"

using namespace shared::registry;

/////////////////////////////////////////////////////////////////////////////

CRegistryEnumerator::CRegistryEnumerator(const HKEY hRoot) : TBase(hRoot, _T("CRegistryEnumerator")) { }
CRegistryEnumerator::~CRegistryEnumerator(void) { }

/////////////////////////////////////////////////////////////////////////////

TRegistryKeyEnum
CRegistryEnumerator::Enumerate(LPCTSTR lpszFolder)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	TRegistryKeyEnum enum_;

	CRegistryArgs args_(m_options);

	if (false == args_.IsFolderName(lpszFolder)) {
		m_error.SetState(
			E_INVALIDARG,
			_T("Folder name cannot be empty.")
		);
		return enum_;
	}

	::ATL::CAtlString qry__ = args_.Format(lpszFolder);

	HKEY reg_ = NULL;
	LSTATUS lResult = ::RegOpenKey(TBase::m_root, qry__, &reg_);

	m_error = __HRESULT_FROM_WIN32(static_cast<DWORD>(lResult));
	if (m_error)
		return enum_;

	DWORD dwFolders = 0;
	lResult = ::RegQueryInfoKey(
		reg_,
		NULL,
		NULL,
		NULL,
		&dwFolders,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL
	);
	m_error = __HRESULT_FROM_WIN32(static_cast<DWORD>(lResult));
	if (!m_error)
	{
		for (DWORD i_ = 0; i_ < dwFolders; i_++)
		{
			TCHAR    pszKey[_MAX_PATH]   = {0};
			DWORD    cbName              =  _countof(pszKey);
			FILETIME ftLastWriteTime     = {0};

			lResult = ::RegEnumKeyEx(
				reg_,
				i_,
				pszKey,
				&cbName,
				NULL,
				NULL,
				NULL,
				&ftLastWriteTime
			);
			m_error = __HRESULT_FROM_WIN32(static_cast<DWORD>(lResult));
			if (m_error)
				break;
			try
			{
				CAtlString cs_key(pszKey);
				enum_.push_back(cs_key);
			}
			catch(::std::bad_alloc&){ m_error = E_OUTOFMEMORY; break; }
		}
	}
	::RegCloseKey(reg_); reg_ = NULL;

	return enum_;
}