#ifndef _SHAREDREGSTDAFX_H_D8061509_9F1B_4e4d_ABA3_B26B8082FFB6_INCLUDED
#define _SHAREDREGSTDAFX_H_D8061509_9F1B_4e4d_ABA3_B26B8082FFB6_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Aug-2017 at 2:56:48pm, GMT+7, Phuket, Rawai, Friday;
	This is Shared MS Windows Registry Wrapper Library precompiled headers declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 0:44:24a, UTC+7, Phuket, Rawai, Monday;
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600 // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600 // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700 // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>
#include <vector>
#include <map>

#endif/*_SHAREDREGSTDAFX_H_D8061509_9F1B_4e4d_ABA3_B26B8082FFB6_INCLUDED*/