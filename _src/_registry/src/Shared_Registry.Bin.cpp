/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jun-2018 at 3:02:35p, UTC+7, Phuket, Rawai, Friday;
	This is Shared Library registry binary data related interface implementation file.
*/
#include "StdAfx.h"
#include "Shared_Registry.Bin.h"

using namespace shared::registry;

/////////////////////////////////////////////////////////////////////////////

CRegistryBin::CRegistryBin(const HKEY hRoot) : TBase(hRoot, _T("CRegistryBin")){}
CRegistryBin::~CRegistryBin(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CRegistryBin::Read  (LPCTSTR lpszFolder, LPCTSTR lpszValueName, PBYTE& _out_value, ULONG& _out_size)const {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	::ATL::CRegKey  reg_key;

	HRESULT hr_ = TBase::_OpenKey(reg_key, lpszFolder, lpszValueName);
	if (FAILED(hr_))
		return hr_;

	const LONG nLastResult = reg_key.QueryBinaryValue(lpszValueName, _out_value, &_out_size);
	hr_ = HRESULT_FROM_WIN32(nLastResult);
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}

HRESULT     CRegistryBin::Write (LPCTSTR lpszFolder, LPCTSTR lpszValueName, const PBYTE _in__value, const ULONG _in_size)const {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	::ATL::CRegKey  reg_key;

	HRESULT hr_ = TBase::_OpenKey(reg_key, lpszFolder, lpszValueName);
	if (FAILED(hr_))
		return hr_;

	const LONG nLastResult = reg_key.SetBinaryValue(lpszValueName, _in__value, _in_size);
	hr_ = HRESULT_FROM_WIN32(nLastResult);
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}