#ifndef _SHAREDLITEWMISERVICEWRAP_H_C860125D_DBC8_435e_9638_AC0A50E7C000_INCLUDED
#define _SHAREDLITEWMISERVICEWRAP_H_C860125D_DBC8_435e_9638_AC0A50E7C000_INCLUDED
/*
	Created by Tech_dog (VToropov) on 16-Feb-2015 at 5:14:48am, GMT+3, Taganrog, Monday;
	This is shared lite library WMI service provider wrapper class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 6:36:31p, UTC+7, Phuket, Rawai, Monday;
*/
#include <Wbemidl.h>
#pragma comment(lib, "wbemuuid.lib")

#include "Shared_SystemError.h"

namespace shared { namespace lite { namespace data
{
	using shared::lite::common::CSysError;
	using shared::lite::common::CLangId;

	typedef ::ATL::CComPtr<IWbemClassObject>     TPropertyProvider;
	typedef ::ATL::CComPtr<IEnumWbemClassObject> TObjectEnumerator;

	class CWMIserviceError : public CSysError
	{
		typedef CSysError TBaseError;
	public:
		explicit CWMIserviceError(LPCTSTR pszModule = NULL);
		explicit CWMIserviceError(const DWORD dwError, const CLangId& = CLangId());
		explicit CWMIserviceError(const HRESULT hError, const CLangId& = CLangId());
	public:
		virtual void         SetHresult(const HRESULT) override sealed;
	public:
		CWMIserviceError&    operator= (const DWORD);
		CWMIserviceError&    operator= (const HRESULT);
		CWMIserviceError&    operator= (const CSysError&);
	public:
		static bool          IsServiceError(const HRESULT);
	};

	class CWMI_BaseServiceWrap
	{
	protected:
		mutable CWMIserviceError   m_error;
		mutable TPropertyProvider  m_provider;
	public:
		CWMI_BaseServiceWrap(void);
		~CWMI_BaseServiceWrap(void);
	public:
		TErrorRef           Error(void)const;
		_variant_t          GetProperty(LPCTSTR pszName) const;
	};

	class CWMI_HardwareProvider : public CWMI_BaseServiceWrap
	{
	public:
		CWMI_HardwareProvider(void);
		~CWMI_HardwareProvider(void);
	};

	class CWMI_NetworkProvider : public CWMI_BaseServiceWrap
	{
		typedef CWMI_BaseServiceWrap TBase;
	private:
		mutable TPropertyProvider  m_net_cfg;
	public:
		CWMI_NetworkProvider(LPCTSTR lpszEnsureProperty = NULL);
		~CWMI_NetworkProvider(void);
	public:
		CAtlString         GetLocalHost(const bool _tcp_v6 = false)const;
		CAtlString         GetMacAddress(void)const;
	};

	class CWMI_ProcessorProvider : public CWMI_BaseServiceWrap
	{
		typedef CWMI_BaseServiceWrap TBase;
	public:
		CWMI_ProcessorProvider(void);
		~CWMI_ProcessorProvider(void);
	public:
		CAtlString         GetIdentifier(void)const;   //it seems that this property is supported from Windows 10 only
		CAtlString         GetSerialNumber(void)const; //it seems that this property is supported from Windows 10 only
	};

	class CWMI_SecurityProvider : public CWMI_BaseServiceWrap
	{
	public:
		CWMI_SecurityProvider(void);
		~CWMI_SecurityProvider(void);
	};

	class CWMI_Win32Provider : public CWMI_BaseServiceWrap
	{
	public:
		CWMI_Win32Provider(void);
		~CWMI_Win32Provider(void);
	};

	typedef ::std::vector<TPropertyProvider>     TProperties;

	class CWMIsecurityEnum
	{
	private:
		CWMIserviceError    m_error;
		TProperties         m_props;
	public:
		CWMIsecurityEnum(void);
		~CWMIsecurityEnum(void);
	public:
		TErrorRef           Error(void)const;
		const TProperties&  GetProperties(void) const;
	};
}}}

#endif/*_SHAREDLITEWMISERVICEWRAP_H_C860125D_DBC8_435e_9638_AC0A50E7C000_INCLUDED*/