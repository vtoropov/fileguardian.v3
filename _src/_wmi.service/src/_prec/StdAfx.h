#ifndef _WMISERVICELIBRARYPRECOMPILEDHEADERS_H_094BF254_1C76_eeee_96F9_11A826EFC206_INCLUDED
#define _WMISERVICELIBRARYPRECOMPILEDHEADERS_H_094BF254_1C76_eeee_96F9_11A826EFC206_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-May-2018 at 6:32:56p, UTC+7, Phuket, Rawai, Wednesday; 
	This is shared WMI service library precompiled header definition file.
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600 // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600 // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700 // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481) // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996) // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <atlconv.h>
#include <comdef.h>
#include <comutil.h>
#include <map>
#include <vector>

#define __deprecated
#ifdef  __deprecated

#include "atlapp.h"

#endif

#endif/*_WMISERVICELIBRARYPRECOMPILEDHEADERS_H_094BF254_1C76_eeee_96F9_11A826EFC206_INCLUDED*/