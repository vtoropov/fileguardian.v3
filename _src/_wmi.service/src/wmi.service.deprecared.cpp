/*
	Created by Tech_dog (VToropov) on 16-Feb-2015 at 5:39:03am, GMT+3, Taganrog, Monday;
	This is shared lite library WMI service provider wrapper class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 6:39:20p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "wmi.service.deprecated.h"

using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace data { namespace details
{
	using shared::lite::common::CLangId;

	CAtlString WMIserviceWrap_FormatMessage(const HRESULT hError, const CLangId& lang_id)
	{
		TCHAR szBuffer[_MAX_PATH] = {0};

		HMODULE hModule = ::LoadLibrary(_T("wmiutils.dll"));

		::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_FROM_HMODULE,
						hModule,
						hError,
						MAKELANGID(lang_id.dwPrimary, lang_id.dwSecond),
						szBuffer,
						_MAX_PATH - 1,
						NULL);
		::ATL::CAtlString msg_(szBuffer);
		msg_.Format(_T("WMI: %s"), szBuffer);
		if (hModule)
			::FreeLibrary(hModule);
		return msg_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CWMIserviceError::CWMIserviceError(LPCTSTR pszModule) : TBaseError(pszModule)
{
}

CWMIserviceError::CWMIserviceError(const DWORD dwError, const CLangId& lang_id) : TBaseError(dwError, lang_id)
{
	TBaseError::m_buffer = details::WMIserviceWrap_FormatMessage(dwError, lang_id);
}

CWMIserviceError::CWMIserviceError(const HRESULT hError, const CLangId& lang_id) : TBaseError(hError, lang_id)
{
	this->SetHresult(hError);
}

/////////////////////////////////////////////////////////////////////////////

void         CWMIserviceError::SetHresult(const HRESULT hResult)
{
	TBaseError::SetHresult(hResult);
	if (TBaseError::m_buffer.IsEmpty())
		TBaseError::m_buffer = details::WMIserviceWrap_FormatMessage(hResult, m_lng_id);
}

/////////////////////////////////////////////////////////////////////////////

CWMIserviceError&    CWMIserviceError::operator= (const DWORD dwError)
{
	*((CSysError*)this) = dwError;
	return *this;
}

CWMIserviceError&    CWMIserviceError::operator= (const HRESULT hError)
{
	this->SetHresult(hError);
	return *this;
}

CWMIserviceError&    CWMIserviceError::operator= (const CSysError& err_ref)
{
	*((CSysError*)this) = err_ref;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

bool         CWMIserviceError::IsServiceError(const HRESULT hError)
{
	return (WBEM_E_FAILED <= hError && hError <= WBEM_E_PROVIDER_DISABLED)
		|| (WBEMESS_E_REGISTRATION_TOO_BROAD <= hError && hError <= WBEMMOF_E_INVALID_DELETECLASS_SYNTAX);
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace data { namespace details
{
	namespace wmi_root
	{
		static LPCTSTR pszRoot_cim_v2    = _T("ROOT\\CIMV2");             // common information model, version 2
		static LPCTSTR pszRoot_security  = _T("ROOT\\SecurityCenter");    // stores installed antivirus information (XP)
		static LPCTSTR pszRoot_security2 = _T("ROOT\\SecurityCenter2");   // stores installed antivirus information (VISTA)
	}

	typedef ::ATL::CComPtr<IWbemServices>     TSystemInfoService;

	HRESULT  WMIserviceWrap_InitCore    (TSystemInfoService& pService, LPCTSTR pszRoot)
	{
		if (!pszRoot || !::_tcslen(pszRoot))
			return E_INVALIDARG;
		HRESULT hr_ = S_OK;
		::ATL::CComPtr<IWbemLocator> pLocator;
		hr_ = ::CoCreateInstance(
					CLSID_WbemLocator,
					NULL,
					CLSCTX_INPROC_SERVER,
					IID_IWbemLocator, (LPVOID*) &pLocator);
		if (FAILED(hr_))
			return hr_;

		hr_ = pLocator->ConnectServer(
					_bstr_t(pszRoot),
					NULL,
					NULL,
					NULL,
					0,
					0,
					NULL,
					&pService
					);
		if (FAILED(hr_))
			return hr_;

		hr_ = ::CoSetProxyBlanket(
					pService,
					RPC_C_AUTHN_WINNT,
					RPC_C_AUTHZ_NONE,
					NULL,
					RPC_C_AUTHN_LEVEL_CALL,
					RPC_C_IMP_LEVEL_IMPERSONATE,
					NULL,
					EOAC_NONE
					);
		return hr_;
	}

	HRESULT  WMIserviceWrap_InitCpu     (TPropertyProvider&  pProvider)
	{
		TSystemInfoService pService;
		HRESULT hr_ = WMIserviceWrap_InitCore(pService, wmi_root::pszRoot_cim_v2);
		if (FAILED(hr_))
			return hr_;
		::ATL::CComPtr<IEnumWbemClassObject> pEnumerator;
		hr_ = pService->ExecQuery(
					bstr_t(_T("WQL")), 
					bstr_t(_T("SELECT * FROM Win32_Processor")),
					WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY, 
					NULL,
					&pEnumerator);
		if (FAILED(hr_))
			return hr_;
		ULONG uReturn = 0;
		hr_ = pEnumerator->Next(WBEM_INFINITE, 1, &pProvider, &uReturn);
		return hr_;
	}

	HRESULT  WMIserviceWrap_InitHardware(TPropertyProvider&  pProvider)
	{
		TSystemInfoService pService;
		HRESULT hr_ = WMIserviceWrap_InitCore(pService, wmi_root::pszRoot_cim_v2);
		if (FAILED(hr_))
			return hr_;
		::ATL::CComPtr<IEnumWbemClassObject> pEnumerator;
		hr_ = pService->ExecQuery(
					bstr_t(_T("WQL")), 
					bstr_t(_T("SELECT * FROM Win32_ComputerSystem")),
					WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY, 
					NULL,
					&pEnumerator);
		if (FAILED(hr_))
			return hr_;
		ULONG uReturn = 0;
		hr_ = pEnumerator->Next(WBEM_INFINITE, 1, &pProvider, &uReturn);
#if defined(_9034E0F3_A949_4180_B8F2_750E0484E910_)
		if (SUCCEEDED(hr_))
			details::WMIserviceWrap_EnumerateProperties(pProvider);
#endif
		return hr_;
	}

	HRESULT  WMIserviceWrap_InitNetwork (TPropertyProvider&  pProvider, LPCTSTR lpszEnsureProperty)
	{
		TSystemInfoService pService;
		HRESULT hr_ = WMIserviceWrap_InitCore(pService, wmi_root::pszRoot_cim_v2);
		if (FAILED(hr_))
			return hr_;
		::ATL::CComPtr<IEnumWbemClassObject> pEnumerator;
		hr_ = pService->ExecQuery(
					bstr_t(_T("WQL")), 
					bstr_t(_T("SELECT * FROM Win32_NetworkAdapter")),
					WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY, 
					NULL,
					&pEnumerator);
		if (FAILED(hr_))
			return hr_;
		CAtlString cs_ensure(lpszEnsureProperty);

		ULONG uReturn = 0;
		hr_ = pEnumerator->Next(WBEM_INFINITE, 1, &pProvider, &uReturn);
		while (!cs_ensure.IsEmpty() && uReturn > 0)
		{
			_variant_t vProperty;
			pProvider->Get(cs_ensure, 0, &vProperty, 0, 0);
			if (VT_BSTR == vProperty.vt)
				break;
			pProvider = NULL;
			hr_ = pEnumerator->Next(WBEM_INFINITE, 1, &pProvider, &uReturn);
		}
		return hr_;
	}

	HRESULT  WMIserviceWrap_InitNetworkCfg(TPropertyProvider&  pProvider)
	{
		TSystemInfoService pService;
		HRESULT hr_ = WMIserviceWrap_InitCore(pService, wmi_root::pszRoot_cim_v2);
		if (FAILED(hr_))
			return hr_;
		::ATL::CComPtr<IEnumWbemClassObject> pEnumerator;
		hr_ = pService->ExecQuery(
					bstr_t(_T("WQL")), 
					bstr_t(_T("SELECT * FROM Win32_NetworkAdapterConfiguration")),
					WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY, 
					NULL,
					&pEnumerator);
		if (FAILED(hr_))
			return hr_;
		ULONG uReturn = 0;
		hr_ = pEnumerator->Next(WBEM_INFINITE, 1, &pProvider, &uReturn);
		return hr_;
	}

	HRESULT  WMIserviceWrap_InitSecurity(TObjectEnumerator&  pEnumObject)
	{
		TSystemInfoService pService;
		static LPCTSTR pszRoot = (::WTL::RunTimeHelper::IsVista() ? 
									wmi_root::pszRoot_security2 : wmi_root::pszRoot_security);
		HRESULT hr_ = WMIserviceWrap_InitCore(pService, pszRoot);
		if (FAILED(hr_))
			return hr_;
		::ATL::CComPtr<IEnumWbemClassObject> pEnumerator;
		hr_ = pService->ExecQuery(
					bstr_t(_T("WQL")), 
					bstr_t(_T("SELECT * FROM AntiVirusProduct")),
					WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY, 
					NULL,
					&pEnumObject);
		return hr_;
	}

	HRESULT  WMIserviceWrap_InitSecurity(TPropertyProvider&  pProvider)
	{
		TObjectEnumerator pEnumObject;
		HRESULT hr_ = WMIserviceWrap_InitSecurity(pEnumObject);
		if (FAILED(hr_))
			return hr_;
		ULONG uReturn = 0;
		hr_ = pEnumObject->Next(WBEM_INFINITE, 1, &pProvider, &uReturn);
		return hr_;
	}

	HRESULT  WMIserviceWrap_InitWin32   (TPropertyProvider&  pProvider)
	{
		TSystemInfoService pService;
		HRESULT hr_ = WMIserviceWrap_InitCore(pService, wmi_root::pszRoot_cim_v2);
		if (FAILED(hr_))
			return hr_;
		::ATL::CComPtr<IEnumWbemClassObject> pEnumerator;
		hr_ = pService->ExecQuery(
					bstr_t(_T("WQL")), 
					bstr_t(_T("SELECT * FROM Win32_OperatingSystem")),
					WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY, 
					NULL,
					&pEnumerator);
		if (FAILED(hr_))
			return hr_;
		ULONG uReturn = 0;
		hr_ = pEnumerator->Next(WBEM_INFINITE, 1, &pProvider, &uReturn);
		return hr_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CWMI_BaseServiceWrap::CWMI_BaseServiceWrap(void)
{
	m_error.Source(_T("WMI Service"));
}

CWMI_BaseServiceWrap::~CWMI_BaseServiceWrap(void)
{
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef    CWMI_BaseServiceWrap::Error(void)const
{
	return m_error;
}

_variant_t   CWMI_BaseServiceWrap::GetProperty(LPCTSTR pszName) const
{
	_variant_t vProperty;
	if (!m_provider)
	{
		m_error = OLE_E_BLANK;
		return vProperty;
	}
	if (!pszName || !::_tcslen(pszName))
	{
		m_error = E_INVALIDARG;
		return vProperty;
	}
	m_error = m_provider->Get(pszName, 0, &vProperty, 0, 0);
	if (m_error)vProperty.Clear();
	return vProperty;
}

/////////////////////////////////////////////////////////////////////////////

CWMI_HardwareProvider::CWMI_HardwareProvider(void)
{
	m_error = details::WMIserviceWrap_InitHardware(m_provider);
}

CWMI_HardwareProvider::~CWMI_HardwareProvider(void)
{
}

/////////////////////////////////////////////////////////////////////////////

CWMI_NetworkProvider::CWMI_NetworkProvider(LPCTSTR lpszEnsureProperty)
{
	m_error = details::WMIserviceWrap_InitNetwork(m_provider, lpszEnsureProperty);
}

CWMI_NetworkProvider::~CWMI_NetworkProvider(void)
{
}

/////////////////////////////////////////////////////////////////////////////

CAtlString   CWMI_NetworkProvider::GetLocalHost(const bool _tcp_v6)const
{
	CAtlString cs_ip;
	if (!m_net_cfg)
		m_error = details::WMIserviceWrap_InitNetworkCfg(m_net_cfg);
	if (!m_net_cfg)
		return cs_ip;
	_variant_t vProperty;
	m_error = m_net_cfg->Get(_T("IPAddress"), 0, &vProperty, 0, 0);
	if (0 != (VT_ARRAY & vProperty.vt) &&
		0 != (VT_BSTR  & vProperty.vt))
	{
		::ATL::CComSafeArray<BSTR> psa;
		m_error = psa.Attach(vProperty.parray);
		if (m_error)
			return cs_ip;

		for (ULONG i_ = 0; i_ < psa.GetCount(); i_++)
		{
			_bstr_t bs_(psa[(LONG)i_], TRUE);

			if (false){}
			else if ( _tcp_v6 && i_ == 1)
				cs_ip = static_cast<LPCTSTR>(bs_);
			else if (!_tcp_v6 && i_ == 0)
				cs_ip = static_cast<LPCTSTR>(bs_);
			if (cs_ip.IsEmpty() == false)
				break;
		}
		psa.Detach();
	}
	return cs_ip;
}

CAtlString   CWMI_NetworkProvider::GetMacAddress(void)const
{
	CAtlString cs_mac;

	_variant_t vProperty = TBase::GetProperty(_T("MACAddress"));
	if (VT_BSTR == vProperty.vt)
		cs_mac  =  vProperty;
	return cs_mac;
}

/////////////////////////////////////////////////////////////////////////////

CWMI_ProcessorProvider::CWMI_ProcessorProvider(void)
{
	m_error = details::WMIserviceWrap_InitCpu(m_provider);
}

CWMI_ProcessorProvider::~CWMI_ProcessorProvider(void)
{
}

/////////////////////////////////////////////////////////////////////////////

CAtlString   CWMI_ProcessorProvider::GetIdentifier(void)const
{
	CAtlString cs_id;

	_variant_t vProperty = TBase::GetProperty(_T("ProcessorId"));
	if (VT_BSTR == vProperty.vt)
		cs_id    = vProperty;

	return cs_id;
}

CAtlString   CWMI_ProcessorProvider::GetSerialNumber(void)const
{
	CAtlString cs_serial;
	// https://msdn.microsoft.com/en-us/library/aa394373.aspx
	_variant_t vProperty = TBase::GetProperty(_T("SerialNumber"));
	if (VT_BSTR == vProperty.vt)
		cs_serial= vProperty;

	return cs_serial;
}

/////////////////////////////////////////////////////////////////////////////

CWMI_SecurityProvider::CWMI_SecurityProvider(void)
{
	m_error = details::WMIserviceWrap_InitSecurity(m_provider);
}

CWMI_SecurityProvider::~CWMI_SecurityProvider(void)
{
}

/////////////////////////////////////////////////////////////////////////////

CWMI_Win32Provider::CWMI_Win32Provider(void)
{
	m_error = details::WMIserviceWrap_InitWin32(m_provider);
}

CWMI_Win32Provider::~CWMI_Win32Provider(void)
{
}

/////////////////////////////////////////////////////////////////////////////

CWMIsecurityEnum::CWMIsecurityEnum(void)
{
	if (m_error)m_error.Clear();
	if (!m_props.empty())m_props.clear();

	TObjectEnumerator pObjectEnum;

	m_error = details::WMIserviceWrap_InitSecurity(pObjectEnum);
	if (m_error)
		return;
	ULONG uReturn = 0;
	while (true)
	{
		TPropertyProvider pProvider;
		HRESULT hr_ = pObjectEnum->Next(WBEM_INFINITE, 1, &pProvider, &uReturn);
		if (FAILED(hr_) || !pProvider)
			break;
		if (!uReturn)
			break;
		try
		{
			m_props.push_back(pProvider);
		}
		catch(::std::bad_alloc&){m_error = E_OUTOFMEMORY; break;}
	}
}

CWMIsecurityEnum::~CWMIsecurityEnum(void)
{
	if (!m_props.empty())m_props.clear();
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef    CWMIsecurityEnum::Error(void)const
{
	return m_error;
}

const
TProperties& CWMIsecurityEnum::GetProperties(void)const
{
	return m_props;
}