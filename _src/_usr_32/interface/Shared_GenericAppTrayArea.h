#ifndef _SHAREDGENERICAPPTRAYAREA_H_CA32ED39_0625_4dfa_9074_BEF5001F95D1_INCLUDED
#define _SHAREDGENERICAPPTRAYAREA_H_CA32ED39_0625_4dfa_9074_BEF5001F95D1_INCLUDED
/*
	Created by Tech_dog (VToropov) on 10-Jun-2016 at 4:59:07p, GMT+7, Phuket, Rawai, Friday;
	This is Shared User32 Wrapper generic application system tray notification class(es) declaration file.
*/
#include <shellapi.h>

namespace shared { namespace user32
{
	interface INotifyTrayAreaCallback
	{
		virtual HRESULT  NotifyTray_OnClickEvent(const UINT eventId)       PURE; // a user clicks application notify area
		virtual HRESULT  NotifyTray_OnContextMenuEvent(const UINT eventId) PURE; // requests to show context menu
		virtual HRESULT  NotifyTray_OnMouseMoveEvent(const UINT eventId)   PURE; // notifies about mouse movement over the tray icon
	};

	class eApplicationPopupType
	{
	public:
		enum _e{
			eInformation    = 0,   // default
			eWarning        = 1,
			eError          = 2
		};
	};

	class CApplicationTrayArea
	{
	public:
		enum {
			NTA_CallbackMessageId = WM_APP + 5
		};
	private:
		class CMessageHandler:
			public ATL::CWindowImpl<CMessageHandler>
		{
			typedef ATL::CWindowImpl<CMessageHandler> TWindow;
		private:
			INotifyTrayAreaCallback&   m_sink_ref;
			const UINT                 m_event_id;
		public:
			CMessageHandler(INotifyTrayAreaCallback&, const UINT eventId);
			~CMessageHandler(void);
		public:
			BEGIN_MSG_MAP(CMessageHandler)
				MESSAGE_HANDLER(CApplicationTrayArea::NTA_CallbackMessageId, OnNotify)
			END_MSG_MAP()
		private:
			LRESULT  OnNotify(UINT, WPARAM, LPARAM, BOOL&);
		};
	private:
		bool             m_bInitialized;
		NOTIFYICONDATA   m_data;
		CMessageHandler  m_handler;
	public:
		CApplicationTrayArea(INotifyTrayAreaCallback&, const UINT eventId);
		~CApplicationTrayArea(void);
	public:
		HRESULT          Initialize(const UINT nDefIconResourceId, LPCTSTR pTooltip);
		bool             IsInitialized(void)const;
		HRESULT          ShowPopup(LPCTSTR pTitle, LPCTSTR pText, const eApplicationPopupType::_e, const UINT uTimeout = 3); // timeout in sec(s)
		HRESULT          Terminate(void);
	};
}}


#endif/*_SHAREDGENERICAPPTRAYAREA_H_CA32ED39_0625_4dfa_9074_BEF5001F95D1_INCLUDED*/