#ifndef _SHAREDGENERICAPPSHORTCUT_H_B4E30B8D_6C2E_47e2_8A83_B0BBBE031315_INCLUDED
#define _SHAREDGENERICAPPSHORTCUT_H_B4E30B8D_6C2E_47e2_8A83_B0BBBE031315_INCLUDED
/*
	Created by Tech_dog (VToropov) on 10-Jun-2016 at 4:54:25p, GMT+7, Phuket, Rawai, Friday;
	This is User32 Wrapper application desktop shorcut class(es) declaration file.
*/
#include <Shobjidl.h>

namespace shared { namespace user32
{
	class CApplicationShortcutCrtData
	{
	private:
		CAtlString       m_target;     // executable file path a link is created to;
		CAtlString       m_icon_file;  // executable file (DLL/EXE) that contains an icon or an icon file path; if empty, target is used;
		INT              m_icon_index; // an index of an icon within the executable file that contains an icon; if -1, the icon path points to regular ICO file
	public:
		CApplicationShortcutCrtData(void);
	public:
		const
		CAtlString&      IconFile(void)const;
		CAtlString&      IconFile(void);
		const
		INT&             IconIndex(void)const;
		INT&             IconIndex(void);
		const
		CAtlString&      Target(void)const;
		CAtlString&      Target(void);
	};

	class CApplicationShortcut
	{
	private:
		CAtlString       m_lnk_folder;
		CAtlString       m_lnk_title;
	public:
		CApplicationShortcut(LPCTSTR pszDestFolder, LPCTSTR pszLnkTitle);
		~CApplicationShortcut(void);
	public:
		HRESULT          Create (const CApplicationShortcutCrtData&);
		HRESULT          Create (LPCTSTR pszTargetPath);   // creates link for target file specified
		HRESULT          Remove (VOID);                    // removes link by path specified (destination folder + link title)
		HRESULT          SetIcon(LPCTSTR pszIconFilePath, const INT nIconIndex = 0);
	};
}}

#endif/*_SHAREDGENERICAPPSHORTCUT_H_B4E30B8D_6C2E_47e2_8A83_B0BBBE031315_INCLUDED*/