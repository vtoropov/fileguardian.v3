#ifndef _SHAREDGENERICCONOBJECT_H_4D74D09A_4FA1_4d03_9401_4D064A3AE294_INCLUDED
#define _SHAREDGENERICCONOBJECT_H_4D74D09A_4FA1_4d03_9401_4D064A3AE294_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jan-2016 at 9:26:48am, GMT+7, Phuket, Rawai, Wednesday;
	This is Shared Lite Library Windows Console generic wrapper class declaration file.
*/
#include <atlwin.h>
namespace shared { namespace user32
{
	class eConsoleNotifyType
	{
	public:
		enum _e{
			eInfo    = 0x0,
			eWarning = 0x1,
			eError   = 0x2
		};
	};

	interface IConsoleNotify
	{
		virtual VOID   ConsoleNotify_OnEvent(const eConsoleNotifyType::_e, const UINT  nResId) PURE;
		virtual VOID   ConsoleNotify_OnEvent(const eConsoleNotifyType::_e, LPCTSTR pszDetails) PURE;
		virtual VOID   ConsoleNotify_OnError(const UINT nResId) PURE;
		virtual VOID   ConsoleNotify_OnInfo (const UINT nResId) PURE;
	};

	class CConsoleNotifyDefImpl : public IConsoleNotify
	{
	private: // IConsoleNotify
		virtual VOID   ConsoleNotify_OnEvent(const eConsoleNotifyType::_e, const UINT  nResId) override sealed;
		virtual VOID   ConsoleNotify_OnEvent(const eConsoleNotifyType::_e, LPCTSTR pszDetails) override sealed;
		virtual VOID   ConsoleNotify_OnError(const UINT nResId) override sealed;
		virtual VOID   ConsoleNotify_OnInfo (const UINT nResId) override sealed;
	};

	class CConsolePage {
	private:
		UINT       m_in;
		UINT       m_out;
	public:
		CConsolePage(const bool bAutoHandle = true);
		~CConsolePage(void);

	public:
		HRESULT       Dos(void);
		HRESULT       Set(const UINT _in, const UINT _out);
	};

	class CConsoleWindow
	{
	private:
		ATL::CWindow  m_wnd;
	public:
		CConsoleWindow(void);
	public:
		HRESULT       ClearContent(void);
		HWND          GetHwndSafe (void)const;
		bool          IsValid(void)const;
		HRESULT       SetIcon(const UINT nResId);
	public:
		static
		HRESULT       Create(LPCTSTR lpszTitle=NULL);
	};
}}

#endif/*_SHAREDGENERICCONOBJECT_H_4D74D09A_4FA1_4d03_9401_4D064A3AE294_INCLUDED*/