#ifndef _SHAREDNTFSSTDAFX_H_D8061509_9F1B_4e4d_ABA3_B26B8082FFB6_INCLUDED
#define _SHAREDNTFSSTDAFX_H_D8061509_9F1B_4e4d_ABA3_B26B8082FFB6_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Jun-2016 at 5:01:04p, GMT+7, Phuket, Rawai, Friday;
	This is Shared User32 Wrapper Library precompiled headers declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 27-May-2018 at 9:45:20p, UTC+7, Phuket, Rawai, Sunday;
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <atlapp.h>

/////////////////////////////////////////////////////////////////////////////
//
// COM helpers
//
/////////////////////////////////////////////////////////////////////////////

#include <comdef.h>
#include <comutil.h>

/////////////////////////////////////////////////////////////////////////////
//
// STL specifics
//
/////////////////////////////////////////////////////////////////////////////

#include <vector>

/////////////////////////////////////////////////////////////////////////////
//
// Console specifics
//
/////////////////////////////////////////////////////////////////////////////

#include <io.h>     // _open_osfhandle
#include <fcntl.h>  // _O_TEXT and _O_BINARY

#endif/*_SHAREDNTFSSTDAFX_H_D8061509_9F1B_4e4d_ABA3_B26B8082FFB6_INCLUDED*/