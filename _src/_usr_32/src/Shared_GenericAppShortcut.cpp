/*
	Created by Tech_dog (VToropov) on 10-Jun-2016 at 4:15:05p, GMT+7, Phuket, Rawai, Friday;
	This is User32 Wrapper application desktop shorcut class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 27-May-2018 at 9:36:30p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Shared_GenericAppShortcut.h"

using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

CApplicationShortcutCrtData::CApplicationShortcutCrtData(void) : m_icon_index(-1)
{
}

/////////////////////////////////////////////////////////////////////////////

const
CAtlString&  CApplicationShortcutCrtData::IconFile(void)const
{
	return m_icon_file;
}

CAtlString&  CApplicationShortcutCrtData::IconFile(void)
{
	return m_icon_file;
}

const
INT&         CApplicationShortcutCrtData::IconIndex(void)const
{
	return m_icon_index;
}

INT&         CApplicationShortcutCrtData::IconIndex(void)
{
	return m_icon_index;
}

const
CAtlString&  CApplicationShortcutCrtData::Target(void)const
{
	return m_target;
}

CAtlString&  CApplicationShortcutCrtData::Target(void)
{
	return m_target;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace user32 { namespace details
{
	HRESULT ApplicationShortcut_CheckArguments (LPCTSTR pLinkFolder, LPCTSTR pTitle, LPCTSTR pTargetPath)
	{
		if (false){}
		else if (!pLinkFolder || ::_tcslen(pLinkFolder) < _MAX_DRIVE + 0) return E_INVALIDARG;
		else if (!pTargetPath || ::_tcslen(pTargetPath) < _MAX_DRIVE + 1) return E_INVALIDARG;
		else if (!pTitle || !::_tcslen(pTitle)) return E_INVALIDARG;
		else
			return S_OK;
	}

	VOID    ApplicationShortcut_ComposeLinkPath(LPCTSTR pLinkFolder, LPCTSTR pTitle, ::ATL::CAtlString& cs_link)
	{
		cs_link = pLinkFolder;
		cs_link.Replace(_T('/'), _T('\\'));
		if (_T('\\') != cs_link.Right(1))
		cs_link  += _T('\\');
		cs_link  += pTitle;
		cs_link  += _T(".lnk");
	}

	HRESULT ApplicationShortcut_SaveLnkObject  (LPCTSTR pLinkFolder, LPCTSTR pTitle, ::ATL::CComPtr<IShellLink>& _lnk)
	{
		if (!_lnk)
			return E_INVALIDARG;

		::ATL::CComQIPtr<IPersistFile> pers_;
		HRESULT hr_ = _lnk.QueryInterface(&pers_);
		if (FAILED(hr_))
			return hr_;

		if (!pers_)
			return (hr_ = E_NOINTERFACE);

		::ATL::CAtlString cs_link;
		details::ApplicationShortcut_ComposeLinkPath(pLinkFolder, pTitle, cs_link);

		hr_ = pers_->Save(cs_link, TRUE);
		if (!FAILED(hr_))
			hr_ = pers_->SaveCompleted(cs_link); // always returns S_OK;
		return hr_;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CApplicationShortcut::CApplicationShortcut(LPCTSTR pszDestFolder, LPCTSTR pszLnkTitle):
	m_lnk_folder(pszDestFolder),
	m_lnk_title (pszLnkTitle)
{
}

CApplicationShortcut::~CApplicationShortcut(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CApplicationShortcut::Create (const CApplicationShortcutCrtData& _crt)
{
	HRESULT hr_ = details::ApplicationShortcut_CheckArguments(m_lnk_folder, m_lnk_title, _crt.Target());
	if (FAILED(hr_))
		return hr_;

	::ATL::CComPtr<IShellLink> lnk_;
	hr_ = lnk_.CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER);
	if (FAILED(hr_))
		return hr_;

	INT        ico_ndx_ = -1;
	CAtlString ico_loc_;

	if (_crt.IconFile().IsEmpty())
	{
		ico_ndx_ = 0;
		ico_loc_ = _crt.Target();
	}
	else
	{
		ico_ndx_ = _crt.IconIndex();
		ico_loc_ = _crt.IconFile();
	}

	hr_ = lnk_->SetIconLocation(ico_loc_, ico_ndx_); if (FAILED(hr_)) return hr_;
	hr_ = lnk_->SetPath(_crt.Target());              if (FAILED(hr_)) return hr_;

	hr_ = details::ApplicationShortcut_SaveLnkObject(m_lnk_folder, m_lnk_title, lnk_);
	return hr_;
}

HRESULT   CApplicationShortcut::Create(LPCTSTR pszTargetPath)
{
	HRESULT hr_ = details::ApplicationShortcut_CheckArguments(m_lnk_folder, m_lnk_title, pszTargetPath);
	if (FAILED(hr_))
		return hr_;

	::ATL::CComPtr<IShellLink> lnk_;
	hr_ = lnk_.CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER);
	if (FAILED(hr_))
		return hr_;

	hr_ = lnk_->SetIconLocation(pszTargetPath, 0); if (FAILED(hr_)) return hr_;
	hr_ = lnk_->SetPath(pszTargetPath);            if (FAILED(hr_)) return hr_;

	hr_ = details::ApplicationShortcut_SaveLnkObject(m_lnk_folder, m_lnk_title, lnk_);
	return hr_;
}

HRESULT   CApplicationShortcut::Remove(VOID)
{
	HRESULT hr_ = details::ApplicationShortcut_CheckArguments(m_lnk_folder, m_lnk_title, _T("dummy"));
	if (FAILED(hr_))
		return hr_;

	::ATL::CAtlString cs_link;
	details::ApplicationShortcut_ComposeLinkPath(m_lnk_folder, m_lnk_title, cs_link);

	if (!::DeleteFile(cs_link))
		hr_ = HRESULT_FROM_WIN32(::GetLastError());

	return hr_;
}

HRESULT   CApplicationShortcut::SetIcon(LPCTSTR pszIconFilePath, const INT nIconIndex)
{
	HRESULT hr_ = details::ApplicationShortcut_CheckArguments(m_lnk_folder, m_lnk_title, pszIconFilePath);
	if (FAILED(hr_))
		return hr_;

	::ATL::CComPtr<IShellLink> lnk_;
	hr_ = lnk_.CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER);
	if (FAILED(hr_))
		return hr_;

	hr_ = lnk_->SetIconLocation(pszIconFilePath, nIconIndex);
	if (FAILED(hr_))
		return hr_;

	hr_ = details::ApplicationShortcut_SaveLnkObject(m_lnk_folder, m_lnk_title, lnk_);
	return hr_;
}