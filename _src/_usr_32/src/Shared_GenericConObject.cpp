/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jan-2016 at 9:44:11am, GMT+7, Phuket, Rawai, Wednesday;
	This is Shared Lite Library Windows Console generic wrapper class implementation file.
*/
#include "StdAfx.h"
#include "Shared_GenericConObject.h"
#include "Shared_GenericAppObject.h"
#include "Shared_GenericAppResource.h"

using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

CConsolePage::CConsolePage(const bool bAutoHandle) : m_in(0), m_out(0) {
	if (bAutoHandle) {
		HANDLE hStdHandle = GetStdHandle(STD_OUTPUT_HANDLE);

		if (INVALID_HANDLE_VALUE != hStdHandle) {
			m_in = ::GetConsoleCP();
			m_out = ::GetConsoleOutputCP();
		}
	}
}

CConsolePage::~CConsolePage(void) {

	HANDLE hStdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	if (INVALID_HANDLE_VALUE != hStdHandle) {

		if (m_in) ::SetConsoleCP(m_in); m_in = 0;
		if (m_out) ::SetConsoleOutputCP(m_out); m_out = 0;
	}
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CConsolePage::Dos(void) {

	const UINT u_dos_cp = 437;
	HRESULT hr_ = this->Set(u_dos_cp, u_dos_cp);

	return hr_;
}

HRESULT   CConsolePage::Set(const UINT _in, const UINT _out) {

	HRESULT hr_ = S_OK;
	if (0 == _in)   return (hr_ = E_INVALIDARG);
	if (0 == _out)   return (hr_ = E_INVALIDARG);

	BOOL
	bResult  = ::SetConsoleCP(_in);        if (!bResult) return (hr_ = HRESULT_FROM_WIN32(::GetLastError()));
	bResult  = ::SetConsoleOutputCP(_out); if (!bResult) return (hr_ = HRESULT_FROM_WIN32(::GetLastError()));

	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

VOID   CConsoleNotifyDefImpl::ConsoleNotify_OnEvent(const eConsoleNotifyType::_e eType, const UINT  nResId)
{
	CAtlString cs_details;
	cs_details.LoadString(nResId);
	this->ConsoleNotify_OnEvent(eType, cs_details.GetString());
}

VOID   CConsoleNotifyDefImpl::ConsoleNotify_OnEvent(const eConsoleNotifyType::_e eType, LPCTSTR pszDetails)
{
	::ATL::CAtlString cs_out;
	switch(eType)
	{
	case eConsoleNotifyType::eError:   cs_out.Format(_T("\n\t[ERROR] %s"), pszDetails); break;
	case eConsoleNotifyType::eInfo:    cs_out.Format(_T("\n\t[INFO]  %s"), pszDetails); break;
	case eConsoleNotifyType::eWarning: cs_out.Format(_T("\n\t[WARN]  %s"), pszDetails); break;
	default:
		return;
	}
	::_tprintf(cs_out);
}

VOID   CConsoleNotifyDefImpl::ConsoleNotify_OnError(const UINT nResId)
{
	this->ConsoleNotify_OnEvent(eConsoleNotifyType::eError, nResId);
}

VOID   CConsoleNotifyDefImpl::ConsoleNotify_OnInfo (const UINT nResId)
{
	this->ConsoleNotify_OnEvent(eConsoleNotifyType::eInfo, nResId);
}

/////////////////////////////////////////////////////////////////////////////

CConsoleWindow::CConsoleWindow(void) : m_wnd(::GetConsoleWindow()) { }

/////////////////////////////////////////////////////////////////////////////

#define __BREAK_ON_ERROR(bSuccess) \
	if (!bSuccess)                 \
		return HRESULT_FROM_WIN32(::GetLastError());

HRESULT   CConsoleWindow::ClearContent(void)
{
	HRESULT hr_ = S_OK;

	HANDLE hStdHandle = GetStdHandle(STD_OUTPUT_HANDLE);

	if (INVALID_HANDLE_VALUE == hStdHandle)
		return HRESULT_FROM_WIN32(::GetLastError());

	COORD coordScreen   = { 0, 0 };
	BOOL  bSuccess      = TRUE;
	DWORD cCharsWritten = 0;
	CONSOLE_SCREEN_BUFFER_INFO csbi = {0};
	DWORD dwConSize     = 0;                  // number of character cells in the current buffer

	//
	// gets the number of character cells in the current buffer
	//
	bSuccess = GetConsoleScreenBufferInfo( hStdHandle, &csbi );
	__BREAK_ON_ERROR(bSuccess);

	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;

	//
	// fills the entire screen with blanks
	//
	bSuccess = FillConsoleOutputCharacter(
					hStdHandle, (TCHAR)' ', dwConSize, coordScreen, &cCharsWritten
				);
	__BREAK_ON_ERROR(bSuccess);

	//
	// gets the current text attribute
	//
	bSuccess = GetConsoleScreenBufferInfo( hStdHandle, &csbi );
	__BREAK_ON_ERROR(bSuccess);

	//
	// sets the buffer's attributes accordingly
	//
	bSuccess = FillConsoleOutputAttribute(
					hStdHandle, csbi.wAttributes, dwConSize, coordScreen, &cCharsWritten
				);
	__BREAK_ON_ERROR(bSuccess);

	//
	// puts the cursor at (0, 0)
	//
	bSuccess = SetConsoleCursorPosition( hStdHandle, coordScreen );
	__BREAK_ON_ERROR(bSuccess);

	return hr_;
}

HWND      CConsoleWindow::GetHwndSafe(void)const { return m_wnd; }
bool      CConsoleWindow::IsValid(void)const  { return !!(m_wnd.IsWindow()); }

HRESULT   CConsoleWindow::SetIcon(const UINT nResId)
{
	if (!m_wnd)
		return OLE_E_BLANK;
	CApplicationIconLoader loader_(nResId);
	if (loader_.GetLargeIcon())
		m_wnd.SetIcon(loader_.DetachLargeIcon(), TRUE);
	if (loader_.GetSmallIcon())
		m_wnd.SetIcon(loader_.DetachSmallIcon(), FALSE);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CConsoleWindow::Create(LPCTSTR lpszTitle)
{
	HRESULT hr_ = S_OK;
	if (!AllocConsole())
		return (hr_ = HRESULT_FROM_WIN32(::GetLastError()));

	if (NULL != lpszTitle)
		::SetConsoleTitle(lpszTitle);

	CONSOLE_SCREEN_BUFFER_INFO info_ = {0};
	::EnableMenuItem(::GetSystemMenu(::GetConsoleWindow(), FALSE), SC_CLOSE , MF_GRAYED);

	::DrawMenuBar(::GetConsoleWindow());
	::GetConsoleScreenBufferInfo(::GetStdHandle(STD_OUTPUT_HANDLE), &info_ );

	// (1) sets output/read handle
	{
		const intptr_t handle_ = reinterpret_cast<intptr_t>(::GetStdHandle(STD_INPUT_HANDLE));
		const INT input_ = ::_open_osfhandle(handle_, _O_TEXT );
		FILE* pFile = ::_fdopen(input_, "r");

		::setvbuf( pFile, NULL, _IONBF, 0 );
	}
	// (2) sets input/write handle
	{
		const intptr_t handle_ = reinterpret_cast<intptr_t>(::GetStdHandle(STD_OUTPUT_HANDLE));
		const INT output_ = ::_open_osfhandle(handle_, _O_TEXT );
		FILE* pFile = ::_fdopen(output_, "w");

		::setvbuf( pFile, NULL, _IONBF, 0 );
	}
	// (3) sets error handle
	{
		const intptr_t handle_ = reinterpret_cast<intptr_t>(::GetStdHandle(STD_ERROR_HANDLE));
		const INT error_ = ::_open_osfhandle(handle_, _O_TEXT );
		FILE* pFile = ::_fdopen(error_, "w");

		::setvbuf( pFile, NULL, _IONBF, 0 );
	}

	return  hr_;
}