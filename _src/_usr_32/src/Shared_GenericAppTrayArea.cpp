/*
	Created by Tech_dog (VToropov) on 10-Jun-2016 at 5:01:50p, GMT+7, Phuket, Rawai, Friday;
	This is Shared User32 Wrapper generic application system tray notification class(es) implementation file.
*/
#include "StdAfx.h"
#include "Shared_GenericAppTrayArea.h"
#include "Shared_GenericAppResource.h"

using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

#if !defined(NIF_SHOWTIP)
	#define  NIF_SHOWTIP           0x00000080
#endif
#if !defined(NOTIFYICON_VERSION_4)
	#define  NOTIFYICON_VERSION_4  0x4
#endif

/////////////////////////////////////////////////////////////////////////////

CApplicationTrayArea::CMessageHandler::CMessageHandler(INotifyTrayAreaCallback& sink_ref, const UINT eventId):
	m_sink_ref(sink_ref),
	m_event_id(eventId)
{
}

CApplicationTrayArea::CMessageHandler::~CMessageHandler(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT  CApplicationTrayArea::CMessageHandler::OnNotify(UINT, WPARAM, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	if (::WTL::RunTimeHelper::IsVista())
	{
		switch (LOWORD(lParam))
		{
		case WM_CONTEXTMENU: m_sink_ref.NotifyTray_OnContextMenuEvent(m_event_id); break;
		case WM_MOUSEMOVE:   m_sink_ref.NotifyTray_OnMouseMoveEvent  (m_event_id); break;
		case WM_LBUTTONDOWN: m_sink_ref.NotifyTray_OnClickEvent      (m_event_id); break;
		}
	}
	else
	{
		switch(lParam)
		{
		case WM_RBUTTONDOWN: m_sink_ref.NotifyTray_OnContextMenuEvent(m_event_id); break;
		case WM_MOUSEMOVE:   m_sink_ref.NotifyTray_OnMouseMoveEvent  (m_event_id); break;
		case WM_LBUTTONDOWN: m_sink_ref.NotifyTray_OnClickEvent      (m_event_id); break;
		}
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CApplicationTrayArea::CApplicationTrayArea(INotifyTrayAreaCallback& sink_ref, const UINT eventId): 
	m_handler(sink_ref, eventId),
	m_bInitialized(false)
{
	::memset((void*)&m_data, 0, sizeof(NOTIFYICONDATA));
}

CApplicationTrayArea::~CApplicationTrayArea(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CApplicationTrayArea::Initialize(const UINT nDefIconResourceId, LPCTSTR pTooltip)
{
	if (m_bInitialized) return S_OK;
	if (m_handler.IsWindow()!= TRUE)
		m_handler.Create(HWND_MESSAGE);
	HRESULT hr_ = m_handler.IsWindow() ? S_OK : HRESULT_FROM_WIN32(::GetLastError());
	if (S_OK != hr_)
		return  hr_;
	CApplicationIconLoader ico_loader(nDefIconResourceId);
	::memset((void*)&m_data, 0, sizeof(NOTIFYICONDATA));
	m_data.cbSize             = sizeof(NOTIFYICONDATA);
	m_data.hWnd               = m_handler;
	m_data.uID                = 1;
	m_data.uCallbackMessage   = CApplicationTrayArea::NTA_CallbackMessageId;
	m_data.uFlags             = (NIF_ICON | NIF_MESSAGE | NIF_TIP);
	if (::WTL::RunTimeHelper::IsVista())
		m_data.uFlags        |= NIF_SHOWTIP;
	m_data.hIcon              = ico_loader.DetachSmallIcon();
	if (pTooltip && ::_tcslen(pTooltip))
	{
		::_tcscpy_s(m_data.szTip, ARRAYSIZE(m_data.szTip), pTooltip);
	}
	if (!::Shell_NotifyIcon(NIM_ADD, &m_data))
		hr_ = HRESULT_FROM_WIN32(::GetLastError());
	if (::WTL::RunTimeHelper::IsVista())
	{
		m_data.uVersion = NOTIFYICON_VERSION_4;
		if (!::Shell_NotifyIcon(NIM_SETVERSION, &m_data))
			hr_ = HRESULT_FROM_WIN32(::GetLastError());
	}
	m_bInitialized = (S_OK == hr_);
	if (!m_bInitialized && m_handler.IsWindow())
	{
		m_handler.SendMessage(WM_CLOSE);
	}
	return  hr_;
}

bool      CApplicationTrayArea::IsInitialized(void)const
{
	return m_bInitialized;
}

HRESULT   CApplicationTrayArea::ShowPopup(LPCTSTR pTitle, LPCTSTR pText, const eApplicationPopupType::_e type_, const UINT uTimeout)
{
	NOTIFYICONDATA nid = {0};
	nid.cbSize         = sizeof(NOTIFYICONDATA);
	nid.hWnd           = m_handler;
	nid.uID            = 1;
	nid.uTimeout       = uTimeout;
	nid.uFlags         = NIF_INFO;
	nid.dwInfoFlags    = (eApplicationPopupType::eError == type_ ? NIIF_ERROR : (eApplicationPopupType::eWarning == type_ ? NIIF_WARNING : NIIF_INFO));
	::_tcscpy_s(nid.szInfoTitle, ARRAYSIZE(nid.szInfoTitle), pTitle);
	::_tcscpy_s(nid.szInfo, ARRAYSIZE(nid.szInfo), pText);
	HRESULT hr_ = (::Shell_NotifyIcon(NIM_MODIFY, &nid) ? S_OK : HRESULT_FROM_WIN32(::GetLastError()));
	return  hr_;
}

HRESULT   CApplicationTrayArea::Terminate(void)
{
	if (!m_bInitialized) return S_FALSE;
	::Shell_NotifyIcon(NIM_DELETE, &m_data);
	if (m_handler.IsWindow())
		m_handler.SendMessage(WM_CLOSE);
	m_bInitialized = false;
	return S_OK;
}