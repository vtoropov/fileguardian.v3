/*
	Created by Tech_dog (VToropov) on 11-Jun-2016 at 4:57:52p, GMT+7, Phuket, Rawai, Saturday;
	This is Shared Lite Library Generic Application class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 27-May-2018 at 9:9:57p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Shared_GenericAppObject.h"

using namespace shared::user32;

#include "Shared_FS_GenericFolder.h"

using namespace shared::ntfs;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace user32 { namespace details
{
	enum Application_ProcState
	{
		Application_ProcState_DoNotCare   = 0x0,
		Application_ProcState_1stInstance = 0x1,
		Application_ProcState_IsRunning   = 0x2,
	};

	LPCTSTR   Application_ProcessName(CONST shared::user32::CApplication& app_obj_ref)
	{
		static ::ATL::CAtlString  proc_name;
		if (proc_name.IsEmpty())
		{
			proc_name.Format(_T("Global\\%s|C9A74DC6-0A84-4c60-BA20-59A516289ED0"), app_obj_ref.GetName());
			proc_name.Replace(_T(" "), _T("_"));
		}
		return proc_name.GetString();
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CApplication::CObject::CObject(const CApplication& app_obj_ref) : 
	m_app_obj_ref(app_obj_ref),
	m_mutex(NULL),
	m_proc_state(details::Application_ProcState_DoNotCare)
{
}

CApplication::CObject::~CObject(VOID)
{
	UnregisterSingleton();
}

/////////////////////////////////////////////////////////////////////////////

bool      CApplication::CObject::IsSingleton(void) const
{
	return (details::Application_ProcState_1stInstance == m_proc_state);
}

HRESULT   CApplication::CObject::RegisterSingleton(LPCTSTR pMutexName)
{
	HRESULT hr__ = S_OK;
	if (m_mutex_name.IsEmpty())
		m_mutex_name = (!pMutexName ? details::Application_ProcessName(m_app_obj_ref) : pMutexName);

	if (details::Application_ProcState_DoNotCare == m_proc_state && !m_mutex)
	{
		m_mutex = ::CreateMutex(NULL, FALSE, m_mutex_name.GetString());
		const DWORD dError = ::GetLastError();
		if (NULL == m_mutex && ERROR_ACCESS_DENIED == dError) // the synch object already exists and we cannot get an access to it
		{
			m_proc_state = details::Application_ProcState_IsRunning;
			m_mutex = ::OpenMutex(MUTEX_ALL_ACCESS, FALSE, m_mutex_name.GetString());  // this is superfluous call, but nevertheless it is made for the sake of clarity
			hr__ = HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
		}
		else if (ERROR_ALREADY_EXISTS == dError)
		{
			m_proc_state = details::Application_ProcState_IsRunning;
			hr__ = HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
		}
		else
			m_proc_state = details::Application_ProcState_1stInstance;
	}
	else
		hr__ = HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	return hr__;
}

HRESULT   CApplication::CObject::UnregisterSingleton(VOID)
{
	if (details::Application_ProcState_1stInstance == m_proc_state)
		if (NULL != m_mutex)
		{
			::CloseHandle(m_mutex); m_mutex = NULL;
			return S_OK;
		}
	return S_FALSE;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace user32 { namespace details
{
	struct   Version_LanguageCodePage
	{
		WORD wLanguage;
		WORD wCodePage;
	};

	HRESULT  Version_GetRawPointer(LPVOID& ptr_ref, LPCTSTR pszModulePath)
	{
		if (ptr_ref)
			return HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS);
		if (!pszModulePath)
			return E_INVALIDARG;
		DWORD dwDummy = 0;
		// determines the size buffer needed to store the version information
		DWORD dwVerInfoSize = ::GetFileVersionInfoSize(pszModulePath, &dwDummy);
		if ( !dwVerInfoSize )
			return OLE_E_BLANK; // no version info
		try
		{
			ptr_ref = new char[dwVerInfoSize];
			::memset(ptr_ref, 0, dwVerInfoSize);
		}
		catch(::std::bad_alloc&)
		{
			return E_OUTOFMEMORY;
		}
		// reads the version info block into the buffer
		if (!::GetFileVersionInfo(pszModulePath, dwDummy, dwVerInfoSize, ptr_ref))
		{
			const DWORD dwError = ::GetLastError();
			try { delete ptr_ref; ptr_ref = NULL; } catch(...){}

			return HRESULT_FROM_WIN32(dwError);
		}
		else
			return S_OK;
	}

	HRESULT  Version_FixedFileInfo(LPVOID p_raw_data, VS_FIXEDFILEINFO& info_ref)
	{
		if (!p_raw_data)
			return E_INVALIDARG;
		LPVOID pVersionPtr = NULL;
		UINT   uiVerLength = NULL;
		if (!::VerQueryValue(p_raw_data, _T ("\\"), &pVersionPtr, &uiVerLength))
			return E_OUTOFMEMORY;
		if (uiVerLength != sizeof(VS_FIXEDFILEINFO))
			return DISP_E_TYPEMISMATCH;
		if (!pVersionPtr)
			return E_OUTOFMEMORY;
		info_ref = *(VS_FIXEDFILEINFO*) pVersionPtr; // scare!
		return S_OK;
	}

	LPCTSTR  Version_LanguageDefault(VOID)
	{
		static LPCTSTR pLanguage = _T("\\StringFileInfo\\040904B0\\"); // 040904B0 means US English, Unicode code page
		return pLanguage;
	}

	HRESULT  Version_LanguageCurrent(LPVOID p_raw_data, Version_LanguageCodePage& cp_ref)
	{
		if (!p_raw_data)
			return E_INVALIDARG;
		LPVOID pVersionPtr = NULL;
		UINT   uiVerLength = NULL;
		if (!::VerQueryValue(p_raw_data, _T("\\VarFileInfo\\Translation"), (LPVOID*)&pVersionPtr, &uiVerLength))
			return E_OUTOFMEMORY;
		if (uiVerLength != sizeof(Version_LanguageCodePage))
			return DISP_E_TYPEMISMATCH;
		if (!pVersionPtr)
			return E_OUTOFMEMORY;
		cp_ref = *((Version_LanguageCodePage*)pVersionPtr);
		return S_OK;
	}
	HRESULT  Version_QueryStringValue(LPVOID p_raw_data, LPCTSTR p_qry, ::ATL::CAtlString& result_ref)
	{
		if (!p_qry || ::_tcslen(p_qry) < 1)
			return E_INVALIDARG;
		Version_LanguageCodePage cp__ = {0};
		HRESULT hr__ = Version_LanguageCurrent(p_raw_data, cp__);
		if (S_OK != hr__)
			return  hr__;
		::ATL::CAtlString s_qry;
		s_qry.Format(_T("\\StringFileInfo\\%04x%04x\\%s"), cp__.wLanguage, cp__.wCodePage, p_qry);
		LPVOID pVersionPtr = NULL;
		UINT   uiVerLength = NULL;
		if (!::VerQueryValue(p_raw_data, s_qry.GetString(), (LPVOID*)&pVersionPtr, &uiVerLength))
		{
			// tries to get default language string block
			s_qry.Format(_T("%s%s"), Version_LanguageDefault(), p_qry);
			if (!::VerQueryValue(p_raw_data, s_qry.GetString(), (LPVOID*)&pVersionPtr, &uiVerLength))
				return E_OUTOFMEMORY;
		}
		if (uiVerLength > 0)
			result_ref = (LPCTSTR)pVersionPtr;
		return  hr__;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CApplication::CVersion::CVersion(void):
	m_hResult(OLE_E_BLANK),
	m_pVerInfo(NULL)
{
	static const DWORD dwPathLength  = _MAX_DRIVE + _MAX_DIR + _MAX_FNAME + _MAX_EXT;
	TCHAR szModulePath[dwPathLength] = {0};
	const DWORD dwResult = ::GetModuleFileName(NULL, szModulePath, _MAX_PATH);
	if ( !dwResult || ERROR_INSUFFICIENT_BUFFER == dwResult )
	{
		m_hResult = DISP_E_BUFFERTOOSMALL;
		return;
	}
	m_hResult = details::Version_GetRawPointer(m_pVerInfo, szModulePath);
}

CApplication::CVersion::CVersion(LPCTSTR pszModulePath):
	m_hResult(OLE_E_BLANK),
	m_pVerInfo(NULL)
{
	m_hResult = details::Version_GetRawPointer(m_pVerInfo, pszModulePath);
}

CApplication::CVersion::~CVersion(void)
{
	if (m_pVerInfo)
	{
		try
		{
			delete [] m_pVerInfo;
		}
		catch (...){}
		m_pVerInfo = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

::ATL::CAtlString CApplication::CVersion::Comments(void) const
{
	::ATL::CAtlString cs_comments;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("Comments"), cs_comments);
	if (S_OK != hr__)
		cs_comments = _T("");
	return cs_comments;
}

::ATL::CAtlString CApplication::CVersion::CompanyName(void) const
{
	::ATL::CAtlString cs_company;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("CompanyName"), cs_company);
	if (S_OK != hr__)
		cs_company = _T("");
	return cs_company;
}

::ATL::CAtlString CApplication::CVersion::CopyRight(void) const
{
	::ATL::CAtlString cs_cr;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("LegalCopyRight"), cs_cr);
	if (S_OK != hr__)
		cs_cr = _T("");
	return cs_cr;
}

::ATL::CAtlString CApplication::CVersion::CustomValue(LPCTSTR lpszValueName)const
{
	::ATL::CAtlString cs_value;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, lpszValueName, cs_value);
	if (S_OK != hr__)
		cs_value = _T("#n/a");
	return cs_value;
}

::ATL::CAtlString CApplication::CVersion::FileDescription(void) const
{
	::ATL::CAtlString cs_desc;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("FileDescription"), cs_desc);
	if (S_OK != hr__)
		cs_desc = _T("");
	return cs_desc;
}	

::ATL::CAtlString CApplication::CVersion::FileVersion(void) const
{
	::ATL::CAtlString cs_file_version;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("FileVersion"), cs_file_version);
	if (S_OK != hr__)
		cs_file_version = _T("");
	return  cs_file_version;
}

::ATL::CAtlString CApplication::CVersion::FileVersionFixed(void) const
{
	::ATL::CAtlString cs_file_ver("");
	VS_FIXEDFILEINFO fi = {0};
	const HRESULT hr__ = details::Version_FixedFileInfo(m_pVerInfo, fi);
	if (S_OK == hr__)
	{
		cs_file_ver.Format(_T("%d.%d.%d.%d"), 
			HIWORD(fi.dwFileVersionMS), LOWORD(fi.dwFileVersionMS),
			HIWORD(fi.dwFileVersionLS), LOWORD(fi.dwFileVersionLS));
	}
	return cs_file_ver;
}

HRESULT           CApplication::CVersion::GetLastResult(void)const
{
	return m_hResult;
}

::ATL::CAtlString CApplication::CVersion::InternalName(void) const
{
	::ATL::CAtlString cs_int_name;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("InternalName"), cs_int_name);
	if (S_OK != hr__)
		cs_int_name = _T("");
	return  cs_int_name;
}

::ATL::CAtlString CApplication::CVersion::OriginalFileName(void) const
{
	::ATL::CAtlString cs_origin;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("OriginalFileName"), cs_origin);
	if (S_OK != hr__)
		cs_origin = _T("");
	return  cs_origin;
}

::ATL::CAtlString CApplication::CVersion::ProductName(void) const
{
	::ATL::CAtlString cs_prod_name;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("ProductName"), cs_prod_name);
	if (S_OK != hr__)
		cs_prod_name = _T("");
	return  cs_prod_name;
}

::ATL::CAtlString CApplication::CVersion::ProductVersion(void) const
{
	::ATL::CAtlString cs_prod_version;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("ProductVersion"), cs_prod_version);
	if (S_OK != hr__)
		cs_prod_version = _T("");
	return  cs_prod_version;
}

::ATL::CAtlString CApplication::CVersion::ProductVersionFixed(void) const
{
	::ATL::CAtlString cs_prod_ver("");
	VS_FIXEDFILEINFO fi = {0};
	const HRESULT hr__ = details::Version_FixedFileInfo(m_pVerInfo, fi);
	if (S_OK == hr__)
	{
		cs_prod_ver.Format(_T("%d.%d.%d.%d"), 
			HIWORD(fi.dwProductVersionMS), LOWORD(fi.dwProductVersionMS),
			HIWORD(fi.dwProductVersionLS), LOWORD(fi.dwProductVersionLS));
	}
	return cs_prod_ver;
}

/////////////////////////////////////////////////////////////////////////////

CApplication::CCommandLine::CCommandLine(void)
{
	::ATL::CAtlString cs_cmd_line = ::GetCommandLine();
	INT n_count = 0;
	LPWSTR* pCmdArgs = ::CommandLineToArgvW(cs_cmd_line.GetString(), &n_count);
	if (n_count && pCmdArgs)
	{
		try
		{
			m_module_full_path = pCmdArgs[0];

			bool bKey = false;
			::ATL::CAtlString cs_key;
			::ATL::CAtlString cs_arg;

			for (INT i__ = 1; i__ < n_count; i__+= 1)
			{
				::ATL::CAtlString cs_val = pCmdArgs[i__];
				bKey = (0 == cs_val.Find(_T("-"))
				     || 0 == cs_val.Find(_T("/")));

				if (bKey)
				{
					if (!cs_key.IsEmpty()) // the previous key is not saved yet;
						m_args.insert(::std::make_pair(cs_key, cs_arg));

					cs_key = pCmdArgs[i__]; cs_key.Replace(_T("-"), _T("")); cs_key.Replace(_T("/"), _T(""));
					cs_arg = _T("");
				}
				else
				{
					cs_arg+= pCmdArgs[i__];
				}

				const bool bLast = (i__ == n_count - 1);
				if (bLast && !cs_key.IsEmpty())
					m_args.insert(::std::make_pair(cs_key, cs_arg));
			}
		} catch (::std::bad_alloc&){}
	}
	if (pCmdArgs)
	{
		::LocalFree(pCmdArgs); pCmdArgs = NULL;
	}
}

CApplication::CCommandLine::~CCommandLine(void)
{
	m_args.clear();
}

/////////////////////////////////////////////////////////////////////////////

#if (1)
HRESULT             CApplication::CCommandLine::Append(LPCTSTR pszName, LPCTSTR lpszValue)
{
	HRESULT hr_ = S_OK;
	try {
		m_args.insert(::std::make_pair(
					CAtlString(pszName), CAtlString(lpszValue)
				));
	}
	catch(::std::bad_alloc&){
		hr_ = E_OUTOFMEMORY;
	}
	return hr_;
}
#endif

::ATL::CAtlString   CApplication::CCommandLine::Argument(LPCTSTR pszName)const
{
	TCmdLineArguments::const_iterator it__ = m_args.find(::ATL::CAtlString(pszName));
	if (it__ == m_args.end())
		return ::ATL::CAtlString();
	else
		return it__->second;
}

LONG                CApplication::CCommandLine::Argument(LPCTSTR pszName, const LONG _def_val)const
{
	TCmdLineArguments::const_iterator it__ = m_args.find(::ATL::CAtlString(pszName));
	if (it__ == m_args.end())
		return _def_val;
	else
		return ::_tstol(it__->second);
}

TCmdLineArguments   CApplication::CCommandLine::Arguments(void)const
{
	return m_args;
}

VOID                CApplication::CCommandLine::Clear(void)       { if (m_args.empty() == false) m_args.clear(); }
INT                 CApplication::CCommandLine::Count(void)const  { return (INT)m_args.size(); }

bool                CApplication::CCommandLine::Has(LPCTSTR pArgName)const
{
	TCmdLineArguments::const_iterator it__ = m_args.find(::ATL::CAtlString(pArgName));
	return (it__ != m_args.end());
}

::ATL::CAtlString   CApplication::CCommandLine::ModuleFullPath(void)const { return m_module_full_path; }

::ATL::CAtlString   CApplication::CCommandLine::ToString(void)const
{
	CAtlString cs_args;
	bool bFirstIter = true;
	for (TCmdLineArguments::const_iterator it_ = m_args.begin(); it_ != m_args.end(); ++it_)
	{
		if (!bFirstIter)
		cs_args += _T("; ");
		cs_args += _T("name=");
		cs_args += it_->first;
		cs_args += _T("; ");
		cs_args += _T("value=");
		cs_args += it_->second;

		bFirstIter = false;
	}
	return cs_args;
}

/////////////////////////////////////////////////////////////////////////////

CApplication::CApplication(void):
	m_hResult(OLE_E_BLANK),
	m_object(*this)
{
	LPCTSTR p = this->GetName();
	if (p)
	{
		if (0 != m_app_name.CompareNoCase(_T("")))
			m_hResult = S_OK;
	}
}

CApplication::~CApplication(void)
{
}

/////////////////////////////////////////////////////////////////////////////

const 
CApplication::CCommandLine& CApplication::CommandLine(void) const
{
	return m_cmd_line;
}

#if defined(_DEBUG)
CApplication::CCommandLine& CApplication::CommandLine(void)
{
	return m_cmd_line;
}
#endif

/////////////////////////////////////////////////////////////////////////////

CAtlString  CApplication::GetFileName(const bool bNoExtension) const
{
	::ATL::CAtlString cs_file;
	HRESULT hr_ = CApplication::GetFullPath(cs_file);
	if (S_OK != hr_)
		return  cs_file;
	const INT n_ptr = cs_file.ReverseFind(_T('\\'));
	if (-1 != n_ptr)
	{
		cs_file = cs_file.Right(cs_file.GetLength() - n_ptr - 1);
		if (bNoExtension)
		{
			const INT n_pos = cs_file.ReverseFind(_T('.'));
			if (-1 != n_pos)
				cs_file = cs_file.Left(n_pos);
		}
	}
	return cs_file;
}

HRESULT     CApplication::GetLastResult(void) const
{
	return m_hResult;
}

LPCTSTR     CApplication::GetName(void) const
{
	if (m_app_name.IsEmpty())
	{
		m_app_name = m_version.ProductName();
	}
	return m_app_name.GetString();
}

HRESULT     CApplication::GetPath(::ATL::CAtlString& __in_out_ref) const
{
	HRESULT hr_ = CApplication::GetFullPath(__in_out_ref);
	if (S_OK != hr_)
		return  hr_;
	const INT n_ptr = __in_out_ref.ReverseFind(_T('\\'));
	if (-1 != n_ptr)
	{
		__in_out_ref = __in_out_ref.Left(n_ptr + 1); // we have to include the trailing backslash
	}
	else
		hr_ = E_UNEXPECTED;
	return  hr_;
}

HRESULT     CApplication::GetPathFromAppFolder(LPCTSTR lpszPattern, ::ATL::CAtlString& __in_out_ref) const
{
	CCurrentFolder curr_;
	__in_out_ref = curr_.GetPathFromPattern(lpszPattern);
	HRESULT hr_  = S_OK; if (__in_out_ref.IsEmpty()) hr_ = curr_.Error();
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CApplication::GetFullPath(::ATL::CAtlString& cs_path)
{
	TCHAR buffer[_LONG_PATH] = {0};
	const BOOL ret__ = ::GetModuleFileName(NULL, buffer, _countof(buffer));
	if (NULL == ret__)
		return HRESULT_FROM_WIN32(::GetLastError());
	cs_path = buffer;
	return S_OK;
}

bool        CApplication::Is64bit(VOID)
{
#if defined(WIN64)
	return true;
#else
	return false;
#endif
}

bool        CApplication::IsRelatedToAppFolder(LPCTSTR lpszPath)
{
	CCurrentFolder curr_;
	return curr_.IsRelative(lpszPath);
}

/////////////////////////////////////////////////////////////////////////////

const CApplication::CObject&  CApplication::Instance(VOID)const
{
	return m_object;
}

CApplication::CObject&        CApplication::Instance(VOID)
{
	return m_object;
}

const CApplication::CVersion& CApplication::Version(VOID)const
{
	return m_version;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace user32
{
	CApplication&  GetAppObjectRef(void)
	{
		static CApplication the_app;
		return the_app;
	}
}}