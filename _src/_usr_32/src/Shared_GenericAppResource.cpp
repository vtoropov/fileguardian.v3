/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Jun-2016 at 3:40:16p, GMT+7, Phuket, Rawai, Friday;
	This is Shared User32 Wrapper Generic Application Resource class(es) implementation file.
*/
#include "StdAfx.h"
#include "Shared_GenericAppResource.h"

using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace user32 { namespace details 
{
	static HICON Application_LoadIcon(const UINT nIconResId, const bool bTreatAsLargeIcon)
	{
		const SIZE szIcon = {
					::GetSystemMetrics(bTreatAsLargeIcon ? SM_CXICON : SM_CXSMICON), 
					::GetSystemMetrics(bTreatAsLargeIcon ? SM_CYICON : SM_CYSMICON)
				};
		const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
		const HICON hIcon = (HICON)::LoadImage(hInstance, MAKEINTRESOURCE(nIconResId), 
			IMAGE_ICON, szIcon.cx, szIcon.cy, LR_DEFAULTCOLOR);
		return hIcon;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CApplicationIconLoader::CApplicationIconLoader(LPCTSTR pszIconFile, const INT nIconIndex) : m_large(NULL), m_small(NULL)
{
	if (::_tcslen(pszIconFile))
	{
		::ExtractIconEx(pszIconFile, nIconIndex, &m_large, &m_small, 1);
	}
}

CApplicationIconLoader::CApplicationIconLoader(const UINT nIconId) : m_large(NULL), m_small(NULL)
{
	m_large = details::Application_LoadIcon(nIconId, true);
	m_small = details::Application_LoadIcon(nIconId, false);
}

CApplicationIconLoader::CApplicationIconLoader(const UINT nSmallIconId, const UINT nLargeIconId) : m_large(NULL), m_small(NULL)
{
	bool bTreatAsLargeIcon = true;
	m_large = details::Application_LoadIcon(nLargeIconId, bTreatAsLargeIcon); bTreatAsLargeIcon = false;
	m_small = details::Application_LoadIcon(nSmallIconId, bTreatAsLargeIcon);
}

CApplicationIconLoader::~CApplicationIconLoader(void)
{
	if (NULL != m_large)
	{
		::DestroyIcon(m_large); m_large = NULL;
	}
	if (NULL != m_small)
	{
		::DestroyIcon(m_small); m_small = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

HICON   CApplicationIconLoader::DetachLargeIcon(void)
{
	HICON tmp_ = m_large;
	m_large = NULL;
	return tmp_;
}

HICON   CApplicationIconLoader::DetachSmallIcon(void)
{
	HICON tmp_ = m_small;
	m_small = NULL;
	return tmp_;
}

HICON   CApplicationIconLoader::GetLargeIcon(void) const
{
	return m_large;
}

HICON   CApplicationIconLoader::GetSmallIcon(void) const
{
	return m_small;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace user32 { namespace details
{
	class  ApplicationCursor_Data
	{
	public:
		HCURSOR  m_hNewCursor;
		HCURSOR  m_hOldCursor;
		bool     m_InUse;
	public:
		ApplicationCursor_Data(void): m_hNewCursor(NULL), m_hOldCursor(NULL), m_InUse(false)
		{
		}
	};

	static ApplicationCursor_Data& ApplicationCursor_GetDataRef(void)
	{
		static ApplicationCursor_Data data;
		return data;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CApplicationCursor::CApplicationCursor(LPCTSTR lpstrCursor) : m_resource_owner(false)
{
	details::ApplicationCursor_Data& data_ref = details::ApplicationCursor_GetDataRef();
	if (false == data_ref.m_InUse)
	{
		data_ref.m_InUse = m_resource_owner = true;
		data_ref.m_hNewCursor = ::LoadCursor(NULL, lpstrCursor);
		data_ref.m_hOldCursor = ::SetCursor(data_ref.m_hNewCursor);
	}
}

CApplicationCursor::~CApplicationCursor(void)
{
	if (true == m_resource_owner)
	{
		details::ApplicationCursor_Data& data_ref = details::ApplicationCursor_GetDataRef();
		::SetCursor(data_ref.m_hOldCursor);
		data_ref.m_InUse = m_resource_owner = false;
	}
}

/////////////////////////////////////////////////////////////////////////////

CGenericResourceLoader::CGenericResourceLoader(void)
{
	m_error.Source(_T("CGenericResourceLoader"));
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef    CGenericResourceLoader::Error(void)const
{
	return m_error;
}

HRESULT      CGenericResourceLoader::LoadRcDataAsUtf8(const UINT uResId, CAtlString& _buffer) {
	CRawData data_;
	HRESULT hr_ = this->LoadRcDataAsUtf8(uResId, data_);
	if (FAILED(hr_))
		return hr_;

	CAtlStringA cs_ansi;
	//
	// a buffer must be interpreted as UTF-8 character sequence first;
	// after that the buffer can be converted to UTF-16 (unicode) string; 
	//
	hr_ = data_.ToStringUtf8(cs_ansi);
	if (FAILED(hr_))
		m_error = data_.Error();
	else
		_buffer = cs_ansi;

	return hr_;
}

HRESULT      CGenericResourceLoader::LoadRcDataAsUtf8(const UINT uResId, CRawData& _buffer)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const HMODULE hModule = ::GetModuleHandle(NULL);
	if (NULL == hModule)
		return (m_error = ::GetLastError());

	const HRSRC hResInfo = ::FindResource(hModule, MAKEINTRESOURCE(uResId), _T("RT_RCDATA"));
	if (NULL == hResInfo)
		return (m_error = ::GetLastError());

	const DWORD  dwSize = ::SizeofResource(hModule, hResInfo);
	if (dwSize < 1)
		return (m_error = ::GetLastError());

	const HGLOBAL hResource = ::LoadResource(hModule, hResInfo);
	if (NULL == hResource)
		return (m_error = ::GetLastError());

	const PBYTE pData = reinterpret_cast<PBYTE>(::LockResource(hResource));
	HRESULT hr_ = _buffer.Create(
						pData,
						dwSize
					);
	if (FAILED(hr_))
		m_error = _buffer.Error();

	return m_error;
}