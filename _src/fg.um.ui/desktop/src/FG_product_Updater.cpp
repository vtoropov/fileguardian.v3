/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Apr-2017 at 03:24:18a, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian product updater class implementation file.
*/
#include "StdAfx.h"
#include "FG_product_Updater.h"
#include "FW_download_Dlg.h"
#include "FG_Generic_Defs.h"

using namespace fw::desktop;
using namespace fw::desktop::UI::dialogs;

#include "Shared_FS_GenericFolder.h"
#include "Shared_GenericAppObject.h"

using namespace shared::ntfs;
using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

CProductUpdater::CProductUpdater(void)
{
	m_error.Source(_T("CProductUpdater"));
}

CProductUpdater::~CProductUpdater(void)
{
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef       CProductUpdater::Error(void)const
{
	return m_error;
}

HRESULT         CProductUpdater::StartUpdatingProcess(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CSpecialFolder special_;

	CAtlString cs_source = _FG_UPDATE_DOWNLOAD_URL;
	CAtlString cs_target = special_.UserTemporary() + _FG_UPDATE_DOWNLOAD_FILE;

	CDownloadDlg dlg_(cs_source, cs_target);

	dlg_.DoModal();

	HRESULT hr_ = dlg_.Error();
	if (FAILED(hr_))
		return (m_error = dlg_.Error());

	if (IDOK != AtlMessageBox(
			NULL,
			_T("FG installer has been downloaded and is ready to run. Do you want to proceed?"),
			_T("FG Updater"),
			MB_ICONEXCLAMATION | MB_OKCANCEL
		))
		return m_error;

	CAtlString cs_agent;
	const CApplication& the_app = GetAppObjectRef();
	the_app.GetPath(cs_agent);
	cs_agent += _T("FW_agent.exe");

#if defined(_DEBUG)
	cs_target = _T("E:\\Prog_man\\_B2B_\\oDesk\\Initec\\Project.Setup\\output\\fg-setup_v1.5.0.1_release_x86.exe");
#else
#endif

	CAtlString cs_cmd_line;
	cs_cmd_line += _T("/run \"");
	cs_cmd_line += cs_target;
	cs_cmd_line += _T("\"");

	SHELLEXECUTEINFO info_ = {0};
	info_.cbSize           = sizeof(SHELLEXECUTEINFO);
	info_.lpVerb           = _T("open");
	info_.lpFile           = cs_agent;
	info_.lpParameters     = cs_cmd_line;
	info_.hwnd             = HWND_DESKTOP;
	info_.nShow            = SW_NORMAL;
	const BOOL result_     = ::ShellExecuteEx(&info_);
	if (!result_)
		return (m_error = ::GetLastError());

	CWindow top_ = ::GetActiveWindow();
	if (top_)
		::EndDialog(top_, IDOK);

	return m_error;
}