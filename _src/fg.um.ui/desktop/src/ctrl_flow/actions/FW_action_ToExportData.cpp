/*
	Created by Tech_dog (VToropov) on 18-May-2016 at 10:19:29pm, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian desktop data export asynchronous action class implementation file.
*/
#include "StdAfx.h"
#include "FW_action_ToExportData.h"

using namespace fw::desktop::ctrl_flow;
using namespace fw::desktop::data;

#include "Shared_SystemCore.h"

using namespace shared::lite::sys_core;

/////////////////////////////////////////////////////////////////////////////

CActionToExportData::CActionToExportData(ICsvDataProviderCallback& _sink): m_sink(_sink)
{
	TBase::m_error.Source(_T("CActionToExportData"));
}

/////////////////////////////////////////////////////////////////////////////

bool    CActionToExportData::CsvDataProvider_CanContinue(void)
{
	return !TBase::m_crt.IsStopped();
}

VOID    CActionToExportData::CsvDataProvider_OnComplete(void)
{
	m_sink.CsvDataProvider_OnComplete();
}

VOID    CActionToExportData::CsvDataProvider_OnError(const CSysError _error)
{
	m_sink.CsvDataProvider_OnError(_error);
}

VOID    CActionToExportData::CsvDataProvider_OnProgress(const INT _ndx, const INT _total)
{
	m_sink.CsvDataProvider_OnProgress(_ndx, _total);
}

VOID    CActionToExportData::CsvDataProvider_OnStart(const INT _total)
{
	m_sink.CsvDataProvider_OnStart(_total);
}

HWND    CActionToExportData::CsvDataProvider_RequestSourceCtrl(void)
{
	return m_sink.CsvDataProvider_RequestSourceCtrl();
}

CAtlString CActionToExportData::CsvDataProvider_RequestTargetPath(void)
{
	return m_sink.CsvDataProvider_RequestTargetPath();
}

/////////////////////////////////////////////////////////////////////////////

VOID    CActionToExportData::ThreadFunction(VOID)
{
	CCoInitializer com_(false);

	const HWND source_ = this->CsvDataProvider_RequestSourceCtrl();
	CAtlString target_ = this->CsvDataProvider_RequestTargetPath();

	::Sleep(100);

	CCsvDataProvider prov_;

	prov_.Save(source_, target_, this);

	::SetEvent(m_crt.EventObject());
}