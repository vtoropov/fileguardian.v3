#ifndef _FWACTIONTOEXPORTDATA_H_75A63FB2_D18F_410b_80DB_A9AE39CE83D6_INCLUDED
#define _FWACTIONTOEXPORTDATA_H_75A63FB2_D18F_410b_80DB_A9AE39CE83D6_INCLUDED
/*
	Created by Tech_dog (VToropov) on 18-May-2016 at 10:12:03pm, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian desktop data export asynchronous action class declaration file.
*/
#include "Shared_SystemThreadPool.h"
#include "FW_csv_Provider.h"

namespace fw { namespace desktop { namespace ctrl_flow
{
	using shared::lite::common::CSysError;
	using shared::lite::sys_core::CThreadBase;

	using fw::desktop::data::ICsvDataProviderCallback;

	class CActionToExportData :
		public  CThreadBase,
		public  ICsvDataProviderCallback
	{
		typedef CThreadBase TBase;
	private:
		ICsvDataProviderCallback& m_sink;
	public:
		CActionToExportData(ICsvDataProviderCallback&);
	private: // ICsvDataProviderCallback
		virtual bool    CsvDataProvider_CanContinue(void) override sealed;
		virtual VOID    CsvDataProvider_OnComplete(void) override sealed;
		virtual VOID    CsvDataProvider_OnError(const CSysError) override sealed;
		virtual VOID    CsvDataProvider_OnProgress(const INT _ndx, const INT _total) override sealed;
		virtual VOID    CsvDataProvider_OnStart(const INT _total) override sealed;
		virtual HWND    CsvDataProvider_RequestSourceCtrl(void) override sealed;
		virtual
		     CAtlString CsvDataProvider_RequestTargetPath(void) override sealed;
	private:
		virtual VOID    ThreadFunction(VOID) override sealed;
	};
}}}

#endif/*_FWACTIONTOEXPORTDATA_H_75A63FB2_D18F_410b_80DB_A9AE39CE83D6_INCLUDED*/