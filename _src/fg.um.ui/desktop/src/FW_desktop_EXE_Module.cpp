/*
	Created by Tech_dog (VToropov) on 15-Jan-2016 at 2:00:32am, GMT+7, Phuket, Rawai, Friday;
	This is File Watcher Desktop Application Entry Point implementation file.
*/
#include "StdAfx.h"
#include "UIX_GdiProvider.h"
#include "FW_desktop_MainFrame.h"

using namespace fw::desktop::UI;

#include "Shared_SystemCore.h"
#include "Shared_GenericAppObject.h"

using namespace shared::lite::common;
using namespace shared::lite::sys_core;
using namespace shared::user32;

#include "Shared_SystemSecurity.h"

using namespace shared::lite::security;

CAppModule _Module;

#include "FW_pass_Dlg.h"

using namespace fw::desktop::UI::dialogs;

#include "FG_Generic_Defs.h"

#define _MODAL
/////////////////////////////////////////////////////////////////////////////

INT RunModal(VOID)
{
	INT nRet = 0;

	CApplication& the_app = GetAppObjectRef();

	// the global mutex name must be consistent with the installer app one;
	// i.e. AppMutex  = Global\file_watcher_sft
	CSysError err_obj = the_app.Instance().RegisterSingleton(_FW_GLOBAL_MUTEX_DESKTOP);

	if (err_obj.GetCode() == ERROR_OBJECT_ALREADY_EXISTS)
		return nRet;
	else
	{
		CPasswordDlg dlg_;
#if !defined(_DEBUG)
		INT_PTR res_ = dlg_.DoModal();
#else
		INT_PTR res_ = IDOK;
#endif
		switch (res_)
		{
		case IDOK:
			{
				CMainFrame frame;
				res_ = frame.DoModal();
			} break;
		}
	}
	return nRet;
}

INT RunModeless(VOID)
{
	INT result_ = 0;

	CApplication& the_app = GetAppObjectRef();
	CSysError err_obj = the_app.Instance().RegisterSingleton(_FW_GLOBAL_MUTEX_DESKTOP);

	if (err_obj.GetCode() == ERROR_OBJECT_ALREADY_EXISTS)
		return result_;
	else
	{
		CPasswordDlg dlg_;
#if !defined(_DEBUG)
		INT_PTR res_ = dlg_.DoModal();
#else
		INT_PTR res_ = IDOK;
#endif
		if (IDOK != res_)
			return result_;

		::WTL::CMessageLoop pump_;
		_Module.AddMessageLoop(&pump_);

		CMainFrame frame;
		const BOOL bResult = frame.Create();
		if (bResult)
		{
			result_ = pump_.Run();
		}
		else
		{
			err_obj = ::GetLastError();
			CEventJournal::LogError(err_obj);
			result_ = -1;
		}
		_Module.RemoveMessageLoop();
	}
	return result_;
}

INT WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpstrCmdLine, INT nCmdShow)
{
	hInstance; hPrevInstance; lpstrCmdLine; nCmdShow;

	CCoInitializer com(false);
	HRESULT hr_ = ::CoInitializeSecurity(
						NULL,
						-1,
						NULL,
						NULL,
						RPC_C_AUTHN_LEVEL_NONE,
						RPC_C_IMP_LEVEL_IDENTIFY,
						NULL,
						EOAC_NONE,
						NULL
					);
	ATLASSERT(SUCCEEDED(hr_));
	/*
		Tech_dog commented on 09-Feb-2010 at 12:47:50pm:
		________________________________________________
		we need to assign lib id to link ATL DLL statically,
		otherwise we get annoying fkn message "Did you forget to pass the LIBID to CComModule::Init?"
	*/
	_Module.m_libid = LIBID_ATLLib;
	INT nResult = 0;
	// this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
	::DefWindowProc(NULL, 0, 0, 0L);

	AtlInitCommonControls(ICC_BAR_CLASSES);	// add flags to support other controls

	hr_ = _Module.Init(NULL, hInstance);
	if(!SUCCEEDED(hr_))
	{
		ATLASSERT(FALSE);
		return -1;
	}

	CSecurityProvider provider_;
	const bool bIsAdminRole = provider_.IsAdminRole();

	if (!bIsAdminRole)
	{
		hr_ = provider_.Elevate();
		if (!FAILED(hr_))
			return nResult;
	}

	ex_ui::draw::CGdiPlusLibLoader  gdi_loader;
#if defined(_MODAL)
	nResult = ::RunModal();
#else
	nResult = ::RunModeless();
#endif
	_Module.Term();

	return nResult;
}