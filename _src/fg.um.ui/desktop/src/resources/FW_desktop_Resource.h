//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FW_desktop_Resource.rc
//
// Created by Tech_dog (ebontrop@gmail.com) on 14-Jan-2016 at 11:14:36pm, GMT+7, Phuket, Rawai, Thursday
// This is File Guardian Desktop Resource Declaration file.
//

#define IDR_FWDESKTOP_MAIN_DLG_ICON              101
#define IDR_FWDESKTOP_MAIN_DLG_HEADER            103
#define IDD_FWDESKTOP_MAIN_DLG                   107
#define IDC_FWDESKTOP_MAIN_DLG_TABSET            111
#define IDC_FWDESKTOP_MAIN_DLG_STATUS            113
#define IDR_FWDESKTOP_MAIN_DLG_IMAGES            115
#define IDR_FWDESKTOP_MAIN_DLG_INFO24            117
#define IDR_FWDESKTOP_MAIN_DLG_INFO32            119
#define IDR_FWDESKTOP_MAIN_DLG_WARN16            121
#define IDR_FWDESKTOP_MAIN_DLG_WARN20            122
#define IDR_FWDESKTOP_MAIN_DLG_WARN24            123
#define IDR_FWDESKTOP_MAIN_DLG_MENUBAR           125
#define IDR_FWDESKTOP_MENU_ITM_OPEN              127
#define IDR_FWDESKTOP_MENU_ITM_PRINT             129
#define IDR_FWDESKTOP_MENU_ITM_EXIT              131
#define IDR_FWDESKTOP_MENU_ITM_FILTER            133
#define IDR_FWDESKTOP_MENU_ITM_REFRESH           135
#define IDR_FWDESKTOP_MENU_ITM_EXPORT            137
#define IDR_FWDESKTOP_MENU_ITM_VIEW_0            139
#define IDR_FWDESKTOP_MENU_ITM_VIEW_1            141
#define IDR_FWDESKTOP_MENU_ITM_VIEW_2            143
#define IDR_FWDESKTOP_MENU_ITM_VIEW_3            145
#define IDR_FWDESKTOP_MENU_ITM_VIEW_4            147
#define IDR_FWDESKTOP_MENU_ITM_HELP              149
#define IDR_FWDESKTOP_MENU_ITM_UPDATE            151
#define IDR_FWDESKTOP_MENU_ITM_HOME              153
#define IDR_FWDESKTOP_MENU_ITM_ABOUT             155
#define IDR_FWDESKTOP_MAIN_DLG_ACCEL             157
#define IDR_FGDESKTOP_MENU_ITM_TOTALS            159
#define IDR_FGDESKTOP_MAIN_MNU_FILE              161
#define IDR_FGDESKTOP_MAIN_MNU_DATA              163
#define IDR_FGDESKTOP_MAIN_MNU_VIEW              165
#define IDR_FGDESKTOP_MAIN_MNU_HELP              167
#define IDR_FGDESKTOP_MAIN_MNU_LOGS              169
#define IDR_FGDESKTOP_MENU_ITM_OPTIONS           171
#define IDR_FWDESKTOP_MAIN_DLG_WARN16_EXT        173
#define IDS_FWDESKTOP_NO_PRINTER                 175
#define IDR_FGDESKTOP_NENU_ITM_PROPERS           177

/////////////////////////////////////////////////////////////////////////////
//
// File Watcher Desktop Log Data Tab Page
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_FWDESKTOP_LOG_PAGE                  1001
#define IDC_FWDESKTOP_LOG_IMAGE                 1003
#define IDR_FWDESKTOP_LOG_IMAGE                 1005
#define IDC_FWDESKTOP_LOG_TITLE                 1007
#define IDC_FWDESKTOP_LOG_DATE_0                1009
#define IDC_FWDESKTOP_LOG_DATE_1                1011
#define IDC_FWDESKTOP_LOG_ACTION                1013
#define IDC_FWDESKTOP_LOG_TABLE                 1015
#define IDC_FWDESKTOP_LOG_FILTER                1017
#define IDC_FWDESKTOP_LOG_REFRESH               1019
#define IDC_FWDESKTOP_LOG_ARCHES                1021
#define IDC_FWDESKTOP_LOG_PRINTV                1023

/////////////////////////////////////////////////////////////////////////////
//
// File Watcher Desktop Settings Tab Page
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_FWDESKTOP_SETUP_PAGE                2001
#define IDC_FWDESKTOP_SETUP_MODS                2005
#define IDC_FWDESKTOP_SETUP_KEY                 2007
#define IDC_FWDESKTOP_SETUP_KSAVE               2009
#define IDR_FWDESKTOP_SETUP_PASS                2011
#define IDC_FWDESKTOP_SETUP_LOCK                2013
#define IDC_FWDESKTOP_SETUP_PWD_OLD             2015
#define IDC_FWDESKTOP_SETUP_PWD_NEW             2017
#define IDC_FWDESKTOP_SETUP_PWD_CNF             2019
#define IDC_FWDESKTOP_SETUP_PWD_SET             2021
#define IDC_FWDESKTOP_SETUP_WARN                2023
#define IDC_FWDESKTOP_SETUP_KINFO               2025
#define IDC_FWDESKTOP_SETUP_REPO                2027
#define IDC_FWDESKTOP_SETUP_REST                2029

/////////////////////////////////////////////////////////////////////////////
//
// File Watcher Desktop Service Management Tab Page
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_FWDESKTOP_SERVICE_PAGE              2101
#define IDC_FWDESKTOP_SERVICE_LOGO              2103
#define IDR_FWDESKTOP_SERVICE_LOGO              2105
#define IDC_FWDESKTOP_SERVICE_SETUP             2107
#define IDC_FWDESKTOP_SERVICE_STATE             2109
#define IDC_FWDESKTOP_SERVICE_INSTL             2111
#define IDC_FWDESKTOP_SERVICE_CTRL              2113
#define IDR_FWDESKTOP_SERVICE_WATCH             2115
#define IDC_FWDESKTOP_SERVICE_WATCH             2117
#define IDC_FWDESKTOP_SERVICE_WLIST             2119
#define IDC_FWDESKTOP_SERVICE_WFADD             2121
#define IDC_FWDESKTOP_SERVICE_WFEXC             2123
#define IDC_FWDESKTOP_SERVICE_WFDEL             2125
#define IDC_FWDESKTOP_SERVICE_RFRSH             2127
#define IDC_FWDESKTOP_SERVICE_INFO              2129
#define IDC_FWDESKTOP_SERVICE_UPDT              2131
#define IDC_FWDESKTOP_SERVICE_STAT              2133
#define IDC_FWDESKTOP_SERVICE_BROWSE            2135
#define IDC_FWDESKTOP_SERVICE_FOLDER            2137
#define IDC_FWDESKTOP_SERVICE_REMDEV            2139

/////////////////////////////////////////////////////////////////////////////
//
// File Guardian Database Management Tab Page
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_FWDESKTOP_DBMAN_PAGE                2201
#define IDC_FWDESKTOP_DBMAN_TRACK               2203
#define IDC_FWDESKTOP_ARCH_THRSH                2205
#define IDC_FWDESKTOP_ARCH_TRUNC                2207
#define IDC_FWDESKTOP_ARCH_ENBLD                2209
#define IDC_FWDESKTOP_ARCH_VOLMS                2211
#define IDC_FWDESKTOP_ARCH_FOLDER               2213
#define IDC_FWDESKTOP_ARCH_BROWSE               2215
#define IDC_FWDESKTOP_ARCH_WARN                 2217
#define IDC_FWDESKTOP_ARCH_APPLY                2219
#define IDC_FWDESKTOP_ARCH_SETDEF               2221
#define IDC_FWDESKTOP_ARCH_VALID                2223
#define IDC_FWDESKTOP_DBMAN_FOLDER              2225
#define IDC_FWDESKTOP_DBMAN_BROWSE              2227
#define IDC_FWDESKTOP_DBMAN_FNAME               2229
#define IDC_FWDESKTOP_DBMAN_CREATE              2231

/////////////////////////////////////////////////////////////////////////////
//
// File Guardian License Management Tab Page
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_FWDESKTOP_LIC_PAGE                  2301
#define IDR_FWDESKTOP_LIC_LOGO                  2303
#define IDC_FWDESKTOP_LIC_LOGO                  2305
#define IDC_FWDESKTOP_LIC_INSTALL               2307
#define IDC_FWDESKTOP_LIC_SERIAL                2309
#define IDC_FWDESKTOP_LIC_PERIOD                2311
#define IDC_FWDESKTOP_LIC_TIME                  2313
#define IDC_FWDESKTOP_LIC_LEFT                  2315
#define IDC_FWDESKTOP_LIC_STATUS                2317
#define IDC_FWDESKTOP_LIC_REFRESH               2319
#define IDC_FWDESKTOP_LIC_NEW_0                 2321
#define IDC_FWDESKTOP_LIC_NEW_1                 2323
#define IDC_FWDESKTOP_LIC_NEW_2                 2325
#define IDC_FWDESKTOP_LIC_NEW_3                 2327
#define IDC_FWDESKTOP_LIC_APPLY                 2329
#define IDC_FWDESKTOP_LIC_DESC                  2333
#define IDC_FWDESKTOP_LIC_INFO                  2335

#define IDC_FWDESKTOP_LIC_V_IMG                 2345 // new version image
#define IDC_FWDESKTOP_LIC_V_MSG                 2347 // new version message
#define IDC_FWDESKTOP_LIC_V_BTN                 2349 // update button

/////////////////////////////////////////////////////////////////////////////
//
// Data Filter Setup Dialog
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_FWDESKTOP_FILTER_DLG                3001
#define IDR_FWDESKTOP_FILTER_DLG_HEADER         3003
#define IDC_FWDESKTOP_FILTER_REM                3005

/////////////////////////////////////////////////////////////////////////////
//
// Data Filter Dialog General Tab Page
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_FWDESKTOP_FILTER_GEN_PAGE           3101
#define IDC_FWDESKTOP_FILTER_GEN_FROM           3103
#define IDC_FWDESKTOP_FILTER_GEN_UPTO           3105
#define IDC_FWDESKTOP_FILTER_GEN_ACT            3107
#define IDC_FWDESKTOP_FILTER_GEN_PATT           3109
#define IDC_FWDESKTOP_FILTER_GEN_INFO           3111
#define IDC_FWDESKTOP_FILTER_GEN_RNGE           3113
#define IDC_FWDESKTOP_FILTER_GEN_SQL3           3115
#define IDR_FWDESKTOP_FILTER_GEN_SQL3           3117

/////////////////////////////////////////////////////////////////////////////
//
// Data Filter Dialog Advanced Tab Page
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_FWDESKTOP_FILTER_ADV_PAGE           3201
#define IDC_FWDESKTOP_FILTER_ADV_RCNT           3203
#define IDC_FWDESKTOP_FILTER_ADV_NUMB           3205
#define IDC_FWDESKTOP_FILTER_ADV_INFO           3207
#define IDC_FWDESKTOP_FILTER_ADV_FLDR           3209
#define IDC_FWDESKTOP_FILTER_ADV_RPRT           3211
#define IDC_FWDESKTOP_FILTER_ADV_CLR            3213

/////////////////////////////////////////////////////////////////////////////
//
// Password Dialog
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_FWDESKTOP_PASS_DLG                  3301
#define IDR_FWDESKTOP_PASS_DLG_HEADER           3303
#define IDC_FWDESKTOP_PASS_DLG_PWD              3305
#define IDC_FWDESKTOP_PASS_DLG_WARN             3307
#define IDC_FWDESKTOP_PASS_DLG_MSG              3309

/////////////////////////////////////////////////////////////////////////////
//
// About Dialog
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_FWDESKTOP_ABOUT_DLG                 3401
#define IDR_FWDESKTOP_ABOUT_DLG_HEADER          3403
#define IDC_FWDESKTOP_ABOUT_DLG_LOGO            3405
#define IDR_FWDESKTOP_ABOUT_DLG_LOGO            3407
#define IDC_FWDESKTOP_ABOUT_DLG_LAW             3409
#define IDS_FWDESKTOP_ABOUT_DLG_LAW             3411
#define IDR_FWDESKTOP_ABOUT_DLG_PARA            3413
#define IDC_FWDESKTOP_ABOUT_DLG_PARA            3415
#define IDC_FWDESKTOP_ABOUT_DLG_COPY            3417
#define IDC_FWDESKTOP_ABOUT_DLG_VERS            3419
#define IDC_FWDESKTOP_ABOUT_DLG_RELS            3421

/////////////////////////////////////////////////////////////////////////////
//
// Data Export Dialog
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_FWDESKTOP_EXP_DLG                   3501
#define IDC_FWDESKTOP_EXP_LABEL                 3503
#define IDC_FWDESKTOP_EXP_PROG                  3505

/////////////////////////////////////////////////////////////////////////////
//
// Installer Download Dialog
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_FWDESKTOP_UPD_DLG                   3601
#define IDC_FWDESKTOP_UPD_LABEL                 3603
#define IDC_FWDESKTOP_UPD_PROG                  3605

/////////////////////////////////////////////////////////////////////////////
//
// File Event Options Dialog
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_FGDESKTOP_OPTS_DLG                  3701
#define IDR_FWDESKTOP_OPTS_DLG_HEADER           3703
#define IDC_FWDESKTOP_OPTS_DLG_TABSET           3705
#define IDD_FGDESKTOP_OPTS_GEN_PAGE             3707
#define IDC_FGDESKTOP_OPTS_GEN_FREQ             3709
#define IDC_FGDESKTOP_OPTS_GEN_SECS             3711
#define IDC_FWDESKTOP_OPTS_GEN_INFO             3713
#define IDC_FWDESKTOP_OPTS_GEN_STAT             3715
#define IDC_FWDESKTOP_OPTS_GEN_STAT_V           3717
#define IDC_FWDESKTOP_OPTS_GEN_SCRL             3719
#define IDD_FGDESKTOP_OPTS_ADV_PAGE             3721
#define IDC_FWDESKTOP_OPTS_ADV_LEVEL            3723
#define IDC_FWDESKTOP_OPTS_ADV_STAT             3725
#define IDC_FWDESKTOP_OPTS_ADV_STAT_V           3727
#define IDS_FGDESKTOP_ADV_AGRESSIVE             3729
#define IDS_FGDESKTOP_ADV_NORMAL                3731
#define IDS_FGDESKTOP_ADV_COMMON                3733

/////////////////////////////////////////////////////////////////////////////
//
// Event Properties Dialog
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_FGDESKTOP_PROP_DLG                  3801
#define IDC_FWDESKTOP_PROP_DLG_TABSET           3803
#define IDD_FGDESKTOP_PROP_GEN_PAGE             3805
#define IDR_FWDESKTOP_PROP_DLG_HEADER           3807
#define IDC_FGDESKTOP_PROP_GEN_TIME             3809
#define IDC_FGDESKTOP_PROP_GEN_SOURCE           3811
#define IDC_FGDESKTOP_PROP_GEN_TARGET           3813
#define IDC_FGDESKTOP_PROP_GEN_ACTION           3815
#define IDC_FGDESKTOP_PROP_GEN_STATUS           3817
#define IDC_FGDESKTOP_PROP_GEN_PROC             3819
#define IDC_FGDESKTOP_PROP_GEN_OBJECT           3821

