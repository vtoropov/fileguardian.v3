#ifndef _FGLVWRECORDIFACE_H_17ACD305_87FF_42be_B1A2_CA1AE102551C_INCLUDED
#define _FGLVWRECORDIFACE_H_17ACD305_87FF_42be_B1A2_CA1AE102551C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Mar-2018 at 5:36:04p, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian extended list view control data interface declaration file;
*/
#include "Shared_SystemError.h"
#include "FG_SysEvent_Model.h"
#include "FG_SqlData_Provider.h"
#include "FG_SysEvent_Filter.h"
#include "FG_Log_Registry.h"

namespace fw { namespace desktop { namespace data
{
	using fg::common::data::CRecord;
	using fg::common::data::local::CDbProvider;
	using fg::common::data::CFilter;
	using fg::common::data::CFilterPersistent;
	using fg::common::data::TOperateTotals;

	using shared::lite::common::CSysError;

	class CLvwCmdType {
	public:
		enum _e {
			t_0 = eFsEventType::eFsEventCreated,
			t_1 = eFsEventType::eFsEventDeleted,
			t_2 = eFsEventType::eFsEventAltered,
			t_3 = eFsEventType::eFsEventRenamed,
			t_4 = eFsEventType::eFsEventCopied ,
		};
		static const INT nCount = 5;
	public:
		static
			INT       Index(const eFsEventType::_e);
	};

	class CLvwCommand {
	private:
		eFsEventType::_e
		           m_action;
		bool       m_bEnabled;
		CAtlString m_name;
		CAtlString m_identifier;
		CAtlString m_class;
		UINT       m_associated;

	public:
		CLvwCommand(void);
		CLvwCommand(LPCTSTR lpszId, LPCTSTR lpszName, LPCTSTR lpszClass, const eFsEventType::_e = eFsEventType::eFsEventUnknown, const bool bEnabled = true);

	public:
		UINT       Associated(void)const;
		VOID       Associated(const UINT);
		eFsEventType::_e
		           Action(void)const;
		VOID       Action(const eFsEventType::_e);
		LPCTSTR    Class  (void)const;
		VOID       Class  (LPCTSTR);
		bool       Enabled(void)const;
		VOID       Enabled(const bool);
		bool       HasThis(const CAtlString& _stream)const;
		LPCTSTR    Identifier(void)const;
		VOID       Identifier(LPCTSTR);
		bool       IsValid(void)const;
		LPCTSTR    Name(void)const;
		VOID       Name(LPCTSTR);
	public:
		static
		LPCTSTR    Prefix(void);   
	};

	typedef ::std::vector<CLvwCommand> TLvwCommands;
	typedef ::std::map<eFsEventType::_e, INT> TLvwIndex;

	class CLvwCommands {
	private:
		TLvwCommands m_commands;
		TLvwIndex    m_index;
	public:
		enum _e {
			eNotFound = -1,
		};

	public:
		CLvwCommands(void);

	public:
		INT          Count(void)const;
		INT          IsCommand(LPCTSTR lpszURL)const;  // returns -1 value if a command is not found;
		const
		CLvwCommand& Item(const eFsEventType::_e)const;
		const
		CLvwCommand& Item(const INT nIndex)const;
	};

	class CLvwTotals {

	private:
		INT             m_result[CLvwCmdType::nCount];

	public:
		CLvwTotals(void);
		~CLvwTotals(void);

	public:
		INT             Total(const eFsEventType::_e)const;
		HRESULT         Total(const eFsEventType::_e, const INT _val);
	public:
		VOID            Clear   (void);
		INT             Count   (void)const;
		INT             TotalAll(void)const;
	public:
		CLvwTotals& operator= (const TOperateTotals&);
	};

	class CLvwAdapter {
	private:

		CFilterPersistent  m_dat_flt;
		CDbProvider        m_dat_prv;
		CEventLog_Registry m_dat_stg;

		CSysError          m_error;
		CLvwTotals         m_totals;
		CLvwCommands       m_commands;

	public:
		CLvwAdapter(void);

	public:
		const
		CLvwCommands&      Commands(void)const;
		CLvwCommands&      Commands(void);
		const
		CFilterPersistent& Filter(void)const;
		CFilterPersistent& Filter(void);
		const
		CDbProvider&       Provider(void)const;
		CDbProvider&       Provider(void);
		const
		CEventLog_Registry&Storage(void)const;
		CEventLog_Registry&Storage(void);
	public:
		HRESULT            Terminate(void);
		const
		CLvwTotals&        Totals(void)const;
		CLvwTotals&        Totals(void);
	};

	class CLvwDataSpec {

	public:
		enum _e {            // this enumeration is almost the same as in CSV record specification class, but they can be much different in the future;
			eTimestamp  = 0, // formatted date string;
			eFilePath   = 1, // complete file path;
			eAction     = 2, // applied action name;
			eActionId   = 3, // action identifier;
			eRecState   = 4, // listview record/row/item current state;
		};
	};

	class CLvwRecord {

	private:
		CAtlString    m_buffer[CLvwDataSpec::eAction + 1];
		INT           m_act_id;
		INT           m_state;
		time_t        m_time_as_is;
		LONGLONG      m_time_ex;
	public:
		CLvwRecord(void);
		CLvwRecord(const CRecord& _record);
		~CLvwRecord(void);

	public:
		LPCTSTR       Action   (void)const;
		HRESULT       Action   (const INT nId);
		const UINT    ActionId (void)const;
		LPCTSTR       FilePath (void)const;
		HRESULT       FilePath (LPCTSTR)  ;
		const UINT    State    (void)const;
		VOID          State    (const INT nValue, const bool bAppend = false);
		LPCTSTR       Timestamp(void)const;
		const
		LONGLONG&     TimestampEx(void)const;
		const time_t& TimestampRaw(void)const;

	public:
		HRESULT       Apply   (const CRecord& _record);
		VOID          Clear   (void)     ;
		bool          IsValid (void)const;
	public:
		CLvwRecord&   operator=(const CRecord&);
	};

	typedef ::std::vector<CLvwRecord> TLvwRecords;

	class CLvwCache;
	class CLvwSize {

		friend class CLvwCache;

	private:
		INT        m_nOffset;   // start index, i.e. shift of rows in the database file, zero-based;
		INT        m_nLimit ;   // rows count, this is the limit of required rows for cache;

	private:
		CLvwSize(void);
		CLvwSize(const INT _offset, const INT _limit);
		~CLvwSize(void);

	public:
		bool       IsEmpty(void)const ;
		bool       InRange(const INT nIndex)const;

	public:
		const
		INT&       Limit (void)const ;
		INT&       Limit (void)      ;
		const
		INT&       Offset(void)const ;
		INT&       Offset(void)      ;
	};

	class CLvwCache {

	public:
		enum _e {
			eEntriesCount = 100,    // max capacity of the cache;
		};

	private:
		CLvwAdapter&  m_adapter;
		CLvwRecord    m_records[CLvwCache::eEntriesCount];
		CLvwSize      m_size;
		mutable
		CSysError     m_error;

	public:
		CLvwCache(CLvwAdapter&);
		~CLvwCache(void);

	public:
		HRESULT       Clear(void);
		TErrorRef     Error(void)const;
		const
		CLvwRecord&   Item(const INT _index, bool bWithUpdateOpt);
		const
		CLvwSize      Size(void)const;
		HRESULT       Update(const INT _from, const INT _upto, const bool bAutoScroll);
	};
}}}

#endif/*_FGLVWRECORDIFACE_H_17ACD305_87FF_42be_B1A2_CA1AE102551C_INCLUDED*/