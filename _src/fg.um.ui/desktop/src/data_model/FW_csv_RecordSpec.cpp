/*
	Created by Tech_dog (VToropov) on 15-May-2016 at 10:19:38pm, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian desktop CSV data record specification base class implementation file.
*/
#include "StdAfx.h"
#include "FW_csv_RecordSpec.h"

using namespace fw::desktop::data;

/////////////////////////////////////////////////////////////////////////////

CCsvRecordSpec::CCsvRecordSpec(void)
{
	const INT nFields = CCsvRecordSpec::nFieldCount;
	for ( INT i_ = 0; i_ < nFields; i_++)
	{
		::ATL::CAtlString cs_field;
		switch (i_)
		{
		case CCsvRecordSpec::eTimestamp:     cs_field = _T("[Timestamp]"); break;
		case CCsvRecordSpec::eFilePath :     cs_field = _T("[File_Path]"); break;
		case CCsvRecordSpec::eAction   :     cs_field = _T("[Action]");    break;
			continue;
		}
		try
		{
			TSpecBase::m_fields.push_back(cs_field);
		} catch (::std::bad_alloc&)
		{
			break;
		}
	}

	m_range = ::std::make_pair(
				CCsvRecordSpec::nFieldCount - 0,
				CCsvRecordSpec::nFieldCount - 0
			);
}