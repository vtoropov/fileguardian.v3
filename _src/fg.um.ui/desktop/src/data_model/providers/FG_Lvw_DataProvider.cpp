/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Mar-2018 at 7:49:21p, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian list view extended data provider interface implementation file.
*/
#include "StdAfx.h"
#include "FG_Lvw_DataProvider.h"

using namespace fw::desktop::data;
using namespace fg::common::data;

#include <Strsafe.h>
#include "UIX_Defs.h"
#include "UIX_Browser.h"

using namespace ex_ui;
using namespace ex_ui::controls;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace data { namespace details
{
	using namespace fg::common::data;
	using namespace fg::common::data::local;

	class CLvwTotals_Query {

	private:
		CLvwAdapter&  m_adapter;
		CSysError     m_error;

	public:
		CLvwTotals_Query(CLvwAdapter& _adapter) : m_adapter(_adapter) { m_error.Source(_T("CLvwTotals_Query")); }

	public:

		HRESULT       Do (CLvwTotals& _totals) {

			m_error.Module(_T(__FUNCTION__));
			m_error = S_OK;

			if (m_adapter.Provider().Accessor().IsOpen() == false)
				return (m_error = OLE_E_BLANK);

			const CDbStats& stats_ = m_adapter.Provider().Stats();
			TOperateTotals totals_;

			HRESULT hr_ = stats_.Totals(
			         m_adapter.Filter(), totals_
				);

			if (FAILED(hr_))
				m_error = stats_.Error();
			else {
				_totals = totals_;
			}
			return m_error;
		}

		TErrorRef     Error(void)const { return m_error; }
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CLvwDataProvider::CLvwDataProvider(CLvwAdapter& _adapter) : m_cache(_adapter), m_adapter(_adapter)
{
	m_error.Source(_T("CLvwDataProvider"));
}

CLvwDataProvider::~CLvwDataProvider(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT CLvwDataProvider::IListView_OnCacheHint( NMLVCACHEHINT* _pCacheHint)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == _pCacheHint)
		return (m_error = E_POINTER);

	m_error = m_cache.Update(
			_pCacheHint->iFrom, _pCacheHint->iTo, true
		);

	return m_error;
}

HRESULT CLvwDataProvider::IListView_OnDispQuery( NMLVDISPINFO* _pQueryInfo )
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == _pQueryInfo)
		return (m_error = E_POINTER);

	if (_pQueryInfo->item.iItem < 0)
		return (m_error = DISP_E_BADINDEX);

	const CLvwRecord& record_ = m_cache.Item(_pQueryInfo->item.iItem, true);
	if (record_.IsValid() == false)
		return (m_error = m_cache.Error());

	if (LVIF_STATE & _pQueryInfo->item.mask) { }

	if (LVIF_IMAGE & _pQueryInfo->item.mask) { }

	if (LVIF_PARAM & _pQueryInfo->item.mask ||
	    _pQueryInfo->item.iSubItem == 0) {
#if (0) // does not work, it's necessary more research for using LPARAM in virtual list;
		_pQueryInfo->item.lParam   = (LPARAM)1;
		bool b_break = false;
		b_break = true;
#endif
	}

	if (LVIF_TEXT  & _pQueryInfo->item.mask) {
		//
		// we need to add 1 byte to string length :(, otherwise, the text is trancated;
		//
		switch (_pQueryInfo->item.iSubItem)
		{
		case CLvwDataSpec::eTimestamp: {
				m_error = ::StringCchCopy(
							_pQueryInfo->item.pszText, ::_tcslen(record_.Timestamp()) + 1, record_.Timestamp()
						);
			} break;
		case CLvwDataSpec::eFilePath:  {
				m_error = ::StringCchCopy(
							_pQueryInfo->item.pszText, ::_tcslen(record_.FilePath()) + 1, record_.FilePath()
						);
			} break;
		case CLvwDataSpec::eAction:    {
				m_error = ::StringCchCopy(
							_pQueryInfo->item.pszText, ::_tcslen(record_.Action()) + 1, record_.Action()
						);
			} break;
		}
	}
	if (LVIF_GROUPID & _pQueryInfo->item.mask) {
	}

	return m_error;
}

HRESULT CLvwDataProvider::IListView_OnFindItem ( LPNMLVFINDITEM _pFindItem )
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == _pFindItem)
		return (m_error = E_POINTER);

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////
HRESULT         CLvwDataProvider::AppendGroups(CListViewCtrl_Ex& _lvw_list)const {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!_lvw_list.IsValid())
		return (m_error = OLE_E_BLANK);

	CLvwGroupEnum groups_;
	HRESULT hr_ = groups_.Create();
	if (FAILED(hr_))
		return (m_error = groups_.Error());

	const TLvwGroups& array_ = groups_.Groups();

	for (size_t i_ = 0; i_ < array_.size(); i_++) {

		const CLvwGroup& el_ = array_[i_];
		if (el_.IsValid() == false)
			continue;

		CListViewGroup group_(
			el_.Caption(), el_.Elements(), el_.Type()
		); 

		hr_ = _lvw_list.Groups().Append(group_);
		if (FAILED(hr_))
			return (m_error = hr_);
	}

	_lvw_list.Groups().Enabled(true);

	return m_error;
}

const
TListViewCols_Ex
                CLvwDataProvider::Columns(void)const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	TListViewCols_Ex cols_;

	try {
		cols_.push_back(CListViewCol_Ex(LVCFMT_LEFT, 150, 0,  _T("Timestamp")));
		cols_.push_back(CListViewCol_Ex(LVCFMT_LEFT, 510, 1,  _T("File")));
		cols_.push_back(CListViewCol_Ex(LVCFMT_LEFT, 100, 2,  _T("Action")));
	}
	catch(::std::bad_alloc&) {
		m_error = E_OUTOFMEMORY;
	}

	return cols_;
}

TErrorRef       CLvwDataProvider::Error(void) const
{
	return m_error;
}

VOID            CLvwDataProvider::ErrorContent(TErrorRef _err, CAtlString& _content)
{
	_content = _err.GetFormattedDetails(_T("<br/>"));

	CAtlString cs_error;
	cs_error += _T("<table style='font-size:11px;'><tr><td><img src='fg_footer_err.png'/></td><td>");
	cs_error += _content;
	cs_error += _T("</td></tr></table>");
	_content  = cs_error;
}

HRESULT         CLvwDataProvider::FooterURL(CAtlString& _url)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CBrowserResource resource_(eBrowserResourceType::eHostExecutable);
	HRESULT hr_ = resource_.URL(_T("res://%s/FG_FOOTER_MAIN.HTML"), _url);
	if (FAILED(hr_))
		m_error = resource_.Error();

	return m_error;
}
const
CLvwRecord&     CLvwDataProvider::RowOf(const INT _index) { return m_cache.Item(_index, false); }

HRESULT         CLvwDataProvider::RowCount(INT& _rows)const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const LONG rows_ = m_adapter.Provider().Stats().RowCount(
			m_adapter.Filter()
		);
	_rows = static_cast<INT>(rows_);
	if (0 == _rows){
		m_error = m_adapter.Provider().Stats().Error();
	}

	return m_error;
}

HRESULT         CLvwDataProvider::Totals(CAxWindow& _ft_wnd)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CHtmlElement el_(_ft_wnd, _T("$rpt_page_progress"));

	const INT nCellCount = 30;

	CAtlString cs_cells(
		_T("<table class=\"rpt_page_container\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr style=\"height:40px\">")
	);
	const CAtlString cs_title(
		_T("<td colspan=\"%d\" class=\"%s\" style=\"width:2px;\">&nbsp;&nbsp;<a href=\"%s\" class=\"%s\">%s&nbsp;%d</a></td>")
	);

	details::CLvwTotals_Query qry_(m_adapter);

	CLvwTotals&origin_ = m_adapter.Totals();
	CLvwTotals totals_;
	HRESULT hr_ = qry_.Do(origin_); hr_;
//	if (FAILED(hr_)) {
//		CAtlString err_content;
//		this->ErrorContent(qry_.Error(), err_content);
//		el_.Content(
//			err_content.GetString()
//		);
//		return (m_error = qry_.Error());
//	}

	static const INT nDefault = 5;

	for (INT i_ = 0; i_ < totals_.Count(); i_++) {

		const CLvwCommand& cmd_ = m_adapter.Commands().Item(i_);
		if (cmd_.IsValid() == false)
			break;

		const eFsEventType::_e type_ = cmd_.Action(); 
		const INT nTotal = origin_.Total(type_);

		if (0 == nTotal ||
		    0 == origin_.TotalAll()
		)
			totals_.Total(type_, nDefault);
		else {
			totals_.Total(type_, nCellCount * origin_.Total(type_)/ origin_.TotalAll());
			if (totals_.Total(type_) == 0)
				totals_.Total(type_, nDefault);
		}

		for (INT j_ = 0; j_ < totals_.Total(type_); j_++) {
			if ( j_== 0 ) {
				CAtlString cs_href;
				cs_href.Format(
					cs_title.GetString(),  totals_.Total(type_), cmd_.Class(), cmd_.Identifier(), cmd_.Name(), cmd_.Name(), origin_.Total(type_)
					);
				cs_cells += cs_href;
			}
			CAtlString cs_empty;
			cs_empty.Format(
					_T("<td class=\"%s\" style=\"width:2px;\">&nbsp;</td>"), cmd_.Class()
				);
			cs_cells += cs_empty;
#if (0)
			//
			// ends up the last action;
			//
			if (totals_.Total(type_) - 1 == j_
			   && eFsEventType::eFsEventCopied == type_) {
					cs_empty.Format(
						_T("<td class=\"%s-end\" style=\"width:10px;\">&nbsp;</td>"), cmd_.Class()
					);
					cs_cells += cs_empty;
			}
#endif
		}
	}
	cs_cells += _T("</tr></table>");

	m_error = el_.Content(cs_cells.GetString());

	return m_error;
}

#if (1)
HRESULT         CLvwDataProvider::Update(CListViewCtrl_Ex& _lvw_list, const bool bAutoScroll)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = S_OK; bAutoScroll;

	const INT nTopIndex = _lvw_list.Control().GetTopIndex();
	hr_ = m_cache.Clear();
	hr_ = m_cache.Update(nTopIndex, CLvwCache::eEntriesCount, true);

	if (FAILED(hr_))
		return (m_error = m_cache.Error());
	{
	}
	return m_error;
}
#endif
/////////////////////////////////////////////////////////////////////////////

INT             CLvwDataProvider::ItemCount(CListViewCtrl_Ex& _lvw_list, const INT nTotal)
{
	//
	// TODO: Not all screen resolution can feet this rule; it's necessary to calculate more precise value;
	//
	const INT nVisible = ListView_GetCountPerPage(_lvw_list.Control());

	if (nTotal < nVisible)
		return nTotal;
	if (nTotal < CLvwCache::eEntriesCount && false) // this can lead to incorrect row count and empty list items;
		return CLvwCache::eEntriesCount;
	else
		return nTotal;
}