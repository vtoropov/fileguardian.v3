#ifndef _FGLOGREGISTRY_H_88E22F27_29E4_4aee_B989_EA6482921088_INCLUDED
#define _FGLOGREGISTRY_H_88E22F27_29E4_4aee_B989_EA6482921088_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 04-Apr-2018 at 10:40:53a, UTC+7, Novosibirsk, Rodniki, Wednesday;
	This is File Guardian desktop application log view registry storage interface declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_Registry.h"

namespace fw { namespace desktop { namespace data
{
	using shared::lite::common::CSysError;
	using shared::registry::CRegistryStorage;

	class CEventLog_Update {

	private:
		CRegistryStorage m_stg;
		CSysError        m_error;
		INT              m_sec;
		bool             m_enabled;
		bool             m_vscroll;                 // indicates keeping vertical auto-scroll
	public:
		CEventLog_Update(void);
		~CEventLog_Update(void);
	public:
		bool             AutoScroll(void)const;
		VOID             AutoScroll(const bool);
		bool             Enabled(void)const;
		VOID             Enabled(const bool);       // if a period is not valid (0 or less), enabling is not turned on;
		bool             IsValid(void)const;
		INT              Period(void)const;
		VOID             Period(const INT nValue);  // if a value is less than 1 sec., updating is disabled;
	public:
		TErrorRef        Error(void)const;
		HRESULT          Load(void);
		HRESULT          Save(void);
	};

	class CEventLog_Registry {

	private:
		CRegistryStorage m_stg;
		CSysError        m_error;
		CEventLog_Update m_update;

	public:
		CEventLog_Registry(void);

	public:
		TErrorRef        Error(void)const;
		bool             ShowFooter(void)const;
		HRESULT          ShowFooter(const bool _value);
		const
		CEventLog_Update&Update(void)const;
		CEventLog_Update&Update(void);
	};
}}}

#endif/*_FGLOGREGISTRY_H_88E22F27_29E4_4aee_B989_EA6482921088_INCLUDED*/