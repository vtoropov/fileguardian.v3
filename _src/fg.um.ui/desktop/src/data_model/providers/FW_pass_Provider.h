#ifndef _FWPASSPROVIDER_H_DADFB9E6_6E7B_4387_9F1C_50010D5CD828_INCLUDED
#define _FWPASSPROVIDER_H_DADFB9E6_6E7B_4387_9F1C_50010D5CD828_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Jan-2016 at 8:53:40pm, GMT+7, Phuket, Rawai, Friday;
	This is File Watcher desktop password data provider class declaration file.
*/
#include "Shared_SystemError.h"

namespace fw { namespace desktop { namespace data
{
	using shared::lite::common::CSysError;

	class CPwdDataProvider
	{
	private:
		CSysError       m_error;
	public:
		CPwdDataProvider(void);
		~CPwdDataProvider(void);
	public:
		HRESULT         CheckPassword(const CAtlString&);
		TErrorRef       Error(void)const;
		HRESULT         GetPassword(CAtlString&);
		HRESULT         SetPassword(const CAtlString&);
	};
}}}

#endif/*_FWPASSPROVIDER_H_DADFB9E6_6E7B_4387_9F1C_50010D5CD828_INCLUDED*/