/*
	Created by Tech_dog (VToropov) on 22-Jan-2016 at 8:58:40pm, GMT+7, Phuket, Rawai, Friday;
	This is File Watcher desktop password data provider class implementation file.
*/
#include "StdAfx.h"
#include "FW_csv_Provider.h"
#include "FW_csv_RecordSpec.h"

using namespace fw::desktop::data;

#include "generic.stg.csv.h"

using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

CCsvDataProvider::CCsvDataProvider(void)
{
	m_error.Source(_T("CCsvDataProvider"));
}

CCsvDataProvider::~CCsvDataProvider(void)
{
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef       CCsvDataProvider::Error(void)const
{
	return m_error;
}

HRESULT         CCsvDataProvider::Save (const HWND _src_ctrl, LPCTSTR lpszPath, ICsvDataProviderCallback* pSink)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	::WTL::CListViewCtrl list_ = _src_ctrl;
	if (!list_)
		return (m_error = OLE_E_INVALIDHWND);

	CCsvFile csv_(eCsvFileOption::eUseCommaSeparator);
	{
		CCsvRecordSpec spec_;
		CCsvFile::THeader header_ = spec_.CreateHeader();

		HRESULT hr_ = csv_.Header(header_);
		if (FAILED(hr_))
			return (m_error = hr_);
	}
	const INT cols_ = list_.GetHeader().GetItemCount();
	const INT rows_ = list_.GetItemCount();

	if (pSink)
		pSink->CsvDataProvider_OnStart(rows_);

	for ( INT i_ = 0; i_ < rows_; i_++)
	{
		CCsvFile::TRow row_;
		for (INT j_ = 0; j_ < cols_; j_++)
		{
			CAtlString val_;
			list_.GetItemText(i_, j_, val_);
			try
			{
				row_.push_back(val_);
			}
			catch(::std::bad_alloc&)
			{
				return (m_error = E_OUTOFMEMORY);
			}
			if (pSink)
				if (pSink->CsvDataProvider_CanContinue() == false)
					return (m_error = S_FALSE);
		}
		HRESULT hr_ = csv_.AddRow(row_);
		if (FAILED(hr_))
			return (m_error = hr_);
		if (pSink)
			pSink->CsvDataProvider_OnProgress(i_ + 1, rows_);
	}
	if (!pSink || pSink->CsvDataProvider_CanContinue())
	{
		HRESULT hr_ = csv_.Save(lpszPath, true);
		if (FAILED(hr_))
			m_error = hr_;
	}
	if (pSink)
	{
		if (m_error)
			pSink->CsvDataProvider_OnError(m_error);
		else
			pSink->CsvDataProvider_OnComplete();
	}

	return m_error;
}