/*
	Created by Tech_dog (VToropov) on 22-Jan-2016 at 9:03:23pm, GMT+7, Phuket, Rawai, Friday;
	This is File Watcher desktop password data provider class implementation file.
*/
#include "StdAfx.h"
#include "FW_pass_Provider.h"

using namespace fw::desktop::data;

#include "Shared_Registry.h"

using namespace shared::registry;

#include "FG_Generic_Encrypt.h"

using namespace fg::common::data;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace data { namespace details
{
	const CCryptoDataCrt& CPwdDataProvider_GetCryptoCrtData(void)
	{
		static CCryptoDataCrt crt_;
		static bool bInitialized = false;
		if (!bInitialized)
		{
			bInitialized = true;
			crt_.Algorithm(_T("rijndael-128"));
			crt_.Key      (_T("BZHS3Lu8PjyGLeBKDnDfr75T1WPvONHMRsLjnfkg"));
			crt_.Mode     (_T("cbc"));
			crt_.Vector   (_T("WWN7Gq5NeYFktsXhaubqbqP1UH1LbkQEIMBwZKJp"));
		}
		return crt_;
	}

	const CAtlString&     CPwdDataProvider_DefSequence(void)
	{
		static CAtlString seq_ = _T("AWTPFCdGJZL43sqjS30yqmuXACcPw1aNWVrTT5HJT90="); // filewatcher is default password (encoded)
		return seq_;
	}

	class CPwdDataProvider_Registry
	{
	private:
		CRegistryStorage  m_stg;
	public:
		CPwdDataProvider_Registry(void) : m_stg(HKEY_CURRENT_USER)
		{
		}
	public:
		HRESULT           Load(CAtlString& _pwd)
		{
			_pwd;
			HRESULT hr_ = m_stg.Load(
					this->_CommonFolderName(),
					this->_NamedValuePwdData(),
					_pwd
				);
			return  hr_;
		}

		HRESULT           Save(const CAtlString& _pwd)
		{
			_pwd;
			HRESULT hr_ = m_stg.Save(
					this->_CommonFolderName(),
					this->_NamedValuePwdData(),
					_pwd
				);
			return  hr_;
		}
	private:
		CAtlString   _CommonFolderName(void)      const { return CAtlString(_T("Common"));      }
		CAtlString   _NamedValuePwdData(void)     const { return CAtlString(_T("Settings"));    }
	};

	class CPwdDataProvider_Encryptor : public CCryptoDataProviderBase
	{
		typedef CCryptoDataProviderBase TBase;
	public:
		CPwdDataProvider_Encryptor(void) : TBase(details::CPwdDataProvider_GetCryptoCrtData())
		{
			m_error.Source(_T("CPwdDataProvider_Encryptor"));
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CPwdDataProvider::CPwdDataProvider(void)
{
	m_error.Source(_T("CPwdDataProvider"));
}

CPwdDataProvider::~CPwdDataProvider(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT         CPwdDataProvider::CheckPassword(const CAtlString& _pwd)
{
	m_error.Module(_T(__FUNCTION__));

	CAtlString pwd_;
	HRESULT hr_ = this->GetPassword(pwd_);
	if (FAILED(hr_))
		return hr_;

	if (pwd_.IsEmpty() && _pwd.IsEmpty())
		return m_error;
	else if (pwd_.IsEmpty() && !_pwd.IsEmpty()
			|| 0 != pwd_.Compare(_pwd))
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_PASSWORD,
				_T("Invalid password")
			);
	}
	else
		m_error = S_OK;

	return m_error;
}

TErrorRef       CPwdDataProvider::Error(void)const
{
	return m_error;
}

HRESULT         CPwdDataProvider::GetPassword(CAtlString& _pwd)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString encoded_;

	details::CPwdDataProvider_Registry reg_;
	HRESULT hr_ = reg_.Load(encoded_);
	if (FAILED(hr_))
		return (m_error = hr_);
	else if (encoded_.IsEmpty())
		encoded_ = details::CPwdDataProvider_DefSequence(); // password is not in registry, sets default value

	CAtlString decoded_;

	details::CPwdDataProvider_Encryptor prov_;
	hr_ = prov_.Decrypt(encoded_, decoded_);
	if (FAILED(hr_))
		m_error.SetState(
				hr_,
				_T("Internal encryption error")
			);
	else
		_pwd = decoded_;
	return m_error;
}

HRESULT         CPwdDataProvider::SetPassword(const CAtlString& _pwd)
{
	m_error.Module(_T(__FUNCTION__));

	CAtlString encoded_;

	details::CPwdDataProvider_Encryptor prov_;
	HRESULT hr_ = prov_.Encrypt(_pwd, encoded_);
	if (FAILED(hr_))
	{
		m_error.SetState(
				hr_,
				_T("Internal encryption error")
			);
		return m_error;
	}
	details::CPwdDataProvider_Registry reg_;
	m_error = reg_.Save(encoded_);

	return m_error;
}