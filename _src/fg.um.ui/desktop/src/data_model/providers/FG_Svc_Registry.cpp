/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jan-2018 at 10:41:38a, UTC+7, Phuket, Rawai, Monday;
	This is File Guardian background service desktop registry provider interface implementation file.
*/
#include "StdAfx.h"
#include "FG_Svc_Registry.h"

using namespace fw::desktop::data;

/////////////////////////////////////////////////////////////////////////////

CService_RegBase::CService_RegBase(LPCTSTR _pfx) : m_stg(HKEY_CURRENT_USER), m_pfx(_pfx)
{
}

/////////////////////////////////////////////////////////////////////////////

CAtlString  CService_RegBase::LastFolderPath(void)const
{
	CAtlString cs_path;
	m_stg.Load(
			this->_CommonFolder(),
			this->_NamedValueLastFolder(),
			cs_path
		);
	return cs_path;
}

HRESULT     CService_RegBase::LastFolderPath(const CAtlString& _path)
{
	HRESULT hr_ = m_stg.Save(
			this->_CommonFolder(),
			this->_NamedValueLastFolder(),
			_path
		);
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString  CService_RegBase::_CommonFolder(void)            const{ return CAtlString(_T("Common"));}
CAtlString  CService_RegBase::_NamedValueLastFolder(void)    const{
	CAtlString cs_path;
	cs_path.Format(
			_T("Last%sFolder"),
			(LPCTSTR)m_pfx
		);
	return cs_path;
}

/////////////////////////////////////////////////////////////////////////////

CService_RegExclude::CService_RegExclude(void) : TBase(_T("Excluded"))
{
}

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////

CService_RegInclude::CService_RegInclude(void) : TBase(_T("Included"))
{
}

/////////////////////////////////////////////////////////////////////////////

CService_RegMain::CService_RegMain(void) : m_stg(HKEY_CURRENT_USER)
{
}

/////////////////////////////////////////////////////////////////////////////

bool        CService_RegMain::StateAutoCheck(void)const
{
	LONG lValue = 0;
	m_stg.Load(
			this->_CommonFolder(),
			this->_NamedValueAutoCheck(),
			lValue,
			0
		);
	return !!lValue;
}

HRESULT     CService_RegMain::StateAutoCheck(const bool _check)
{
	HRESULT hr_ = m_stg.Save(
			this->_CommonFolder(),
			this->_NamedValueAutoCheck(),
			static_cast<LONG>(_check)
		);
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString  CService_RegMain::_CommonFolder(void)            const{ return CAtlString(_T("Common"));                }
CAtlString  CService_RegMain::_NamedValueAutoCheck(void)     const{ return CAtlString(_T("AutoCheckSvcState"));     }

/////////////////////////////////////////////////////////////////////////////