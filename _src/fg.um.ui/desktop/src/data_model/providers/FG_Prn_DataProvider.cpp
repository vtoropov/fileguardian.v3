/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Apr-2018 at 7:14:34a, UTC+7, Novosibirsk, Rodniki, Monday;
	This is File Guardian desktop application file event print data provider interface implementation file.
*/
#include "StdAfx.h"
#include "FG_Prn_DataProvider.h"

using namespace fw::desktop::data;

#include "Shared_GenericAppObject.h"

using namespace shared::user32;
using namespace ex_ui::printing;

#include "FG_SysEvent_Model.h"

using namespace fg::common::data;

/////////////////////////////////////////////////////////////////////////////

CPrintDataProvider::CPrintDataProvider(CLvwAdapter& _ada, CLvwDataProvider& _prov) : m_lvw_ada(_ada), m_lvw_data(_prov)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CPrintDataProvider::IPrintDataProvider_DocumentName(CAtlString& _name)
{
	SYSTEMTIME st_ = {0};
	::GetLocalTime(&st_);

	_name.Format(
			_T("File Guardian Report on %04d-%02d-%02d %02dh%02dm"),
			st_.wYear  ,
			st_.wMonth ,
			st_.wDay   ,
			st_.wHour  ,
			st_.wMinute
		);

	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT    CPrintDataProvider::IPrintDataProvider_FooterText(CAtlString& _footer)
{
	_footer = _T("Page ${page} of ${pages}");
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT    CPrintDataProvider::IPrintDataProvider_HeaderText(CAtlString& _header)
{
	_header.Format(
			_T("%s %s"),
			(LPCTSTR)GetAppObjectRef().Version().ProductName(),
			(LPCTSTR)GetAppObjectRef().Version().FileVersionFixed()
		);
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT    CPrintDataProvider::IPrintDataProvider_PageData(const INT nRow, const INT nCount, ::std::vector<::std::vector<CAtlString>>& _data)
{
#if (0)
	HRESULT hr_ = m_lvw_ada.Provider().Reader().Recordset(nRow, nCount, _data);
	if (FAILED(hr_))
		return hr_;
#else
	TRecords recs_ = m_lvw_ada.Provider().Reader().Records(m_lvw_ada.Filter(), nRow, nCount);
	HRESULT hr_ = m_lvw_ada.Provider().Reader().Error();
	if (SUCCEEDED(hr_)) {
		if (recs_.empty() == false) {
			for (size_t i_ = 0; i_ < recs_.size(); i_++) {
				::std::vector<CAtlString> row_;
				try {
					row_.push_back(recs_[i_].TimestampAsText());
					row_.push_back(recs_[i_].Target());
					row_.push_back(recs_[i_].ActionAsText());

					_data.push_back(row_);
				}
				catch(::std::bad_alloc&) {
					return hr_ = E_OUTOFMEMORY;
				}
			}
		}
	}
#endif

	return hr_;
}

HRESULT    CPrintDataProvider::IPrintDataProvider_RowCount(INT& _count)
{
	HRESULT hr_ = m_lvw_data.RowCount(_count);
	return  hr_;
}

#if (0)
HRESULT    CPrintDataProvider::IPrintDataProvider_RowData (const INT nRow, ::std::vector<CAtlString>& _data)
{
	HRESULT hr_ = S_OK;
	if (nRow < 0)
		return (hr_ = E_INVALIDARG);
	INT count_ = 0;
	hr_ = m_lvw_data.RowCount(count_)
	if (nRow > count_ - 1)
		return (hr_ = DISP_E_BADINDEX);

	const INT cols_ = m_lvw_list.Control().GetHeader().GetItemCount();
	for ( INT i_ = 0; i_ < cols_; i_++)
	{
		CAtlString val_;
		m_lvw_list.Control().GetItemText(nRow, i_, val_);
		try
		{
			_data.push_back(val_);
		}
		catch(::std::bad_alloc&)
		{
			return E_OUTOFMEMORY;
		}
	}
	HRESULT hr_ = S_OK;
	return  hr_;
}
#endif

HRESULT    CPrintDataProvider::IPrintDataProvider_TableHeader(::std::vector<CAtlString>& _cols)
{
	static ::std::vector<CAtlString> cols_;
	if (cols_.empty())
	{
		try
		{
			cols_.push_back(CAtlString(_T("Timestamp")));
			cols_.push_back(CAtlString(_T("File")));
			cols_.push_back(CAtlString(_T("Action")));
		}
		catch(::std::bad_alloc&)
		{
			return E_OUTOFMEMORY;
		}
	}
	_cols = cols_;
	HRESULT hr_ = S_OK;
	return  hr_;
}