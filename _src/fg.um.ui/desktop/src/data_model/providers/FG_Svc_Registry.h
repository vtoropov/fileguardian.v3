#ifndef _FGSERVICEPROVIDER_H_05BC609E_CDBF_4ff2_92A0_2ABBBE5948D9_INCLUDED
#define _FGSERVICEPROVIDER_H_05BC609E_CDBF_4ff2_92A0_2ABBBE5948D9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Jan-2018 at 10:29:49a, UTC+7, Phuket, Rawai, Monday;
	This is File Guardian background service desktop registry provider interface declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_Registry.h"

namespace fw { namespace desktop { namespace data
{
	using shared::lite::common::CSysError;
	using shared::registry::CRegistryStorage;

	class CService_RegBase {
	private:
		mutable
		CRegistryStorage m_stg;
		CAtlString       m_pfx;
	public:
		CService_RegBase(LPCTSTR _pfx);
	public:
		CAtlString       LastFolderPath(void) const;
		HRESULT          LastFolderPath(const CAtlString& _path);
	private:
		CAtlString      _CommonFolder(void) const;
		CAtlString      _NamedValueLastFolder(void) const;
	};

	class CService_RegExclude : public CService_RegBase
	{
		typedef CService_RegBase TBase;
	public:
		CService_RegExclude(void);
	};

	class CService_RegInclude : public CService_RegBase
	{
		typedef CService_RegBase TBase;
	public:
		CService_RegInclude(void);
	};

	class CService_RegMain
	{
	private:
		mutable
		CRegistryStorage m_stg;
	public:
		CService_RegMain(void);
	public:
		bool             StateAutoCheck(void)const;
		HRESULT          StateAutoCheck(const bool _value);
	private:
		CAtlString      _CommonFolder(void) const;
		CAtlString      _NamedValueAutoCheck(void) const;
	};
}}}

#endif/*_FGSERVICEPROVIDER_H_05BC609E_CDBF_4ff2_92A0_2ABBBE5948D9_INCLUDED*/