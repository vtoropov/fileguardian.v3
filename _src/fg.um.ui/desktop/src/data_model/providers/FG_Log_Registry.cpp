/*
	Created by Tech_dog (ebontrop@gmail.com) on 04-Apr-2018 at 12:37:28p, UTC+7, Novosibirsk, Rodniki, Wednesday;
	This is File Guardian desktop application log view registry storage interface implementation file.
*/
#include "StdAfx.h"
#include "FG_Log_Registry.h"

using namespace fw::desktop::data;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace data { namespace details
{
	CAtlString       CEventLog_Registry_CommonFolder(void)  { return CAtlString(_T("Common")); }
	CAtlString       CEventLog_Registry_NamedValueShowFooter(void) { return CAtlString(_T("ShowFooter")); }
	CAtlString       CEventLog_Registry_NamedValueUpdInterval(void){ return CAtlString(_T("UpdateInterval")); }
	CAtlString       CEventLog_Registry_NamedValueUpdEnabled(void) { return CAtlString(_T("UpdateEnabled")); }
	CAtlString       CEventLog_Registry_NamedValueUpdAScroll(void) { return CAtlString(_T("UpdateAScroll")); }
}}}}

/////////////////////////////////////////////////////////////////////////////

CEventLog_Update::CEventLog_Update(void) : m_sec(0), m_enabled(false), m_stg(HKEY_CURRENT_USER), m_vscroll(true)
{
	m_error.Source(_T("CEventLog_Update"));
}

CEventLog_Update::~CEventLog_Update(void)
{
}

/////////////////////////////////////////////////////////////////////////////

bool             CEventLog_Update::AutoScroll(void)const       { return m_vscroll; }
VOID             CEventLog_Update::AutoScroll(const bool _val) { m_vscroll = _val; }
bool             CEventLog_Update::Enabled(void)const
{
	return m_enabled;
}

VOID             CEventLog_Update::Enabled(const bool _val)
{
	if (!this->IsValid()) {
		m_enabled = false;
		return;
	}
	else
		m_enabled = _val;
}

bool             CEventLog_Update::IsValid(void)const
{
	return (0 < m_sec);
}

INT              CEventLog_Update::Period(void)const
{
	return m_sec;
}

VOID             CEventLog_Update::Period(const INT nValue)
{
	if (nValue < 0) {
		m_sec =  0;
		m_enabled = false;
		return;
	}

	m_sec = nValue;
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef        CEventLog_Update::Error(void)const
{
	return m_error;
}

HRESULT          CEventLog_Update::Load(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	LONG lValue = 0;
	HRESULT hr_ = m_stg.Load(
			details::CEventLog_Registry_CommonFolder(),
			details::CEventLog_Registry_NamedValueUpdInterval(),
			lValue,
			5
		);
	if (FAILED(hr_))
		return (m_error = m_stg.Error());

	this->Period(
			static_cast<INT>(lValue)
		);

	hr_ = m_stg.Load(
			details::CEventLog_Registry_CommonFolder(),
			details::CEventLog_Registry_NamedValueUpdEnabled(),
			lValue,
			0
		);
	if (FAILED(hr_))
		return (m_error = m_stg.Error());

	this->Enabled(!!lValue);

	hr_ = m_stg.Load(
			details::CEventLog_Registry_CommonFolder(),
			details::CEventLog_Registry_NamedValueUpdAScroll(),
			lValue,
			1
		);
	this->AutoScroll(!!lValue);

	return m_error;
}

HRESULT          CEventLog_Update::Save(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = m_stg.Save(
			details::CEventLog_Registry_CommonFolder(),
			details::CEventLog_Registry_NamedValueUpdInterval(),
			static_cast<LONG>(this->Period())
		);
	if (FAILED(hr_))
		return (m_error = m_stg.Error());

	hr_ = m_stg.Save(
			details::CEventLog_Registry_CommonFolder(),
			details::CEventLog_Registry_NamedValueUpdEnabled(),
			static_cast<LONG>(this->Enabled())
		);
	if (FAILED(hr_))
		return (m_error = m_stg.Error());

	hr_ = m_stg.Save(
			details::CEventLog_Registry_CommonFolder(),
			details::CEventLog_Registry_NamedValueUpdAScroll(),
			static_cast<LONG>(this->AutoScroll())
		);

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CEventLog_Registry::CEventLog_Registry(void) : m_stg(HKEY_CURRENT_USER)
{
	m_error.Source(_T("CEventLog_Registry"));
}

/////////////////////////////////////////////////////////////////////////////

bool             CEventLog_Registry::ShowFooter(void)const
{
	LONG lValue = 0;
	m_stg.Load(
			details::CEventLog_Registry_CommonFolder(),
			details::CEventLog_Registry_NamedValueShowFooter(),
			lValue,
			0
		);
	return !!lValue;
}

HRESULT          CEventLog_Registry::ShowFooter(const bool _value)
{
	HRESULT hr_ = m_stg.Save(
			details::CEventLog_Registry_CommonFolder(),
			details::CEventLog_Registry_NamedValueShowFooter(),
			static_cast<LONG>(_value)
		);
	return hr_;
}

const
CEventLog_Update&CEventLog_Registry::Update(void)const { return m_update; }
CEventLog_Update&CEventLog_Registry::Update(void)      { return m_update; }

/////////////////////////////////////////////////////////////////////////////