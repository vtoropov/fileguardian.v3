#ifndef _FWCSVPROVIDER_H_22DFA8E5_62AA_4a84_B34A_682FAAF6E747_INCLUDED
#define _FWCSVPROVIDER_H_22DFA8E5_62AA_4a84_B34A_682FAAF6E747_INCLUDED
/*
	Created by Tech_dog (VToropov) on 13-May-2016 at 8:52:54pm, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian desktop CSV data provider class declaration file.
*/
#include "Shared_SystemError.h"

namespace fw { namespace desktop { namespace data
{
	using shared::lite::common::CSysError;

	interface ICsvDataProviderCallback
	{
		virtual bool    CsvDataProvider_CanContinue(void) PURE;
		virtual VOID    CsvDataProvider_OnComplete(void) PURE;
		virtual VOID    CsvDataProvider_OnError(const CSysError) PURE;
		virtual VOID    CsvDataProvider_OnProgress(const INT _ndx, const INT _total) PURE;
		virtual VOID    CsvDataProvider_OnStart(const INT _total) PURE;
		virtual HWND    CsvDataProvider_RequestSourceCtrl(void) PURE;
		virtual CAtlString
		                CsvDataProvider_RequestTargetPath(void) PURE;
	};

	class CCsvDataProvider
	{
	private:
		CSysError       m_error;
	public:
		CCsvDataProvider(void);
		~CCsvDataProvider(void);
	public:
		TErrorRef       Error(void)const;
		HRESULT         Save (const HWND _src_ctrl, LPCTSTR lpszPath, ICsvDataProviderCallback* = NULL); // saves data from system list view control to specified path
	};
}}}

#endif/*_FWCSVPROVIDER_H_22DFA8E5_62AA_4a84_B34A_682FAAF6E747_INCLUDED*/