#ifndef _FGLVWDATAPROVIDER_H_CCFCCBA2_AF4B_4b4a_9A8E_A48B302DF601_INCLUDED
#define _FGLVWDATAPROVIDER_H_CCFCCBA2_AF4B_4b4a_9A8E_A48B302DF601_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Mar-2018 at 6:39:27p, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian list view extended data provider interface declaration file.
*/
#include "Shared_SystemError.h"
#include "FG_SqlData_Provider.h"
#include "FG_SysEvent_Filter.h"

#include "UIX_ListView_Ex.h"
#include "FG_lvw_RecordIface.h"
#include "FG_lvw_UiController.h"

namespace fw { namespace desktop { namespace data
{
	using shared::lite::common::CSysError;
	using ex_ui::controls::IListView_Ex_DataSource;
	using ex_ui::controls::TListViewCols_Ex;
	using ex_ui::controls::CListViewCol_Ex;
	using ex_ui::controls::CListViewCtrl_Ex;

	class CLvwDataProvider :
		public  IListView_Ex_DataSource {

	private:
		mutable
		CSysError       m_error;
		CLvwCache       m_cache;
		CLvwAdapter&    m_adapter;

	public:
		CLvwDataProvider(CLvwAdapter&);
		~CLvwDataProvider(void);

	private: // IListView_Ex_DataSource

		virtual HRESULT IListView_OnCacheHint( NMLVCACHEHINT* _pCacheHint) override sealed;
		virtual HRESULT IListView_OnDispQuery( NMLVDISPINFO* _pQueryInfo ) override sealed;
		virtual HRESULT IListView_OnFindItem ( LPNMLVFINDITEM _pFindItem ) override sealed;

	public:
		HRESULT         AppendGroups(CListViewCtrl_Ex& _lvw_list)const;
		const
		TListViewCols_Ex
		                Columns(void)const;
		HRESULT         FooterURL(CAtlString& _url);
		TErrorRef       Error(void)const;
		VOID            ErrorContent(TErrorRef, CAtlString& _content);
		const
		CLvwRecord&     RowOf(const INT _index);
		HRESULT         RowCount(INT& _rows)const;
		HRESULT         Totals(CAxWindow& _ft_wnd);
		HRESULT         Update(CListViewCtrl_Ex& _lvw_list, const bool bAutoScroll = false);

	public:
		static
		INT             ItemCount(CListViewCtrl_Ex& _lvw_list, const INT nTotal = 0);
	};
}}}

#endif/*_FGLVWDATAPROVIDER_H_CCFCCBA2_AF4B_4b4a_9A8E_A48B302DF601_INCLUDED*/