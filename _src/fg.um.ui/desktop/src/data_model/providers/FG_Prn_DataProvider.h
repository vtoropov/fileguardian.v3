#ifndef _FGPRNDATAPROVIDER_H_2E68F7BC_4472_401a_AFA4_9B874E533CAC_INCLUDED
#define _FGPRNDATAPROVIDER_H_2E68F7BC_4472_401a_AFA4_9B874E533CAC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Apr-2018 at 7:02:46a, UTC+7, Novosibirsk, Rodniki, Monday;
	This is File Guardian desktop application file event print data provider interface declaration file.
*/
#include "Shared_PrintView.h"
#include "FG_Lvw_DataProvider.h"

namespace fw { namespace desktop { namespace data
{
	using ex_ui::printing::IPrintEventSink;
	using ex_ui::printing::IPrintDataProvider;

	class CPrintDataProvider : public  IPrintDataProvider
	{
	private:
		CLvwAdapter&       m_lvw_ada;
		CLvwDataProvider&  m_lvw_data;

	public:
		CPrintDataProvider(CLvwAdapter&, CLvwDataProvider&);
	private: // IPrintDataProvider
		virtual HRESULT    IPrintDataProvider_DocumentName(CAtlString&)override sealed;
		virtual HRESULT    IPrintDataProvider_FooterText(CAtlString&)  override sealed;
		virtual HRESULT    IPrintDataProvider_HeaderText(CAtlString&)  override sealed;
		virtual HRESULT    IPrintDataProvider_PageData(const INT nRow, const INT nCount, ::std::vector<::std::vector<CAtlString>>&) override sealed;
		virtual HRESULT    IPrintDataProvider_RowCount(INT&) override sealed;
#if (0)
		virtual HRESULT    IPrintDataProvider_RowData (const INT nRow, ::std::vector<CAtlString>&) override sealed;
#endif
		virtual HRESULT    IPrintDataProvider_TableHeader(::std::vector<CAtlString>&) override sealed;
	};
}}}

#endif/*_FGPRNDATAPROVIDER_H_2E68F7BC_4472_401a_AFA4_9B874E533CAC_INCLUDED*/