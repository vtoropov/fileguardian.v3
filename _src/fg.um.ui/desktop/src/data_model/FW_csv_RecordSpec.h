#ifndef _FWRCSVECORDSPEC_H_7D7D5FDB_4A68_4421_AABE_F7D509330406_INCLUDED
#define _FWRCSVECORDSPEC_H_7D7D5FDB_4A68_4421_AABE_F7D509330406_INCLUDED
/*
	Created by Tech_dog (VToropov) on 14-May-2016 at 2:06:13pm, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian desktop CSV data record specification base class declaration file.
*/
#include "generic.stg.csv.h"

namespace fw { namespace desktop { namespace data
{
	using shared::lite::data::CCsvFile;
	using shared::lite::data::CCsvRecordSpecBase;

	class CCsvRecordSpec : public CCsvRecordSpecBase
	{
		typedef CCsvRecordSpecBase TSpecBase;
	public:
		enum _enum{
			eTimestamp  = 0,
			eFilePath   = 1,
			eAction     = 2,
		};
	public:
		CCsvRecordSpec(void);
	public:
		static const INT nFieldCount = CCsvRecordSpec::eAction + 1;
	};
}}}

#endif/*_FWRCSVECORDSPEC_H_7D7D5FDB_4A68_4421_AABE_F7D509330406_INCLUDED*/