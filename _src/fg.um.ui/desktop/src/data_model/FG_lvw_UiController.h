#ifndef _FGLVWUICONTROLLER_H_B42587D4_9EAB_45c3_A229_AFAC2327D1D1_INCLUDED
#define _FGLVWUICONTROLLER_H_B42587D4_9EAB_45c3_A229_AFAC2327D1D1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-May-2018 at 1:12:26a, UTC+7, Novosibirsk, Rodniki, Sunday;
	This is File Guardian desktop UI app event list view output controller interface declaration file. 
*/
#include "Shared_SystemError.h"
#include "FG_SysEvent_Model.h"

namespace fw { namespace desktop { namespace data
{
	using shared::lite::common::CSysError;

	using fg::common::data::CSystemEvent;
	using fg::common::data::CSystemEventEnum;
	using fg::common::data::TSystemEventCollection;

	class CLvwGroup {
	private:
		eFsEventType::_e   m_e_type;
		CAtlString         m_s_name;       // a group name, which is composed by action name and number of elements in a group;
		INT                m_n_els;        // current number of group elements;
	public:
		CLvwGroup(const eFsEventType::_e = eFsEventType::eFsEventUnknown);
		~CLvwGroup(void);
	public:
		LPCTSTR      Caption(void)const;   // formatted group caption;
		INT          Elements(void)const;
		HRESULT      Elements(const INT);
		bool         IsValid(void)const;
		eFsEventType::_e
		             Type(void)const;
		VOID         Type(const eFsEventType::_e);
	public:
		CLvwGroup&   operator=(const CSystemEvent&);
	};

	typedef ::std::vector<CLvwGroup>  TLvwGroups;

	class CLvwGroupEnum {
	private:
		CSysError    m_error;
		TLvwGroups   m_groups;
	public:
		CLvwGroupEnum(void);
		~CLvwGroupEnum(void);
	public:
		INT          Count (void)const;
		HRESULT      Create(void);
		const
		TLvwGroups&  Groups(void)const;
		TErrorRef    Error (void)const;
		const
		CLvwGroup&   Item  (const INT nIndex) const;
		CLvwGroup&   Item  (const INT nIndex);
	};
}}}
#endif/*_FGLVWUICONTROLLER_H_B42587D4_9EAB_45c3_A229_AFAC2327D1D1_INCLUDED*/