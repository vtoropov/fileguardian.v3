/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Mar-2018 at 6:01:44p, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian extended list view control data interface implementation file;
*/
#include "StdAfx.h"
#include "FG_lvw_RecordIface.h"

using namespace fw::desktop::data;

/////////////////////////////////////////////////////////////////////////////

INT       CLvwCmdType::Index(const eFsEventType::_e _type){
	switch (_type) {
	case eFsEventType::eFsEventCreated: return 0;
	case eFsEventType::eFsEventDeleted: return 1;
	case eFsEventType::eFsEventAltered: return 2;
	case eFsEventType::eFsEventRenamed: return 3;
	case eFsEventType::eFsEventCopied : return 4;
	default:;
	}
	return -1;
}

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace data { namespace details
{
	const
	CLvwCommand& CLvwCommand_GetInvalidRef(void)
	{
		static const CLvwCommand na_(_T("#na"), _T("#Undefined"), _T("na_cls"), eFsEventType::eFsEventUnknown, false);
		return na_;
	}

	LPCTSTR      CLvwCommand_GetPrefix(void)
	{
		static LPCTSTR prfx_ = _T("command:");
		return prfx_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CLvwCommand::CLvwCommand(void) : m_bEnabled(true), m_action(eFsEventType::eFsEventUnknown), m_associated(0) {}

CLvwCommand::CLvwCommand(LPCTSTR lpszId, LPCTSTR lpszName, LPCTSTR lpszClass, const eFsEventType::_e _type, const bool bEnabled) :
	m_identifier(lpszId), m_name(lpszName), m_class(lpszClass), m_bEnabled(bEnabled), m_action(_type), m_associated(0)
{}

/////////////////////////////////////////////////////////////////////////////

UINT       CLvwCommand::Associated(void)const    { return m_associated; }
VOID       CLvwCommand::Associated(const UINT _v){ m_associated = _v;   }
eFsEventType::_e
           CLvwCommand::Action(void)const        { return m_action;    }
VOID       CLvwCommand::Action(const eFsEventType::_e _type)
                                                 { m_action = _type;   }

LPCTSTR    CLvwCommand::Class  (void)const       { return m_class.GetString(); }
VOID       CLvwCommand::Class  (LPCTSTR _cls)    { m_class = _cls;      }
bool       CLvwCommand::Enabled(void)const       { return m_bEnabled;   }
VOID       CLvwCommand::Enabled(const bool _val) { m_bEnabled = _val;   }

bool       CLvwCommand::HasThis(const CAtlString& _stream)const
{
	if (_stream.IsEmpty())
		return false;

	const INT n_ptr = _stream.Find(details::CLvwCommand_GetPrefix());
	if (-1 == n_ptr)
		return false;

	const CAtlString cs_cmd = _stream.Right(_stream.GetLength() - n_ptr - 1);

	return (0 == m_identifier.CollateNoCase(
				cs_cmd.GetString()));
}

LPCTSTR    CLvwCommand::Identifier(void)const    { return m_identifier.GetString(); }
VOID       CLvwCommand::Identifier(LPCTSTR _val) { m_identifier = _val; }
bool       CLvwCommand::IsValid(void)const       { return !m_name.IsEmpty() &&  eFsEventType::eFsEventUnknown != m_action; }
LPCTSTR    CLvwCommand::Name(void)const          { return  m_name.GetString(); }
VOID       CLvwCommand::Name(LPCTSTR lpszName)   { m_name = lpszName;   }
/////////////////////////////////////////////////////////////////////////////

LPCTSTR    CLvwCommand::Prefix(void) {
	return details::CLvwCommand_GetPrefix();
}

/////////////////////////////////////////////////////////////////////////////

CLvwCommands::CLvwCommands(void) {

	INT nIndex = 0;
	try {
		m_commands.push_back(
			CLvwCommand(_T("command:$rpt_page_added")  , _T("Added")  , _T("cls_bluecolor")  , eFsEventType::eFsEventCreated, true)
			);  m_index.insert(::std::make_pair(eFsEventType::eFsEventCreated, nIndex++));
		m_commands.push_back(
			CLvwCommand(_T("command:$rpt_page_deleted"), _T("Deleted"), _T("cls_redcolor")   , eFsEventType::eFsEventDeleted, true)
			);  m_index.insert(::std::make_pair(eFsEventType::eFsEventDeleted, nIndex++));
		m_commands.push_back(
			CLvwCommand(_T("command:$rpt_page_changed"), _T("Modified"), _T("cls_greencolor"), eFsEventType::eFsEventAltered, true)
			);  m_index.insert(::std::make_pair(eFsEventType::eFsEventAltered, nIndex++));
		m_commands.push_back(
			CLvwCommand(_T("command:$rpt_page_renamed"), _T("Renamed"), _T("cls_indcolor")   , eFsEventType::eFsEventRenamed, true)
			);  m_index.insert(::std::make_pair(eFsEventType::eFsEventRenamed, nIndex++));
		m_commands.push_back(
			CLvwCommand(_T("command:$rpt_page_copied" ), _T("Copied" ), _T("cls_yelcolor")   , eFsEventType::eFsEventCopied , true)
			);  m_index.insert(::std::make_pair(eFsEventType::eFsEventCopied, nIndex++));
	}
	catch(::std::bad_alloc&) {
	}
}

/////////////////////////////////////////////////////////////////////////////

INT          CLvwCommands::Count(void)const
{
	return static_cast<INT>(m_commands.size());
}

INT          CLvwCommands::IsCommand(LPCTSTR lpszURL)const
{
	CAtlString cs_url(lpszURL);

	if (CLvwCommands::eNotFound == cs_url.Find(details::CLvwCommand_GetPrefix()))
		return CLvwCommands::eNotFound;

	for (size_t i_ = 0; i_ < m_commands.size(); i_++) {

		const CLvwCommand& cmd_ = m_commands[i_];
		if (cmd_.HasThis(cs_url))
			return static_cast<INT>(i_);
	}
	return CLvwCommands::eNotFound;
}

const
CLvwCommand& CLvwCommands::Item(const eFsEventType::_e _type)const
{
	TLvwIndex::const_iterator it_ = m_index.find(_type);
	if (it_ != m_index.end())
		return m_commands[it_->second];
	else
		return details::CLvwCommand_GetInvalidRef();
}

const
CLvwCommand& CLvwCommands::Item(const INT nIndex)const
{
	if (0 > nIndex || nIndex > Count() - 1) {
		return details::CLvwCommand_GetInvalidRef();
	}
	else
		return m_commands[nIndex];
}

/////////////////////////////////////////////////////////////////////////////

CLvwTotals::CLvwTotals(void)
{
	::memset(&m_result[0], 0, _countof(m_result) * sizeof(INT));
}

CLvwTotals::~CLvwTotals(void)
{
}

/////////////////////////////////////////////////////////////////////////////

INT            CLvwTotals::Total(const eFsEventType::_e _type)const
{
	const INT nIndex = CLvwCmdType::Index(_type);

	if (0 > nIndex || nIndex > this->Count() - 1)
		return 0;
	else
		return m_result[nIndex];
}

HRESULT        CLvwTotals::Total(const eFsEventType::_e _type, const INT _val)
{
	const INT nIndex = CLvwCmdType::Index(_type);

	HRESULT hr_ = S_OK;
	if (0 > nIndex || nIndex > this->Count() - 1)
		return (hr_ = DISP_E_BADINDEX);

	if (0 > _val)
		return (hr_ = E_INVALIDARG);

	m_result[nIndex] = _val;

	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

VOID           CLvwTotals::Clear(void)
{
	::memset(&m_result[0], 0, _countof(m_result) * sizeof(INT));
}

INT            CLvwTotals::Count(void)const { return _countof(m_result); }

INT            CLvwTotals::TotalAll(void)const
{
	INT nTotal = 0;
	for (INT i_ = 0; i_ < this->Count(); i_++)
		nTotal += m_result[i_];

	return nTotal;
}

/////////////////////////////////////////////////////////////////////////////

CLvwTotals& CLvwTotals::operator= (const TOperateTotals& _totals)
{
	this->Clear();

	for (TOperateTotals::const_iterator it_ = _totals.begin(); it_ != _totals.end(); ++it_) {
		const eFsEventType::_e type_ = it_->first;

		this->Total(type_, it_->second);
	}
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CLvwAdapter::CLvwAdapter(void) { m_error.Source(_T("CLvwAdapter")); } 

/////////////////////////////////////////////////////////////////////////////

const
CLvwCommands&  CLvwAdapter::Commands(void)const { return m_commands; }
CLvwCommands&  CLvwAdapter::Commands(void)      { return m_commands; }
const
CFilterPersistent&       CLvwAdapter::Filter(void)const  { return m_dat_flt; }
CFilterPersistent&       CLvwAdapter::Filter(void)       { return m_dat_flt; }
const
CDbProvider&   CLvwAdapter::Provider(void)const { return m_dat_prv; }
CDbProvider&   CLvwAdapter::Provider(void)      { return m_dat_prv; }

const
CEventLog_Registry&CLvwAdapter::Storage(void)const { return m_dat_stg; }
CEventLog_Registry&CLvwAdapter::Storage(void)      { return m_dat_stg; }

/////////////////////////////////////////////////////////////////////////////

HRESULT        CLvwAdapter::Terminate(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	m_dat_prv.Accessor().Close();

	return m_error;
}

const
CLvwTotals&    CLvwAdapter::Totals(void)const { return m_totals; }
CLvwTotals&    CLvwAdapter::Totals(void)      { return m_totals; }

/////////////////////////////////////////////////////////////////////////////

CLvwRecord::CLvwRecord(void) : m_act_id(0), m_state(0), m_time_as_is(0) {}
CLvwRecord::CLvwRecord(const CRecord& _record) : m_act_id(0), m_state(0), m_time_as_is(0) { this->Apply(_record);}
CLvwRecord::~CLvwRecord(void) {}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR       CLvwRecord::Action   (void)const { return m_buffer[CLvwDataSpec::eAction].GetString();}
HRESULT       CLvwRecord::Action   (const INT nId)
{
	HRESULT hr_ = S_OK;
	if (nId < 1)
		return (hr_ = E_INVALIDARG);

	m_act_id = nId;

	return  hr_;
}

const UINT    CLvwRecord::ActionId (void)const { return m_act_id; }
LPCTSTR       CLvwRecord::FilePath (void)const { return m_buffer[CLvwDataSpec::eFilePath].GetString(); }

HRESULT       CLvwRecord::FilePath (LPCTSTR lpszPath)
{
	HRESULT hr_ = S_OK;
	if (NULL == lpszPath || !::_tcslen(lpszPath))
		return (hr_ = E_INVALIDARG);

	m_buffer[CLvwDataSpec::eFilePath] = lpszPath;

	return  hr_;
}

const UINT    CLvwRecord::State    (void)const { return m_state; }
VOID          CLvwRecord::State    (const INT nValue, const bool bAppend) {
	if (bAppend)
		m_state |= nValue;
	else
		m_state  = nValue;
}

LPCTSTR       CLvwRecord::Timestamp(void)const { return m_buffer[CLvwDataSpec::eTimestamp].GetString(); }
const
LONGLONG&     CLvwRecord::TimestampEx(void)const { return m_time_ex; }
const time_t& CLvwRecord::TimestampRaw(void)const { return m_time_as_is; }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CLvwRecord::Apply   (const CRecord& _record)
{
	HRESULT hr_ = S_OK;
	
	m_buffer[CLvwDataSpec::eAction] = _record.ActionAsText();
	m_buffer[CLvwDataSpec::eFilePath] = _record.Target();
	m_buffer[CLvwDataSpec::eTimestamp] = _record.TimestampAsText();

	m_act_id = _record.Action();
	m_state  = 0;
	m_time_as_is = _record.Timestamp();
	m_time_ex    = _record.TimestampEx();

	return  hr_;
}

VOID          CLvwRecord::Clear   (void)
{
	for (INT i_ = 0; i_ < _countof(m_buffer); i_++){
		if (m_buffer[i_].IsEmpty() == false)
			m_buffer[i_].Empty();
	}
	m_act_id = eFsEventType::eFsEventUnknown;
	m_state  = 0;
}

bool          CLvwRecord::IsValid (void)const
{
	const bool bResult = (
			!m_buffer[CLvwDataSpec::eAction].IsEmpty() &&
			!m_buffer[CLvwDataSpec::eFilePath].IsEmpty() &&
			!m_buffer[CLvwDataSpec::eTimestamp].IsEmpty() && m_act_id > 0
		);
	return bResult;
}

/////////////////////////////////////////////////////////////////////////////

CLvwRecord&   CLvwRecord::operator=(const CRecord& _record) { this->Apply(_record); return *this; }

/////////////////////////////////////////////////////////////////////////////

CLvwSize::CLvwSize(void) : m_nOffset(0), m_nLimit(0) {}
CLvwSize::CLvwSize(const INT _offset, const INT _limit) : m_nOffset(_offset), m_nLimit(_limit) {}
CLvwSize::~CLvwSize(void) {}

/////////////////////////////////////////////////////////////////////////////

bool       CLvwSize::IsEmpty(void)const { return (1 > m_nLimit); }
bool       CLvwSize::InRange(const INT nIndex)const { return ((nIndex >= m_nOffset) && (nIndex < m_nOffset + m_nLimit)); }
const
INT&       CLvwSize::Limit (void)const { return  m_nLimit;  }
INT&       CLvwSize::Limit (void)      { return  m_nLimit;  }
const
INT&       CLvwSize::Offset(void)const { return  m_nOffset; }
INT&       CLvwSize::Offset(void)      { return  m_nOffset; }

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace data { namespace details
{
	using namespace fg::common::data;
	using namespace fg::common::data::local;

	class CLvwCache_Query {

	private:
		CLvwAdapter&  m_adapter;
		CSysError     m_error;
		bool          m_auto_scroll;

	public:
		CLvwCache_Query(CLvwAdapter& _adapter) : m_adapter(_adapter), m_auto_scroll(false) { m_error.Source(_T("CLvwCache_Query")); }

	public:

		HRESULT       Do (const CLvwSize& _sz, CLvwRecord _records[]) {
			m_error.Module(_T(__FUNCTION__));
			m_error = S_OK;

			if (_sz.IsEmpty())
				return (m_error = E_INVALIDARG);

			if (m_adapter.Provider().Accessor().IsOpen() == false)
				return (m_error = OLE_E_BLANK);

			CDbReader& reader_ = m_adapter.Provider().Reader();

			TRecords data_ = reader_.Records(
			         m_adapter.Filter(), _sz.Offset(), _sz.Limit()
				);
			if (reader_.Error())
				m_error = reader_.Error();

			if (m_error)
				return m_error;

			for (size_t i_ = 0; i_ < data_.size() && i_ < CLvwCache::eEntriesCount; i_++) {

				const CRecord& record_ = data_[i_];
				_records[i_] = record_;
				
			}

			return  m_error;
		}

		HRESULT       Do (const CLvwSize& _sz, const bool bAutoScroll, CLvwRecord _records[]) {
			m_auto_scroll = bAutoScroll;
			return this->Do(_sz, _records);
		}

		TErrorRef     Error(void)const { return m_error; }
	};

	const CLvwRecord& CLvwCache_InvalidObjectRef(void) {
		static CLvwRecord rec_;
		return rec_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CLvwCache::CLvwCache(CLvwAdapter& _adapter) : m_adapter(_adapter) { m_error.Source(_T("CLvwCache")); }
CLvwCache::~CLvwCache(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CLvwCache::Clear(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	for (INT i_ = 0; i_ < _countof(m_records); i_++ ) {
		m_records[i_].Clear();
	}

	m_size.Limit()  = 0;
	m_size.Offset() = 0;

	return  m_error;
}

TErrorRef     CLvwCache::Error(void)const { return m_error; }
const
CLvwRecord&   CLvwCache::Item(const INT _index, bool bWithUpdateOpt)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (0 > _index) {
		m_error = DISP_E_BADINDEX; return details::CLvwCache_InvalidObjectRef();
	}

	if (m_size.InRange(_index) == false && bWithUpdateOpt) {

		this->Update(_index, _index, true);
	}

	if (CLvwCache::eEntriesCount - 1 < _index - m_size.Offset()) {
		m_error = DISP_E_BADINDEX; return details::CLvwCache_InvalidObjectRef();
	}
	else
		return m_records[_index - m_size.Offset()];
}
const
CLvwSize      CLvwCache::Size(void)const { return m_size; }

HRESULT       CLvwCache::Update(const INT _from, const INT /*_upto*/, const bool bAutoScroll)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;
	
	if (m_size.InRange(_from) == false)
	{
		m_size.Offset() = _from;
		m_size.Limit () = CLvwCache::eEntriesCount;

		details::CLvwCache_Query query(m_adapter);

		HRESULT hr_ = query.Do(
						m_size, bAutoScroll, m_records
					);
		if (FAILED(hr_))
			m_error = query.Error();
	}

	return  m_error;
}