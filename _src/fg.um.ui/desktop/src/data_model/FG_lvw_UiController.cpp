/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-May-2018 at 9:22:04a, UTC+7, Novosibirsk, Rodniki, Sunday;
	This is File Guardian desktop UI app event list view output controller interface implementation file. 
*/
#include "StdAfx.h"
#include "FG_lvw_UiController.h"

using namespace fw::desktop::data;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace data { namespace details {
	VOID     CLvwGroup_ComposeCap(const CLvwGroup& _group, CAtlString& _cap) {
		CAtlString cs_pattern(_T("Files %s"));
		CAtlString cs_template;
		switch (_group.Type()) {
			case eFsEventType::eFsEventCreated   :cs_template.Format(cs_pattern, _T("Created"));  break;
			case eFsEventType::eFsEventCopied    :cs_template.Format(cs_pattern, _T("Copied"));   break;
			case eFsEventType::eFsEventAltered   :cs_template.Format(cs_pattern, _T("Modified")); break;
			case eFsEventType::eFsEventRenamed   :cs_template.Format(cs_pattern, _T("Renamed"));  break;
			case eFsEventType::eFsEventDeleted   :cs_template.Format(cs_pattern, _T("Deleted"));  break;
			case eFsEventType::eFsEventMoved     :cs_template.Format(cs_pattern, _T("Moved"));    break;
			case eFsEventType::eFsEventReplaced  :cs_template.Format(cs_pattern, _T("Replaced")); break;
			case eFsEventType::eFsEventMovedToRecycle:cs_template.Format(cs_pattern, _T("Recycled"));   break;
			case eFsEventType::eFsEventRestoredFromRecycle:cs_template.Format(cs_pattern, _T("Restored")); break;
		}
		_cap.Format(
				_T("%s (%d)"), (LPCTSTR)cs_template, _group.Elements()
			);
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CLvwGroup::CLvwGroup(const eFsEventType::_e _type) : m_e_type(_type), m_n_els(0) {}
CLvwGroup::~CLvwGroup(void) {}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR      CLvwGroup::Caption(void)const {
	if (this->IsValid() == false)
		return NULL;
	return m_s_name;
}

INT          CLvwGroup::Elements(void)const    { return m_n_els;  }
HRESULT      CLvwGroup::Elements(const INT _v) { if (_v < 0) return E_INVALIDARG; m_n_els = _v; return S_OK; }
bool         CLvwGroup::IsValid(void)const     { return (eFsEventType::eFsEventUnknown != m_e_type && -1 < m_n_els); }
eFsEventType::_e
             CLvwGroup::Type(void)const        { return m_e_type; }
VOID         CLvwGroup::Type(const eFsEventType::_e _v) { m_e_type = _v; details::CLvwGroup_ComposeCap(*this, this->m_s_name); }

/////////////////////////////////////////////////////////////////////////////

CLvwGroup&   CLvwGroup::operator=(const CSystemEvent& _evt) {
	this->m_e_type = _evt._id;
	this->m_n_els  = 0;
	details::CLvwGroup_ComposeCap(*this, this->m_s_name);
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CLvwGroupEnum::CLvwGroupEnum(void) { m_error.Source(_T("CLvwGroupEnum")); }
CLvwGroupEnum::~CLvwGroupEnum(void){}

/////////////////////////////////////////////////////////////////////////////

INT          CLvwGroupEnum::Count (void)const { return static_cast<INT>(m_groups.size()); }
HRESULT      CLvwGroupEnum::Create(void) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (m_groups.empty() == false) m_groups.clear();

	CSystemEventEnum enum_;
	const TSystemEventCollection& evts_ = enum_.Events();

	for (size_t i_ = 0; i_ < evts_.size(); i_++) {
		const CSystemEvent& evt_obj = evts_[i_];
		CLvwGroup group_; group_ = evt_obj;
		try {
			m_groups.push_back(group_);
		}
		catch (::std::bad_alloc&) {
			m_error.SetState(
				E_OUTOFMEMORY, _T("Cannot append event '%s'"), (LPCTSTR)evt_obj._name
			); break;
		}
	}

	return m_error;
}

TErrorRef    CLvwGroupEnum::Error (void)const { return m_error; }
const
TLvwGroups&  CLvwGroupEnum::Groups(void)const { return m_groups; }
const
CLvwGroup&   CLvwGroupEnum::Item  (const INT nIndex) const {
	if (0 > nIndex || nIndex > this->Count() - 1) {
		static CLvwGroup na_group;
		return na_group;
	}
	else
		return m_groups[nIndex];
}
CLvwGroup&   CLvwGroupEnum::Item  (const INT nIndex) {
	if (0 > nIndex || nIndex > this->Count() - 1) {
		static CLvwGroup na_group;
		return na_group;
	}
	else
		return m_groups[nIndex];
}