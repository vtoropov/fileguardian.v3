/*
	Created by Tech_dog (VToropov) on 14-Jan-2016 at 10:27:39pm, GMT+7, Phuket, Rawai, Thursday;
	This is File Watcher desktop application precompiled headers implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 10:08:47a, UTC+7, Phuket, Rawai, Thursday;
*/

#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)

#include "FG_Generic_Defs.h"
#include "fg.desktop.tab.set.h"

namespace global
{
	namespace _hidden
	{
		using fw::desktop::UI::components::CTabView;
		using fw::desktop::UI::components::CTabSetMain;
		using fw::desktop::UI::components::CStatusMsgType;

		class CStatusMessage
		{
		private:
			CStatusMsgType::_e
			               m_type;
			CAtlString     m_text;
		public:
			CStatusMessage(void) : m_type(CStatusMsgType::eInfo), m_text(_T("Ready")) {}
			CStatusMessage(const CStatusMsgType::_e _e, LPCTSTR _t) : m_type(_e), m_text(_t){}
		public:
			CAtlString         Text(void)const { return m_text; }
			CStatusMsgType::_e Type(void)const { return m_type; }
		};

		typedef ::std::map<CTabView::ID, CStatusMessage> TMessageMap;

		class CMessageStore
		{
		private:
			TMessageMap    m_map;
			 CTabView::ID  m_redirectViewId;
			bool           m_bUseRedirectedViewId;
		public:
			CMessageStore(void):m_bUseRedirectedViewId(false), m_redirectViewId(CTabView::eEventLog) {}
		public:
			CStatusMessage Get(const CTabView::ID _viewId)const
			{
				TMessageMap::const_iterator it_ = m_map.find(_viewId);
				if (it_ != m_map.end())
					return it_->second;
				else
					return CStatusMessage();
			}
			VOID           Set(const CStatusMsgType::_e _type, LPCTSTR _text)
			{
				CTabSetMain& tabs_ = fw::desktop::UI::components::GetMainTabSetObject();
				const CTabView selected_ = tabs_.Selected();

				this->Set(_type, _text, selected_.Identifier());
			}

			VOID           Set(const CStatusMsgType::_e _type, LPCTSTR _text, const CTabView::ID _viewId)
			{				
				const CTabView::ID selected_ = (m_bUseRedirectedViewId ? m_redirectViewId : _viewId);
				m_map[selected_] = CStatusMessage(_type, _text);

				CTabSetMain& tabs_ = fw::desktop::UI::components::GetMainTabSetObject();
				tabs_.SetErrorImage(selected_, CStatusMsgType::eError == _type);
			}
		public:
			VOID           RedirectTo(const CTabView::ID _viewId, const bool bEnable)
			{
				m_redirectViewId = _viewId;
				m_bUseRedirectedViewId = bEnable;
			}
		public:
			static CTabView::ID DwordToViewId(const DWORD _dwId)
			{
				CTabView::ID id_ = CTabView::eEventLog;

				switch (_dwId)
				{
				case 0: case 1: case 2: case 3: case 4: case 5:
					{
						id_ = static_cast<CTabView::ID>(_dwId);
					} break;
				}
				return id_;
			}
		};

		CMessageStore& GetMsgStoreObject(void)
		{
			static CMessageStore store_;
			return store_;
		}
	}

	using namespace fw::desktop::UI::components;

	UINT GetBroadcastMsg (void)
	{
		static UINT msg_ = 0;
		if (msg_ == 0)
			msg_ = ::RegisterWindowMessage(_FW_BROADCAST_MESSAGE_CLOSE);
		return msg_;
	}

	CAtlString FormatErrorMessage (TErrorRef _err)
	{
		CAtlString src_(_err.Source());
		if (src_.IsEmpty())
			src_ = _T("#n/a");

		CAtlString cs_desc = _err.GetDescription();

		if (cs_desc.Right(1) == _T('.')) // we do not need the dot before semicolon;
		    cs_desc = cs_desc.Left(
					cs_desc.GetLength() - 1
				);

		CAtlString cs_error;
		cs_error.Format(
				_T("Error: %s; code=0x%x; source=%s"), cs_desc.GetString(), _err.GetHresult(), src_.GetString()
			);
		return cs_error;
	}

/////////////////////////////////////////////////////////////////////////////

	VOID SetGenericMessage(LPCTSTR lpszText, CStatusMsgType::_e _type, const bool bUpdateStore, const bool bTruncate = false)
	{
		GetStatusBarObject().SetText(lpszText, _type, bTruncate);
		if (bUpdateStore)
		{
			_hidden::CMessageStore& store_ = _hidden::GetMsgStoreObject();
			store_.Set(_type, lpszText);
		}
	}

	VOID SetErrorMessage  (TErrorRef _err , const bool bUpdateStateOpt)
	{
		CAtlString cs_error = FormatErrorMessage(_err);
		SetErrorMessage(cs_error);
		if (bUpdateStateOpt)
			GetStatusBarObject().SetState(_T("Error"), CStatusColor::eRed);
	}

	VOID SetErrorMessage  (LPCTSTR lpszText)
	{
		SetGenericMessage(lpszText, CStatusMsgType::eError, true);
	}

	VOID SetInfoMessage   (LPCTSTR lpszText)
	{
		CAtlString msg_(lpszText);
		if (msg_.IsEmpty())
			msg_ = _T("Ready");
		SetGenericMessage(msg_, CStatusMsgType::eInfo, true);
	}

	VOID SetWaitMessage   (LPCTSTR lpszText)
	{
		CAtlString msg_(lpszText);
		if (msg_.IsEmpty())
			msg_ = _T("Waiting...");
		SetGenericMessage(msg_, CStatusMsgType::eWaiting, true);
	}

	VOID SetWarnMessage   (LPCTSTR lpszText, const bool bTruncate )
	{
		SetGenericMessage(lpszText, CStatusMsgType::eWarning, true, bTruncate);
	}

/////////////////////////////////////////////////////////////////////////////

	VOID SetRedirectView(const DWORD _viewId, const bool bEnable)
	{
		const CTabView::ID id_ = _hidden::CMessageStore::DwordToViewId(_viewId);
		_hidden::CMessageStore& store_ = _hidden::GetMsgStoreObject();
		store_.RedirectTo(
				id_,
				bEnable
			);
	}

	VOID PopupLastMessage (const DWORD _viewId)
	{
		const CTabView::ID id_ = _hidden::CMessageStore::DwordToViewId(_viewId);
		const _hidden::CMessageStore& store_ = _hidden::GetMsgStoreObject();
		const _hidden::CStatusMessage msg_ = store_.Get(id_);

		SetGenericMessage(
				msg_.Text(),
				msg_.Type(),
				false
			);
	}

	VOID SetErrorMsgOnInit(const DWORD _viewId, TErrorRef _err, const bool bUpdateStateOpt)
	{
		CAtlString cs_error = FormatErrorMessage(_err);
		SetErrorMsgOnInit(_viewId, cs_error);
		if (bUpdateStateOpt)
			GetStatusBarObject().SetState(_T("Error"), CStatusColor::eRed);
	}

	VOID SetErrorMsgOnInit(const DWORD _viewId, LPCTSTR lpszText)
	{
		_hidden::CMessageStore& store_ = _hidden::GetMsgStoreObject();
		store_.Set(
				CStatusMsgType::eError,
				lpszText,
				_hidden::CMessageStore::DwordToViewId(_viewId)
			);
	}

/////////////////////////////////////////////////////////////////////////////

	VOID SetErrorMessage  (const DWORD _viewId, TErrorRef _err)
	{
		CAtlString cs_error = FormatErrorMessage(_err);

		_hidden::CMessageStore& store_ = _hidden::GetMsgStoreObject();
		store_.Set(
				CStatusMsgType::eError,
				cs_error,
				_hidden::CMessageStore::DwordToViewId(_viewId)
			);
	}

}