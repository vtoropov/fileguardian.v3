#ifndef _FWDESKTOPPRECOMPILEDHEADER_H_043DE573_37BB_4dd6_8202_C2909779E6EC_INCLUDED
#define _FWDESKTOPPRECOMPILEDHEADER_H_043DE573_37BB_4dd6_8202_C2909779E6EC_INCLUDED
/*
	Created by Tech_dog (VToropov) on 14-Jan-2016 at 1:32:15pm, GMT+7, Phuket, Rawai, Thursday;
	This is File Watcher desktop application precompiled headers definition file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 8:44:48a, UTC+7, Phuket, Rawai, Thursday;
*/
#ifndef WINVER
#define WINVER          0x0600  // Specifies that the minimum required platform is Windows Vista.
#endif

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // Specifies that the minimum required platform is Windows Vista.
#endif

#ifndef _WIN32_WINDOWS
#define _WIN32_WINDOWS  0x0600  // Specifies that the minimum required platform is Windows Vista.
#endif

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // Specifies that the minimum required platform is Internet Explorer 7.0.
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe
#pragma warning(disable: 4458)  // declaration of {declaration} hides class member; (GDI+)

#include <atlbase.h>
#include <atlstr.h>             // important order, this file must be included before any includes of the WTL headers
#include <atlapp.h>

extern CAppModule _Module;

#include <atlwin.h>
#include <atlframe.h>
#include <atlctrls.h>
#include <atldlgs.h>
#include <atlcrack.h>
#include <atlsafe.h>
#include <atltheme.h>
#include <atlfile.h>

#include <comdef.h>
#include <comutil.h>

#include <map>
#include <vector>
#include <time.h>

#include "FW_desktop_StatusBarEx.h"

#include "Shared_SystemError.h"
#include "Shared_LogJournal.h"

using shared::log::CEventJournal;

namespace global
{
	UINT GetBroadcastMsg(void);
	VOID SetErrorMessage(TErrorRef, const bool bUpdateStateOpt = false);
	VOID SetErrorMessage(LPCTSTR  );
	VOID SetInfoMessage (LPCTSTR  );
	VOID SetWaitMessage (LPCTSTR  );
	VOID SetWarnMessage (LPCTSTR , const bool bTruncate = false);

	VOID SetRedirectView(const DWORD _viewId, const bool bEnable);

	VOID PopupLastMessage (const DWORD _viewId);
	VOID SetErrorMsgOnInit(const DWORD _viewId, TErrorRef, const bool bUpdateStateOpt);// this error message must be sent from initialize function of a tab, when it is not selected yet
	VOID SetErrorMsgOnInit(const DWORD _viewId, LPCTSTR lpszText);                     // this error message must be sent from initialize function of a tab, when it is not selected yet

	VOID SetErrorMessage  (const DWORD _viewId, TErrorRef); // this is for timer/automatic events when source tab page is not selected;
}

#pragma comment(lib, "__shared.lite_v15.lib")
#pragma comment(lib, "_browser_v15.lib")
#pragma comment(lib, "_crypto_v15.lib")
#pragma comment(lib, "_generic.stg_v15.lib")
#pragma comment(lib, "_log_v15.lib")
#pragma comment(lib, "_net_v15.lib")
#pragma comment(lib, "_ntfs_v15.lib")
#pragma comment(lib, "_registry_v15.lib")
#pragma comment(lib, "_runnable_v15.lib")
#pragma comment(lib, "_service_v15.lib")
#pragma comment(lib, "_uix.ctrl_v15.lib")
#pragma comment(lib, "_uix.draw_v15.lib")
#pragma comment(lib, "_uix.frms_v15.lib")
#pragma comment(lib, "_uix.view.print_v15.lib")
#pragma comment(lib, "_user.32_v15.lib")
#pragma comment(lib, "_wmi.service_v15.lib")
#pragma comment(lib, "_xml_v15.lib")
#pragma comment(lib, "fg.generic.shared_v15.lib")
#pragma comment(lib, "fg.service.db.local_v15.lib")
#pragma comment(lib, "fg.service.shared_v15.lib")
#pragma comment(lib, "fg.um.filter.bridge_v15.lib")
#pragma comment(lib, "lib.mcrypt.adopted_v15.lib")

#endif/*_FWDESKTOPPRECOMPILEDHEADER_H_043DE573_37BB_4dd6_8202_C2909779E6EC_INCLUDED*/