#ifndef _FWDESKTOPUICOMMONDEFS_H_591600E9_7DB4_4ec1_BE04_755F53449EE0_INCLUDED
#define _FWDESKTOPUICOMMONDEFS_H_591600E9_7DB4_4ec1_BE04_755F53449EE0_INCLUDED
/*
	Created by Tech_dog (VToropov) on 14-Jan-2016 at 11:44:33am, GMT+7, Phuket, Rawai, Thursday;
	This is File Watcher desktop UI common definitions file.
*/

namespace fw { namespace desktop { namespace UI
{
	interface ITabPageCallback
	{
		virtual LRESULT TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) PURE;
	};

	interface ITabPageCmdHandler
	{
		virtual BOOL    TabPage_OnCommand(const WORD wCmdId) PURE;
	};

	interface ITabSetCallback
	{
		virtual bool    TabSet_IsNewMode(void) PURE;
		virtual HRESULT TabSet_OnDataChanged (const UINT pageId, const bool bChanged) PURE;
		virtual HRESULT TabSet_OnDataComplete(const UINT pageId) {pageId; return E_NOTIMPL;}
	};

	class CTabPageBase:
		public  ::ATL::CDialogImpl<CTabPageBase>,
		public  ITabPageCmdHandler
	{
		typedef ::ATL::CDialogImpl<CTabPageBase> TBaseDialog;
	public:
		const UINT IDD;
	protected:
		bool                m_bInitilized;
		INT                 m_nIndex;
		::WTL::CTabCtrl&    m_ctrl_ref;      // parent tabbed control reference
		ITabPageCallback&   m_evt_sink;
	protected:
		CTabPageBase(const UINT RID, ::WTL::CTabCtrl&, ITabPageCallback&);
		virtual ~CTabPageBase(void);
	public:
		BEGIN_MSG_MAP(CTabPageBase)
			MESSAGE_HANDLER (WM_INITDIALOG,  OnPageInit )
			MESSAGE_HANDLER (WM_PAINT,       OnPagePaint)
			m_evt_sink.TabPage_OnEvent(uMsg, wParam, lParam, bHandled);
			if (bHandled)
				return TRUE;
		END_MSG_MAP()
	public:
		virtual CAtlString  GetPageTitle(void) const PURE;
		virtual VOID        UpdateData  (const DWORD _opt = 0)       {_opt;}
		virtual VOID        UpdateLayout(void) {}
		virtual HRESULT     Validate(void)     const { return E_NOTIMPL; };
	protected:
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		virtual LRESULT     OnPagePaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	protected: // ITabPageCmdHandler
		virtual BOOL        TabPage_OnCommand(const WORD wCmdId) override;
	public:
		INT                 Index(void)const;
		VOID                Index(const INT);
	};

	VOID       AlignComboHeightTo(const CWindow& _host, const UINT _combo_id, const UINT _height);
	VOID       AlignCtrlsByHeight(const CWindow& _host, const UINT _ctrl_0, const UINT _ctrl_1, const SIZE& _shifts);

	class CWindowEx : public ATL::CWindow
	{
		typedef ATL::CWindow TWindow;
	public:
		CWindowEx(const CWindow&);
	public:
		CWindowEx& operator=(const CWindow&);
	public:
		RECT      GetDlgItemRect(const UINT _ctrl_id)const;
	};

	class CCtrlAutoState
	{
	private:
		const
		UINT      m_ctrlId;
		CWindow   m_host;
		bool      m_bEnabled; // the old enable status that will be restored on this class object destruction
	public:
		CCtrlAutoState(const CWindow& _host, const UINT ctrlId, const bool bEnabled);
		~CCtrlAutoState(void);
	};
}}}

#endif/*_FWDESKTOPUICOMMONDEFS_H_591600E9_7DB4_4ec1_BE04_755F53449EE0_INCLUDED*/