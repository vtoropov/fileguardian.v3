#ifndef _FWDESKTOPMAINFRAME_H_B39417B6_3801_4521_B00A_0589B9B71AA6_INCLUDED
#define _FWDESKTOPMAINFRAME_H_B39417B6_3801_4521_B00A_0589B9B71AA6_INCLUDED
/*
	Created by Tech_dog (VToropov) on 15-Jan-2016 at 12:49:42pm, GMT+7, Phuket, Rawai, Friday,
	This is File Watcher Desktop Main Frame class declaration file.
*/
#include "UIX_ImgHeader.h"
#include "fg.desktop.tab.set.h"
#include "FW_desktop_StatusBarEx.h"
#include "FW_desktop_PrintView.h"
#include "FW_desktop_MenuBar.h"

#include "Shared_SystemError.h"

namespace fw { namespace desktop { namespace UI
{
	using ex_ui::frames::CImageHeader;
	using ex_ui::printing::IPrintEventSink;

	using fw::desktop::UI::components::CTabSetMain;
	using fw::desktop::UI::components::CStatusBar;
	using fw::desktop::UI::components::CPrintPreview;
	using fw::desktop::UI::components::CMenuBar;

	using shared::lite::common::CSysError;

	class CMainFrame
	{
	private:
		class CMainDialogImpl : public ::ATL::CDialogImpl<CMainDialogImpl>, public IPrintEventSink
		{
			typedef ::ATL::CDialogImpl<CMainDialogImpl> TBaseDlg;
		private:
			CImageHeader        m_header;
			CStatusBar&         m_status; // a reference to global object
			CTabSetMain&        m_tabset; // a reference to global object
			CPrintPreview       m_printview;
			CMenuBar            m_menubar;
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CMainDialogImpl)
				MESSAGE_HANDLER      (WM_DESTROY      ,  OnDestroy    )
				MESSAGE_HANDLER      (WM_DRAWITEM     ,  OnOwnerDraw  )
				MESSAGE_HANDLER      (WM_INITDIALOG   ,  OnInitDialog )
				MESSAGE_HANDLER      (WM_PRINTVIEW    ,  OnPrintView  )
				MESSAGE_HANDLER      (WM_SYSCOMMAND   ,  OnSysCommand )
				MESSAGE_HANDLER      (WM_INITMENUPOPUP,  OnSubMenuInit)
				MESSAGE_HANDLER      (WM_MOUSEWHEEL   ,  OnMouseWheel )
				COMMAND_RANGE_HANDLER(
							CMenuBar::CommandFirst(),
							CMenuBar::CommandLast() ,
							OnMenuCommand
						)
				NOTIFY_CODE_HANDLER  (TCN_SELCHANGE   ,  OnTabNotify  )
			END_MSG_MAP()
		public:
			CMainDialogImpl(void);
			~CMainDialogImpl(void);
		private:
			LRESULT OnDestroy    (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnInitDialog (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnMouseWheel (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnOwnerDraw  (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnPrintView  (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnSubMenuInit(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnSysCommand (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
		private:
			LRESULT OnTabNotify  (INT idCtrl, LPNMHDR pnmh, BOOL& bHandled);
		private:
			LRESULT OnMenuCommand(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
		private: // IPrintEventSink
			HRESULT IPrintEvent_OnClose(void) override sealed;
			HRESULT IPrintEvent_OnError(const CSysError&) override sealed;
			HRESULT IPrintEvent_OnPageChanged(const INT nPage, const INT nPages) override sealed;
			HRESULT IPrintEvent_OnPagePrint(const INT nPage, const INT nPages) override sealed;
			HRESULT IPrintEvent_OnPrintAborted(void) override sealed;
			HRESULT IPrintEvent_OnPrintComplete(void) override sealed;
		};
	private:
		CMainFrame::CMainDialogImpl  m_dlg;
	public:
		CMainFrame(void);
		~CMainFrame(void);
	public:
		BOOL       Create (void); // modeless
		INT_PTR    DoModal(void);
	private:
		CMainFrame(const CMainFrame&);
		CMainFrame& operator= (const CMainFrame&);
	};
}}}

#endif/*_FWDESKTOPMAINFRAME_H_B39417B6_3801_4521_B00A_0589B9B71AA6_INCLUDED*/