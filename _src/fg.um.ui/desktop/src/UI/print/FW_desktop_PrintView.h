#ifndef _FWDESKTOPPRINTVIEW_H_BE4DCB82_C0B5_408f_A6E8_CA495E391E39_INCLUDED
#define _FWDESKTOPPRINTVIEW_H_BE4DCB82_C0B5_408f_A6E8_CA495E391E39_INCLUDED
/*
	Created by Tech_dog (VToropov) on 7-Apr-2016 at 11:31:47am, GMT+7, Phuket, Rawai, Thursday;
	This is File Guardian desktop UI print view class declaration file.
*/
#include "Shared_PrintView.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	using ex_ui::printing::CPrintView;
	using ex_ui::printing::IPrintEventSink;
	using ex_ui::printing::IPrintDataProvider;

	class CPrintPreview :
		public  CPrintView
	{
		typedef CPrintView TView;
	public:
		CPrintPreview(IPrintEventSink&, IPrintDataProvider&);
		~CPrintPreview(void);
	};
}}}}

#endif/*_FWDESKTOPPRINTVIEW_H_BE4DCB82_C0B5_408f_A6E8_CA495E391E39_INCLUDED*/