/*
	Created by Tech_dog (VToropov) on 7-Apr-2016 at 11:46:49am, GMT+7, Phuket, Rawai, Thursday;
	This is File Guardian desktop UI print view class implementation file.
*/
#include "StdAfx.h"
#include "FW_desktop_PrintView.h"

using namespace fw::desktop::UI::components;

/////////////////////////////////////////////////////////////////////////////

CPrintPreview::CPrintPreview(IPrintEventSink& _sink, IPrintDataProvider& _provider):TView(_sink, _provider)
{
}

CPrintPreview::~CPrintPreview(void)
{
}

/////////////////////////////////////////////////////////////////////////////