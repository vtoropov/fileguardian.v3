#ifndef _FWDESKTOPCHMHELPER_H_D8EEDC92_E4BA_4735_9510_79779EA24E5D_INCLUDED
#define _FWDESKTOPCHMHELPER_H_D8EEDC92_E4BA_4735_9510_79779EA24E5D_INCLUDED
/*
	Created by Tech_dog (VToropov) on 28-Jun-2016 at 9:19:55am, GMT+7, Phuket, Rawai, Tuesday;
	This is File Guardian desktop CHM help system wrapper class declaration file.
*/
#include "fg.desktop.tab.set.h"

namespace fw { namespace desktop { namespace UI
{
	using fw::desktop::UI::components::CTabView;

	class CChmHelper
	{
	private:
		HMODULE   m_help;
	public:
		CChmHelper(void);
		~CChmHelper(void);
	public:
		VOID      ShowHelpTopic(const CTabView::ID);
	private:
		CChmHelper(const CChmHelper&);
		CChmHelper& operator=(const CChmHelper&);
	};
}}}

#endif/*_FWDESKTOPCHMHELPER_H_D8EEDC92_E4BA_4735_9510_79779EA24E5D_INCLUDED*/