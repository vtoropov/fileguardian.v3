/*
	Created by Tech_dog (VToropov) on 14-Jan-2016 at 11:50:01am, GMT+7, Phuket, Rawai, Thursday;
	This is File Watcher desktop UI common definitions implementation file.
*/
#include "StdAfx.h"
#include "FW_desktop_UI_Defs.h"

using namespace fw::desktop::UI;

#include "Shared_GenericAppObject.h"

using namespace shared::lite::common;

#include "UIX_GdiProvider.h"

/////////////////////////////////////////////////////////////////////////////

CTabPageBase::CTabPageBase(const UINT RID, ::WTL::CTabCtrl& ctrl_ref, ITabPageCallback& evt_snk_ref):
	m_ctrl_ref(ctrl_ref),
	m_evt_sink(evt_snk_ref),
	IDD(RID),
	m_bInitilized(false),
	m_nIndex(-1)
{
}

CTabPageBase::~CTabPageBase(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CTabPageBase::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	RECT rcArea = {0};
	m_ctrl_ref.GetWindowRect(&rcArea);
	m_ctrl_ref.ScreenToClient(&rcArea);
	m_ctrl_ref.AdjustRect(false, &rcArea);
	TBaseDialog::SetWindowPos(0, rcArea.left, rcArea.top, __W(rcArea), __H(rcArea), SWP_NOZORDER|SWP_NOACTIVATE);
	if (::WTL::CTheme::IsThemingSupported())
	{
		HRESULT hr_ = ::EnableThemeDialogTexture(*this, ETDT_ENABLETAB);
		ATLVERIFY(SUCCEEDED(hr_));
	}
	m_evt_sink.TabPage_OnEvent(uMsg, wParam, lParam, bHandled);
	return 0;
}

LRESULT   CTabPageBase::OnPagePaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	::WTL::CPaintDC dc(TBaseDialog::m_hWnd);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

BOOL      CTabPageBase::TabPage_OnCommand(const WORD wCmdId)
{
	wCmdId;
	return FALSE; // not handled
}

/////////////////////////////////////////////////////////////////////////////

INT       CTabPageBase::Index(void)const
{
	return m_nIndex;
}

VOID      CTabPageBase::Index(const INT nIndex)
{
	m_nIndex = nIndex;
}

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI
{
	VOID       AlignComboHeightTo(const CWindow& _host, const UINT _combo_id, const UINT _height)
	{
		CWindow ctrl_ = _host.GetDlgItem(_combo_id);
		if (!ctrl_)
			return;

		static const INT _COMBOBOX_EDIT_MARGIN = 3;

		const INT pixHeight = _height - _COMBOBOX_EDIT_MARGIN * 2;
		LRESULT res__ = ERROR_SUCCESS;
		res__ = ctrl_.SendMessage(CB_SETITEMHEIGHT, (WPARAM)-1, pixHeight); ATLASSERT(res__ != CB_ERR);
		res__ = ctrl_.SendMessage(CB_SETITEMHEIGHT, (WPARAM) 0, pixHeight); ATLASSERT(res__ != CB_ERR);
	}

	VOID       AlignCtrlsByHeight(const CWindow& _host, const UINT _ctrl_0, const UINT _ctrl_1, const SIZE& _shifts)
	{
		RECT rc_0 = {0};
		CWindow ctrl_;
		ctrl_ = _host.GetDlgItem(_ctrl_0);
		if (ctrl_)
		{
			ctrl_.GetWindowRect(&rc_0);
			::MapWindowPoints(HWND_DESKTOP, _host, (LPPOINT)&rc_0, 0x2);
		}
		ctrl_ = _host.GetDlgItem(_ctrl_1);
		if (ctrl_ && !::IsRectEmpty(&rc_0))
		{
			RECT rc_1 = {0};
			ctrl_.GetWindowRect(&rc_1);
			::MapWindowPoints(HWND_DESKTOP, _host, (LPPOINT)&rc_1, 0x2);
			rc_1.top    = rc_0.top    + _shifts.cx;
			rc_1.bottom = rc_0.bottom + _shifts.cy;
			ctrl_.SetWindowPos(
					NULL,
					&rc_1,
					SWP_NOZORDER|SWP_NOACTIVATE
				);
		}
	}
}}}

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI
{
	//
	// it is necessary to keep this class implementation in namespace, which the class belongs to;
	// otherwise, class name coincidence appears with ATL window class;
	//
	CWindowEx::CWindowEx(const CWindow& _wnd) { *this = _wnd; }

/////////////////////////////////////////////////////////////////////////////

	CWindowEx& CWindowEx::operator=(const CWindow& _wnd) { TWindow::m_hWnd = _wnd; return *this; }

/////////////////////////////////////////////////////////////////////////////

	RECT      CWindowEx::GetDlgItemRect(const UINT _ctrl_id)const
	{
		RECT rc_ = {0};
		if (!*this)
			return rc_;

		CWindow ctrl_ = TWindow::GetDlgItem(_ctrl_id);
		if (!ctrl_)
			return rc_;

		ctrl_.GetWindowRect(&rc_);
		::MapWindowPoints(HWND_DESKTOP, *this, (LPPOINT)&rc_, 0x2);

		return rc_;
	}

}}}
/////////////////////////////////////////////////////////////////////////////

CCtrlAutoState::CCtrlAutoState(const CWindow& _host, const UINT ctrlId, const bool bEnabled):
m_host(_host), m_ctrlId(ctrlId), m_bEnabled(bEnabled)
{
	CWindow ctrl_ = m_host.GetDlgItem(m_ctrlId);
	if (ctrl_)
	{
		m_bEnabled = !!ctrl_.IsWindowEnabled();
		if (m_bEnabled != bEnabled)
			ctrl_.EnableWindow(static_cast<BOOL>(bEnabled));
	}
}

CCtrlAutoState::~CCtrlAutoState(void)
{
	CWindow ctrl_ = m_host.GetDlgItem(m_ctrlId);
	if (ctrl_)
		ctrl_.EnableWindow(static_cast<BOOL>(m_bEnabled));
}