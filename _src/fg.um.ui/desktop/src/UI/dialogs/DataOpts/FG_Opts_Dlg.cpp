/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Apr-2018 at 3:23:11p, UTC+7, Novosibirsk, Rodniki, Monday;
	This is File Guardian desktop application file event data options dialog interface implementation file.
*/
#include "StdAfx.h"
#include "FG_Opts_Dlg.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI::components;
using namespace fw::desktop::UI::dialogs;

#include "Shared_GenericAppResource.h"

using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace dialogs { namespace details
{
	class CDataOptsDlg_Layout
	{
	public:
		enum _e{
			logout_h = 165,
		};
	private:
		CWindow&      m_dlg_ref;
		RECT          m_client_area;
	public:
		CDataOptsDlg_Layout(CWindow& dlg_ref) : m_dlg_ref(dlg_ref)
		{
			if (!m_dlg_ref.GetClientRect(&m_client_area))
			{
				::SetRectEmpty(&m_client_area);
			}
		}
	public:
		RECT          RecalcTabArea(const SIZE& szHeader)
		{
			RECT rcTab = m_client_area;
			rcTab.top += szHeader.cy;
			::ATL::CWindow btn = m_dlg_ref.GetDlgItem(IDOK);
			if (btn)
			{
				RECT rc_btn = {0};
				if (btn.GetWindowRect(&rc_btn))
				{
					::MapWindowPoints(HWND_DESKTOP, m_dlg_ref, (LPPOINT)&rc_btn, 0x2);
					rcTab.bottom = rc_btn.top - 5;
				}
			}
			return rcTab;
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CDataOptsDlg::CDataOptsDlgImpl::CDataOptsDlgImpl(CEventLog_Registry& _stg): 
	IDD(IDD_FGDESKTOP_OPTS_DLG),
	m_header(IDR_FWDESKTOP_OPTS_DLG_HEADER), m_tabset(_stg), m_storage(_stg)
{
}

CDataOptsDlg::CDataOptsDlgImpl::~CDataOptsDlgImpl(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CDataOptsDlg::CDataOptsDlgImpl::OnButtonDown(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	wNotifyCode; wID; hWndCtl; bHandled = TRUE;
	TBaseDlg::EndDialog(wID);
	return 0;
}

LRESULT CDataOptsDlg::CDataOptsDlgImpl::OnDestroy   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;
	m_tabset.Destroy();
	m_header.Destroy();
	return 0;
}

LRESULT CDataOptsDlg::CDataOptsDlgImpl::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;

	HRESULT hr_ = m_header.Create(TBaseDlg::m_hWnd);
	if (!FAILED(hr_))
	{
	}

	details::CDataOptsDlg_Layout layout_(*this);

	const RECT rcTabs = layout_.RecalcTabArea(m_header.GetSize());
	hr_ = m_tabset.Create(*this, rcTabs);

	TBaseDlg::CenterWindow();
	{
		CApplicationIconLoader loader_(IDR_FWDESKTOP_MAIN_DLG_ICON);
		TBaseDlg::SetIcon(loader_.DetachLargeIcon(), TRUE);
		TBaseDlg::SetIcon(loader_.DetachSmallIcon(), FALSE);
	}
	return 0;
}

LRESULT CDataOptsDlg::CDataOptsDlgImpl::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			TBaseDlg::EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

LRESULT CDataOptsDlg::CDataOptsDlgImpl::OnTabNotify (INT idCtrl, LPNMHDR pnmh, BOOL& bHandled)
{
	idCtrl; pnmh; bHandled = TRUE;
	m_tabset.UpdateLayout();
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CDataOptsDlg::CDataOptsDlg(CEventLog_Registry& _stg) : m_dlg(_stg)
{
}

CDataOptsDlg::~CDataOptsDlg(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CDataOptsDlg::DoModal(void)
{
	const INT_PTR result_ = m_dlg.DoModal();
	switch (result_)
	{
	case IDCANCEL:  return S_FALSE;
	case IDNO    :
		{
			// forces to save default driver settings;
			m_dlg.m_tabset.AdvancedPage().UpdateData(IDNO);
			m_dlg.m_storage.Update().Enabled(false);
			m_dlg.m_storage.Update().Save();
			return OLE_E_PROMPTSAVECANCELLED;
		} break;
	default      :
		{
			// forces to save new driver settings;
			m_dlg.m_tabset.AdvancedPage().UpdateData(IDOK);

			m_dlg.m_storage.Update().Enabled(true);
			m_dlg.m_storage.Update().Save();
			return S_OK;
		}break;
	}
}