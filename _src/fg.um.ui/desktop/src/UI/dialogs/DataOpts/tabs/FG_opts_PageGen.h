#ifndef _FGOPTSPAGEGEN_H_68133F72_D23C_4f88_AE4A_59BFB457600F_INCLUDED
#define _FGOPTSPAGEGEN_H_68133F72_D23C_4f88_AE4A_59BFB457600F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Apr-2018 at 3:08:35p, UTC+7, Novosibirsk, Rodniki, Monday;
	This is File Guardian desktop application event data options dialog page interface declaration file.
*/
#include "FW_desktop_UI_Defs.h"
#include "FG_Log_Registry.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	using fw::desktop::UI::CTabPageBase;
	using fw::desktop::data::CEventLog_Registry;
	using fw::desktop::data::CEventLog_Update;

	class CTabPageOptsGen : public CTabPageBase, public  ITabPageCallback {

		typedef CTabPageBase   TBasePage;
	public:
		enum _e {
			eUpdateIntMin =    1,
			eUpdateIntMax = 1000,
		};
	private:
		WTL::CTabCtrl&         m_tabs;
		CEventLog_Registry&    m_storage;
	public:
		CTabPageOptsGen(WTL::CTabCtrl&, CEventLog_Registry& );
		~CTabPageOptsGen(void);
	private: // ITabPageCallback
		virtual LRESULT        TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
	public:  // TBasePage
		virtual CAtlString     GetPageTitle(void) const override sealed;
	};

}}}}


#endif/*_FGOPTSPAGEGEN_H_68133F72_D23C_4f88_AE4A_59BFB457600F_INCLUDED*/