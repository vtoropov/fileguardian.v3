/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Jun-2018 at 12:21:45p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian desktop application event data advanced options dialog page interface implementation file.
*/
#include "StdAfx.h"
#include "FG_opts_PageAdv.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI::components;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

#include "Shared_Registry.h"

using namespace shared::registry;

#include "FG_Generic_Defs.h"

using namespace fg::common::data;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components { namespace details {
	//
	// Log registry does not confirm to settings a driver;
	// it looks like service settings are much better, but they also do nothing for driver;
	// the only exception is a setting folders, but it is applied on another page and, moreover,
	// setting folders does not configure driver performance;
	// all above lead to creating separate registry wrapper class;
	//
	class CTabPageAdv_Registry {
	private:
		CRegistryStorage   m_stg;
	public:
		CTabPageAdv_Registry(void) :
		m_stg(CRegistryPathFinder::CService::SettingsRoot(), CRegistryOptions::eDoNotModifyPath) {}
	public:
		HRESULT Load(DWORD& _opts) {

			LONG l_saved = 0;

			HRESULT hr_ = m_stg.Load(
				CRegistryPathFinder::CExtension::Path(),
				CRegistryPathFinder::CExtension::Mode(),
				l_saved
			);
			if (SUCCEEDED(hr_))
				_opts = static_cast<DWORD>(l_saved);
			return  hr_;
		}

		HRESULT Save(const DWORD _opts) {

			HRESULT hr_ = m_stg.Save(
				CRegistryPathFinder::CExtension::Path(),
				CRegistryPathFinder::CExtension::Mode(),
				(DWORD)_opts
			);
			return  hr_;
		}
	};

	class CTabPageAdv_Layout
	{
	private:
		RECT           m_client_area;
		const CWindow& m_page_ref;
		CEventLog_Registry&
			m_storage;
	public:
		CTabPageAdv_Layout(const CWindow& page_ref, CEventLog_Registry& _stg) : m_page_ref(page_ref), m_storage(_stg)
		{
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		VOID    AdjustCtrls(void) {

			HBITMAP hBitmap = NULL;
			HRESULT hr_ = CGdiPlusPngLoader::LoadResource(
				IDR_FWDESKTOP_MAIN_DLG_WARN20,
				NULL,
				hBitmap
			);
			if (SUCCEEDED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_OPTS_ADV_STAT);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}
		}
	};

	class CTabPageAdv_Init
	{
	private:
		CWindow& m_page_ref;
		CEventLog_Registry&
		         m_storage;
	public:
		CTabPageAdv_Init(CWindow& dlg_ref, CEventLog_Registry& _stg) : m_page_ref(dlg_ref), m_storage(_stg){}
	public:
		VOID  InitCtrls(void)
		{
			CComboBox box_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_OPTS_ADV_LEVEL);
			if (box_) {
				box_.AddString(_T("Normal"));
				box_.AddString(_T("Aggressive"));
				box_.SetCurSel(_last_selected_level());
			}
#if(1)
			CTabPageAdv_Registry reg_;
			HRESULT hr_ = reg_.Load(_last_selected_level());
			if (SUCCEEDED(hr_)) {
				box_.SetCurSel(_last_selected_level());
			}
#endif
		}

		VOID  UpdateCtrls(void) {

			CComboBox box_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_OPTS_ADV_LEVEL);
			CWindow label_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_OPTS_ADV_STAT_V);
			if (label_ && box_) {
				INT n_sel_ = box_.GetCurSel();
				if (n_sel_ < 0)
					n_sel_ = 0;
				_last_selected_level() = static_cast<DWORD>(n_sel_);
				CAtlString cs_msg;
				if (0 == _last_selected_level()) cs_msg.LoadString(IDS_FGDESKTOP_ADV_NORMAL);
				if (1 == _last_selected_level()) cs_msg.LoadString(IDS_FGDESKTOP_ADV_AGRESSIVE);

				CAtlString cs_com;
				cs_com.LoadString(IDS_FGDESKTOP_ADV_COMMON);
				cs_msg += cs_com;

				label_.SetWindowText(cs_msg.GetString());
			}
		}

		VOID  UpdateData(void)
		{
			CTabPageAdv_Registry reg_;
			HRESULT hr_ = reg_.Save(_last_selected_level());
			hr_;
		}

		VOID UpdateDefault(const DWORD _level) {

			CTabPageAdv_Registry reg_;
			HRESULT hr_ = reg_.Save(_level);
			hr_;
		}
	private:
		DWORD&   _last_selected_level(void) { static DWORD level_ = 0; return level_;}
	};

	class CTabPageAdv_Handler
	{
	private:
		CWindow&  m_page_ref;
		CEventLog_Registry&
		          m_storage;
	public:
		CTabPageAdv_Handler(CWindow& dlg_ref, CEventLog_Registry& _stg) : m_page_ref(dlg_ref), m_storage(_stg){}
	public:
		BOOL  OnCommand(const WORD ctrlId, const WORD wNotify)
		{
			BOOL bHandled = FALSE; wNotify; ctrlId;

			if (IDC_FWDESKTOP_OPTS_ADV_LEVEL == ctrlId &&
			    CBN_SELCHANGE == wNotify) {

				details::CTabPageAdv_Init init_(m_page_ref, m_storage);
				init_.UpdateCtrls();

				bHandled = TRUE;
			}

			return bHandled;
		}

	};

}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageOptsAdv::CTabPageOptsAdv(WTL::CTabCtrl& _tabs, CEventLog_Registry& _stg):
	TBasePage(IDD_FGDESKTOP_OPTS_ADV_PAGE, _tabs, *this), m_tabs(_tabs), m_storage(_stg) { }

CTabPageOptsAdv::~CTabPageOptsAdv(void) { }

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageOptsAdv::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;

			details::CTabPageAdv_Layout layout_(*this, m_storage);
			layout_.AdjustCtrls();

			details::CTabPageAdv_Init init_(*this, m_storage);
			init_.InitCtrls();
			init_.UpdateCtrls();

			TBasePage::m_bInitilized = true;
			bHandled = TRUE;
		} break;
	case WM_DESTROY:
		{
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			details::CTabPageAdv_Handler handler_(*this, m_storage);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
		} break;
	case WM_NOTIFY:
		{
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString CTabPageOptsAdv::GetPageTitle(void) const
{
	CAtlString cs_title(_T("Advanced"));
	return cs_title;
}

VOID       CTabPageOptsAdv::UpdateData  (const DWORD _opt) {
	details::CTabPageAdv_Init init_(*this, m_storage);
	//
	// TODO: Advanced search does not work on driver side; it needs more tests;
	//       (has been fixed)
	//
	if (IDNO == _opt)
		init_.UpdateDefault(0); // restores 'Normal' mode by default;
	else
		init_.UpdateData();     // applies a recently selected level;
}