/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Apr-2018 at 4:55:53p, UTC+7, Novosibirsk, Rodniki, Monday;
	This is File Guardian desktop application event data options dialog page interface implementation file.
*/
#include "StdAfx.h"
#include "FG_opts_PageGen.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI::components;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui;
using namespace ex_ui::draw::common;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components { namespace details
{
	class CTabPageGen_Layout
	{
	private:
		RECT           m_client_area;
		const CWindow& m_page_ref;
		CEventLog_Registry&
		               m_storage;
	public:
		CTabPageGen_Layout(const CWindow& page_ref, CEventLog_Registry& _stg) : m_page_ref(page_ref), m_storage(_stg)
		{
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		VOID    AdjustCtrls(void)
		{
			HBITMAP hBitmap = NULL;
			HRESULT hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_MAIN_DLG_INFO24,
								NULL,
								hBitmap
							);
			if (SUCCEEDED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_OPTS_GEN_INFO);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}
			hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_MAIN_DLG_WARN20,
								NULL,
								hBitmap
							);
			if (SUCCEEDED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_OPTS_GEN_STAT);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}
		}
	};

	class CTabPageGen_Init
	{
	private:
		CWindow& m_page_ref;
		CEventLog_Registry&
		         m_storage;
	public:
		CTabPageGen_Init(CWindow& dlg_ref, CEventLog_Registry& _stg) : m_page_ref(dlg_ref), m_storage(_stg){}
	public:
		VOID  InitCtrls(void)
		{
			WTL::CUpDownCtrl ctrl_ = m_page_ref.GetDlgItem(IDC_FGDESKTOP_OPTS_GEN_FREQ);
			if (ctrl_)
			{
				const INT nPeriod = static_cast<LONG>(m_storage.Update().Period());

				ctrl_.SetRange(
						CTabPageOptsGen::eUpdateIntMin,
						CTabPageOptsGen::eUpdateIntMax
					);
				ctrl_.SetPos(nPeriod);

				WTL::CEdit edit_ = m_page_ref.GetDlgItem(IDC_FGDESKTOP_OPTS_GEN_SECS);
				if (edit_)
				{
					edit_.SetLimitText(4);
					//
					// TODO: updown control does not draw its borders properly at least under Win10;
					//
					//	ctrl_.SetBuddy(edit_);
					//
					// TODO: it's necessary to apply last saved value, not default one; (done)
					//
					m_page_ref.SetDlgItemInt(
							IDC_FGDESKTOP_OPTS_GEN_SECS,
							nPeriod,
							FALSE
						);
				}
			}
			//
			// auto-scroll is disabled and unchecked for now;
			//
			WTL::CButton auto_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_OPTS_GEN_SCRL);
			if (auto_) {
				auto_.SetCheck(
						m_storage.Update().AutoScroll() && (FALSE) ? BST_CHECKED : BST_UNCHECKED
					);
			}
		}

		VOID  UpdateCtrls(void)
		{
			CWindow state_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_OPTS_GEN_STAT_V);
			if (!state_)
				return;
			const bool bEnabled = m_storage.Update().Enabled();
			CAtlString cs_state;
			cs_state.Format(
					_T("Current state: the feature is%s active"), (bEnabled ? _T("") : _T(" not"))
				);
			state_.SetWindowText(cs_state.GetString());
		}

		VOID  UpdateData(void)
		{
			WTL::CUpDownCtrl ctrl_ = m_page_ref.GetDlgItem(IDC_FGDESKTOP_OPTS_GEN_SECS);
			if (ctrl_)
			{
				CAtlString cs_sec;
				ctrl_.GetWindowText(cs_sec);

				const INT nPeriod = ::_tstoi(cs_sec.GetString());

				m_storage.Update().Period(nPeriod);
			}
			WTL::CButton auto_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_OPTS_GEN_SCRL);
			if (auto_) {
				m_storage.Update().AutoScroll(BST_CHECKED == auto_.GetCheck());
			}
		}
	};

	class CTabPageGen_Handler
	{
	private:
		CWindow&  m_page_ref;
		CEventLog_Registry&
		               m_storage;
	public:
		CTabPageGen_Handler(CWindow& dlg_ref, CEventLog_Registry& _stg) : m_page_ref(dlg_ref), m_storage(_stg){}
	public:
		BOOL  OnCommand(const WORD ctrlId, const WORD wNotify)
		{
			BOOL bHandled = TRUE; wNotify; ctrlId;

			if (false){}
			else if (IDC_FWDESKTOP_OPTS_GEN_SCRL == ctrlId) {

				details::CTabPageGen_Init init_(m_page_ref, m_storage);
				init_.UpdateData();

			}
			else
				bHandled = FALSE;

			return bHandled;
		}

	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageOptsGen::CTabPageOptsGen(WTL::CTabCtrl& _tabs, CEventLog_Registry& _stg):
	TBasePage(IDD_FGDESKTOP_OPTS_GEN_PAGE, _tabs, *this), m_tabs(_tabs), m_storage(_stg)
{
}

CTabPageOptsGen::~CTabPageOptsGen(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageOptsGen::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;

			details::CTabPageGen_Layout layout_(*this, m_storage);
			layout_.AdjustCtrls();

			details::CTabPageGen_Init init_(*this, m_storage);
			init_.InitCtrls();
			init_.UpdateCtrls();

			TBasePage::m_bInitilized = true;
			bHandled = TRUE;
		} break;
	case WM_DESTROY:
		{
			details::CTabPageGen_Init init_(*this, m_storage);
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			details::CTabPageGen_Handler handler_(*this, m_storage);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
		} break;
	case WM_NOTIFY:
		{
			switch(wParam)
			{
			case IDC_FGDESKTOP_OPTS_GEN_FREQ:
				{
					LPNMUPDOWN  lpnmud = reinterpret_cast<LPNMUPDOWN>(lParam);
					if (NULL != lpnmud) {
						const UINT nValue = lpnmud->iPos + lpnmud->iDelta;
						this->SetDlgItemInt(
								IDC_FGDESKTOP_OPTS_GEN_SECS, nValue, FALSE
							);
						
						details::CTabPageGen_Init init_(*this, m_storage);
						init_.UpdateData();
					}
				} break;
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString CTabPageOptsGen::GetPageTitle(void) const
{
	CAtlString cs_title(_T("General"));
	return cs_title;
}