#ifndef _FGOPTSTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
#define _FGOPTSTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Apr-2018 at 3:51:20p, UTC+7, Novosibirsk, Rodniki, Monday;
	This is File Guardian desktop application file event data option tab set interface declaration file.
*/
#include "FG_opts_PageGen.h"
#include "FG_Log_Registry.h"
#include "FG_opts_PageAdv.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	using fw::desktop::data::CEventLog_Registry;
	using fw::desktop::data::CEventLog_Update;

	class CTabSetOpts
	{
	private:
		CEventLog_Registry&
		                 m_storage;
		WTL::CTabCtrl    m_cTabCtrl;
	private: //tab pages
		CTabPageOptsGen  m_general;
		CTabPageOptsAdv  m_advance;

	public:
		CTabSetOpts(CEventLog_Registry&);
		~CTabSetOpts(void);

	public:
		HRESULT       Create(const HWND hParent, const RECT& rcArea);
		HRESULT       Destroy(void);
		VOID          UpdateLayout(void);
	public:
		CTabPageBase& AdvancedPage(void);
	};
}}}}

#endif/*_FGOPTSTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED*/