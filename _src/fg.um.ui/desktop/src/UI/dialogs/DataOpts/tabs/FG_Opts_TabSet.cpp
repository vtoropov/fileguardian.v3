/*
	Created by Tech_dog (VToropov) on 16-Jan-2016 at 3:58:22pm, GMT+7, Phuket, Rawai, Saturday;
	This is File Watcher Desktop Data Filter Dailog Tab Set class implementation file.
*/
#include "StdAfx.h"
#include "FG_Opts_TabSet.h"

#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI;
using namespace fw::desktop::UI::components;

#include "Shared_Registry.h"

using namespace shared::registry;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components { namespace details
{
	class CTabSetOpts_Layout
	{
	private:
		RECT        m_area;
	public:
		CTabSetOpts_Layout(const RECT& rcArea) : m_area(rcArea)
		{
		}
	public:
		RECT        GetTabsArea(void)const
		{
			RECT rcTabs = m_area;
			::InflateRect(&rcTabs, -5, -3);
			return rcTabs;
		}
	};

	class CTabSetOpts_Registry
	{
	private:
		mutable
		CRegistryStorage m_stg;
	public:
		CTabSetOpts_Registry(void) : m_stg(HKEY_CURRENT_USER)
		{
		}
	public:
		INT     LastIndex(void)const
		{
			LONG lIndex = 0;
			m_stg.Load(
					this->_CommonRegFolder(),
					this->_LastIndexRegName(),
					lIndex
				);
			if (lIndex < 0)
				lIndex = 0;
			return static_cast<INT>(lIndex);
		}

		HRESULT LastIndex(const INT nIndex)
		{
			HRESULT hr_ = m_stg.Save(
					this->_CommonRegFolder(),
					this->_LastIndexRegName(),
					static_cast<LONG>(nIndex)
				);
			return  hr_;
		}
	private:
		LPCTSTR     _CommonRegFolder(void) const
		{
			static CAtlString cs_folder(_T("DataOpts"));
			return cs_folder;
		}

		LPCTSTR     _LastIndexRegName(void) const
		{
			static CAtlString cs_name(_T("LastTabIndex"));
			return cs_name;
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabSetOpts::CTabSetOpts(CEventLog_Registry& _stg) : m_general(m_cTabCtrl, _stg), m_storage(_stg), m_advance(m_cTabCtrl, _stg) { }
CTabSetOpts::~CTabSetOpts(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CTabSetOpts::Create(const HWND hParent, const RECT& rcArea)
{
	if(!::IsWindow(hParent) || ::IsRectEmpty(&rcArea))
		return E_INVALIDARG;

	details::CTabSetOpts_Layout layout(rcArea);
	RECT rcTabs = layout.GetTabsArea();

	m_cTabCtrl.Create(
			hParent,
			rcTabs,
			0,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			WS_EX_CONTROLPARENT,
			IDC_FWDESKTOP_OPTS_DLG_TABSET
		);
	m_cTabCtrl.SetFont(::ATL::CWindow(hParent).GetFont());

	INT nIndex = 0;
	const DWORD dwFlags = SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW;

	m_cTabCtrl.AddItem(m_general.GetPageTitle());
	{
		m_general.Create(m_cTabCtrl.m_hWnd);
		m_general.SetWindowPos(
				HWND_TOP,
				0, 0, 0, 0,
				dwFlags
			);
		if (m_general.IsWindow())m_general.Index(nIndex++);
	}

	m_cTabCtrl.AddItem(m_advance.GetPageTitle());
	{
		m_advance.Create(m_cTabCtrl.m_hWnd);
		m_advance.SetWindowPos(
			HWND_TOP,
			0, 0, 0, 0,
			dwFlags
		);
		if (m_advance.IsWindow())m_advance.Index(nIndex++);
	}

	details::CTabSetOpts_Registry reg_;
	nIndex = reg_.LastIndex();
	
	m_cTabCtrl.SetCurSel(nIndex);
	this->UpdateLayout();

	return S_OK;
}

HRESULT       CTabSetOpts::Destroy(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();

	details::CTabSetOpts_Registry reg_;
	reg_.LastIndex(nTabIndex);

	if (m_general.IsWindow())
	{
		m_general.DestroyWindow();
		m_general.m_hWnd = NULL;
	}

	if (m_advance.IsWindow())
	{
		m_advance.DestroyWindow();
		m_advance.m_hWnd = NULL;
	}

	return S_OK;
}

void          CTabSetOpts::UpdateLayout(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();

	if (m_general.IsWindow())   m_general.ShowWindow  (nTabIndex == m_general.Index  () ? SW_SHOW : SW_HIDE);
	if (m_advance.IsWindow())   m_advance.ShowWindow  (nTabIndex == m_advance.Index  () ? SW_SHOW : SW_HIDE);
}

/////////////////////////////////////////////////////////////////////////////

CTabPageBase& CTabSetOpts::AdvancedPage(void) {
	return m_advance;
}