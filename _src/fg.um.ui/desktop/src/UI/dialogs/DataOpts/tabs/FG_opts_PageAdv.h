#ifndef _FGOPTSPAGEADV_H_492CBE09_2BEF_4439_B19E_BC62836E5E53_INCLUDED
#define _FGOPTSPAGEADV_H_492CBE09_2BEF_4439_B19E_BC62836E5E53_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Jun-2018 at 12:15:31p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian desktop application event data advanced options dialog page interface declaration file.
*/
#include "FW_desktop_UI_Defs.h"
#include "FG_Log_Registry.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	using fw::desktop::UI::CTabPageBase;
	using fw::desktop::data::CEventLog_Registry;
	using fw::desktop::data::CEventLog_Update;

	class CTabPageOptsAdv : public CTabPageBase, public  ITabPageCallback {

		typedef CTabPageBase   TBasePage;

	private:
		WTL::CTabCtrl&         m_tabs;
		CEventLog_Registry&    m_storage;

	public:
		CTabPageOptsAdv(WTL::CTabCtrl&, CEventLog_Registry& );
		~CTabPageOptsAdv(void);

	private: // ITabPageCallback
		virtual LRESULT        TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	public:  // TBasePage
		virtual CAtlString     GetPageTitle(void) const override sealed;
		virtual VOID           UpdateData  (const DWORD _opt = 0) override sealed;
	};

}}}}

#endif/*_FGOPTSPAGEADV_H_492CBE09_2BEF_4439_B19E_BC62836E5E53_INCLUDED*/