#ifndef _FGOPTSDLG_H_576D692C_27DA_4182_B754_D881D519ED3F_INCLUDED
#define _FGOPTSDLG_H_576D692C_27DA_4182_B754_D881D519ED3F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Apr-2018 at 3:16:57p, UTC+7, Novosibirsk, Rodniki, Monday;
	This is File Guardian desktop application file event data options dialog interface declaration file.
*/
#include "UIX_ImgHeader.h"
#include "FG_Opts_TabSet.h"
#include "FG_Log_Registry.h"

namespace fw { namespace desktop { namespace UI { namespace dialogs
{
	using ex_ui::frames::CImageHeader;

	using fw::desktop::UI::components::CTabSetOpts;
	using fw::desktop::data::CEventLog_Registry;
	using fw::desktop::data::CEventLog_Update;

	class CDataOptsDlg
	{
	private:
		class CDataOptsDlgImpl :
			public  ::ATL::CDialogImpl<CDataOptsDlgImpl>
		{
			typedef ::ATL::CDialogImpl<CDataOptsDlgImpl> TBaseDlg;
			friend class CDataOptsDlg;
		private:
			CEventLog_Registry&
			                   m_storage;
			CImageHeader       m_header;
			CTabSetOpts        m_tabset;
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CDataOptsDlgImpl)
				MESSAGE_HANDLER     (WM_DESTROY   ,   OnDestroy   )
				MESSAGE_HANDLER     (WM_INITDIALOG,   OnInitDialog)
				MESSAGE_HANDLER     (WM_SYSCOMMAND,   OnSysCommand)
				COMMAND_ID_HANDLER  (IDOK         ,   OnButtonDown)
				COMMAND_ID_HANDLER  (IDCANCEL     ,   OnButtonDown)
				COMMAND_ID_HANDLER  (IDNO         ,   OnButtonDown)
				NOTIFY_CODE_HANDLER (TCN_SELCHANGE,   OnTabNotify )
			END_MSG_MAP()
		public:
			CDataOptsDlgImpl(CEventLog_Registry&);
			~CDataOptsDlgImpl(void);
		private:
			LRESULT OnButtonDown(WORD , WORD   , HWND   , BOOL&);
			LRESULT OnDestroy   (UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnInitDialog(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnSysCommand(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnTabNotify (INT  , LPNMHDR, BOOL&);
		};
	private:
		CDataOptsDlgImpl  m_dlg;
	public:
		CDataOptsDlg(CEventLog_Registry&);
		~CDataOptsDlg(void);
	public:
		HRESULT    DoModal(void);
	private:
		CDataOptsDlg(const CDataOptsDlg&);
		CDataOptsDlg& operator= (const CDataOptsDlg&);
	};

}}}}

#endif/*_FGOPTSDLG_H_576D692C_27DA_4182_B754_D881D519ED3F_INCLUDED*/