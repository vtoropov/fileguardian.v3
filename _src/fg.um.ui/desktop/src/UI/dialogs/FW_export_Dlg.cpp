/*
	Created by Tech_dog (VToropov) on 18-May-2016 at 9:45:02pm, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian desktop data export process dialog class implementation file.
*/
#include "StdAfx.h"
#include "FW_export_Dlg.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI::dialogs;

#include "Shared_GenericAppResource.h"

using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

CDataExportDlg::CDataExportDlgImpl::CDataExportDlgImpl(HWND _source, const CAtlString _target): 
	IDD(IDD_FWDESKTOP_EXP_DLG),
	m_action(*this),
	m_source(_source),
	m_target(_target),
	m_timer(NULL),
	m_interrupted(false)
{
}

CDataExportDlg::CDataExportDlgImpl::~CDataExportDlgImpl(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CDataExportDlg::CDataExportDlgImpl::OnDestroy   (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	if (m_action.IsRunning())
		m_action.Stop();
	m_progress = NULL;
	m_label    = NULL;
	if (m_timer)
		TBaseDlg::KillTimer(m_timer);
	return 0;
}

LRESULT CDataExportDlg::CDataExportDlgImpl::OnDismiss   (WORD     , WORD wID     , HWND hWndCtl , BOOL& bHandled)
{
	wID; hWndCtl; bHandled;
	if (m_action.IsRunning())
		m_action.MarkToStop();

	CWindow btn_ = hWndCtl;
	btn_.EnableWindow(FALSE);

	global::SetWaitMessage(
			_T("Cancelling data export...")
		);
	m_interrupted = true;

	m_timer = TBaseDlg::SetTimer(1, 500);
	return 0;
}

LRESULT CDataExportDlg::CDataExportDlgImpl::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	TBaseDlg::CenterWindow();
	{
		CApplicationIconLoader loader_(IDR_FWDESKTOP_MAIN_DLG_ICON);
		TBaseDlg::SetIcon(loader_.DetachLargeIcon(), TRUE);
		TBaseDlg::SetIcon(loader_.DetachSmallIcon(), FALSE);
	}
	m_progress = TBaseDlg::GetDlgItem(IDC_FWDESKTOP_EXP_PROG);
	m_label = TBaseDlg::GetDlgItem(IDC_FWDESKTOP_EXP_LABEL);
	m_action.Start();
	return 0;
}

LRESULT CDataExportDlg::CDataExportDlgImpl::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			TBaseDlg::EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

LRESULT CDataExportDlg::CDataExportDlgImpl::OnTimerEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;
	if (m_action.IsComplete())
	{
		TBaseDlg::KillTimer(m_timer);
		m_timer = NULL;
		TBaseDlg::EndDialog(IDOK);

		if (m_interrupted)
			global::SetWarnMessage(
				_T("Data export has been interrupted")
			);
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool    CDataExportDlg::CDataExportDlgImpl::CsvDataProvider_CanContinue(void)
{
	return true;
}

VOID    CDataExportDlg::CDataExportDlgImpl::CsvDataProvider_OnComplete(void)
{
	global::SetInfoMessage(_T("Data export has been completed successfully"));
	m_timer = TBaseDlg::SetTimer(1, 500);
}

VOID    CDataExportDlg::CDataExportDlgImpl::CsvDataProvider_OnError(const CSysError _error)
{
	global::SetErrorMessage(_error);
	m_timer = TBaseDlg::SetTimer(1, 500);
}

VOID    CDataExportDlg::CDataExportDlgImpl::CsvDataProvider_OnProgress(const INT _ndx, const INT _total)
{
	if (_total < 1)
		return;

	static INT prior_ = 0;
	INT curr_ = INT(FLOAT(_ndx)/FLOAT(_total) * 100.0);
	if (prior_ != curr_)
	{
		prior_  = curr_;
		if (m_label)
		{
			CAtlString cs_value;
			cs_value.Format(
				_T("Data processed: %d %%"),
				curr_
				);
			m_label.SetWindowText(cs_value);
		}
		if (m_progress)
			m_progress.SetPos(curr_);
	}
}

VOID    CDataExportDlg::CDataExportDlgImpl::CsvDataProvider_OnStart(const INT _total)
{
	_total;
	if (m_progress)
		m_progress.SetRange(1, 100);
}

HWND    CDataExportDlg::CDataExportDlgImpl::CsvDataProvider_RequestSourceCtrl(void)
{
	return m_source;
}

CAtlString
        CDataExportDlg::CDataExportDlgImpl::CsvDataProvider_RequestTargetPath(void)
{
	return m_target;
}

/////////////////////////////////////////////////////////////////////////////

CDataExportDlg::CDataExportDlg(HWND _source, const CAtlString _target) : m_dlg(_source, _target)
{
}

CDataExportDlg::~CDataExportDlg(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CDataExportDlg::DoModal(void)
{
	const INT_PTR result = m_dlg.DoModal();

	return (result == IDCANCEL ? S_FALSE : S_OK);
}