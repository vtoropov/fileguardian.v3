/*
	Created by Tech_Dog (ebontrop@gmail.com) on 11-Apr-2017 at 3:47:25a, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian installer download UI dialog class implementation file.
*/
#include "StdAfx.h"
#include "FW_download_Dlg.h"
#include "FW_desktop_Resource.h"

using namespace shared::net;
using namespace fw::desktop::UI::dialogs;

#include "Shared_GenericAppResource.h"

using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

CDownloadDlg::CDownloadDlgImpl::CDownloadDlgImpl(const CAtlString& _source, const CAtlString& _target): 
	IDD(IDD_FWDESKTOP_UPD_DLG),
	m_source(_source),
	m_target(_target),
	m_timer(NULL),
	m_interrupted(false),
	m_downloader(*this)
{
	m_error.Source(_T("CDownloadDlgImpl"));
}

CDownloadDlg::CDownloadDlgImpl::~CDownloadDlgImpl(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CDownloadDlg::CDownloadDlgImpl::OnDestroy(UINT  uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	if (m_downloader.IsRunning())
		m_downloader.Stop();

	m_progress = NULL;
	m_label    = NULL;

	if (m_timer)
		TBaseDlg::KillTimer(m_timer); m_timer = NULL;

	return 0;
}

LRESULT CDownloadDlg::CDownloadDlgImpl::OnDismiss(WORD, WORD, HWND hControl, BOOL& bHandled)
{
	bHandled = TRUE;
	m_interrupted = true;

	if (m_downloader.IsRunning())
		m_downloader.MarkToStop();

	CWindow cancel_button = hControl;
	cancel_button.EnableWindow(FALSE);

	global::SetWaitMessage(
			_T("Cancelling download...")
		);

	m_timer = TBaseDlg::SetTimer(1, 500);
	return 0;
}

LRESULT CDownloadDlg::CDownloadDlgImpl::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	TBaseDlg::CenterWindow();
	{
		CApplicationIconLoader loader_(IDR_FWDESKTOP_MAIN_DLG_ICON);
		TBaseDlg::SetIcon(loader_.DetachLargeIcon(), TRUE);
		TBaseDlg::SetIcon(loader_.DetachSmallIcon(), FALSE);
	}
	m_progress = TBaseDlg::GetDlgItem(IDC_FWDESKTOP_UPD_PROG);
	m_label    = TBaseDlg::GetDlgItem(IDC_FWDESKTOP_UPD_LABEL);
	m_downloader.SourceURL(m_source);
	m_downloader.TargetFile(m_target);
	m_downloader.Start();
	return 0;
}

LRESULT CDownloadDlg::CDownloadDlgImpl::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			CWindow cancel_button = TBaseDlg::GetDlgItem(IDCANCEL);
			this->OnDismiss(0, 0, cancel_button, bHandled);
		} break;
	}
	return 0;
}

LRESULT CDownloadDlg::CDownloadDlgImpl::OnTimerEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;
	if (m_downloader.IsComplete())
	{
		TBaseDlg::KillTimer(m_timer);
		m_timer = NULL;
		TBaseDlg::EndDialog(IDOK);
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool    CDownloadDlg::CDownloadDlgImpl::INetDownloader_OnCanContinue (void)
{
	return !m_interrupted;
}

void    CDownloadDlg::CDownloadDlgImpl::INetDownloader_OnBegin (void)
{
	global::SetInfoMessage(
			_T("Downloading the latest installer...")
		);
	if (m_progress)
		m_progress.SetRange(1, 100);
}

void    CDownloadDlg::CDownloadDlgImpl::INetDownloader_OnFinish(void)
{
	global::SetInfoMessage(_T("Downloading the installer has been completed successfully"));
	m_error = S_OK;
	m_timer = TBaseDlg::SetTimer(1, 500);
}

void    CDownloadDlg::CDownloadDlgImpl::INetDownloader_OnDataReceived(const DWORD dwReceived, const DWORD dwTotal)
{
	if (dwTotal < 1)
		return;

	static INT prior_ = 0;
	INT curr_ = INT(FLOAT(dwReceived)/FLOAT(dwTotal) * 100.0);
	if (prior_ != curr_)
	{
		prior_  = curr_;
		if (m_label)
		{
			CAtlString cs_value;
			cs_value.Format(
				_T("Downloading: %d %%"),
				curr_
				);
			m_label.SetWindowText(cs_value);
		}
		if (m_progress)
			m_progress.SetPos(curr_);
	}
}

void    CDownloadDlg::CDownloadDlgImpl::INetDownloader_OnError (CSysError _error)
{
	global::SetErrorMessage(_error);
	m_error = _error;
	m_timer = TBaseDlg::SetTimer(1, 500);
}

void    CDownloadDlg::CDownloadDlgImpl::INetDownloader_OnInterrupt(void)
{
	CAtlString cs_msg = _T("Downloading the installer has been interrupted.");
	global::SetWarnMessage(
			cs_msg
		);
	m_error.SetState(
			(DWORD)ERROR_CANCELLED,
			cs_msg
		);
}

/////////////////////////////////////////////////////////////////////////////

CDownloadDlg::CDownloadDlg(const CAtlString& _source, const CAtlString& _target) : m_dlg(_source, _target)
{
}

CDownloadDlg::~CDownloadDlg(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CDownloadDlg::DoModal(void)
{
	HRESULT hr_ = S_OK;
	m_dlg.DoModal();
	return  hr_;
}

TErrorRef  CDownloadDlg::Error(void)const
{
	return m_dlg.m_error;
}