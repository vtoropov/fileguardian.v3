/*
	Created by Tech_dog (VToropov) on 18-May-2016 at 1:37:46am, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian desktop UI application about dialog class implementation file.
*/
#include "StdAfx.h"
#include "FW_about_Dlg.h"
#include "FW_desktop_Resource.h"
#include "FG_Generic_Defs.h"

using namespace fw::desktop::UI::dialogs;

#include "Shared_GenericAppObject.h"
#include "Shared_GenericAppResource.h"

using namespace shared::user32;

using namespace ex_ui::draw;
using namespace ex_ui;
using namespace ex_ui::draw::common;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace dialogs { namespace details
{
	class CAboutDlg_Init
	{
	private:
		CWindow&  m_dlg_ref;
	public:
		CAboutDlg_Init(CWindow& _dlg_ref) : m_dlg_ref(_dlg_ref)
		{
		}
	public:
		VOID    InitCtrls(void)
		{
			const CApplication& the_app = GetAppObjectRef();

			CWindow label_ = m_dlg_ref.GetDlgItem(IDC_FWDESKTOP_ABOUT_DLG_COPY);
			if (label_)
			{
				CAtlString cs_copy = the_app.Version().CopyRight();
				label_.SetWindowText(cs_copy);
			}

			label_ = m_dlg_ref.GetDlgItem(IDC_FWDESKTOP_ABOUT_DLG_VERS);
			if (label_)
			{
				CAtlString cs_vers(FG_PRODUCT_VER);
				label_.SetWindowText(cs_vers);
			}

			label_ = m_dlg_ref.GetDlgItem(IDC_FWDESKTOP_ABOUT_DLG_RELS);
			if (label_)
			{
				CAtlString cs_release = the_app.Version().CustomValue(_T("ReleaseDate"));
				label_.SetWindowText(cs_release);
			}
		}
	};

	class CAboutDlg_Layout
	{
	private:
		RECT      m_client_area;
		CWindow&  m_dlg_ref;
	public:
		CAboutDlg_Layout(CWindow& _dlg_ref) : m_dlg_ref(_dlg_ref)
		{
			if (m_dlg_ref.IsWindow())
				m_dlg_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		VOID    AdjustCtrls(void)
		{
			HBITMAP hBitmap = NULL;
			HRESULT hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_ABOUT_DLG_LOGO,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_dlg_ref.GetDlgItem(IDC_FWDESKTOP_ABOUT_DLG_LOGO);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}

			hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_ABOUT_DLG_PARA,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_dlg_ref.GetDlgItem(IDC_FWDESKTOP_ABOUT_DLG_PARA);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}

			CWindow label_ = m_dlg_ref.GetDlgItem(IDC_FWDESKTOP_ABOUT_DLG_LAW);
			if (label_)
			{
				CAtlString cs_law;
				cs_law.LoadString(IDS_FWDESKTOP_ABOUT_DLG_LAW);
				label_.SetWindowText(cs_law);
			}
		}
		VOID    AdjustDlg(const SIZE _sz_header)
		{
			RECT rc_ = {0};
			m_dlg_ref.GetClientRect(&rc_);
			rc_.right = rc_.left + _sz_header.cx;

			::AdjustWindowRect(
					&rc_,
					DS_SETFONT|DS_FIXEDSYS|WS_MINIMIZEBOX|WS_CAPTION|WS_SYSMENU,
					FALSE
				);
			{
				RECT rcParent = {0};
				CWindow parent_ = m_dlg_ref.GetTopLevelWindow();
				parent_.GetWindowRect(&rcParent);

				INT left_ = rcParent.left + (__W(rcParent) - __W(rc_)) / 2;
				INT top_  = rcParent.top  + ::GetSystemMetrics(SM_CYCAPTION) * 2;

				::SetRect(
						&rc_,
						left_,
						top_,
						left_ + __W(rc_),
						top_ + __H(rc_)
					);
			}
			m_dlg_ref.MoveWindow(&rc_);
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CAboutDlg::CAboutDlgImpl::CAboutDlgImpl(void): 
	IDD(IDD_FWDESKTOP_ABOUT_DLG),
	m_header(IDR_FWDESKTOP_ABOUT_DLG_HEADER)
{
}

CAboutDlg::CAboutDlgImpl::~CAboutDlgImpl(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CAboutDlg::CAboutDlgImpl::OnCommand   (UINT, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	if (IDOK == wParam)
	{
		bHandled = TRUE;
		TBaseDlg::EndDialog(IDOK);
	}
	else
		bHandled = FALSE;
	return 0;
}


LRESULT CAboutDlg::CAboutDlgImpl::OnButtonDown(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	wNotifyCode; wID; hWndCtl; bHandled = TRUE;
	switch (wID)
	{
	case IDOK:
		{
		} break;
	}
	TBaseDlg::EndDialog(wID);
	return 0;
}

LRESULT CAboutDlg::CAboutDlgImpl::OnDestroy   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;
	m_header.Destroy();
	return 0;
}

LRESULT CAboutDlg::CAboutDlgImpl::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;

	HRESULT hr_ = m_header.Create(TBaseDlg::m_hWnd);
	if (!FAILED(hr_))
	{
		details::CAboutDlg_Layout layout_(*this);
		layout_.AdjustCtrls();

		const SIZE sz_ = m_header.GetSize();
		layout_.AdjustDlg(sz_);
	}
	details::CAboutDlg_Init init_(*this);
	init_.InitCtrls();
	{
		CApplicationIconLoader loader_(IDR_FWDESKTOP_MAIN_DLG_ICON);
		TBaseDlg::SetIcon(loader_.DetachLargeIcon(), TRUE);
		TBaseDlg::SetIcon(loader_.DetachSmallIcon(), FALSE);
	}
	return 0;
}

LRESULT CAboutDlg::CAboutDlgImpl::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			TBaseDlg::EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CAboutDlg::CAboutDlg(void)
{
}

CAboutDlg::~CAboutDlg(void)
{
}

/////////////////////////////////////////////////////////////////////////////

INT_PTR    CAboutDlg::DoModal(void)
{
	const INT_PTR result_ = m_dlg.DoModal();
	return result_;
}