#ifndef _FWPASSDLG_H_DADB01B9_2D3C_428b_9F8E_3F527A43ED5D_INCLUDED
#define _FWPASSDLG_H_DADB01B9_2D3C_428b_9F8E_3F527A43ED5D_INCLUDED
/*
	Created by Tech_dog (VToropov) on 25-Jan-2016 at 0:28:24am, GMT+7, Phuket, Rawai, Monday;
	This is File Watcher desktop password dialog class declaration file.
*/
#include "UIX_ImgHeader.h"

namespace fw { namespace desktop { namespace UI { namespace dialogs
{
	using ex_ui::frames::CImageHeader;

	class CPasswordDlg
	{
	private:
		class CPasswordDlgImpl :
			public  ::ATL::CDialogImpl<CPasswordDlgImpl>
		{
			typedef ::ATL::CDialogImpl<CPasswordDlgImpl> TBaseDlg;
		private:
			CImageHeader             m_header;
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CPasswordDlgImpl)
				MESSAGE_HANDLER     (WM_COMMAND   ,   OnCommand   )
				MESSAGE_HANDLER     (WM_DESTROY   ,   OnDestroy   )
				MESSAGE_HANDLER     (WM_INITDIALOG,   OnInitDialog)
				MESSAGE_HANDLER     (WM_SYSCOMMAND,   OnSysCommand)
				COMMAND_ID_HANDLER  (IDOK         ,   OnButtonDown)
				COMMAND_ID_HANDLER  (IDCANCEL     ,   OnButtonDown)
			END_MSG_MAP()
		public:
			CPasswordDlgImpl(void);
			~CPasswordDlgImpl(void);
		private:
			LRESULT OnCommand   (UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnButtonDown(WORD , WORD   , HWND   , BOOL&);
			LRESULT OnDestroy   (UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnInitDialog(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnSysCommand(UINT , WPARAM , LPARAM , BOOL&);
		};
	private:
		CPasswordDlgImpl  m_dlg;
	public:
		CPasswordDlg(void);
		~CPasswordDlg(void);
	public:
		INT_PTR    DoModal(void);
	private:
		CPasswordDlg(const CPasswordDlg&);
		CPasswordDlg& operator= (const CPasswordDlg&);
	};
}}}}

#endif/*_FWPASSDLG_H_DADB01B9_2D3C_428b_9F8E_3F527A43ED5D_INCLUDED*/