#ifndef _FWABOUTDLG_H_C0911FFC_DE50_4876_B155_6130A960D9DC_INCLUDED
#define _FWABOUTDLG_H_C0911FFC_DE50_4876_B155_6130A960D9DC_INCLUDED
/*
	Created by Tech_dog (VToropov) on 18-May-2016 at 1:33:16am, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian desktop UI application about dialog class declaration file.
*/
#include "UIX_ImgHeader.h"

namespace fw { namespace desktop { namespace UI { namespace dialogs
{
	using ex_ui::frames::CImageHeader;

	class CAboutDlg
	{
	private:
		class CAboutDlgImpl :
			public  ::ATL::CDialogImpl<CAboutDlgImpl>
		{
			typedef ::ATL::CDialogImpl<CAboutDlgImpl> TBaseDlg;
		private:
			CImageHeader             m_header;
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CAboutDlgImpl)
				MESSAGE_HANDLER     (WM_COMMAND   ,   OnCommand   )
				MESSAGE_HANDLER     (WM_DESTROY   ,   OnDestroy   )
				MESSAGE_HANDLER     (WM_INITDIALOG,   OnInitDialog)
				MESSAGE_HANDLER     (WM_SYSCOMMAND,   OnSysCommand)
				COMMAND_ID_HANDLER  (IDOK         ,   OnButtonDown)
				COMMAND_ID_HANDLER  (IDCANCEL     ,   OnButtonDown)
			END_MSG_MAP()
		public:
			CAboutDlgImpl(void);
			~CAboutDlgImpl(void);
		private:
			LRESULT OnCommand   (UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnButtonDown(WORD , WORD   , HWND   , BOOL&);
			LRESULT OnDestroy   (UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnInitDialog(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnSysCommand(UINT , WPARAM , LPARAM , BOOL&);
		};
	private:
		CAboutDlgImpl  m_dlg;
	public:
		CAboutDlg(void);
		~CAboutDlg(void);
	public:
		INT_PTR    DoModal(void);
	private:
		CAboutDlg(const CAboutDlg&);
		CAboutDlg& operator= (const CAboutDlg&);
	};
}}}}


#endif/*_FWABOUTDLG_H_C0911FFC_DE50_4876_B155_6130A960D9DC_INCLUDED*/