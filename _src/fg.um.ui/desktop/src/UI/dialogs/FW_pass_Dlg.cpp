/*
	Created by Tech_dog (VToropov) on 25-Jan-2016 at 0:48:31am, GMT+7, Phuket, Rawai, Monday;
	This is File Watcher desktop password dialog class implementation file.
*/
#include "StdAfx.h"
#include "FW_pass_Dlg.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI::dialogs;

#include "Shared_GenericAppResource.h"

using namespace shared::user32;

#include "FW_pass_Provider.h"

using namespace fw::desktop::data;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui;
using namespace ex_ui::draw::common;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace dialogs { namespace details
{
	class CPasswordDlg_Layout
	{
	private:
		RECT           m_client_area;
		const CWindow& m_page_ref;
	public:
		CPasswordDlg_Layout(const CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		VOID    AdjustCtrls(void)
		{
			HBITMAP hBitmap = NULL;
			HRESULT hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_MAIN_DLG_WARN16,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_PASS_DLG_WARN);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}
		}
	};

	class CPasswordDlg_Init
	{
	private:
		const CWindow& m_page_ref;
	public:
		CPasswordDlg_Init(const CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		VOID    InitCtrls(void)
		{
			::WTL::CEdit ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_PASS_DLG_PWD);
			if (ctrl_)
				ctrl_.SetCueBannerText(_T("Type the password"), TRUE);
		}
	};

	class CPasswordDlg_Handler
	{
	private:
		const CWindow& m_page_ref;
	public:
		CPasswordDlg_Handler(const CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		bool         CheckPassword(void)
		{
			CAtlString cs_pwd;

			CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_PASS_DLG_PWD);
			if (ctrl_)
				ctrl_.GetWindowText(cs_pwd);
			
			CPwdDataProvider prov_;
			HRESULT hr_ = prov_.CheckPassword(cs_pwd);
			if (FAILED(hr_))
			{
				ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_PASS_DLG_MSG);
				if (ctrl_)
					ctrl_.SetWindowText(_T("Invalid password."));
			}
			return (hr_ == S_OK);
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CPasswordDlg::CPasswordDlgImpl::CPasswordDlgImpl(void): 
	IDD(IDD_FWDESKTOP_PASS_DLG),
	m_header(IDR_FWDESKTOP_PASS_DLG_HEADER)
{
}

CPasswordDlg::CPasswordDlgImpl::~CPasswordDlgImpl(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CPasswordDlg::CPasswordDlgImpl::OnCommand   (UINT, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	if (IDOK == wParam)
	{
		bHandled = TRUE;
		details::CPasswordDlg_Handler handler_(*this);
		
		if (handler_.CheckPassword())
			TBaseDlg::EndDialog(IDOK);
	}
	else
		bHandled = FALSE;
	return 0;
}

LRESULT CPasswordDlg::CPasswordDlgImpl::OnButtonDown(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	wNotifyCode; wID; hWndCtl; bHandled = TRUE;
	switch (wID)
	{
	case IDOK:
		{
			details::CPasswordDlg_Handler handler_(*this);
			if (!handler_.CheckPassword())
				return 0;
		} break;
	}
	TBaseDlg::EndDialog(wID);
	return 0;
}

LRESULT CPasswordDlg::CPasswordDlgImpl::OnDestroy   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;
	m_header.Destroy();
	return 0;
}

LRESULT CPasswordDlg::CPasswordDlgImpl::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;

	HRESULT hr_ = m_header.Create(TBaseDlg::m_hWnd);
	if (!FAILED(hr_))
	{
		details::CPasswordDlg_Layout layout_(*this);
		layout_.AdjustCtrls();
	}
	details::CPasswordDlg_Init init_(*this);
	init_.InitCtrls();

	TBaseDlg::CenterWindow();
	{
		CApplicationIconLoader loader_(IDR_FWDESKTOP_MAIN_DLG_ICON);
		TBaseDlg::SetIcon(loader_.DetachLargeIcon(), TRUE);
		TBaseDlg::SetIcon(loader_.DetachSmallIcon(), FALSE);
	}
	return 0;
}

LRESULT CPasswordDlg::CPasswordDlgImpl::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			TBaseDlg::EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CPasswordDlg::CPasswordDlg(void)
{
}

CPasswordDlg::~CPasswordDlg(void)
{
}

/////////////////////////////////////////////////////////////////////////////

INT_PTR    CPasswordDlg::DoModal(void)
{
	const INT_PTR result_ = m_dlg.DoModal();
	return result_;
}