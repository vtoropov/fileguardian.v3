/*
	Created by Tech_dog (VToropov) on 16-Jan-2016 at 2:24:30pm, GMT+7, Phuket, Rawai, Saturday;
	This is File Watcher desktop data filter setup dialog class implementation file.
*/
#include "StdAfx.h"
#include "FW_filter_SetupDlg.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI::components;
using namespace fw::desktop::UI::dialogs;

#include "Shared_GenericAppResource.h"

using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace dialogs { namespace details
{
	class CDataFilterSetupDlg_Layout
	{
	public:
		enum _e{
			logout_h = 165,
		};
	private:
		CWindow&      m_dlg_ref;
		RECT          m_client_area;
	public:
		CDataFilterSetupDlg_Layout(CWindow& dlg_ref) : m_dlg_ref(dlg_ref)
		{
			if (!m_dlg_ref.GetClientRect(&m_client_area))
			{
				::SetRectEmpty(&m_client_area);
			}
		}
	public:
		RECT                 RecalcTabArea(const SIZE& szHeader)
		{
			RECT rcTab = m_client_area;
			rcTab.top += szHeader.cy;
			::ATL::CWindow btn = m_dlg_ref.GetDlgItem(IDOK);
			if (btn)
			{
				RECT rc_btn = {0};
				if (btn.GetWindowRect(&rc_btn))
				{
					::MapWindowPoints(HWND_DESKTOP, m_dlg_ref, (LPPOINT)&rc_btn, 0x2);
					rcTab.bottom = rc_btn.top - 5;
				}
			}
			return rcTab;
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CDataFilterSetupDlg::CDataFilterSetupDlgImpl::CDataFilterSetupDlgImpl(CFilter& _flt): 
	IDD(IDD_FWDESKTOP_FILTER_DLG),
	m_header(IDR_FWDESKTOP_FILTER_DLG_HEADER),
	m_tabset(_flt)
{
}

CDataFilterSetupDlg::CDataFilterSetupDlgImpl::~CDataFilterSetupDlgImpl(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CDataFilterSetupDlg::CDataFilterSetupDlgImpl::OnButtonDown(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	wNotifyCode; wID; hWndCtl; bHandled = TRUE;
	TBaseDlg::EndDialog(wID);
	return 0;
}

LRESULT CDataFilterSetupDlg::CDataFilterSetupDlgImpl::OnDestroy   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;
	m_tabset.Destroy();
	m_header.Destroy();
	return 0;
}

LRESULT CDataFilterSetupDlg::CDataFilterSetupDlgImpl::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;
	details::CDataFilterSetupDlg_Layout layout_(*this);

	HRESULT hr_ = m_header.Create(TBaseDlg::m_hWnd);
	if (!FAILED(hr_))
	{
	}

	const RECT rcTabs = layout_.RecalcTabArea(m_header.GetSize());
	hr_ = m_tabset.Create(*this, rcTabs);

	TBaseDlg::CenterWindow();
	{
		CApplicationIconLoader loader_(IDR_FWDESKTOP_MAIN_DLG_ICON);
		TBaseDlg::SetIcon(loader_.DetachLargeIcon(), TRUE);
		TBaseDlg::SetIcon(loader_.DetachSmallIcon(), FALSE);
	}
	return 0;
}

LRESULT CDataFilterSetupDlg::CDataFilterSetupDlgImpl::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			TBaseDlg::EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

LRESULT CDataFilterSetupDlg::CDataFilterSetupDlgImpl::OnTabNotify (INT idCtrl, LPNMHDR pnmh, BOOL& bHandled)
{
	idCtrl; pnmh; bHandled = TRUE;
	m_tabset.UpdateLayout();
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CDataFilterSetupDlg::CDataFilterSetupDlg(CFilter& _flt) : m_dlg(_flt)
{
}

CDataFilterSetupDlg::~CDataFilterSetupDlg(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CDataFilterSetupDlg::DoModal(void)
{
	const INT_PTR result_ = m_dlg.DoModal();
	switch (result_)
	{
	case IDCANCEL:  return S_FALSE;
	case IDNO    :  return ERROR_CANCELLED;
	default      :  return S_OK;
	}
}