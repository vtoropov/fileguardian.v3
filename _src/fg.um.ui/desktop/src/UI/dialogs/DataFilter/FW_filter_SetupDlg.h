#ifndef _FWDESKTOPFILTERDLG_H_ADD17CCD_2A97_4057_B6AB_57F18249E1E8_INCLUDED
#define _FWDESKTOPFILTERDLG_H_ADD17CCD_2A97_4057_B6AB_57F18249E1E8_INCLUDED
/*
	Created by Tech_dog (VToropov) on 16-Jan-2016 at 2:11:19pm, GMT+7, Phuket, Rawai, Saturday;
	This is File Watcher desktop data filter setup dialog class declaration file.
*/
#include "UIX_ImgHeader.h"
#include "FW_filter_TabSet.h"

namespace fw { namespace desktop { namespace UI { namespace dialogs
{
	using ex_ui::frames::CImageHeader;

	using fw::desktop::UI::components::CTabSetFilter;
	using fg::common::data::CFilter;

	class CDataFilterSetupDlg
	{
	private:
		class CDataFilterSetupDlgImpl :
			public  ::ATL::CDialogImpl<CDataFilterSetupDlgImpl>
		{
			typedef ::ATL::CDialogImpl<CDataFilterSetupDlgImpl> TBaseDlg;
		private:
			CImageHeader             m_header;
			CTabSetFilter            m_tabset;
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CDataFilterSetupDlgImpl)
				MESSAGE_HANDLER     (WM_DESTROY   ,   OnDestroy   )
				MESSAGE_HANDLER     (WM_INITDIALOG,   OnInitDialog)
				MESSAGE_HANDLER     (WM_SYSCOMMAND,   OnSysCommand)
				COMMAND_ID_HANDLER  (IDOK         ,   OnButtonDown)
				COMMAND_ID_HANDLER  (IDCANCEL     ,   OnButtonDown)
				COMMAND_ID_HANDLER  (IDNO         ,   OnButtonDown)
				NOTIFY_CODE_HANDLER (TCN_SELCHANGE,   OnTabNotify )
			END_MSG_MAP()
		public:
			CDataFilterSetupDlgImpl(CFilter&);
			~CDataFilterSetupDlgImpl(void);
		private:
			LRESULT OnButtonDown(WORD , WORD   , HWND   , BOOL&);
			LRESULT OnDestroy   (UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnInitDialog(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnSysCommand(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnTabNotify (INT  , LPNMHDR, BOOL&);
		};
	private:
		CDataFilterSetupDlgImpl  m_dlg;
	public:
		CDataFilterSetupDlg(CFilter&);
		~CDataFilterSetupDlg(void);
	public:
		HRESULT    DoModal(void);
	private:
		CDataFilterSetupDlg(const CDataFilterSetupDlg&);
		CDataFilterSetupDlg& operator= (const CDataFilterSetupDlg&);
	};
}}}}

#endif/*_FWDESKTOPFILTERDLG_H_ADD17CCD_2A97_4057_B6AB_57F18249E1E8_INCLUDED*/