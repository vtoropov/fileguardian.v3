/*
	Created by Tech_dog (VToropov) on 16-Jan-2016 at 3:46:45pm, GMT+7, Phuket, Rawai, Friday;
	This is File Watcher Desktop Data Filter Setup Dialog Tab Set class implementation file.
*/
#include "StdAfx.h"
#include "FW_filter_TabSet.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop;
using namespace fw::desktop::UI;
using namespace fw::desktop::UI::components;

#include "Shared_Registry.h"

using namespace shared::registry;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components { namespace details
{
	class CTabSetFilter_Layout
	{
	private:
		RECT        m_area;
	public:
		CTabSetFilter_Layout(const RECT& rcArea) : m_area(rcArea)
		{
		}
	public:
		RECT        GetTabsArea(void)const
		{
			RECT rcTabs = m_area;
			::InflateRect(&rcTabs, -5, -3);
			return rcTabs;
		}
	};

	class CTabSetFilter_Registry
	{
	private:
		mutable
		CRegistryStorage m_stg;
	public:
		CTabSetFilter_Registry(void) : m_stg(HKEY_CURRENT_USER)
		{
		}
	public:
		INT     LastIndex(void)const
		{
			LONG lIndex = 0;
			m_stg.Load(
					this->_CommonRegFolder(),
					this->_LastIndexRegName(),
					lIndex
				);
			if (lIndex < 0)
				lIndex = 0;
			return static_cast<INT>(lIndex);
		}

		HRESULT LastIndex(const INT nIndex)
		{
			HRESULT hr_ = m_stg.Save(
					this->_CommonRegFolder(),
					this->_LastIndexRegName(),
					static_cast<LONG>(nIndex)
				);
			return  hr_;
		}
	private:
		LPCTSTR     _CommonRegFolder(void) const
		{
			static CAtlString cs_folder(_T("DataFilter"));
			return cs_folder;
		}

		LPCTSTR     _LastIndexRegName(void) const
		{
			static CAtlString cs_name(_T("LastTabIndex"));
			return cs_name;
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabSetFilter::CTabSetFilter(CFilter& _flt) : m_crt(m_cTabCtrl, _flt), m_general(m_crt), m_advanced(m_crt)
{
}

CTabSetFilter::~CTabSetFilter(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CTabSetFilter::Create(const HWND hParent, const RECT& rcArea)
{
	if(!::IsWindow(hParent) || ::IsRectEmpty(&rcArea))
		return E_INVALIDARG;

	details::CTabSetFilter_Layout layout(rcArea);
	RECT rcTabs = layout.GetTabsArea();

	m_cTabCtrl.Create(
			hParent,
			rcTabs,
			0,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			WS_EX_CONTROLPARENT,
			IDC_FWDESKTOP_MAIN_DLG_TABSET
		);
	m_cTabCtrl.SetFont(::ATL::CWindow(hParent).GetFont());

	INT nIndex = 0;
	const DWORD dwFlags = SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW;

	m_cTabCtrl.AddItem(m_general.GetPageTitle());
	{
		m_general.Create(m_cTabCtrl.m_hWnd);
		m_general.SetWindowPos(
				HWND_TOP,
				0, 0, 0, 0,
				dwFlags
			);
		if (m_general.IsWindow())m_general.Index(nIndex++);
	}

	m_cTabCtrl.AddItem(m_advanced.GetPageTitle());
	{
		m_advanced.Create(m_cTabCtrl.m_hWnd);
		m_advanced.SetWindowPos(
				HWND_TOP,
				0, 0, 0, 0,
				dwFlags
			);
		if (m_advanced.IsWindow())m_advanced.Index(nIndex++);
	}

	details::CTabSetFilter_Registry reg_;
	nIndex = reg_.LastIndex();
	
	m_cTabCtrl.SetCurSel(nIndex);
	this->UpdateLayout();

	return S_OK;
}

HRESULT       CTabSetFilter::Destroy(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();

	details::CTabSetFilter_Registry reg_;
	reg_.LastIndex(nTabIndex);

	if (m_advanced.IsWindow())
	{
		m_advanced.DestroyWindow();
		m_advanced.m_hWnd = NULL;
	}

	if (m_general.IsWindow())
	{
		m_general.DestroyWindow();
		m_general.m_hWnd = NULL;
	}

	return S_OK;
}

void          CTabSetFilter::UpdateLayout(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();

	if (m_general.IsWindow())   m_general.ShowWindow  (nTabIndex == m_general.Index  () ? SW_SHOW : SW_HIDE);
	if (m_advanced.IsWindow())  m_advanced.ShowWindow (nTabIndex == m_advanced.Index () ? SW_SHOW : SW_HIDE);
}