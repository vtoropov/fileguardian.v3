/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Jan-2016 at 1:14:21p, GMT+7, Phuket, Rawai, Sunday;
	This is File Watcher Data Filter General Tab Page class implementation file.
*/
#include "StdAfx.h"
#include "FW_filter_PageGen.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI;
using namespace fw::desktop::UI::components;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui;
using namespace ex_ui::draw::common;

#include "FG_SqlData_Model_ex.h"

using namespace fg::common::data;
using namespace fg::common::data::local;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components { namespace details
{
	class CTabPageFilterGeneral_Layout
	{
	private:
		RECT           m_client_area;
		const CWindow& m_page_ref;
	public:
		CTabPageFilterGeneral_Layout(const CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		VOID    AdjustCtrls(void)
		{
			HBITMAP hBitmap = NULL;
			HRESULT hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_MAIN_DLG_INFO32,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_GEN_INFO);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}

			hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_FILTER_GEN_SQL3,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_GEN_SQL3);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}
		}
	};

	class CTabPageFilterGeneral_Init
	{
	private:
		const CWindow& m_page_ref;
	public:
		CTabPageFilterGeneral_Init(const CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		VOID     ApplyFilter(const CFilter& _flt)
		{
			bool bEnabled  = true;
			if ( bEnabled) bEnabled = _flt.TimeSpan().Enabled();
			if ( bEnabled) bEnabled = _flt.TimeSpan().IsValid();
			::WTL::CButton time_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_GEN_RNGE);
			if (time_)
				time_.SetCheck((INT)bEnabled);
			{
				const UINT ctrlId[] = {
					IDC_FWDESKTOP_FILTER_GEN_FROM,
					IDC_FWDESKTOP_FILTER_GEN_UPTO
				};
				for (INT i_ = 0; i_ < _countof(ctrlId); i_++)
				{
					::WTL::CDateTimePickerCtrl ctrl_ = m_page_ref.GetDlgItem(ctrlId[i_]);
					if (!ctrl_)
						continue;
					switch(i_)
					{
					case 0: { SYSTEMTIME st_ = _flt.TimeSpan().FromAsSysTime(); ctrl_.SetSystemTime(GDT_VALID, &st_); } break;
					case 1: { SYSTEMTIME st_ = _flt.TimeSpan().UptoAsSysTime(); ctrl_.SetSystemTime(GDT_VALID, &st_); } break;
					}
					ctrl_.EnableWindow(bEnabled);
				}
			}

			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_GEN_ACT);
			if (cbo_)
			{
				const DWORD_PTR data_ = static_cast<DWORD_PTR>(_flt.Action());
				const INT count_ = cbo_.GetCount();
				for ( INT i_ = 0; i_ < count_; i_++)
					if (data_ == cbo_.GetItemData(i_))
						cbo_.SetCurSel(i_);
			}
			CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_GEN_PATT);
			if (ctrl_)
				ctrl_.SetWindowText(_flt.Target());
		}

		VOID     InitCtrls(void)
		{
			const UINT ctrlId[] = {
					IDC_FWDESKTOP_FILTER_GEN_FROM,
					IDC_FWDESKTOP_FILTER_GEN_UPTO
			};
			for (INT i_ = 0; i_ < _countof(ctrlId); i_++)
			{
				::WTL::CDateTimePickerCtrl ctrl_ = m_page_ref.GetDlgItem(ctrlId[i_]);
				if (!ctrl_)
					continue;
				if (ctrl_)
					ctrl_.SetFormat(
						_T("dd-MMM-yyyy hh:mm:ss")
						);
			}
			CSystemEventEnum enum_;
			{
				::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_GEN_ACT);
				if (cbo_)
				{
					cbo_.AddString(_T("All actions"));
					const INT count_ = enum_.Count();
					for ( INT i_ = 0; i_ < count_; i_++ )
					{
						const CSystemEvent& evt_ = enum_.Item(i_);
						if (!evt_._valid)
							break;
						cbo_.AddString(evt_._name);
						cbo_.SetItemData(
								cbo_.GetCount() - 1,
								(DWORD_PTR)(DWORD)evt_._id
							);
					}
					cbo_.SetCurSel(0);
				}
			}
			{
				::WTL::CEdit edt_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_GEN_PATT);
				if (edt_)
					edt_.SetCueBannerText(_T("Include all folders (*.*)"), TRUE);
			}
		}

		VOID     UpdateCtrls(void)
		{
			bool bEnabled  = true;
			::WTL::CButton time_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_GEN_RNGE);
			if (time_)
				bEnabled = !!time_.GetCheck();
			const UINT ctrlId[] = {
					IDC_FWDESKTOP_FILTER_GEN_FROM,
					IDC_FWDESKTOP_FILTER_GEN_UPTO
				};
			for (INT i_ = 0; i_ < _countof(ctrlId); i_++)
			{
				::WTL::CDateTimePickerCtrl ctrl_ = m_page_ref.GetDlgItem(ctrlId[i_]);
				if (ctrl_)
					ctrl_.EnableWindow(bEnabled);
			}
		}

		VOID     UpdateFilter(CFilter& _flt)
		{
			bool bEnabled  = false;
			::WTL::CButton time_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_GEN_RNGE);
			if (time_)
				bEnabled = !!time_.GetCheck();
			_flt.TimeSpan().Enabled(bEnabled);

			const UINT ctrlId[] = {
					IDC_FWDESKTOP_FILTER_GEN_FROM,
					IDC_FWDESKTOP_FILTER_GEN_UPTO
				};
			for (INT i_ = 0; i_ < _countof(ctrlId); i_++)
			{
				::WTL::CDateTimePickerCtrl ctrl_ = m_page_ref.GetDlgItem(ctrlId[i_]);
				if (!ctrl_)
					continue;
				SYSTEMTIME st_ = {0};
				if (GDT_VALID != ctrl_.GetSystemTime(&st_))
					continue;

				switch(i_)
				{
				case 0: _flt.TimeSpan().From() = st_; break;
				case 1: _flt.TimeSpan().Upto() = st_; break;
				}
				ctrl_.EnableWindow(bEnabled);
			}

			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_GEN_ACT);
			if (cbo_)
			{
				const INT action_ = static_cast<INT>(cbo_.GetItemData(cbo_.GetCurSel()));
				_flt.Action(action_);
			}

			CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_GEN_PATT);
			if (ctrl_)
			{
				CAtlString cs_pattern;
				ctrl_.GetWindowText(cs_pattern);
				_flt.Target(cs_pattern);
			}
		}
	};

	class CTabPageFilterGeneral_Handler
	{
	private:
		const CWindow& m_page_ref;
	public:
		CTabPageFilterGeneral_Handler(const CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		BOOL     OnCommand(const WORD wCtrlId, const WORD wNotifyCode)
		{
			BOOL bHandled = TRUE; wNotifyCode;

			if (false){}
			else if (IDC_FWDESKTOP_FILTER_GEN_RNGE == wCtrlId)
			{
				CTabPageFilterGeneral_Init init_(m_page_ref);
				init_.UpdateCtrls();
			}
			else
				bHandled = FALSE;

			return  bHandled;
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageFilterGeneral::CTabPageFilterGeneral(CTabPageFilterBaseCrt& _crt):
	TBasePage(IDD_FWDESKTOP_FILTER_GEN_PAGE, _crt.TabControl(), *this),
	TFilterBase(_crt)
{
}

CTabPageFilterGeneral::~CTabPageFilterGeneral(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageFilterGeneral::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			details::CTabPageFilterGeneral_Layout layout_(*this);
			layout_.AdjustCtrls();

			details::CTabPageFilterGeneral_Init init_(*this);
			init_.InitCtrls();
			init_.ApplyFilter(m_crt.Filter());

			TBasePage::m_bInitilized = true;
			bHandled = TRUE;
		} break;
	case WM_DESTROY:
		{
			details::CTabPageFilterGeneral_Init init_(*this);
			init_.UpdateFilter(m_crt.Filter());
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			details::CTabPageFilterGeneral_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString CTabPageFilterGeneral::GetPageTitle(void) const
{
	CAtlString cs_title(_T("General"));
	return cs_title;
}