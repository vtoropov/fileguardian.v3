#ifndef _FWFILTERPAGEBASE_H_5EBCCDCC_6534_4643_8426_6445A6679F53_INCLUDED
#define _FWFILTERPAGEBASE_H_5EBCCDCC_6534_4643_8426_6445A6679F53_INCLUDED
/*
	Created by Tech_dog (VToropov) on 21-Jan-2016 at 10:03:10pm, GMT+7, Phuket, Rawai, Thursday;
	This is File Watcher Data Filter Settings Tab Page Base class declaration file.
*/
#include "FW_desktop_UI_Defs.h"
#include "FG_SysEvent_Filter.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	using fg::common::data::CFilter;

	class CTabPageFilterBaseCrt
	{
	private:
		::WTL::CTabCtrl&    m_tab_ctrl;
		CFilter&            m_flt_ref;
	public:
		CTabPageFilterBaseCrt(::WTL::CTabCtrl&, CFilter&);
	public:
		CFilter&            Filter(void)const;
		::WTL::CTabCtrl&    TabControl(void)const;
	};

	class CTabPageFilterBase
	{
	protected:
		CTabPageFilterBaseCrt& m_crt;
	protected:
		CTabPageFilterBase(CTabPageFilterBaseCrt&);
	};
}}}}


#endif/*_FWFILTERPAGEBASE_H_5EBCCDCC_6534_4643_8426_6445A6679F53_INCLUDED*/