#ifndef _FWDATAFILTERDLGTABSET_H_CBE4D90A_5BEA_4324_B7CA_C92D7BEB1FC7_INCLUDED
#define _FWDATAFILTERDLGTABSET_H_CBE4D90A_5BEA_4324_B7CA_C92D7BEB1FC7_INCLUDED
/*
	Created by Tech_dog (VToropov) on 16-Jan-2016 at 3:42:18pm, GMT+7, Phuket, Rawai, Saturday;
	This is File Watcher Desktop Data Filter Dailog Tab Set class declaration file.
*/
#include "FW_filter_PageGen.h"
#include "FW_filter_PageAdv.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	class CTabSetFilter
	{
	private:
		CTabPageFilterBaseCrt  m_crt;
		::WTL::CTabCtrl        m_cTabCtrl;
	private: //tab pages
		CTabPageFilterGeneral  m_general;
		CTabPageFilterAdvanced m_advanced;
	public:
		CTabSetFilter(CFilter&);
		~CTabSetFilter(void);
	public:
		HRESULT           Create(const HWND hParent, const RECT& rcArea);
		HRESULT           Destroy(void);
		VOID              UpdateLayout(void);
	};
}}}}

#endif/*_FWDATAFILTERDLGTABSET_H_CBE4D90A_5BEA_4324_B7CA_C92D7BEB1FC7_INCLUDED*/