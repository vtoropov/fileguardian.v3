/*
	Created by Tech_dog (VToropov) on 21-Jan-2016 at 10:14:20pm, GMT+7, Phuket, Rawai, Thursday;
	This is File Watcher Data Filter Settings Tab Page Base class implementation file.
*/
#include "StdAfx.h"
#include "FW_filter_PageBase.h"

using namespace fw::desktop::UI::components;

/////////////////////////////////////////////////////////////////////////////

CTabPageFilterBaseCrt::CTabPageFilterBaseCrt(::WTL::CTabCtrl& _tab, CFilter& _flt) : m_tab_ctrl(_tab), m_flt_ref(_flt)
{
}

/////////////////////////////////////////////////////////////////////////////

CFilter&            CTabPageFilterBaseCrt::Filter(void)const
{
	return m_flt_ref;
}

::WTL::CTabCtrl&    CTabPageFilterBaseCrt::TabControl(void)const
{
	return m_tab_ctrl;
}

/////////////////////////////////////////////////////////////////////////////

CTabPageFilterBase::CTabPageFilterBase(CTabPageFilterBaseCrt& _crt) : m_crt(_crt)
{
}