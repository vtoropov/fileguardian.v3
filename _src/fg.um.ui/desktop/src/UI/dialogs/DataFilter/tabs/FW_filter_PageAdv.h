#ifndef _FWFILTERPAGEADV_H_71A25A98_CF9F_45ad_935E_B7C9FF3E8F59_INCLUDED
#define _FWFILTERPAGEADV_H_71A25A98_CF9F_45ad_935E_B7C9FF3E8F59_INCLUDED
/*
	Created by Tech_dog (VToropov) on 17-Jan-2016 at 1:34:32p, GMT+7, Phuket, Rawai, Sunday;
	This is File Watcher Data Filter Advanced Tab Page class declaration file.
*/
#include "FW_filter_PageBase.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	class CTabPageFilterAdvanced:
		public  CTabPageBase, 
		public  ITabPageCallback,
		private CTabPageFilterBase
	{
		typedef CTabPageBase       TBasePage;
		typedef CTabPageFilterBase TFilterBase;
	public:
		CTabPageFilterAdvanced(CTabPageFilterBaseCrt&);
		~CTabPageFilterAdvanced(void);
	private: // ITabPageCallback
		virtual LRESULT        TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
	public:  // TBasePage
		virtual CAtlString     GetPageTitle(void) const override sealed;
	};
}}}}

#endif/*_FWFILTERPAGEADV_H_71A25A98_CF9F_45ad_935E_B7C9FF3E8F59_INCLUDED*/