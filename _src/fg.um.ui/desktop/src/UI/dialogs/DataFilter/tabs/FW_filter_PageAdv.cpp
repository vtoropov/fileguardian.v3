/*
	Created by Tech_dog (VToropov) on 17-Jan-2016 at 1:37:20p, GMT+7, Phuket, Rawai, Sunday;
	This is File Watcher Data Filter Advanced Tab Page class implementation file.
*/
#include "StdAfx.h"
#include "FW_filter_PageAdv.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI;
using namespace fw::desktop::UI::components;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui;
using namespace ex_ui::draw::common;

#include "FG_SqlData_Model_ex.h"
#include "FG_Service_Settings.h"

using namespace fg::common::data;
using namespace fg::common::data::local;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components { namespace details
{
	class CTabPageFilterAdvanced_Layout
	{
	private:
		RECT           m_client_area;
		const CWindow& m_page_ref;
	public:
		CTabPageFilterAdvanced_Layout(const CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		VOID    AdjustCtrls(void)
		{
			HBITMAP hBitmap = NULL;
			HRESULT hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_MAIN_DLG_INFO32,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_ADV_INFO);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}
			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_ADV_FLDR);
			if (cbo_)
			{
				TFolderList folders_ = GetServiceSettingsRef().IncludedFolders();

				for (size_t i_ = 0; i_ < folders_.size(); i_++)
					cbo_.AddString(folders_[i_]);

				cbo_.SetCueBannerText(_T("[Not selected]"));
				cbo_.SetCurSel(CB_ERR);
			}
		}
	};

	class CTabPageFilterAdvanced_Init
	{
	private:
		CWindow& m_page_ref;
	public:
		CTabPageFilterAdvanced_Init(CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		VOID     ApplyFilter(const CFilter& _flt)
		{
			const DWORD limit_ = _flt.Extension().RowLimit();
			if (limit_)
				m_page_ref.SetDlgItemInt(
						IDC_FWDESKTOP_FILTER_ADV_NUMB,
						static_cast<UINT>(limit_),
						FALSE
					);
			::WTL::CButton recent_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_ADV_RCNT);
			if (recent_)
				recent_.SetCheck((INT)_flt.Extension().Reversed());

			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_ADV_FLDR);
			if (cbo_)
				cbo_.SetCurSel(_flt.Watched());

			::WTL::CButton unrep_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_ADV_RPRT);
			if (unrep_)
				unrep_.SetCheck(CRecord::eStatusUnreported == _flt.Status());
		}

		VOID     InitCtrls(void)
		{
			::WTL::CEdit edt_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_ADV_NUMB);
			if (edt_)
				edt_.SetCueBannerText(_T("No limit"), TRUE);
		}

		VOID     UpdateFilter(CFilter& _flt)
		{
			::WTL::CEdit limit_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_ADV_NUMB);
			if (limit_)
			{
				BOOL bTransition = FALSE;
				const INT val_ = m_page_ref.GetDlgItemInt(IDC_FWDESKTOP_FILTER_ADV_NUMB, &bTransition, FALSE);
				if ( -1 < val_)
					_flt.Extension().RowLimit(val_);
			}
			::WTL::CButton recent_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_ADV_RCNT);
			if (recent_)
				_flt.Extension().Reversed(!!recent_.GetCheck());
			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_ADV_FLDR);
			if (cbo_)
				_flt.Watched(cbo_.GetCurSel());

			::WTL::CButton unrep_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_ADV_RPRT);
			if (unrep_)
				_flt.Status(unrep_.GetCheck() ? CRecord::eStatusUnreported : CRecord::eStatusNone);
		}
	};

	class CTabPageFilterAdvanced_Handler
	{
	private:
		const CWindow& m_page_ref;
	public:
		CTabPageFilterAdvanced_Handler(const CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		BOOL     OnCommand(const WORD wCtrlId, const WORD wNotifyCode)
		{
			BOOL bHandled = TRUE; wNotifyCode;

			if (false){}
			else if (IDC_FWDESKTOP_FILTER_ADV_CLR == wCtrlId)
			{
				::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_FILTER_ADV_FLDR);
				if (cbo_)
					cbo_.SetCurSel(CB_ERR);
			}
			else
				bHandled = FALSE;

			return  bHandled;
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageFilterAdvanced::CTabPageFilterAdvanced(CTabPageFilterBaseCrt& _crt):
	TBasePage(IDD_FWDESKTOP_FILTER_ADV_PAGE, _crt.TabControl(), *this),
	TFilterBase(_crt)
{
}

CTabPageFilterAdvanced::~CTabPageFilterAdvanced(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageFilterAdvanced::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			details::CTabPageFilterAdvanced_Layout layout_(*this);

			layout_.AdjustCtrls();

			details::CTabPageFilterAdvanced_Init init_(*this);
			init_.InitCtrls();
			init_.ApplyFilter(m_crt.Filter());

			TBasePage::m_bInitilized = true;
			bHandled = TRUE;
		} break;
	case WM_DESTROY:
		{
			details::CTabPageFilterAdvanced_Init init_(*this);
			init_.UpdateFilter(m_crt.Filter());
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			details::CTabPageFilterAdvanced_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString CTabPageFilterAdvanced::GetPageTitle(void) const
{
	CAtlString cs_title(_T("Advanced"));
	return cs_title;
}