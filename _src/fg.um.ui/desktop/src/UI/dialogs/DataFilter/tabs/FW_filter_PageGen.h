#ifndef _FWFILTERPAGEGEN_H_7EBDDA71_F00B_4643_9EA5_62F0072A730C_INCLUDED
#define _FWFILTERPAGEGEN_H_7EBDDA71_F00B_4643_9EA5_62F0072A730C_INCLUDED
/*
	Created by Tech_dog (VToropov) on 17-Jan-2016 at 12:58:33p, GMT+7, Phuket, Rawai, Sunday;
	This is File Watcher Data Filter General Tab Page class declaration file.
*/
#include "FW_filter_PageBase.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	class CTabPageFilterGeneral:
		public  CTabPageBase, 
		public  ITabPageCallback,
		private CTabPageFilterBase
	{
		typedef CTabPageBase       TBasePage;
		typedef CTabPageFilterBase TFilterBase;
	public:
		CTabPageFilterGeneral(CTabPageFilterBaseCrt&);
		~CTabPageFilterGeneral(void);
	private: // ITabPageCallback
		virtual LRESULT        TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
	public:  // TBasePage
		virtual CAtlString     GetPageTitle(void) const override sealed;
	};
}}}}

#endif/*_FWFILTERPAGEGEN_H_7EBDDA71_F00B_4643_9EA5_62F0072A730C_INCLUDED*/