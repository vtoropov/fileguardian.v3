#ifndef _FWEXPORTDLG_H_D44B1C3A_06C4_4c81_8C3B_609183813A9C_INCLUDED
#define _FWEXPORTDLG_H_D44B1C3A_06C4_4c81_8C3B_609183813A9C_INCLUDED
/*
	Created by Tech_dog (VToropov) on 18-May-2016 at 9:37:50pm, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian desktop data export process dialog class declaration file.
*/
#include "Shared_SystemError.h"
#include "FW_csv_Provider.h"
#include "FW_action_ToExportData.h"

namespace fw { namespace desktop { namespace UI { namespace dialogs
{
	using shared::lite::common::CSysError;
	using shared::lite::sys_core::CThreadBase;

	using fw::desktop::data::ICsvDataProviderCallback;
	using fw::desktop::ctrl_flow::CActionToExportData;

	class CDataExportDlg
	{
	private:
		class CDataExportDlgImpl :
			public  ::ATL::CDialogImpl<CDataExportDlgImpl>,
			public  ICsvDataProviderCallback
		{
			typedef ::ATL::CDialogImpl<CDataExportDlgImpl> TBaseDlg;
		private:
			CActionToExportData      m_action;
			WTL::CProgressBarCtrl    m_progress;
			CWindow                  m_label;
			CWindow                  m_source; // list view
			CAtlString               m_target; // destination file full path
			INT_PTR                  m_timer;
			bool                     m_interrupted;
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CDataExportDlgImpl)
				MESSAGE_HANDLER     (WM_DESTROY   ,   OnDestroy   )
				MESSAGE_HANDLER     (WM_INITDIALOG,   OnInitDialog)
				MESSAGE_HANDLER     (WM_SYSCOMMAND,   OnSysCommand)
				MESSAGE_HANDLER     (WM_TIMER     ,   OnTimerEvent)
				COMMAND_ID_HANDLER  (IDCANCEL     ,   OnDismiss   )
			END_MSG_MAP()
		public:
			CDataExportDlgImpl(HWND _source, const CAtlString _target);
			~CDataExportDlgImpl(void);
		private:
			LRESULT OnDestroy   (UINT uMsg       , WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnDismiss   (WORD wNotifyCode, WORD wID     , HWND hWndCtl , BOOL& bHandled);
			LRESULT OnInitDialog(UINT uMsg       , WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnSysCommand(UINT uMsg       , WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnTimerEvent(UINT uMsg       , WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		private: // ICsvDataProviderCallback
			virtual bool    CsvDataProvider_CanContinue(void) override sealed;
			virtual VOID    CsvDataProvider_OnComplete(void) override sealed;
			virtual VOID    CsvDataProvider_OnError(const CSysError) override sealed;
			virtual VOID    CsvDataProvider_OnProgress(const INT _ndx, const INT _total) override sealed;
			virtual VOID    CsvDataProvider_OnStart(const INT _total) override sealed;
			virtual HWND    CsvDataProvider_RequestSourceCtrl(void) override sealed;
			virtual CAtlString
			                CsvDataProvider_RequestTargetPath(void) override sealed;
		};
	private:
		CDataExportDlgImpl   m_dlg;
	public:
		CDataExportDlg(HWND _source, const CAtlString _target);
		~CDataExportDlg(void);
	public:
		HRESULT    DoModal(void);
	private:
		CDataExportDlg(const CDataExportDlg&);
		CDataExportDlg& operator= (const CDataExportDlg&);
	};
}}}}

#endif/*_FWEXPORTDLG_H_D44B1C3A_06C4_4c81_8C3B_609183813A9C_INCLUDED*/