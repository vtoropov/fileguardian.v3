/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jun-2018 at 6:31:12p, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian desktop application file event data properties dialog interface implementation file.
*/
#include "StdAfx.h"
#include "FG_Prop_Dlg.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI::components;
using namespace fw::desktop::UI::dialogs;

#include "Shared_GenericAppResource.h"

using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace dialogs { namespace details
{
	class CDataPropDlg_Layout
	{
	public:
		enum _e{
			logout_h = 165,
		};
	private:
		CWindow&      m_dlg_ref;
		RECT          m_client_area;
	public:
		CDataPropDlg_Layout(CWindow& dlg_ref) : m_dlg_ref(dlg_ref)
		{
			if (!m_dlg_ref.GetClientRect(&m_client_area))
			{
				::SetRectEmpty(&m_client_area);
			}
		}
	public:
		RECT          RecalcTabArea(const SIZE& szHeader)
		{
			RECT rcTab = m_client_area;
			rcTab.top += szHeader.cy;
			::ATL::CWindow btn = m_dlg_ref.GetDlgItem(IDCANCEL);
			if (btn)
			{
				RECT rc_btn = {0};
				if (btn.GetWindowRect(&rc_btn))
				{
					::MapWindowPoints(HWND_DESKTOP, m_dlg_ref, (LPPOINT)&rc_btn, 0x2);
					rcTab.bottom = rc_btn.top - 5;
				}
			}
			return rcTab;
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CDataPropDlg::CDataPropDlgImpl::CDataPropDlgImpl(const CLvwRecord& _rec, CDbProvider& _prov): 
	IDD(IDD_FGDESKTOP_PROP_DLG),
	m_header(IDR_FWDESKTOP_PROP_DLG_HEADER), m_tabset(_rec, _prov) {}

CDataPropDlg::CDataPropDlgImpl::~CDataPropDlgImpl(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT CDataPropDlg::CDataPropDlgImpl::OnButtonDown(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	wNotifyCode; wID; hWndCtl; bHandled = TRUE;
	TBaseDlg::EndDialog(wID);
	return 0;
}

LRESULT CDataPropDlg::CDataPropDlgImpl::OnDestroy   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;
	m_tabset.Destroy();
	m_header.Destroy();
	return 0;
}

LRESULT CDataPropDlg::CDataPropDlgImpl::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;

	HRESULT hr_ = m_header.Create(TBaseDlg::m_hWnd);
	if (!FAILED(hr_))
	{
	}

	details::CDataPropDlg_Layout layout_(*this);

	const RECT rcTabs = layout_.RecalcTabArea(m_header.GetSize());
	hr_ = m_tabset.Create(*this, rcTabs);

	TBaseDlg::CenterWindow();
	{
		CApplicationIconLoader loader_(IDR_FWDESKTOP_MAIN_DLG_ICON);
		TBaseDlg::SetIcon(loader_.DetachLargeIcon(), TRUE);
		TBaseDlg::SetIcon(loader_.DetachSmallIcon(), FALSE);
	}
	return 0;
}

LRESULT CDataPropDlg::CDataPropDlgImpl::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			TBaseDlg::EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

LRESULT CDataPropDlg::CDataPropDlgImpl::OnTabNotify (INT idCtrl, LPNMHDR pnmh, BOOL& bHandled)
{
	idCtrl; pnmh; bHandled = TRUE;
	m_tabset.UpdateLayout();
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CDataPropDlg::CDataPropDlg(const CLvwRecord& _rec, CDbProvider& _prov) : m_dlg(_rec, _prov) {}
CDataPropDlg::~CDataPropDlg(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CDataPropDlg::DoModal(void)
{
	const INT_PTR result_ = m_dlg.DoModal();
	switch (result_)
	{
	case IDCANCEL:  return S_FALSE;
	default      :  return S_FALSE;
	}
}