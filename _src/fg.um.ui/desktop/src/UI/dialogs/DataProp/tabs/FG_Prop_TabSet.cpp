/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jun-2018 at 6:17:46pm, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian Desktop Event Property Dailog Tab Set class implementation file.
*/
#include "StdAfx.h"
#include "FG_Prop_TabSet.h"

#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI;
using namespace fw::desktop::UI::components;

#include "Shared_Registry.h"

using namespace shared::registry;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components { namespace details
{
	class CTabSetOpts_Layout
	{
	private:
		RECT        m_area;
	public:
		CTabSetOpts_Layout(const RECT& rcArea) : m_area(rcArea) { }
	public:
		RECT        GetTabsArea(void)const
		{
			RECT rcTabs = m_area;
			::InflateRect(&rcTabs, -5, -3);
			return rcTabs;
		}
	};

}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabSetProp::CTabSetProp(const CLvwRecord& _rec, CDbProvider& _prov) : m_general(m_cTabCtrl, _rec, _prov) { }
CTabSetProp::~CTabSetProp(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CTabSetProp::Create(const HWND hParent, const RECT& rcArea)
{
	if(!::IsWindow(hParent) || ::IsRectEmpty(&rcArea))
		return E_INVALIDARG;

	details::CTabSetOpts_Layout layout(rcArea);
	RECT rcTabs = layout.GetTabsArea();

	m_cTabCtrl.Create(
			hParent,
			rcTabs,
			0,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			WS_EX_CONTROLPARENT,
			IDC_FWDESKTOP_OPTS_DLG_TABSET
		);
	m_cTabCtrl.SetFont(::ATL::CWindow(hParent).GetFont());

	INT nIndex = 0;
	const DWORD dwFlags = SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW;

	m_cTabCtrl.AddItem(m_general.GetPageTitle());
	{
		m_general.Create(m_cTabCtrl.m_hWnd);
		m_general.SetWindowPos(
				HWND_TOP,
				0, 0, 0, 0,
				dwFlags
			);
		if (m_general.IsWindow())m_general.Index(nIndex++);
	}

	m_cTabCtrl.SetCurSel(0);
	this->UpdateLayout();

	return S_OK;
}

HRESULT       CTabSetProp::Destroy(void)
{
	if (m_general.IsWindow())
	{
		m_general.DestroyWindow();
		m_general.m_hWnd = NULL;
	}

	return S_OK;
}

void          CTabSetProp::UpdateLayout(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();

	if (m_general.IsWindow())   m_general.ShowWindow  (nTabIndex == m_general.Index  () ? SW_SHOW : SW_HIDE);
}