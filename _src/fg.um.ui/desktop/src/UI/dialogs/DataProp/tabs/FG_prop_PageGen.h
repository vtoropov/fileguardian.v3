#ifndef _FGPROPPAGEGEN_H_68133F72_D23C_4f88_AE4A_59BFB457600F_INCLUDED
#define _FGPROPPAGEGEN_H_68133F72_D23C_4f88_AE4A_59BFB457600F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jun-2018 at 6:04:19p, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian desktop application event data properties dialog page interface declaration file.
*/
#include "FW_desktop_UI_Defs.h"
#include "FG_SqlData_Provider.h"
#include "FG_lvw_RecordIface.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	using fg::common::data::local::CDbProvider;
	using fw::desktop::data::CLvwRecord;

	class CTabPagePropGen : public CTabPageBase, public  ITabPageCallback {

		typedef CTabPageBase   TBasePage;
	private:
		WTL::CTabCtrl&         m_tabs;
		CDbProvider&           m_provider;
		const CLvwRecord&      m_evt_rec;          
	public:
		CTabPagePropGen(WTL::CTabCtrl&, const CLvwRecord&, CDbProvider&);
		~CTabPagePropGen(void);
	private: // ITabPageCallback
		virtual LRESULT        TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
	public:  // TBasePage
		virtual CAtlString     GetPageTitle(void) const override sealed;
	};

}}}}


#endif/*_FGPROPPAGEGEN_H_68133F72_D23C_4f88_AE4A_59BFB457600F_INCLUDED*/