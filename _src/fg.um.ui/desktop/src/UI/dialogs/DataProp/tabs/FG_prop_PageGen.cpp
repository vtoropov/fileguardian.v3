/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jun-2018 at 6:08:24p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian desktop application event data properties dialog page interface implementation file.
*/
#include "StdAfx.h"
#include "FG_prop_PageGen.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI::components;

#include "FG_SysEvent_Model.h"
#include "FG_SqlData_Qry.h"

using namespace fg::common::data;
using namespace fg::common::data::local;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui;
using namespace ex_ui::draw::common;

#include "Shared_DateTime.h"

using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components { namespace details
{
	class CTabPageGen_Layout
	{
	private:
		RECT           m_client_area;
		const CWindow& m_page_ref;
	public:
		CTabPageGen_Layout(const CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		VOID    AdjustCtrls(void) {}
	};

	class CTabPageGen_Init
	{
	private:
		CWindow&       m_page_ref;
		CDbProvider&   m_provider;

	public:
		CTabPageGen_Init(CWindow& dlg_ref, CDbProvider& _prov) : m_page_ref(dlg_ref),  m_provider(_prov){}
	public:
		VOID  InitCtrls(void) {
		}
		VOID  UpdateCtrls(const CLvwRecord& _rec) {

			CRecord rec_;

			CEdit ctl_tms = m_page_ref.GetDlgItem(IDC_FGDESKTOP_PROP_GEN_TIME);
			CEdit ctl_src = m_page_ref.GetDlgItem(IDC_FGDESKTOP_PROP_GEN_SOURCE);
			CEdit ctl_tgt = m_page_ref.GetDlgItem(IDC_FGDESKTOP_PROP_GEN_TARGET);
			CEdit ctl_act = m_page_ref.GetDlgItem(IDC_FGDESKTOP_PROP_GEN_ACTION);
			CEdit ctl_obj = m_page_ref.GetDlgItem(IDC_FGDESKTOP_PROP_GEN_OBJECT);
			CEdit ctl_sts = m_page_ref.GetDlgItem(IDC_FGDESKTOP_PROP_GEN_STATUS);
			CEdit ctl_pro = m_page_ref.GetDlgItem(IDC_FGDESKTOP_PROP_GEN_PROC);

			if (ctl_tms) ctl_tms.SetWindowText(_T("#n/a"));
			if (ctl_src) ctl_src.SetWindowText(_T("#n/a"));
			if (ctl_tgt) ctl_tgt.SetWindowText(_T("#n/a"));
			if (ctl_act) ctl_act.SetWindowText(_T("#n/a"));
			if (ctl_obj) ctl_obj.SetWindowText(_T("#n/a"));
			if (ctl_sts) ctl_sts.SetWindowText(_T("#n/a"));
			if (ctl_pro) ctl_pro.SetWindowText(_T("#n/a"));

			HRESULT hr_  = this->UpdateData(_rec, rec_);

			if (SUCCEEDED(hr_)) {
				if (ctl_tms) {
					CSystemTime sys_tm(_rec.TimestampRaw());
					CAtlString cs_time = sys_tm.DefaultFormatValue();
					if (cs_time.IsEmpty() == false)
						ctl_tms.SetWindowText(cs_time.GetString());
				}
				if (ctl_src) {
					CAtlString cs_src = rec_.Source();
					if (cs_src.IsEmpty() == false)
						ctl_src.SetWindowText(cs_src.GetString());
				}
				if (ctl_tgt) {
					CAtlString cs_tgt = rec_.Target();
					if (cs_tgt.IsEmpty() == false)
						ctl_tgt.SetWindowText(cs_tgt.GetString());
				}
				if (ctl_act) {
					CAtlString cs_act = rec_.ActionAsText();
					if (cs_act.IsEmpty() == false)
						ctl_act.SetWindowText(cs_act.GetString());
				}
				if (ctl_obj) {
					CAtlString cs_obj = rec_.TypeAsText();
					if (cs_obj.IsEmpty() == false)
						ctl_obj.SetWindowText(cs_obj.GetString());
				}
				if (ctl_sts) {
					CAtlString cs_sts = rec_.StatusAsText();
					if (cs_sts.IsEmpty() == false) {
						cs_sts = cs_sts.MakeLower();
						ctl_sts.SetWindowText(cs_sts.GetString());
					}
				}
				if (ctl_pro) {
					CAtlString cs_pro= rec_.Process();
					if (cs_pro.IsEmpty() == false)
						ctl_pro.SetWindowText(cs_pro.GetString());
				}
			}
		}
	private:
		HRESULT   UpdateData(const CLvwRecord& _rec, CRecord& _out){
			HRESULT hr_ = S_OK;
			if (0 == _rec.TimestampRaw()){
				return (hr_ = E_INVALIDARG);
			}

			const CDataSchema& default_ = m_provider.Database().Schemata().Default();
			if (default_.IsValid() == false)
				return default_.Error();
			//
			// TODO: field name must be taken by not 'magic' number;
			// expects pre-defined sequense of fields in schema XML;
			//
			CAtlString cs_time   = default_.TableOf(0).Fields().Item(0).Name();
			CAtlString cs_target = default_.TableOf(0).Fields().Item(2).Name();
			CAtlString cs_action = default_.TableOf(0).Fields().Item(3).Name();

			CDbQuery qry_;
			hr_ = qry_.Params().Append(cs_time.GetString(), _rec.TimestampRaw());
			if (FAILED(hr_))
				return hr_;
			hr_ = qry_.Params().Append(cs_target.GetString(), _rec.FilePath());
			if (FAILED(hr_))
				return hr_;

			hr_ = qry_.Params().Append(cs_action.GetString(), static_cast<LONG>(_rec.ActionId()));
			if (FAILED(hr_))
				return hr_;

			TRecords recs_ = m_provider.Reader().Records(qry_);
			if (recs_.empty() == false)
				_out = recs_[0];
			else if (recs_.empty() && !m_provider.Reader().Error())
				hr_ = HRESULT_FROM_WIN32(ERROR_NOT_FOUND);
			else
				hr_ = m_provider.Reader().Error();

			return  hr_;
		}
	};

	class CTabPageGen_Handler
	{
	private:
		CWindow&  m_page_ref;
	public:
		CTabPageGen_Handler(CWindow& dlg_ref) : m_page_ref(dlg_ref){}
	public:
		BOOL  OnCommand(const WORD ctrlId, const WORD wNotify)
		{
			BOOL bHandled = TRUE; wNotify; ctrlId;

			if (false){}
			else
				bHandled = FALSE;

			return bHandled;
		}

	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabPagePropGen::CTabPagePropGen(WTL::CTabCtrl& _tabs, const CLvwRecord& _evt_rec, CDbProvider& _prov):
	TBasePage(IDD_FGDESKTOP_PROP_GEN_PAGE, _tabs, *this), m_tabs(_tabs),  m_evt_rec(_evt_rec), m_provider(_prov)
{
}

CTabPagePropGen::~CTabPagePropGen(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPagePropGen::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;

			details::CTabPageGen_Layout layout_(*this);
			layout_.AdjustCtrls();

			details::CTabPageGen_Init init_(*this, m_provider);
			init_.InitCtrls();
			init_.UpdateCtrls(m_evt_rec);

			TBasePage::m_bInitilized = true;
			bHandled = TRUE;
		} break;
	case WM_DESTROY:
		{
			details::CTabPageGen_Init init_(*this, m_provider);
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			details::CTabPageGen_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
		} break;
	case WM_NOTIFY:
		{
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString CTabPagePropGen::GetPageTitle(void) const
{
	CAtlString cs_title(_T("General"));
	return cs_title;
}