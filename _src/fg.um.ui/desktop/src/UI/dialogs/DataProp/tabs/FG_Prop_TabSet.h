#ifndef _FGPROPTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
#define _FGPROPTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jun-2018 at 6:14:44p, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian desktop application file event data property tab set interface declaration file.
*/
#include "FG_prop_PageGen.h"
#include "FG_SqlData_Provider.h"
#include "FG_lvw_RecordIface.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	using fg::common::data::local::CDbProvider;
	using fw::desktop::data::CLvwRecord;

	class CTabSetProp
	{
	private:
		WTL::CTabCtrl    m_cTabCtrl;
	private: //tab pages
		CTabPagePropGen  m_general;
	public:
		CTabSetProp(const CLvwRecord&, CDbProvider&);
		~CTabSetProp(void);
	public:
		HRESULT       Create(const HWND hParent, const RECT& rcArea);
		HRESULT       Destroy(void);
		VOID          UpdateLayout(void);
	};
}}}}

#endif/*_FGPROPTABSET_H_56C814DB_268C_4145_85FE_A930F00C5ABC_INCLUDED*/