#ifndef _FGPROPDLG_H_576D692C_27DA_4182_B754_D881D519ED3F_INCLUDED
#define _FGPROPDLG_H_576D692C_27DA_4182_B754_D881D519ED3F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Jun-2018 at 6:24:52p, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian desktop application file event data properties dialog interface declaration file.
*/
#include "UIX_ImgHeader.h"
#include "FG_Prop_TabSet.h"
#include "FG_SqlData_Provider.h"
#include "FG_lvw_RecordIface.h"

namespace fw { namespace desktop { namespace UI { namespace dialogs
{
	using ex_ui::frames::CImageHeader;

	using fw::desktop::data::CLvwRecord;
	using fw::desktop::UI::components::CTabSetProp;
	using fg::common::data::local::CDbProvider;

	class CDataPropDlg
	{
	private:
		class CDataPropDlgImpl :
			public  ::ATL::CDialogImpl<CDataPropDlgImpl>
		{
			typedef ::ATL::CDialogImpl<CDataPropDlgImpl> TBaseDlg;
			friend class CDataOptsDlg;
		private:
			CImageHeader       m_header;
			CTabSetProp        m_tabset;
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CDataPropDlgImpl)
				MESSAGE_HANDLER     (WM_DESTROY   ,   OnDestroy   )
				MESSAGE_HANDLER     (WM_INITDIALOG,   OnInitDialog)
				MESSAGE_HANDLER     (WM_SYSCOMMAND,   OnSysCommand)
				COMMAND_ID_HANDLER  (IDOK         ,   OnButtonDown)
				COMMAND_ID_HANDLER  (IDCANCEL     ,   OnButtonDown)
				COMMAND_ID_HANDLER  (IDNO         ,   OnButtonDown)
				NOTIFY_CODE_HANDLER (TCN_SELCHANGE,   OnTabNotify )
			END_MSG_MAP()
		public:
			CDataPropDlgImpl(const CLvwRecord&, CDbProvider&);
			~CDataPropDlgImpl(void);
		private:
			LRESULT OnButtonDown(WORD , WORD   , HWND   , BOOL&);
			LRESULT OnDestroy   (UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnInitDialog(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnSysCommand(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnTabNotify (INT  , LPNMHDR, BOOL&);
		};
	private:
		CDataPropDlgImpl  m_dlg;
	public:
		CDataPropDlg(const CLvwRecord&, CDbProvider&);
		~CDataPropDlg(void);
	public:
		HRESULT    DoModal(void);
	private:
		CDataPropDlg(const CDataPropDlg&);
		CDataPropDlg& operator= (const CDataPropDlg&);
	};

}}}}

#endif/*_FGPROPDLG_H_576D692C_27DA_4182_B754_D881D519ED3F_INCLUDED*/