/*
	Created by Tech_dog (VToropov) on 28-Jun-2016 at 9:53:30am, GMT+7, Phuket, Rawai, Tuesday;
	This is File Guardian desktop CHM help system wrapper class implementation file.
*/
#include "StdAfx.h"
#include "FW_desktop_ChmHelper.h"

using namespace fw::desktop::UI;

#include "Shared_GenericAppObject.h"

using namespace shared::user32;

#ifndef HH_DISPLAY_TOPIC
#define HH_DISPLAY_TOPIC        0x0000
#endif
/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace details
{
	CAtlString    CChmHelper_GetTopicOf (const CTabView::ID _view)
	{
		CAtlString cs_topic;
		switch (_view)
		{
		case CTabView::eDatabase  : cs_topic = _T("Fg_20_DbManager.html"  ); break;
		case CTabView::eEventLog  : cs_topic = _T("Fg_08_SysEvtList.html" ); break;
		case CTabView::eGenSetup  : cs_topic = _T("Fg_12_GenSettings.html"); break;
		case CTabView::eLicense   : cs_topic = _T("Fg_23_LicPolicy.html"  ); break;
		case CTabView::ePrinting  : cs_topic = _T("Fg_10_SysPrint.html"   ); break;
		case CTabView::eService   : cs_topic = _T("Fg_16_ManService.html" ); break;
		default:
			cs_topic = _T("Fg_01_Overview.html");
		}
		return cs_topic;
	}

	CAtlString    CChmHelper_GetCmdLine (const CTabView::ID _view)
	{
		CAtlString cs_topic = CChmHelper_GetTopicOf(_view);
		CAtlString cs_file;
		{
			const CApplication& the_app = GetAppObjectRef();
			the_app.GetPathFromAppFolder(
					_T(".\\Fg_HelpSystem.chm"),
					cs_file
				);
		}
		CAtlString cmd_line;
		cmd_line.Format(
				_T("ms-its:%s::/html/%s"),
				(LPCTSTR)cs_file,
			(LPCTSTR)cs_topic
			);
		return cmd_line;
	}

	CAtlStringA   CChmHelper_GetFilePath(const CTabView::ID _view)
	{
		CAtlStringA file_;
		{
			CAtlString current_;
			const CApplication& the_app = GetAppObjectRef();
			the_app.GetPath(current_);

			file_ += current_;
			file_ += "Fg_HelpSystem.chm";
		}

		switch (_view)
		{
		case CTabView::eDatabase  : file_ += "::/Fg_20_DbManager.html"   ; break;
		case CTabView::eEventLog  : file_ += "::/Fg_08_SysEvtList.html"  ; break;
		case CTabView::eGenSetup  : file_ += "::/Fg_12_GenSettings.html" ; break;
		case CTabView::eLicense   : file_ += "::/Fg_23_LicPolicy.html"   ; break;
		case CTabView::ePrinting  : file_ += "::/Fg_10_SysPrint.html"    ; break;
		case CTabView::eService   : file_ += "::/Fg_16_ManService.html"  ; break;
		default:
			file_ += "::/Fg_01_Overview.html";
		}

		return file_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CChmHelper::CChmHelper(void) : m_help(NULL)
{
	m_help = ::LoadLibrary(_T("Htmlhelp.dll"));
}

CChmHelper::~CChmHelper(void)
{
	if (m_help)
	{
		::FreeLibrary(m_help);
		m_help = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

VOID      CChmHelper::ShowHelpTopic(const CTabView::ID _view)
{
	/*if (NULL == m_help)
		return;
	typedef HWND (WINAPI *pfn_HtmlHelp)(HWND hwndCaller, LPCSTR pszFile, UINT uCommand, DWORD dwData);

	pfn_HtmlHelp pfHelp = reinterpret_cast<pfn_HtmlHelp>(
								::GetProcAddress(m_help, "HtmlHelp")
							);
	if (NULL == pfHelp)
		return;

	CAtlStringA file_ = details::CChmHelper_GetFilePath(_view);
	pfHelp(
			::GetActiveWindow(),
			file_.GetString(),
			HH_DISPLAY_TOPIC ,
			NULL
		);*/
	CWindow hHelp = ::FindWindow(NULL, _T("File Guardian Help"));
	if (hHelp)
		hHelp.SendMessage(WM_CLOSE);

	CAtlString cmd_line = details::CChmHelper_GetCmdLine(_view);
	::ShellExecute(
			NULL,
			_T("open")  ,
			_T("hh.exe"),
			cmd_line,
			NULL    ,
			SW_SHOW
		);
}