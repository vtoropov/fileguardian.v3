#ifndef _FWDESKTOPMENUBAR_H_822EC824_BE86_4d40_A172_0DE7F2E95259_INCLUDED
#define _FWDESKTOPMENUBAR_H_822EC824_BE86_4d40_A172_0DE7F2E95259_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-May-2016 at 10:16:52pm, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian Main Frame Manu Bar class declaration file.
*/
#include "fg.desktop.tab.set.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	class CMenuBar
	{
	private:
		CTabSetMain&   m_tabs;
		WTL::CMenu     m_bar;
	public:
		CMenuBar(CTabSetMain&);
	public:
		VOID     UpdateState(const HMENU _sub_menu, const INT _sub_ndx, const CTabView& _selected);
	public:
		operator HMENU(void)const;
	public:
		static WORD CommandFirst(void);
		static WORD CommandLast(void);
	};
}}}}

#endif/*_FWDESKTOPMENUBAR_H_822EC824_BE86_4d40_A172_0DE7F2E95259_INCLUDED*/