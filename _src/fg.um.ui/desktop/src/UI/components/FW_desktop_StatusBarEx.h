#ifndef _FWDESKTOPSTATUSBAREX_H_F6D64B6A_6CCB_4380_A7E7_9F157DA375FC_INCLUDED
#define _FWDESKTOPSTATUSBAREX_H_F6D64B6A_6CCB_4380_A7E7_9F157DA375FC_INCLUDED
/*
	Created by Tech_dog (VToropov) on 12-Nov-2015 at 10:29:42pm, GMT+7, Phuket, Rawai, Thursday;
	This is File Watcher desktop status bar extended control class declaration file.
*/

namespace fw { namespace desktop { namespace UI { namespace components
{

	class CStatusMsgType
	{
	public:
		enum _e{
			eError   = 0x0,
			eWarning = 0x1,
			eInfo    = 0x2,
			eWaiting = 0x3,
		};
	};

	class CStatusColor
	{
	public:
		enum _e{
			eYellow = 0x0,
			eGreen  = 0x1,
			eRed    = 0x2,
			eBlue   = 0x3
		};
	};

	class CStatusBarPane
	{
	public:
		enum _enum{
			eImage     = 0,
			eMessage   = 1,
			eRecords   = 2,
			eIndicator = 3,
			eGap       = 4,
		};
	};

	class CStatusBar
	{
	private:
		::WTL::CStatusBarCtrl    m_control;       // status bar control object
		mutable SIZE             m_size;          // status bar control size
		CStatusMsgType::_e       m_type;          // last message type
		HIMAGELIST               m_images;        // images for the message type displaying
		CAtlString               m_state;         // state indicator text
		CStatusColor::_e         m_color;         // state indicator color
	public:
		CStatusBar(void);
		~CStatusBar(void);
	public:
		HRESULT                  Create  (const HWND hParent);
		HRESULT                  Destroy (void);
		const SIZE&              GetSize (void) const;
		HRESULT                  SetRecords(const INT _count);
		HRESULT                  SetText (LPCTSTR lpszText, const CStatusMsgType::_e, const bool bFitToSize = false);
		HRESULT                  SetState(LPCTSTR lpszText, const CStatusColor::_e);
		LRESULT                  Update  (const DRAWITEMSTRUCT&);
	private:
		CStatusBar(const CStatusBar&);
		CStatusBar& operator= (const CStatusBar&);
	};

	CStatusBar&    GetStatusBarObject(void);         // gets a reference to status bar control static object
}}}}

#endif/*_FWDESKTOPSTATUSBAREX_H_F6D64B6A_6CCB_4380_A7E7_9F157DA375FC_INCLUDED*/