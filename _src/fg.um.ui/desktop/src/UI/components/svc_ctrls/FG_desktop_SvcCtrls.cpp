/*
	Created by Tech_dog (ebontrop@gmail.com) 11-Jan-2018 at 9:02:43p, UTC+7, Phuket, Rawai, Thursday;
	This is File Guardian background service desktop UI control interface implementation file.
*/
#include "StdAfx.h"
#include "FG_desktop_SvcCtrls.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI::components;

#include "FG_Service_Manager.h"
#include "FG_Service_Settings.h"

using namespace fg::common;
using namespace fg::common::data;

#include "Shared_SystemServiceDefs.h"

using namespace shared::service;

#include "FG_Svc_Registry.h"

using namespace fw::desktop::data;

#include "fg.desktop.tab.set.h"

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components { namespace details
{
	using fg::common::CFwServiceManager;

	CFwServiceManager& CTabPageSvc_ManagerObject(void)
	{
		static CFwServiceManager mgr_;
		return mgr_;
	}

	CTabPageSvcTimer& CTabPageSvc_GetTimerObject(void)
	{
		static CTabPageSvcTimer timer_;
		return timer_;
	}
}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageSvcCtrls::CTabPageSvcCtrls(CWindow& page_ref) : m_page_ref(page_ref)
{
}

CTabPageSvcCtrls::~CTabPageSvcCtrls(void)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID           CTabPageSvcCtrls::Initialize(void)
{
	bool bServiceInstalled = details::CTabPageSvc_ManagerObject().IsInstalled();

	CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_INSTL);
	if (ctrl_)
		ctrl_.SetWindowText(bServiceInstalled ? _T("Uninstall") : _T("Install"));

	::WTL::CEdit edt_;
	edt_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_SETUP);
	if (edt_)
	{
		CServiceSetup::_e eSetup_ = CServiceSetup::eNotInstalled;
		if (bServiceInstalled)
			eSetup_ = CServiceSetup::eInstalled;

		CServiceSetup setup_(eSetup_);
		edt_.SetWindowText(setup_.Name());
		edt_.EnableWindow(bServiceInstalled);
	}

	const CService& svc_ref = details::CTabPageSvc_ManagerObject().GetServiceObject();
	global::SetInfoMessage(NULL);

	edt_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_STATE);
	if (edt_)
	{
		edt_.SetWindowText(svc_ref.State().Name());
	}

	ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_CTRL);
	if (ctrl_)
	{
		ctrl_.EnableWindow(bServiceInstalled);

		if (svc_ref.State().IsRunning()) {
			ctrl_.SetWindowText(_T("Stop"));
		}
		else if (!svc_ref.State().Error())
			ctrl_.SetWindowText(_T("Start"));
		else {
			ctrl_.SetWindowText(_T("Try To Start"));
			global::SetErrorMessage(
					static_cast<DWORD>(CTabView::eService), svc_ref.State().Error()
				);
		}
	}
	ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_UPDT);
	if (ctrl_)
		ctrl_.EnableWindow(bServiceInstalled);
}

VOID           CTabPageSvcCtrls::InitializeForWait(void)
{
	CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_INSTL);
	if (ctrl_){
		ctrl_.SetWindowText(_T("Waiting"));
		ctrl_.EnableWindow (FALSE);
	}
	::WTL::CEdit edt_;
	edt_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_SETUP);
	if (edt_){
		edt_.SetWindowText(_T("Service is being deleted..."));
		edt_.EnableWindow (FALSE);
	}
	edt_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_STATE);
	if (edt_){
		edt_.SetWindowText(_T("Waiting for completion..."));
		edt_.EnableWindow (FALSE);
	}
	ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_CTRL);
	if (ctrl_){
		ctrl_.SetWindowText(_T("Undefined"));
		ctrl_.EnableWindow(FALSE);
	}
}

BOOL           CTabPageSvcCtrls::OnCommand(const WORD wCtrlId, const WORD wNotifyCode)
{
	BOOL bHandled = TRUE;  wNotifyCode;

	if (false){}
	else if (IDC_FWDESKTOP_SERVICE_INSTL  == wCtrlId)
	{
		CFwServiceManager& mgr_ = details::CTabPageSvc_ManagerObject();

		bool bResult = false;

		const bool bServiceInstalled = mgr_.IsInstalled();

		if (bServiceInstalled)
			bResult = mgr_.UninstallService();
		else
			bResult = mgr_.InstallService();
		if (bResult)
		{
			this->Initialize();
		}
		else {
			global::SetErrorMessage(mgr_.Error());
			this->InitializeForWait();
		}
	}
	else if (IDC_FWDESKTOP_SERVICE_CTRL   == wCtrlId)
	{
		CFwServiceManager& mgr_ = details::CTabPageSvc_ManagerObject();
		
		bool bResult = false;

		const CService& svc_ref = mgr_.GetServiceObject();
		if (svc_ref.State().IsRunning())
			bResult = mgr_.StopService();
		else
			bResult = mgr_.StartService();

		if (bResult)
		{
			this->Initialize();
		}
		else
			global::SetErrorMessage(mgr_.Error());
	}
	else if (IDC_FWDESKTOP_SERVICE_UPDT   == wCtrlId)
	{
		this->Initialize();
	}
	else if (IDC_FWDESKTOP_SERVICE_STAT   == wCtrlId)
	{
		CButton btn_ = m_page_ref.GetDlgItem(wCtrlId);
		if (btn_)
		{
			CTabPageSvcTimer& timer_ = details::CTabPageSvc_GetTimerObject();

			bool bAutoCheck = !!btn_.GetCheck();
			timer_.ManageByState(bAutoCheck);

			CService_RegMain reg_;
			reg_.StateAutoCheck(bAutoCheck); // saves the control state
		}
	}
	else if (IDC_FWDESKTOP_SERVICE_REMDEV == wCtrlId)
	{
		CButton btn_ = m_page_ref.GetDlgItem(wCtrlId);
		if (btn_)
		{
			CServiceSettingPersBase pers_;
			pers_.TrackRemoval(!!btn_.GetCheck());
			pers_.Save();
		}
	}
	else
		bHandled = FALSE;

	return bHandled;
}

/////////////////////////////////////////////////////////////////////////////

CTabPageSvcTimer::CTabPageSvcTimer(void) : m_timer(NULL)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID        CTabPageSvcTimer::Create(void)
{
	if (!m_owner)
		return;
	if (m_timer)
		return;
	m_timer = m_owner.SetTimer(1, 2000);
}

VOID        CTabPageSvcTimer::Destroy(void)
{
	if (!m_owner)
		return;
	if (!m_timer)
		return;
	m_owner.KillTimer(m_timer);
	m_timer = NULL;
}

VOID        CTabPageSvcTimer::ManageByCtrl(const UINT ctrlId)
{
	if (!m_owner)
		return;
	WTL::CButton btn_ = m_owner.GetDlgItem(ctrlId);
	if (!btn_)
		return;
	const bool bAutoCheck = !!btn_.GetCheck();
	this->ManageByState(bAutoCheck);
}

VOID        CTabPageSvcTimer::ManageByState(const bool _auto_check)
{
	if (!m_owner)
		return;
	if (_auto_check)
		this->Create();
	else
		this->Destroy();
}

HWND        CTabPageSvcTimer::Owner(void)const
{
	return m_owner;
}

HRESULT     CTabPageSvcTimer::Owner(const CWindow& _owner)
{
	m_owner = _owner;
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CTabPageSvcTimer& CTabPageSvcTimer::GetTimer(void)
{
	return details::CTabPageSvc_GetTimerObject();
}