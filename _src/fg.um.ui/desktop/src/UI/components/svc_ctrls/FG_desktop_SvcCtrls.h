#ifndef _FGDESKTOPSVCCTRLS_H_CE3271EC_639C_4f55_A50A_4C6B37E6586B_INCLUDED
#define _FGDESKTOPSVCCTRLS_H_CE3271EC_639C_4f55_A50A_4C6B37E6586B_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) 11-Jan-2018 at 8:52:24p, UTC+7, Phuket, Rawai, Thursday;
	This is File Guardian background service desktop UI control interface declaration file.
*/

namespace fw { namespace desktop { namespace UI { namespace components
{
	class CTabPageSvcCtrls {

	private:
		CWindow&       m_page_ref;

	public:
		CTabPageSvcCtrls(CWindow& page_ref);
		~CTabPageSvcCtrls(void);

	public:
		VOID           Initialize(void);
		VOID           InitializeForWait(void);
		BOOL           OnCommand(const WORD wCtrlId, const WORD wNotifyCode);
	};

	class CTabPageSvcTimer {

	private:
		UINT_PTR       m_timer;
		CWindow        m_owner;

	public:
		CTabPageSvcTimer(void);

	public:
		VOID           Create (void);
		VOID           Destroy(void);
		VOID           ManageByCtrl (const UINT ctrlId);
		VOID           ManageByState(const bool _auto_check);
		HWND           Owner(void)const;
		HRESULT        Owner(const CWindow& _owner);

	public:
		static
		CTabPageSvcTimer& GetTimer(void);
	};

}}}}

#endif/*_FGDESKTOPSVCCTRLS_H_CE3271EC_639C_4f55_A50A_4C6B37E6586B_INCLUDED*/