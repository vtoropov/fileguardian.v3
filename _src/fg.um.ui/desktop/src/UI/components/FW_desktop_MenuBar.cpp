/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-May-2016 at 10:24:36pm, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian Main Frame Manu Bar class implementation file.
*/
#include "StdAfx.h"
#include "FW_desktop_MenuBar.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI::components;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components { namespace details
{
	class MenuBar_Spec {
	public:

		static const UINT* MenuBarIds(INT& _size) {
			static UINT ids_[] = {
				IDR_FGDESKTOP_MAIN_MNU_FILE, IDR_FGDESKTOP_MAIN_MNU_DATA, IDR_FGDESKTOP_MAIN_MNU_VIEW, IDR_FGDESKTOP_MAIN_MNU_HELP, IDR_FGDESKTOP_MAIN_MNU_LOGS, NULL
			};
			_size = _countof(ids_);
			return ids_;
		}

		static const UINT* ViewMenuIds(INT& _size) {
			static UINT ids_[] = {
				IDR_FWDESKTOP_MENU_ITM_VIEW_0, IDR_FWDESKTOP_MENU_ITM_VIEW_1, IDR_FWDESKTOP_MENU_ITM_VIEW_2, IDR_FWDESKTOP_MENU_ITM_VIEW_3, IDR_FWDESKTOP_MENU_ITM_VIEW_4, NULL
			};
			_size = _countof(ids_);
			return ids_;
		}

		static const UINT* LogMenuIds(INT& _size) {
			static UINT ids_[] = {
				IDR_FWDESKTOP_MENU_ITM_VIEW_0, IDR_FGDESKTOP_MENU_ITM_TOTALS
			};
			_size = _countof(ids_);
			return ids_;
		}
	};

	VOID   MenuBar_UpdateFileMenu(const HMENU _menu, const CTabView& _selected)
	{
		CMenuHandle menu_ = _menu;
		if ( !menu_ )
			return;
		{
			const bool bEnabled = (CTabView::eEventLog == _selected.Identifier());
			menu_.EnableMenuItem(
					IDR_FWDESKTOP_MENU_ITM_OPEN,
					MF_BYCOMMAND| (bEnabled ? MF_ENABLED : MF_DISABLED|MF_GRAYED)
				);
			menu_.EnableMenuItem(
					IDR_FWDESKTOP_MENU_ITM_PRINT,
					MF_BYCOMMAND| (bEnabled ? MF_ENABLED : MF_DISABLED|MF_GRAYED)
				);
		}
		{
			const bool bChecked = (CTabView::ePrinting == _selected.Identifier());
			menu_.CheckMenuItem(
					IDR_FWDESKTOP_MENU_ITM_PRINT,
					MF_BYCOMMAND| (bChecked ? MF_CHECKED : MF_UNCHECKED)
				);
		}
	}

	VOID   MenuBar_UpdateDataMenu(const HMENU _menu, const CTabView& _selected)
	{
		CMenuHandle menu_ = _menu;
		if ( !menu_ )
			return;
		{
			const bool bEnabled = (CTabView::eEventLog == _selected.Identifier());
			static const UINT nMenuItmIds[] = {
				IDR_FWDESKTOP_MENU_ITM_FILTER ,
				IDR_FWDESKTOP_MENU_ITM_REFRESH,
				IDR_FWDESKTOP_MENU_ITM_EXPORT , IDR_FGDESKTOP_NENU_ITM_PROPERS, IDR_FGDESKTOP_MENU_ITM_OPTIONS
			};
			for (INT i_ = 0; i_ < _countof(nMenuItmIds); i_++) {
				menu_.EnableMenuItem(
					nMenuItmIds[i_],
					MF_BYCOMMAND| (bEnabled ? MF_ENABLED : MF_DISABLED|MF_GRAYED)
				);
			}
		}
	}

	VOID   MenuBar_UpdateLogsMenu(const HMENU _menu, const CTabView& _selected, CTabSetMain& _tabs)
	{
		CMenuHandle menu_ = _menu;
		if ( !menu_ )
			return;

		const bool bEnabled = (CTabView::ePrinting != _selected.Identifier());

		INT sz_ = 0;
		const UINT* nIds = details::MenuBar_Spec::LogMenuIds(sz_);

		for (INT i_ = 0; i_ < sz_; i_++) {

			menu_.EnableMenuItem(
					nIds[i_],
					MF_BYCOMMAND| (bEnabled ? MF_ENABLED : MF_DISABLED|MF_GRAYED)
				);

			bool bChecked = false;
			switch (i_)
			{
			case 0: bChecked = (CTabView::eEventLog == _selected.Identifier()); break;
			case 1: bChecked = (_tabs.GetTabOfLog().FooterVisible()); break;
			}

			menu_.CheckMenuItem(
					nIds[i_],
					MF_BYCOMMAND| (bChecked ? MF_CHECKED : MF_UNCHECKED)
				);
		}
	}

	VOID   MenuBar_UpdateViewMenu(const HMENU _menu, const CTabView& _selected, CTabSetMain& _tabs)
	{
		CMenuHandle menu_ = _menu; _tabs;
		if ( !menu_ )
			return;

		const bool bEnabled = (CTabView::ePrinting != _selected.Identifier());

		INT sz_ = 0;
		const UINT* nIds = details::MenuBar_Spec::ViewMenuIds(sz_);

		for (INT i_ = 0; i_ < sz_; i_++)
		{
			menu_.EnableMenuItem(
					nIds[i_],
					MF_BYCOMMAND| (bEnabled ? MF_ENABLED : MF_DISABLED|MF_GRAYED)
				);

			bool bChecked = false;
			switch (i_)
			{
			case 0: bChecked = (CTabView::eEventLog == _selected.Identifier()); break;
			case 1: bChecked = (CTabView::eGenSetup == _selected.Identifier()); break;
			case 2: bChecked = (CTabView::eService  == _selected.Identifier()); break;
			case 3: bChecked = (CTabView::eDatabase == _selected.Identifier()); break;
			case 4: bChecked = (CTabView::eLicense  == _selected.Identifier()); break;
			}
			menu_.CheckMenuItem(
					nIds[i_],
					MF_BYCOMMAND| (bChecked ? MF_CHECKED : MF_UNCHECKED)
				);
		}
	}
}}}}}

/////////////////////////////////////////////////////////////////////////////

CMenuBar::CMenuBar(CTabSetMain& _tabs) : m_tabs(_tabs)
{
	m_bar.LoadMenu(IDR_FWDESKTOP_MAIN_DLG_MENUBAR);
	if (NULL != m_bar) {

		CMenuHandle bar_ = m_bar;
		const INT nItemCount = bar_.GetMenuItemCount();

		INT sz_ = 0;
		const UINT* pIds = details::MenuBar_Spec::MenuBarIds(sz_);

		for (INT i_ = 0; i_ < nItemCount && i_ < sz_; i_++) {

			::WTL::CMenuHandle menu_ = bar_.GetSubMenu(i_);
			if (NULL != menu_) {

				MENUINFO info_ = {0};
				info_.cbSize = sizeof(info_);
				info_.fMask  = MIM_MENUDATA;
				info_.dwMenuData = pIds[i_];

				menu_.SetMenuInfo(&info_);

				const INT nSubItmCount = menu_.GetMenuItemCount();

				for (INT j_ = 0; j_ < nSubItmCount; j_++) {

					::WTL::CMenuHandle sub_menu_ = menu_.GetSubMenu(j_);
					if (NULL == sub_menu_)
						continue;
					//
					// TODO: this code expects log sub-menu only; it must be re-viewed for accessing any sub-menu;
					//
					info_.dwMenuData = IDR_FGDESKTOP_MAIN_MNU_LOGS;
					sub_menu_.SetMenuInfo(&info_);
				}
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////

VOID     CMenuBar::UpdateState(const HMENU _sub_menu, const INT _sub_ndx, const CTabView& _selected)
{
	_sub_ndx;
	if (!m_bar)
		return;

	MENUINFO info_ = {0};
	info_.cbSize = sizeof(info_);
	info_.fMask  = MIM_MENUDATA;

	::WTL::CMenuHandle menu_ = _sub_menu;
	menu_.GetMenuInfo(&info_);

	switch (info_.dwMenuData)
	{
	case IDR_FGDESKTOP_MAIN_MNU_FILE: details::MenuBar_UpdateFileMenu(_sub_menu, _selected); break;
	case IDR_FGDESKTOP_MAIN_MNU_DATA: details::MenuBar_UpdateDataMenu(_sub_menu, _selected); break;
	case IDR_FGDESKTOP_MAIN_MNU_VIEW: details::MenuBar_UpdateViewMenu(_sub_menu, _selected, m_tabs); break;
	case IDR_FGDESKTOP_MAIN_MNU_LOGS: details::MenuBar_UpdateLogsMenu(_sub_menu, _selected, m_tabs); break;
	}
}

/////////////////////////////////////////////////////////////////////////////

CMenuBar::operator HMENU(void)const
{
	return m_bar;
}

/////////////////////////////////////////////////////////////////////////////

WORD CMenuBar::CommandFirst(void)
{
	return IDR_FWDESKTOP_MENU_ITM_OPEN;
}

WORD CMenuBar::CommandLast(void)
{
	return IDR_FGDESKTOP_NENU_ITM_PROPERS;
}