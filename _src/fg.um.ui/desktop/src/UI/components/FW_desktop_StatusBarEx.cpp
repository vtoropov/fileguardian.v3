/*
	Created by Tech_dog (VToropov) on 14-Jan-2016 at 10:35:24pm, GMT+7, Phuket, Rawai, Thursday;
	This is TS1 key gen application status bar extended control class implementation file.
*/
#include "StdAfx.h"
#include "FW_desktop_StatusBarEx.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI::components;

#include "UIX_GdiProvider.h"
#include "UIX_GdiObject.h"

using namespace ex_ui;
using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components 
{
	CStatusBar&    GetStatusBarObject(void)
	{
		static CStatusBar ctrl_;
		return ctrl_;
	}

namespace details
{
	static INT StatusBar_MsgTypeToImageIndex(const CStatusMsgType::_e eType)
	{
		switch (eType)
		{
		case CStatusMsgType::eError:     return ( 2);
		case CStatusMsgType::eInfo:      return ( 0);
		case CStatusMsgType::eWarning:   return ( 1);
		case CStatusMsgType::eWaiting:   return ( 4);
		}
		return -1;
	}

	struct StatusBar_ColorInfo
	{
		COLORREF   clr0;
		COLORREF   clr1;
		COLORREF   fore;
		StatusBar_ColorInfo(void) : clr0(CLR_NONE), clr1(CLR_NONE), fore(::GetSysColor(COLOR_WINDOWTEXT)) {}
	};

	class  StatusBar_DrawingPart
	{
	public:
		enum _e{
			eEntire = 0,
			eTop    = 1,
		};
	};

	HRESULT    StatusBar_GetColorInfo(const CStatusColor::_e eColor, StatusBar_ColorInfo& clr_info, const StatusBar_DrawingPart::_e ePart)
	{
		switch (eColor)
		{
		case CStatusColor::eRed:
			{
				if (StatusBar_DrawingPart::eEntire == ePart)
				{
					clr_info.clr0 = RGB(200,  28,  23);
					clr_info.clr1 = RGB(233,  97,  69);
				}
				else
				{
					clr_info.clr0 = RGB(246, 183, 192);
					clr_info.clr1 = RGB(221,  61,  80);
				}
				clr_info.fore = RGB(0xff, 0xff, 0xff);
			} break;
		case CStatusColor::eYellow:
			{
				if (StatusBar_DrawingPart::eEntire == ePart)
				{
					clr_info.clr0 = RGB(210, 200,   4);
					clr_info.clr1 = RGB(243, 231,   4);
				}
				else
				{
					clr_info.clr0 = RGB(251, 250, 193);
					clr_info.clr1 = RGB(223, 211,  90);
				}
				clr_info.fore = COLORREF(0);
			} break;
		case CStatusColor::eBlue:
			{
				if (StatusBar_DrawingPart::eEntire == ePart)
				{
					clr_info.clr0 = RGB(  0,  54, 116);
					clr_info.clr1 = RGB(  3, 104, 179);
				}
				else
				{
					clr_info.clr0 = RGB( 83, 200, 248);
					clr_info.clr1 = RGB(  4,  80, 155);
				}
				clr_info.fore = RGB(0xff, 0xff, 0xff);
			} break;
		case CStatusColor::eGreen:
			{
				if (StatusBar_DrawingPart::eEntire == ePart)
				{
					clr_info.clr0 = RGB( 35, 101,  41);
					clr_info.clr1 = RGB( 96, 173,  93);
				}
				else
				{
					clr_info.clr0 = RGB(207, 223, 210);
					clr_info.clr1 = RGB( 67, 132,  84);
				}
				clr_info.fore = RGB(0xff, 0xff, 0xff);
			} break;
		default:
			ATLASSERT(0);
			return DISP_E_TYPEMISMATCH;
		}
		return S_OK;
	}
}}}}}

/////////////////////////////////////////////////////////////////////////////

CStatusBar::CStatusBar(void) :
m_type(CStatusMsgType::eInfo), m_images(NULL), m_color(CStatusColor::eYellow), m_state(_T("Not Connected"))
{
	m_size.cx = m_size.cy = 0;

	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
	CGdiPlusPngLoader::CreateImages(
			IDR_FWDESKTOP_MAIN_DLG_IMAGES,
			hInstance,
			5,
			m_images
		);
}

CStatusBar::~CStatusBar(void)
{
	if (m_images)
	{
		::ImageList_Destroy(m_images); m_images = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

HRESULT         CStatusBar::Create(const HWND hParent)
{
	if (m_control.IsWindow())
		return S_OK;
	if (!::IsWindow(hParent))
		return E_INVALIDARG;
	RECT rcClient = {0};
	if (!::GetClientRect(hParent, &rcClient))
		return E_UNEXPECTED;

	// creates a status bar
	m_control.Create(
			hParent,
			rcClient,
			0,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|SBT_TOOLTIPS,
			//
			// this extended style is very important: it enables double buffer draw and removes blink of the control;
			//
			WS_EX_COMPOSITED,
			IDC_FWDESKTOP_MAIN_DLG_STATUS
		);

	m_control.SetFont(::ATL::CWindow(hParent).GetFont());

	RECT rcStatus = {0};
	m_control.GetWindowRect(&rcStatus);
	::OffsetRect(&rcStatus, -rcStatus.left, -rcStatus.top);

	m_size.cx = rcStatus.right;
	m_size.cy = rcStatus.bottom;

	INT nWidth[] = { // each element specifies the position, in client coordinates, of the right edge of the corresponding part.
			25,
			m_size.cx - 250,
			m_size.cx - 140,
			m_size.cx -  25,
			m_size.cx -   5
		};

	m_control.SetParts(_countof(nWidth), &nWidth[0]);
	m_control.SetText (CStatusBarPane::eImage    ,   _T(""), SBT_OWNERDRAW);
	m_control.SetText (CStatusBarPane::eMessage  ,   _T("Ready"));
	m_control.SetText (CStatusBarPane::eRecords  ,   _T("0 rec(s)"));
	m_control.SetText (CStatusBarPane::eIndicator,   _T(""), SBT_OWNERDRAW);

	return (m_control.IsWindow() ? S_OK : E_FAIL);
}

HRESULT         CStatusBar::Destroy(void)
{
	if (!m_control.IsWindow())
		return S_FALSE;
	m_control.SendMessage(WM_CLOSE);
	if (m_control.m_hWnd)
		m_control.m_hWnd = NULL; // static object preserves old invalid value
	return S_OK;
}

const SIZE&     CStatusBar::GetSize(void) const
{
	return m_size;
}

HRESULT         CStatusBar::SetRecords(const INT _count)
{
	CAtlString cs_recs;
	cs_recs.Format(
			_T("%d rec(s)"),
			_count
		);
	HRESULT hr_ = (m_control.SetText(CStatusBarPane::eRecords, cs_recs) ? S_OK : E_UNEXPECTED);
	if (!FAILED(hr_))
		m_control.RedrawWindow(NULL, NULL, RDW_ERASE|RDW_INVALIDATE|RDW_ERASENOW);
	return hr_;
}

HRESULT         CStatusBar::SetText(LPCTSTR lpszText, const CStatusMsgType::_e eType, const bool bFitToSize)
{
	m_type = eType;
	HRESULT hr_ = S_OK;

	if (bFitToSize) {
		RECT rc_ = {0};
		if (m_control.GetRect(eType, &rc_) == FALSE)
			return (hr_ = DISP_E_BADINDEX);

		const LONG n_width_ = (rc_.right - rc_.left);
		if (0 == n_width_)
			return (hr_ = HRESULT_FROM_WIN32(ERROR_INVALID_DATA));

		CAtlString cs_msg(lpszText);

		ex_ui::draw::common::CText text_obj_(
				m_control.GetDC(), m_control.GetFont()
			);
		SIZE sz_bound = {0};

		do {

			hr_ = text_obj_.GetSize(cs_msg.GetString(), sz_bound);
			if (FAILED(hr_))
				return hr_;
			if (sz_bound.cx > n_width_) {
				UINT n_ratio = sz_bound.cx / n_width_;
				if ( n_ratio == 0)
				     n_ratio  = 1;

				cs_msg = cs_msg.Left(cs_msg.GetLength()/ n_ratio - 1);
			} else {
				if (cs_msg.GetLength() > 3) {
					cs_msg = cs_msg.Left(cs_msg.GetLength() - 3);
					cs_msg+=_T("...");
				}
				else
					cs_msg =_T("...");

				break;
			}

		} while (cs_msg.IsEmpty() == false);

		if (!m_control.SetText(CStatusBarPane::eMessage, cs_msg.GetString()))
			hr_ = E_UNEXPECTED;
	}
	else {
		if (!m_control.SetText(CStatusBarPane::eMessage, lpszText))
			hr_ = E_UNEXPECTED;
	}

	if (!FAILED(hr_))
	{
		m_control.SetTipText(CStatusBarPane::eMessage, lpszText);
		m_control.RedrawWindow(NULL, NULL, RDW_ERASE|RDW_INVALIDATE|RDW_ERASENOW|RDW_UPDATENOW);
	}
	return hr_;
}

HRESULT         CStatusBar::SetState(LPCTSTR lpszText, const CStatusColor::_e eColor)
{
	m_color = eColor;
	m_state = lpszText;
	HRESULT hr_ = S_OK;
	if (!FAILED(hr_))
		m_control.RedrawWindow(NULL, NULL, RDW_ERASE|RDW_INVALIDATE|RDW_ERASENOW|RDW_UPDATENOW);
	return hr_;
}

LRESULT         CStatusBar::Update (const DRAWITEMSTRUCT& cData)
{
	if (cData.itemID == CStatusBarPane::eImage)
	{
		RECT rect = cData.rcItem;
		if (WTL::CTheme::IsThemingSupported())
			rect.right -= 5;
		if (rect.right  < 1)
			return 0;

		ex_ui::draw::CZBuffer z_buffer(cData.hDC, rect);

		z_buffer.FillSolidRect(&rect, ::GetSysColor(COLOR_BTNFACE));

		const INT x_ = rect.left + (__W(rect) - 16)/2;
		const INT y_ = rect.top  + (__H(rect) - 16)/2;

		if (m_images)
		{
			ImageList_DrawEx(
					m_images,
					details::StatusBar_MsgTypeToImageIndex(m_type),
					z_buffer,
					x_,
					y_,
					16,
					16,
					CLR_DEFAULT,
					CLR_DEFAULT,
					ILD_NORMAL
				);
		}
	}
	if (cData.itemID == CStatusBarPane::eIndicator)
	{
		RECT rect = cData.rcItem;
		if (WTL::CTheme::IsThemingSupported())
			rect.right -= 5;
		if (rect.right  < 1)
			return 0;

		ex_ui::draw::CZBuffer z_buffer(cData.hDC, rect);

		RECT rectPart1 = rect;
		RECT rectPart2 = rect;

		rectPart2.bottom = (rect.bottom + rect.top) /2;

		details::StatusBar_ColorInfo clr_schema;

		details::StatusBar_GetColorInfo(m_color, clr_schema, details::StatusBar_DrawingPart::eEntire);

		z_buffer.DrawGragRect(rectPart1, clr_schema.clr0, clr_schema.clr1, true, 255);

		details::StatusBar_GetColorInfo(m_color, clr_schema, details::StatusBar_DrawingPart::eTop);

		z_buffer.DrawGragRect(rectPart2, clr_schema.clr0, clr_schema.clr1, true, 255);

		int iZPoint = z_buffer.SaveDC();

		rect.top += 2;

		z_buffer.SelectFont(m_control.GetParent().GetFont());
		z_buffer.SetBkMode(TRANSPARENT);
		z_buffer.SetTextColor(clr_schema.fore);

		::ATL::CAtlString cs_text = m_state;
		z_buffer.DrawText(cs_text.GetString(), -1, &rect, DT_VCENTER|DT_CENTER);

		z_buffer.RestoreDC(iZPoint);
	}
	return 0;
}