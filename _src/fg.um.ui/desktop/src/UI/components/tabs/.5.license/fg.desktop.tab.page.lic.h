#ifndef _FWDESKTOPTABPAGELIC_H_C1F4E47A_0586_4943_BFF1_8FF3840C6602_INCLUDED
#define _FWDESKTOPTABPAGELIC_H_C1F4E47A_0586_4943_BFF1_8FF3840C6602_INCLUDED
/*
	Created by Tech_dog (VToropov) on 4-May-2016 at 4:30:36pm, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian License Management Tab Page class declaration file.
*/
#include "FW_desktop_UI_Defs.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	class CTabPageLic:
		public  CTabPageBase, 
		public  ITabPageCallback
	{
		typedef CTabPageBase   TBasePage;
	public:
		CTabPageLic(::WTL::CTabCtrl&);
		~CTabPageLic(void);
	private: // ITabPageCallback
		virtual LRESULT        TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
	public:  // TBasePage
		virtual CAtlString     GetPageTitle(void) const override sealed;
	};
}}}}


#endif/*_FWDESKTOPTABPAGELIC_H_C1F4E47A_0586_4943_BFF1_8FF3840C6602_INCLUDED*/