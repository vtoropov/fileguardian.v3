/*
	Created by Tech_dog (VToropov) on 4-May-2016 at 4:35:38pm, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian License Management Tab Page class implementation file.
*/
#include "StdAfx.h"
#include "fg.desktop.tab.page.lic.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI;
using namespace fw::desktop::UI::components;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui;
using namespace ex_ui::draw::common;

#include "FG_License_Info.h"
#include "FG_License_Provider.h"

using namespace fg::common::lic;

#include "Shared_GenericAppResource.h"

using namespace shared::user32;

#include "fg.desktop.tab.set.h"
#include "FG_product_Updater.h"

using namespace fw::desktop;
/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components { namespace details
{
	class CTabPageLic_Update
	{
		typedef ::std::vector<UINT> TCtrlIds;
	private:
		const CWindow& m_page_ref;
		TCtrlIds m_ctrlIds;
	public:
		CTabPageLic_Update(const CWindow& page_ref) : m_page_ref(page_ref)
		{
			try {
				m_ctrlIds.push_back(IDC_FWDESKTOP_LIC_V_IMG);
				m_ctrlIds.push_back(IDC_FWDESKTOP_LIC_V_MSG);
				m_ctrlIds.push_back(IDC_FWDESKTOP_LIC_V_BTN);
			}catch(::std::bad_alloc&){
			}
		}
	public:
		VOID     InitCtrls(const CLicenseInfo& _lic) // this method is called when new version is available, otherwise, default;
		{
			for (size_t j_ = 0; j_ < m_ctrlIds.size(); j_++){
				CWindow ctrl_ = m_page_ref.GetDlgItem(m_ctrlIds[j_]);
				if (!ctrl_)
					continue;
				switch (m_ctrlIds[j_]){
				case IDC_FWDESKTOP_LIC_V_MSG:
					{
						CAtlString cs_msg;
						cs_msg.Format(
								_T("New version %s is available. You can upgrade File Guardian by pressing [Update] button."),
								_lic.Version().Value().GetString()
							);
						ctrl_.SetWindowText(cs_msg);
					} break;
				case IDC_FWDESKTOP_LIC_V_BTN:
					{
						ctrl_.EnableWindow(static_cast<BOOL>(_lic.IsActive()));
						ctrl_.SetActiveWindow();
					} break;
				}
				ctrl_.ShowWindow(SW_SHOW);
			}
		}
		VOID     InitCtrlsDefault(void)
		{
			for (size_t j_ = 0; j_ < m_ctrlIds.size(); j_++){
				CWindow ctrl_ = m_page_ref.GetDlgItem(m_ctrlIds[j_]);
				if (!ctrl_)
					continue;
				ctrl_.ShowWindow(SW_HIDE);
			}
		}
	};

	class CTabPageLic_Init
	{
	private:
		const CWindow& m_page_ref;
	public:
		CTabPageLic_Init(const CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		VOID     InitCtrls(void)
		{
			const UINT ctrlId[] = {
				IDC_FWDESKTOP_LIC_NEW_0,
				IDC_FWDESKTOP_LIC_NEW_1,
				IDC_FWDESKTOP_LIC_NEW_2,
				IDC_FWDESKTOP_LIC_NEW_3,
			};
			for (INT i_ = 0; i_ < _countof(ctrlId); i_++)
			{
				::WTL::CEdit edt_ = m_page_ref.GetDlgItem(ctrlId[i_]);
				if (!edt_)
					continue;
				edt_.SetCueBannerText(_T("XXXX"), FALSE);
				edt_.SetLimitText(4);
			}
			::WTL::CEdit edt_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_LIC_DESC);
			if (edt_)
				edt_.SetCueBannerText(_T("For example: Sales Computer 1"), TRUE);
		}

		VOID     InitCtrlsToDefault(void)
		{
			const UINT ctrlId[] = {
					IDC_FWDESKTOP_LIC_INSTALL,
					IDC_FWDESKTOP_LIC_SERIAL ,
					IDC_FWDESKTOP_LIC_PERIOD ,
					IDC_FWDESKTOP_LIC_LEFT   ,
					IDC_FWDESKTOP_LIC_STATUS ,
				};
			for (INT i_ = 0; i_ < _countof(ctrlId); i_++)
			{
				::WTL::CEdit edt_ = m_page_ref.GetDlgItem(ctrlId[i_]);
				if (!edt_)
					continue;
				edt_.SetWindowText(_T("#na"));
			}
		}

		VOID     LoadLicense(void)
		{
			CTabPageLic_Update update_(m_page_ref);
			update_.InitCtrlsDefault();

			CLicenseInfoPersistent lic_;
			HRESULT hr_ = lic_.Load();
			if (FAILED(hr_))
			{
				global::SetErrorMessage(lic_.Error());
				this->InitCtrlsToDefault();
				return;
			}
			CLicenseProvider prov_;
			hr_ = prov_.CheckLicense(lic_);
			if (FAILED(hr_))
			{
				global::SetErrorMessage(prov_.Error());
				this->InitCtrlsToDefault();
				return;
			}
			else
				global::SetInfoMessage(NULL);

			const UINT ctrlId[] = {
					IDC_FWDESKTOP_LIC_INSTALL,
					IDC_FWDESKTOP_LIC_SERIAL ,
					IDC_FWDESKTOP_LIC_PERIOD ,
					IDC_FWDESKTOP_LIC_LEFT   ,
					IDC_FWDESKTOP_LIC_STATUS ,
					IDC_FWDESKTOP_LIC_DESC   ,
				};
			for (INT i_ = 0; i_ < _countof(ctrlId); i_++)
			{
				::WTL::CEdit edt_ = m_page_ref.GetDlgItem(ctrlId[i_]);
				if (!edt_)
					continue;
				switch(i_)
				{
				case 0: edt_.SetWindowText(lic_.UserId());           break;
				case 1: edt_.SetWindowText(lic_.SerialFormatted());  break;
				case 2: edt_.SetWindowText(lic_.ExpireDateAsText()); break;
				case 3: edt_.SetWindowText(lic_.DaysLeftAsText());   break;
				case 4: edt_.SetWindowText(lic_.State().ToString()); break;
				case 5: edt_.SetWindowText(lic_.Description());      break;
				}
			}
			// checks new version availability
			const bool bIsAvailable = prov_.IsNewVersionAvailable(lic_);
			if (bIsAvailable){
				update_.InitCtrls(lic_);
			}
		}
	};

	class CTabPageLic_Layout
	{
	private:
		RECT           m_client_area;
		const CWindow& m_page_ref;
	public:
		CTabPageLic_Layout(const CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		VOID    AdjustCtrls(void)
		{
			HBITMAP hBitmap = NULL;
			HRESULT hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_LIC_LOGO,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_LIC_LOGO);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}
			hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_MAIN_DLG_INFO24,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_LIC_INFO);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap); // the image is destroyed in the next code section;
			}
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_LIC_V_IMG);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}
		}
	};

	class CTabPageLic_Handler
	{
	private:
		CWindow&    m_page_ref;
	public:
		CTabPageLic_Handler(CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		BOOL        OnCommand(const WORD wCtrlId, const WORD wNotifyCode)
		{
			BOOL bHandled = TRUE;  wNotifyCode;

			if (false){}
			else if (IDC_FWDESKTOP_LIC_REFRESH == wCtrlId)
			{
				CApplicationCursor wc_;
				global::SetWaitMessage(
						_T("Querying web license service...")
					);
				CTabPageLic_Init init_(m_page_ref);
				init_.LoadLicense();
			}
			else if (IDC_FWDESKTOP_LIC_APPLY   == wCtrlId)
			{
				CApplicationCursor wc_;

				const UINT ctrlId[] = {
					IDC_FWDESKTOP_LIC_NEW_0,
					IDC_FWDESKTOP_LIC_NEW_1,
					IDC_FWDESKTOP_LIC_NEW_2,
					IDC_FWDESKTOP_LIC_NEW_3,
				};
				CAtlString cs_serial;
				for (INT i_ = 0; i_ < _countof(ctrlId); i_++)
				{
					::WTL::CEdit edt_ = m_page_ref.GetDlgItem(ctrlId[i_]);
					if (!edt_)
						continue;
					CAtlString cs_block;
					edt_.GetWindowText(cs_block);
					cs_serial += cs_block;
				}
				cs_serial.Trim();
				if (cs_serial.GetLength() != CLicenseInfo::eSerialNumberLen)
				{
					global::SetErrorMessage(
							_T("New serial number is invalid")
						);
					return bHandled;
				}

				global::SetWaitMessage(
						_T("Applying new serial number...")
					);

				CAtlString cs_desc;
				::WTL::CEdit edt_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_LIC_DESC);
				if (edt_)
					edt_.GetWindowText(cs_desc);

				CLicenseInfo lic_;
				lic_.Serial(cs_serial);
				lic_.Description(cs_desc);

				CLicenseProvider prov_;
				HRESULT hr_ = prov_.RegisterLicense(lic_);
				if (FAILED(hr_))
					global::SetErrorMessage(
							prov_.Error()
						);
				else
				{
					CTabPageLic_Init init_(m_page_ref);
					init_.LoadLicense();

					global::SetInfoMessage(
							_T("New serial number has been applied successfully")
						);
				}
			}
			else if (IDC_FWDESKTOP_LIC_V_BTN   == wCtrlId)
			{
				CWindow ctrl_ = m_page_ref.GetDlgItem(wCtrlId);

				CProductUpdater updater_;
				HRESULT hr_ = updater_.StartUpdatingProcess();
				if (FAILED(hr_))
					global::SetErrorMessage(
							updater_.Error()
						);
			}
			else
				bHandled = FALSE;

			return  bHandled;
		}
	};

}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageLic::CTabPageLic(::WTL::CTabCtrl& tab_ref):
	TBasePage(IDD_FWDESKTOP_LIC_PAGE, tab_ref, *this)
{
}

CTabPageLic::~CTabPageLic(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageLic::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			global::SetRedirectView(CTabView::eLicense, true);
			details::CTabPageLic_Layout layout_(*this);
			layout_.AdjustCtrls();

			details::CTabPageLic_Init init_(*this);
			init_.InitCtrls();
			init_.LoadLicense();

			global::SetRedirectView(CTabView::eLicense, false);
			TBasePage::m_bInitilized = true;
			bHandled = TRUE;
		} break;
	case WM_DESTROY:
		{
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			details::CTabPageLic_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString CTabPageLic::GetPageTitle(void) const
{
	CAtlString cs_title(_T("License Info"));
	return cs_title;
}