#ifndef _FWDESKTOPTABPAGESVC_H_96BF34B0_95FE_443c_B42D_B37D24201319_INCLUDED
#define _FWDESKTOPTABPAGESVC_H_96BF34B0_95FE_443c_B42D_B37D24201319_INCLUDED
/*
	Created by Tech_dog (VToropov) on 25-Jan-2016 at 12:48:11pm, GMT+7, Phuket, Rawai, Monday;
	This is File Watcher Service Management Tab Page class declaration file.
*/
#include "FW_desktop_UI_Defs.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	class CTabPageSvc:
		public  CTabPageBase, 
		public  ITabPageCallback
	{
		typedef CTabPageBase   TBasePage;
	private:
		::WTL::CListViewCtrl   m_list;
	public:
		CTabPageSvc(::WTL::CTabCtrl&);
		~CTabPageSvc(void);
	private: // ITabPageCallback
		virtual LRESULT        TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
	public:  // TBasePage
		virtual CAtlString     GetPageTitle(void) const override sealed;
	};
}}}}

#endif/*_FWDESKTOPTABPAGESVC_H_96BF34B0_95FE_443c_B42D_B37D24201319_INCLUDED*/