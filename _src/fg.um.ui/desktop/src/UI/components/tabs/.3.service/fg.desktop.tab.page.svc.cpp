/*
	Created by Tech_dog (VToropov) on 25-Jan-2016 at 12:51:25pm, GMT+7, Phuket, Rawai, Monday;
	This is File Watcher Service Management Tab Page class implementation file.
*/
#include "StdAfx.h"
#include "fg.desktop.tab.page.svc.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI;
using namespace fw::desktop::UI::components;

#include "FG_Svc_Registry.h"

using namespace fw::desktop::data;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui;
using namespace ex_ui::draw::common;

#include "Shared_SystemService.h"

using namespace shared::service;

#include "FG_Service_Manager.h"
#include "FG_Service_Settings.h"

using namespace fg::common;
using namespace fg::common::data;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"
#include "Shared_FS_CommonDefs.h"

using namespace shared::user32;
using namespace shared::registry;
using namespace shared::ntfs;

#include "Shared_FS_GenericFolder.h"
#include "Shared_FS_Browser.h"

using namespace shared::ntfs;

#include "fg.desktop.tab.set.h"
#include "FG_desktop_SvcCtrls.h"

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components { namespace details
{
	class CTabPageSvc_Layout
	{
	private:
		RECT           m_client_area;
		const CWindow& m_page_ref;
	public:
		CTabPageSvc_Layout(const CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		VOID    AdjustCtrls(void)
		{
			HBITMAP hBitmap = NULL;
			HRESULT hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_SERVICE_LOGO,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_LOGO);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}
#if 0
			hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_SERVICE_WATCH,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_WATCH);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}
#endif
			hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_MAIN_DLG_WARN16,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_INFO);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}
			const SIZE sz_ = {-1, +1};
			AlignCtrlsByHeight(
					m_page_ref,
					IDC_FWDESKTOP_SERVICE_SETUP,
					IDC_FWDESKTOP_SERVICE_INSTL,
					sz_
				);
			AlignCtrlsByHeight(
					m_page_ref,
					IDC_FWDESKTOP_SERVICE_STATE,
					IDC_FWDESKTOP_SERVICE_CTRL,
					sz_
				);
			AlignCtrlsByHeight(
					m_page_ref,
					IDC_FWDESKTOP_SERVICE_STATE,
					IDC_FWDESKTOP_SERVICE_UPDT,
					sz_
				);
			AlignCtrlsByHeight(
					m_page_ref,
					IDC_FWDESKTOP_SERVICE_FOLDER,
					IDC_FWDESKTOP_SERVICE_BROWSE,
					sz_
				);
		}

		RECT    GetListRect(void)
		{
			CWindowEx host_ = m_page_ref;
			INT top_ = 0;
			INT low_ = 0;
			{
				const RECT rc_ = host_.GetDlgItemRect(IDC_FWDESKTOP_SERVICE_WFADD);
				top_ = rc_.top - 5;
			}
			{
				const RECT rc_ = host_.GetDlgItemRect(IDC_FWDESKTOP_SERVICE_REMDEV);
				low_ = rc_.top - 5;
			}
			INT right_ = 0;
			{
				const RECT rc_ = host_.GetDlgItemRect(IDC_FWDESKTOP_SERVICE_FOLDER);
				right_ = rc_.right;
			}
			const RECT rc_ = {
					 80   ,
					top_  ,
					right_,
					low_
				};

			return rc_;
		}
	};

	class CTabPageSvc_Init
	{
	private:
		const CWindow& m_page_ref;
	public:
		CTabPageSvc_Init(const CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		VOID     InitCtrls(void)
		{
			CEdit edt_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_FOLDER);
			if (edt_)
				edt_.SetCueBannerText(_T("Type folder path or press [Browse] button"), TRUE);
		}

		VOID     LoadPersistState(void)
		{
			WTL::CButton btn_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_STAT);
			if (btn_)
			{
				CService_RegMain reg_;
				btn_.SetCheck(static_cast<INT>(reg_.StateAutoCheck()));
			}
			btn_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_REMDEV);
			if (btn_)
			{
				CServiceSettingPersBase cfg_;
				cfg_.Load();
				btn_.SetCheck(static_cast<INT>(cfg_.TrackRemoval()));
			}
		}

		VOID     SavePersistState(void)
		{
			// auto-check service status
			WTL::CButton btn_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_STAT);
			if (btn_)
			{
				const bool bAutoCheck = !!btn_.GetCheck();
				CService_RegMain reg_;
				btn_.SetCheck(bAutoCheck);
			}
		}
	};

	INT&  CTabPageSvc_LastListIndex(void)
	{
		static INT nIndex = 0;
		return nIndex;
	}

	class CTabPageSvc_List
	{
	private:
		struct _lst_col {
			INT     _width;
			LPTSTR  _title;
		};
	private:
		CWindow     m_list;
		CWindow&    m_list_ref;
	public:
		CTabPageSvc_List(CWindow& _parent) : m_list_ref(m_list)
		{
			m_list = _parent.GetDlgItem(IDC_FWDESKTOP_SERVICE_WLIST);
		}
		CTabPageSvc_List(::WTL::CListViewCtrl& _list) : m_list_ref(_list){m_list = m_list_ref;}
	public:
		VOID        AddRow(const CAtlString& _folder, const bool bExcluded, const bool bSelect = false)
		{
			const INT nRow = ListView_GetItemCount(m_list_ref);

			CAtlString cs_buffer = _folder;

			LVITEM item_   = {0};
			item_.mask     = LVIF_TEXT;
			item_.iItem    = nRow;
			item_.pszText  = cs_buffer.GetBuffer();

			ListView_InsertItem(m_list_ref, &item_);

			cs_buffer = (bExcluded ? _T("Excluded") : _T("Watched"));

			ListView_SetItemText(m_list_ref, nRow, 1, cs_buffer.GetBuffer());

			if (bSelect)
			{
				ListView_SetItemState (m_list_ref,   -1,             0, LVIS_SELECTED);
				ListView_SetItemState (m_list_ref, nRow, LVIS_SELECTED, LVIS_SELECTED);
				ListView_EnsureVisible(m_list_ref, nRow, FALSE);
				m_list_ref.SetFocus();

				CTabPageSvc_LastListIndex() = nRow;
			}
		}

		HRESULT     Create(CWindow& _parent)const
		{
			if (m_list_ref.IsWindow())
				return (HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS));

			CTabPageSvc_Layout layout_(_parent);

			RECT rc_ = layout_.GetListRect();
			m_list_ref.Create(
					_T("SysListView32"),
					_parent,
					rc_,
					NULL,
					WS_CHILD|WS_VISIBLE|LVS_REPORT|LVS_SHOWSELALWAYS,
					WS_EX_CLIENTEDGE,
					IDC_FWDESKTOP_SERVICE_WLIST
				);
			if (!m_list_ref.IsWindow())
				return (HRESULT_FROM_WIN32(::GetLastError()));

			const DWORD ext_style_ = LVS_EX_FULLROWSELECT
			                        |LVS_EX_LABELTIP;

			ListView_SetExtendedListViewStyle((HWND)m_list_ref, ext_style_);

			const HFONT hFont = _parent.GetFont();
			m_list_ref.SetFont(hFont);
			{
				CWindow header_ = ListView_GetHeader(m_list_ref);
				if (header_)
					header_.SetFont(hFont);
			}

			CTabPageSvc_List::_lst_col cols_[] = {
					{420, _T("Folder Path")},
					{ 80, _T("Status"     )},
				};

			LVCOLUMN lvc ={0};
			lvc.mask     = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
			lvc.fmt      = LVCFMT_LEFT;
			lvc.pszText  = cols_[0]._title;
			lvc.cx       = cols_[0]._width;
			lvc.iSubItem = 0;

			ListView_InsertColumn(m_list_ref, 0, &lvc);

			for (INT i_  = 1; i_ < _countof(cols_); i_++)
			{
				lvc.pszText  = cols_[i_]._title;
				lvc.cx       = cols_[i_]._width;
				lvc.iSubItem = i_;

				ListView_InsertColumn(m_list_ref, i_, &lvc);
			}
			ListView_SetExtendedListViewStyle(m_list_ref, LVS_EX_FULLROWSELECT);
			return S_OK;
		}

		INT         GetSelectedIndex(void)const
		{
			::WTL::CListViewCtrl ctrl_ = m_list_ref;
			const INT nSelected = ctrl_.GetNextItem(-1, LVNI_SELECTED);
			return nSelected;
		}

		HRESULT     GetSelectedStatus(CWatchedType::_e& _type, CAtlString& _folder)const
		{
			const INT nSelected = this->GetSelectedIndex();
			if (nSelected < 0)
				return DISP_E_BADINDEX;

			::WTL::CListViewCtrl ctrl_ = m_list_ref;
			CAtlString cs_status;
			ctrl_.GetItemText(nSelected, 1, cs_status);

			_type = (0 == cs_status.CompareNoCase(_T("Excluded")) ? CWatchedType::eExcluded : CWatchedType::eIncluded);

			ctrl_.GetItemText(nSelected, 0, cs_status);
			_folder = cs_status;

			return S_OK;
		}

		HRESULT     Update(void)
		{
			::WTL::CListViewCtrl ctrl_ = m_list_ref;
				if (ctrl_.GetItemCount())
					ctrl_.DeleteAllItems();

			const CServiceSettingPersistent& pers_ = GetServiceSettingsRef();
			HRESULT hr_ = pers_.Error();
			if (FAILED(hr_))
			{
				global::SetErrorMessage(pers_.Error());
				return hr_;
			}
			{
				TFolderList folders_ = pers_.IncludedFolders();

				for (size_t i_ = 0; i_ < folders_.size(); i_++)
				{
					const bool bSelected = (
							CTabPageSvc_LastListIndex() == static_cast<INT>(i_)
						);
					this->AddRow(
							folders_[i_],
							false,
							bSelected
						);
				}
			}
			{
				TFolderList folders_ = pers_.ExcludedFolders();

				for (size_t i_ = 0; i_ < folders_.size(); i_++)
				{
					const bool bSelected = (
							CTabPageSvc_LastListIndex() == static_cast<INT>(i_)
						);
					this->AddRow(
							folders_[i_],
							true,
							bSelected
						);
				}
			}
			return  hr_;
		}
	};

	class CTabPageSvc_Handler
	{
	private:
		CWindow&    m_page_ref;
	public:
		CTabPageSvc_Handler(CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		BOOL        OnCommand(const WORD wCtrlId, const WORD wNotifyCode)
		{
			BOOL bHandled = TRUE;  wNotifyCode;

			if (false){}
			else if (IDC_FWDESKTOP_SERVICE_BROWSE == wCtrlId)
			{
				CAtlString cs_path = this->_SelectPath(_T("Choose a folder for adding to or excluding from watch"));
				if (!cs_path.IsEmpty())
				{
					CEdit edt_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_FOLDER);
					if (edt_)
						edt_.SetWindowText(cs_path);
				}
			}
			else if (IDC_FWDESKTOP_SERVICE_WFADD  == wCtrlId)
			{
				CAtlString cs_path = _GetPathFromEdit();
				if (!cs_path.IsEmpty())
				{
					CServiceFolderComparator comparator_(
							GetServiceSettingsRef().IncludedFolders(), GetServiceSettingsRef().ExcludedFolders()
						);

					if (FAILED(comparator_.CheckIncluded(cs_path)))
						global::SetErrorMessage(comparator_.Error().GetDescription());
					else {

						HRESULT hr_ = GetServiceSettingsRef().AppendFolder(cs_path, CWatchedType::eIncluded);
						if (FAILED(hr_))
							global::SetErrorMessage(GetServiceSettingsRef().Error());
						else
						{
							CTabPageSvc_List list_(m_page_ref);
							list_.Update();
						}
					}
				}
				else
					global::SetWarnMessage(_T("No valid watch folder is specified"));
			}
			else if (IDC_FWDESKTOP_SERVICE_WFEXC  == wCtrlId)
			{
				CAtlString cs_path = _GetPathFromEdit();
				if (!cs_path.IsEmpty())
				{
					CServiceFolderComparator comparator_(
							GetServiceSettingsRef().IncludedFolders(), GetServiceSettingsRef().ExcludedFolders()
						);

					if (FAILED(comparator_.CheckExcluded(cs_path)))
						global::SetErrorMessage(comparator_.Error().GetDescription());
					else {

						HRESULT hr_ = GetServiceSettingsRef().AppendFolder(cs_path, CWatchedType::eExcluded);
						if (FAILED(hr_))
							global::SetErrorMessage(GetServiceSettingsRef().Error());
						else
						{
							CTabPageSvc_List list_(m_page_ref);
							list_.Update();
						}
					}
				}
				else
					global::SetWarnMessage(_T("No valid exclude folder is specified"));
			}
			else if (IDC_FWDESKTOP_SERVICE_WFDEL  == wCtrlId)
			{
				CTabPageSvc_List list_(m_page_ref);
				const INT nSelected = list_.GetSelectedIndex();
				if (nSelected < 0)
					global::SetWarnMessage(_T("No folder is selected"));
				else
				{
					CTabPageSvc_LastListIndex() = nSelected;

					CWatchedType::_e type_ = CWatchedType::eIncluded;
					CAtlString cs_path;

					HRESULT hr_ = list_.GetSelectedStatus(type_, cs_path);
					if (FAILED(hr_))
					{
						global::SetWarnMessage(_T("No folder is selected to delete"));
						return bHandled;
					}

					hr_ = GetServiceSettingsRef().RemoveFolder(cs_path, type_);
					if (FAILED(hr_))
						global::SetErrorMessage(
							GetServiceSettingsRef().Error()
						);
					else
					{
						list_.Update();
					}
				}
			}
			else if (IDC_FWDESKTOP_SERVICE_RFRSH  == wCtrlId)
			{
				CTabPageSvc_List list_(m_page_ref);
				const INT nSelected = list_.GetSelectedIndex();
				if (nSelected > -1)
					CTabPageSvc_LastListIndex() = nSelected;
				list_.Update();
			}
			else
				bHandled = FALSE;

			return  bHandled;
		}
	private:
		CAtlString _GetPathFromEdit(void)
		{
			CAtlString cs_path;
			CEdit edt_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SERVICE_FOLDER);
			if (edt_)
				edt_.GetWindowText(cs_path);

			if (cs_path.IsEmpty() == false)cs_path.Trim();
			if (cs_path.IsEmpty())
				global::SetErrorMessage(
						_T("Type or browse for the folder path")
					);
			else
			{
				shared::ntfs::CGenericFolder folder_(cs_path);
				if (!folder_.IsValid())
				{
					global::SetErrorMessage(
						_T("The specified path does not exist or is not a folder.")
					);
					cs_path.Empty();
				}
			}
			return cs_path;
		}

		CAtlString _SelectPath(LPCTSTR pszTitle)
		{
			CAtlString cs_path;
			
			CService_RegBase reg_(_T(""));
			cs_path = reg_.LastFolderPath();

			if (cs_path.IsEmpty())
			{
				CApplication& the_app = GetAppObjectRef();
				the_app.GetPath(cs_path);
			}

			CFolderBrowser dlg_(m_page_ref.GetTopLevelParent(), pszTitle);
			HRESULT hr_ = dlg_.SelectFolder(cs_path, cs_path);
			if (FAILED(hr_))
			{
				cs_path.Empty();
				global::SetErrorMessage( dlg_.Error() );
			}
			else if (S_FALSE == hr_)
				cs_path.Empty();
			else
				reg_.LastFolderPath(cs_path);
			return cs_path;
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageSvc::CTabPageSvc(::WTL::CTabCtrl& tab_ref):
	TBasePage(IDD_FWDESKTOP_SERVICE_PAGE, tab_ref, *this)
{
}

CTabPageSvc::~CTabPageSvc(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageSvc::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_TIMER:
		{
#if 0
			details::CTabPageSvc_Init init_(*this);
			init_.InitCtrls();
#endif
			CTabPageSvcCtrls svc_ctrls(*this);
			svc_ctrls.Initialize();

			bHandled = TRUE;
		} break;
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			TBasePage::m_bInitilized = true;
			global::SetRedirectView(CTabView::eService, true);

			HRESULT hr_ = GetServiceSettingsRef().Load();
			if (FAILED(hr_))
				global::SetErrorMessage(GetServiceSettingsRef().Error());

			details::CTabPageSvc_Layout layout_(*this);

			layout_.AdjustCtrls();

			CTabPageSvcCtrls svc_ctrls(*this);
			svc_ctrls.Initialize();

			details::CTabPageSvc_Init init_(*this);
			init_.InitCtrls();
			init_.LoadPersistState();

			details::CTabPageSvc_List list_(m_list);
			list_.Create(*this);
			list_.Update();

			CTabPageSvcTimer& timer_ = CTabPageSvcTimer::GetTimer();
			timer_.Owner(*this);
			timer_.ManageByCtrl(IDC_FWDESKTOP_SERVICE_STAT);

			global::SetRedirectView(CTabView::eService, false);
			bHandled = TRUE;
		} break;
	case WM_DESTROY:
		{
			CTabPageSvcTimer& timer_ = CTabPageSvcTimer::GetTimer();
			timer_.Destroy();
			timer_.Owner(NULL);

			if (m_list)
				m_list.SendMessage(WM_CLOSE);
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			CTabPageSvcCtrls svc_ctrls(*this);
			bHandled = svc_ctrls.OnCommand(ctrlId, wNotify);

			details::CTabPageSvc_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString CTabPageSvc::GetPageTitle(void) const
{
	CAtlString cs_title(_T("Service Management"));
	return cs_title;
}