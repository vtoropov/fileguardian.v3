/*
	Created by Tech_dog (VToropov) on 15-Jan-2016 at 12:12:37pm, GMT+7, Phuket, Rawai, Friday;
	This is File Watcher desktop setup tab page class implementation file.
*/
#include "StdAfx.h"
#include "fg.desktop.tab.page.tune.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI;
using namespace fw::desktop::UI::components;

#include "FW_pass_Provider.h"

using namespace fw::desktop::data;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui;
using namespace ex_ui::draw::common;

#include "FG_Service_WebCfg.h"
#include "FG_Generic_Launcher.h"
#include "FG_Generic_UI.h"

using namespace fg::common;
using namespace fg::common::data;

#include "FG_Generic_Defs.h"

#include "fg.desktop.tab.set.h"
/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components { namespace details
{
	class CTabPageSetup_Layout
	{
	private:
		RECT           m_client_area;
		const CWindow& m_page_ref;
	public:
		CTabPageSetup_Layout(const CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		VOID    AdjustCtrls(void)
		{
			HBITMAP hBitmap = NULL;
			HRESULT hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_SETUP_PASS,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_LOCK);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}
			hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_MAIN_DLG_WARN16,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_WARN);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
			}
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_REST);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL; // note: the bitmap is used twice, so we delete it after the last usage!
			}

			hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_MAIN_DLG_INFO24,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_KINFO);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}
		}
	};

	class CTabPageSetup_Init
	{
	private:
		const CWindow& m_page_ref;
	public:
		CTabPageSetup_Init(const CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		VOID     ApplyHotKeySettings(void)
		{
			CUISharedCfgPersistent pers_;
			HRESULT hr_ = pers_.Load();
			if (FAILED(hr_))
			{
				global::SetErrorMessage(pers_.Error());
				return;
			}

			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_MODS);
			if (cbo_)
				cbo_.SetCurSel(this->_ModifiersToIndex(pers_.HotKeys().Modifiers()));

			cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_KEY);
			if (cbo_)
				cbo_.SetCurSel(this->_VirtualCodeToIndex(pers_.HotKeys().VirtualKey()));
		}

		VOID     ApplyWebSvcSettings(void)
		{
			CWebMonitorSettingPersistent pers_(true);
			HRESULT hr_ = pers_.Load();
			if (FAILED(hr_))
			{
				global::SetErrorMessage(pers_.Error());
				return;
			}
			WTL::CButton btn_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_REPO);
			if (btn_)
				btn_.SetCheck(static_cast<INT>(pers_.Enabled()));
		}

		VOID     InitCtrls(void)
		{
			const UINT ctrlIds[] = {
				IDC_FWDESKTOP_SETUP_PWD_OLD,
				IDC_FWDESKTOP_SETUP_PWD_NEW,
				IDC_FWDESKTOP_SETUP_PWD_CNF
			};
			for (INT i_ = 0; i_ < _countof(ctrlIds); i_++)
			{
				::WTL::CEdit ctrl_ = m_page_ref.GetDlgItem(ctrlIds[i_]);
				if (!ctrl_)
					continue;
				switch(i_)
				{
				case 0: ctrl_.SetCueBannerText(_T("Type old password"), TRUE); break;
				case 1: ctrl_.SetCueBannerText(_T("Type new password"), TRUE); break;
				case 2: ctrl_.SetCueBannerText(_T("Type new password again"), TRUE); break;
				};
			}
			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_MODS);
			if (cbo_)
			{
				cbo_.AddString(_T("Alt"));
				cbo_.AddString(_T("Alt + Ctrl"));
				cbo_.AddString(_T("Alt + Shift"));
				cbo_.AddString(_T("Alt + Shift + Ctrl"));
				cbo_.SetCurSel(CB_ERR);
				cbo_.SetCueBannerText(_T("Not selected"));
			}
			cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_KEY);
			if (cbo_)
			{
				CLauncherKeyEnum enum_;
				const INT count_ = enum_.Count();
				for ( INT i_ = 0; i_ < count_; i_++)
					cbo_.AddString(enum_.Item(i_).Name());

				cbo_.SetCurSel(CB_ERR);
				cbo_.SetCueBannerText(_T("Not selected"));
			}
		}

		bool     CheckHotKeys(void)
		{
			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_MODS);
			if (!cbo_)
				return false;
			if (CB_ERR == cbo_.GetCurSel())
			{
				global::SetErrorMessage(
						_T("Hot key modifiers are not selected")
					);
				return false;
			}
			cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_KEY);
			if (!cbo_)
				return false;
			if (CB_ERR == cbo_.GetCurSel())
			{
				global::SetErrorMessage(
						_T("Hot key is not specified")
					);
				return false;
			}
			return true;
		}

		VOID     UpdateHotKeySettings(void)
		{
			const UINT mods_ = this->_HotKeyToModifiers();
			const UINT vkey_ = this->_HotKeyToVirtualCode();

			CUISharedCfgPersistent pers_;

			pers_.HotKeys().Modifiers(mods_);
			pers_.HotKeys().VirtualKey(vkey_);

			HRESULT hr_ = pers_.Save();
			if (FAILED(hr_))
				global::SetErrorMessage(pers_.Error());
			else
			{
				CWindow agent_ = ::FindWindow(NULL, _FW_AGENT_WINDOW_TITLE);
				if (agent_)
					agent_.SendMessage(_FW_BROADCAST_MESSAGE_HOTKEY_ID);
				global::SetInfoMessage(_T("Hot keys have been saved successfully"));
			}
		}

		VOID     UpdateWebSvcSettings(void)
		{
			CWebMonitorSettingPersistent pers_(true);

			WTL::CButton btn_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_REPO);
			if (!btn_)
				return;

			const bool bEnabled = !!btn_.GetCheck();
			pers_.Enabled(bEnabled);

			HRESULT hr_ = pers_.Save();
			if (FAILED(hr_))
				global::SetErrorMessage(pers_.Error());
		}

	private:
		UINT    _HotKeyToModifiers(void)
		{
			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_MODS);
			if (!cbo_)
				return 0;
			switch (cbo_.GetCurSel())
			{
			case 0: /*Alt*/             return MOD_ALT;
			case 1: /*Alt+Ctrl*/        return MOD_ALT|MOD_CONTROL;
			case 2: /*Alt+Shift*/       return MOD_ALT|MOD_SHIFT;
			case 3: /*Alt+Shift+Ctrl*/  return MOD_ALT|MOD_SHIFT|MOD_CONTROL;
			}
			return 0;
		}

		UINT    _HotKeyToVirtualCode(void)
		{
			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_KEY);
			if (!cbo_)
				return 0;
			CLauncherKeyEnum enum_;
			const CLauncherKey& key_ = enum_.Item(cbo_.GetCurSel());
			return key_.Identifier();
		}

		INT     _ModifiersToIndex(const UINT _modifiers)
		{
			switch(_modifiers)
			{
			case MOD_ALT:                        return 0;
			case MOD_ALT|MOD_CONTROL:            return 1;
			case MOD_ALT|MOD_SHIFT:              return 2;
			case MOD_ALT|MOD_SHIFT|MOD_CONTROL:  return 3;
			}
			return CB_ERR;
		}

		INT     _VirtualCodeToIndex(const UINT _vkey)
		{
			CLauncherKeyEnum enum_;
			return enum_.Find(_vkey);
		}
	};

	class CTabPageSetup_Handler
	{
	private:
		const CWindow& m_page_ref;
	public:
		CTabPageSetup_Handler(const CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		BOOL     OnCommand(const WORD wCtrlId, const WORD wNotifyCode)
		{
			BOOL bHandled = TRUE; wNotifyCode;

			if (false){}
			else if (IDC_FWDESKTOP_SETUP_REPO    == wCtrlId)
			{
				CTabPageSetup_Init init_(m_page_ref);
				init_.UpdateWebSvcSettings();
			}
			else if (IDC_FWDESKTOP_SETUP_KSAVE   == wCtrlId)
			{
				CTabPageSetup_Init init_(m_page_ref);
				if (init_.CheckHotKeys())
					init_.UpdateHotKeySettings();
			}
			else if (IDC_FWDESKTOP_SETUP_PWD_SET == wCtrlId)
			{
				CPwdDataProvider prov_;
				// (1) checks the old password first, it can be empty if no password was set before;
				{
					CAtlString cs_old;
					::WTL::CEdit edt_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_PWD_OLD);
					if (edt_)
						edt_.GetWindowText(cs_old);
					HRESULT hr_ = prov_.CheckPassword(cs_old);
					if (FAILED(hr_))
					{
						global::SetErrorMessage(prov_.Error());
						return bHandled;
					}
				}
				// (2) checks new password and confirmation
				::WTL::CEdit edt_;
				::ATL::CAtlString pwd_;
				{
					CAtlString cs_new; edt_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_PWD_NEW); if (edt_)edt_.GetWindowText(cs_new);
					CAtlString cs_cfn; edt_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_SETUP_PWD_CNF); if (edt_)edt_.GetWindowText(cs_cfn);

					if (!cs_new.IsEmpty())cs_new.Trim();
					if (!cs_cfn.IsEmpty())cs_cfn.Trim();

					if (cs_new.GetLength() < 6)
					{
						CSysError err_;
						err_.SetState(
								(DWORD)ERROR_INVALID_DATA, _T("Invalid new password")
							);
						global::SetErrorMessage(err_);
						return bHandled;
					}
					if (0 != cs_new.Compare(cs_cfn))
					{
						CSysError err_;
						err_.SetState(
								(DWORD)ERROR_INVALID_DATA, _T("New password is not confirmed")
							);
						global::SetErrorMessage(err_);
						return bHandled;
					}
					pwd_ = cs_new;
				}
				// (3) encrypts and saves new password;
				{
					HRESULT hr_ = prov_.SetPassword(pwd_);
					if (FAILED(hr_))
						global::SetErrorMessage(prov_.Error());
					else
						global::SetInfoMessage(_T("New password has been successfully set."));
				}
			}
			else
				bHandled = FALSE;

			return  bHandled;
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageSetup::CTabPageSetup(::WTL::CTabCtrl& tab_ref):
	TBasePage(IDD_FWDESKTOP_SETUP_PAGE, tab_ref, *this)
{
}

CTabPageSetup::~CTabPageSetup(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageSetup::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			global::SetRedirectView(CTabView::eGenSetup, true);

			details::CTabPageSetup_Layout layout_(*this);
			layout_.AdjustCtrls();

			details::CTabPageSetup_Init init_(*this);
			init_.InitCtrls();
			init_.ApplyHotKeySettings();
			init_.ApplyWebSvcSettings();

			TBasePage::m_bInitilized = true;
			global::SetRedirectView(CTabView::eGenSetup, false);
			bHandled = TRUE;
		} break;
	case WM_DESTROY:
		{
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			details::CTabPageSetup_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString CTabPageSetup::GetPageTitle(void) const
{
	CAtlString cs_title(_T("General Settings"));
	return cs_title;
}