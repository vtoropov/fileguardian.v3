#ifndef _FWDESKTOPTABPAGESETUP_H_E1F24113_7076_4178_B8CC_81A051F90167_INCLUDED
#define _FWDESKTOPTABPAGESETUP_H_E1F24113_7076_4178_B8CC_81A051F90167_INCLUDED
/*
	Created by Tech_dog (VToropov) on 15-Jan-2016 at 12:10:09pm, GMT+7, Phuket, Rawai, Friday;
	This is File Watcher desktop setup tab page class declaration file.
*/
#include "FW_desktop_UI_Defs.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	class CTabPageSetup:
		public  CTabPageBase, 
		public  ITabPageCallback
	{
		typedef CTabPageBase   TBasePage;
	public:
		CTabPageSetup(::WTL::CTabCtrl&);
		~CTabPageSetup(void);
	private: // ITabPageCallback
		virtual LRESULT        TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
	public:  // TBasePage
		virtual CAtlString     GetPageTitle(void) const override sealed;
	};
}}}}

#endif/*_FWDESKTOPTABPAGESETUP_H_E1F24113_7076_4178_B8CC_81A051F90167_INCLUDED*/