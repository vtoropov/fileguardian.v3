#ifndef _FWDESKTOPTABPAGELOG_H_B875C6B5_9BA2_4480_9500_2B4DC6F076C2_INCLUDED
#define _FWDESKTOPTABPAGELOG_H_B875C6B5_9BA2_4480_9500_2B4DC6F076C2_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jan-2016 at 11:35:01am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian desktop log tab page interface declaration file.
*/
#include "FW_desktop_UI_Defs.h"
#include "FG_SqlData_Provider.h"
#include "FG_Lvw_DataProvider.h"
#include "FG_Prn_DataProvider.h"

#include "UIX_ListView_Ex.h"
#include "UIX_ListView_Ex_Footer.h"

#include "Shared_PrintView.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	using fg::common::data::local::CDbProvider;
	using fg::common::data::CFilterPersistent;

	using ex_ui::controls::IListView_Ex_Footer;
	using ex_ui::controls::CListViewCtrl_Ex;

	using fw::desktop::data::CLvwAdapter;
	using fw::desktop::data::CLvwDataProvider;
	using fw::desktop::data::CPrintDataProvider;

	using ex_ui::printing::IPrintDataProvider;

	class CTabPageLog:
		public  CTabPageBase,
		public  ITabPageCallback,
		public  IListView_Ex_Footer
	{
		typedef CTabPageBase   TBasePage;
	private:
		CLvwAdapter            m_lvw_adapter;
		CLvwDataProvider       m_lvw_data;
		CListViewCtrl_Ex       m_lvw_list;
		UINT_PTR               m_timer;
		CPrintDataProvider     m_prn_prov;
	public:
		CTabPageLog(::WTL::CTabCtrl&);
		~CTabPageLog(void);
	private: // ITabPageCallback
		virtual LRESULT    TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
	public:  // TBasePage
		virtual CAtlString GetPageTitle(void) const override sealed;
	private: // IListView_Ex_Footer
		virtual HRESULT    IListView_OnCtrlClick   (const INT nIndex)  override sealed;
		virtual HRESULT    IListView_OnCtrlDestroy (const INT nIndex)  override sealed;
		virtual HRESULT    IListView_OnLoadBefore  (const CAtlString&) override sealed;
		virtual HRESULT    IListView_OnLoadComplete(void) override sealed;
	protected: // ITabPageCmdHandler
		virtual BOOL       TabPage_OnCommand(const WORD wCmdId) override sealed;
	public:
		VOID    AdjustControls(void);
		bool    FooterVisible(void)const;
		IPrintDataProvider& PrintDataProvider(void);
	};
}}}}

#endif/*_FWDESKTOPTABPAGELOG_H_B875C6B5_9BA2_4480_9500_2B4DC6F076C2_INCLUDED*/