/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jan-2016 at 11:46:37am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian desktop application log tab page interface implementation file.
*/
#include "StdAfx.h"
#include "fg.desktop.tab.page.evt.h"
#include "FW_desktop_Resource.h"
#include "FW_desktop_PrintView.h"
#include "FW_filter_SetupDlg.h"
#include "FW_export_Dlg.h"
#include "FG_Opts_Dlg.h"
#include "FG_Prop_Dlg.h"

using namespace fw::desktop::UI;
using namespace fw::desktop::UI::components;
using namespace fw::desktop::UI::dialogs;
using namespace fw::desktop::data;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"
#include "UIX_ListView_Ex.h"
#include "UIX_MouseHandler.h"

using namespace ex_ui::draw;
using namespace ex_ui;
using namespace ex_ui::draw::common;
using namespace ex_ui::controls;
using namespace ex_ui::frames;

#include "FG_SysEvent_Model.h"
#include "FG_SysEvent_Filter.h"
#include "FG_ArcData_Manager.h"

using namespace fg::common::data;
using namespace fg::common::data::local;

#include "Shared_GenericAppObject.h"
#include "Shared_GenericAppResource.h"

using namespace shared::user32;

#include "fg.desktop.tab.set.h"
#include "FG_Log_Registry.h"

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components { namespace details
{
	class CTabPageLog_Layout
	{
		enum {
			eTableHeaderTop  = 0x40,
			eFooterHeight    = 0x28
		};
	private:
		RECT           m_client_area;
		const CWindow& m_page_ref;
	public:
		CTabPageLog_Layout(const CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		VOID     AdjustCtrls(void)
		{
			HBITMAP hBitmap = NULL;
			HRESULT hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_LOG_IMAGE,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_LOG_IMAGE);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}
			AlignComboHeightTo(
					m_page_ref,
					IDC_FWDESKTOP_LOG_ARCHES,
					24
				);
		}

		HRESULT  CreateTable(CListViewCtrl_Ex& _lvw_ctrl, CLvwDataProvider& _lvw_data)const
		{
			HRESULT hr_ = S_OK;

			hr_ = _lvw_ctrl.Attach(
					m_page_ref.GetDlgItem(IDC_FWDESKTOP_LOG_TABLE), IDC_FWDESKTOP_LOG_TABLE
				);
			if (FAILED(hr_))
				return hr_;

			CEventLog_Registry reg_;

			TListViewCols_Ex cols_ = _lvw_data.Columns();

			RECT rcList = this->_GetListRect(false);
			_lvw_ctrl.Control().MoveWindow(&rcList);

			CAtlString cs_url;
			hr_ = _lvw_data.FooterURL(cs_url);

			hr_ = _lvw_ctrl.Insert(cols_);
			hr_ = _lvw_ctrl.Footer().Create(m_page_ref, cs_url, this->_GetFootRect());

			_lvw_ctrl.Control().SetItemCountEx(CLvwDataProvider::ItemCount(_lvw_ctrl), LVSICF_NOSCROLL|LVSICF_NOINVALIDATEALL);
			_lvw_ctrl.Control().GetHeader().RedrawWindow();

			this->ShowFooter(_lvw_ctrl, reg_.ShowFooter());
			return hr_;
		}

		VOID     ShowFooter (CListViewCtrl_Ex& _lvw_ctrl, const bool _value)const
		{
			_lvw_ctrl.Footer().Visible(_value);
			const RECT rcList = this->_GetListRect(!_value);
			_lvw_ctrl.Control().MoveWindow(&rcList);
			_lvw_ctrl.Footer().Visible(_value);

			CEventLog_Registry reg_;
			reg_.ShowFooter(_value);
		}

	private:
		RECT    _GetFootRect(void)const
		{
			RECT rc_ = m_client_area;
			::InflateRect(&rc_, -3, -3);

			rc_.top  = rc_.bottom - CTabPageLog_Layout::eFooterHeight;
			return rc_;
		}

		RECT    _GetListRect(const bool bComplete)const
		{
			RECT rc_ = m_client_area;
			::InflateRect(&rc_, -3, -3);

			CWindow ctl_btn = m_page_ref.GetDlgItem(IDC_FWDESKTOP_LOG_FILTER);
			if (ctl_btn) {
				RECT rc_btn = {0};
				ctl_btn.GetWindowRect(&rc_btn); ::MapWindowPoints(HWND_DESKTOP, m_page_ref, (LPPOINT)&rc_btn, 0x2);
				rc_.top += rc_btn.bottom + 0x5; 
			}
			else
				rc_.top += CTabPageLog_Layout::eTableHeaderTop;

			if (bComplete == false)
				rc_.bottom -= CTabPageLog_Layout::eFooterHeight;
			return rc_;
		}
	};

	class CTabPageLog_Init
	{
	private:
		const CWindow&    m_page_ref;
		CLvwAdapter&      m_adapter;
		CLvwDataProvider& m_lvw_data;
	public:
		CTabPageLog_Init(const CWindow& page_ref, CLvwAdapter& _adapter, CLvwDataProvider& _lvw_data) :
		         m_page_ref(page_ref), m_adapter(_adapter), m_lvw_data(_lvw_data) {}
	public:
		INT      GetDataSourceListIndex(void)const
		{
			INT nSelected = 0;
			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_LOG_ARCHES);
			if (cbo_)
			{
				nSelected = cbo_.GetCurSel();
				if (nSelected == CB_ERR)
					nSelected  = 0;
			}
			return nSelected;
		}

		VOID     InitCtrls(void)
		{
			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_LOG_ARCHES);
			if (!cbo_)
				return;

			INT nSelected = cbo_.GetCurSel();

			cbo_.ResetContent();
			cbo_.AddString(_T("FG main database"));

			CArchiveSettingPersistent pers_(true);
			HRESULT hr_ = pers_.Load();
			if (!FAILED(hr_))
			{
				CArchiveList lst_(pers_);
				TFileList vols_ = lst_.GetList(true);
				for (size_t i_ = 0; i_ < vols_.size(); i_++)
					cbo_.AddString(vols_[i_]);
			}
			if (nSelected == CB_ERR)
				nSelected  = 0;
			if (nSelected  > cbo_.GetCount() - 1)
				nSelected  = cbo_.GetCount() - 1;

			cbo_.SetCurSel(nSelected);
		}

		VOID     InitDataSource(LPCTSTR lpszDbPath, CListViewCtrl_Ex& _lvw_ctrl)
		{
			CCtrlAutoState state_0(m_page_ref, IDC_FWDESKTOP_LOG_REFRESH, false);
			CCtrlAutoState state_1(m_page_ref, IDC_FWDESKTOP_LOG_FILTER , false);
			CCtrlAutoState state_2(m_page_ref, IDC_FWDESKTOP_LOG_PRINTV , false);
			CCtrlAutoState state_3(m_page_ref, IDC_FWDESKTOP_LOG_ARCHES , false);

			CApplicationCursor wc_(IDC_APPSTARTING);

			HRESULT hr_ = S_OK;

			CDbProvider& dat_prv = m_adapter.Provider();

			hr_ = dat_prv.Accessor().Close();
			hr_ = dat_prv.Accessor().Open(lpszDbPath);
			if (FAILED(hr_))
				this->UpdateStatus(dat_prv);
			else
			{
				CFilterPersistent& flt_ = m_adapter.Filter();
				flt_.Load();

				this->UpdateTable(_lvw_ctrl);
				this->UpdateStatus(flt_, dat_prv);
			}
		}

		VOID     UpdateStatus(const CDbProvider& _prov)
		{
			if (_prov.Accessor().Locator().Error())
			{
				global::SetErrorMessage( _prov.Accessor().Locator().Error());
				GetStatusBarObject().SetState(_T("Error"), CStatusColor::eRed);
			}
			else if (_prov.Accessor().Error())
			{
				global::SetErrorMessage( _prov.Accessor().Error());
				GetStatusBarObject().SetState(_T("Error"), CStatusColor::eRed);
			}
			if (_prov.Error())
			{
				global::SetErrorMessage( _prov.Error());
				GetStatusBarObject().SetState(_T("Error"), CStatusColor::eRed);
			}
		}

		VOID     UpdateStatus(const CFilter& _filter)
		{
			if (_filter.Enabled())
				GetStatusBarObject().SetState(_T("Filtered"), CStatusColor::eBlue);
			else
				GetStatusBarObject().SetState(_T("Connected"), CStatusColor::eGreen);
		}

		VOID     UpdateStatus(const CFilter& _filter, const CDbProvider& _prov)
		{
			if (_prov.Error())
				this->UpdateStatus(_prov);
			else
				this->UpdateStatus(_filter);
		}

		VOID     UpdateTable (CListViewCtrl_Ex& _lvw_list)
		{
			CApplicationCursor wc_(IDC_APPSTARTING);

			if (_lvw_list.Footer().Visible())
				m_lvw_data.Totals(_lvw_list.Footer().Control());

			if (!_lvw_list.IsValid())
				return;
			INT nTopIndex = -1;
			INT nSelIndex = -1;

			_lvw_list.Control().SetRedraw(FALSE);

			m_lvw_data.Update(_lvw_list);
			const INT nTotal = static_cast<INT>(
							m_adapter.Provider().Stats().RowCount(m_adapter.Filter())
						);

			const INT count_ = _lvw_list.Control().GetItemCount();
			if (count_)
			{
				nTopIndex = _lvw_list.Control().GetTopIndex();
				nSelIndex = _lvw_list.Control().GetSelectedIndex();
			}
			//
			// TODO: it is necessary to track record amount in order to determine correct row count for list view control;
			//
			_lvw_list.Control().SetItemCountEx(
					CLvwDataProvider::ItemCount(_lvw_list, nTotal), LVSICF_NOSCROLL|LVSICF_NOINVALIDATEALL
					);

			if (0 < count_ && nSelIndex < 0 && nSelIndex > count_ - 1)
			{
				ListView_SetItemState (_lvw_list.Control(), nSelIndex, LVIS_SELECTED, LVIS_SELECTED);
				ListView_EnsureVisible(_lvw_list.Control(), nSelIndex, FALSE);

				_lvw_list.Control().SetFocus();
			}
			_lvw_list.Control().SetRedraw(TRUE);
			_lvw_list.Redraw();

			GetStatusBarObject().SetRecords(
					nTotal
				);

		}
	};

	class CTabPageLog_Handler
	{
	private:
		const CWindow&     m_page_ref;
		CLvwAdapter&       m_adapter;
		CListViewCtrl_Ex&  m_lvw_ctrl;
		CLvwDataProvider&  m_lvw_data;
	public:
		CTabPageLog_Handler(const CWindow& page_ref, CLvwAdapter& _adapter, CListViewCtrl_Ex& _lvw_ctrl, CLvwDataProvider& _lvw_data):
			m_page_ref(page_ref),
			m_adapter (_adapter), m_lvw_ctrl(_lvw_ctrl), m_lvw_data(_lvw_data)
		{
		}
	public:
		BOOL     OnCommand(const WORD wCtrlId, const WORD wNotifyCode)
		{
			BOOL bHandled = TRUE; wNotifyCode;

			if (false){}
			else if (IDR_FGDESKTOP_NENU_ITM_PROPERS == wCtrlId) {

				CListViewCtrl lv_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_LOG_TABLE);
				const INT n_sel_ = lv_.GetSelectedIndex();
				if (n_sel_ == CB_ERR)
					global::SetInfoMessage(_T("No event is selected."));
				else {

					global::SetInfoMessage(NULL);
					const CLvwRecord rec_ = m_lvw_data.RowOf(n_sel_);

					CDataPropDlg props_(rec_, m_adapter.Provider());
					props_.DoModal();
				}
			}
			else if (IDC_FWDESKTOP_LOG_FILTER  == wCtrlId)
			{
				CCtrlAutoState state_0(m_page_ref, IDC_FWDESKTOP_LOG_REFRESH, false);
				CCtrlAutoState state_1(m_page_ref, IDC_FWDESKTOP_LOG_FILTER , false);
				CCtrlAutoState state_2(m_page_ref, IDC_FWDESKTOP_LOG_PRINTV , false);
				CCtrlAutoState state_3(m_page_ref, IDC_FWDESKTOP_LOG_ARCHES , false);

				CFilterPersistent& flt_ = m_adapter.Filter();
				flt_.Load();

				CDataFilterSetupDlg dlg_(flt_);
				HRESULT hr_ = dlg_.DoModal();
				if (FAILED(hr_))
				{
					global::SetErrorMessage( CSysError(hr_) );
					return bHandled;
				}
				else if (S_FALSE == hr_) // a user cancelled the dialog
					return bHandled;
				else if (ERROR_CANCELLED == hr_)
					flt_.Enabled(false);
				else
					flt_.Enabled(true);

				details::CTabPageLog_Init init_(m_page_ref, m_adapter, m_lvw_data);

				CDbProvider& dat_prv = m_adapter.Provider();

				init_.UpdateTable(m_lvw_ctrl);
				if (dat_prv.Error())
					init_.UpdateStatus(dat_prv);
				else
				{
					init_.UpdateStatus(flt_, dat_prv);
					flt_.Save();
				}
			}
			else if (IDC_FWDESKTOP_LOG_REFRESH == wCtrlId)
			{
				CCtrlAutoState state_0(m_page_ref, IDC_FWDESKTOP_LOG_REFRESH, false);
				CCtrlAutoState state_1(m_page_ref, IDC_FWDESKTOP_LOG_FILTER , false);
				CCtrlAutoState state_2(m_page_ref, IDC_FWDESKTOP_LOG_PRINTV , false);
				CCtrlAutoState state_3(m_page_ref, IDC_FWDESKTOP_LOG_ARCHES , false);

				details::CTabPageLog_Init init_(m_page_ref, m_adapter, m_lvw_data);

				CFilterPersistent& flt_ = m_adapter.Filter();
				flt_.Load();

				CDbProvider& dat_prv = m_adapter.Provider();

				init_.UpdateTable(m_lvw_ctrl);
				TErrorRef err_obj = dat_prv.Reader().Error();
				if (err_obj)
					global::SetErrorMessage(err_obj, true);
			}
			else if (IDC_FWDESKTOP_LOG_PRINTV  == wCtrlId)
			{
				CWindow main_ = m_page_ref.GetTopLevelWindow();
				main_.SendMessage(WM_PRINTVIEW);
			}
			else
				bHandled = FALSE;

			return  bHandled;
		}
	public:
		static
		WORD     MapCommandToCtrl(const WORD wCmdId)
		{
			switch (wCmdId)
			{
			case IDR_FWDESKTOP_MENU_ITM_PRINT  : return IDC_FWDESKTOP_LOG_PRINTV;
			case IDR_FWDESKTOP_MENU_ITM_FILTER : return IDC_FWDESKTOP_LOG_FILTER;
			case IDR_FWDESKTOP_MENU_ITM_REFRESH: return IDC_FWDESKTOP_LOG_REFRESH;
			case IDR_FWDESKTOP_MENU_ITM_EXPORT : return IDR_FWDESKTOP_MENU_ITM_EXPORT;
			case IDR_FGDESKTOP_MENU_ITM_TOTALS : return IDR_FGDESKTOP_MENU_ITM_TOTALS;
			case IDR_FGDESKTOP_MENU_ITM_OPTIONS: return IDR_FGDESKTOP_MENU_ITM_OPTIONS;
			case IDR_FGDESKTOP_NENU_ITM_PROPERS: return IDR_FGDESKTOP_NENU_ITM_PROPERS;
			}
			return 0;
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageLog::CTabPageLog(::WTL::CTabCtrl& tab_ref):
	TBasePage(IDD_FWDESKTOP_LOG_PAGE, tab_ref, *this), m_lvw_data(m_lvw_adapter), m_lvw_list(m_lvw_data, *this), m_timer(NULL), m_prn_prov(m_lvw_adapter, m_lvw_data)
{
}

CTabPageLog::~CTabPageLog(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageLog::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			TBasePage::m_bInitilized = true;
			global::SetRedirectView(CTabView::eEventLog, true);

			details::CTabPageLog_Layout layout_(*this);

			layout_.AdjustCtrls();
			layout_.CreateTable(m_lvw_list, m_lvw_data);

			details::CTabPageLog_Init init_(*this, m_lvw_adapter, m_lvw_data);
			init_.InitCtrls();
			init_.InitDataSource(NULL, m_lvw_list);
			// inits an update timer if necessary;
			{
				fw::desktop::data::CEventLog_Registry stg_;
				HRESULT hr_ = stg_.Update().Load();
				if (SUCCEEDED(hr_) && stg_.Update().Enabled()) {
					m_timer = this->SetTimer(1, stg_.Update().Period() * 1000);
				}
			}

			global::SetRedirectView(CTabView::eEventLog, false);

			CMouseEvtHandler::GetObjectRef().Subscribe(TBasePage::m_hWnd);
			bHandled = TRUE;
		} break;
	case WM_DESTROY:
		{
			CMouseEvtHandler::GetObjectRef().Unsubscribe(TBasePage::m_hWnd);
			m_lvw_adapter.Terminate();
			if (m_lvw_list.IsValid())
				m_lvw_list.Destroy();
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			details::CTabPageLog_Handler handler_(*this, m_lvw_adapter, m_lvw_list, m_lvw_data);
			//
			// TODO: all commands must be moved to command handler;
			//
			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
			if (bHandled == FALSE) {
				if (false){}
				else if (IDC_FWDESKTOP_LOG_ARCHES  == ctrlId) {
					if ( CBN_SELCHANGE == wNotify ) {
						details::CTabPageLog_Init init_(*this, m_lvw_adapter, m_lvw_data);
						INT nSelected = init_.GetDataSourceListIndex();
						if (nSelected == 0) { // main database
							init_.InitDataSource(NULL, m_lvw_list);
						}
						else {
							CArchiveSettingPersistent pers_(true);
							HRESULT hr_ = pers_.Load();
							if (!FAILED(hr_)) {
								CArchiveList list_(pers_);
								TFileList vols_ = list_.GetList(false);
								nSelected -= 1;
								if (nSelected > CB_ERR && nSelected < static_cast<INT>(vols_.size())) {
									CAtlString volume_ = vols_[nSelected];
									init_.InitDataSource(volume_, m_lvw_list);
					}}}}
					else if (CBN_DROPDOWN == wNotify) {
#if !defined(_DEBUG)
						//
						// this handler needs to re-initiate the list for taking into account new archives, which possible may
						// appear dynamically;
						// TODO: this use case must be re-viewed;
						//
						details::CTabPageLog_Init init_(*this, m_lvw_adapter, m_lvw_data);
						init_.InitCtrls();
#endif
					}
				}
				else if (IDC_FWDESKTOP_LOG_PRINTV  == ctrlId)
				{
					CWindow main_ = TBasePage::GetTopLevelWindow();
					main_.SendMessage(WM_PRINTVIEW);
				}
			}
		} break;

	case WM_NOTIFY:
		{
			const LPNMHDR p_header = reinterpret_cast<LPNMHDR>(lParam);
			if (NULL != p_header && NM_DBLCLK == p_header->code) {

				details::CTabPageLog_Handler handler_(*this, m_lvw_adapter, m_lvw_list, m_lvw_data);
				handler_.OnCommand(IDR_FGDESKTOP_NENU_ITM_PROPERS, 0);
			}
			else
			m_lvw_list.OnMessage(uMsg, wParam, lParam, bHandled);
		} break;
	case WM_MOUSEWHEEL_EX:
		{
			if (m_lvw_list.Control().m_hWnd == ::GetFocus())
				m_lvw_list.OnMessage(WM_MOUSEWHEEL, wParam, lParam, bHandled);
		} break;
	case WM_TIMER:
		{
			details::CTabPageLog_Init init_(*this, m_lvw_adapter, m_lvw_data);
			init_.UpdateTable(m_lvw_list);
			bHandled = TRUE;
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString CTabPageLog::GetPageTitle(void) const
{
	CAtlString cs_title(_T("File System Events"));
	return cs_title;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT CTabPageLog::IListView_OnCtrlClick  (const INT nIndex)
{
	nIndex;
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT CTabPageLog::IListView_OnCtrlDestroy(const INT nIndex)
{
	nIndex;
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT CTabPageLog::IListView_OnLoadBefore  (const CAtlString& _url)
{
	//
	// TODO: command identification must be made in listview data provider or/and commands collection;
	//
	const INT nIndex = m_lvw_adapter.Commands().IsCommand(_url);
	if (-1 != nIndex) {
		const CLvwCommand& cmd_ = m_lvw_adapter.Commands().Item(nIndex); cmd_;
	}
	else if (0==_url.CompareNoCase(_T("command:$rpt_page_options"))) {
		this->TabPage_OnCommand(IDR_FGDESKTOP_MENU_ITM_OPTIONS);
	}
	HRESULT hr_ = S_FALSE;
	return  hr_;
}

HRESULT CTabPageLog::IListView_OnLoadComplete(void)
{
	m_lvw_data.Totals(m_lvw_list.Footer().Control());

	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

BOOL    CTabPageLog::TabPage_OnCommand(const WORD wCmdId)
{
	const WORD ctrlId = details::CTabPageLog_Handler::MapCommandToCtrl(wCmdId);
	if (!ctrlId)
		return FALSE;

	BOOL bHandled = TRUE;

	switch (wCmdId)
	{
	case IDR_FWDESKTOP_MENU_ITM_EXPORT:
		{
			CWindow source_ = TBasePage::GetDlgItem(IDC_FWDESKTOP_LOG_TABLE);
			CAtlString target_;
			{
				TCHAR szFilter[_MAX_PATH] = _T("Comma Separated Values (*.csv)\0*.csv");
				const DWORD dwStyle = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_EXPLORER;

				WTL::CFileDialog dlg_(
						FALSE, _T("csv"), NULL, dwStyle, szFilter, *this
						);
				CAtlString cs_folder;
				CApplication& the_app = GetAppObjectRef();
				the_app.GetPath(cs_folder);

				dlg_.m_ofn.lpstrInitialDir = cs_folder;

				if (IDOK != dlg_.DoModal())
					return bHandled;
				else
					target_ = dlg_.m_ofn.lpstrFile;
			}

			CDataExportDlg dlg_(source_, target_);
			HRESULT hr_ = dlg_.DoModal();
			if (FAILED(hr_)) {
			}
		} break;
	case IDR_FGDESKTOP_MENU_ITM_TOTALS:
		{
			details::CTabPageLog_Layout layout_(*this);
			const bool bFooterVisible = m_lvw_list.Footer().Visible();
			layout_.ShowFooter(m_lvw_list, !bFooterVisible);
		} break;
	case IDR_FGDESKTOP_MENU_ITM_OPTIONS:
			{
				fw::desktop::data::CEventLog_Registry stg_;
				HRESULT hr_ = stg_.Update().Load();
				if (FAILED(hr_)) {
					global::SetErrorMessage(stg_.Update().Error());
					return 0;
				}

				CDataOptsDlg dlg_(stg_);
				hr_ = dlg_.DoModal();

				details::CTabPageLog_Init init_(*this, m_lvw_adapter, m_lvw_data);
				switch (hr_)
				{
				case OLE_E_PROMPTSAVECANCELLED:
					{
						if (NULL != m_timer) {
							::KillTimer(*this, m_timer); m_timer = NULL;
						}
					} break;
				case S_OK:
					{
						if (NULL != m_timer) {
							::KillTimer(*this, m_timer); m_timer = NULL;
						}
						if (stg_.Update().Enabled() && stg_.Update().Period() > 0)
							m_timer = this->SetTimer(1, stg_.Update().Period() * 1000);
					} break;
				default:
					m_lvw_list.Control().SetFocus();
				}
			} break;
	default:
		details::CTabPageLog_Handler handler_(*this, m_lvw_adapter, m_lvw_list, m_lvw_data);
		bHandled = handler_.OnCommand(ctrlId, 0);
	}
	return bHandled;
}

/////////////////////////////////////////////////////////////////////////////

VOID    CTabPageLog::AdjustControls(void)
{
	::WTL::CListViewCtrl table_ = this->GetDlgItem(IDC_FWDESKTOP_LOG_TABLE);
	if (!table_)
		return;

	RECT rcTable = {0};
	table_.GetWindowRect(&rcTable);

	::MapWindowPoints(HWND_DESKTOP, table_.GetParent(), (LPPOINT)&rcTable, 0x2);

	rcTable.right -= 0x2;
	table_.MoveWindow(&rcTable, FALSE);
}

bool    CTabPageLog::FooterVisible(void)const
{
	return m_lvw_list.Footer().Visible();
}

IPrintDataProvider&
        CTabPageLog::PrintDataProvider(void)
{
	return static_cast<IPrintDataProvider&>(m_prn_prov);
}