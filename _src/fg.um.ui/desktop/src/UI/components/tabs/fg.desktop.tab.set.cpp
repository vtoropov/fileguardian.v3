/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jan-2016 at 12:36:42pm, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian Desktop Main Form Tab Set class implementation file.
*/
#include "StdAfx.h"
#include "fg.desktop.tab.set.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI::components;

#include "Shared_Registry.h"

using namespace shared::registry;

#include "UIX_GdiProvider.h"

using namespace ex_ui;
using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components { namespace details
{
	class CTabSetMain_Layout
	{
	private:
		RECT        m_area;
	public:
		CTabSetMain_Layout(const RECT& rcArea) : m_area(rcArea)
		{
		}
	public:
		RECT        GetTabsArea(void)const
		{
			RECT rcTabs = m_area;
			::InflateRect(&rcTabs, -5, -3);
			return rcTabs;
		}
	};

	class CTabSetMain_Registry
	{
	private:
		mutable
		CRegistryStorage m_stg;
	public:
		CTabSetMain_Registry(void) : m_stg(HKEY_CURRENT_USER)
		{
		}
	public:
		INT     LastIndex(void)const
		{
			LONG lIndex = 0;
			m_stg.Load(
					this->_CommonRegFolder(),
					this->_LastIndexRegName(),
					lIndex
				);
			if (lIndex < 0)
				lIndex = 0;
			return static_cast<INT>(lIndex);
		}

		HRESULT LastIndex(const INT nIndex)
		{
			HRESULT hr_ = m_stg.Save(
					this->_CommonRegFolder(),
					this->_LastIndexRegName(),
					static_cast<LONG>(nIndex)
				);
			return  hr_;
		}
	private:
		LPCTSTR     _CommonRegFolder(void) const
		{
			static CAtlString cs_folder(_T("Common"));
			return cs_folder;
		}

		LPCTSTR     _LastIndexRegName(void) const
		{
			static CAtlString cs_name(_T("LastTabIndex"));
			return cs_name;
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabSetMain::CTabSetMain(void) :
	m_set(m_cTabCtrl), m_log(m_cTabCtrl), m_svc(m_cTabCtrl), m_man(m_cTabCtrl), m_lic(m_cTabCtrl), m_images(NULL)
{
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
	CGdiPlusPngLoader::CreateImages(
			IDR_FWDESKTOP_MAIN_DLG_WARN16_EXT,
			hInstance,
			1,
			m_images
		);
}

CTabSetMain::~CTabSetMain(void)
{
	if (m_images)
	{
		::ImageList_Destroy(m_images); m_images = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CTabSetMain::Create(const HWND hParent, const RECT& rcArea)
{
	if(!::IsWindow(hParent) || ::IsRectEmpty(&rcArea))
		return E_INVALIDARG;

	details::CTabSetMain_Layout layout(rcArea);
	RECT rcTabs = layout.GetTabsArea();

	m_cTabCtrl.Create(
			hParent,
			rcTabs,
			0,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			WS_EX_CONTROLPARENT,
			IDC_FWDESKTOP_MAIN_DLG_TABSET
		);

	const SIZE szPadding = {
			5, 2
		};

	m_cTabCtrl.SetFont(::ATL::CWindow(hParent).GetFont());
	m_cTabCtrl.SetImageList(m_images);
	m_cTabCtrl.SetPadding(szPadding);
	m_cTabCtrl.SetItemSize(128, 24);

	INT nIndex = 0;
	const DWORD dwFlags = SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW;

	CAtlString cs_page_title;
	cs_page_title.Format(
			_T(" %s "), m_log.GetPageTitle().GetString()
		);
	m_cTabCtrl.AddItem(cs_page_title); {
		m_log.Create(m_cTabCtrl.m_hWnd);
		m_log.SetWindowPos(
				HWND_TOP, 0, 0, 0, 0 , dwFlags
			);
		if (m_log.IsWindow()) {
			m_log.Index(nIndex++);

			RECT rcItem = {0};
			m_log.GetWindowRect(&rcItem);
			::MapWindowPoints(HWND_DESKTOP, m_cTabCtrl.m_hWnd, (LPPOINT)&rcItem, 0x2);

			rcItem.right -= 0x5;

			m_log.SetWindowPos(NULL, &rcItem, SWP_NOZORDER|SWP_NOACTIVATE);
			m_log.AdjustControls();
		}
	}
	cs_page_title.Format(
			_T(" %s "), m_set.GetPageTitle().GetString()
		);
	m_cTabCtrl.AddItem(cs_page_title);
	{
		m_set.Create(m_cTabCtrl.m_hWnd);
		m_set.SetWindowPos(
				HWND_TOP, 0, 0, 0, 0, dwFlags
			);
		if (m_set.IsWindow())m_set.Index(nIndex++);
	}
	cs_page_title.Format(
			_T(" %s "), m_svc.GetPageTitle().GetString()
		);
	m_cTabCtrl.AddItem(cs_page_title);
	{
		m_svc.Create(m_cTabCtrl.m_hWnd);
		m_svc.SetWindowPos(
				HWND_TOP, 0, 0, 0, 0, dwFlags
			);
		if (m_svc.IsWindow())m_svc.Index(nIndex++);
	}
	cs_page_title.Format(
			_T(" %s "), m_man.GetPageTitle().GetString()
		);
	m_cTabCtrl.AddItem(cs_page_title);
	{
		m_man.Create(m_cTabCtrl.m_hWnd);
		m_man.SetWindowPos(
				HWND_TOP,
				0, 0, 0, 0,
				dwFlags
			);
		if (m_man.IsWindow())m_man.Index(nIndex++);
	}
	cs_page_title.Format(
			_T(" %s "), m_lic.GetPageTitle().GetString()
		);
	m_cTabCtrl.AddItem(cs_page_title);
	{
		m_lic.Create(m_cTabCtrl.m_hWnd);
		m_lic.SetWindowPos(
				HWND_TOP,
				0, 0, 0, 0,
				dwFlags
			);
		if (m_lic.IsWindow())m_lic.Index(nIndex++);
	}

	details::CTabSetMain_Registry reg_;
	nIndex = reg_.LastIndex();
	
	m_cTabCtrl.SetCurSel(nIndex);
	this->UpdateLayout();

	m_cTabCtrl.SetPadding(szPadding);

	return S_OK;
}

HRESULT       CTabSetMain::Destroy(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();

	details::CTabSetMain_Registry reg_;
	reg_.LastIndex(nTabIndex);

	CWindow pages_[] = {
		m_log,
		m_set,
		m_svc,
		m_man,
		m_lic
	};

	for (INT i_ = 0; i_ < _countof(pages_); i_++)
	{
		pages_[i_].DestroyWindow();
		pages_[i_].m_hWnd = NULL;
	}
	return S_OK;
}

CTabPageLog&  CTabSetMain::GetTabOfLog(void) { return m_log; }

BOOL          CTabSetMain::OnCommand(const WORD wCmdId)
{
	BOOL bHandled = TRUE;

	switch (wCmdId)
	{
	case IDR_FWDESKTOP_MENU_ITM_VIEW_0: m_cTabCtrl.SetCurSel(m_log.Index()); this->UpdateLayout(); break;
	case IDR_FWDESKTOP_MENU_ITM_VIEW_1: m_cTabCtrl.SetCurSel(m_set.Index()); this->UpdateLayout(); break;
	case IDR_FWDESKTOP_MENU_ITM_VIEW_2: m_cTabCtrl.SetCurSel(m_svc.Index()); this->UpdateLayout(); break;
	case IDR_FWDESKTOP_MENU_ITM_VIEW_3: m_cTabCtrl.SetCurSel(m_man.Index()); this->UpdateLayout(); break;
	case IDR_FWDESKTOP_MENU_ITM_VIEW_4: m_cTabCtrl.SetCurSel(m_lic.Index()); this->UpdateLayout(); break;
	default:
		bHandled = FALSE;
	}

	if (!bHandled) bHandled = (static_cast<ITabPageCmdHandler&>(m_log)).TabPage_OnCommand(wCmdId);
	return bHandled;
}

IPrintDataProvider&
              CTabSetMain::PrintDataProvider(void)
{
	return m_log.PrintDataProvider();
}

CTabView      CTabSetMain::Selected(void)const
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();

	switch(nTabIndex)
	{
	case 1: return CTabView(CTabView::eGenSetup);
	case 2: return CTabView(CTabView::eService );
	case 3: return CTabView(CTabView::eDatabase);
	case 4: return CTabView(CTabView::eLicense );
	}
	return CTabView(CTabView::eEventLog);
}

VOID          CTabSetMain::SetErrorImage(const CTabView::ID _viewId, const bool bEnable)
{
	const INT nIndex = static_cast<INT>(_viewId);
	TCITEM item_ = {0};
	item_.mask   = TCIF_IMAGE;
	item_.iImage = (bEnable ? 0 : -1);
	m_cTabCtrl.SetItem(nIndex, &item_); 
}

VOID          CTabSetMain::Visible(const bool _visible)
{
	if (_visible)
		m_cTabCtrl.SetWindowPos(HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE|SWP_NOMOVE|SWP_SHOWWINDOW);
	else
		m_cTabCtrl.SetWindowPos(HWND_BOTTOM, 0, 0, 0, 0, SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW);
}

VOID          CTabSetMain::UpdateLayout(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();

	if (m_set.IsWindow())   m_set.ShowWindow  (nTabIndex == m_set.Index  () ? SW_SHOW : SW_HIDE);
	if (m_log.IsWindow())   m_log.ShowWindow  (nTabIndex == m_log.Index  () ? SW_SHOW : SW_HIDE);
	if (m_svc.IsWindow())   m_svc.ShowWindow  (nTabIndex == m_svc.Index  () ? SW_SHOW : SW_HIDE);
	if (m_man.IsWindow())   m_man.ShowWindow  (nTabIndex == m_man.Index  () ? SW_SHOW : SW_HIDE);
	if (m_lic.IsWindow())   m_lic.ShowWindow  (nTabIndex == m_lic.Index  () ? SW_SHOW : SW_HIDE);

	const CTabView view_ = this->Selected();
	global::PopupLastMessage((DWORD)view_.Identifier());
}

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components
{
	CTabSetMain&  GetMainTabSetObject(void)
	{
		static CTabSetMain tabs_;
		return tabs_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CTabView::CTabView(const CTabView::ID _id) : m_id(_id)
{
}

/////////////////////////////////////////////////////////////////////////////

CTabView::ID  CTabView::Identifier(void)const
{
	return m_id;
}