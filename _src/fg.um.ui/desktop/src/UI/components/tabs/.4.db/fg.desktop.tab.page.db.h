#ifndef _FWDESKTOPTABPAGEDB_H_D6901A9C_7043_4f28_B716_C4EE4E3EFCD5_INCLUDED
#define _FWDESKTOPTABPAGEDB_H_D6901A9C_7043_4f28_B716_C4EE4E3EFCD5_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Mar-2016 at 9:49:57pm, GMT+8, Hong Kong, Tuesday;
	This is File Watcher Database Management Tab Page class declaration file.
*/
#include "FW_desktop_UI_Defs.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	class CTabPageDbMan:
		public  CTabPageBase, 
		public  ITabPageCallback
	{
		typedef CTabPageBase   TBasePage;
	public:
		CTabPageDbMan(::WTL::CTabCtrl&);
		~CTabPageDbMan(void);
	private: // ITabPageCallback
		virtual LRESULT        TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
	public:  // TBasePage
		virtual CAtlString     GetPageTitle(void) const override sealed;
	};
}}}}

#endif/*_FWDESKTOPTABPAGEDB_H_D6901A9C_7043_4f28_B716_C4EE4E3EFCD5_INCLUDED*/