/*
	Created by Tech_dog (VToropov) on 22-Mar-2016 at 10:03:02pm, GMT+8, Hong Kong, Tuesday;
	This is File Watcher Database Management Tab Page class implementation file.
*/
#include "StdAfx.h"
#include "fg.desktop.tab.page.db.h"
#include "FW_desktop_Resource.h"

using namespace fw::desktop::UI;
using namespace fw::desktop::UI::components;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui;
using namespace ex_ui::draw::common;

#include "FG_ArcData_Manager.h"
#include "FG_SqlData_Model_ex.h"

using namespace fg::common::data;
using namespace fg::common::data::local;

#include "Shared_GenericAppObject.h"

using namespace shared::user32;

#include "Shared_FS_Browser.h"

using namespace shared::ntfs;

#include "fg.desktop.tab.set.h"
/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace components { namespace details
{
	class CTabPageDbMan_Init
	{
	private:
		const CWindow& m_page_ref;
	public:
		CTabPageDbMan_Init(const CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		CAtlString ArcFolder(void)
		{
			CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_ARCH_FOLDER);
			if (!ctrl_)
				return CAtlString();

			CAtlString folder_;
			ctrl_.GetWindowText(folder_);

			this->_unwind_path(folder_);
			return folder_;
		}

		VOID       ArcFolder(const CAtlString& _folder)
		{
			CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_ARCH_FOLDER);
			if (!ctrl_)
				return;
			ctrl_.SetWindowText(_folder);
		}

		CAtlString DbMainFolder(void)
		{
			CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_DBMAN_FOLDER);
			if (!ctrl_)
				return CAtlString();

			CAtlString folder_;
			ctrl_.GetWindowText(folder_);

			this->_unwind_path(folder_);
			return folder_;
		}

		VOID       DbMainFolder(const CAtlString& _folder)
		{
			CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_DBMAN_FOLDER);
			if (!ctrl_)
				return;
			ctrl_.SetWindowText(_folder);
		}

		bool       GetArcEnable(void)
		{
			::WTL::CButton chk_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_ARCH_ENBLD);
			if (!chk_)
				return false;
			else
				return !!chk_.GetCheck();
		}

		VOID       InitCtrls(void)
		{
			HRESULT hr_ = S_OK;
			{
				CArchiveSettingPersistent pers_(true);
				hr_ = pers_.Load();
				if (FAILED(hr_))
					global::SetErrorMessage(pers_.Error());
				this->InitCtrls(pers_);
			}
			{
				CDbSettingPersistent pers_(true);
				hr_ = pers_.Load();
				if (FAILED(hr_))
					global::SetErrorMessage(pers_.Error());
				this->InitCtrls(pers_);
			}
		}

		VOID       InitCtrls(const CArchiveSettings& _cfg)
		{
			const UINT ctrlId[] = {
				IDC_FWDESKTOP_ARCH_THRSH ,
				IDC_FWDESKTOP_ARCH_TRUNC ,
				IDC_FWDESKTOP_ARCH_ENBLD ,
				IDC_FWDESKTOP_ARCH_VOLMS ,
				IDC_FWDESKTOP_ARCH_FOLDER,
			};
			for (INT i_ = 0; i_ < _countof(ctrlId); i_++)
			{
				CWindow ctrl_ = m_page_ref.GetDlgItem(ctrlId[i_]);
				if (!ctrl_)
					continue;
				switch (i_)
				{
				case 0: {ctrl_.SetWindowText(_cfg.Threshold().ValueAsText());} break;
				case 1: {ctrl_.SetWindowText(_cfg.Truncated().ValueAsText());} break;
				case 2: {::WTL::CButton btn_ = ctrl_; btn_.SetCheck(static_cast<INT>(_cfg.Enabled()));} break;
				case 3: {ctrl_.SetWindowText(_cfg.VolumesAsText());          } break;
				case 4: {ctrl_.SetWindowText(_cfg.VolumeFolder());           } break;
				}
			}
		}

		VOID       InitCtrls(const CDbSettings& _cfg)
		{
			const UINT ctrlId[] = {
				IDC_FWDESKTOP_DBMAN_FNAME ,
				IDC_FWDESKTOP_DBMAN_FOLDER,
				IDC_FWDESKTOP_DBMAN_CREATE,
			};
			for (INT i_ = 0; i_ < _countof(ctrlId); i_++)
			{
				CWindow ctrl_ = m_page_ref.GetDlgItem(ctrlId[i_]);
				if (!ctrl_)
					continue;
				switch (i_)
				{
				case 0: {ctrl_.SetWindowText(_cfg.FileName());  } break;
				case 1: {ctrl_.SetWindowText(_cfg.FolderPath());} break;
				case 2:
					{
						CButton btn_ = ctrl_;
						btn_.SetCheck(static_cast<INT>(_cfg.CreateFolderOpt()));
					} break;
				}
			}
		}

		VOID       UpdateCtrlState(void)
		{
			bool bEnabled = this->GetArcEnable();

			const UINT ctrlId[] = {
				IDC_FWDESKTOP_ARCH_VOLMS ,
				IDC_FWDESKTOP_ARCH_FOLDER,
				IDC_FWDESKTOP_ARCH_BROWSE,
			};
			for (INT i_ = 0; i_ < _countof(ctrlId); i_++)
			{
				CWindow ctrl_ = m_page_ref.GetDlgItem(ctrlId[i_]);
				if (!ctrl_)
					continue;
				ctrl_.EnableWindow(static_cast<BOOL>(bEnabled));
			}
		}

		VOID       UpdateSettings(CArchiveSettings& _cfg)
		{
			const UINT ctrlId[] = {
				IDC_FWDESKTOP_ARCH_THRSH ,
				IDC_FWDESKTOP_ARCH_TRUNC ,
				IDC_FWDESKTOP_ARCH_ENBLD ,
				IDC_FWDESKTOP_ARCH_VOLMS ,
				IDC_FWDESKTOP_ARCH_FOLDER,
			};

			CAtlString cs_buffer;

			for (INT i_ = 0; i_ < _countof(ctrlId); i_++)
			{
				CWindow ctrl_ = m_page_ref.GetDlgItem(ctrlId[i_]);
				if (!ctrl_)
					continue;
				switch (i_)
				{
				case 0: { ctrl_.GetWindowText(cs_buffer); _cfg.Threshold().Value(cs_buffer); } break;
				case 1: { ctrl_.GetWindowText(cs_buffer); _cfg.Truncated().Value(cs_buffer); } break;
				case 2: {::WTL::CButton btn_ = ctrl_;     _cfg.Enabled(!!btn_.GetCheck());   } break;
				case 3: { ctrl_.GetWindowText(cs_buffer); _cfg.Volumes(cs_buffer);           } break;
				case 4: { ctrl_.GetWindowText(cs_buffer); _cfg.VolumeFolder(cs_buffer);      } break;
				}
			}
		}

		VOID       UpdateSettings(CDbSettings& _cfg)
		{
			const UINT ctrlId[] = {
				IDC_FWDESKTOP_DBMAN_FNAME ,
				IDC_FWDESKTOP_DBMAN_FOLDER,
				IDC_FWDESKTOP_DBMAN_CREATE,
			};
			CAtlString cs_buffer;

			for (INT i_ = 0; i_ < _countof(ctrlId); i_++)
			{
				CWindow ctrl_ = m_page_ref.GetDlgItem(ctrlId[i_]);
				if (!ctrl_)
					continue;
				switch (i_)
				{
				case 0: { ctrl_.GetWindowText(cs_buffer); _cfg.FileName(cs_buffer);   } break;
				case 1: { ctrl_.GetWindowText(cs_buffer); _cfg.FolderPath(cs_buffer); } break;
				case 2:
					{
						CButton btn_ = ctrl_; _cfg.CreateFolderOpt(!!btn_.GetCheck());
					} break;
				}
			}
		}

		HRESULT    ValidateData(void)
		{
			const bool bInitToDefaults = true;
			HRESULT hr_ = S_OK;
			{
				CArchiveSettings pers_(bInitToDefaults);
				this->UpdateSettings(pers_);

				hr_ = pers_.Validate();
				if (FAILED(hr_))
				{
					global::SetErrorMessage(pers_.Error().GetDescription());
					return hr_;
				}
			}
			{
				CDbSettings pers_(bInitToDefaults);
				this->UpdateSettings(pers_);

				hr_ = pers_.Validate();
				if (FAILED(hr_))
					global::SetErrorMessage(pers_.Error().GetDescription());
			}
			return  hr_;
		}
	private:
		VOID  _unwind_path(CAtlString& _path)
		{
			if (_path.IsEmpty())
			{
				CApplication& the_app = GetAppObjectRef();
				the_app.GetPath(_path);
			}
			else if (CApplication::IsRelatedToAppFolder(_path))
			{
				CApplication& the_app = GetAppObjectRef();
				CAtlString cs_pattern = _path;
				the_app.GetPathFromAppFolder(cs_pattern, _path);
			}
		}
	};

	class CTabPageDbMan_Handler
	{
	private:
		CWindow&    m_page_ref;
	public:
		CTabPageDbMan_Handler(CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		BOOL        OnCommand(const WORD wCtrlId, const WORD wNotifyCode)
		{
			BOOL bHandled = TRUE;  wNotifyCode;
			if (false){}
			else if (IDC_FWDESKTOP_ARCH_ENBLD   == wCtrlId)
			{
				CTabPageDbMan_Init init_(m_page_ref);
				init_.UpdateCtrlState();
			}
			else if (IDC_FWDESKTOP_ARCH_SETDEF  == wCtrlId)
			{
				CArchiveSettings arc_(true);

				CTabPageDbMan_Init init_(m_page_ref);
				init_.InitCtrls(arc_);
				init_.UpdateCtrlState();

				global::SetInfoMessage(
						_T("Archive options have been set to default values.")
					);
			}
			else if (IDC_FWDESKTOP_ARCH_BROWSE  == wCtrlId)
			{
				CTabPageDbMan_Init init_(m_page_ref);

				CAtlString cs_folder = init_.ArcFolder();

				CFolderBrowser dlg_(
							m_page_ref,
							_T("Choose a folder for database archive volumes")
						);

				HRESULT  hr_ = dlg_.SelectFolder(cs_folder, cs_folder);
				if (S_OK == hr_)
					init_.ArcFolder(cs_folder);
			}
			else if (IDC_FWDESKTOP_ARCH_APPLY   == wCtrlId)
			{
				CTabPageDbMan_Init init_(m_page_ref);
				HRESULT hr_ = init_.ValidateData();
				if (!FAILED(hr_))
				{
					CArchiveSettingPersistent pers_(true);
					init_.UpdateSettings(pers_);
					hr_ = pers_.Save();
					if (FAILED(hr_))
						global::SetErrorMessage(pers_.Error());
				}
				if (!FAILED(hr_))
				{
					CDbSettingPersistent pers_(true);
					init_.UpdateSettings(pers_);
					hr_ = pers_.Save();
					if (FAILED(hr_))
						global::SetErrorMessage(pers_.Error());
				}
				if (!FAILED(hr_))
					global::SetInfoMessage(
							_T("Archive and main database settings have been saved.")
						);
			}
			else if (IDC_FWDESKTOP_DBMAN_BROWSE == wCtrlId)
			{
				CTabPageDbMan_Init init_(m_page_ref);

				CAtlString cs_folder = init_.DbMainFolder();

				CFolderBrowser dlg_(
							m_page_ref,
							_T("Choose a folder for main database file")
						);

				HRESULT  hr_ = dlg_.SelectFolder(cs_folder, cs_folder);
				if (S_OK == hr_)
					init_.DbMainFolder(cs_folder);
			}
			else
				bHandled = FALSE;

			return  bHandled;
		}
	};

	class CTabPageDbMan_Layout
	{
	private:
		RECT           m_client_area;
		const CWindow& m_page_ref;
	public:
		CTabPageDbMan_Layout(const CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		VOID    AdjustCtrls(void)
		{
			HBITMAP hBitmap = NULL;
			HRESULT hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_FWDESKTOP_MAIN_DLG_WARN16,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_FWDESKTOP_ARCH_WARN);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}

			const SIZE sz_ = {-1, +1};
			AlignCtrlsByHeight(
					m_page_ref,
					IDC_FWDESKTOP_DBMAN_FOLDER,
					IDC_FWDESKTOP_DBMAN_BROWSE,
					sz_
				);

			AlignCtrlsByHeight(
					m_page_ref,
					IDC_FWDESKTOP_ARCH_FOLDER,
					IDC_FWDESKTOP_ARCH_BROWSE,
					sz_
				);
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageDbMan::CTabPageDbMan(::WTL::CTabCtrl& tab_ref):
	TBasePage(IDD_FWDESKTOP_DBMAN_PAGE, tab_ref, *this)
{
}

CTabPageDbMan::~CTabPageDbMan(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageDbMan::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			global::SetRedirectView(CTabView::eDatabase, true);

			details::CTabPageDbMan_Layout layout_(*this);
			layout_.AdjustCtrls();

			details::CTabPageDbMan_Init init_(*this);
			init_.InitCtrls();
			init_.UpdateCtrlState();

			global::SetRedirectView(CTabView::eDatabase, false);
			TBasePage::m_bInitilized = true;
			bHandled = TRUE;
		} break;
	case WM_DESTROY:
		{
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			details::CTabPageDbMan_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString CTabPageDbMan::GetPageTitle(void) const
{
	CAtlString cs_title(_T("Local Database"));
	return cs_title;
}