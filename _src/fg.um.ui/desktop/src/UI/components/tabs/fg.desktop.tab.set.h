#ifndef _FWDESKTOPMAINTABSET_H_CBE4D90A_5BEA_4324_B7CA_C92D7BEB1FC7_INCLUDED
#define _FWDESKTOPMAINTABSET_H_CBE4D90A_5BEA_4324_B7CA_C92D7BEB1FC7_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jan-2016 at 12:29:27pm, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian Desktop Main Form Tab Set interface declaration file.
*/
#include "fg.desktop.tab.page.evt.h"
#include "fg.desktop.tab.page.tune.h"
#include "fg.desktop.tab.page.svc.h"
#include "fg.desktop.tab.page.db.h"
#include "FW_desktop_PrintView.h"
#include "fg.desktop.tab.page.lic.h"

namespace fw { namespace desktop { namespace UI { namespace components
{
	using ex_ui::printing::IPrintDataProvider;

	class CTabView
	{
	public:
		enum ID{
			eEventLog  = 0x0, // default
			eGenSetup  = 0x1,
			eService   = 0x2,
			eDatabase  = 0x3,
			eLicense   = 0x4,
			ePrinting  = 0x5,
		};
	private:
		ID                  m_id;
	public:
		CTabView(const CTabView::ID);
	public:
		ID                  Identifier(void)const;
	};

	class CTabSetMain
	{
	private:
		::WTL::CTabCtrl     m_cTabCtrl;
		CTabPageLog         m_log;
		CTabPageSetup       m_set;
		CTabPageSvc         m_svc;
		CTabPageDbMan       m_man;
		CTabPageLic         m_lic;
		HIMAGELIST          m_images;
	public:
		CTabSetMain(void);
		~CTabSetMain(void);
	public:
		HRESULT             Create(const HWND hParent, const RECT& rcArea);
		HRESULT             Destroy(void);
		CTabPageLog&        GetTabOfLog(void);
		BOOL                OnCommand(const WORD wCmdId);
		IPrintDataProvider& PrintDataProvider(void);
		CTabView            Selected(void)const;
		VOID                SetErrorImage(const CTabView::ID, const bool bEnable);
		VOID                Visible(const bool);
		VOID                UpdateLayout(void);
	};

	CTabSetMain&            GetMainTabSetObject(void);
}}}}

#endif/*_FWDESKTOPMAINTABSET_H_CBE4D90A_5BEA_4324_B7CA_C92D7BEB1FC7_INCLUDED*/