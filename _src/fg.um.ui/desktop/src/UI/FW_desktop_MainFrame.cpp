/*
	Created by Tech_dog (VToropov) on 15-Jan-2016 at 12:54:14pm, GMT+7, Phuket, Rawai, Friday,
	This is File Watcher Desktop Main Frame class implementation file.
*/
#include "StdAfx.h"
#include "FW_desktop_MainFrame.h"
#include "FW_desktop_Resource.h"
#include "FW_about_Dlg.h"

using namespace fw::desktop::UI;
using namespace fw::desktop::UI::components;
using namespace fw::desktop::UI::dialogs;

#include "Shared_GenericAppObject.h"
#include "Shared_GenericAppResource.h"
using namespace shared::user32;

#include "FW_desktop_ChmHelper.h"
#include "FG_Generic_Defs.h"

#define _MODAL
/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace desktop { namespace UI { namespace details
{
	struct CMainFrame_AccelData
	{
		HACCEL    _accel;
		HWND      _dialog;
		HHOOK     _hook;

		CMainFrame_AccelData(void):_accel(NULL), _dialog(NULL), _hook(NULL){}
	};

	CMainFrame_AccelData& GetMainFrame_AccelData(void)
	{
		static CMainFrame_AccelData accel_;
		return accel_;
	}

	class CMainFrame_AccelHandler
	{
	public:
		CMainFrame_AccelHandler(void)
		{
		}
	public:

		HRESULT    Initialize(void)
		{
			CMainFrame_AccelData& acc_data = GetMainFrame_AccelData();

			if (acc_data._dialog == NULL)
				return OLE_E_INVALIDHWND;
			if (acc_data._hook)
				return HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED);

			HRESULT hr_ = S_OK;

			if (acc_data._accel == NULL)
			{
				acc_data._accel = ::LoadAccelerators(NULL, MAKEINTRESOURCEW(IDR_FWDESKTOP_MAIN_DLG_ACCEL));
				if (!acc_data._accel)
					hr_ = HRESULT_FROM_WIN32(::GetLastError());
			}

			acc_data._hook = ::SetWindowsHookEx(
					WH_GETMESSAGE,
					CMainFrame_AccelHandler::HookProc,
					NULL,
					::GetCurrentThreadId()
				);
			if (!acc_data._hook)
				hr_ = HRESULT_FROM_WIN32(::GetLastError());
			return  hr_;
		}

		HRESULT    Terminate(void)
		{
			CMainFrame_AccelData& acc_data = GetMainFrame_AccelData();
			if (acc_data._hook)
			{
				::UnhookWindowsHookEx(acc_data._hook);
				acc_data._hook = NULL;
			}

			if (acc_data._accel)
			{
				::DestroyAcceleratorTable(acc_data._accel);
				acc_data._accel = NULL;
			}
			HRESULT hr_ = S_OK;
			return  hr_;
		}
	private:
		static LRESULT CALLBACK HookProc(int nCode, WPARAM wParam, LPARAM lParam)
		{
			LRESULT nResult = 1;

			if(nCode == HC_ACTION && wParam == PM_REMOVE)
			{
				MSG* pMsg = reinterpret_cast<MSG*>(lParam);
				if ( pMsg )
				{
					if(pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
					{
						CMainFrame_AccelData& acc_data = GetMainFrame_AccelData();

						if ( acc_data._dialog == ::GetForegroundWindow())
						{
							HWND hwndActive = ::GetFocus(); // ignores editable controls
							if (DLGC_HASSETSEL & ~::SendMessage(hwndActive, WM_GETDLGCODE, 0, 0)
								|| ES_READONLY &  ::GetWindowLong(hwndActive, GWL_STYLE))
							{
								if (::TranslateAccelerator(acc_data._dialog, acc_data._accel, pMsg))
								{
									pMsg->message = WM_NULL;
									nResult = 0;
								}
							}
						}
					}
				}
			}
			if (nCode < 0 || nResult)
			{
				CMainFrame_AccelData& acc_data = GetMainFrame_AccelData();

				return ::CallNextHookEx(acc_data._hook, nCode, wParam, lParam);
			}
			else
				return nResult;
		}

	};

	CMainFrame_AccelHandler&  GetMainFrame_AccelHandler(void)
	{
		static CMainFrame_AccelHandler handler_;
		return handler_;
	}

	class CMainFrame_Layout
	{
	private:
		CWindow&      m_frame_ref;
		RECT          m_client_area;
	public:
		CMainFrame_Layout(CWindow& frame_ref) : m_frame_ref(frame_ref)
		{
			if (m_frame_ref)
				m_frame_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		VOID          RecalcPosition(void)
		{
			RECT rcWindow = {0};
			m_frame_ref.GetWindowRect(&rcWindow);
			const RECT rcScreen = CMainFrame_Layout::GetAvailableArea();
			const SIZE sz_delta = {(__W(rcWindow) - __W(rcScreen)), (__H(rcWindow) - __H(rcScreen))};
			if (0 < sz_delta.cx)
				rcWindow.right -= sz_delta.cx;
			if (0 < sz_delta.cy)
				rcWindow.bottom -= sz_delta.cy;
			if (0 < sz_delta.cx ||
				0 < sz_delta.cy)
			{
				m_frame_ref.SetWindowPos(
						NULL, 
						&rcWindow,
						SWP_NOZORDER|SWP_NOACTIVATE|SWP_NOMOVE
					);
				m_frame_ref.GetClientRect(&m_client_area);
			}
			m_frame_ref.CenterWindow();
		}
	private:
		static RECT   GetAvailableArea(void)
		{
			const POINT ptZero = {0};
			const HMONITOR hMonitor = ::MonitorFromPoint(ptZero, MONITOR_DEFAULTTOPRIMARY);
			MONITORINFO mInfo  = {0};
			mInfo.cbSize = sizeof(MONITORINFO);
			::GetMonitorInfo(hMonitor, &mInfo);
			return mInfo.rcWork;
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CMainFrame::CMainDialogImpl::CMainDialogImpl(void): 
	IDD(IDD_FWDESKTOP_MAIN_DLG),
	m_header(IDR_FWDESKTOP_MAIN_DLG_HEADER),
	m_status(GetStatusBarObject()),
	m_tabset(GetMainTabSetObject()),
	m_printview(*this, m_tabset.PrintDataProvider()),
	m_menubar(m_tabset)
{
}

CMainFrame::CMainDialogImpl::~CMainDialogImpl(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CMainFrame::CMainDialogImpl::OnDestroy    (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;

	details::GetMainFrame_AccelHandler().Terminate();
	details::GetMainFrame_AccelData()._dialog = NULL;

	m_tabset.Destroy();
	m_header.Destroy();
	m_status.Destroy();
	m_printview.Destroy();
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnInitDialog (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	details::CMainFrame_Layout layout(*this);
	layout.RecalcPosition();
	{
		CApplicationIconLoader loader(IDR_FWDESKTOP_MAIN_DLG_ICON);
		TBaseDlg::SetIcon(loader.DetachLargeIcon(), TRUE);
		TBaseDlg::SetIcon(loader.DetachSmallIcon(), FALSE);
	}
	TBaseDlg::SetMenu(m_menubar);
	{
		CAtlString cs_title(FG_DESKTOP_TITLE);
		TBaseDlg::SetWindowText(cs_title);
	}
	HRESULT hr_ = S_OK;
	{
		hr_ = m_header.Create(TBaseDlg::m_hWnd); ATLASSERT(S_OK == hr_);
		hr_ = m_status.Create(TBaseDlg::m_hWnd); ATLASSERT(S_OK == hr_);
	}
	RECT rcTab = {0};
	if (TBaseDlg::GetClientRect(&rcTab))
	{
		if (rcTab.right % 0x2)
			rcTab.right+= 0x1;
		rcTab.top    += m_header.GetSize().cy;
		rcTab.bottom -= m_status.GetSize().cy;
		hr_ = m_tabset.Create(TBaseDlg::m_hWnd, rcTab);
	}
	{
		const RECT rc_ = rcTab;
		hr_ = m_printview.Create(TBaseDlg::m_hWnd, rc_); ATLASSERT(S_OK == hr_);
	}

	details::GetMainFrame_AccelData()._dialog = *this;
	details::GetMainFrame_AccelHandler().Initialize();

	::SetWindowTheme(*this, _T("Explorer"), NULL);
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnMouseWheel (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnOwnerDraw  (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	if (lParam)
	{
		LPDRAWITEMSTRUCT lpDrawItem = (LPDRAWITEMSTRUCT)lParam;
		if (lpDrawItem)
		{
			m_status.Update(*lpDrawItem);
		}
	}
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnPrintView  (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;
	if (!m_printview.HasDefaultPrinter())
	{
		// https://support.microsoft.com/en-us/help/291174/error-message-when-you-attempt-to-print-or-fax-document-no-printers-ar
		CAtlString cs_warn;
		cs_warn.LoadString(IDS_FWDESKTOP_NO_PRINTER);

		global::SetWarnMessage(cs_warn.GetString(), false);
		return 0;
	}
	CApplicationCursor wait_;

	HRESULT hr_ = m_printview.Show();
	if (!FAILED(hr_))
	{
		CAtlString cs_title;
		cs_title.Format(
				_T("%s %s [Print Preview]"),
				(LPCTSTR)GetAppObjectRef().Version().ProductName(),
				(LPCTSTR)GetAppObjectRef().Version().FileVersionFixed()
			);
		TBaseDlg::SetWindowText(cs_title);
		m_tabset.Visible(false);
	}
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnSubMenuInit(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;

	CTabView view_(CTabView::eEventLog);

	if (m_printview.Visible())
		view_ = CTabView(CTabView::ePrinting);
	else
		view_ = m_tabset.Selected();

	const INT bSubMenuIndex = LOWORD(lParam);
	const HMENU hSubMenu = reinterpret_cast<HMENU>(wParam);

	m_menubar.UpdateState(
			hSubMenu,
			bSubMenuIndex,
			view_
		);
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnSysCommand (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
#if defined(_MODAL)
			EndDialog(IDCANCEL);
#else
			TBaseDlg::DestroyWindow();
#endif
			::PostQuitMessage(0);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CMainFrame::CMainDialogImpl::OnTabNotify  (INT idCtrl, LPNMHDR pnmh, BOOL& bHandled)
{
	idCtrl; pnmh; bHandled = TRUE;
	m_tabset.UpdateLayout();
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CMainFrame::CMainDialogImpl::OnMenuCommand(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	wNotifyCode; wID; hWndCtl;
	
	bHandled = TRUE;

	switch (wID)
	{
	case IDR_FWDESKTOP_MENU_ITM_EXIT  :
		{
			this->OnSysCommand(WM_COMMAND, SC_CLOSE, 0, bHandled);
		} break;
	case IDR_FWDESKTOP_MENU_ITM_HELP  :
		{
			const CTabView view_ = m_tabset.Selected();
			CChmHelper chm_;
			chm_.ShowHelpTopic(view_.Identifier());
		} break;
	case IDR_FWDESKTOP_MENU_ITM_UPDATE:
	case IDR_FWDESKTOP_MENU_ITM_HOME  :
		{
			::ShellExecute(
					NULL      ,
					_T("open"),
					_T("https://thefileguardian.com"),
					NULL,
					NULL,
					SW_SHOW
				);
		} break;
	case IDR_FWDESKTOP_MENU_ITM_ABOUT : 
		{
			CAboutDlg dlg_;
			dlg_.DoModal();
		} break;
	default:
		bHandled = FALSE;
	}

	if (bHandled == FALSE)
		bHandled = m_tabset.OnCommand(wID);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT CMainFrame::CMainDialogImpl::IPrintEvent_OnClose(void)
{
	m_tabset.Visible(true);
	m_printview.Hide();
	CAtlString cs_title;
	cs_title.Format(
			_T("%s %s"),
			(LPCTSTR)GetAppObjectRef().Version().ProductName(),
			(LPCTSTR)GetAppObjectRef().Version().FileVersionFixed()
		);
	TBaseDlg::SetWindowText(cs_title);

	global::SetInfoMessage(
			_T("Ready")
		);

	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT CMainFrame::CMainDialogImpl::IPrintEvent_OnError(const CSysError& _error)
{
	global::SetErrorMessage(_error);
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT CMainFrame::CMainDialogImpl::IPrintEvent_OnPageChanged(const INT nPage, const INT nPages)
{
	CAtlString cs_text;
	cs_text.Format(
			_T("Print preview: Page %d of %d"),
			nPage,
			nPages
		);

	global::SetInfoMessage(
			cs_text
		);

	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT CMainFrame::CMainDialogImpl::IPrintEvent_OnPagePrint(const INT nPage, const INT nPages)
{
	CAtlString cs_msg;
	cs_msg.Format(
			_T("Printing the page %d of %d"),
			nPage,
			nPages
		);
	global::SetInfoMessage(cs_msg);
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT CMainFrame::CMainDialogImpl::IPrintEvent_OnPrintAborted(void)
{
	global::SetWarnMessage(
			_T("Printing is cancelled")
		);
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT CMainFrame::CMainDialogImpl::IPrintEvent_OnPrintComplete(void)
{
	global::SetInfoMessage(
			_T("Printing is completed")
		);
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CMainFrame::CMainFrame(void)
{
}

CMainFrame::~CMainFrame(void)
{
}

/////////////////////////////////////////////////////////////////////////////

BOOL       CMainFrame::Create (void)
{
	HWND hDialog = m_dlg.Create(::GetDesktopWindow());
	if (!hDialog)
		return FALSE;
	else
		return TRUE;
}

INT_PTR    CMainFrame::DoModal(void)
{
	INT_PTR result = m_dlg.DoModal();
	return (result);
}