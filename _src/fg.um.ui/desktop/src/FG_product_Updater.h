#ifndef _FGPRODUCTUPDATER_H_DD8F2314_B127_45B2_98D3_21A7B7466A1C_INCLUDED
#define _FGPRODUCTUPDATER_H_DD8F2314_B127_45B2_98D3_21A7B7466A1C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Apr-2017 at 02:48:16a, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian product updater class declaration file.
*/
#include "Shared_SystemError.h"

namespace fw { namespace desktop
{
	using shared::lite::common::CSysError;

	class CProductUpdater
	{
	private:
		CSysError       m_error;
	public:
		CProductUpdater(void);
		~CProductUpdater(void);
	public:
		TErrorRef       Error(void)const;
		HRESULT         StartUpdatingProcess(void);
	};
}}


#endif/*_FGPRODUCTUPDATER_H_DD8F2314_B127_45B2_98D3_21A7B7466A1C_INCLUDED*/