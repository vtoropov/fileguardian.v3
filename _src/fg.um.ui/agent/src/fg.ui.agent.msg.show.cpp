/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Jun-2018 at 10:09:13a, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian agent utility message box based notify interface implementation file.
*/
#include "StdAfx.h"
#include "fg.ui.agent.msg.show.h"

using namespace fg::agent;

#include "Shared_GenericAppObject.h"

using namespace shared::user32;

#include <strsafe.h>
/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace agent { namespace details {

	LPCTSTR CEventMsg_DlgCap(void) {

		static CAtlString cs_title;

		if (cs_title.IsEmpty()) {

			const CApplication::CVersion& version = GetAppObjectRef().Version();

			cs_title.Format(
				_T("%s v.%s"), (LPCTSTR)version.CustomValue(_T("InternalName")), (LPCTSTR)version.FileVersionFixed()
			);
		}

		return cs_title.GetString();
	}

	VOID CEventMsg_DlgBox(const eLogType::_e _type, LPCTSTR _msg, LPCTSTR _caption = NULL) {

		DWORD style_ = MB_OK;
		switch (_type) {
		case eLogType::eError  :  style_ |= MB_ICONERROR;       break;
		case eLogType::eInfo   :  style_ |= MB_ICONASTERISK;    break;
		case eLogType::eWaiting:  style_ |= MB_ICONWARNING;     break;
		case eLogType::eWarning:  style_ |= MB_ICONWARNING;     break;
		}

		CAtlString cs_title(_caption);

		if (cs_title.IsEmpty())
			cs_title = CEventMsg_DlgCap();

		AtlMessageBox(
			NULL, _msg, cs_title.GetString(), style_
		);

	}

	CAtlString CEventMsg_Format(LPCTSTR lpszFormat, va_list& _args)
	{
		CAtlString msg_;
		size_t t_size = 0;

		try {

			HRESULT hr_ = S_OK;

			do
			{
				t_size += 2048;
				TCHAR* lpszBuffer = new TCHAR[t_size];

				::memset(lpszBuffer, 0, t_size * sizeof(TCHAR));

				hr_ = ::StringCchVPrintfEx(
					lpszBuffer, t_size, NULL, NULL, 0, lpszFormat, _args
				);

				if (S_OK == hr_)
					msg_ = lpszBuffer;

				if (lpszBuffer) {
					delete lpszBuffer; lpszBuffer = NULL;
				}
			}
			while(STRSAFE_E_INSUFFICIENT_BUFFER == hr_);
		}
		catch (::std::bad_alloc&) { }

		return msg_;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

bool  CEventMsg::IsAcceptable(void) {
}

/////////////////////////////////////////////////////////////////////////////

VOID  CEventMsg::LogError(const CSysError& _err) {

	CAtlString details_ = _err.GetFormattedDetails(_T("\n\t\t"));

	details::CEventMsg_DlgBox(
			eLogType::eError, details_.GetString(), NULL
		);
}

VOID  CEventMsg::LogError(LPCTSTR pszFormat, ...) {

	va_list  args_;
	va_start(args_, pszFormat);

	CAtlString details_ = details::CEventMsg_Format(pszFormat, args_);

	va_end(args_);

	details::CEventMsg_DlgBox(
		eLogType::eError, details_.GetString(), NULL
	);
}

VOID  CEventMsg::LogInfo (LPCTSTR pszFormat, ...) {

	va_list  args_;
	va_start(args_, pszFormat);

	CAtlString details_ = details::CEventMsg_Format(pszFormat, args_);

	va_end(args_);

	details::CEventMsg_DlgBox(
		eLogType::eInfo, details_.GetString(), NULL
	);
}

VOID  CEventMsg::LogWarn (LPCTSTR pszFormat, ...) {

	va_list  args_;
	va_start(args_, pszFormat);

	CAtlString details_ = details::CEventMsg_Format(pszFormat, args_);

	va_end(args_);

	details::CEventMsg_DlgBox(
		eLogType::eWarning, details_.GetString(), NULL
	);
}