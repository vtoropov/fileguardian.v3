#ifndef _FWAGENTHIDDENSCREEN_H_11308584_D9B8_4013_9D13_34E2F051A074_INCLUDED
#define _FWAGENTHIDDENSCREEN_H_11308584_D9B8_4013_9D13_34E2F051A074_INCLUDED
/*
	Created by Tech_dog (VToropov) on 10-Feb-2016 at 12:08:46am, GMT+7, Phuket, Rawai, Wednesday;
	This is File Watcher system tray agent hidden screen class declaration file.
*/
#include <shellapi.h>

#pragma comment(lib, "shell32.lib")

#include "FG_Generic_Defs.h"

namespace fw { namespace agent
{
	class CHiddenScreen :
			public ::ATL::CWindowImpl<CHiddenScreen>
	{
		typedef ::ATL::CWindowImpl<CHiddenScreen> TWindow;
	public:
		BEGIN_MSG_MAP(CHiddenScreen)
			MESSAGE_HANDLER(WM_QUERYENDSESSION,  OnQuery  )
			MESSAGE_HANDLER(WM_ENDSESSION     ,  OnEndSess)
			MESSAGE_HANDLER(WM_DESTROY        ,  OnDestroy)
			MESSAGE_HANDLER(WM_CREATE         ,  OnCreate )
			MESSAGE_HANDLER(WM_HOTKEY         ,  OnHotkey )

			if (uMsg == _FW_BROADCAST_MESSAGE_CLOSE_ID)
			{
				CEventJournal::LogInfo(
						_T("Hidden screen: The application received close message")
					);
				TWindow::SendMessage(WM_CLOSE);
				::PostQuitMessage(0);
				return TRUE;
			}
			if (uMsg == _FW_BROADCAST_MESSAGE_HOTKEY_ID)
			{
				OnUpdateHotKey();
				return TRUE;
			}
		END_MSG_MAP()
	private:
		VOID    OnUpdateHotKey(VOID);
	private:
		LRESULT OnCreate (UINT, WPARAM, LPARAM, BOOL&);
		LRESULT OnDestroy(UINT, WPARAM, LPARAM, BOOL& bHandled);
		LRESULT OnHotkey (UINT, WPARAM, LPARAM, BOOL&);
		LRESULT OnQuery  (UINT, WPARAM, LPARAM, BOOL&);
		LRESULT OnEndSess(UINT, WPARAM, LPARAM, BOOL&);
	};

}}
#endif/*_FWAGENTHIDDENSCREEN_H_11308584_D9B8_4013_9D13_34E2F051A074_INCLUDED*/