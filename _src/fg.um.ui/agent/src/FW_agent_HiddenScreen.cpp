/*
	Created by Tech_dog (VToropov) on 10-Feb-2016 at 12:13:21am, GMT+7, Phuket, Rawai, Wednesday;
	This is File Watcher system tray agent hidden screen class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jun-2018 at 9:40:17p, UTC+7, Phuket, Rawai, Friday;
*/
#include "StdAfx.h"
#include "FW_agent_HiddenScreen.h"
#include "FW_agent_Resource.h"

using namespace fw::agent;

#include "Shared_GenericAppObject.h"
#include "Shared_GenericAppResource.h"

using namespace shared::user32;

#include "FG_Generic_UI.h"
#include "FG_Generic_Defs.h"

using namespace fg::common::data;

/////////////////////////////////////////////////////////////////////////////

namespace fw { namespace agent { namespace details
{
	CAtlString HiddenScreen_GetDesktopPath(void)
	{
		static CAtlString cs_path;
		if (cs_path.IsEmpty())
		{
			CAtlString cs_desktop;
			cs_desktop.Format(
					_T(".\\%s.exe"), _FG_DESKTOP_NAME_COMMON
				);

			CApplication& the_app = GetAppObjectRef();

			the_app.GetPathFromAppFolder(cs_desktop.GetString(), cs_path);
		}
		return cs_path;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

LRESULT CHiddenScreen::OnCreate (UINT, WPARAM, LPARAM, BOOL&)
{
	OnUpdateHotKey();

	CApplicationIconLoader loader_(IDR_RT_MAINICON);
	TWindow::SetIcon(loader_.DetachLargeIcon(), TRUE);
	TWindow::SetIcon(loader_.DetachSmallIcon(), FALSE);

	return 0;
}

LRESULT CHiddenScreen::OnDestroy(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	BOOL bResult = ::UnregisterHotKey(TWindow::m_hWnd, 1);
	if (!bResult)
	{
		CSysError err_obj(::GetLastError());
		CEventJournal::LogError(
				err_obj
			);
	}
	else
	CEventJournal::LogInfo(
			_T("FG Agent: hot keys have been unregestered")
		);
	CEventJournal::LogInfo(
			_T("FG Agent: hidden screen is being destroyed")
		);
	return 0;
}

LRESULT CHiddenScreen::OnHotkey (UINT, WPARAM, LPARAM, BOOL&)
{
	CSysError
	err_obj(_T(__FUNCTION__));
	err_obj = S_OK;

	HINSTANCE hInstance = ::ShellExecute(
			NULL,
			_T("open"),
			details::HiddenScreen_GetDesktopPath(),
			NULL,
			NULL,
			SW_SHOWNORMAL
		);
	const DWORD dwCode = (DWORD)(INT_PTR)hInstance;
	if (dwCode < ERROR_LOCK_VIOLATION)
	{
		err_obj = dwCode;
		CEventJournal::LogError(
				err_obj
			);
	}
	return 0;
}

LRESULT CHiddenScreen::OnQuery  (UINT, WPARAM, LPARAM, BOOL&)
{
	CEventJournal::LogInfo(
			_T("FG Agent: on end session query")
		);
	return TRUE;
}

LRESULT CHiddenScreen::OnEndSess(UINT, WPARAM, LPARAM, BOOL&)
{
	CEventJournal::LogInfo(
			_T("FG Agent: being closed by session end")
		);
	TWindow::SendMessage(WM_CLOSE);
	::PostQuitMessage(0);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

VOID    CHiddenScreen::OnUpdateHotKey(VOID)
{
	CUISharedCfgPersistent pers_;

	HRESULT hr_ = pers_.Load();
	if (FAILED(hr_))
	{
		CEventJournal::LogError(
				pers_.Error()
			);
	}
	else if (!::RegisterHotKey(TWindow::m_hWnd, 1, pers_.HotKeys().Modifiers(), pers_.HotKeys().VirtualKey()))
	{
		CSysError err_;
		err_ = ::GetLastError();

		CEventJournal::LogError(
				err_
			);
	}
	else
		CEventJournal::LogInfo(_T("FG Agent: Hot keys are registered"));
}