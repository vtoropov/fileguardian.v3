/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Feb-2016 on 10:19:01pm, GMT+7, Phuket, Rawai, Tuesday;
	This is File Watcher System Tray Agent Entry Point file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jun-2018 at 9:38:13p, UTC+7, Phuket, Rawai, Friday;
*/
#include "StdAfx.h"
#include "Shared_SystemCore.h"
#include "Shared_GenericAppObject.h"
#include "Shared_GenericConObject.h"

using namespace shared::lite::common;
using namespace shared::lite::sys_core;
using namespace shared::user32;

#include "FG_Generic_Defs.h"
#include "FW_agent_HiddenScreen.h"

using namespace fw::agent;

#include "fg.ui.agent.handler.service.h"
#include "fg.ui.agent.handler.task.h"
#include "fg.ui.agent.handler.license.h"
#include "fg.ui.agent.handler.prefer.h"
#include "fg.ui.agent.handler.install.h"

using namespace fg::agent::handlers;

CAppModule _Module;

/////////////////////////////////////////////////////////////////////////////

INT _cmd_close_agent(void)
{
	CWindow screen_ = ::FindWindow(NULL, _FW_AGENT_WINDOW_TITLE);
	if (!screen_)
	{
		CSysError err_;
		err_.SetState(
				E_INVALIDARG, _T("FG Agent window is not found")
			);
		CEventJournal::LogError(
				err_
			);
	}
	else
	if (!screen_.SendMessage(_FW_BROADCAST_MESSAGE_CLOSE_ID, 0, 0))
		CEventJournal::LogError(
				CSysError(::GetLastError())
			);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

INT WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpstrCmdLine, INT nCmdShow)
{
	hInstance; hPrevInstance; lpstrCmdLine; nCmdShow;
	CCoInitializer com(false);

	HRESULT hr_ = ::CoInitializeSecurity(
						NULL,
						-1,
						NULL,
						NULL,
						RPC_C_AUTHN_LEVEL_NONE,
						RPC_C_IMP_LEVEL_IDENTIFY,
						NULL,
						EOAC_NONE,
						NULL
					);
	ATLVERIFY(SUCCEEDED(hr_));

	CApplication& the_app = GetAppObjectRef();
	const TCommandLine& cmd_line = the_app.CommandLine();
	const bool bUseConsoleOutput = cmd_line.Has(_T("console"));

	if (bUseConsoleOutput) {
		CEventJournal::OutputToConsole(true);
		CEventJournal::VerboseMode(true);
	}

	if (cmd_line.Has(_T("debug"))) {
		AtlMessageBox(
				NULL, _T("Awaiting debugger connection..."), _T("Breakpoint"), MB_ICONEXCLAMATION|MB_OK
			);
	}

	if (cmd_line.Count())
		CEventJournal::LogInfo(
				_T("FG Agent command: %s"), cmd_line.ToString().GetString()
			);
	else
		CEventJournal::LogInfo(
				_T("FG Agent is launched in hidden mode")
			);
	INT result_ = 0;
	{
		CServiceCmdHandler handler_(cmd_line);
		if (handler_.IsApplicable())
		{
			hr_ = handler_.Handle(result_);
			if (S_FALSE != hr_)
				return result_;
		}
	}
	{
		CStartupTaskCmdHandler handler_(cmd_line);
		if (handler_.IsApplicable())
		{
			hr_ = handler_.Handle(result_);
			if (S_FALSE != hr_)
				return result_;
		}
	}
	{
		CLicenseCmdHandler handler_(cmd_line);
		if (handler_.IsApplicable())
		{
			hr_ = handler_.Handle(result_);
			if (bUseConsoleOutput) {
				if (FAILED(hr_))
					CEventJournal::LogError(handler_.Error());
				else {
					CAtlString cs_msg = handler_.Error().GetDescription();
					AtlMessageBox(NULL, cs_msg.GetString());
				}
			}
			if (S_FALSE != hr_)
				return result_;
		}
	}
	{
		CPreferCmdHandler handler_(cmd_line);
		if (handler_.IsApplicable())
		{
			hr_ = handler_.Handle(result_);
			if (S_FALSE != hr_)
				return result_;
		}
	}
	{
		CInstallerCmdHandler handler_(cmd_line);
		if (handler_.IsApplicable())
		{
			hr_ = handler_.Handle(result_);
			if (S_FALSE != hr_)
				return result_;
		}
	}

	CSysError err_obj = the_app.Instance().RegisterSingleton(_FW_GLOBAL_MUTEX_AGENT);

	if (err_obj.GetCode() == ERROR_OBJECT_ALREADY_EXISTS)
	{
		CEventJournal::LogWarn(
				_T("FG Agent is already launched")
			);
		return 0;
	}
	else
		CEventJournal::LogInfo(
				_T("FG Agent is starting in background mode")
			);

	// this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
	::DefWindowProc(NULL, 0, 0, 0L);

	::WTL::AtlInitCommonControls(ICC_BAR_CLASSES);
	_Module.Init(NULL, NULL);

	::WTL::CMessageLoop pump_;
	_Module.AddMessageLoop(&pump_);

	RECT rc_ = {0};

	CHiddenScreen screen_;
	screen_.Create(NULL, rc_, _FW_AGENT_WINDOW_TITLE, WS_POPUP);

	if (screen_.IsWindow())
		result_ = pump_.Run();
	else
	{
		err_obj = ::GetLastError();

		CEventJournal::LogError(err_obj);

		result_ = _FW_CONSOLE_CODE_FAILURE;
	}
	_Module.RemoveMessageLoop();
	_Module.Term();

	return result_;
}