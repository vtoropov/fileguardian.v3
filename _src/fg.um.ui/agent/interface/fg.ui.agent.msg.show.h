#ifndef _FGUIAGENTMSGSHOW_H_E28DD43F_A553_49C9_A239_316F9F021CB1_INCLUDED
#define _FGUIAGENTMSGSHOW_H_E28DD43F_A553_49C9_A239_316F9F021CB1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Jun-2018 at 9:54:54a, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian agent utility message box based notify interface declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_LogCommonDefs.h"

namespace fg { namespace agent {

	using shared::lite::common::CSysError;
	using shared::log::eLogType;

	class CEventMsg {
	public:
		static bool  IsAcceptable(void);
	public:
		static VOID  LogError(const CSysError&);
		static VOID  LogError(LPCTSTR pszFormat, ...);
		static VOID  LogInfo (LPCTSTR pszFormat, ...);
		static VOID  LogWarn (LPCTSTR pszFormat, ...);
	};

}}

#endif/*_FGUIAGENTMSGSHOW_H_E28DD43F_A553_49C9_A239_316F9F021CB1_INCLUDED*/