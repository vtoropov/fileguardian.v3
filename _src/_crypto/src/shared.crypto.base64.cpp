/*
	Created by Tech_dog (VToropov) on 22-Jun-2016 at 5:05:13p, GMT+7, Phuket, Rawai, Wednesday;
	This is Shared Lite library system base64 encoder class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 10:37:13a, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "shared.crypto.base64.h"

using namespace shared::crypto;
using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

CBase64Provider::CBase64Provider(const DWORD dwOptions) : m_options(dwOptions)
{
	m_error.Source(_T("CBase64Provider"));
}

CBase64Provider::~CBase64Provider(void){}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace crypto { namespace details
{
	HRESULT Base64_CreateXmlObject(::ATL::CComPtr<IXMLDOMElement>& _root)
	{
		static CLSID cls_ids[] = {
#if defined(CLSID_DOMDocument60)
			CLSID_DOMDocument60,
#endif
#if defined(CLSID_DOMDocument40)
			CLSID_DOMDocument40,
#endif
			CLSID_DOMDocument30, CLSID_DOMDocument26};
		::ATL::CComPtr<IXMLDOMDocument2> doc_obj;

		HRESULT  hr_ = S_OK;
		for (INT it_ = 0; it_ < _countof(cls_ids); it_++)
		{
			hr_ = doc_obj.CoCreateInstance(cls_ids[it_]);
			if (S_OK == hr_)
				break;
		}
		if (S_OK != hr_) // the appropriate class cannot be found
			return  hr_;

		hr_ = doc_obj->createElement(_bstr_t(_T("data")), &_root);
		if (S_OK != hr_)
			return  hr_;

		hr_ = _root->put_dataType(_bstr_t(_T("bin.base64")));
		return  hr_;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CBase64Provider::Decode  (const _bstr_t&    enc_data, CRawData&   dec_data)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!enc_data.length())
		return (m_error = E_INVALIDARG);

	HRESULT hr_ = S_OK;

	::ATL::CComPtr<IXMLDOMElement> root;
	hr_ = details::Base64_CreateXmlObject(root);
	if (FAILED(hr_))
		return (m_error = hr_);

	hr_ = root->put_text(enc_data);
	if (FAILED(hr_))
		return (m_error = hr_);

	_variant_t val_;
	hr_ = root->get_nodeTypedValue(&val_);
	if (FAILED(hr_))
		return (m_error = hr_);

	CSafeArrayAccessor acc_(val_);
	if (!acc_.IsValid())
		return (m_error = acc_.Error());

	LONG len_ = 0;
	hr_ = acc_.GetSize(len_);
	if (FAILED(hr_))
		return (m_error = hr_);

	dec_data.Create(
			acc_.AccessData(),
			static_cast<DWORD>(len_)
		);

	if (!dec_data.IsValid())
		m_error = dec_data.Error();

	return m_error;
}

HRESULT      CBase64Provider::Decode  (const _variant_t& enc_data, _variant_t& dec_data)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (enc_data.vt != VT_BSTR)
		return (m_error = DISP_E_TYPEMISMATCH);

	CRawData raw_data;
	HRESULT hr_ = this->Decode(enc_data.bstrVal, raw_data);
	if (FAILED(hr_))
		return hr_;

	hr_ = raw_data.CopyToVariantAsUtf8(dec_data);
	if (FAILED(hr_))
		m_error = raw_data.Error();

	return m_error;
}

HRESULT      CBase64Provider::Encode  (const _variant_t& dec_data, _variant_t& enc_data)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (VT_BSTR != dec_data.vt || !dec_data.bstrVal)
		return (m_error = E_INVALIDARG);

	::ATL::CComPtr<IXMLDOMElement> root;
	m_error = details::Base64_CreateXmlObject(root);
	if (m_error)
		return  m_error;

	const DWORD n_data_len = ::SysStringByteLen(dec_data.bstrVal);
	SAFEARRAY* psa = ::SafeArrayCreateVector( VT_UI1, 0L, n_data_len);
	errno_t err_no = ::memcpy_s(psa->pvData, n_data_len, dec_data.bstrVal, n_data_len);
	if (err_no)
		return (m_error = E_OUTOFMEMORY);

	_variant_t raw_data;
	raw_data.vt = (VT_ARRAY | VT_UI1 );
	raw_data.parray = psa;
	psa = NULL;

	m_error = root->put_nodeTypedValue(raw_data);
	if (m_error)
		return  m_error;

	_bstr_t val_;
	m_error = root->get_text(val_.GetAddress());
	if (m_error)
		return  m_error;

	if (CBase64Options::eRemoveNewLine & m_options)
	{
		CAtlString cs_val_ = (LPCTSTR)val_;
		cs_val_.Replace(_T("\r"), _T(""));
		cs_val_.Replace(_T("\n"), _T(""));

		val_ = cs_val_;
	}

	enc_data.Clear();
	enc_data.vt = VT_BSTR;
	enc_data.bstrVal = val_.Detach();
	return  m_error;
}

HRESULT      CBase64Provider::EncodeSafeArray(const _variant_t& dec_sa, _variant_t& enc_data)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!(VT_ARRAY & dec_sa.vt) || !dec_sa.parray)
		return (m_error = E_INVALIDARG);

	::ATL::CComPtr<IXMLDOMElement> root;
	m_error = details::Base64_CreateXmlObject(root);
	if (m_error)
		return  m_error;

	m_error = root->put_nodeTypedValue(dec_sa);
	if (m_error)
		return  m_error;

	_bstr_t val_;
	m_error = root->get_text(val_.GetAddress());
	if (m_error)
		return  m_error;

	if (CBase64Options::eRemoveNewLine & m_options)
	{
		CAtlString cs_val_ = (LPCTSTR)val_;
		cs_val_.Replace(_T("\r"), _T(""));
		cs_val_.Replace(_T("\n"), _T(""));

		val_ = cs_val_;
	}

	enc_data.Clear();
	enc_data.vt = VT_BSTR;
	enc_data.bstrVal = val_.Detach();

	return m_error;
}

TErrorRef    CBase64Provider::Error(void)const
{
	return m_error;
}