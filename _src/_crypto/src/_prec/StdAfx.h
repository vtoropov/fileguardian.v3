#ifndef _CRYPTOSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED
#define _CRYPTOSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED
/*
	Created by Tech_dog (VToropov) on 16-Jul-2016 at 7:32:26p, GMT+7, Phuket, Rawai, Saturday;
	This is Shared Crypto data Provider Library precompiled headers declaration file.
	-----------------------------------------------------------------------------
	Adopted tp v15 on 27-May-2018 at 11:26:30p, UTC+7, Phuket, Rawai, Sunday;
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>

#endif/*_CRYPTOSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED*/