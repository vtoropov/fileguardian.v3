/*
	Created by Tech_dog (VToropov) on 29-Nov-2014 at 3:41:57am, GMT+3, Taganrog, Saturday;
	This is Shared Library MCRYPT data provider class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 27-May-2018 at 11:24:47p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "shared.crypto.mcrypt.h"
#include "shared.crypto.base64.h"
#include "generic.stg.data.h"
#include "mcrypt.h"

using namespace shared::crypto;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace crypto { namespace details
{
	DWORD CryptoDataProvider_GetBufferLen(MCRYPT crypt_, const DWORD data_len)
	{
		DWORD n_len = 0;
		if (data_len)
		{
			const INT blocksize = mcrypt_enc_get_block_size(crypt_);

			if (blocksize < 1){}
			else if (data_len < (DWORD)blocksize)
			{
				n_len = blocksize;
			}
			else if (data_len > (DWORD)blocksize)
			{
				const DWORD dw_module = (data_len % blocksize);
				if (dw_module)
					n_len = data_len - dw_module + (DWORD)blocksize; // aligns the size to k * blocksize as required by mcrypt spec
				else
					n_len = data_len;
			}
			else
				n_len = blocksize;
		}
		return n_len;
	}
}}}

#define CRT_OBJ(_pvoid) reinterpret_cast<MCRYPT>(_pvoid)
/////////////////////////////////////////////////////////////////////////////

CCryptoDataCrt::CCryptoDataCrt(void)
{
}

CCryptoDataCrt::~CCryptoDataCrt(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR      CCryptoDataCrt::Algorithm(void)const
{
	return m_algo;
}

HRESULT      CCryptoDataCrt::Algorithm(const UINT uId)
{
	m_algo.LoadString(uId);
	return (m_algo.IsEmpty() ? E_INVALIDARG : S_OK);
}

HRESULT      CCryptoDataCrt::Algorithm(LPCTSTR pszValue)
{
	m_algo = pszValue;
	return (m_algo.IsEmpty() ? E_INVALIDARG : S_OK);
}

LPCTSTR      CCryptoDataCrt::Key(void)const
{
	return m_key;
}

HRESULT      CCryptoDataCrt::Key(const UINT uId)
{
	m_key.LoadString(uId);
	return (m_key.IsEmpty() ? E_INVALIDARG : S_OK);
}

HRESULT      CCryptoDataCrt::Key(LPCTSTR pszValue)
{
	m_key = pszValue;
	return (m_key.IsEmpty() ? E_INVALIDARG : S_OK);
}

LPCTSTR      CCryptoDataCrt::Mode(void)const
{
	return m_mode;
}

HRESULT      CCryptoDataCrt::Mode(const UINT uId)
{
	m_mode.LoadString(uId);
	return (m_mode.IsEmpty() ? E_INVALIDARG : S_OK);
}

HRESULT      CCryptoDataCrt::Mode(LPCTSTR pszValue)
{
	m_mode = pszValue;
	return (m_mode.IsEmpty() ? E_INVALIDARG : S_OK);
}

LPCTSTR      CCryptoDataCrt::Vector(void)const
{
	return m_vector;
}

HRESULT      CCryptoDataCrt::Vector(const UINT uId)
{
	m_vector.LoadString(uId);
	return (m_vector.IsEmpty() ? E_INVALIDARG : S_OK);
}

HRESULT      CCryptoDataCrt::Vector(LPCTSTR pszValue)
{
	m_vector = pszValue;
	return (m_vector.IsEmpty() ? E_INVALIDARG : S_OK);
}

/////////////////////////////////////////////////////////////////////////////

CCryptoDataProvider::CCryptoDataProvider(void) : m_crypt(NULL), m_state(eCryptoModuleState::eUndefined)
{
	m_error.Source(_T("CCryptoDataProvider"));
}

CCryptoDataProvider::CCryptoDataProvider(const CCryptoDataCrt& _crt) : m_crypt(NULL), m_state(eCryptoModuleState::eUndefined)
{
	m_error.Source(_T("CCryptoDataProvider"));
	m_crt = _crt;
}

CCryptoDataProvider::~CCryptoDataProvider(void)
{
	this->Terminate();
}

/////////////////////////////////////////////////////////////////////////////

VOID         CCryptoDataProvider::CreateData(const CCryptoDataCrt& crt)
{
	m_crt = crt;
}

HRESULT      CCryptoDataProvider::Decrypt(const _variant_t& enc_data, _variant_t& dec_data)
{
	if (!this->IsInitialized())
		return (m_error = OLE_E_BLANK);
	if (VT_BSTR != enc_data.vt || !enc_data.bstrVal)
		return (m_error = E_INVALIDARG);

	const DWORD n_data_len = ::SysStringByteLen(enc_data.bstrVal);
	const DWORD n_buff_len = details::CryptoDataProvider_GetBufferLen(CRT_OBJ(m_crypt), n_data_len);
	if (n_buff_len < 1)
		return (m_error = E_UNEXPECTED);

	CRawData raw_data(n_buff_len);
	if (!raw_data.IsValid())
		return (m_error = raw_data.Error());

	const errno_t err_  = ::memcpy_s(raw_data.GetData(), raw_data.GetSize(), enc_data.bstrVal, n_data_len);
	if (err_)
		return (m_error = E_OUTOFMEMORY);

	const INT nResult = mdecrypt_generic(CRT_OBJ(m_crypt), raw_data.GetData(), raw_data.GetSize());
	if ( 0 == nResult )
	{
		dec_data.Clear();
		dec_data.vt = VT_BSTR;
		dec_data.bstrVal = ::SysAllocStringByteLen((LPCSTR)raw_data.GetData(), raw_data.GetSize());
		if (!dec_data.bstrVal)
			m_error = E_OUTOFMEMORY;
		else
			m_error.Clear();
	}
	else
		m_error = E_FAIL;
	return m_error;
}

HRESULT      CCryptoDataProvider::Encrypt(const _variant_t& dec_data, _variant_t& enc_data)
{
	if (!this->IsInitialized())
		return (m_error = OLE_E_BLANK);
	if (VT_BSTR != dec_data.vt || !dec_data.bstrVal)
		return (m_error = E_INVALIDARG);

	const DWORD n_data_len = ::SysStringByteLen(dec_data.bstrVal);
	const DWORD n_buff_len = details::CryptoDataProvider_GetBufferLen(CRT_OBJ(m_crypt), n_data_len);
	if (n_buff_len < 1)
		return (m_error = E_UNEXPECTED);

	CRawData raw_data(n_buff_len);
	if (!raw_data.IsValid())
		return (m_error = raw_data.Error());

	const errno_t err_  = ::memcpy_s(raw_data.GetData(), raw_data.GetSize(), dec_data.bstrVal, n_data_len);
	if (err_)
		return (m_error = E_OUTOFMEMORY);

	const INT nResult = mcrypt_generic(CRT_OBJ(m_crypt), raw_data.GetData(), raw_data.GetSize());
	if ( 0 == nResult )
	{
		enc_data.Clear();
		enc_data.vt = VT_BSTR;
		enc_data.bstrVal = ::SysAllocStringByteLen((LPCSTR)raw_data.GetData(), raw_data.GetSize());
		if (!enc_data.bstrVal)
			m_error = E_OUTOFMEMORY;
		else
			m_error.Clear();
	}
	else
		m_error = E_FAIL;
	return m_error;
}

TErrorRef    CCryptoDataProvider::Error(void)const
{
	return m_error;
}

HRESULT      CCryptoDataProvider::Initialize(void)
{
	if (this->IsInitialized())
		return (m_error = ERROR_ALREADY_INITIALIZED);
	m_error.Clear();
	
	::ATL::CAtlString cs_algo = m_crt.Algorithm();
	::ATL::CAtlString cs_mode = m_crt.Mode();

	if (!cs_algo.IsEmpty() && !cs_mode.IsEmpty())
	{
		CAtlStringA algo_ = cs_algo.GetString();
		CAtlStringA mode_ = cs_mode.GetString();
		m_crypt = mcrypt_module_open(algo_.GetBuffer(), NULL, mode_.GetBuffer(), NULL);
	}
	else
		return (m_error = E_INVALIDARG);

	if (m_crypt)
		m_state = eCryptoModuleState::eOpened;
	else
		m_error = E_FAIL;
	if (m_error)
		return m_error;

	::ATL::CAtlString cs_key = m_crt.Key();
	::ATL::CAtlString cs_iv  = m_crt.Vector();

	if (cs_key.IsEmpty() || cs_iv.IsEmpty())
		return (m_error = E_INVALIDARG);

	const ::CT2A key_(cs_key);
	const ::CT2A iv_(cs_iv);

	const INT n_key_sz  = mcrypt_enc_get_key_size(CRT_OBJ(m_crypt));
	const INT n_iv_sz   = mcrypt_enc_get_iv_size(CRT_OBJ(m_crypt));
	
	CRawData raw_key(n_key_sz);
	if (!raw_key.IsValid())
		return (m_error = raw_key.Error());
	{
		errno_t err_ = 0;
		if (n_key_sz < (INT)::strlen(key_))
			err_ = ::memcpy_s(raw_key.GetData(), raw_key.GetSize(), key_, raw_key.GetSize());
		else
			err_ = ::memcpy_s(raw_key.GetData(), raw_key.GetSize(), key_, ::strlen(key_));
		if (err_)
			return (m_error = E_OUTOFMEMORY);
	}
	CRawData raw_iv(n_iv_sz);
	if (!raw_iv.IsValid())
		return (m_error = raw_iv.Error());
	{
		errno_t err_ = 0;
		if (n_iv_sz < (INT)::strlen(iv_))
			err_ = ::memcpy_s(raw_iv.GetData(), raw_iv.GetSize(), iv_, raw_iv.GetSize());
		else
			err_ = ::memcpy_s(raw_iv.GetData(), raw_iv.GetSize(), iv_, ::strlen(iv_));
		if (err_)
			return (m_error = E_OUTOFMEMORY);
	}
	const INT n_result  = mcrypt_generic_init(CRT_OBJ(m_crypt), raw_key.GetData(), raw_key.GetSize(), raw_iv.GetData());
	if (n_result == 0)
		m_state = eCryptoModuleState::eInitialized;
	return m_error;
}

bool         CCryptoDataProvider::IsInitialized(void)const
{
	return (eCryptoModuleState::eInitialized == m_state);
}

HRESULT      CCryptoDataProvider::Terminate(void)
{
	if (m_crypt)
	{
		if (eCryptoModuleState::eInitialized == m_state)
		{
			mcrypt_generic_deinit(CRT_OBJ(m_crypt));
			m_state = eCryptoModuleState::eOpened;
		}
		if (eCryptoModuleState::eOpened == m_state)
		{
			mcrypt_module_close(CRT_OBJ(m_crypt));
			m_state = eCryptoModuleState::eUndefined;
		}
		m_crypt = NULL;
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CCryptoTextProvider::CCryptoTextProvider(const CCryptoDataCrt& _crt) : m_provider(_crt)
{
	m_error.Source(_T("CCryptoTextProvider"));
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CCryptoTextProvider::Decrypt(const CAtlString& _encoded, CAtlString& _decoded)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (_encoded.IsEmpty())
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Encoded data cannot be empty")
			);
		return m_error;
	}

	_variant_t v_dec_b64;
	_variant_t v_enc_b64 = _encoded.GetString();

	CBase64Provider b64_;
	HRESULT hr_ = b64_.Decode(v_enc_b64, v_dec_b64);
	if (FAILED(hr_))
		return (m_error = b64_.Error());

	hr_ = m_provider.Initialize();
	if (FAILED(hr_))
		return (m_error = m_provider.Error());

	_variant_t v_dec_;
	hr_ = m_provider.Decrypt(v_dec_b64, v_dec_);
	if (FAILED(hr_))
		m_error = m_provider.Error();
	else
		_decoded = v_dec_;

	m_provider.Terminate();

	return m_error;
}

HRESULT      CCryptoTextProvider::Encrypt(const CAtlString& _decoded, CAtlString& _encoded)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = m_provider.Initialize();
	if (FAILED(hr_))
		return (m_error = m_provider.Error());

	_variant_t v_enc_;
	_variant_t v_dec_ = _decoded.GetString();

	hr_ = m_provider.Encrypt(v_dec_, v_enc_);
	if (FAILED(hr_))
		m_error = m_provider.Error();
	else
	{
#if (0)
		shared::log::DumpToFile(
				_T("e:\\enc_dump.bin"),
				v_enc_
			);
#endif
		_variant_t v_b64;
		CBase64Provider b64_;
		hr_ = b64_.Encode(v_enc_, v_b64);
		if (FAILED(hr_))
			m_error = b64_.Error();
		else
			_encoded = v_b64;
	}
	m_provider.Terminate();

	return m_error;
}

TErrorRef    CCryptoTextProvider::Error(void)const
{
	return m_error;
}