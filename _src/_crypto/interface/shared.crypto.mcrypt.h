#ifndef _SHAREDRCRYPTOPROVIDER_H_05ED1BF8_B989_4d04_BF4D_23CECE1F9D8C_INCLUDED
#define _SHAREDRCRYPTOPROVIDER_H_05ED1BF8_B989_4d04_BF4D_23CECE1F9D8C_INCLUDED
/*
	Created by Tech_dog (VToropov) on 29-Nov-2014 at 3:41:57am, GMT+3, Taganrog, Saturday;
	This is Shared Lite Library MCRYPT data provider class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 10:39:34a, UTC+7, Phuket, Rawai, Monday;
*/
#include "Shared_SystemError.h"

namespace shared { namespace crypto
{
	using shared::lite::common::CSysError;

	class eCryptoModuleState
	{
	public:
		enum _e{
			eUndefined   = 0,
			eOpened      = 1,
			eInitialized = 2,
		};
	};

	class CCryptoDataCrt
	{
	private:
		CAtlString             m_key;
		CAtlString             m_mode;
		CAtlString             m_algo;
		CAtlString             m_vector;
	public:
		CCryptoDataCrt(void);
		~CCryptoDataCrt(void);
	public:
		LPCTSTR                Algorithm(void)const;
		HRESULT                Algorithm(const UINT);
		HRESULT                Algorithm(LPCTSTR);
		LPCTSTR                Key(void)const;
		HRESULT                Key(const UINT);
		HRESULT                Key(LPCTSTR);
		LPCTSTR                Mode(void)const;
		HRESULT                Mode(const UINT);
		HRESULT                Mode(LPCTSTR);
		LPCTSTR                Vector(void)const;
		HRESULT                Vector(const UINT);
		HRESULT                Vector(LPCTSTR);
	};

	class CCryptoDataProvider
	{
	private:
		eCryptoModuleState::_e m_state;
		CSysError              m_error;
		PVOID                  m_crypt;
		CCryptoDataCrt         m_crt;
	public:
		CCryptoDataProvider(void);
		CCryptoDataProvider(const CCryptoDataCrt&);
		~CCryptoDataProvider(void);
	public:
		VOID                   CreateData(const CCryptoDataCrt&);
		HRESULT                Decrypt(const _variant_t& enc_data, _variant_t& dec_data);
		HRESULT                Encrypt(const _variant_t& dec_data, _variant_t& enc_data);
		const CSysError&       Error(void)const;
		HRESULT                Initialize(void);
		bool                   IsInitialized(void)const;
		HRESULT                Terminate(void);
	};

	class CCryptoTextProvider
	{
	protected:
		CSysError             m_error;
		CCryptoDataProvider   m_provider;
	public:
		CCryptoTextProvider(const CCryptoDataCrt&);
	public:
		HRESULT      Decrypt(const CAtlString& _encoded, CAtlString& _decoded);
		HRESULT      Encrypt(const CAtlString& _decoded, CAtlString& _encoded);
		TErrorRef    Error(void)const;
	};
}}

#endif/*_SHAREDRCRYPTOPROVIDER_H_05ED1BF8_B989_4d04_BF4D_23CECE1F9D8C_INCLUDED*/