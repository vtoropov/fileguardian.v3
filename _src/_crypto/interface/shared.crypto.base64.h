#ifndef _SHAREDBASE64ENCODER_H_A81435F2_13E4_46ba_A8EF_3822F0FCEBCD_INCLUDED
#define _SHAREDBASE64ENCODER_H_A81435F2_13E4_46ba_A8EF_3822F0FCEBCD_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Jun-2016 at 4:59:58p, GMT+7, Phuket, Rawai, Wednesday;
	This is Shared Lite library system base64 encoder class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 8:28:33a, UTC+7, Phuket, Rawai, Monday;
*/
#include "Shared_SystemError.h"
#include "generic.stg.data.h"
#include <msxml2.h>

#pragma comment(lib, "msxml2.lib")

namespace shared { namespace crypto
{
	using shared::lite::common::CSysError;
	using shared::lite::data::CRawData;

	class CBase64Options
	{
	public:
		enum _e{
			eNone          = 0,
			eRemoveNewLine = 1,  // removes new line symbol from the output;
		};
	};

	class CBase64Provider
	{
	private:
		CSysError    m_error;
		DWORD        m_options;
	public:
		CBase64Provider(const DWORD dwOptions = CBase64Options::eNone);
		~CBase64Provider(void);
	public:
		HRESULT      Decode  (const _bstr_t&    enc_data, CRawData&   dec_data);      // restores data (to memory block) from base-64 encoded string
		HRESULT      Decode  (const _variant_t& enc_data, _variant_t& dec_data);      // restores data (to BSTR) from base-64 encoded string (BSTR)
		HRESULT      Encode  (const _variant_t& dec_data, _variant_t& enc_data);      // converts data (VT_BSTR) to base-64 encoded string
		HRESULT      EncodeSafeArray(const _variant_t& dec_sa, _variant_t& enc_data); // converts binary data (VT_ARRAY|VT_U1) to base-64 encoded string
		TErrorRef    Error   (void)const;
	};
}}

#endif