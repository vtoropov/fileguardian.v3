/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Jun-2016 at 6:58:29p, UTC+7, Phuket, Rawai, Tuesday;
	This is Shared Lite Library System Service Base class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 27-May-2018 at 6:40:37p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Shared_SystemServiceImpl.h"

using namespace shared::service;

#include <dbt.h>

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace service { namespace details
{
	CServiceBaseImpl*&    ServiceBaseImpl_SingletonPtrRef(void)
	{
		static CServiceBaseImpl* pObject = NULL;
		return pObject;
	}

	CServiceBaseImplEx*&  ServiceBaseImpl_SingletonPtrRefEx(void)
	{
		static CServiceBaseImplEx* pObject = NULL;
		return pObject;
	}

	VOID  ServiceBaseImpl_DummyLog(LPCTSTR pszMessage)
	{
		::MessageBox(NULL, pszMessage, _T("CServiceBaseImpl"), MB_SERVICE_NOTIFICATION);
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CServiceEvtLog::CServiceEvtLog(LPCTSTR pszEventSource) : m_source(pszEventSource)
{
}

CServiceEvtLog::~CServiceEvtLog(void)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID   CServiceEvtLog::WriteErrorEntry(LPCTSTR pszFunction, const DWORD dwError)
{
	CAtlString cs_error;
	cs_error.Format(
			_T("%s failed with error: 0x%08lx"),
			pszFunction,
			dwError
		);
	this->WriteEventEntry(cs_error, EVENTLOG_ERROR_TYPE);
}

VOID   CServiceEvtLog::WriteEventEntry(LPCTSTR pszMessage, const WORD wType)
{
	LPCWSTR lpszStrings[2] = { NULL, NULL };

	HANDLE hEventSource = ::RegisterEventSource(NULL, m_source);
	if (hEventSource)
	{
		lpszStrings[0] = m_source;
        lpszStrings[1] = pszMessage;
		::ReportEvent(
				hEventSource,
				wType,
				0,
				0,
				NULL,
				_countof(lpszStrings),
				0,
				lpszStrings,
				NULL
			);

		::DeregisterEventSource(hEventSource);
		hEventSource = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

CServiceBaseImpl::CServiceBaseImpl(const CServiceCrtData& crt_ref) : m_statusHandle(NULL), m_crt_data(crt_ref)
{
	::memset(&m_status, 0, sizeof(SERVICE_STATUS));

	m_status.dwServiceType  = SERVICE_WIN32_OWN_PROCESS;
	m_status.dwCurrentState = SERVICE_START_PENDING;

	DWORD dwCtrlAccepted    = CServiceCrtOption::eNone;
	
	if (CServiceCrtOption::eCanContinue & m_crt_data.Options()) dwCtrlAccepted |= SERVICE_ACCEPT_PAUSE_CONTINUE;
	if (CServiceCrtOption::eCanShutdown & m_crt_data.Options()) dwCtrlAccepted |= SERVICE_ACCEPT_SHUTDOWN;
	if (CServiceCrtOption::eStoppable   & m_crt_data.Options()) dwCtrlAccepted |= SERVICE_ACCEPT_STOP;

	m_status.dwControlsAccepted = dwCtrlAccepted;
	m_status.dwWin32ExitCode    = NO_ERROR;
	m_status.dwServiceSpecificExitCode = 0;
	m_status.dwCheckPoint       = 0;
	m_status.dwWaitHint         = 0;
}

CServiceBaseImpl::~CServiceBaseImpl(void)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID     CServiceBaseImpl::OnContinue(void) {}
VOID     CServiceBaseImpl::OnPause(void) {}
VOID     CServiceBaseImpl::OnShutdown(void) {}
VOID     CServiceBaseImpl::OnStart(const DWORD, LPWSTR*){}
VOID     CServiceBaseImpl::OnStop(void){}

/////////////////////////////////////////////////////////////////////////////

VOID     CServiceBaseImpl::SetServiceStatus(
				const DWORD dwCurrentState,
				const DWORD dwWin32ExitCode,
				const DWORD dwWaitHint
			)
{
	static DWORD dwCheckPoint = 1;

	m_status.dwCurrentState   = dwCurrentState;
	m_status.dwWin32ExitCode  = dwWin32ExitCode;
	m_status.dwWaitHint       = dwWaitHint;

	switch (dwCurrentState)
	{
	case SERVICE_RUNNING:
	case SERVICE_STOPPED:
		{
			m_status.dwCheckPoint = 0;
		}break;
	default:
			m_status.dwCheckPoint = dwCheckPoint++;
	}

	::SetServiceStatus(m_statusHandle, &m_status);
}

/////////////////////////////////////////////////////////////////////////////

VOID   CServiceBaseImpl::Continue(void)
{
	CServiceEvtLog log_(this->m_crt_data.Name());

	try
	{
		this->SetServiceStatus(SERVICE_CONTINUE_PENDING);
		OnContinue();
		this->SetServiceStatus(SERVICE_RUNNING);
	}
	catch(DWORD dwError){ log_.WriteErrorEntry(_T(__FUNCTION__), dwError);          this->SetServiceStatus(SERVICE_PAUSED);}
	catch(...)          { log_.WriteErrorEntry(_T(__FUNCTION__), ::GetLastError()); this->SetServiceStatus(SERVICE_PAUSED);}
}

VOID   CServiceBaseImpl::Pause(void)
{
	CServiceEvtLog log_(this->m_crt_data.Name());

	try
	{
		this->SetServiceStatus(SERVICE_PAUSE_PENDING);
		OnPause();
		this->SetServiceStatus(SERVICE_PAUSED);
	}
	catch(DWORD dwError){ log_.WriteErrorEntry(_T(__FUNCTION__), dwError);          this->SetServiceStatus(SERVICE_RUNNING);}
	catch(...)          { log_.WriteErrorEntry(_T(__FUNCTION__), ::GetLastError()); this->SetServiceStatus(SERVICE_RUNNING);}
}

VOID   CServiceBaseImpl::Exit(void)
{
	this->SetServiceStatus(SERVICE_STOPPED);
}

VOID   CServiceBaseImpl::Shutdown(void)
{
	CServiceEvtLog log_(this->m_crt_data.Name());

	try
	{
		OnShutdown();
		this->SetServiceStatus(SERVICE_STOPPED);
	}
	catch(DWORD dwError){ log_.WriteErrorEntry(_T(__FUNCTION__), dwError);          }
	catch(...)          { log_.WriteErrorEntry(_T(__FUNCTION__), ::GetLastError()); }
}

VOID   CServiceBaseImpl::Start(DWORD dwArgc, PWSTR* pszArgv)
{
	CServiceEvtLog log_(this->m_crt_data.Name());
	try
	{
		this->SetServiceStatus(SERVICE_START_PENDING);
		OnStart(dwArgc, pszArgv);
		this->SetServiceStatus(SERVICE_RUNNING);
	}
	catch(DWORD dwError){ log_.WriteErrorEntry(_T(__FUNCTION__), dwError);          this->SetServiceStatus(SERVICE_STOPPED, dwError);}
	catch(...)          { log_.WriteErrorEntry(_T(__FUNCTION__), ::GetLastError()); this->SetServiceStatus(SERVICE_STOPPED);         }
}

VOID   CServiceBaseImpl::Stop(void)
{
	CServiceEvtLog log_(this->m_crt_data.Name());
	const DWORD dwOriginalState = m_status.dwCurrentState;

	try
	{
		this->SetServiceStatus(SERVICE_STOP_PENDING);
		OnStop();
		this->SetServiceStatus(SERVICE_STOPPED);
	}
	catch(DWORD dwError){ log_.WriteErrorEntry(_T(__FUNCTION__), dwError);          this->SetServiceStatus(dwOriginalState); }
	catch(...)          { log_.WriteErrorEntry(_T(__FUNCTION__), ::GetLastError()); this->SetServiceStatus(dwOriginalState); }
}

/////////////////////////////////////////////////////////////////////////////

VOID WINAPI CServiceBaseImpl::ServiceCtrlHandler(DWORD dwControl)
{
	details::ServiceBaseImpl_DummyLog(_T(__FUNCTION__));

	CServiceBaseImpl* pObject = details::ServiceBaseImpl_SingletonPtrRef();
	if (!pObject)
		return;

	switch (dwControl)
    {
	case SERVICE_CONTROL_STOP:     pObject->Stop();     break;
	case SERVICE_CONTROL_PAUSE:    pObject->Pause();    break;
	case SERVICE_CONTROL_CONTINUE: pObject->Continue(); break;
	case SERVICE_CONTROL_SHUTDOWN: pObject->Shutdown(); break;
	default: break;
    }
}

VOID WINAPI CServiceBaseImpl::ServiceMain(DWORD dwArgc, LPWSTR* lpszArgv)
{
	dwArgc; lpszArgv;
	details::ServiceBaseImpl_DummyLog(_T(__FUNCTION__));

	CServiceBaseImpl* pObject = details::ServiceBaseImpl_SingletonPtrRef();
	if (!pObject)
		return;

	pObject->m_statusHandle = ::RegisterServiceCtrlHandler(
			_T("SharedServiceBase"),
			CServiceBaseImpl::ServiceCtrlHandler
		);
	if (!pObject->m_statusHandle)
		return;

	pObject->Start(dwArgc, lpszArgv);
}

/////////////////////////////////////////////////////////////////////////////

BOOL WINAPI CServiceBaseImpl::Run(CServiceBaseImpl& obj_ref)
{
	details::ServiceBaseImpl_SingletonPtrRef() = &obj_ref;
	details::ServiceBaseImpl_DummyLog(_T(__FUNCTION__));

	const CServiceCrtData& crt_data = obj_ref.m_crt_data;

	TCHAR szServiceName[_MAX_PATH] = {0};
	::_tcscpy_s(szServiceName, _countof(szServiceName) - 1, crt_data.Name().GetString());

	CAtlString cs_name( _T("SharedServiceBase"));

	SERVICE_TABLE_ENTRY serviceTable[] = 
	{
		{ cs_name.GetBuffer(), CServiceBaseImpl::ServiceMain },
		{ NULL, NULL }
	};

	return ::StartServiceCtrlDispatcher(serviceTable);
}

/////////////////////////////////////////////////////////////////////////////

CServiceBaseImplEx::CServiceBaseImplEx(const CServiceCrtData& _crt) : TBase(_crt), m_notify(NULL), m_last_error(NO_ERROR)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID  CServiceBaseImplEx::OnStart(const DWORD dwArgc, LPWSTR* pszArgv)
{
	dwArgc; pszArgv;
	m_last_error = NO_ERROR;
#if (0)
	DEV_BROADCAST_DEVICEINTERFACE filter_ = {0};
	filter_.dbcc_size       = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
	filter_.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
	filter_.dbcc_reserved   = 0;
	filter_.dbcc_classguid  = GUID_DEVINTERFACE_USB_DEVICE;

	m_notify = ::RegisterDeviceNotification(
					TBase::m_statusHandle,
					&filter_,
					DEVICE_NOTIFY_SERVICE_HANDLE|DEVICE_NOTIFY_ALL_INTERFACE_CLASSES
				);
	if (m_notify == NULL)
		m_last_error = ::GetLastError();
#endif
}

VOID  CServiceBaseImplEx::OnStop(void)
{
#if (0)
	if (m_notify)
	{
		::UnregisterDeviceNotification(m_notify);
		m_notify = NULL;
	}
#endif
}

/////////////////////////////////////////////////////////////////////////////

DWORD CServiceBaseImplEx::ServiceCtrlHandlerEx(DWORD dwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext)
{
	TBase::ServiceCtrlHandler(dwControl);

	CServiceBaseImplEx* pObject = reinterpret_cast<CServiceBaseImplEx*>(lpContext);
	if (!pObject)
		return ERROR_OBJECT_NOT_FOUND;

	switch (dwControl)
    {
	case SERVICE_CONTROL_DEVICEEVENT:
		{
			pObject->OnDeviceEvent(dwEventType, lpEventData);
		} break;
	}
	return NO_ERROR;
}

VOID  CServiceBaseImplEx::ServiceMain(DWORD dwArgc, LPWSTR* lpszArgv)
{
	dwArgc; lpszArgv;
	details::ServiceBaseImpl_DummyLog(_T(__FUNCTION__));

	CServiceBaseImplEx* pObject = details::ServiceBaseImpl_SingletonPtrRefEx();
	if (!pObject)
		return;

	pObject->m_statusHandle = ::RegisterServiceCtrlHandlerEx(
			pObject->m_crt_data.Name(),
			CServiceBaseImplEx::ServiceCtrlHandlerEx,
			pObject
		);
	if (!pObject->m_statusHandle)
		return;

	pObject->Start(dwArgc, lpszArgv);
}

BOOL  CServiceBaseImplEx::Run(CServiceBaseImplEx& obj_ref)
{
	details::ServiceBaseImpl_SingletonPtrRef  () = &obj_ref;
	details::ServiceBaseImpl_SingletonPtrRefEx() = &obj_ref;
	details::ServiceBaseImpl_DummyLog(_T(__FUNCTION__));

	const CServiceCrtData& crt_data = obj_ref.m_crt_data;

	TCHAR szServiceName[_MAX_PATH] = {0};
	::_tcscpy_s(szServiceName, _countof(szServiceName) - 1, crt_data.Name().GetString());

	SERVICE_TABLE_ENTRY serviceTable[] = 
	{
		{ szServiceName, CServiceBaseImplEx::ServiceMain },
		{ NULL, NULL }
	};

	return ::StartServiceCtrlDispatcher(serviceTable);
}