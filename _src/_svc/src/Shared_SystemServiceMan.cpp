/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jun-2016 at 2:19:01p, UTC+7, Phuket, Rawai, Wednesday;
	This is System Service Manager Wrapper class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 5:38:54p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "Shared_SystemServiceMan.h"

using namespace shared::service;

#include "generic.stg.data.h"

using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace service { namespace details
{
	bool  ServiceManager_CheckErrorCode(const DWORD dwError)
	{
		return (ERROR_MORE_DATA == dwError 
				|| ERROR_INSUFFICIENT_BUFFER == dwError
					|| ERROR_SUCCESS == dwError);
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CServiceManager::CServiceManager(const SC_HANDLE _handle) : m_handle(_handle)
{
	m_error.Source(_T("CServiceManager"));
}

CServiceManager::~CServiceManager(void)
{
	const bool bIgnoreIfUninited = true;
	this->Uninitialize(bIgnoreIfUninited);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CServiceManager::Attach(const SC_HANDLE _handle)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == _handle)
		return (m_error = E_INVALIDARG);

	if (NULL != m_handle)
	{
		::CloseServiceHandle(m_handle); m_handle = NULL;
	}
	m_handle = _handle;
	return m_error;
}

HRESULT       CServiceManager::Create(const CServiceCrtData& crt_ref, CService& svc_ref)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsValid())
		return (m_error = OLE_E_BLANK);
	if (!crt_ref.IsValid())
		return (m_error = crt_ref.Error());
	if (svc_ref.IsValid())
		return (m_error = ERROR_ALREADY_INITIALIZED);

	svc_ref = ::CreateService(
				m_handle,
				crt_ref.Name(),
				crt_ref.Name()     /*crt_ref.DisplayName()*/,
				SERVICE_ALL_ACCESS /*SERVICE_QUERY_STATUS|SERVICE_CHANGE_CONFIG*/,
				(crt_ref.IsFileSysDriver() ?  SERVICE_FILE_SYSTEM_DRIVER : SERVICE_WIN32_OWN_PROCESS),
				(crt_ref.IsStartOnDemand() ?  SERVICE_DEMAND_START       : SERVICE_AUTO_START       ),
				SERVICE_ERROR_NORMAL,
				crt_ref.Path(),
				NULL  ,
				NULL  ,
				NULL/*_T("")*/,
				NULL  ,//crt_ref.Account(), // uses a local system account by default
				NULL
			);
	if (!svc_ref.IsValid())
		return (m_error = ::GetLastError());

	HRESULT hr_ = svc_ref.Description(crt_ref.Description());
	if (FAILED(hr_))
		m_error = svc_ref.Error();

	return m_error;
}

SC_HANDLE     CServiceManager::Detach(void)
{
	SC_HANDLE handle_ = m_handle; m_handle = NULL;
	m_error = OLE_E_BLANK;
	return handle_;
}

HRESULT       CServiceManager::EnumServices(const DWORD dwStatus)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsValid())
		return (m_error = OLE_E_BLANK);
	if (!m_services.empty())m_services.clear();

	DWORD dwReqSize = sizeof(ENUM_SERVICE_STATUS) * 0xff;
	DWORD dwResumed = 0;
	do
	{
		CRawData raw_data(dwReqSize);
		if (!raw_data.IsValid())
		{
			m_error = raw_data.Error();
			break;
		}
		DWORD dwServices = 0;

		if (!::EnumServicesStatus(
				m_handle,
				SERVICE_DRIVER|SERVICE_WIN32,
				dwStatus,
				reinterpret_cast<LPENUM_SERVICE_STATUS>(raw_data.GetData()),
				raw_data.GetSize(),
				&dwReqSize,
				&dwServices,
				&dwResumed
			))
		{
			m_error = ::GetLastError();
			if (!details::ServiceManager_CheckErrorCode(m_error.GetCode()))
				break;
		}
		if (m_error)
			m_error.Clear(); // removes 'need more data' error code;

		if (!dwServices)
			break;

		const LPENUM_SERVICE_STATUS pServices = reinterpret_cast<LPENUM_SERVICE_STATUS>(raw_data.GetData());

		for (DWORD it_ = 0; it_ < dwServices; it_++)
		{
			const ENUM_SERVICE_STATUS& e_status = pServices[it_];
			CServiceInfo info_(e_status, *this);
			try
			{
				m_services.push_back(info_);
			}catch(::std::bad_alloc&){ m_error = E_OUTOFMEMORY; break; }
		}

	} while(!m_error);

	return m_error;
}

TErrorRef     CServiceManager::Error(void)const
{
	return m_error;
}

HRESULT       CServiceManager::Initialize(const DWORD dwAccessRights, const bool bIgnoreIfInited)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (this->IsValid()){
		if (bIgnoreIfInited == false)
		m_error.SetState(
				(DWORD) ERROR_OBJECT_ALREADY_EXISTS,
				_T("The service manager is already open.")
			);
		return m_error;
	}

	m_handle = ::OpenSCManager(NULL, NULL, dwAccessRights);

	if (NULL == m_handle)
		m_error = ::GetLastError();
	
	return m_error;
}

bool          CServiceManager::IsInstalled(const  CServiceCrtData& _crt)const
{
	return this->IsInstalled(_crt.Name());
}

bool          CServiceManager::IsInstalled(LPCTSTR lpszServiceName)const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	bool bInstalled = false;

	if (!this->IsValid()){
		m_error = OLE_E_BLANK;
		return bInstalled;
	}

	SC_HANDLE service_ = ::OpenService(m_handle, lpszServiceName, GENERIC_READ);
	if (!service_){
		const DWORD code_ = ::GetLastError();
		if (ERROR_SERVICE_DOES_NOT_EXIST != code_)
			m_error = code_;
	}
	else{
		bInstalled = true;
		::CloseServiceHandle(service_); service_ = NULL;
	}

	return bInstalled;
}

bool          CServiceManager::IsValid(void) const
{
	return (NULL != m_handle);
}

HRESULT       CServiceManager::Open(const CServiceCrtData& crt_data, const DWORD dwReason, CService& svc_obj)
{
	HRESULT hr_ = this->Open(crt_data.Name(), dwReason, svc_obj);
	return  hr_;
}

HRESULT       CServiceManager::Open(LPCTSTR lpszServiceName, const DWORD dwReason, CService& svc_obj)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	svc_obj = ::OpenService(m_handle, lpszServiceName, dwReason);
	if (!svc_obj.IsValid()){
		m_error.SetState(
			(DWORD)::GetLastError(),
			_T("Cannot open service: name=%s; reason=%d"), lpszServiceName, dwReason
		);
	}
	return m_error;
}

HRESULT       CServiceManager::Remove(LPCTSTR lpszServiceName)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_svc(lpszServiceName);
	if (cs_svc.IsEmpty())
		return (m_error = E_INVALIDARG);

	if (!this->IsValid())
		return (m_error = OLE_E_BLANK);

	CService svc_obj;
	m_error = this->Open(lpszServiceName, DELETE, svc_obj);
	if (m_error)
		return m_error;

	if (!::DeleteService(svc_obj))
		m_error = ::GetLastError();

	return m_error;
}

const
TServiceList& CServiceManager::Services(void)const
{
	return m_services;
}

HRESULT       CServiceManager::Start(LPCTSTR lpszServiceName)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CService svc_obj;
	HRESULT hr_ = this->Open(lpszServiceName, SERVICE_START, svc_obj);
	if (FAILED(hr_))
		return hr_;

	m_error.Module(_T(__FUNCTION__));

#if defined(_DEBUG_NOT_USED)
	const wchar_t* args_[] = { L"debug" };
	const DWORD    count_  = 1;

	const BOOL bResult = ::StartService(svc_obj, count_, args_);
#else
	const BOOL bResult = ::StartService(svc_obj, 0, NULL);
#endif
	if (!bResult){
		m_error = (DWORD)::GetLastError();
	}
	return m_error;
}

HRESULT       CServiceManager::Stop(LPCTSTR lpszServiceName)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CService svc_obj;
	HRESULT hr_ = this->Open(lpszServiceName, SERVICE_STOP, svc_obj);
	if (FAILED(hr_))
		return hr_;

	SERVICE_STATUS status_ = {0};
	const BOOL bResult = ::ControlService(svc_obj, SERVICE_CONTROL_STOP, &status_);
	if (!bResult)
		m_error = ::GetLastError();

	return m_error;
}

HRESULT       CServiceManager::Uninitialize(const bool bIgnoreIfUninited)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == m_handle){
		if (bIgnoreIfUninited == false)
			m_error.SetState(
				OLE_E_BLANK,
				_T("Service manager is not intialized")
			);
		return m_error;
	}
	else
	{
		::CloseServiceHandle(m_handle); m_handle = NULL;
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CServiceManager::operator SC_HANDLE(void) const
{
	return m_handle;
}

/////////////////////////////////////////////////////////////////////////////

CServiceManager&  CServiceManager::operator= (const SC_HANDLE _handle)
{
	this->Attach(_handle);
	return *this;
}