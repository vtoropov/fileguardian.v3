/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Jan-2015 at 8:58:55p, UTC+3, Taganrog, Sunday;
	This is shared lite library system service wrapper class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 5:36:44p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "Shared_SystemService.h"
#include "generic.stg.data.h"

using namespace shared::lite::data;
using namespace shared::service;

/////////////////////////////////////////////////////////////////////////////

CServiceInfo::CServiceInfo(void) : m_status(0)
{
}

CServiceInfo::CServiceInfo(const ENUM_SERVICE_STATUS& status_ref, const SC_HANDLE sc_mgr) :
	m_status(status_ref.ServiceStatus.dwCurrentState)
{
	this->Name(status_ref.lpServiceName);
	this->Description(status_ref.lpDisplayName);
	if (NULL == sc_mgr)
		return;
	SC_HANDLE sc_handle = ::OpenService(sc_mgr, this->Name(), SERVICE_QUERY_CONFIG);
	if (!sc_handle)
		return;
	DWORD dwBytesNeeded = 0;

	::QueryServiceConfig(sc_handle, NULL, 0, &dwBytesNeeded);
	if (dwBytesNeeded)
	{
		CRawData raw_data(dwBytesNeeded);
		if (raw_data.IsValid())
		{
			if (::QueryServiceConfig(
					sc_handle,
					reinterpret_cast<LPQUERY_SERVICE_CONFIG>(raw_data.GetData()),
					raw_data.GetSize(),
					&dwBytesNeeded))
			{
				const LPQUERY_SERVICE_CONFIG pCfg = reinterpret_cast<LPQUERY_SERVICE_CONFIG>(raw_data.GetData());
				if (pCfg)
					m_group = pCfg->lpLoadOrderGroup;
			}
		}
	}
	::CloseServiceHandle(sc_handle); sc_handle = NULL;
}

CServiceInfo::~CServiceInfo(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR   CServiceInfo::Description(void)const
{
	return m_desc.GetString();
}

HRESULT   CServiceInfo::Description(LPCTSTR lpszDesc)
{
	m_desc = lpszDesc;
	return (m_desc.IsEmpty() ? OLE_E_BLANK : S_OK);
}

bool      CServiceInfo::IsValid(void)const
{
	return !m_name.IsEmpty();
}

bool      CServiceInfo::IsRunning(void)const
{
	return (m_status > SERVICE_STOPPED);
}

LPCTSTR   CServiceInfo::Group(void)const
{
	return m_group.GetString();
}

HRESULT   CServiceInfo::Group(LPCTSTR pszGroup)
{
	m_group = pszGroup;
	return (m_group.IsEmpty() ? OLE_E_BLANK : S_OK);
}

LPCTSTR   CServiceInfo::Name (void)const
{
	return m_name.GetString();
}

HRESULT   CServiceInfo::Name (LPCTSTR pszName)
{
	m_name = pszName;
	return (m_name.IsEmpty() ? OLE_E_BLANK : S_OK);
}

DWORD     CServiceInfo::Status(void)const
{
	return m_status;
}

HRESULT   CServiceInfo::Status(const DWORD dwStatus)
{
	if (!dwStatus)
		return E_INVALIDARG;
	const bool bChanged = (dwStatus != m_status);
	if (bChanged)
		m_status = dwStatus;
	return (bChanged ? S_OK : S_FALSE);
}

/////////////////////////////////////////////////////////////////////////////

CService::CService(SC_HANDLE _handle) : m_handle(NULL)
{
	m_error.Source(_T("CService"));
	*this = _handle;
}

CService::~CService(void)
{
	this->Close();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT        CService::Attach (const SC_HANDLE _handle)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == _handle)
		return (m_error = E_INVALIDARG);

	this->Close();
	m_handle = _handle;
	return m_error;
}

HRESULT        CService::Close(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsValid())
		return m_error;

	if (!::CloseServiceHandle(m_handle))
		return (m_error = ::GetLastError());

	m_handle = NULL;
	m_error.Reset();

	return S_OK;
}

HRESULT        CService::Description(LPCTSTR pszDesc)
{
	m_error.Module(_T(__FUNCTION__));
	if (!this->IsValid())
	{
		m_error.SetState(
				OLE_E_BLANK,
				_T("Service is not initialized") 
			);
		return m_error;
	}

	SERVICE_DESCRIPTION desc_ = {0};

	CAtlString cs_desc(pszDesc);

	desc_.lpDescription = cs_desc.GetBuffer();

	const BOOL bResult = ::ChangeServiceConfig2(
							m_handle,
							SERVICE_CONFIG_DESCRIPTION,
							&desc_
						);
	if (!bResult)
		m_error = ::GetLastError();
	else
		m_error = S_OK;

	return m_error;
}

SC_HANDLE      CService::Detach(void)
{
	SC_HANDLE handle_ = m_handle; m_handle = NULL;
	m_error = OLE_E_BLANK;
	return handle_;
}

TErrorRef      CService::Error(void)const
{
	return m_error;
}

bool           CService::IsValid(void)const
{
	return (NULL != m_handle);
}

const
CServiceState& CService::State  (void)const
{
	return m_state;
}

CServiceState& CService::State  (void)
{
	return m_state;
}

HRESULT        CService::UpdateCurrentState(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	m_state.Error().Module(_T(__FUNCTION__));

	if (!this->IsValid())
		return (m_error = OLE_E_BLANK);

	SERVICE_STATUS_PROCESS status_ = {0};
	DWORD nBufferSize = 0;

	const BOOL res_ = ::QueryServiceStatusEx(
			m_handle,
			SC_STATUS_PROCESS_INFO,
			reinterpret_cast<PBYTE>(&status_),
			sizeof(SERVICE_STATUS_PROCESS),
			&nBufferSize
		);
	if (!res_)
	{
		m_error = ::GetLastError();
		m_state.Identifier(
				CServiceState::eSvcStateUndefined
			);
		m_state.Error() = ::GetLastError();
	}
	else
		m_state.Identifier(
				static_cast<CServiceState::_e>(status_.dwCurrentState)
			);
	if (status_.dwWin32ExitCode)
		m_state.Error() = (DWORD)status_.dwWin32ExitCode;

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CService&      CService::operator=(SC_HANDLE _handle)
{
	this->Attach(_handle);
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CService::operator SC_HANDLE(void) const
{
	return m_handle;
}