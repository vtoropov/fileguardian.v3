/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jun-2016 at 12:00:15p, UTC+7, Phuket, Rawai, Wednesday;
	This is System Service Manager Common Data/Structure definitions implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 27-May-2018 at 6:28:59p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Shared_SystemServiceDefs.h"

using namespace shared::service;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace service { namespace details
{

	CAtlString ServiceSetup_IdToName(const CServiceSetup::_e _id)
	{
		CAtlString cs_install;
		switch (_id)
		{
		case CServiceSetup::eInstalled   : cs_install = _T("Installed"); break;
		case CServiceSetup::eNotInstalled: cs_install = _T("Uninstalled"); break;
		}
		return cs_install;
	}

	CServiceSetup& ServiceSetup_InvalidObject(void)
	{
		static CServiceSetup install_;
		return install_;
	}

}}}

/////////////////////////////////////////////////////////////////////////////

CServiceSetup::CServiceSetup(void) : m_id(CServiceSetup::eNotInstalled), m_name(details::ServiceSetup_IdToName(m_id))
{
}

CServiceSetup::CServiceSetup(const CServiceSetup::_e _id) : m_id(_id), m_name(details::ServiceSetup_IdToName(m_id))
{
}

/////////////////////////////////////////////////////////////////////////////

CServiceSetup::_e
              CServiceSetup::Identifier(void)const
{
	return m_id;
}

VOID          CServiceSetup::Identifier(const CServiceSetup::_e _id)
{
	m_id = _id;
	m_name = details::ServiceSetup_IdToName(m_id);
}

CAtlString    CServiceSetup::Name(void)const
{
	return m_name;
}

VOID          CServiceSetup::Name(LPCTSTR pszName)
{
	m_name = pszName;
}

/////////////////////////////////////////////////////////////////////////////

CServiceSetupEnum::TServiceSetups CServiceSetupEnum::m_setups;

CServiceSetupEnum::CServiceSetupEnum(void)
{
	if (m_setups.empty())
	{
		try
		{
			m_setups.push_back(CServiceSetup(CServiceSetup::eNotInstalled));
			m_setups.push_back(CServiceSetup(CServiceSetup::eInstalled));
		}
		catch(::std::bad_alloc&)
		{}
	}
}

/////////////////////////////////////////////////////////////////////////////

INT           CServiceSetupEnum::Count(void)const
{
	return static_cast<INT>(m_setups.size());
}

const
CServiceSetup&
              CServiceSetupEnum::Item(const INT nIndex) const
{
	if (-1 < nIndex && nIndex < this->Count())
		return m_setups[nIndex];
	else
		return details::ServiceSetup_InvalidObject();
}

/////////////////////////////////////////////////////////////////////////////

CServiceCrtData::CServiceCrtData(void) : m_dwOptions(CServiceCrtOption::eNone)
{
}

CServiceCrtData::~CServiceCrtData(void)
{
}

/////////////////////////////////////////////////////////////////////////////

const
CAtlString&   CServiceCrtData::Account(void)const
{
	return m_account;
}

CAtlString&   CServiceCrtData::Account(void)
{
	return m_account;
}

const
CAtlString&   CServiceCrtData::Description(void)const
{
	return m_desc;
}

CAtlString&   CServiceCrtData::Description(void)
{
	return m_desc;
}

const
CAtlString&   CServiceCrtData::DisplayName(void)const
{
	return m_title;
}

CAtlString&   CServiceCrtData::DisplayName(void)
{
	return m_title;
}

TErrorRef     CServiceCrtData::Error(void)const
{
	return m_error;
}

bool          CServiceCrtData::IsValid(void)const
{
	CAtlString cs_error;
	if (!cs_error.IsEmpty())cs_error.Empty();
	if (false){}
	else if (m_name.IsEmpty())   cs_error = _T("Service name is not specified");
	else if (m_path.IsEmpty())   cs_error = _T("Service executable path is not provided");
	
	if (!cs_error.IsEmpty())
		m_error.SetState(
			(DWORD)ERROR_INVALID_DATA,
			cs_error
		);
	else
		m_error = S_OK;

	return !m_error;
}

bool          CServiceCrtData::IsFileSysDriver(void)const
{
	return 0 != (CServiceCrtOption::eFileSysDriver & m_dwOptions);
}

bool          CServiceCrtData::IsStartOnDemand(void)const
{
	return 0 != (CServiceCrtOption::eStartOnDemand & m_dwOptions);
}

const
CAtlString&   CServiceCrtData::Name(void)const    { return m_name; }
CAtlString&   CServiceCrtData::Name(void)         { return m_name; }
DWORD         CServiceCrtData::Options(void)const { return m_dwOptions; }
DWORD&        CServiceCrtData::Options(void)      { return m_dwOptions; }
const
CAtlString&   CServiceCrtData::Path(void)const    { return m_path; }
CAtlString&   CServiceCrtData::Path(void)         { return m_path; }

/////////////////////////////////////////////////////////////////////////////

CAtlString    CServiceCrtData::ToString(void)const
{
	CAtlString cs_crt;
	cs_crt.Format(
			_T("Account=%s;Desc=%s;Disp=%s;Name=%s;Path=%s"),
			this->Account().GetString(),
			this->Description().GetString(),
			this->DisplayName().GetString(),
			this->Name().GetString(),
			this->Path().GetString()
		);
	return cs_crt;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace service { namespace details
{

	CAtlString ServiceState_IdToName(const CServiceState::_e _id)
	{
		CAtlString cs_name;
		switch (_id)
		{
		case CServiceState::eSvcContinuePending : cs_name = _T("The service is about to continue"); break;
		case CServiceState::eSvcPaused          : cs_name = _T("The service is paused");      break;
		case CServiceState::eSvcPausePending    : cs_name = _T("The service is pausing");     break;
		case CServiceState::eSvcRunning         : cs_name = _T("The service is running");     break;
		case CServiceState::eSvcStartPending    : cs_name = _T("The service is starting");    break;
		case CServiceState::eSvcStopPanding     : cs_name = _T("The service is stopping");    break;
		case CServiceState::eSvcStopped         : cs_name = _T("The service has stopped");    break;
		case CServiceState::eSvcUninstalled     : cs_name = _T("The service is uninstalled"); break;
		case CServiceState::eSvcUninstallPending: cs_name = _T("The service is being uninstalled"); break;
		default:
			 cs_name = _T("Undefined"); break;
		}
		return cs_name;
	}

	CServiceState& ServiceState_InvalidObject(void)
	{
		static CServiceState state_;
		return state_;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CServiceState::CServiceState(void) : m_id(CServiceState::eSvcStateUndefined)
{
	m_error.Source(_T("CServiceState"));
}

CServiceState::CServiceState(const CServiceState::_e _id) : m_id(_id), m_name(details::ServiceState_IdToName(_id))
{
}

CServiceState::CServiceState(const CServiceState::_e _id, LPCTSTR pszName) : m_id(_id), m_name(pszName)
{
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef         CServiceState::Error     (void)const
{
	return m_error;
}

CSysError&        CServiceState::Error(void)
{
	return m_error;
}

CServiceState::_e CServiceState::Identifier(void)const
{
	return m_id;
}

VOID              CServiceState::Identifier(const CServiceState::_e _id)
{
	m_id = _id;
	m_name = details::ServiceState_IdToName(_id);
}

bool              CServiceState::IsRunning   (void)const
{
	return (CServiceState::eSvcRunning == m_id);
}

bool              CServiceState::IsStopped   (void)const
{
	return (CServiceState::eSvcStopped == m_id);
}

bool              CServiceState::IsTransitive(void)const
{
	return (CServiceState::eSvcContinuePending == m_id
			|| CServiceState::eSvcPausePending == m_id
				|| CServiceState::eSvcStartPending == m_id
					|| CServiceState::eSvcStopPanding == m_id);
}

CAtlString        CServiceState::Name(void)const
{
	return m_name;
}

VOID              CServiceState::Name(LPCTSTR pszName)
{
	m_name = pszName;
}

/////////////////////////////////////////////////////////////////////////////

CServiceStateEnum::TServiceStates CServiceStateEnum::m_states;

CServiceStateEnum::CServiceStateEnum(void)
{
	if (m_states.empty())
	{
		try
		{
			m_states.push_back(CServiceState(CServiceState::eSvcStateUndefined));
			m_states.push_back(CServiceState(CServiceState::eSvcStopped));
			m_states.push_back(CServiceState(CServiceState::eSvcStartPending));
			m_states.push_back(CServiceState(CServiceState::eSvcStopPanding));
			m_states.push_back(CServiceState(CServiceState::eSvcRunning));
			m_states.push_back(CServiceState(CServiceState::eSvcContinuePending));
			m_states.push_back(CServiceState(CServiceState::eSvcPausePending));
			m_states.push_back(CServiceState(CServiceState::eSvcPaused));
		}
		catch(::std::bad_alloc&)
		{
		}
	}
}

/////////////////////////////////////////////////////////////////////////////

INT               CServiceStateEnum::Count(void)const
{
	return static_cast<INT>(m_states.size());
}

const
CServiceState&    CServiceStateEnum::Item (const INT nIndex)
{
	if (-1 < nIndex && nIndex < this->Count())
		return m_states[nIndex];
	else
		return details::ServiceState_InvalidObject();
}