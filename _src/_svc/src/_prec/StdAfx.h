#ifndef _SERVICEMANSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED
#define _SERVICEMANSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Apr-2017 at 7:48:05p, GMT+7, Phuket, Rawai, Monday;
	This is Shared system service manager library precompiled headers declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 27-May-2018 at 6:42:29p, UTC+7, Phuket, Rawai, Sunday
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>
#include <comutil.h>
#include <vector>

#endif/*_SERVICEMANSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED*/