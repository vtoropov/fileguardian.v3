/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Apr-2017 at 7:50:44p, GMT+7, Phuket, Rawai, Monday;
	This is Shared system service manager library precompiled headers implementation file.
*/
#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)