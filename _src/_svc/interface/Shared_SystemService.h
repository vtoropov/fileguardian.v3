#ifndef _SHAREDLITELIBRARYSYSTEMSERVICE_H_54EA6A52_EE05_4685_8E50_AB7EDCF70B94_INCLUDED
#define _SHAREDLITELIBRARYSYSTEMSERVICE_H_54EA6A52_EE05_4685_8E50_AB7EDCF70B94_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Jan-2015 on 8:45:58p, UTC+3, Taganrog, Wednesday;
	This is shared system service manager library class(es) declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_SystemServiceDefs.h"

namespace shared { namespace service
{
	using shared::lite::common::CSysError;

	class CServiceInfo
	{
	private:
		::ATL::CAtlString m_name;
		::ATL::CAtlString m_desc;
		::ATL::CAtlString m_group;
		DWORD             m_status;
	public:
		CServiceInfo(void);
		CServiceInfo(const ENUM_SERVICE_STATUS&, const SC_HANDLE = NULL);
		~CServiceInfo(void);
	public:
		LPCTSTR           Description(void)const;
		HRESULT           Description(LPCTSTR);
		bool              IsValid(void)const;
		bool              IsRunning(void)const;
		LPCTSTR           Group(void)const;
		HRESULT           Group(LPCTSTR);
		LPCTSTR           Name (void)const;
		HRESULT           Name (LPCTSTR);
		DWORD             Status(void)const;
		HRESULT           Status(const DWORD);
	};

	typedef ::std::vector<CServiceInfo> TServiceList;

	class CService
	{
	private:
		mutable
		CSysError        m_error;
		SC_HANDLE        m_handle;
		CServiceState    m_state;
	public:
		CService(SC_HANDLE = NULL);
		~CService(void);
	public:
		HRESULT          Attach (const SC_HANDLE);
		HRESULT          Close  (void);
		HRESULT          Description(LPCTSTR);  // sets a service description
		SC_HANDLE        Detach (void);
		TErrorRef        Error  (void)const;
		bool             IsValid(void)const;
		const
		CServiceState&   State  (void)const;
		CServiceState&   State  (void);
		HRESULT          UpdateCurrentState(void);
	public:
		CService&        operator=(SC_HANDLE);
	public:
		operator SC_HANDLE(void) const;
	private:
		CService(const CService&);
		CService& operator= (const CService&);
	};
}}

#endif/*_SHAREDLITELIBRARYSYSTEMSERVICE_H_54EA6A52_EE05_4685_8E50_AB7EDCF70B94_INCLUDED*/