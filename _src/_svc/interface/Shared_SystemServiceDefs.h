#ifndef _SHAREDSYSTEMSERVICEDEFS_H_F6FED3CC_D476_4d88_82AC_06C9BC8745C9_INCLUDED
#define _SHAREDSYSTEMSERVICEDEFS_H_F6FED3CC_D476_4d88_82AC_06C9BC8745C9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jun-2016 at 11:25:42a, UTC+7, Phuket, Rawai, Monday;
	This is Shared system service manager library common data/structure declaration file.
*/
#include <AccCtrl.h>
#include <Aclapi.h>

#include "Shared_SystemError.h"

namespace shared { namespace service
{
	using shared::lite::common::CSysError;

	class CServiceCrtOption
	{
	public:
		enum _e{
			eNone           = 0x00,
			eStoppable      = 0x01,
			eCanShutdown    = 0x02,
			eCanContinue    = 0x04,
			eStartOnDemand  = 0x08,
			eFileSysDriver  = 0x10,
		};
	};

	class CServiceCrtData
	{
	private:
		mutable
		CSysError     m_error;
	private:
		CAtlString    m_account;
		CAtlString    m_name;
		CAtlString    m_title;     // aka display name
		CAtlString    m_path;      // path to a service binary
		CAtlString    m_desc;      // service description
		DWORD         m_dwOptions;
	public:
		CServiceCrtData(void);
		~CServiceCrtData(void);
	public:
		const
		CAtlString&   Account(void)    const;
		CAtlString&   Account(void)         ;
		const
		CAtlString&   Description(void)const;
		CAtlString&   Description(void)     ;
		const
		CAtlString&   DisplayName(void)const;
		CAtlString&   DisplayName(void)     ;
		TErrorRef     Error(void)      const;
		bool          IsFileSysDriver(void)const;
		bool          IsStartOnDemand(void)const;
		bool          IsValid(void)    const;
		const
		CAtlString&   Name(void)       const;
		CAtlString&   Name(void)            ;
		DWORD         Options(void)    const;
		DWORD&        Options(void)         ;
		const
		CAtlString&   Path(void)       const;
		CAtlString&   Path(void)            ;
	public:
		CAtlString    ToString(void)const;
	};

	class CServiceState
	{
	public:
		enum _e{
			eSvcStateUndefined   = 0x0,
			eSvcStopped          = SERVICE_STOPPED,
			eSvcStartPending     = SERVICE_START_PENDING,
			eSvcStopPanding      = SERVICE_STOP_PENDING,
			eSvcRunning          = SERVICE_RUNNING,
			eSvcContinuePending  = SERVICE_CONTINUE_PENDING,
			eSvcPausePending     = SERVICE_PAUSE_PENDING,
			eSvcPaused           = SERVICE_PAUSED,
			eSvcUninstalled      = eSvcPaused + 0x1,
			eSvcUninstallPending = eSvcPaused + 0x2,
		};
	private:
		_e            m_id;
		CAtlString    m_name;
		CSysError     m_error;
	public:
		CServiceState(void);
		CServiceState(const CServiceState::_e);
		CServiceState(const CServiceState::_e, LPCTSTR pszName);
	public:
		TErrorRef         Error       (void)const;
		CSysError&        Error       (void);
		CServiceState::_e Identifier  (void)const;
		VOID              Identifier  (const CServiceState::_e);
		bool              IsRunning   (void)const;
		bool              IsStopped   (void)const;
		bool              IsTransitive(void)const;
		CAtlString        Name(void)const;
		VOID              Name(LPCTSTR);
	};

	class CServiceStateEnum
	{
		typedef ::std::vector<CServiceState> TServiceStates;
	private:
		static
		TServiceStates   m_states;
	public:
		CServiceStateEnum(void);
	public:
		INT              Count(void)const;
		const
		CServiceState&   Item (const INT nIndex);
	};

	class CServiceSetup
	{
	public:
		enum _e{
			eNotInstalled =  0x0,
			eInstalled    =  0x1,
		};
	private:
		_e            m_id;
		CAtlString    m_name;
	public:
		CServiceSetup(void);
		CServiceSetup(const CServiceSetup::_e);
	public:
		CServiceSetup::_e
		              Identifier(void)const;
		VOID          Identifier(const CServiceSetup::_e);
		CAtlString    Name(void)const;
		VOID          Name(LPCTSTR);
	};

	class CServiceSetupEnum
	{
		typedef ::std::vector<CServiceSetup> TServiceSetups;
	private:
		static
		TServiceSetups    m_setups;
	public:
		CServiceSetupEnum(void);
	public:
		INT           Count(void)const;
		const
		CServiceSetup&
		              Item(const INT nIndex) const;
	};

	class CServiceAccessLevel
	{
	public:
		enum {
			eQueryStatus = SERVICE_QUERY_STATUS ,
		};
	};
}}

#endif/*_SHAREDSYSTEMSERVICEDEFS_H_F6FED3CC_D476_4d88_82AC_06C9BC8745C9_INCLUDED*/