#ifndef _SHAREDSYSTEMSERVICEMAN_H_A9A4D670_9050_4562_9D73_9457363B8347_INCLUDED
#define _SHAREDSYSTEMSERVICEMAN_H_A9A4D670_9050_4562_9D73_9457363B8347_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jun-2016 at 2:02:10p, UTC+7, Phuket, Rawai, Monday;
	This is Shared system service manager wrapper class declaration file.
*/
#include "Shared_SystemService.h"
#include "Shared_SystemServiceDefs.h"

namespace shared { namespace service
{
	class CServiceManagerParams
	{
	public:
		enum _e{
			eOpenForService = SC_MANAGER_ALL_ACCESS,
			eOpenForEnum    = SC_MANAGER_ENUMERATE_SERVICE
		};
	};

	class CServiceManager
	{
	private:
		mutable
		CSysError         m_error;
		SC_HANDLE         m_handle;
		TServiceList      m_services;
	public:
		CServiceManager(const SC_HANDLE = NULL);
		~CServiceManager(void);
	public:
		HRESULT           Attach(const SC_HANDLE);
		HRESULT           Create(const CServiceCrtData&, CService&);
		SC_HANDLE         Detach(void);
		HRESULT           EnumServices(const DWORD dwStatus);
		TErrorRef         Error(void)const;
		HRESULT           Initialize (const DWORD dwAccessRights, const bool bIgnoreIfInited = false);
		bool              IsInstalled(const  CServiceCrtData&)const;
		bool              IsInstalled(LPCTSTR lpszServiceName)const;
		bool              IsValid(void) const;
		HRESULT           Open(const  CServiceCrtData&, const DWORD dwReason, CService&);
		HRESULT           Open(LPCTSTR lpszServiceName, const DWORD dwReason, CService&);
		HRESULT           Remove(LPCTSTR lpszServiceName);
		const
		TServiceList&     Services(void)const;
		HRESULT           Start(LPCTSTR lpszServiceName);
		HRESULT           Stop (LPCTSTR lpszServiceName);
		HRESULT           Uninitialize(const bool bIgnoreIfUninited = false);
	public:
		operator SC_HANDLE(void) const;
	private:
		CServiceManager(const CServiceManager&);
		CServiceManager&  operator= (const CServiceManager&);
	public:
		CServiceManager&  operator= (const SC_HANDLE);
	};
}}

#endif/*_SHAREDSYSTEMSERVICEMAN_H_A9A4D670_9050_4562_9D73_9457363B8347_INCLUDED*/