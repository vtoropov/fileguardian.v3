/*
	Created by Tech_dog (VToropov) on 4-Jul-2015 at 3:47:40pm, GMT+7, Phuket, Rawai, Saturday;
	This is Shared Lite Generic XML Data Provider class implementation file.
*/
#include "StdAfx.h"
#include "Shared_GenericXmlProvider.h"
#include "Shared_GenericHandle.h"
#include "generic.stg.data.h"

using namespace shared::lite::common;
using namespace shared::lite::data;

#include <msxml2.h>
#pragma comment(lib, "msxml2.lib")

#include <atlcomcli.h> // CComBSTR

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace data { namespace details
{
	HRESULT  XmlDataProvider_GetNodeName(const ::ATL::CComPtr<IXMLDOMElement>& el_ref, CXmlNode& node_obj);
	HRESULT  XmlDataProvider_GetNodeText(const ::ATL::CComPtr<IXMLDOMElement>& el_ref, CXmlNode& node_obj);

	HRESULT  XmlDataProvider_GetAttributes(const ::ATL::CComPtr<IXMLDOMElement>& el_ref, TXmlAttributes& atts_ref)
	{
		if (!el_ref)
			return E_INVALIDARG;

		::ATL::CComPtr<IXMLDOMNamedNodeMap> root_atts;

		HRESULT hr_ = el_ref->get_attributes(&root_atts);
		if (S_OK != hr_)
			return  hr_;
		LONG lCount = 0;
		hr_ = root_atts->get_length(&lCount);
		if (S_OK != hr_)
			return  hr_;

		for (LONG i_ = 0; i_ < lCount; i_++)
		{
			::ATL::CComPtr<IXMLDOMNode> att_obj;
			hr_ = root_atts->get_item(i_, &att_obj);
			if (S_OK != hr_)
				break;
			_bstr_t b_att_name;
			hr_ = att_obj->get_nodeName(b_att_name.GetAddress());
			if (S_OK != hr_)
				break;
			_variant_t v_value;
			hr_ = att_obj->get_nodeValue(&v_value);
			if (S_OK != hr_)
				break;
			try
			{
				atts_ref.insert(::std::make_pair(
							::ATL::CAtlString((LPCTSTR)b_att_name),
							::ATL::CAtlString(v_value)
						));
			}catch(::std::bad_alloc&)
			{
				hr_ = E_OUTOFMEMORY; break;
			}
		}

		return  hr_;
	}

	HRESULT  XmlDataProvider_GetChildren(const ::ATL::CComPtr<IXMLDOMElement>& el_ref, TXmlNodes& children_ref)
	{
		if (!el_ref)
			return E_INVALIDARG;
		::ATL::CComPtr<IXMLDOMNodeList> node_objs;
		HRESULT hr_ = el_ref->get_childNodes(&node_objs);
		if (S_OK != hr_)
			return  hr_;
		LONG lCount = 0;
		hr_ = node_objs->get_length(&lCount);
		if (S_OK != hr_)
			return  hr_;
		for (LONG i_ = 0; i_ < lCount; i_++)
		{
			::ATL::CComPtr<IXMLDOMNode> child_obj;
			hr_ = node_objs->get_item(i_, &child_obj);
			if (S_OK != hr_)
				break;
			DOMNodeType eType = NODE_INVALID;
			hr_ = child_obj->get_nodeType(&eType);
			if (S_OK != hr_)
				break;
			if (NODE_ELEMENT != eType)
				continue;
			CXmlNode node_obj;

			::ATL::CComQIPtr<IXMLDOMElement> el_obj;
			hr_ = child_obj.QueryInterface(&el_obj);
			if (FAILED(hr_))
				break;
			if (!el_obj){
				hr_ = E_NOINTERFACE; break;
			}
			hr_ = XmlDataProvider_GetAttributes(el_obj, node_obj.Attributes());
			if (S_OK != hr_)
				break;
			hr_ = XmlDataProvider_GetNodeName(el_obj, node_obj);
			if (S_OK != hr_)
				break;
			hr_ = XmlDataProvider_GetNodeText(el_obj, node_obj);
			if (S_OK != hr_)
				break;
			hr_ = XmlDataProvider_GetChildren(el_obj, node_obj.Children());
			if (S_OK != hr_)
				break;
			try
			{
				children_ref.push_back(node_obj);
			}
			catch(::std::bad_alloc&)
			{
				hr_ = E_OUTOFMEMORY;
				break;
			}
		}
		return  hr_;
	}

	HRESULT  XmlDataProvider_GetDocumentObject(::ATL::CComPtr<IXMLDOMDocument2>& doc_obj)
	{
		CSysError err_obj;
		static CLSID cls_ids[] = {
#if defined(CLSID_DOMDocument60)
			CLSID_DOMDocument60,
#endif
#if defined(CLSID_DOMDocument40)
			CLSID_DOMDocument40,
#endif
			CLSID_DOMDocument30, CLSID_DOMDocument26};

		for (INT it_ = 0; it_ < _countof(cls_ids); it_++)
		{
			err_obj = doc_obj.CoCreateInstance(cls_ids[it_]);
			if (!err_obj)
				break;
		}
		return err_obj;
	}

	HRESULT  XmlDataProvider_GetNodeName(const ::ATL::CComPtr<IXMLDOMElement>& el_ref, CXmlNode& node_obj)
	{
		if (!el_ref)
			return E_INVALIDARG;
		_bstr_t bs_name;
		HRESULT hr_ = el_ref->get_nodeName(bs_name.GetAddress());
		if (S_OK == hr_)
			node_obj.Name((LPCTSTR)bs_name);
		return  hr_;
	}

	HRESULT  XmlDataProvider_GetNodeText(const ::ATL::CComPtr<IXMLDOMElement>& el_ref, CXmlNode& node_obj)
	{
		if (!el_ref)
			return E_INVALIDARG;
		::ATL::CComPtr<IXMLDOMNode> child_;
		HRESULT hr_ = el_ref->get_firstChild(&child_);
		if (S_OK != hr_)
			return (hr_ = S_OK); // no children

		DOMNodeType eType = NODE_INVALID;
		hr_ = child_->get_nodeType(&eType);
		if (S_OK != hr_)
			return  hr_;

		if (NODE_TEXT != eType)
			return  hr_; // no text node

		variant_t val_;
		hr_ = child_->get_nodeValue(&val_);
		if (S_OK != hr_)
			return  hr_;

		if (VT_BSTR == val_.vt && NULL != val_.bstrVal)
		{
			CAtlString cs_val(val_.bstrVal);
			node_obj.Text(cs_val.GetString());
		}

		return  hr_;
	}

	HRESULT  XmlDataProvider_ReadRawData(LPCTSTR pszXmlFilePath, CAtlString& raw_data)
	{
		CSysError err_obj;

		CAutoHandle hFile = ::CreateFile(
				pszXmlFilePath,
				GENERIC_READ,
				FILE_SHARE_READ,
				NULL,
				OPEN_EXISTING,
				FILE_ATTRIBUTE_NORMAL,
				NULL
			);

		if (!hFile.IsValid())
			return (err_obj = ::GetLastError());

		const DWORD dwSize  = ::GetFileSize(hFile, NULL);
		CRawData buffer(dwSize);
		if (!buffer.IsValid())
			return (err_obj = buffer.Error());

		DWORD dwBytesRead = 0;
		if (!::ReadFile(hFile, (LPVOID)buffer.GetData(), buffer.GetSize(), &dwBytesRead, NULL))
			return (err_obj = ::GetLastError());

		::ATL::CAtlStringA cs_data_a((char*)buffer.GetData(), buffer.GetSize());
		raw_data = cs_data_a;

		return err_obj;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CXmlNode::CXmlNode(void)
{
}

CXmlNode::~CXmlNode(void)
{
	this->Clear();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT         CXmlNode::AppendChild (const CXmlNode& _node)
{
	if (!_node.IsValid())
		return E_INVALIDARG;
	HRESULT hr_ = S_OK;
	try
	{
		m_children.push_back(_node);
	}
	catch(::std::bad_alloc&)
	{
		hr_ = E_OUTOFMEMORY;
	}
	return  hr_;
}

CAtlString      CXmlNode::Attribute(LPCTSTR pszName)const
{
	CAtlString cs_value;
	TXmlAttributes::const_iterator it_ = m_atts.find(CAtlString(pszName));
	if (it_ != m_atts.end())
		cs_value = it_->second;
	return cs_value;
}

LONG            CXmlNode::Attribute(LPCTSTR pszName, const LONG lDefault)const
{
	CAtlString cs_att = this->Attribute(pszName);
	if (cs_att.IsEmpty())
		return lDefault;
	else
		return ::_tstol(cs_att.GetString());
}

HRESULT         CXmlNode::Attribute(LPCTSTR pszName, const LONG lValue, bool bCreate)
{
	CAtlString cs_value;
		cs_value.Format(
				_T("%d"), lValue
			);

		HRESULT hr_ = this->Attribute(pszName, cs_value.GetString(), bCreate);
	return  hr_;
}

HRESULT         CXmlNode::Attribute(LPCTSTR pszName, LPCTSTR lpszValue, const bool bCreate)
{
	CAtlString cs_name(pszName);
	if (cs_name.IsEmpty())
		return E_INVALIDARG;

	CAtlString cs_value(lpszValue);

	TXmlAttributes::iterator it_ = m_atts.find(CAtlString(pszName));
	if (it_ == m_atts.end())
	{
		if (bCreate)
		{
			try
			{
			m_atts.insert(
				::std::make_pair(cs_name, cs_value)
				);
			}
			catch(::std::bad_alloc&)
			{
				return E_OUTOFMEMORY;
			}
		}
		else
			return TYPE_E_ELEMENTNOTFOUND;
	}
	else
		it_->second = cs_value;
	return S_OK;
}

bool            CXmlNode::AttributeToBoolean(LPCTSTR pszName, const bool bDefault)const
{
	CAtlString cs_att = this->Attribute(pszName);
	if (cs_att.IsEmpty())
		return bDefault;
	else if (0 == cs_att.CompareNoCase(_T("true")))
		return true;
	else if (0 == cs_att.CompareNoCase(_T("false")))
		return false;
	else
		return 0 != ::_tstol(cs_att.GetString());
}

const
TXmlAttributes& CXmlNode::Attributes(void)const
{
	return m_atts;
}

TXmlAttributes& CXmlNode::Attributes(void)
{
	return m_atts;
}

const
CXmlNode&       CXmlNode::Child(LPCTSTR pszName)const
{
	if (::_tcslen(pszName))
	{
		const TXmlNodes& children = this->Children();
		for (size_t i_ = 0; i_ < children.size(); i_++)
		{
			const CXmlNode& node_ = children[i_];
			CAtlString cs_name = node_.Name();
			if (0 == cs_name.CompareNoCase(pszName))
				return node_;
		}
	}
	static CXmlNode na_;
	return na_;
}

CXmlNode&       CXmlNode::Child(LPCTSTR pszName)
{
	if (::_tcslen(pszName))
	{
		TXmlNodes& children = this->Children();
		for (size_t i_ = 0; i_ < children.size(); i_++)
		{
			CXmlNode& node_ = children[i_];
			CAtlString cs_name = node_.Name();
			if (0 == cs_name.CompareNoCase(pszName))
				return node_;
		}
	}
	static CXmlNode na_;
	return na_;
}

const
TXmlNodes&      CXmlNode::Children(void)const
{
	return m_children;
}

TXmlNodes&      CXmlNode::Children(void)
{
	return m_children;
}

VOID            CXmlNode::Clear(void)
{
	if (!m_atts.empty())m_atts.clear();
	if (!m_children.empty())m_children.clear();
}

CXmlNode*       CXmlNode::Find (LPCTSTR lpszXpath)
{
	CXmlNode* pNode = this;
	INT n_pos = 0;
	CAtlString cs_path(lpszXpath);
	CAtlString cs_node = cs_path.Tokenize(_T("\\"), n_pos);

	while (!cs_node.IsEmpty() && pNode)
	{
		pNode = &(pNode->Child(cs_node));
		if (pNode->IsValid())
			cs_node = cs_path.Tokenize(_T("\\"), n_pos);
		else
			pNode = NULL;
	}

	return pNode;
}

bool            CXmlNode::IsIdentifiable(LPCTSTR lpszName)const
{
	if (!lpszName)
		return false;
	if (!this->IsValid())
		return false;
	return (0 == m_name.CompareNoCase(lpszName));
}

bool            CXmlNode::IsValid(void)const
{
	return (!m_name.IsEmpty());
}

LPCTSTR         CXmlNode::Name(void)const
{
	return m_name;
}

VOID            CXmlNode::Name(LPCTSTR pszName)
{
	m_name = pszName;
}

LPCTSTR         CXmlNode::Text(void)const
{
	return m_text;
}

VOID            CXmlNode::Text(LPCTSTR pszText)
{
	m_text = pszText;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace data { namespace details
{
	HRESULT  XmlDataProvider_SetAttributes(::ATL::CComPtr<IXMLDOMElement>& _el, const CXmlNode& _node)    
	{
		if (!_el)
			return E_INVALIDARG;

		HRESULT hr_ = S_OK;

		const TXmlAttributes& atts_ = _node.Attributes();
		for ( TXmlAttributes::const_iterator it_ = atts_.begin(); it_ != atts_.end(); ++it_)
		{
			hr_ = _el->setAttribute(_bstr_t(it_->first), _variant_t(it_->second.GetString()));
			if (FAILED(hr_))
				break;
		}

		return  hr_;
	}

	HRESULT  XmlDataProvider_SetElement(
						::ATL::CComPtr<IXMLDOMDocument2>& _doc,
						::ATL::CComPtr<IXMLDOMElement>&   _parent, 
						const CXmlNode& _node
					)
	{
		if (!_doc)
			return OLE_E_BLANK;
		if (!_node.IsValid())
			return E_INVALIDARG;

		::ATL::CComPtr<IXMLDOMElement> el_;
		HRESULT hr_ = _doc->createElement(_bstr_t(_node.Name()), &el_);
		if (FAILED(hr_))
			return hr_;

		::ATL::CComPtr<IXMLDOMNode> dm_;
		::ATL::CComQIPtr<IXMLDOMNode>  nd_;
		hr_ = el_.QueryInterface(&nd_);
		if (FAILED(hr_))
			return hr_;
		if (!nd_)
			return (hr_ = E_NOINTERFACE);

		if (_parent)
			hr_ = _parent->appendChild(nd_, &dm_);
		else
			hr_ = _doc->appendChild(nd_, &dm_);

		if (FAILED(hr_))
			return hr_;
		hr_ = XmlDataProvider_SetAttributes(el_, _node);
		if (FAILED(hr_))
			return hr_;
		hr_ = nd_->put_text(_bstr_t(_node.Text()));
		if (FAILED(hr_))
			return hr_;
		const TXmlNodes& children_ = _node.Children();
		for (size_t i_ = 0; i_ < children_.size(); i_++)
		{
			hr_ = XmlDataProvider_SetElement(_doc, el_, children_[i_]);
			if (FAILED(hr_))
				break;
		}
		return hr_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CXmlDataProvider::CXmlDataProvider(void) : m_normalize(true)
{
	m_error.Source(_T("CXmlDataProvider"));
}

CXmlDataProvider::~CXmlDataProvider(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT         CXmlDataProvider::Create(LPCTSTR lpszRawData)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	m_root.Clear();
	::ATL::CComPtr<IXMLDOMDocument2> doc_obj;
	m_error = details::XmlDataProvider_GetDocumentObject(doc_obj);
	if (m_error)
		return m_error;

	doc_obj->put_preserveWhiteSpace(
			(m_normalize ? VARIANT_FALSE : VARIANT_TRUE)
		);

	VARIANT_BOOL vResult = VARIANT_FALSE;
	m_error = doc_obj->loadXML(_bstr_t(lpszRawData), &vResult);
	if (m_error)
		return m_error;

	::ATL::CComPtr<IXMLDOMElement> root_obj;
	m_error = doc_obj->get_documentElement(&root_obj);
	if (m_error)
		return m_error;

	m_error = details::XmlDataProvider_GetNodeName(root_obj, m_root);
	if (m_error)
		return m_error;

	m_error = details::XmlDataProvider_GetAttributes(root_obj, m_root.Attributes());
	if (m_error)
		return m_error;

	m_error = details::XmlDataProvider_GetChildren(root_obj, m_root.Children());
	return m_error;
}

TErrorRef       CXmlDataProvider::Error(void)const
{
	return m_error;
}

HRESULT         CXmlDataProvider::Load (LPCTSTR lpszXmlSourcePath)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_data;

	HRESULT hr_ = details::XmlDataProvider_ReadRawData(lpszXmlSourcePath, cs_data);
	if (FAILED(hr_))
		return  (m_error = hr_);

	hr_ = this->Create(cs_data.GetString());
	return hr_;
}

VOID            CXmlDataProvider::Normalize(const bool _val)
{
	m_normalize = _val;
}

const CXmlNode& CXmlDataProvider::Root (void)const
{
	return m_root;
}

CXmlNode&       CXmlDataProvider::Root (void)
{
	return m_root;
}

HRESULT         CXmlDataProvider::Save  (LPCTSTR lpszXmlTargetPath)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_path(lpszXmlTargetPath);
	if (cs_path.IsEmpty())
		return (m_error = E_INVALIDARG);

	::ATL::CComPtr<IXMLDOMDocument2> doc_obj;
	m_error = details::XmlDataProvider_GetDocumentObject(doc_obj);
	if (m_error)
		return m_error;

	doc_obj->put_preserveWhiteSpace(
			(m_normalize ? VARIANT_FALSE : VARIANT_TRUE)
		);

	::ATL::CComPtr<IXMLDOMElement> pRoot;
	m_error = details::XmlDataProvider_SetElement(doc_obj, pRoot, m_root);
	if (m_error)
		return m_error;

	m_error = doc_obj->save(_variant_t(lpszXmlTargetPath));

	return m_error;
}