/*
	Created by Tech_dog (VToropov) on 22-Jul-2016 at 0:37:43a, GMT+7, Phuket, Rawai, Friday;
	This is Shared Lite Generic XSL Processor class implementation file.
*/
#include "StdAfx.h"
#include "Shared_GenericXslProc.h"

using namespace shared::lite::common;
using namespace shared::lite::data;

#include <msxml2.h>
#pragma comment(lib, "msxml2.lib")

#include <atlcomcli.h> // CComBSTR

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace data { namespace details
{
	HRESULT  XmlDataProcessor_CreateFreeThreadDoc(::ATL::CComPtr<IXMLDOMDocument2>& _doc)
	{
		CSysError err_obj;
		static CLSID cls_ids[] = {
#if defined(CLSID_FreeThreadedDOMDocument60)
			CLSID_FreeThreadedDOMDocument60,
#endif
#if defined(CLSID_FreeThreadedDOMDocument40)
			CLSID_FreeThreadedDOMDocument40,
#endif
			CLSID_FreeThreadedDOMDocument30,
			CLSID_FreeThreadedDOMDocument26
		};

		for (INT it_ = 0; it_ < _countof(cls_ids); it_++)
		{
			err_obj = _doc.CoCreateInstance(cls_ids[it_]);
			if (!err_obj)
				break;
		}
		return err_obj;
	}

	HRESULT  XmlDataProcessor_CreateXslTemplate(::ATL::CComPtr<IXSLTemplate>& _templ)
	{
		CSysError err_obj;
		static CLSID cls_ids[] = {
#if defined(CLSID_XSLTemplate60)
			CLSID_XSLTemplate60,
#endif
#if defined(CLSID_XSLTemplate40)
			CLSID_XSLTemplate40,
#endif
			CLSID_XSLTemplate30, CLSID_XSLTemplate26};

		for (INT it_ = 0; it_ < _countof(cls_ids); it_++)
		{
			err_obj = _templ.CoCreateInstance(cls_ids[it_]);
			if (!err_obj)
				break;
		}
		return err_obj;
	}

	HRESULT  XmlDataProcessor_GetDocumentObject(::ATL::CComPtr<IXMLDOMDocument2>& doc_obj)
	{
		CSysError err_obj;
		static CLSID cls_ids[] = {
#if defined(CLSID_DOMDocument60)
			CLSID_DOMDocument60,
#endif
#if defined(CLSID_DOMDocument40)
			CLSID_DOMDocument40,
#endif
			CLSID_DOMDocument30, CLSID_DOMDocument26};

		for (INT it_ = 0; it_ < _countof(cls_ids); it_++)
		{
			err_obj = doc_obj.CoCreateInstance(cls_ids[it_]);
			if (!err_obj)
				break;
		}
		return err_obj;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CXmlDataProcessor::CXmlDataProcessor(void)
{
	m_error.Source(_T("CXmlDataProcessor"));
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef       CXmlDataProcessor::Error (void)const
{
	return m_error;
}

HRESULT         CXmlDataProcessor::Transform(const CRawData& _raw_xml_data, const CRawData& _raw_xsl_data)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!m_result.IsEmpty()) m_result.Empty();

	if (!_raw_xml_data.IsValid())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_DATA,
				_T("XML raw data is invalid")
			);
		return m_error;
	}
	if (!_raw_xsl_data.IsValid())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_DATA,
				_T("XSL raw data is invalid")
			);
		return m_error;
	}

	::ATL::CComPtr<IXMLDOMDocument2> xml_obj;
	m_error = details::XmlDataProcessor_GetDocumentObject(xml_obj);
	if (m_error)
		return m_error;

	VARIANT_BOOL vResult = VARIANT_FALSE;

	CComBSTR xml_(
			static_cast<INT>(_raw_xml_data.GetSize()),
			reinterpret_cast<LPCSTR>(_raw_xml_data.GetData())
		);

	m_error = xml_obj->loadXML(xml_, &vResult);
	if (m_error)
		return m_error;

	::ATL::CComPtr<IXMLDOMDocument2> xsl_obj;
	m_error = details::XmlDataProcessor_GetDocumentObject(xsl_obj);
	if (m_error)
		return m_error;
	m_error = xsl_obj->put_resolveExternals(VARIANT_TRUE);

	vResult = VARIANT_FALSE;

	CComBSTR xsl_(
			static_cast<INT>(_raw_xsl_data.GetSize()),
			reinterpret_cast<LPCSTR>(_raw_xsl_data.GetData())
		);

	m_error = xsl_obj->loadXML(xsl_, &vResult);
	if (m_error)
		return m_error;

	_bstr_t v_result;
	m_error = xml_obj->transformNode(xsl_obj, v_result.GetAddress());

	m_result = (LPCTSTR)v_result;

	return m_error;
}

HRESULT         CXmlDataProcessor::Transform(const CRawData& _raw_xml_data, const CRawData& _raw_xsl_data, const TXslParams& _params)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	::ATL::CComPtr<IXSLTemplate> pXslTemplate;
	m_error = details::XmlDataProcessor_CreateXslTemplate(pXslTemplate);
	if (m_error)
		return m_error;

	::ATL::CComPtr<IXMLDOMDocument2> pStyleSheet;
	m_error = details::XmlDataProcessor_CreateFreeThreadDoc(pStyleSheet);
	if (m_error)
		return m_error;
	m_error = pStyleSheet->put_resolveExternals(VARIANT_TRUE);
	if (m_error)
		return m_error;

	m_error = pStyleSheet->put_async(VARIANT_FALSE);
	if (m_error)
		return m_error;

	VARIANT_BOOL vResult = VARIANT_FALSE;

	::ATL::CComBSTR xsl_(
			static_cast<INT>(_raw_xsl_data.GetSize()),
			reinterpret_cast<LPCSTR>(_raw_xsl_data.GetData())
		);
	m_error = pStyleSheet->loadXML(xsl_, &vResult);
	if (m_error)
		return m_error;

	m_error = pXslTemplate->putref_stylesheet(pStyleSheet);
	if (m_error)
		return m_error;

	::ATL::CComPtr<IXSLProcessor> pXslProcessor;
	m_error = pXslTemplate->createProcessor(&pXslProcessor);
	if (m_error)
		return m_error;

	::ATL::CComPtr<IXMLDOMDocument2> pXmlData;
	m_error = details::XmlDataProcessor_GetDocumentObject(pXmlData);
	if (m_error)
		return m_error;

	CComBSTR xml_(
			static_cast<INT>(_raw_xml_data.GetSize()),
			reinterpret_cast<LPCSTR>(_raw_xml_data.GetData())
		);
	m_error = pXmlData->loadXML(xml_, &vResult);
	if (m_error)
		return m_error;

	m_error = pXslProcessor->put_input(_variant_t(pXmlData));
	if (m_error)
		return m_error;

	if (!_params.empty())
	{
		for (TXslParams::const_iterator it_ = _params.begin(); it_ != _params.end(); ++it_)
		{
			m_error = pXslProcessor->addParameter(
						_bstr_t(it_->first),
						it_->second
				);
			if (m_error)
				return m_error;
		}
	}
	m_error = pXslProcessor->transform(&vResult);
	if (m_error)
		return m_error;

	_variant_t vOutput;
	m_error = pXslProcessor->get_output(&vOutput);
	if (m_error)
		return m_error;

	if (vOutput.vt != VT_BSTR)
		m_error.SetState(
			DISP_E_TYPEMISMATCH,
			_T("Transform output has incorrect data type")
		);
	else
		m_result = vOutput.bstrVal;

	return m_error;
}

LPCTSTR         CXmlDataProcessor::Result(void)const
{
	return m_result;
}