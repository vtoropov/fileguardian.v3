/*
	Created by Tech_dog (VToropov) on 5-Aug-2016 at 1:36:29p, GMT+7, Phuket, Rawai, Friday;
	This is Shared Lite Generic XML Resolver class implementaion file.
	-----------------------------------------------------------------------------
	Adopted to v15 29-May-2019 at 12:10:00a, UTC+7, Phuket, Rawai, Tuesday; 
*/
#include "StdAfx.h"
#include "Shared_GenericXmlResolver.h"

using namespace shared::lite::data;

#include "Shared_GenericAppResource.h"

using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace data { namespace details
{
	VOID CXmlResolver_RemoveXslDocNode(CAtlStringA& _raw_xsl)
	{
		INT n_pos = _raw_xsl.Find("<xsl:stylesheet");
		if (n_pos!= -1)
		{
			n_pos = _raw_xsl.Find(">", n_pos + 1);
			if (n_pos != -1)
				_raw_xsl = _raw_xsl.Mid(n_pos + 1);
		}
		n_pos = _raw_xsl.Find("</xsl:stylesheet");
		if (n_pos != -1)
			_raw_xsl = _raw_xsl.Left(n_pos);
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CXmlResolver::CXmlResolver(void)
{
	m_error.Source(_T("CXmlResolver"));
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef    CXmlResolver::Error (void)const
{
	return m_error;
}

HRESULT      CXmlResolver::ResolveImport(LPCTSTR lpszURL, const UINT _resId, CRawData& _utf8_raw_xml)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!_utf8_raw_xml.IsValid())
		return m_error = _utf8_raw_xml.Error();

	CAtlString cs_import;
	cs_import.Format(
			_T("<xsl:import href=\"%s\"/>"),
			lpszURL
		);

	CAtlStringA cs_import_a = cs_import.GetString();

	CRawData resolved_;

	CGenericResourceLoader loader_;
	HRESULT hr_ = loader_.LoadRcDataAsUtf8(_resId, resolved_);
	if (FAILED(hr_))
		return (m_error = loader_.Error());

	CAtlStringA cs_fragment_a;
	hr_ = resolved_.ToStringUtf8(cs_fragment_a);
	if (FAILED(hr_))
		return (m_error = resolved_.Error());
	details::CXmlResolver_RemoveXslDocNode(cs_fragment_a);

	CAtlStringA cs_xml_a;
	hr_ = _utf8_raw_xml.ToStringUtf8(cs_xml_a);
	if (FAILED(hr_))
		return (m_error = _utf8_raw_xml.Error());

	cs_xml_a.Replace(cs_import_a, cs_fragment_a);

	hr_ = _utf8_raw_xml.Create(
			reinterpret_cast<PBYTE>(cs_xml_a.GetBuffer()),
			static_cast<DWORD>(cs_xml_a.GetLength())
		);
	if (FAILED(hr_))
		return (m_error = _utf8_raw_xml.Error());

	return m_error;
}