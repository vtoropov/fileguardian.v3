#ifndef _SHAREDGENERICXSLPROC_H_09B1AD5C_0428_4eb2_BBA7_3882DBC0FA77_INCLUDED
#define _SHAREDGENERICXSLPROC_H_09B1AD5C_0428_4eb2_BBA7_3882DBC0FA77_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Jul-2016 at 0:37:43a, GMT+7, Phuket, Rawai, Friday;
	This is Shared Lite Generic XSL Processor class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 29-May-2018 at 8:27:17a, UTC+7, Phuket, Rawai, Tuesday;
*/
#include "Shared_SystemError.h"
#include "generic.stg.data.h"

namespace shared { namespace lite { namespace data
{
	using shared::lite::common::CSysError;
	using shared::lite::data::CRawData;

	typedef ::std::map<CAtlString, _variant_t> TXslParams;

	class CXmlDataProcessor
	{
	private:
		CSysError       m_error;
		CAtlString      m_result;
	public:
		CXmlDataProcessor(void);
	public:
		TErrorRef       Error (void)const;
		HRESULT         Transform(const CRawData& _raw_xml_data, const CRawData& _raw_xsl_data); // it is assumed the raw data in UTF8 encoding
		HRESULT         Transform(const CRawData& _raw_xml_data, const CRawData& _raw_xsl_data, const TXslParams&);
		LPCTSTR         Result(void)const;
	};
}}}

#endif/*_SHAREDGENERICXSLPROC_H_09B1AD5C_0428_4eb2_BBA7_3882DBC0FA77_INCLUDED*/