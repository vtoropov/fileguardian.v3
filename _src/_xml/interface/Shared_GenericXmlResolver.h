#ifndef _SHAREDGENERICXMLRESOLVER_H_C8E86C05_2F8B_44b5_8D29_52DAD4DCE458_INCLUDED
#define _SHAREDGENERICXMLRESOLVER_H_C8E86C05_2F8B_44b5_8D29_52DAD4DCE458_INCLUDED
/*
	Created by Tech_dog (VToropov) on 5-Aug-2016 at 1:19:49p, GMT+7, Phuket, Rawai, Friday;
	This is Shared Lite Generic XML Resolver class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 29-May-2018 at 0:01:51a, UTC+7, Phuket, Rawai, Tuesday;
*/
#include "Shared_SystemError.h"
#include "generic.stg.data.h"

namespace shared { namespace lite { namespace data
{
	using shared::lite::common::CSysError;
	using shared::lite::data::CRawData;

	class CXmlResolver
	{
	private:
		CSysError       m_error;
		CAtlString      m_result;
	public:
		CXmlResolver(void);
	public:
		TErrorRef       Error (void)const;
		HRESULT         ResolveImport(LPCTSTR lpszURL, const UINT _resId, CRawData& _utf8_raw_xml); // resolves specified import URL to resource identifier
	};
}}}

#endif/*_SHAREDGENERICXMLRESOLVER_H_C8E86C05_2F8B_44b5_8D29_52DAD4DCE458_INCLUDED*/