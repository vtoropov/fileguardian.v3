#ifndef _SHAREDLITEGENERICXMLPROVIDER_H_3CD8F42A_931B_46b3_AEA5_8C91C02BE075_INCLUDED
#define _SHAREDLITEGENERICXMLPROVIDER_H_3CD8F42A_931B_46b3_AEA5_8C91C02BE075_INCLUDED
/*
	Created by Tech_dog (VToropov) on 4-Jul-2015 at 3:36:56pm, GMT+7, Phuket, Rawai, Saturday;
	This is Shared Lite Generic XML Data Provider class declaration file.
*/
#include "Shared_SystemError.h"

namespace shared { namespace lite { namespace data
{
	using shared::lite::common::CSysError;

	class CXmlNode;
	typedef ::std::map<CAtlString, CAtlString> TXmlAttributes;
	typedef ::std::vector<CXmlNode>            TXmlNodes;

	class CXmlNode
	{
	private:
		CAtlString      m_text;
		CAtlString      m_name;
		TXmlAttributes  m_atts;
		TXmlNodes       m_children;
	public:
		CXmlNode(void);
		~CXmlNode(void);
	public:
		HRESULT         AppendChild(const CXmlNode&);
		CAtlString      Attribute(LPCTSTR pszName) const;                      // gets attribute value by attribute name
		LONG            Attribute(LPCTSTR pszName, const LONG lDefault)const;  // gets attribute value as long data type
		HRESULT         Attribute(LPCTSTR pszName, const LONG lValue, const bool bCreate); // sets attribute value as long data type
		HRESULT         Attribute(LPCTSTR pszName, LPCTSTR lpszValue, const bool bCreate); // sets attribute value as string data type
		bool            AttributeToBoolean(LPCTSTR pszName, const bool bDefault)const;     // gets attribute value as boolean data type
		const
		TXmlAttributes& Attributes(void)const;
		TXmlAttributes& Attributes(void);
		const
		CXmlNode&       Child(LPCTSTR pszName)const;
		CXmlNode&       Child(LPCTSTR pszName);
		const
		TXmlNodes&      Children(void)const;
		TXmlNodes&      Children(void);
		VOID            Clear(void);
		CXmlNode*       Find (LPCTSTR lpszXpath);
		bool            IsIdentifiable(LPCTSTR lpszName)const;
		bool            IsValid(void) const;
		LPCTSTR         Name(void)const;
		VOID            Name(LPCTSTR);
		LPCTSTR         Text(void)const;
		VOID            Text(LPCTSTR);
	};

	class CXmlDataProvider
	{
	private:
		CSysError       m_error;
		CXmlNode        m_root;
		bool            m_normalize; // if true (by default), the formatting (white spaces) is not preserved
	public:
		CXmlDataProvider(void);
		~CXmlDataProvider(void);
	public:
		HRESULT         Create(LPCTSTR lpszRawData);
		TErrorRef       Error (void)const;
		VOID            Normalize(const bool);
		HRESULT         Load  (LPCTSTR lpszXmlSourcePath);
		const
		CXmlNode&       Root  (void)const;
		CXmlNode&       Root  (void);
		HRESULT         Save  (LPCTSTR lpszXmlTargetPath);
	};
}}}

#endif/*_SHAREDLITEGENERICXMLPROVIDER_H_3CD8F42A_931B_46b3_AEA5_8C91C02BE075_INCLUDED*/