/*
	Created by Tech_dog (VToropov) on 31-Aug-2014 on 00:14:15am, GMT+4, Taganrog, Sunday;
	This is BotRevolt Application Protection View class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 8:40:31p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "fg.um.test.8.evt.uix.grd.frm.h"

using namespace fg::um::test;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace um { namespace test { namespace details {

	class CTest_8_EvtMainForm_Layout {
	public:
		static RECT   CenterArea(const RECT& _rc) {

			const RECT  screen_ = CTest_8_EvtMainForm_Layout::GetAvailableArea();
			const POINT left_top = {
				(screen_.right - screen_.left) / 2  - (_rc.right - _rc.left) / 2,
				(screen_.bottom - screen_.top) / 2  - (_rc.bottom - _rc.top) / 2,
			};
			RECT center_ = {
				left_top.x,
				left_top.y,
				left_top.x + (_rc.right - _rc.left),
				left_top.y + (_rc.bottom - _rc.top)
			};

			return center_;
		}
	private:
		static RECT   GetAvailableArea(void)
		{
			const POINT ptZero = {0};
			const HMONITOR hMonitor = ::MonitorFromPoint(ptZero, MONITOR_DEFAULTTOPRIMARY);
			MONITORINFO mInfo  = {0};
			mInfo.cbSize = sizeof(MONITORINFO);
			::GetMonitorInfo(hMonitor, &mInfo);
			return mInfo.rcWork;
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CTest_8_EvtMainForm::CTest_8_EvtMainFormWnd::CTest_8_EvtMainFormWnd(void) : m_pGrid(NULL) { }

CTest_8_EvtMainForm::CTest_8_EvtMainFormWnd::~CTest_8_EvtMainFormWnd(void) { }

/////////////////////////////////////////////////////////////////////////////

LRESULT   CTest_8_EvtMainForm::CTest_8_EvtMainFormWnd::OnCreate (UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	wParam; lParam; bHandled = TRUE;
	RECT rc_ = {0};
	TWindow::GetClientRect(&rc_);
	HRESULT hr_ = flex_grd::CreateFlexGrid(*this, rc_, m_pGrid);
	if (S_OK == hr_)
	{
		flex_grd::IColumns& cols = m_pGrid->Columns();
		cols.Append(1, _T("Timestamp")       , 130);
		cols.Append(2, _T("File/Folder")     , 400);
		cols.Append(3, _T("Action")          , 120);
	}
	return 0;
}

LRESULT   CTest_8_EvtMainForm::CTest_8_EvtMainFormWnd::OnDestroy(UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	wParam; lParam; bHandled = FALSE;
	if (m_pGrid)
		flex_grd::DestroyFlexGrid(m_pGrid);
	return 0;
}

LRESULT   CTest_8_EvtMainForm::CTest_8_EvtMainFormWnd::OnSize   (UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	wParam; lParam; bHandled = FALSE;
	RECT rc_ = {0, 0, LOWORD(lParam), HIWORD(lParam)};
	if (m_pGrid)
		m_pGrid->UpdateLayout(rc_);
	return 0;
}

LRESULT   CTest_8_EvtMainForm::CTest_8_EvtMainFormWnd::OnSysCmd (UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE: {
		::PostQuitMessage(0);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CTest_8_EvtMainForm::CTest_8_EvtMainForm(void) { }
CTest_8_EvtMainForm::~CTest_8_EvtMainForm(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CTest_8_EvtMainForm::Create(const HWND hParent, const RECT& rcArea)
{
	if (m_wnd.IsWindow())
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	if (::IsRectEmpty(&rcArea))
		return OLE_E_INVALIDRECT;

	RECT rc_ = details::CTest_8_EvtMainForm_Layout::CenterArea(rcArea);
	HWND hView = m_wnd.Create(hParent, &rc_, NULL, WS_VISIBLE|WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN);
	if (!hView)
		return HRESULT_FROM_WIN32(::GetLastError());
	else
		return S_OK;
}

HRESULT       CTest_8_EvtMainForm::Destroy(void)
{
	if (m_wnd.IsWindow())
		m_wnd.SendMessage(WM_CLOSE);
	return S_OK;
}

HRESULT       CTest_8_EvtMainForm::UpdateLayout(const RECT& rcArea)
{
	if (::IsRectEmpty(&rcArea))
		return OLE_E_INVALIDRECT;
	if (!m_wnd.IsWindow())
		return OLE_E_BLANK;
	RECT rc_ = rcArea;
	m_wnd.SetWindowPos(NULL, &rc_, SWP_NOZORDER|SWP_NOACTIVATE);
	return S_OK;
}