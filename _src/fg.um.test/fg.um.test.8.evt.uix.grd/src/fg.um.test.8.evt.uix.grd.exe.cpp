/*
	Created by Tech_dog (VToropov) on 15-Jan-2016 at 2:00:32am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian Desktop Application Entry Point implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 6:11:43p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "fg.um.test.8.evt.uix.grd.frm.h"

using namespace fg::um::test;

#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;

#include "Shared_SystemCore.h"

using namespace shared::lite::sys_core;

CAppModule _Module;
/////////////////////////////////////////////////////////////////////////////

INT RunModal(VOID) {
	INT nRet = 0; return nRet;
}

INT RunModeless(VOID)
{
	INT result_ = 0;
	{

		::WTL::CMessageLoop pump_;
		_Module.AddMessageLoop(&pump_);

		RECT rc_def_ = {0, 0, 700, 400};

		CTest_8_EvtMainForm  frame;
		const HRESULT hr_ = frame.Create(HWND_DESKTOP, rc_def_);
		if (SUCCEEDED(hr_))
		{
			result_ = pump_.Run();
		}
		_Module.RemoveMessageLoop();
	}
	return result_;
}

INT WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpstrCmdLine, INT nCmdShow)
{
	hInstance; hPrevInstance; lpstrCmdLine; nCmdShow;

	CCoInitializer com(false);
	CCoSecurityProvider sec_;

	HRESULT hr_ = sec_.InitDefailt();
	ATLVERIFY(SUCCEEDED(hr_));
	/*
		Tech_dog commented on 09-Feb-2010 at 12:47:50pm:
		________________________________________________
		we need to assign lib id to link ATL DLL statically,
		otherwise we get annoying fkn message "Did you forget to pass the LIBID to CComModule::Init?"
	*/
	_Module.m_libid = LIBID_ATLLib;
	INT nResult = 0;
	// this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
	::DefWindowProc(NULL, 0, 0, 0L);

	AtlInitCommonControls(ICC_BAR_CLASSES);	// add flags to support other controls

	hr_ = _Module.Init(NULL, hInstance);
	if(!SUCCEEDED(hr_))
	{
		ATLASSERT(FALSE);
		return -1;
	}

	ex_ui::draw::CGdiPlusLibLoader  gdi_loader;
#if defined(_MODAL)
	nResult = ::RunModal();
#else
	nResult = ::RunModeless();
#endif
	_Module.Term();

	return nResult;
}