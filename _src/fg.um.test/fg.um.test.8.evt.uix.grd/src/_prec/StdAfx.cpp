/*
	Created by Tech_dog (VToropov) on 2-Feb-2016 at 10:08:34pm, GMT+7, Phuket, Rawai, Tuesday;
	This is File Watcher system tray agent precompiled headers implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 7:46:27p, UTC+7, Phuket, Rawai, Sunday;
*/

#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)