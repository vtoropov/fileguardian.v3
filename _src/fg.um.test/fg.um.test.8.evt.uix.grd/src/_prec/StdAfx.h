#ifndef _FWAGENTPRECOMPILEDHEADER_H_763B71B5_DDC9_4480_A737_C1100328D3D4_INCLUDED
#define _FWAGENTPRECOMPILEDHEADER_H_763B71B5_DDC9_4480_A737_C1100328D3D4_INCLUDED
/*
	Created by Tech_dog (VToropov) on 2-Feb-2016 at 10:02:56pm, GMT+7, Phuket, Rawai, Tuesday;
	This is File Watcher system tray agent precompiled headers definition file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 7:43:02p, UTC+7, Phuket, Rawai, Sunday;
*/
#ifndef WINVER                  // Specifies that the minimum required platform is Windows Vista.
#define WINVER 0x0600           // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT            // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINNT 0x0600     // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINDOWS          // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINDOWS 0x0600   // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE               // Specifies that the minimum required platform is Internet Explorer 7.0.
#define _WIN32_IE 0x0700        // Change this to the appropriate value to target other versions of IE.
#endif


#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe
#pragma warning(disable: 4458) // declaration of 'nativeCap' hides class member (GDI+);

#include <atlbase.h>
#include <atlstr.h>             // important order, this file must be included before any includes of the WTL headers

#include <atlapp.h>

extern CAppModule _Module;

#include <atlwin.h>

#include <comdef.h>
#include <comutil.h>

#include <map>
#include <vector>
#include <conio.h>

#pragma comment(lib, "_uix.grid_v15.lib")
#pragma comment(lib, "__shared.lite_v15.lib")

#endif/*_FWAGENTPRECOMPILEDHEADER_H_763B71B5_DDC9_4480_A737_C1100328D3D4_INCLUDED*/