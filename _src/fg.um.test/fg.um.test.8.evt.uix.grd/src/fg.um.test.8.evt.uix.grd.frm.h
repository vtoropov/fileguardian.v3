#ifndef __FGUMTEST8EVTUIXGRDFRM_H_DFF7313A_9C01_46B4_8BD1_E8DCED919AB7_INCLUDED
#define __FGUMTEST8EVTUIXGRDFRM_H_DFF7313A_9C01_46B4_8BD1_E8DCED919AB7_INCLUDED
/*
	Created by Tech_dog (VToropov) on 31-Aug-2014 on 00:00:57am, GMT+4, Taganrog, Sunday;
	This is BotRevolt Application Protection View class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 8:36:46p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "Flex_grd_Ifaces.h"

namespace fg { namespace um { namespace test {

	using flex_grd::IFlex_Grid;
	
	class CTest_8_EvtMainForm
	{
	private:
		class CTest_8_EvtMainFormWnd :
			public  ::ATL::CWindowImpl<CTest_8_EvtMainFormWnd>
		{
			typedef ::ATL::CWindowImpl<CTest_8_EvtMainFormWnd> TWindow;
		private:
			IFlex_Grid*      m_pGrid;
		public:
			DECLARE_WND_CLASS(_T("fg::um::test::8::evt::grd::MainForm"));
			BEGIN_MSG_MAP(CTest_8_EvtMainFormWnd)
				MESSAGE_HANDLER(WM_CREATE     , OnCreate )
				MESSAGE_HANDLER(WM_DESTROY    , OnDestroy)
				MESSAGE_HANDLER(WM_SIZE       , OnSize   )
				MESSAGE_HANDLER(WM_SYSCOMMAND , OnSysCmd )
			END_MSG_MAP()
		private:
			LRESULT   OnCreate (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnDestroy(UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnSize   (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnSysCmd (UINT, WPARAM, LPARAM, BOOL&);
		public:
			CTest_8_EvtMainFormWnd(void);
			~CTest_8_EvtMainFormWnd(void);
		};
	private:
		CTest_8_EvtMainFormWnd   m_wnd;
	public:
		CTest_8_EvtMainForm(void);
		~CTest_8_EvtMainForm(void);
	public:
		HRESULT       Create(const HWND hParent, const RECT& rcArea);
		HRESULT       Destroy(void);
		HRESULT       UpdateLayout(const RECT& rcArea);
	};

}}}

#endif __FGUMTEST8EVTUIXGRDFRM_H_DFF7313A_9C01_46B4_8BD1_E8DCED919AB7_INCLUDED