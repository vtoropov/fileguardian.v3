#ifndef _FGUMTEST7EVENTSPY_H_6CC73290_A579_4F11_8E27_517FE99E98AE_INCLUDED
#define _FGUMTEST7EVENTSPY_H_6CC73290_A579_4F11_8E27_517FE99E98AE_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jun-2018 at 4:58:20a, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver event spy connection test interface declaration file. 
*/
#include "fg.um.test.0._Iface.h"

namespace fg { namespace um { namespace test {

	using shared::lite::common::CSysError;

	class CTest_7_EvtSpy : public CTest_0__Iface {

		typedef CTest_0__Iface TBase;

	public:
		CTest_7_EvtSpy(ITestModuleConsoleCallback&);
		~CTest_7_EvtSpy(void);

	public:
		virtual HRESULT    Run  (void) override sealed;
		virtual bool       IsApplicable(LPCTSTR lpszArgName = NULL, LPCTSTR lpszArgValue = NULL) override sealed;
	};

}}}

#endif/*_FGUMTEST7EVENTSPY_H_6CC73290_A579_4F11_8E27_517FE99E98AE_INCLUDED*/