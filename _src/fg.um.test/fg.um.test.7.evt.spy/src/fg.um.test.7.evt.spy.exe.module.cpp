/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Jul-2017 on 8:01:04p, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver event sink console test application entry point file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 9-Jun-2018 at 5:47:03a, UTC+7, Phuket, Rawai, Saturday;
*/
#include "StdAfx.h"
#include "Shared_SystemCore.h"

using namespace shared::lite::sys_core;

#include "fg.um.test.0._Iface.h"
#include "fg.um.test.0.console.h"
#include "fg.um.test.7.evt.spy.h"
#include "fg.um.test.7.evt.spy.resource.h"

#ifndef __OUTPUT_TO_CONSOLE
#define __OUTPUT_TO_CONSOLE
#endif

using namespace fg::test::common;
using namespace fg::test::common::ctrl_flow;

/////////////////////////////////////////////////////////////////////////////

INT _tmain(VOID)
{
	CCoInitializer com(false);
	CCoSecurityProvider sec_;

	HRESULT hr_ = sec_.InitDefailt();
	ATLVERIFY(SUCCEEDED(hr_));

	INT result_ = 0;

	const bool bVerbose = true;
	global::_out::OutputToConsole(true);
	global::_out::VerboseMode(bVerbose);

	const TCommandLine& cmd_line = GetAppObjectRef().CommandLine();
	const bool bIgnoreAwaitArg = true;

	if (false == bIgnoreAwaitArg && false==cmd_line.Has(_T("no_break"))) {
		MessageBox(NULL, _T("Awaiting for connecting debugger"), _T("Break Point"), MB_ICONEXCLAMATION | MB_OK);
	}

	hr_ = ctrl_flow::Event_OnCreate();
	if (FAILED(hr_)){
		return ctrl_flow::Event_OnExit(bVerbose, true);
	}

	CConsoleEvents_Base console_evt;
	fg::um::test::CTest_7_EvtSpy evt_spy_test(console_evt);

	hr_ = evt_spy_test.Run();
	if (FAILED(hr_)) {
		global::_out::LogError(evt_spy_test.Error());
		result_ = ctrl_flow::Event_OnExit(bVerbose, true);
	}
	else
		result_ = ctrl_flow::Event_OnExit(bVerbose, false);

	return result_;
}