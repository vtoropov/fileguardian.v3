#ifndef _FGSTDAFX_H_5A8547C4_8C9E_441A_9D25_87D7969194AC_INCLUDED
#define _FGSTDAFX_H_5A8547C4_8C9E_441A_9D25_87D7969194AC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Jul-2017 at 6:32:43p, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver console test application precompiled header include file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 9-Jun-2018 at 4:53:58a, UTC+7, Phuket, Rawai, Saturday;
*/
#ifndef STRICT
#define STRICT
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>

#ifdef _DEBUG
	#define _ATL_DEBUG_INTERFACES
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif

#include <vector>
#include <map>
#include <queue>
#include <time.h>
#include <typeinfo>

#include "Shared_LogJournal.h"

namespace global
{
	class _out : public shared::log::CEventJournal{};
}

#pragma comment(lib, "fg.generic.shared_v15.lib")
#pragma comment(lib, "fg.um.evt.spy_v15.lib")
#pragma comment(lib, "fg.um.test.0.shared_v15.lib")

#endif/*_FGSTDAFX_H_5A8547C4_8C9E_441A_9D25_87D7969194AC_INCLUDED*/