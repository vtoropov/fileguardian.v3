/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jun-2018 at 5:01:02a, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver event spy connection test interface implementation file. 
*/
#include "StdAfx.h"
#include "fg.um.test.7.evt.spy.h"

using namespace fg::um::test;

#include "fg.um.evt.spy.iface.h"

using namespace fg::um::spy;

/////////////////////////////////////////////////////////////////////////////

CTest_7_EvtSpy::CTest_7_EvtSpy(ITestModuleConsoleCallback& _clbk) : TBase(_clbk, _T("CTest_7_EvtSpy")) {}
CTest_7_EvtSpy::~CTest_7_EvtSpy(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT         CTest_7_EvtSpy::Run  (void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (false == this->IsApplicable()) {
		m_error.SetState(
			(DWORD)ERROR_INVALID_STATE,
			_T("The app is not configured to run [evt.spy] test.")
		);
		return m_error;
	}

	CEvtSpyInstaller installer_;

	HRESULT hr_ = installer_.Install(true);
	if (FAILED(hr_))
		return (m_error = installer_.Error());

	CEvtSpyModule module_;

	hr_ = module_.Connect(true);
	if (FAILED(hr_))
		return (m_error = module_.Error());

	TBase::m_callback.ITestModuleConsole_OnWait(_T("FG event spy module has started and waits for driver notifications..."));

	hr_ = module_.Disconnect();

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

bool     CTest_7_EvtSpy::IsApplicable(LPCTSTR lpszArgName, LPCTSTR lpszArgValue)
{
	return TBase::IsApplicable(lpszArgName, lpszArgValue);
}