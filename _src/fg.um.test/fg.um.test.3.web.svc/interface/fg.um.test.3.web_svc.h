#ifndef _FGUMTEST3WEBSVC_H_7A1DEE8B_B81B_4AA5_9D0D_7268CFA67702_INCLUDED
#define _FGUMTEST3WEBSVC_H_7A1DEE8B_B81B_4AA5_9D0D_7268CFA67702_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 11:20:32a, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian web service test interface declaration file.
*/
#include "fg.um.test.0._Iface.h"
#include "FG_License_Monitor.h"
#include "FG_Service_WebMon.h"

namespace fg { namespace um { namespace test {

	using shared::lite::common::CSysError;
	using fg::service::ILicenseCallback;
	using fg::service::CLicenseState;
	using fg::common::data::CWebMonitor;
	using fg::common::data::CWebMonOpt;

	class CTest_3_WebSvc : public CTest_0__Iface, public ILicenseCallback {

		typedef CTest_0__Iface TBase;

	private:
		bool       m_b_license;
		bool       m_b_upload;
	public:
		CTest_3_WebSvc(ITestModuleConsoleCallback&);
		~CTest_3_WebSvc(void);

	public:
		virtual HRESULT    Run  (void) override sealed;
		virtual bool       IsApplicable(LPCTSTR lpszArgName = NULL, LPCTSTR lpszArgValue = NULL) override sealed;

	public:
		virtual HRESULT    ILicenseCallback_OnChanged(const CLicenseState&) override sealed;

	public:
		VOID       License(const bool); // enables/disables monitoring license thread;
		VOID       Upload (const bool); // enables/disables uploading data thread;
	};
}}}
#endif/*_FGUMTEST3WEBSVC_H_7A1DEE8B_B81B_4AA5_9D0D_7268CFA67702_INCLUDED*/