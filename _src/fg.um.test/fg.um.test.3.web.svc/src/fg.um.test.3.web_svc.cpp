/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 11:24:52a, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian web service test interface implementation file.
*/
#include "StdAfx.h"
#include "fg.um.test.3.web_svc.h"
#include "fg.um.test.0.loc.db.read.h"

using namespace fg::um::test;

#include "FG_License_Info.h"
#include "FG_License_Provider.h"

using namespace fg::common;
using namespace fg::common::lic;
using namespace fg::service;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace um { namespace test { namespace details {

	LPCTSTR    CTest_WebSvc_CmdArg_0(void) {
		static LPCTSTR lpsz_arg_0 = _T("web_svc_url");
		return lpsz_arg_0;
	}

	LPCTSTR    CTest_WebSvc_CmdArg_1(void) {
		static LPCTSTR lpsz_arg_0 = _T("db_path");
		return lpsz_arg_0;
	}

}}}}
/////////////////////////////////////////////////////////////////////////////

CTest_3_WebSvc::CTest_3_WebSvc(ITestModuleConsoleCallback& _clbk) : TBase(_clbk, _T("CTest_3_WebSvc")), m_b_license(true), m_b_upload(true) { }

CTest_3_WebSvc::~CTest_3_WebSvc(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT         CTest_3_WebSvc::Run  (void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (false == this->IsApplicable()) {
		m_error.SetState(
			(DWORD)ERROR_INVALID_STATE,
			_T("The app is not configured to run [web.svc] test.")
		);
		return m_error;
	}

	global::_out::LogInfo(_T("Trying to get license information..."));

	CLicenseInfo lic_;
	CLicenseProvider prov_;

	HRESULT hr_ = prov_.Storage().Load(lic_);
	if (FAILED(hr_)) {
		return (m_error = prov_.Error());
	}

	hr_ = prov_.CheckLicense(
			lic_
	);
	if (FAILED(hr_)) {
		return (m_error = prov_.Error());
	}
	else {
#if defined(_DEBUG)
		global::_out::LogInfo(
			_T("License info:\n\t\t%s"), lic_.ToString(_T("\n\t\t")).GetString()
		);
#endif
	}
	if (lic_.IsActive())
		m_error.SetState(
			(DWORD)NO_ERROR, _T("License state is active.")
		);

	CLicenseMonitor      mon_(*this);
	CTest_0_LocDbRead    read_(TBase::m_callback);
	CWebMonitor          upl_(read_.Provider());   upl_.State(CWebMonOpt::eLogUpload, true);
	

	if (m_b_license) {
		global::_out::LogInfo(_T("Starting license monitor..."));
		mon_.Start();
	}

	if (m_b_upload) {
		global::_out::LogInfo(_T("Starting data uploading..."));
		//
		// TODO: it is necessary to provide exact path to real database file;
		//       otherwise, empty database is automatically created if does not exist;
		//
		CAtlString cs_db = GetAppObjectRef().CommandLine().Argument(details::CTest_WebSvc_CmdArg_1());
		hr_ = read_.Path(cs_db.GetString());
		if (FAILED(hr_)) {
			m_error = read_.Error(); m_b_upload = false; goto __exit_label__;
		}

		hr_ = read_.Run();
		if (FAILED(hr_)) {
			m_error = read_.Error(); m_b_upload = false; goto __exit_label__;
		}

		hr_ = read_.Provider().Writer().UpdateStatus(1, 0);
		hr_ = upl_.Start();
		if (FAILED(hr_)) {
			m_error = upl_.Error();  m_b_upload = false; goto __exit_label__;
		}
	}

	if (m_b_license || m_b_upload) {
		TBase::m_callback.ITestModuleConsole_OnWait(_T("Waiting for current license state..."));
	}

__exit_label__:

	if (m_b_license) { mon_.Stop(); }
	if (m_b_upload ) { upl_.Stop(); }


	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

bool     CTest_3_WebSvc::IsApplicable(LPCTSTR lpszArgName, LPCTSTR lpszArgValue)
{
	lpszArgName; lpszArgValue;
	const TCommandLine& cmd_line = GetAppObjectRef().CommandLine();
#if defined(_DEBUG)
	// no argument is provided; it seems the test is run by this console manually;
	if (0 == cmd_line.Count() || true)
		return true;
#endif
	return cmd_line.Has(details::CTest_WebSvc_CmdArg_0());
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CTest_3_WebSvc::ILicenseCallback_OnChanged(const CLicenseState& _state) {

	global::_out::LogInfo(
			_T("License state: %s"), (LPCTSTR)_state.ToString()
		);

	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

VOID      CTest_3_WebSvc::License(const bool _val) { m_b_license = _val; }
VOID      CTest_3_WebSvc::Upload (const bool _val) { m_b_upload  = _val; }