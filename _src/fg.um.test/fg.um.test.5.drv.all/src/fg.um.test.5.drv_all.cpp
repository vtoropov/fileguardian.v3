/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 9:13:31p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian driver, local database and web service integrity test interface implementation file.
*/
#include "StdAfx.h"
#include "fg.um.test.5.drv_all.h"

using namespace fg::um::test;

#include "Shared_FS_GenericDrive.Monitor.h"

using namespace shared::ntfs;

#include "fg.um.test.0.loc.db.write.h"
#include "fg.um.test.0.drv.con.h"
#include "fg.um.test.5.drv_all.resource.h"

#include "FG_FsBridgeIface.h"
#include "FG_SqlData_Writer.h"

using namespace fg::common::filter;
using namespace fg::common::data::local;

#include "FG_License_Info.h"

using namespace fg::common;
using namespace fg::common::lic;
using namespace fg::service;

#include "FG_Service_WebMon.h"

using namespace fg::common::data;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace um { namespace test { namespace details {

	LPCTSTR    CTest_DrvDb_CmdArg_0(void) {
		static LPCTSTR lpsz_arg_0 = _T("drv_db_url");
		return lpsz_arg_0;
	}
}}}}
/////////////////////////////////////////////////////////////////////////////

CTest_5_DrvAll::CTest_5_DrvAll(ITestModuleConsoleCallback& _clbk) : TBase(_clbk, _T("CTest_5_DrvAll")) { }

CTest_5_DrvAll::~CTest_5_DrvAll(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT         CTest_5_DrvAll::Run  (void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (false == this->IsApplicable()) {
		m_error.SetState(
			(DWORD)ERROR_INVALID_STATE,
			_T("The app is not configured to run [drv.db.all] test.")
		);
		return m_error;
	}

	global::_out::LogInfo(_T("Trying to connect to local database..."));

	CTest_0_LocDbWrite db_writer(TBase::m_callback);

	HRESULT hr_ = db_writer.Run();
	if (FAILED(hr_))
		return (m_error = db_writer.Error());

	db_writer.LogInfo();
	db_writer.Details(true);

	if (db_writer.Schema().IsValid() == false) {
		return (m_error = db_writer.Schema().Error());
	}

	bool b_lic_started = false;
	bool b_web_started = false;

	global::_out::LogInfo(_T("Starting FG license monitor..."));

	CLicenseMonitor lic_mon(*this);

	hr_ = lic_mon.Start();
	if (FAILED(hr_)) {
		return (m_error = lic_mon.Error());
	}
	else
		b_lic_started = true;

	global::_out::LogInfo(_T("Connecting to FG web service..."));

	CWebMonitor web_mon(db_writer.Provider());  web_mon.State(CWebMonOpt::eLogUpload, true);
	CTest_0_DrvConnect drv_con(TBase::m_callback, db_writer.Cache()); drv_con.LogInfo(true);

	hr_ = web_mon.Start();
	if (FAILED(hr_)) {
		m_error = web_mon.Error();  goto __exit_label__;
	}
	else
		b_web_started = true;

	global::_out::LogInfo(_T("Trying to get FG minifilter driver connection..."));

	hr_ = drv_con.Run();  // expects wait state for getting FG driver events;
	if (FAILED(hr_)) {
		m_error = drv_con.Error();
	}

__exit_label__:

	if (b_lic_started) lic_mon.Stop();
	if (b_web_started) web_mon.Stop();

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

bool     CTest_5_DrvAll::IsApplicable(LPCTSTR lpszArgName, LPCTSTR lpszArgValue)
{
	lpszArgName; lpszArgValue;
	const TCommandLine& cmd_line = GetAppObjectRef().CommandLine();
#if defined(_DEBUG)
	// no argument is provided; it seems the test is run by this console manually;
	if (0 == cmd_line.Count())
		return true;
#endif
	return cmd_line.Has(details::CTest_DrvDb_CmdArg_0());
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CTest_5_DrvAll::ILicenseCallback_OnChanged(const CLicenseState& _state) {

	global::_out::LogInfo(
		_T("License state: %s"), (LPCTSTR)_state.ToString()
	);

	HRESULT hr_ = S_OK;
	return  hr_;
}