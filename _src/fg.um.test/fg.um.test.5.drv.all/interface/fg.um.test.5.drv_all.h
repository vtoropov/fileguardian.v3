#ifndef _FGUMTEST3WEBSVC_H_7A1DEE8B_B81B_4555_9D0D_7268CFA67702_INCLUDED
#define _FGUMTEST3WEBSVC_H_7A1DEE8B_B81B_4555_9D0D_7268CFA67702_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 9:46:14p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver, local database and web service integrity test interface declaration file.
*/
#include "fg.um.test.0._Iface.h"
#include "FG_License_Monitor.h"

namespace fg { namespace um { namespace test {

	using shared::lite::common::CSysError;
	using fg::service::ILicenseCallback;
	using fg::service::CLicenseState;

	class CTest_5_DrvAll : public CTest_0__Iface, public ILicenseCallback {

		typedef CTest_0__Iface TBase;

	public:
		CTest_5_DrvAll(ITestModuleConsoleCallback&);
		~CTest_5_DrvAll(void);

	public:
		virtual HRESULT    Run  (void) override sealed;
		virtual bool       IsApplicable(LPCTSTR lpszArgName = NULL, LPCTSTR lpszArgValue = NULL) override sealed;

	public:
		virtual HRESULT    ILicenseCallback_OnChanged(const CLicenseState&) override sealed;
	};
}}}
#endif/*_FGUMTEST3WEBSVC_H_7A1DEE8B_B81B_4555_9D0D_7268CFA67702_INCLUDED*/