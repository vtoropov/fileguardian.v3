/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Jul-2017 on 8:01:04p, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver event sink console test application entry point file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 29-May-2018 at 10:32:15a, UTC+7, Phuket, Rawai, Tuesday;
*/
#include "StdAfx.h"
#include "Shared_SystemCore.h"

using namespace shared::lite::sys_core;

#include "Shared_FS_GenericDrive.Monitor.h"

using namespace shared::ntfs;

#include "fg.um.test.0._Iface.h"
#include "fg.um.test.0.console.h"
#include "fg.um.test.1.drv.flt.cfg.h"
#include "fg.um.test.1.drv.flt.evt.h"
#include "fg.um.test.1.drv.evt.resource.h"

namespace fg { namespace test { namespace common { namespace ctrl_flow
{
	class CConsoleEvents : public CConsoleEvents_Base {
	public:
		virtual HRESULT ITestModuleConsole_OnBefore(LPCTSTR lpszPrompt) override sealed {

			global::_out::LogInfo(
					_T("ITestModuleConsole_OnBefore::%s"), lpszPrompt
				);
			fg::um::test::CTest_1_FilterCfg cfg_(*this);
			HRESULT hr_ = cfg_.Run();
			return  hr_;
		}
	};
}}}}

#ifndef __OUTPUT_TO_CONSOLE
#define __OUTPUT_TO_CONSOLE
#endif

#include "FG_FsBridgeIface.h"
#include "FG_FsBridgeCache.h"

using namespace fg::common::filter;

using namespace fg::test::common;
using namespace fg::test::common::ctrl_flow;

/////////////////////////////////////////////////////////////////////////////

INT _tmain(VOID)
{
	CCoInitializer com(false);
	CCoSecurityProvider sec_;

	HRESULT hr_ = sec_.InitDefailt();
	ATLVERIFY(SUCCEEDED(hr_));

	INT result_ = 0;

	const bool bVerbose = true;
	global::_out::OutputToConsole(true);
	global::_out::VerboseMode(bVerbose);

	const TCommandLine& cmd_line = GetAppObjectRef().CommandLine();
	const bool bIgnoreAwaitArg = true;

	if (false == bIgnoreAwaitArg && false==cmd_line.Has(_T("no_break"))) {
		MessageBox(NULL, _T("Awaiting for connecting debugger"), _T("Break Point"), MB_ICONEXCLAMATION | MB_OK);
	}

	hr_ = ctrl_flow::Event_OnCreate();
	if (FAILED(hr_)){
		return ctrl_flow::Event_OnExit(bVerbose, true);
	}

	CConsoleEvents console_evt;
	fg::um::test::CTest_1_FilterEvents flt_evt_test(console_evt);

	hr_ = flt_evt_test.Run();
	if (FAILED(hr_)) {
		global::_out::LogError(flt_evt_test.Error());
		result_ = ctrl_flow::Event_OnExit(bVerbose, true);
	}
	else
		result_ = ctrl_flow::Event_OnExit(bVerbose, false);

	return result_;
}