/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-May-2018 at 11:00:25p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian minifilter driver log feature test interface implementation file.
*/
#include "StdAfx.h"
#include "fg.um.test.1.drv.flt.cfg.h"

using namespace fg::um::test;

#include "FG_FsBridgeIface.h"

using namespace fg::common::filter;

#include "Shared_GenericAppObject.h"
#include "Shared_DateTime.h"

using namespace shared::user32;
using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

CTest_1_FilterCfg::CTest_1_FilterCfg(ITestModuleConsoleCallback& _clbk) : TBase(_clbk, _T("CTest_1_FilterCfg")) {}
CTest_1_FilterCfg::~CTest_1_FilterCfg(void){}

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace filter { namespace details {

	LPCTSTR   CTest_1_Arg0(void) { static LPCTSTR lpsz_arg0_name = _T("compare"); return lpsz_arg0_name; }
	HRESULT   CTest_1_GetDefaultLogPath(CSysError& _err, CAtlString& _path) {
		_err.Module(_T(__FUNCTION__));
		_err = S_OK;

		CAtlString cs_path;
		HRESULT hr_ = GetAppObjectRef().GetPath(cs_path);
		if (FAILED(hr_))
			_err = hr_;
		else {
			CSystemTime sys_;
			hr_ = sys_.Current();
			if (FAILED(hr_))
				_err = hr_;
			else
			_path.Format(
				_T("%s%d_%d_%d_%d_%d_%d.log"),
				cs_path.GetString(),
				sys_.wYear         ,
				sys_.wMonth        ,
				sys_.wDay          ,
				sys_.wHour         ,
				sys_.wMinute       ,
				sys_.wSecond       
			);
		}

		return _err;
	}

}}}}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTest_1_FilterCfg::Run  (void) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	// (1) assumes a connection to FG minifilter driver has been successfully made;
	CDriverState state_;
	state_.Add(CDriverState::eConnected);

	// (2) declares driver log connect object;
	//     this functionality is turned off for the time being;
	//     it is very possible that it will be activated soon;
#if (0)
	CDriverLog log_;

	global::_out::LogInfo(_T("Getting FG driver log options..."));

	const DWORD dOptions = log_.Options();
	if (log_.Error()) {
		global::_out::LogError(
				log_.Error()
			);
	}
	else {
		global::_out::LogInfo(
				_T("FG driver log options are %u"), dOptions
			);
	}
	CAtlString cs_path;
	HRESULT hr_ = details::CTest_0_GetDefaultLogPath(m_error, cs_path);
	if (FAILED(hr_))
		global::_out::LogError(m_error);
	else {
		global::_out::LogInfo(_T("Getting FG driver log file:\n\t%s"), cs_path.GetString());
		hr_ = log_.Path(cs_path.GetString());
		if (FAILED(hr_))
			global::_out::LogError(log_.Error());
		else
			global::_out::LogInfo(_T("Log file path has been successfully set."));
	}
	global::_out::LogInfo(_T("Getting FG driver log file..."));

	const CAtlString path_ = log_.Path();
	if (log_.Error()) {
		global::_out::LogError(
			log_.Error()
		);
	}
	else {
		global::_out::LogInfo(
			_T("FG driver log file is\n\t\t%s"), path_.GetString()
		);
	}

	global::_out::LogInfo(_T("Getting FG driver log verbose level..."));

	const DWORD verbose_ = log_.Verbose();
	if (log_.Error()) {
		global::_out::LogError(
			log_.Error()
		);
	}
	else {
		global::_out::LogInfo(
			_T("FG driver log verbose mode is %u"), verbose_
		);
	}
#else
	global::_out::LogInfo(
		_T("a log is available via predefined log file: %s"), _T("c:\\fg_driver_log.log")
	);
#endif

	const TCommandLine& cmd_ln = GetAppObjectRef().CommandLine();
	if (cmd_ln.Has(details::CTest_1_Arg0())) {

		CAtlString cs_path = cmd_ln.Argument(details::CTest_1_Arg0());
		if (cs_path.IsEmpty()) {
			m_error.SetState(
				(DWORD)ERROR_INVALID_DATA, _T("Command line argument '%s' is provided with no value")
			);
			return m_error;
		}
		CDriverFolders folders_;

		HRESULT hr_ = folders_.Compare(cs_path.GetString());
		if (FAILED(hr_)) {
			m_error = folders_.Error();
			return m_error;
		}
		else
		global::_out::LogInfo(
			_T("The result of comparing %s with watch folder(s) is placed to log file"), cs_path.GetString()
		);
	}

	return m_error;
}

bool       CTest_1_FilterCfg::IsApplicable(LPCTSTR lpszArgName, LPCTSTR lpszArgValue) {
	return TBase::IsApplicable(lpszArgName, lpszArgValue);
}