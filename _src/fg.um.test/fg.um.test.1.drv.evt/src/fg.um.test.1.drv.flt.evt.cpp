/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-May-2018 at 1:06:23p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian driver minifilter event callback connection test interface implementation file. 
*/
#include "StdAfx.h"
#include "fg.um.test.1.drv.flt.evt.h"

using namespace fg::um::test;

#include "fg.um.test.0.drv.con.h"
#include "fg.um.test.0.drv.evt.adapter.h"
#include "FG_FsBridgeIface.h"

using namespace fg::common::filter;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace filter { namespace details {
}}}}

/////////////////////////////////////////////////////////////////////////////

CTest_1_FilterEvents::CTest_1_FilterEvents(ITestModuleConsoleCallback& _clbk) : TBase(_clbk, _T("CTest_1_FilterEvents")) {}
CTest_1_FilterEvents::~CTest_1_FilterEvents(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT         CTest_1_FilterEvents::Run  (void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (false == this->IsApplicable()) {
		m_error.SetState(
			(DWORD)ERROR_INVALID_STATE,
			_T("The app is not configured to run [drv.evt] test.")
		);
		return m_error;
	}

	CTest_0_DrvEvtAdapter adapter_(TBase::m_callback);
	CEventCache cache_( adapter_);

	CTest_0_DrvConnect drv_con(TBase::m_callback, cache_); drv_con.LogInfo(true);

	HRESULT hr_ = drv_con.Run();
	if (FAILED(hr_)) {
		m_error = drv_con.Error();
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

bool     CTest_1_FilterEvents::IsApplicable(LPCTSTR lpszArgName, LPCTSTR lpszArgValue)
{
	return TBase::IsApplicable(lpszArgName, lpszArgValue);
}