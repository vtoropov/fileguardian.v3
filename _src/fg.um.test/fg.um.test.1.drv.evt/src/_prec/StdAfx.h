#ifndef _FGSTDAFX_H_5A8547C4_8C9E_441A_9D25_87D7969194AC_INCLUDED
#define _FGSTDAFX_H_5A8547C4_8C9E_441A_9D25_87D7969194AC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Jul-2017 at 6:32:43p, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver console test application precompiled header include file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 29-May-2018 at 9:52:51a, UTC+7, Phuket, Rawai, Tuesday;
*/
#include "..\..\..\fg.um.test.0.shared\src\_prec\StdAfx.h" // trying to make a reference to parent precompiled header without project settings;
                                                           // it seems to be not good way, but nevertheless;
#pragma comment(lib, "fg.generic.shared_v15.lib")
#pragma comment(lib, "fg.um.filter.cfg_v15.lib")
#pragma comment(lib, "fg.um.filter.bridge_v15.lib")
#pragma comment(lib, "fg.um.test.0.shared_v15.lib")

#endif/*_FGSTDAFX_H_5A8547C4_8C9E_441A_9D25_87D7969194AC_INCLUDED*/