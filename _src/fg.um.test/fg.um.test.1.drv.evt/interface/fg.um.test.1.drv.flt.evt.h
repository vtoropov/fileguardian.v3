#ifndef _FGUMTEST0FILTEREVENTS_H_6CC73290_A579_4F11_8E27_517FE99E98AE_INCLUDED
#define _FGUMTEST0FILTEREVENTS_H_6CC73290_A579_4F11_8E27_517FE99E98AE_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-May-2018 at 12:59:21p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian driver minifilter event callback connection test interface declaration file. 
*/
#include "fg.um.test.0._Iface.h"

namespace fg { namespace um { namespace test {

	using shared::lite::common::CSysError;

	class CTest_1_FilterEvents : public CTest_0__Iface {

		typedef CTest_0__Iface TBase;

	public:
		CTest_1_FilterEvents(ITestModuleConsoleCallback&);
		~CTest_1_FilterEvents(void);

	public:
		virtual HRESULT    Run  (void) override sealed;
		virtual bool       IsApplicable(LPCTSTR lpszArgName = NULL, LPCTSTR lpszArgValue = NULL) override sealed;
	};

}}}

#endif/*_FGUMTEST0FILTEREVENTS_H_6CC73290_A579_4F11_8E27_517FE99E98AE_INCLUDED*/