#ifndef _FGUMTEST0FILTERCFG_H_9C019A51_7124_418B_B9A5_B7070EC96C5A_INCLUDED
#define _FGUMTEST0FILTERCFG_H_9C019A51_7124_418B_B9A5_B7070EC96C5A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-May-2018 at 10:57:08p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian minifilter driver log feature test interface declaration file.
*/
#include "fg.um.test.0._Iface.h"

namespace fg { namespace um { namespace test {

	using shared::lite::common::CSysError;

	class CTest_1_FilterCfg : public CTest_0__Iface {

		typedef CTest_0__Iface TBase;

	public:
		CTest_1_FilterCfg(ITestModuleConsoleCallback&);
		~CTest_1_FilterCfg(void);

	public:
		virtual HRESULT    Run  (void) override sealed;
		virtual bool       IsApplicable(LPCTSTR lpszArgName = NULL, LPCTSTR lpszArgValue = NULL) override sealed;
	};

}}}

#endif/*_FGUMTEST0FILTERCFG_H_9C019A51_7124_418B_B9A5_B7070EC96C5A_INCLUDED*/