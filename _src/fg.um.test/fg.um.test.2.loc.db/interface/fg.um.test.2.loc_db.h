#ifndef _FGFSFLTTSTDBLOCAL_H_20C95D4B_93E1_441f_A75C_C4B3F1A3C0CD_INCLUDED
#define _FGFSFLTTSTDBLOCAL_H_20C95D4B_93E1_441f_A75C_C4B3F1A3C0CD_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Apr-2018 at 6:35:24a, UTC+7, Novosibirsk, Rodniki, Sunday;
	This is File Guardian filter local database test interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jun-2018 at 9:39:23a, UTC+7, Phuket, Rawai, Friday;
*/
#include "fg.um.test.0._Iface.h"

namespace fg { namespace um { namespace test {

	using shared::lite::common::CSysError;

	class CTest_2_LocalDb : public CTest_0__Iface {

		typedef CTest_0__Iface TBase;

	public:
		CTest_2_LocalDb(ITestModuleConsoleCallback&);
		~CTest_2_LocalDb(void);

	public:
		virtual HRESULT    Run  (void) override sealed;
		virtual bool       IsApplicable(LPCTSTR lpszArgName = NULL, LPCTSTR lpszArgValue = NULL) override sealed;
	};
}}}

#endif/*_FGFSFLTTSTDBLOCAL_H_20C95D4B_93E1_441f_A75C_C4B3F1A3C0CD_INCLUDED*/