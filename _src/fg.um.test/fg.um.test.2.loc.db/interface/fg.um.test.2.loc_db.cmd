::
::   Created by Tech_dog (ebontrop@gmail.com) on 1-Jun-2018 at 12:26:46p, UTC+7, Phuket, Rawai, Friday;
::   This is File Guardian local databse test command line sample file;
::
::   The following arguments are acceptable:
::   -----------------------------------------------------------------------------
::   name                value
::   -----------------------------------------------------------------------------
::   db_name             file name; if a path is not absolute, database file is created in test executable folder;
::                       this argument can be missed; in such case, a database with auto-generated name is created in current directory; 
::   db_schema           XML file name; a file describes internal structure of a database being created;
::                       if argument is missed, a default schema is applied, the schema is read from test executable file resource;
::
::
::   a sample of command line:
::
::   /db_name fg.um.test.2.loc_db.db /db_schema fg.um.test.2.loc_db2.xml