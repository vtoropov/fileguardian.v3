/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Apr-2018 at 6:45:29a, UTC+7, Novosibirsk, Rodniki, Sunday;
	This is File Guardian filter local database test interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jun-2018 at 9:42:17a, UTC+7, Phuket, Rawai, Friday;
*/
#include "StdAfx.h"
#include "fg.um.test.2.loc_db.h"
#include "FG_SqlData_Model_ex.h"
#include "FG_SqlData_Provider.h"
#include "FG_SqlData_Writer.h"

using namespace fg::um::test;
using namespace fg::common::data::local;

#include "Shared_FS_CommonDefs.h"
#include "Shared_DateTime.h"

using namespace shared::lite::data;
using namespace shared::ntfs;

using fg::common::data::CRecord;
using fg::common::data::TRecords;

#include "fg.um.test.0.loc.db.conn.h"
#include "fg.um.test.2.loc_db.resource.h"
/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace um { namespace test { namespace details {

	LPCTSTR    CTest_LocalDb_CmdArg_0(void) {
		static LPCTSTR lpsz_arg_0 = _T("db_name");
		return lpsz_arg_0;
	}

}}}}

/////////////////////////////////////////////////////////////////////////////

CTest_2_LocalDb::CTest_2_LocalDb(ITestModuleConsoleCallback& _clbk) : TBase(_clbk, _T("CTest_2_LocalDb")) { }

CTest_2_LocalDb::~CTest_2_LocalDb(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT         CTest_2_LocalDb::Run  (void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (false == this->IsApplicable()) {
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("The app is not configured to run [loc.db] test.")
			);
		return m_error;
	}

	CDbProvider prov_;

	CTest_0_LocDbConn db_conn(TBase::m_callback, prov_);

	HRESULT hr_ = db_conn.ApplySchema(IDR_FG_DB_LOC_SCHEMA_TEST);
	if (FAILED(hr_))
		return (m_error = db_conn.Error());

	hr_ = db_conn.Run();
	if (FAILED(hr_))
		return (m_error = db_conn.Error());

	global::_out::LogInfo(
			_T("Local database is initialized:\n\t%s\n\tsettings=%s\n"),
			db_conn.Path(), (LPCTSTR)prov_.Accessor().Pragma().ToString()
		);
	global::_out::LogEmptyLine();
	global::_out::LogInfo(
			_T("Database schema is applied:\n%s"), db_conn.Schema().ToString()
		);

	if (db_conn.Schema().IsValid() == false) {
		return (m_error = db_conn.Schema().Error());
	}

	CDbWriter& writer_ = prov_.Writer(); writer_;
	CDbReader& reader_ = prov_.Reader(); reader_;

	const INT nCount = 5;
	CAtlString cs_date;

	for (INT i_ = 0; i_ < nCount; i_++) {

		CSystemTime sys_(true);

		if (0 == i_) sys_.Current();
		else         sys_.Random();

		sys_.ToString(cs_date);

		CUnixTime unx_(sys_.IsLocal());
		unx_ = sys_;

		CAtlString cs_unx_; unx_.ToString(true, cs_unx_);

		global::_out::LogInfo(
			_T("Value ##%03d of %03d: %s == %s"), i_ + 1, nCount, cs_date.GetString(), cs_unx_.GetString()
			);
#if defined(_DEBUG)
		const TDataTableEnum& tables_ = db_conn.Schema().Tables();
		if (tables_.empty()) {// unexpected after checking schema validity;
			m_error.SetState(
				(DWORD)ERROR_INVALID_STATE, _T("Schema object is invalid state.")
			);
			return m_error;
		}
		hr_ = writer_.Insert(db_conn.Schema().Tables().at(0).Name(), unx_);
		if (FAILED(hr_)) {
			m_error = writer_.Error();
			break;
		}
#endif
	}

	if (!m_error) {

		TRecords recs_;
		global::_out::LogEmptyLine();
		global::_out::LogInfo(_T("Reading data from database...\n\n"));

		hr_ = reader_.Records(db_conn.Schema(), recs_);
		if (FAILED(hr_))
			m_error = reader_.Error();
		else
		{
			for (size_t i_ = 0; i_ < recs_.size(); i_++) {

				const CRecord& rec_ = recs_[i_];

				CSystemTime sys_(true); sys_ = rec_.Timestamp(); sys_.ToString(cs_date);

				global::_out::LogInfo(
					_T("Value ##%03d of %03d: %s == %s"), i_ + 1, recs_.size(), cs_date.GetString(), (LPCTSTR)rec_.TimestampAsText(true)
				);
			}
		}
	}
	prov_.Accessor().Close();
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

bool     CTest_2_LocalDb::IsApplicable(LPCTSTR lpszArgName, LPCTSTR lpszArgValue)
{
	lpszArgName; lpszArgValue;
	const TCommandLine& cmd_line = GetAppObjectRef().CommandLine();
#if defined(_DEBUG)
	// no argument is provided; it seems the test is run by this console manually;
	if (0 == cmd_line.Count())
		return true;
#endif
	return cmd_line.Has(details::CTest_LocalDb_CmdArg_0());
}