/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 1:16:54p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian driver & database integrity test interface implementation file.
*/
#include "StdAfx.h"
#include "fg.um.test.4.drv_db.h"

using namespace fg::um::test;

#include "Shared_FS_GenericDrive.Monitor.h"

using namespace shared::ntfs;

#include "fg.um.test.0.drv.con.h"
#include "fg.um.test.0.drv.evt.adapter.h"
#include "fg.um.test.0.drv.evt.remove.h"
#include "fg.um.test.0.loc.db.conn.h"
#include "fg.um.test.0.loc.db.write.h"
#include "fg.um.test.4.drv_db.resource.h"

#include "FG_FsBridgeIface.h"
#include "FG_SqlData_Writer.h"

using namespace fg::common::filter;
using namespace fg::common::data::local;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace um { namespace test { namespace details {

	LPCTSTR    CTest_DrvDb_CmdArg_0(void) {
		static LPCTSTR lpsz_arg_0 = _T("drv_db_url");
		return lpsz_arg_0;
	}

}}}}
/////////////////////////////////////////////////////////////////////////////

CTest_4_DrvDb::CTest_4_DrvDb(ITestModuleConsoleCallback& _clbk) : TBase(_clbk, _T("CTest_4_DrvDb")) { }

CTest_4_DrvDb::~CTest_4_DrvDb(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT         CTest_4_DrvDb::Run  (void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (false == this->IsApplicable()) {
		m_error.SetState(
			(DWORD)ERROR_INVALID_STATE,
			_T("The app is not configured to run [drv.db] test.")
		);
		return m_error;
	}

	global::_out::LogInfo(_T("Trying to connect to local database..."));

	CTest_0_LocDbWrite db_writer(TBase::m_callback);

	HRESULT hr_ = db_writer.Run();
	if (FAILED(hr_))
		return (m_error = db_writer.Error());

	db_writer.LogInfo();

	if (db_writer.Schema().IsValid() == false) {
		return (m_error = db_writer.Schema().Error());
	}

	CTest_0_DrvConnect drv_con(TBase::m_callback, db_writer.Cache()); drv_con.LogInfo(true);
	hr_ = drv_con.Run();
	if (FAILED(hr_))
		m_error = drv_con.Error();

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

bool     CTest_4_DrvDb::IsApplicable(LPCTSTR lpszArgName, LPCTSTR lpszArgValue)
{
	lpszArgName; lpszArgValue;
	const TCommandLine& cmd_line = GetAppObjectRef().CommandLine();
#if defined(_DEBUG)
	// no argument is provided; it seems the test is run by this console manually;
	if (0 == cmd_line.Count())
		return true;
#endif
	return cmd_line.Has(details::CTest_DrvDb_CmdArg_0());
}