/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 on 1:20:32p, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver & database integrity test application entry point file.
*/
#include "StdAfx.h"
#include "Shared_SystemCore.h"

using namespace shared::lite::sys_core;

#include "fg.um.test.0._Iface.h"
#include "fg.um.test.0.console.h"
#include "fg.um.test.4.drv_db.h"

namespace fg { namespace test { namespace common { namespace ctrl_flow
{
}}}}

#ifndef __OUTPUT_TO_CONSOLE
#define __OUTPUT_TO_CONSOLE
#endif

using namespace fg::test::common;
using namespace fg::test::common::ctrl_flow;

/////////////////////////////////////////////////////////////////////////////

INT _tmain(VOID)
{
	CCoInitializer com(false);
	CCoSecurityProvider sec_;

	HRESULT hr_ = sec_.InitDefailt();
	ATLVERIFY(SUCCEEDED(hr_));

	INT result_ = 0;

	const bool bVerbose = true;
	global::_out::OutputToConsole(true);
	global::_out::VerboseMode(bVerbose);

	bool bShowBreak = true;

	const TCommandLine& cmd_line = GetAppObjectRef().CommandLine();

	if (cmd_line.Has(_T("no_break")) == false) {
		bShowBreak  = false;
	}

	if (true == bShowBreak) {
		MessageBox(NULL, _T("Awaiting for connecting debugger"), _T("Break Point"), MB_ICONEXCLAMATION | MB_OK);
	}

	hr_ = ctrl_flow::Event_OnCreate();
	if (FAILED(hr_)){
		return ctrl_flow::Event_OnExit(bVerbose, true);
	}

	CConsoleEvents_Base console_evt;
	fg::um::test::CTest_4_DrvDb drv_db_test(console_evt);

	hr_ = drv_db_test.Run();
	if (FAILED(hr_)) {
		global::_out::LogError(drv_db_test.Error());
		result_ = ctrl_flow::Event_OnExit(bVerbose, true);
	}
	else
		result_ = ctrl_flow::Event_OnExit(bVerbose, false);

	return result_;
}