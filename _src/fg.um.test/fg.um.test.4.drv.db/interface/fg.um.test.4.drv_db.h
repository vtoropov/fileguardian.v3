#ifndef _FGUMTEST3WEBSVC_H_7A1DEE8B_B81B_4EE5_9D0D_7268CFA67702_INCLUDED
#define _FGUMTEST3WEBSVC_H_7A1DEE8B_B81B_4EE5_9D0D_7268CFA67702_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 1:15:50p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver & database integrity test interface declaration file.
*/
#include "fg.um.test.0._Iface.h"

namespace fg { namespace um { namespace test {

	using shared::lite::common::CSysError;

	class CTest_4_DrvDb : public CTest_0__Iface {

		typedef CTest_0__Iface TBase;

	public:
		CTest_4_DrvDb(ITestModuleConsoleCallback&);
		~CTest_4_DrvDb(void);

	public:
		virtual HRESULT    Run  (void) override sealed;
		virtual bool       IsApplicable(LPCTSTR lpszArgName = NULL, LPCTSTR lpszArgValue = NULL) override sealed;
	};
}}}
#endif/*_FGUMTEST3WEBSVC_H_7A1DEE8B_B81B_4EE5_9D0D_7268CFA67702_INCLUDED*/