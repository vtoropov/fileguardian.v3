#ifndef _FGUMTEST0LOCDBREAD_H_0C0F3405_BF75_4FC5_BDB0_BE1200094032_INCLUDED
#define _FGUMTEST0LOCDBREAD_H_0C0F3405_BF75_4FC5_BDB0_BE1200094032_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Jun-2018 at 8:01:29p, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian local database reader common test interface declaration file.
*/
#include "Shared_SystemThreadPool.h"

#include "fg.um.test.0._Iface.h"
#include "fg.um.test.0.loc.db.conn.h"

#include "FG_SqlData_Reader.h"
#include "FG_SqlData_Provider.h"

namespace fg { namespace um { namespace test {

	using shared::lite::common::CSysError;
	using shared::lite::sys_core::CThreadBase;
	using fg::common::data::local::CDbProvider;
	using fg::common::data::local::CDbReader;
	using fg::common::data::local::CDataSchemaOpt;

	class CTest_0_LocDbRead : public CTest_0_LocDbConn {

		typedef CTest_0_LocDbConn TBase;

	protected:
		CDbProvider         m_provider;

	public:
		CTest_0_LocDbRead(ITestModuleConsoleCallback&);
		~CTest_0_LocDbRead(void);

	public:
		virtual HRESULT     Run  (void) override;

	public:
		const CDbProvider&  Provider(void) const;
		CDbProvider&        Provider(void)      ;
	};

	class CTest_0_LocDbRead_Ex : public CThreadBase, public CTest_0_LocDbRead {

		typedef CThreadBase TBase;
		typedef CTest_0_LocDbRead TData;

	public:
		CTest_0_LocDbRead_Ex(ITestModuleConsoleCallback&);
		~CTest_0_LocDbRead_Ex(void);

	public:
		TErrorRef  Error(void)const;
	private:
		VOID       ThreadFunction(void) override;
	};

}}}
#endif/*_FGUMTEST0LOCDBREAD_H_0C0F3405_BF75_4FC5_BDB0_BE1200094032_INCLUDED*/