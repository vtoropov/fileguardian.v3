#ifndef _FGUMTEST0DRVCON_H_311DF64F_58B1_4086_9B6E_EDF50DB4F204_INCLUDED
#define _FGUMTEST0DRVCON_H_311DF64F_58B1_4086_9B6E_EDF50DB4F204_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Jun-2018 at 9:53:27a, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian minifilter driver connector interface declaration file.
*/
#include "fg.um.test.0._Iface.h"
#include "FG_FsBridgeCache.h"
#include "FG_FsBridgeDefs.h"

namespace fg { namespace um { namespace test {

	using shared::lite::common::CSysError;
	using fg::common::filter::CEventCache;

	class CTest_0_DrvConnect : public CTest_0__Iface {

		typedef CTest_0__Iface TBase;

	protected:
		CEventCache&       m_cache_ref;
		bool               m_log_info;

	public:
		CTest_0_DrvConnect(ITestModuleConsoleCallback&, CEventCache&);
		~CTest_0_DrvConnect(void);

	public:
		virtual HRESULT    Run  (void) override;
		virtual bool       IsApplicable(LPCTSTR lpszArgName = NULL, LPCTSTR lpszArgValue = NULL) override;
	public:
		VOID               LogInfo(void)const;  // shows watched folder info;
		VOID               LogInfo(const bool); // sets an internal flag for automatic showing watched folders info;
	};
}}}

#endif/*_FGUMTEST0DRVCON_H_311DF64F_58B1_4086_9B6E_EDF50DB4F204_INCLUDED*/