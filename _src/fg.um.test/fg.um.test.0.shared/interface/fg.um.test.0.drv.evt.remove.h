#ifndef _FGUMTEST1DRVEVTREMOVE_H_4CEE2C97_3163_495C_B7C3_FC9F1EA655AD_INCLUDED
#define _FGUMTEST1DRVEVTREMOVE_H_4CEE2C97_3163_495C_B7C3_FC9F1EA655AD_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 2:13:38p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver removable notification interface declaration file.
*/
#include "fg.um.test.0._Iface.h"
#include "FG_FsBridgeIface.h"
#include "Shared_FS_GenericDrive.Monitor.h"

namespace fg { namespace um { namespace test {

	using shared::lite::common::CSysError;
	using shared::ntfs::IRemovableDriveEventSink;
	using shared::ntfs::TDriveList;
	using shared::ntfs::CRemovableDriveMonitor;
	using fg::common::filter::CDriverRemovable;

	class CTest_0_DrvEvtRemove : public CTest_0__Iface, public IRemovableDriveEventSink {

		typedef CTest_0__Iface TBase;
	protected:
		CDriverRemovable       m_removable;
		CRemovableDriveMonitor m_monitor;
	public:
		CTest_0_DrvEvtRemove(ITestModuleConsoleCallback&);
		~CTest_0_DrvEvtRemove(void);

	public:
		virtual HRESULT    Run  (void) override sealed;
		virtual bool       IsApplicable(LPCTSTR lpszArgName = NULL, LPCTSTR lpszArgValue = NULL) override sealed;

	public: // IRemovableDriveEventSink
		HRESULT IRemovableDrive_BeingAdded(const TDriveList& _list) override;
		HRESULT IRemovableDrive_BeingEjected(const TDriveList& _list) override;

	public:
		CRemovableDriveMonitor&
		                   Monitor(void);
	};

}}}

#endif/*_FGUMTEST1DRVEVTREMOVE_H_4CEE2C97_3163_495C_B7C3_FC9F1EA655AD_INCLUDED*/