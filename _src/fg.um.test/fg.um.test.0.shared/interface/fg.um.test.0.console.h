#ifndef _FGUMTEST0CONSOLE_H_535DDDF7_A2AB_4C98_9DFD_4DE11F4F0218_INCLUDED
#define _FGUMTEST0CONSOLE_H_535DDDF7_A2AB_4C98_9DFD_4DE11F4F0218_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 9:33:55a, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian common test console related interface declaration file.
*/
#include "Shared_GenericAppObject.h"
#include "Shared_GenericConObject.h"

#include "fg.um.test.0._Iface.h"

namespace fg { namespace test { namespace common { namespace ctrl_flow
{
	using namespace shared::user32;

	HRESULT Event_OnCreate(void);
	HRESULT Event_OnExit(const bool bVerbose, const bool bError = true);
	HRESULT Event_OnWait(LPCTSTR lpszPrompt);

	using namespace fg::um::test;

	class CConsoleEvents_Base : public fg::um::test::ITestModuleConsoleCallback {
	public:
		virtual HRESULT ITestModuleConsole_OnBefore(LPCTSTR lpszPrompt) override;
		virtual HRESULT ITestModuleConsole_OnWait  (LPCTSTR lpszPrompt) override;
		virtual HRESULT ITestModuleConsole_OnAfter (LPCTSTR lpszPrompt) override;
	};
}}}}
#endif/*_FGUMTEST0CONSOLE_H_535DDDF7_A2AB_4C98_9DFD_4DE11F4F0218_INCLUDED*/