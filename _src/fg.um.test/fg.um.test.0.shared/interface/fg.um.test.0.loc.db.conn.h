#ifndef _FGUMTEST0LOCDBCONN_H_6E3AF8E5_1F76_4558_A62C_C2C9DCA20787_INCLUDED
#define _FGUMTEST0LOCDBCONN_H_6E3AF8E5_1F76_4558_A62C_C2C9DCA20787_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 5:08:42p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian local database common test related interface declaration file.
*/
#include "fg.um.test.0._Iface.h"
#include "FG_SqlData_Provider.h"

namespace fg { namespace um { namespace test {

	using shared::lite::common::CSysError;
	using fg::common::data::local::CDbProvider;
	using fg::common::data::local::CDataSchema;

	class CTest_0_LocDbConn : public CTest_0__Iface {

		typedef CTest_0__Iface TBase;

	protected:
		CDbProvider&       m_prov_ref;
		CAtlString         m_db_path;
	public:
		CTest_0_LocDbConn(ITestModuleConsoleCallback&, CDbProvider&);
		~CTest_0_LocDbConn(void);

	public:
		virtual HRESULT    Run  (void) override;
		virtual bool       IsApplicable(LPCTSTR lpszArgName = NULL, LPCTSTR lpszArgValue = NULL) override;

	public:
		HRESULT       ApplySchema(const UINT uResourceId);
		HRESULT       ApplySchema(LPCTSTR lpszXmlData);
		LPCTSTR       Path  (void)const;
		HRESULT       Path  (LPCTSTR);
		const
		CDataSchema&  Schema(void)const;
	};
}}}

#endif/*_FGUMTEST0LOCDBCONN_H_6E3AF8E5_1F76_4558_A62C_C2C9DCA20787_INCLUDED*/