#ifndef _FGUMTEST0LOCDBWRITE_H_3604E42D_7EDB_47EA_B678_D81635E995CE_INCLUDED
#define _FGUMTEST0LOCDBWRITE_H_3604E42D_7EDB_47EA_B678_D81635E995CE_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 10:12:47p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian local database writer common test interface declaration file.
*/
#include "fg.um.test.0._Iface.h"
#include "fg.um.test.0.loc.db.conn.h"
#include "FG_SqlData_Writer.h"
#include "FG_SqlData_Provider.h"
#include "FG_FsBridgeCache.h"

namespace fg { namespace um { namespace test {

	using shared::lite::common::CSysError;
	using fg::common::data::local::CDbProvider;
	using fg::common::data::local::CDbWriter;
	using fg::common::data::local::CDataSchemaOpt;
	using fg::common::filter::ICacheCallback;
	using fg::common::filter::CEventCache;
	using fg::common::filter::CEventItem;
	using fg::common::filter::CEventTime;

	class CTest_0_LocDbWrite : public CTest_0_LocDbConn, public ICacheCallback {

		typedef CTest_0_LocDbConn TBase;

	protected:
		CEventCache         m_cache;
		CDbProvider         m_provider;
		bool                m_b_details;

	public:
		CTest_0_LocDbWrite(ITestModuleConsoleCallback&);
		~CTest_0_LocDbWrite(void);

	public:
		virtual HRESULT     Run  (void) override;

	public:
		virtual HRESULT     ICacheCallback_OnError(TErrorRef) override;
		virtual HRESULT     ICacheCallback_OnItemArrive(const CEventItem&) override;

	public:
		const
		CEventCache&        Cache(void)const;
		CEventCache&        Cache(void);
		bool                Details(void)const;
		VOID                Details(const bool);
		VOID                LogInfo(void)const;
		const
		CDbProvider&        Provider(void)const;
		CDbProvider&        Provider(void)     ;
	};

}}}

#endif/*_FGUMTEST0LOCDBWRITE_H_3604E42D_7EDB_47EA_B678_D81635E995CE_INCLUDED*/