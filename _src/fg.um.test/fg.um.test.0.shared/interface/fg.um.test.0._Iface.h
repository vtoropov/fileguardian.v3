#ifndef _FGUMTEST0IFACE_H_0D4ACA46_3F59_48CF_8353_D64B9E33FBE5_INCLUDED
#define _FGUMTEST0IFACE_H_0D4ACA46_3F59_48CF_8353_D64B9E33FBE5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-May-2018 at 1:19:44p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian driver minifilter common test module interface declaration file.
*/
#include "Shared_SystemError.h"

#include "Shared_GenericAppObject.h"
#include "Shared_GenericAppResource.h"

namespace fg { namespace um { namespace test {

	using shared::lite::common::CSysError;

	using namespace shared::user32;

	interface ITestModuleConsoleCallback {
		virtual HRESULT ITestModuleConsole_OnBefore(LPCTSTR lpszPrompt) PURE;
		virtual HRESULT ITestModuleConsole_OnWait  (LPCTSTR lpszPrompt) PURE;
		virtual HRESULT ITestModuleConsole_OnAfter (LPCTSTR lpszPrompt) PURE;
	};

	class CTest_0__Iface {
	protected:
		ITestModuleConsoleCallback&
		                m_callback;
		CSysError       m_error;

	protected:
		CTest_0__Iface(ITestModuleConsoleCallback&, LPCTSTR lpszModuleName);
		~CTest_0__Iface(void);

	public:
		virtual TErrorRef  Error(void)const;
		virtual bool       IsApplicable(LPCTSTR lpszArgName = NULL, LPCTSTR lpszArgValue = NULL);
		virtual HRESULT    Run  (void);
	};

}}}

#endif/*_FGUMTEST0IFACE_H_0D4ACA46_3F59_48CF_8353_D64B9E33FBE5_INCLUDED*/