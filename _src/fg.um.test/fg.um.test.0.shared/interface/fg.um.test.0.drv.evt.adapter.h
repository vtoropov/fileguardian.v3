#ifndef _FGUMTEST1DRVEVTADAPTER_H_AEAE9CA0_6413_446C_8237_5F50D43A1197_INCLUDED
#define _FGUMTEST1DRVEVTADAPTER_H_AEAE9CA0_6413_446C_8237_5F50D43A1197_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 1:48:08p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver event adapter interface declaration file. 
*/
#include "fg.um.test.0._Iface.h"
#include "FG_FsBridgeCache.h"
#include "FG_FsBridgeDefs.h"

namespace fg { namespace um { namespace test {

	using shared::lite::common::CSysError;
	using fg::common::filter::ICacheCallback;
	using fg::common::filter::CEventItem;

	class CTest_0_DrvEvtAdapter : public CTest_0__Iface, public ICacheCallback {

		typedef CTest_0__Iface TBase;

	public:
		CTest_0_DrvEvtAdapter(ITestModuleConsoleCallback&);
		~CTest_0_DrvEvtAdapter(void);

	public:
		virtual HRESULT    Run  (void) override sealed;
		virtual bool       IsApplicable(LPCTSTR lpszArgName = NULL, LPCTSTR lpszArgValue = NULL) override sealed;

	public: // ICacheCallback
		HRESULT ICacheCallback_OnError(TErrorRef _error) override;
		HRESULT ICacheCallback_OnItemArrive(const CEventItem& _event) override;
	};
}}}

#endif/*_FGUMTEST1DRVEVTADAPTER_H_AEAE9CA0_6413_446C_8237_5F50D43A1197_INCLUDED*/