//
//    Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 9:48:05a, UTC+7, Phuket, Rawai, Saturday;
//    This is File Guardian common test resource declaration file.
//
#define IDR_RT_MANIFEST                1
#define IDR_RT_MAINICON                3

#define IDR_FG_DB_LOC_SCHEMA_TEST      5
#define IDR_FG_DB_LOC_SCHEMA_DEFAULT   7   // actually, this schema is defined in fg.um.svc\fg.um.svc.shared.db.local\src\resources\xml\FG_SqlData_db.local.schema.default.xml
                                           // but this copy is used for testing possible change of a local database DDL schema;
