/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-May-2018 at 1:30:59p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian driver minifilter common test module interface implementation file.
*/
#include "StdAfx.h"
#include "fg.um.test.0._Iface.h"

using namespace fg::um::test;

/////////////////////////////////////////////////////////////////////////////

CTest_0__Iface::CTest_0__Iface(ITestModuleConsoleCallback& _clbk, LPCTSTR lpszModuleName) : m_callback(_clbk) {
	m_error.Source(lpszModuleName);
}

CTest_0__Iface::~CTest_0__Iface(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef  CTest_0__Iface::Error(void)const { return m_error; }
bool       CTest_0__Iface::IsApplicable(LPCTSTR lpszArgName, LPCTSTR lpszArgValue) {

	bool bApplicable = true;

	if (NULL == lpszArgName)
		return bApplicable;

	bApplicable = GetAppObjectRef().CommandLine().Has(lpszArgName);
	if (false == bApplicable)
		return   bApplicable;

	if (NULL != lpszArgValue) {
		CAtlString cs_value = GetAppObjectRef().CommandLine().Argument(lpszArgName);
		bApplicable = (0 == cs_value.CompareNoCase(lpszArgValue));
	}

	return bApplicable;
}

HRESULT    CTest_0__Iface::Run  (void) {
	HRESULT hr_ = S_OK;
	return  hr_;
}