/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 1:59:12p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver event adapter interface implementation file. 
*/
#include "StdAfx.h"
#include "fg.um.test.0.drv.evt.adapter.h"

using namespace fg::um::test;

/////////////////////////////////////////////////////////////////////////////

CTest_0_DrvEvtAdapter::CTest_0_DrvEvtAdapter(ITestModuleConsoleCallback& _clbk) : TBase(_clbk, _T("CTest_0_DrvEvtAdapter")) {}
CTest_0_DrvEvtAdapter::~CTest_0_DrvEvtAdapter(void){}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTest_0_DrvEvtAdapter::Run  (void) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	return m_error;
}

bool       CTest_0_DrvEvtAdapter::IsApplicable(LPCTSTR lpszArgName, LPCTSTR lpszArgValue) {
	return TBase::IsApplicable(lpszArgName, lpszArgValue);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT CTest_0_DrvEvtAdapter::ICacheCallback_OnError(TErrorRef _error) {
	HRESULT hr_ = S_OK;

	global::_out::LogError(_error);

	return  hr_;
}

HRESULT CTest_0_DrvEvtAdapter::ICacheCallback_OnItemArrive(const CEventItem& _event) {

	SYSTEMTIME complete_ = {0};
	_event.Time().CompletionAsSysTime(complete_);

	global::_out::LogInfo(
		_T("\n\tEvent time: %d/%d/%d %d:%d:%d.%d\n\tSource: %s\n\tTarget: %s\n\tEvent Type: %d\n\tObject Type: %d\n\tProcId: %d\n\tProcPath: %s"),
		complete_.wMonth ,
		complete_.wDay   ,
		complete_.wYear  ,
		complete_.wHour  ,
		complete_.wMinute,
		complete_.wSecond,
		complete_.wMilliseconds,
		_event.Source(), _event.Target(), _event.Action(), _event.Type(), _event.Process().Id(), _event.Process().Path()
	);

	HRESULT hr_ = S_OK;
	return  hr_;
}