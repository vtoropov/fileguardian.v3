/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Jun-2018 at 8:11:54p, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian local database reader common test interface implementation file.
*/
#include "StdAfx.h"
#include "fg.um.test.0.loc.db.read.h"
#include "fg.um.test.0.resource.h"

using namespace fg::um::test;

#include "Shared_SystemCore.h"
#include "Shared_GenericSyncObject.h"
#include "Shared_GenericEvent.h"

using namespace shared::lite::sys_core;
using namespace shared::runnable;

#include "FG_SysEvent_Model.h"

using namespace fg::common::data;

/////////////////////////////////////////////////////////////////////////////

CTest_0_LocDbRead::CTest_0_LocDbRead(ITestModuleConsoleCallback& _clbk) : TBase(_clbk, m_provider), m_provider(CDataSchemaOpt::eDoNotCare) {}
CTest_0_LocDbRead::~CTest_0_LocDbRead(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CTest_0_LocDbRead::Run  (void) {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (TBase::IsApplicable() == false) {
		m_error.SetState(
			(DWORD)ERROR_INVALID_STATE,
			_T("The app is not configured to run [loc.db.read] test.")
		);
		return m_error;
	}

	HRESULT hr_ = TBase::ApplySchema(IDR_FG_DB_LOC_SCHEMA_DEFAULT);
	if (FAILED(hr_))
		return hr_;

	hr_ = TBase::Run(); // connects to a database;
	if (FAILED(hr_))
		return hr_;

	if (TBase::Schema().IsValid() == false) {
		return (m_error = TBase::Schema().Error());
	}

	CDbReader& reader_ = this->m_provider.Reader();
	if (reader_.Error())
		return (m_error = reader_.Error());


	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

const CDbProvider&  CTest_0_LocDbRead::Provider(void) const { return m_provider; }
CDbProvider&  CTest_0_LocDbRead::Provider(void)             { return m_provider; }

/////////////////////////////////////////////////////////////////////////////

CTest_0_LocDbRead_Ex::CTest_0_LocDbRead_Ex(ITestModuleConsoleCallback& _clbk) : TData(_clbk) {}
CTest_0_LocDbRead_Ex::~CTest_0_LocDbRead_Ex(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef  CTest_0_LocDbRead_Ex::Error(void)const { return TData::m_error; }

/////////////////////////////////////////////////////////////////////////////

VOID       CTest_0_LocDbRead_Ex::ThreadFunction(void) {

	CCoInitializer com_core(false);

	CGenericWaitCounter wait_(10, 10000 * 01); // 10 sec(s)

	static DWORD dwIter_ = 0;
	static const DWORD dwReq = 10;

	while (!m_crt.IsStopped())
	{
		wait_.Wait();
		if (wait_.IsElapsed())
			wait_.Reset();
		else
			continue;

		TRecords recs_ = TData::m_provider.Reader().Records(dwIter_, dwReq);
		HRESULT hr_ = m_provider.Reader().Error();
		if (FAILED(hr_)) {
			global::_out::LogError(TData::m_provider.Reader().Error());
			break;
		}
		if (recs_.size()) {
			global::_out::LogInfo(_T("Database auto-reader gets %d record(s)."), recs_.size());

			dwIter_ += dwReq;
		}
	}
	dwIter_ = 0;
	global::_out::LogInfo(_T("Database auto-reader has stopped."));
	::SetEvent(m_crt.EventObject());
}