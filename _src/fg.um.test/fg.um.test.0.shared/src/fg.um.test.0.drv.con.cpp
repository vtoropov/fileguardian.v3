/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Jun-2018 at 9:58:41a, UTC+7, Phuket, Rawai, Sunday;
	This is File Guardian minifilter driver connector interface implementation file.
*/
#include "StdAfx.h"
#include "fg.um.test.0.drv.con.h"
#include "fg.um.test.0.drv.evt.remove.h"

using namespace fg::um::test;

#include "fg.um.filter.cfg.h"
#include "FG_FsBridgeIface.h"

using namespace fg::common::filter;

/////////////////////////////////////////////////////////////////////////////

CTest_0_DrvConnect::CTest_0_DrvConnect(ITestModuleConsoleCallback& _clbk, CEventCache& _cache) : 
   TBase(_clbk, _T("CTest_0_DrvConnect")), m_cache_ref(_cache), m_log_info(true) {}
CTest_0_DrvConnect::~CTest_0_DrvConnect(void){}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTest_0_DrvConnect::Run  (void) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (false == this->IsApplicable()) {
		m_error.SetState(
			(DWORD)ERROR_INVALID_STATE,
			_T("The app is not configured to run [drv.conn] test.")
		);
		return m_error;
	}

	global::_out::LogInfo(_T("Trying to get FG minifilter driver connection..."));

	CDriverInstaller installer_;

	static const bool bIgnoreIfInstalled = true;

	HRESULT hr_ = installer_.Install(bIgnoreIfInstalled);
	if (FAILED(hr_)){
		return (m_error = installer_.Error());
	}

	CDriverBridge bridge_(this->m_cache_ref);

	static const bool bIgnoreIfConnected = true;

	hr_ = bridge_.Connect(bIgnoreIfConnected);
	if (FAILED(hr_)){
		m_error = bridge_.Error();
	}
	else {
		hr_ = bridge_.Folders().UpdateFolders();
		if (FAILED(hr_))
			m_error = bridge_.Folders().Error();
		else {

			if (m_log_info) { this->LogInfo(); }

			CTest_0_DrvEvtRemove removable_(TBase::m_callback) ;
			removable_.Monitor().Start();

			TBase::m_callback.ITestModuleConsole_OnBefore(_T(__FUNCTION__));
			TBase::m_callback.ITestModuleConsole_OnWait(
				_T("\n\tPress any key to stop monitoring file system events...")
			);

			removable_.Monitor().Stop();
		}
		hr_ = bridge_.Disconnect();
		if (FAILED(hr_)){
			m_error = bridge_.Error();
		}
	}
	static const bool bIgonreIfUninstalled = true;

	hr_ = installer_.Uninstall(bIgonreIfUninstalled);
	if (FAILED(hr_))
		m_error = installer_.Error();

	return m_error;
}

bool       CTest_0_DrvConnect::IsApplicable(LPCTSTR lpszArgName, LPCTSTR lpszArgValue) {
	return TBase::IsApplicable(lpszArgName, lpszArgValue);
}

/////////////////////////////////////////////////////////////////////////////

VOID       CTest_0_DrvConnect::LogInfo(void)const {

	CFolderSettingPersistent pers_;
	HRESULT hr_ = pers_.Load();
	if (FAILED(hr_)) {
		global::_out::LogError(pers_.Error());
	}
	else {
		CAtlString cs_exclude = pers_.ExcludedFolders().ToString(_T("\n\t\t"));
		CAtlString cs_include = pers_.IncludedFolders().ToString(_T("\n\t\t"));
		global::_out::LogInfo(_T("Included Folders:\n\t\t%s"), cs_include.GetString());
		global::_out::LogInfo(_T("Excluded Folders:\n\t\t%s"), cs_exclude.GetString());
	}
}

VOID       CTest_0_DrvConnect::LogInfo(const bool _val) { m_log_info = _val; }