#ifndef _FGENERICSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED
#define _FGENERICSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-May-2018 at 12:49:00a, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian common test precompiled headers declaration file.
*/

#ifndef STRICT
#define STRICT
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>

#ifdef _DEBUG
	#define _ATL_DEBUG_INTERFACES
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif

#include <vector>
#include <map>
#include <queue>
#include <time.h>
#include <typeinfo>

#include "Shared_LogJournal.h"

namespace global
{
	class _out : public shared::log::CEventJournal{};
}

#pragma comment(lib, "__shared.lite_v15.lib")
#pragma comment(lib, "_crypto_v15.lib")
#pragma comment(lib, "_generic.stg_v15.lib")
#pragma comment(lib, "_log_v15.lib")
#pragma comment(lib, "_net_v15.lib")
#pragma comment(lib, "_ntfs_v15.lib")
#pragma comment(lib, "_registry_v15.lib")
#pragma comment(lib, "_runnable_v15.lib")
#pragma comment(lib, "_service_v15.lib")
#pragma comment(lib, "_user.32_v15.lib")
#pragma comment(lib, "_xml_v15.lib")
#pragma comment(lib, "_wmi.service_v15.lib")

#endif/*_FGENERICSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED*/