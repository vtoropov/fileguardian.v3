/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 5:14:06p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian local database common test related interface implementation file.
*/
#include "StdAfx.h"
#include "fg.um.test.0.loc.db.conn.h"
#include "fg.um.test.0.resource.h"

using namespace fg::um::test;
using namespace fg::common::data::local;

#include "Shared_DateTime.h"
#include "Shared_FS_CommonDefs.h"

using namespace shared::lite::data;
using namespace shared::ntfs;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace um { namespace test { namespace details {

	CAtlString CTest_LocalDb_CmdArg_0(void) {
		CAtlString cs_arg(_T("db_path"));
		return cs_arg;
	}

	LPCTSTR    CTest_LocalDb_CmdArg_1(void) {
		static LPCTSTR lpsz_arg_1 = _T("db_schema");
		return lpsz_arg_1;
	}

	CAtlString CTest_LocalDb_DefaultFileName(void) {

		CAtlString  cs_name;
		CSystemTime date_(true);

		date_.Current();
		cs_name.Format(
				_T("fg_db_%04d-%02d-%02d_%02d#%02d#%02d.%03d.db"),
				date_.wYear    ,
				date_.wMonth   ,
				date_.wDay     ,
				date_.wHour    ,
				date_.wMinute  ,
				date_.wSecond  ,
				date_.wMilliseconds
			);
		return cs_name;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CTest_0_LocDbConn::CTest_0_LocDbConn(ITestModuleConsoleCallback& _clbk, CDbProvider& _prov) :
  TBase(_clbk, _T("CTest_0_LocDbConn")), m_prov_ref(_prov) {}
CTest_0_LocDbConn::~CTest_0_LocDbConn(void) {}

/////////////////////////////////////////////////////////////////////////////


HRESULT       CTest_0_LocDbConn::Run  (void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (false == this->IsApplicable()) {
		m_error.SetState(
			(DWORD)ERROR_INVALID_STATE,
			_T("The app is not configured to run [loc.db.conn] test.")
		);
		return m_error;
	}

	HRESULT hr_     = S_OK;
	LPCTSTR lpsz_db = details::CTest_LocalDb_CmdArg_0();

	const TCommandLine& cmd_line = GetAppObjectRef().CommandLine();

	if (m_db_path.IsEmpty()) {
		m_db_path = cmd_line.Argument(lpsz_db);
	}

	if (m_db_path.IsEmpty()) {
		GetAppObjectRef().GetPathFromAppFolder(_T(".\\"), m_db_path);
		m_db_path += details::CTest_LocalDb_DefaultFileName();
	}

	const CDataSchema& schema_ = m_prov_ref.Database().Schemata().Default();

	if (cmd_line.Has(details::CTest_LocalDb_CmdArg_1())) {

		CAtlString cs_xml = cmd_line.Argument(details::CTest_LocalDb_CmdArg_1());

		CGenericPath xml_path = (LPCTSTR)cs_xml;
		if (false == xml_path.IsAbsolute()) {
			cs_xml = xml_path.ToAbsolute();
		}

		CDataSchemaLocator locator_;
		hr_ = locator_.LoadFromFile(cs_xml);
		if (FAILED(hr_))
			return (m_error = locator_.Error());

		hr_ = this->ApplySchema(locator_.Data());
		if (FAILED(hr_))
			return hr_;
	}
	else if (schema_.IsValid() == false)
		return (m_error = schema_.Error());


	hr_ = m_prov_ref.Accessor().Open(m_db_path, schema_);
	if (FAILED(hr_))
		return (m_error = m_prov_ref.Accessor().Error());

	return m_error;
}

bool          CTest_0_LocDbConn::IsApplicable(LPCTSTR lpszArgName, LPCTSTR lpszArgValue)
{
	lpszArgName; lpszArgValue;
	const TCommandLine& cmd_line = GetAppObjectRef().CommandLine();
#if defined(_DEBUG)
	// no argument is provided; it seems the test is run by this console manually;
	if (0 == cmd_line.Count())
		return true;
#endif
	return cmd_line.Has(details::CTest_LocalDb_CmdArg_0());
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CTest_0_LocDbConn::ApplySchema(const UINT uResourceId) {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (0 == uResourceId)
		return (m_error = E_INVALIDARG);

	CAtlString cs_xml_data;

	CGenericResourceLoader loader_;
	HRESULT hr_ = loader_.LoadRcDataAsUtf8(uResourceId, cs_xml_data);
	if (FAILED(hr_))
		return (m_error = loader_.Error());
	else
		hr_ = this->ApplySchema(cs_xml_data);

	return m_error;
}

HRESULT       CTest_0_LocDbConn::ApplySchema(LPCTSTR lpszXmlData) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == lpszXmlData || 0 == ::_tcslen(lpszXmlData))
		return (m_error = E_INVALIDARG);

	CDataSchema& schema_ = m_prov_ref.Database().Schemata().Default();

	HRESULT hr_ = schema_.Create(lpszXmlData);
	if (FAILED(hr_))
		return (m_error = schema_.Error());

	return m_error;
}

LPCTSTR       CTest_0_LocDbConn::Path  (void)const { return m_db_path.GetString(); }
HRESULT       CTest_0_LocDbConn::Path  (LPCTSTR _val) {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == _val || !::_tcslen(_val))
		m_error = E_INVALIDARG;
	else
		m_db_path = _val;
	return m_error;
}
const
CDataSchema&  CTest_0_LocDbConn::Schema(void)const { return m_prov_ref.Database().Schemata().Default(); }