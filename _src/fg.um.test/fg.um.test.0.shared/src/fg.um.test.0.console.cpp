/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 9:37:43a, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian common test console related interface implementation file.
*/
#include "StdAfx.h"
#include "fg.um.test.0.console.h"
#include "fg.um.test.0.resource.h"

namespace fg { namespace test { namespace common { namespace ctrl_flow
{
	HRESULT Event_OnCreate(void)
	{
		CConsoleWindow console_;
		console_.SetIcon(IDR_RT_MAINICON);
		::SetConsoleOutputCP(CP_UTF8);

		CApplication& app_ref = GetAppObjectRef();
		{
			const CApplication::CVersion& version = app_ref.Version();

			std::wstring w_sep_(77, _T('-'));

			CAtlString cs_title;
			cs_title.Format(
				_T("%s v.%s"), (LPCTSTR)version.CustomValue(_T("Comments")), (LPCTSTR)version.FileVersionFixed()
			);

			::_tprintf(_T("%s\n")  , w_sep_.c_str());
			::_tprintf(_T("\t%s\n"), cs_title.GetString());
			::_tprintf(_T("%s\n"), w_sep_.c_str());
		}
		HRESULT hr_ = S_OK;
		return  hr_;
	}

	HRESULT Event_OnExit(const bool bVerbose, const bool bError)
	{
		if (bVerbose || bError)
		{
			::_tprintf(_T("\n\n\tPress any key or click [x] button to exit"));
			::_gettch();
		}
		HRESULT hr_ = S_OK;
		return  hr_;
	}

	HRESULT Event_OnWait(LPCTSTR lpszPrompt)
	{
		HRESULT hr_ = S_OK;
		TCHAR t_char = 0;
		bool bClearScreen = false;
		do {
			::_tprintf(_T("\n\t%s"), lpszPrompt);
			::_tprintf(_T("\n\t%s"), _T("or\n\tPress [c] key to clear console\n"));

			t_char = ::_gettch();

			bClearScreen = ((TCHAR)'c' == t_char);
			if (bClearScreen){

				shared::user32::CConsoleWindow console_;
				hr_ = console_.ClearContent();
			}
		}
		while (bClearScreen);
		return  hr_;
	}
}}}}

using namespace fg::test::common::ctrl_flow;

HRESULT CConsoleEvents_Base::ITestModuleConsole_OnBefore(LPCTSTR lpszPrompt) {

	global::_out::LogInfo(
		_T("ITestModuleConsole_OnBefore::%s"), lpszPrompt
	);
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT CConsoleEvents_Base::ITestModuleConsole_OnWait  (LPCTSTR lpszPrompt) {

	return Event_OnWait(lpszPrompt);
}

HRESULT CConsoleEvents_Base::ITestModuleConsole_OnAfter (LPCTSTR lpszPrompt) {

	global::_out::LogInfo(
		_T("ITestModuleConsole_OnAfter::%s"), lpszPrompt
	);
	HRESULT hr_ = S_OK;
	return  hr_;
}