/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 10:28:46p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian local database writer common test interface implementation file.
*/
#include "StdAfx.h"
#include "fg.um.test.0.loc.db.write.h"
#include "fg.um.test.0.drv.evt.adapter.h"
#include "fg.um.test.0.resource.h"

using namespace fg::um::test;
using namespace fg::common::filter;
using namespace fg::common::data::local;

#include "Shared_DateTime.h"

using namespace shared::lite::data;

////////////////////////////////////////////////////////////////////////////

namespace fg { namespace um { namespace test { namespace details {
}}}}

/////////////////////////////////////////////////////////////////////////////

CTest_0_LocDbWrite::CTest_0_LocDbWrite(ITestModuleConsoleCallback& _clbk) : 
	TBase(_clbk, m_provider), m_cache(*this), m_provider(CDataSchemaOpt::eDoNotCare), m_b_details(true) {}
CTest_0_LocDbWrite::~CTest_0_LocDbWrite(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CTest_0_LocDbWrite::Run  (void) {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (TBase::IsApplicable() == false) {
		m_error.SetState(
			(DWORD)ERROR_INVALID_STATE,
			_T("The app is not configured to run [loc.db.write] test.")
		);
		return m_error;
	}

	HRESULT hr_ = TBase::ApplySchema(IDR_FG_DB_LOC_SCHEMA_DEFAULT);
	if (FAILED(hr_))
		return hr_;

	hr_ = TBase::Run(); // connects to a database;
	if (FAILED(hr_))
		return hr_;

	if (TBase::Schema().IsValid() == false) {
		return (m_error = TBase::Schema().Error());
	}

	CDbWriter& writer_ = this->m_provider.Writer();
	if (writer_.Error())
		return (m_error = writer_.Error());


	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CTest_0_LocDbWrite::ICacheCallback_OnError(TErrorRef _err) {
	global::_out::LogError(_err);
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT     CTest_0_LocDbWrite::ICacheCallback_OnItemArrive(const CEventItem& _event) {

	CTest_0_DrvEvtAdapter adapter_(TBase::m_callback);

	HRESULT hr_ = S_OK;
	if (this->Details()) {
		hr_ = adapter_.ICacheCallback_OnItemArrive(_event);
		if (FAILED(hr_))
			return hr_;
	}

	CRecord rec_(_event);

	hr_ = this->m_provider.Writer().InsertSync(rec_);  // synchronous call;
	if (FAILED(hr_)) {
		global::_out::LogError(this->m_provider.Writer().Error());
	}
	else {
		static INT count_ = 0; count_ += 1;
		if (this->Details()) {
			global::_out::LogInfo(_T("Record #%04d has been written."), count_);
		}
		else {
			static INT prior_ = 0;

			static CUnixTime time_(true);
			if (time_.IsValid() == false) {
				time_.Current();
			}

			CUnixTime curr_(true); curr_.Current();
			if (curr_.GapWith(time_) > 2) {

				const  INT delta_ = count_ - prior_;
				prior_ = count_;
				if (delta_ > 1) global::_out::LogInfo(_T("Records (%d) have been written."), delta_);
				else            global::_out::LogInfo(_T("One record has been written."));

				time_ = curr_;

			}
		}
	}
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

const CEventCache&  CTest_0_LocDbWrite::Cache(void)const { return m_cache; }
CEventCache&        CTest_0_LocDbWrite::Cache(void)      { return m_cache; }
bool                CTest_0_LocDbWrite::Details(void)const       { return m_b_details; }
VOID                CTest_0_LocDbWrite::Details(const bool _val) { m_b_details = _val; }
VOID                CTest_0_LocDbWrite::LogInfo(void)const {

	global::_out::LogInfo(
		_T("Local database is initialized:\n\t%s\n\tsettings=%s\n"),
		TBase::Path(), (LPCTSTR)m_provider.Accessor().Pragma().ToString()
	);
	global::_out::LogEmptyLine();
	global::_out::LogInfo(
		_T("Database schema is applied:\n%s"), TBase::Schema().ToString()
	);
}
const
CDbProvider&        CTest_0_LocDbWrite::Provider(void)const { return m_provider; }
CDbProvider&        CTest_0_LocDbWrite::Provider(void)      { return m_provider; }