/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 2:20:24p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver removable notification interface implementation file.
*/
#include "StdAfx.h"
#include "fg.um.test.0.drv.evt.remove.h"

using namespace fg::um::test;

/////////////////////////////////////////////////////////////////////////////

CTest_0_DrvEvtRemove::CTest_0_DrvEvtRemove(ITestModuleConsoleCallback& _clbk) :
     TBase(_clbk, _T("CTest_0_DrvEvtRemove")), m_monitor(*this) {}
CTest_0_DrvEvtRemove::~CTest_0_DrvEvtRemove(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTest_0_DrvEvtRemove::Run  (void) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	return m_error;
}

bool       CTest_0_DrvEvtRemove::IsApplicable(LPCTSTR lpszArgName, LPCTSTR lpszArgValue) {
	return TBase::IsApplicable(lpszArgName, lpszArgValue);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT CTest_0_DrvEvtRemove::IRemovableDrive_BeingAdded(const TDriveList& _list)
{
	HRESULT hr_ = S_OK;

	for (size_t i_ = 0; i_ < _list.size(); i_++) {

		hr_ = m_removable.Append(_list[i_].GetString());
		global::_out::LogInfo(_T("Removable drive is appended: %s"), _list[i_].GetString());
	}

	return  hr_;
}
HRESULT CTest_0_DrvEvtRemove::IRemovableDrive_BeingEjected(const TDriveList& _list)
{
	HRESULT hr_ = S_OK;

	for (size_t i_ = 0; i_ < _list.size(); i_++) {

		hr_ = m_removable.Delete(_list[i_].GetString());
		global::_out::LogInfo(_T("Removable drive is excluded: %s"), _list[i_].GetString());
	}

	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CRemovableDriveMonitor&
CTest_0_DrvEvtRemove::Monitor(void) { return m_monitor; }