#ifndef _FGUMTEST9STGOLE_H_9C019A51_7124_418B_B9A5_B7070EC96C5A_INCLUDED
#define _FGUMTEST9STGOLE_H_9C019A51_7124_418B_B9A5_B7070EC96C5A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Jul-2018 at 3:18:52p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian desktop OLE storage feature test interface declaration file.
*/
#include "fg.um.test.0._Iface.h"

namespace fg { namespace um { namespace test {

	using shared::lite::common::CSysError;

	class CTest_9_StgOle : public CTest_0__Iface {

		typedef CTest_0__Iface TBase;

	public:
		CTest_9_StgOle(ITestModuleConsoleCallback&);
		~CTest_9_StgOle(void);

	public:
		virtual HRESULT    Run  (void) override sealed;
		virtual bool       IsApplicable(LPCTSTR lpszArgName = NULL, LPCTSTR lpszArgValue = NULL) override sealed;
	};

}}}

#endif/*_FGUMTEST9STGOLE_H_9C019A51_7124_418B_B9A5_B7070EC96C5A_INCLUDED*/