/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Jul-2017 on 3:10:37p, GMT+7, Phuket, Rawai, Tuesday;
	This is File Guardian desktop features console test application entry point file.
*/
#include "StdAfx.h"
#include "Shared_SystemCore.h"

using namespace shared::lite::sys_core;

#include "fg.um.test.0._Iface.h"
#include "fg.um.test.0.console.h"
#include "fg.um.test.9.stg.ole.resource.h"
#include "fg.um.test.9.stg.ole.h"

namespace fg { namespace test { namespace common { namespace ctrl_flow
{
	class CConsoleEvents : public CConsoleEvents_Base {
	public:
		virtual HRESULT ITestModuleConsole_OnBefore(LPCTSTR lpszPrompt) override sealed {

			global::_out::LogInfo(
					_T("ITestModuleConsole_OnBefore::%s"), lpszPrompt
				);
			HRESULT hr_ = S_OK;
			return  hr_;
		}
	};
}}}}

#ifndef __OUTPUT_TO_CONSOLE
#define __OUTPUT_TO_CONSOLE
#endif

using namespace fg::test::common;
using namespace fg::test::common::ctrl_flow;

/////////////////////////////////////////////////////////////////////////////

INT _tmain(VOID)
{
	CCoInitializer com(false);
	CCoSecurityProvider sec_;

	HRESULT hr_ = sec_.InitDefailt();
	ATLVERIFY(SUCCEEDED(hr_));

	INT result_ = 0;

	const bool bVerbose = true;
	global::_out::OutputToConsole(true);
	global::_out::VerboseMode(bVerbose);

	const TCommandLine& cmd_line = GetAppObjectRef().CommandLine();
	const bool bIgnoreAwaitArg = true;

	if (false == bIgnoreAwaitArg && false==cmd_line.Has(_T("no_break"))) {
		MessageBox(NULL, _T("Awaiting for connecting debugger"), _T("Break Point"), MB_ICONEXCLAMATION | MB_OK);
	}

	hr_ = ctrl_flow::Event_OnCreate();
	if (FAILED(hr_)){
		return ctrl_flow::Event_OnExit(bVerbose, true);
	}

	CConsoleEvents console_evt;
	fg::um::test::CTest_9_StgOle stg_ole_test(console_evt);

	hr_ = stg_ole_test.Run();
	if (FAILED(hr_)) {
		global::_out::LogError(stg_ole_test.Error());
		result_ = ctrl_flow::Event_OnExit(bVerbose, true);
	}
	else
		result_ = ctrl_flow::Event_OnExit(bVerbose, false);

	return result_;
}