#ifndef _FGSTDAFX_H_F2148FA7_9EE1_4ECF_9606_4CE656DA9679_INCLUDED
#define _FGSTDAFX_H_F2148FA7_9EE1_4ECF_9606_4CE656DA9679_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Jul-2017 at 2:43:12p, GMT+7, Phuket, Rawai, Tuesday;
	This is File Guardian desktop features test application precompiled header include file.
*/
#include "..\..\..\fg.um.test.0.shared\src\_prec\StdAfx.h" // trying to make a reference to parent precompiled header without project settings;
                                                           // it seems to be not good way, but nevertheless;

#pragma comment(lib, "fg.um.test.0.shared_v15.lib")

#endif/*_FGSTDAFX_H_F2148FA7_9EE1_4ECF_9606_4CE656DA9679_INCLUDED*/