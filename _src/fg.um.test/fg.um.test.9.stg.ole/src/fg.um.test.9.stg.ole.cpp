/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Jul-2018 at 3:25:30p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian desktop OLE storage feature test interface implementation file.
*/
#include "StdAfx.h"
#include "fg.um.test.9.stg.ole.h"

using namespace fg::um::test;

#include "Shared_GenericAppObject.h"
#include "Shared_GenericConObject.h"
#include "Shared_DateTime.h"
#include "generic.stg.ole.h"

using namespace shared::lite::data;
using namespace shared::user32;
using namespace shared::stg::ole;

/////////////////////////////////////////////////////////////////////////////

CTest_9_StgOle::CTest_9_StgOle(ITestModuleConsoleCallback& _clbk) : TBase(_clbk, _T("CTest_9_StgOle")) {}
CTest_9_StgOle::~CTest_9_StgOle(void){}

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace um { namespace test { namespace details {

	LPCTSTR   CTest_9_Arg0(void) { static LPCTSTR lpsz_arg0_name = _T("stg_path"); return lpsz_arg0_name; }

	VOID      CTest_9_Print(const COleStream& _strm, const size_t _iter) {

		static
		LPCTSTR lpszPattern = _T(
				"%s #%d Content:\n"
			);

		const COleStats_Ex& stats_ex = _strm.Stats();

		global::_out::LogInfo(
			lpszPattern,
			stats_ex.TypeAsText().GetString(),
			_iter
		);

		if (stats_ex.Size().QuadPart) {

			CRawData raw_data;
			raw_data.Create(stats_ex.Size().QuadPart & 0xFFFF);

			HRESULT hr_ = _strm.Read(raw_data);
			if (FAILED(hr_))
				global::_out::LogError(
					_strm.Error()
				);
			else {
				CConsolePage page_(true);
				page_.Dos();

				const PBYTE p_data = raw_data.GetData();
				for ( DWORD i_ = 0; i_ < raw_data.GetSize(); i_++ ) {
					if (i_ % 64 == 0 )
					_tprintf(_T("\n\t\t\t"));
					if (0xd != p_data[i_] && true)
						_tprintf(_T("%C"), (char)p_data[i_]);
					else
						_tprintf(_T("%C"), (char)0x20);
				}
			}
		}
		else
			global::_out::LogInfo(_T("\n\t\t#n/a"));
	}

	VOID      CTest_9_Print(const COleStats_Ex& _stats_ex, const size_t _iter) {

		static
		LPCTSTR lpszPattern = _T(
			"%s #%d details:"
			"\n\t\tClass     : %s"
			"\n\t\tName      : %s"
			"\n\t\tCreated   : %s"
			"\n\t\tModified  : %s"
			"\n\t\tAccessed  : %s"
			"\n\t\tSize      : %lld (bytes)"
		);

		global::_out::LogInfo(
			lpszPattern,
			_stats_ex.TypeAsText().GetString(),
			_iter,
			_stats_ex.ClassAsText().GetString(),
			_stats_ex.Name(),
			CSystemTime((_stats_ex.Created())).DefaultFormatValue().GetString(),
			CSystemTime((_stats_ex.Modified())).DefaultFormatValue().GetString(),
			CSystemTime((_stats_ex.LastAccess())).DefaultFormatValue().GetString(),
			_stats_ex.Size()
		);
	}

}}}}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTest_9_StgOle::Run  (void) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const TCommandLine& cmd_ln = GetAppObjectRef().CommandLine();
	if (cmd_ln.Has(details::CTest_9_Arg0()) == false) {

		m_error.SetState(
			E_INVALIDARG, _T("OLE storage file is not specified.")
		);

		return m_error;
	}

	CAtlString cs_stg = cmd_ln.Argument(details::CTest_9_Arg0());

	TBase::m_callback.ITestModuleConsole_OnBefore(
			_T(__FUNCTION__)
		);
	global::_out::LogInfo(
		_T("OLE storage path is\n\t%s"), cs_stg.GetString()
	);

	COleStorage ole_stg;

	HRESULT hr_ = ole_stg.OpenEx(cs_stg.GetString());
	if (FAILED(hr_))
		return (m_error = ole_stg.Error());

	global::_out::LogInfo(
		_T("OLE storage\n\t%s\n\tis opened."), cs_stg.GetString()
	);
	details::CTest_9_Print(ole_stg.Stats(), 0);

	const t_ole_sub_stg& stgs_ = ole_stg.Storages();
	const t_ole_streams& strm_ = ole_stg.Streams ();
	global::_out::LogInfo(
			_T("OLE storage contains:\n\t\tsub-storage(s): %d\n\t\tstream(s):\t%d\n\t\tproperties:\t%d"),
			stgs_.size(), strm_.size(), 0
		);

	if (strm_.empty() == false) {
		global::_out::LogInfo(_T("Enumerating OLE streams:"));

		for (size_t i_ = 0; i_ < strm_.size(); i_++) {
			const COleStream& stream_ = strm_[i_];
			details::CTest_9_Print(stream_.Stats(), i_);
			details::CTest_9_Print(stream_, i_);
		}
	}

	return m_error;
}

bool       CTest_9_StgOle::IsApplicable(LPCTSTR lpszArgName, LPCTSTR lpszArgValue) {
	return TBase::IsApplicable(lpszArgName, lpszArgValue);
}