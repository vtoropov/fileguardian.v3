/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 12:55:15p, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian desktop agent utility test interface implementation file.
*/
#include "StdAfx.h"
#include "fg.um.test.6.age_utl.h"

using namespace fg::um::test;

#include "fg.ui.agent.handler.install.h"
#include "fg.ui.agent.handler.license.h"
#include "fg.ui.agent.handler.prefer.h"
#include "fg.ui.agent.handler.service.h"
#include "fg.ui.agent.handler.task.h"

using namespace fg::agent::handlers;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace um { namespace test { namespace details {

}}}}
/////////////////////////////////////////////////////////////////////////////

CTest_6_AgentUtil::CTest_6_AgentUtil(ITestModuleConsoleCallback& _clbk) : TBase(_clbk, _T("CTest_6_AgentUtil")) { }

CTest_6_AgentUtil::~CTest_6_AgentUtil(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT         CTest_6_AgentUtil::Run  (void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (false == this->IsApplicable()) {
		m_error.SetState(
			(DWORD)ERROR_INVALID_STATE,
			_T("The app is not configured to run [age.utl] test.")
		);
		return m_error;
	}

	HRESULT  hr_ = S_OK;
	INT nResult_ = CHandlerResult::eSuccess;
	TCommandLine cmd_line = GetAppObjectRef().CommandLine(); // makes a copy;

	global::_out::LogInfo(_T("Trying to run external app..."));

	CInstallerCmdHandler ins_cmd(cmd_line); ins_cmd.DisableTargetAppArgs(true);
	CAtlString cs_app(_T("Notepad"));
	{
		global::_out::LogInfo(_T("Acceptable arguments:\n\t\t%s"), ins_cmd.Args());
		cmd_line.Append(_T("run"), cs_app.GetString());
	}

	if (ins_cmd.IsApplicable()) {
		hr_ = ins_cmd.Handle(nResult_);
		if (FAILED(hr_))
			global::_out::LogError( ins_cmd.Error() );
		else
			global::_out::LogInfo(_T("The target application '%s' has been running successfully."), cs_app.GetString());
	}

	global::_out::LogInfo(_T("Trying to register invalid serial..."));

	CLicenseCmdHandler lic_cmd(cmd_line);
	CAtlString cs_serial = _T("this_is_a_test_serial");
	{
		global::_out::LogInfo(_T("Acceptable arguments:\n\t\t%s"), lic_cmd.Args());
		cmd_line.Clear ();
		cmd_line.Append(_T("license"), _T("register"));
		cmd_line.Append(_T("serial") , cs_serial.GetString());
		cmd_line.Append(_T("desc")   , _T("This license registration is runing from agent test;"));
	}

	if (lic_cmd.IsApplicable()) {
		hr_ = lic_cmd.Handle(nResult_);
		if (FAILED(hr_))
			global::_out::LogError( lic_cmd.Error() );
		else
			global::_out::LogInfo(_T("The license: serial=%s has been registered."), cs_serial.GetString());
	}

	global::_out::LogInfo(_T("Trying to get current license state..."));
	{
		cmd_line.Clear ();
		cmd_line.Append(_T("license"), _T("checkin"));
	}

	if (lic_cmd.IsApplicable()) {
		hr_ = lic_cmd.Handle(nResult_);
		if (FAILED(hr_))
			global::_out::LogError( lic_cmd.Error() );
		else
			global::_out::LogInfo(_T("The license state active."));
	}

	global::_out::LogInfo(_T("Trying to get preferences..."));

	CPreferCmdHandler prf_cmd(cmd_line);
	{
		global::_out::LogInfo(_T("Acceptable arguments:\n\t\t%s"), prf_cmd.Args());
		cmd_line.Clear ();
		cmd_line.Append(_T("def_folder"), NULL);
	}

	if (prf_cmd.IsApplicable()) {
		hr_ = prf_cmd.Handle(nResult_);
		if (FAILED(hr_))
			global::_out::LogError( prf_cmd.Error() );
		else
			global::_out::LogInfo(_T("Default folder:\n\t\t%s"), prf_cmd.FavorFolder());
	}

	global::_out::LogInfo(_T("Trying to start FG service..."));

	CServiceCmdHandler svc_cmd(cmd_line);
	{
		global::_out::LogInfo(_T("Acceptable arguments:\n\t\t%s"), svc_cmd.Args());
		cmd_line.Clear ();
		cmd_line.Append(_T("service"), _T("start"));
	}

	if (svc_cmd.IsApplicable()) {
		hr_ = svc_cmd.Handle(nResult_);
		if (FAILED(hr_))
			global::_out::LogError( svc_cmd.Error() );
		else
			global::_out::LogInfo(_T("FG service has been started"));
	}

	global::_out::LogInfo(_T("Trying to create FG startup task..."));

	CStartupTaskCmdHandler tsk_cmd(cmd_line);
	{
		global::_out::LogInfo(_T("Acceptable arguments:\n\t\t%s"), tsk_cmd.Args());
		cmd_line.Clear ();
		cmd_line.Append(_T("startup_task"), _T("create"));
		cmd_line.Append(_T("exe_path")    , _T("notepad"));
	}

	if (tsk_cmd.IsApplicable()) {
		hr_ = tsk_cmd.Handle(nResult_);
		if (FAILED(hr_))
			global::_out::LogError( tsk_cmd.Error() );
		else
			global::_out::LogInfo(_T("FG startup task is created."));
	}

	global::_out::LogInfo(_T("Trying to remove FG startup task..."));
	{
		cmd_line.Clear ();
		cmd_line.Append(_T("startup_task"), _T("remove"));
	}

	if (tsk_cmd.IsApplicable()) {
		hr_ = tsk_cmd.Handle(nResult_);
		if (FAILED(hr_))
			global::_out::LogError( tsk_cmd.Error() );
		else
			global::_out::LogInfo(_T("FG startup task is removed."));
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

bool     CTest_6_AgentUtil::IsApplicable(LPCTSTR lpszArgName, LPCTSTR lpszArgValue) { lpszArgName; lpszArgValue; return true; }