#ifndef _FGSTDAFX_H_5A8547C4_8C9E_441B_9D25_87D7969194AC_INCLUDED
#define _FGSTDAFX_H_5A8547C4_8C9E_441B_9D25_87D7969194AC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jun-2018 at 12:44:40p, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian desktop agent utility application test precompiled header include file.
*/
#include "..\..\..\fg.um.test.0.shared\src\_prec\StdAfx.h" // trying to make a reference to parent precompiled header without project settings;
                                                           // it seems to be not good way, but nevertheless;
#pragma comment(lib, "fg.um.filter.bridge_v15.lib")
#pragma comment(lib, "fg.um.test.0.shared_v15.lib")
#pragma comment(lib, "fg.generic.agent_v15.lib")

#endif/*_FGSTDAFX_H_5A8547C4_8C9E_441B_9D25_87D7969194AC_INCLUDED*/