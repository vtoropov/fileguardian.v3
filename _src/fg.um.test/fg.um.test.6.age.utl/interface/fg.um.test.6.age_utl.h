#ifndef _FGUMTEST6AGEUTL_H_7A1DEE8B_B81B_4AB5_9D0D_7268CFA67702_INCLUDED
#define _FGUMTEST6AGEUTL_H_7A1DEE8B_B81B_4AB5_9D0D_7268CFA67702_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Jun-2018 at 5:05:32p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian desktop agent utility common test interface declaration file.
*/
#include "fg.um.test.0._Iface.h"

namespace fg { namespace um { namespace test {

	using shared::lite::common::CSysError;

	class CTest_6_AgentUtil : public CTest_0__Iface {

		typedef CTest_0__Iface TBase;

	public:
		CTest_6_AgentUtil(ITestModuleConsoleCallback&);
		~CTest_6_AgentUtil(void);

	public:
		virtual HRESULT    Run  (void) override sealed;
		virtual bool       IsApplicable(LPCTSTR lpszArgName = NULL, LPCTSTR lpszArgValue = NULL) override sealed;
	};
}}}
#endif/*_FGUMTEST6AGEUTL_H_7A1DEE8B_B81B_4AB5_9D0D_7268CFA67702_INCLUDED*/