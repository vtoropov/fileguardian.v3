#ifndef _FLEXGRIDCONTROL_H_BA1D5552_80D7_4361_B632_186A194CD692_INCLUDED
#define _FLEXGRIDCONTROL_H_BA1D5552_80D7_4361_B632_186A194CD692_INCLUDED
/*
	Created by Tech_dog (VToropov) on 31-Aug-2014 at 7:35:13am, GMT+4, Taganrog, Sunday;
	This is Flex Grid Control class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 6:11:43p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "Flex_grd_Ifaces.h"
#include "Flex_grd_Header.h"
#include "Flex_grd_ControlWnd.h"
#include "Flex_grd_Column.h"

namespace flex_grd { namespace impl_
{
	class CFlexGrid : public flex_grd::IFlex_Grid
	{
	private:
		CFlexHeader   m_header;
		CFlexGridWnd  m_wnd;
	public:
		CFlexGrid(void);
		~CFlexGrid(void);
	private:
		virtual const IColumns&  Columns(void)       const override sealed;
		virtual IColumns&        Columns(void)             override sealed;
		virtual IHeader&         Header(void)              override sealed;
		virtual HRESULT          UpdateLayout(const RECT&) override sealed;
	public:
		HRESULT       Create(const HWND hParent, const RECT& rcArea);
		HRESULT       Destroy(void);
	};
}}

#endif/*_FLEXGRIDCONTROL_H_BA1D5552_80D7_4361_B632_186A194CD692_INCLUDED*/