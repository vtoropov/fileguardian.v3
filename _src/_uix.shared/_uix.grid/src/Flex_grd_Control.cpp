/*
	Created by Tech_dog (VToropov) on 31-Aug-2014 at 7:56:24am, GMT+4, Taganrog, Sunday;
	This is Flex Grid Control class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 6:11:44p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Flex_grd_Control.h"

using namespace flex_grd;
using namespace flex_grd::impl_;

/////////////////////////////////////////////////////////////////////////////

CFlexGrid::CFlexGrid(void) : m_wnd(m_header) { }
CFlexGrid::~CFlexGrid(void) { }

/////////////////////////////////////////////////////////////////////////////

const
IColumns&     CFlexGrid::Columns(void)const { return m_header.Columns(); }
IColumns&     CFlexGrid::Columns(void)      { return m_header.Columns(); }
IHeader&      CFlexGrid::Header (void)      { return m_header; }

HRESULT       CFlexGrid::UpdateLayout(const RECT& rcArea)
{
	HRESULT hr_ = m_wnd.Layout().Update(rcArea);
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CFlexGrid::Create(const HWND hParent, const RECT& rcArea)
{
	if (m_wnd.IsWindow())
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	if (!::IsWindow(hParent))
		return OLE_E_INVALIDHWND;
	if (::IsRectEmpty(&rcArea))
		return OLE_E_INVALIDRECT;
	RECT rc_ = rcArea;
	HWND hWindow = m_wnd.Create(hParent, &rc_, NULL, WS_VISIBLE|WS_CHILD|WS_CLIPCHILDREN);
	if (!hWindow)
		return HRESULT_FROM_WIN32(::GetLastError());
	else
	{
		HRESULT hr_ = m_header.Create(m_wnd);
		return  hr_;
	}
}

HRESULT       CFlexGrid::Destroy(void)
{
	m_header.Destroy();
	if (m_wnd.IsWindow())
		m_wnd.SendMessage(WM_CLOSE);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

namespace flex_grd
{
	HRESULT    CreateFlexGrid(const HWND hParent, const RECT& rcArea, IFlex_Grid*& ptr_ref)
	{
		if (ptr_ref)
			return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
		HRESULT hr_ = S_OK;
		try
		{
			CFlexGrid* p_grid = new CFlexGrid();
			hr_ = p_grid->Create(hParent, rcArea);
			if (S_OK != hr_)
			{
				flex_grd::IFlex_Grid* p_interface = p_grid;
				DestroyFlexGrid(p_interface);
			}
			else
				ptr_ref = p_grid;
		}
		catch(::std::bad_alloc&)
		{
			hr_ = E_OUTOFMEMORY;
		}
		return hr_;
	}

	HRESULT    DestroyFlexGrid(IFlex_Grid*& ptr_ref)
	{
		if (!ptr_ref)
			return S_OK;
		HRESULT hr_ = S_OK;
		try
		{
			CFlexGrid* p_grid = dynamic_cast<CFlexGrid*>(ptr_ref);
			if (p_grid)
				p_grid->Destroy();
			if (p_grid)
				delete p_grid;
			p_grid = NULL;
		}
		catch(::std::bad_cast&)
		{
			hr_ = DISP_E_TYPEMISMATCH;
		}
		catch(...)
		{
			hr_ = HRESULT_FROM_WIN32(ERROR_INVALID_ADDRESS);
		}
		return hr_;
	}
}