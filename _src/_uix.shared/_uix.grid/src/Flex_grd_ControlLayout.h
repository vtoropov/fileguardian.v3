#ifndef _FLEXGRIDCTRLLAYOUT_H_AFB54231_F3EF_448c_8760_E30AC9E79B37_INCLUDED
#define _FLEXGRIDCTRLLAYOUT_H_AFB54231_F3EF_448c_8760_E30AC9E79B37_INCLUDED
/*
	Created by Tech_dog (VToropov) on 5-Sep-2014 at 5:37:52pm, GMT+4, Taganrog, Friday;
	This is Flex Grid Control Layout class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 5:55:09p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "Flex_grd_Header.h"

namespace flex_grd { namespace impl_
{
	class CFlexGridLayout
	{
	private:
		CFlexHeader&  m_header;
		CWindow&      m_wnd;
		RECT          m_rcArea;
	public:
		CFlexGridLayout(CWindow& ctrl_wnd, CFlexHeader&);
		~CFlexGridLayout(void);
	public:
		const RECT&   EntireArea(void)const;
		const RECT    GridArea(void)const;
		const
		CFlexHeader&  Header(void)const;
		HRESULT       Recalc(void);
		HRESULT       Update(const RECT& rcArea);
	};
}}

#endif/*_FLEXGRIDCTRLLAYOUT_H_AFB54231_F3EF_448c_8760_E30AC9E79B37_INCLUDED*/