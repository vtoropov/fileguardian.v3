/*
	Created by Tech_dog (VToropov) on 3-Sep-2014 at 8:46:48pm, GMT+4, Taganrog, Wednesday;
	This is Flex Grid Column class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 4:34:01p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Flex_grd_Column.h"

using namespace flex_grd;
using namespace flex_grd::impl_;

/////////////////////////////////////////////////////////////////////////////

CFlexColumn::CFlexColumn(void) : m_eAlign(eColumnAlign::eGeneric), m_nId(0), m_nWidth(0), m_nIndex(-1), m_sink(NULL) {
	::SetRectEmpty(&m_rect);
}

CFlexColumn::~CFlexColumn(void) { }

/////////////////////////////////////////////////////////////////////////////

eColumnAlign::_e CFlexColumn::Align(void)const { return m_eAlign; }
HRESULT          CFlexColumn::Align(const eColumnAlign::_e eAlign)
{
	const bool bChanged = (eAlign != m_eAlign);
	if (bChanged)
	{
		m_eAlign = eAlign;
		if (m_sink)
			m_sink->Columns_OnRequestRedraw();
	}
	return (bChanged ? S_OK : S_FALSE);
}

LPCTSTR          CFlexColumn::Caption(void)const { return m_caption.GetString(); }
HRESULT          CFlexColumn::Caption(LPCTSTR szCaption)
{
	const bool bChanged = (0 != m_caption.Compare(szCaption));
	if (bChanged)
	{
		m_caption = szCaption;
		if (m_sink)
			m_sink->Columns_OnRequestRedraw();
	}
	return (bChanged ? S_OK : S_FALSE);
}

LONG             CFlexColumn::Identifier(void)const { return m_nId; }
HRESULT          CFlexColumn::Identifier(const LONG nId)
{
	const bool bChanged = (nId != m_nId);
	if (bChanged)
		m_nId = nId;
	return (bChanged ? S_OK : S_FALSE);
}

INT              CFlexColumn::Width(void)const { return m_nWidth; }
HRESULT          CFlexColumn::Width(const INT nWidth)
{
	const bool bChanged = (nWidth != m_nWidth);
	if (bChanged)
	{
		m_nWidth = nWidth;
		if (m_sink)
			m_sink->Columns_OnRequestRecalc();
	}
	return (bChanged ? S_OK : S_FALSE);
}

/////////////////////////////////////////////////////////////////////////////

const RECT&      CFlexColumn::DrawArea(void)const     { return m_rect; }
RECT&            CFlexColumn::DrawArea(void)          { return m_rect; }
const INT        CFlexColumn::Index(void)const        { return m_nIndex;   }
VOID             CFlexColumn::Index(const INT nIndex) { m_nIndex = nIndex; }
VOID             CFlexColumn::Callback(IColumnCollectionChange* _sink) { m_sink = _sink; }

/////////////////////////////////////////////////////////////////////////////

CFlexColumns::CFlexColumns(IColumnCollectionChange& _sink) : m_sink(_sink) { }
CFlexColumns::~CFlexColumns(void) {
	if (!m_cols.empty())m_cols.clear();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT          CFlexColumns::Append(const LONG nId, LPCTSTR szCaption, const INT nWidth, const eColumnAlign::_e eAlign)
{
	const IColumn* p_col = this->Find(nId);
	if (p_col)
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	try
	{
		CFlexColumn col_obj;
		IColumn& col_ref = col_obj;
		col_ref.Align(eAlign);
		col_ref.Caption(szCaption);
		col_ref.Identifier(nId);
		col_ref.Width(nWidth);
		col_obj.Index(this->Count());
		col_obj.Callback(&m_sink);

		m_cols.push_back(col_obj);
		m_sink.Columns_OnRequestRecalc();
	}
	catch (::std::bad_alloc&){ return E_OUTOFMEMORY; }
	return S_OK;
}

const IColumn*   CFlexColumns::Column(const INT nIndex) const
{
	if (nIndex < 0 || nIndex > this->Count() - 1)
		return NULL;
	else
		return &(m_cols[nIndex]);
}

INT              CFlexColumns::Count(void) const { return (INT)m_cols.size(); }

const IColumn*   CFlexColumns::Find(const LONG nId) const
{
	for (size_t i_ = 0; i_ < m_cols.size(); i_++)
	{
		const IColumn& col_ref = m_cols[i_];
		if (col_ref.Identifier() == nId)
			return &col_ref;
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////

const
TColumns&        CFlexColumns::Objects(void)const { return m_cols; }
TColumns&        CFlexColumns::Objects(void)      { return m_cols; }