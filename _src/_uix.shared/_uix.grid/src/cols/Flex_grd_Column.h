#ifndef _FLEXGRIDCOLUMN_H_E7BF4738_7904_4fc3_B865_6FA782CB4CD1_INCLUDED
#define _FLEXGRIDCOLUMN_H_E7BF4738_7904_4fc3_B865_6FA782CB4CD1_INCLUDED
/*
	Created by Tech_dog (VToropov) on 3-Sep-2014 at 7:23:27pm, GMT+4, Taganrog, Wednesday;
	This is Flex Grid Column class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 4:20:44p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "Flex_grd_Ifaces.h"

namespace flex_grd { namespace impl_
{
	interface IColumnCollectionChange
	{
		virtual  VOID   Columns_OnRequestRecalc(void) PURE;
		virtual  VOID   Columns_OnRequestRedraw(void) PURE;
	};

	class CFlexColumn : public flex_grd::IColumn
	{
	private:
		eColumnAlign::_e         m_eAlign;
		CAtlString               m_caption;
		LONG                     m_nId;
		INT                      m_nWidth;
	private:
		INT                      m_nIndex;
		RECT                     m_rect;
		IColumnCollectionChange* m_sink;
	public:
		CFlexColumn(void);
		~CFlexColumn(void);
	private:
		virtual eColumnAlign::_e Align(void)              const override sealed;
		virtual HRESULT          Align(const eColumnAlign::_e)  override sealed;
		virtual LPCTSTR          Caption(void)            const override sealed;
		virtual HRESULT          Caption(LPCTSTR)               override sealed;
		virtual LONG             Identifier(void)         const override sealed;
		virtual HRESULT          Identifier(const LONG)         override sealed;
		virtual INT              Width(void)              const override sealed;
		virtual HRESULT          Width(const INT)               override sealed;
	public:
		VOID                     Callback(IColumnCollectionChange*);
		const RECT&              DrawArea(void)const;
		RECT&                    DrawArea(void);
		const INT                Index(void)const;
		VOID                     Index(const INT);
	};

	typedef ::std::vector<CFlexColumn> TColumns;

	class CFlexColumns : public flex_grd::IColumns
	{
	private:
		TColumns                 m_cols;
		IColumnCollectionChange& m_sink;
	public:
		CFlexColumns(IColumnCollectionChange&);
		~CFlexColumns(void);
	private:
		virtual HRESULT          Append(const LONG nId, LPCTSTR szCaption, const INT nWidth, const eColumnAlign::_e = eColumnAlign::eGeneric) override sealed;
		virtual const IColumn*   Column(const INT nIndex) const override sealed;
		virtual INT              Count(void)              const override sealed;
		virtual const IColumn*   Find(const LONG nId)     const override sealed;
	public:
		const
		TColumns&                Objects(void)const;
		TColumns&                Objects(void);
	};
}}

#endif/*_FLEXGRIDCOLUMN_H_E7BF4738_7904_4fc3_B865_6FA782CB4CD1_INCLUDED*/