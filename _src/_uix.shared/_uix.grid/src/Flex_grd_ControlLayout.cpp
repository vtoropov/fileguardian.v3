/*
	Created by Tech_dog (VToropov) on 5-Sep-2014 at 5:46:46pm, GMT+4, Taganrog, Friday;
	This is Flex Grid Control Layout class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 5:55:47p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Flex_grd_ControlLayout.h"
#include "Flex_grd_HeaderLayout.h"

using namespace flex_grd;
using namespace flex_grd::impl_;

/////////////////////////////////////////////////////////////////////////////

CFlexGridLayout::CFlexGridLayout(CWindow& ctrl_wnd, CFlexHeader& _header) : m_header(_header), m_wnd(ctrl_wnd) { ::SetRectEmpty(&m_rcArea); }
CFlexGridLayout::~CFlexGridLayout(void) { }

/////////////////////////////////////////////////////////////////////////////

const RECT&   CFlexGridLayout::EntireArea(void)const  { return m_rcArea; }
const RECT    CFlexGridLayout::GridArea(void)const    {
	RECT rc_ = m_rcArea; rc_.top += CFlexHeaderLayout::GetDefaultHeight();
	return rc_;
}

const
CFlexHeader&  CFlexGridLayout::Header(void)const   { return m_header; }
HRESULT       CFlexGridLayout::Recalc(void)
{
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT       CFlexGridLayout::Update(const RECT& rcArea)
{
	if (!m_wnd.IsWindow())
		return OLE_E_BLANK;
	if (::IsRectEmpty(&rcArea))
		return OLE_E_INVALIDRECT;
	if (::EqualRect(&rcArea, &m_rcArea))
		return S_FALSE;
	m_rcArea = rcArea;
	m_wnd.SetWindowPos(NULL, &m_rcArea, SWP_NOZORDER|SWP_NOACTIVATE);
	HRESULT hr_ = S_OK;
	{
		hr_ = m_header.Layout().Update();
		if (S_OK == hr_)
		{
			RECT rc_ = m_header.Layout().DrawArea();
			m_header.Window().SetWindowPos(NULL, &rc_, SWP_NOACTIVATE|SWP_NOZORDER);
		}
		else
			hr_ = S_OK;
	}
	return  hr_;
}