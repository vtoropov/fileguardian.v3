#ifndef _FLEXGRIDLIBRARYPRECOMPILEDHEADERS_H_A8EF9363_9B52_4183_9C5F_E12E9F24C746_INCLUDED
#define _FLEXGRIDLIBRARYPRECOMPILEDHEADERS_H_A8EF9363_9B52_4183_9C5F_E12E9F24C746_INCLUDED
/*
	Created by Tech_dog (VToropov) on 31-Aug-2014 at 6:38:19am, GMT+4, Taganrog, Sunday;
	This is Flex Grid Control Library Precompiled Header definition file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 4:02:19p, UTC+7, Phuket, Rawai, Sunday;
*/

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef  WINVER
#define  WINVER         0x0600 // this is for use Windows XP (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600 // this is for use Windows XP (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700 // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481) // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996) // security warning: function or variable may be unsafe
#pragma warning(disable: 4458) // declaration of 'nativeCap' hides class member (GDI+);

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <comdef.h>
#include <new>
#include <map>
#include <vector>
#include <typeinfo>

#include <atlsafe.h>
#include <atlconv.h>

#include <atlfile.h>

#include <atlcomtime.h>
#include <comutil.h>

/////////////////////////////////////////////////////////////////////////////
//
// WTL specific header(s)
//
/////////////////////////////////////////////////////////////////////////////

#include <atlapp.h>

#pragma comment(lib, "_uix.draw_v15.lib")

#endif/*_FLEXGRIDLIBRARYPRECOMPILEDHEADERS_H_A8EF9363_9B52_4183_9C5F_E12E9F24C746_INCLUDED*/