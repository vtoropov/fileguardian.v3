#ifndef _FLEXGRIDCONTROLRENDERER_H_2EB48440_82F5_4b3b_8AA4_980780C5C49A_INCLUDED
#define _FLEXGRIDCONTROLRENDERER_H_2EB48440_82F5_4b3b_8AA4_980780C5C49A_INCLUDED
/*
	Created by Tech_dog (VToropov) on 5-Sep-2014 at 6:33:55pm, GMT+4, Taganrog, Friday;
	This is Flex Grid Control Renderer class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 5:53:55p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"
#include "Flex_grd_ControlLayout.h"

namespace flex_grd { namespace impl_ { namespace rendering
{
	using ex_ui::draw::CZBuffer;

	class CFlexGridRenderer
	{
	private:
		const CFlexGridLayout&   m_layout;
	public:
		CFlexGridRenderer(const CFlexGridLayout&);
		~CFlexGridRenderer(void);
	public:
		HRESULT    Draw(CZBuffer&);
	};
}}}

#endif/*_FLEXGRIDCONTROLRENDERER_H_2EB48440_82F5_4b3b_8AA4_980780C5C49A_INCLUDED*/