#ifndef _FLEXGRDFORMAT_H_244427BE_335F_4A5E_B871_8029DE1D9CAA_INCLUDED
#define _FLEXGRDFORMAT_H_244427BE_335F_4A5E_B871_8029DE1D9CAA_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jul-2018 at 6:26:25p, UTC+7, Phuket, Rawai, Monday;
	This is Flex Grid control draw format interface class(es) declaration file. 
*/
#include "Flex_grd_Iformat.h"

namespace flex_grd { namespace format { namespace impl_ {

	using ex_ui::draw::common::CColour;
	using ex_ui::draw::shape::CRectEdge;

	class CFlexFacet : public IFacet {
	private:
		CColour       m_color;
		eFacetType::_e
		              m_type;
	public:
		CFlexFacet(void);
		CFlexFacet(const CColour&, const eFacetType::_e);
		~CFlexFacet(void);

	public:
		virtual const CColour&   Colour(void)       const override sealed;
		virtual       CColour&   Colour(void)             override sealed;
		virtual eFacetType::_e   Type  (void)       const override sealed;
	};

	class CFlexFacetEnum : public IFacets {
		typedef ::std::vector<CFlexFacet> TFacets;
	private:
		TFacets       m_facets;

	public:
		CFlexFacetEnum(void);
		~CFlexFacetEnum(void);

	public:
		virtual       DWORD      Count(void)             const override sealed;
		virtual const IFacet&    Facet(const INT nIndex) const override sealed;
		virtual       IFacet&    Facet(const INT nIndex)       override sealed;
	};

	typedef CFlexFacetEnum CFlexFacets;

	class CFlexGradient : public IGradient {

	private:
		CColour       m_clr_1st;
		CColour       m_clr_2nd;
		eGradientStyle::_e   m_style;
		eGradientType::_e    m_type;

	public:
		CFlexGradient(void);
		~CFlexGradient(void);

	public:
		virtual const CColour&   First (void)   const override sealed;
		virtual       CColour&   First (void)         override sealed;
		virtual const CColour&   Second(void)   const override sealed;
		virtual       CColour&   Second(void)         override sealed;
		virtual const eGradientStyle::_e
		                         Style (void)   const override sealed;
		virtual       VOID       Style (const eGradientStyle::_e) override sealed;
		virtual eGradientType::_e  Type(void)   const override sealed;
		virtual       VOID         Type(const eGradientType::_e) override sealed;
	};

	class CFlexRowGroupFormat : public IRowGroupFormat {
	private:
		CFlexFacets       m_facets;
		CFlexGradient     m_gradient;

	public:
		CFlexRowGroupFormat(void);
		~CFlexRowGroupFormat(void);

	public:
		virtual const IFacets&   Facets(void)   const override sealed;
		virtual       IFacets&   Facets(void)         override sealed;
		virtual const IGradient& Gradient(void) const override sealed;
		virtual       IGradient& Gradient(void)       override sealed;
	};
}}}

#endif/*_FLEXGRDFORMAT_H_244427BE_335F_4A5E_B871_8029DE1D9CAA_INCLUDED*/