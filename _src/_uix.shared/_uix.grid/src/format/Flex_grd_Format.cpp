/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jul-2018 at 7:25:32p, UTC+7, Phuket, Rawai, Monday;
	This is Flex Grid control draw format interface class(es) implementation file. 
*/
#include "StdAfx.h"
#include "Flex_grd_Format.h"

using namespace flex_grd;
using namespace flex_grd::format;
using namespace flex_grd::format::impl_;

/////////////////////////////////////////////////////////////////////////////

CFlexFacet::CFlexFacet(void) : m_type(eFacetType::eLeft), m_color() {}
CFlexFacet::CFlexFacet(const CColour& _clr, const eFacetType::_e _type) : m_type(_type), m_color(_clr) {}
CFlexFacet::~CFlexFacet(void) {}

/////////////////////////////////////////////////////////////////////////////

const CColour&   CFlexFacet::Colour(void)       const { return m_color; }
      CColour&   CFlexFacet::Colour(void)             { return m_color; }
eFacetType::_e   CFlexFacet::Type  (void)       const { return m_type;  }

/////////////////////////////////////////////////////////////////////////////

CFlexFacetEnum::CFlexFacetEnum(void) {}
CFlexFacetEnum::~CFlexFacetEnum(void) {}

/////////////////////////////////////////////////////////////////////////////

      DWORD      CFlexFacetEnum::Count(void)             const { return static_cast<DWORD>(m_facets.size()); }
const IFacet&    CFlexFacetEnum::Facet(const INT nIndex) const {

	if (0 > nIndex || nIndex > (INT)this->Count() - 1) {
		static CFlexFacet invalid_;
		return invalid_;
	}
	else
		return m_facets[nIndex];
}
      IFacet&    CFlexFacetEnum::Facet(const INT nIndex) {
	if (0 > nIndex || nIndex > (INT)this->Count() - 1) {
		static CFlexFacet invalid_;
		return invalid_;
	}
	else
		return m_facets[nIndex];
}

/////////////////////////////////////////////////////////////////////////////

CFlexGradient::CFlexGradient(void) {}
CFlexGradient::~CFlexGradient(void) {}

/////////////////////////////////////////////////////////////////////////////

const CColour&   CFlexGradient::First (void)   const { return m_clr_1st; }
      CColour&   CFlexGradient::First (void)         { return m_clr_1st; }
const CColour&   CFlexGradient::Second(void)   const { return m_clr_2nd; }
      CColour&   CFlexGradient::Second(void)         { return m_clr_2nd; }
const eGradientStyle::_e
                 CFlexGradient::Style (void)   const { return m_style;   }
      VOID       CFlexGradient::Style (const eGradientStyle::_e _val) { m_style = _val; }
eGradientType::_e  CFlexGradient::Type(void)   const { return m_type; }
      VOID         CFlexGradient::Type(const eGradientType::_e _val)  { m_type = _val; }

/////////////////////////////////////////////////////////////////////////////

CFlexRowGroupFormat::CFlexRowGroupFormat(void) {}
CFlexRowGroupFormat::~CFlexRowGroupFormat(void) {}

/////////////////////////////////////////////////////////////////////////////

const IFacets&   CFlexRowGroupFormat::Facets(void)   const { return m_facets; }
      IFacets&   CFlexRowGroupFormat::Facets(void)         { return m_facets; }
const IGradient& CFlexRowGroupFormat::Gradient(void) const { return m_gradient; }
      IGradient& CFlexRowGroupFormat::Gradient(void)       { return m_gradient; }