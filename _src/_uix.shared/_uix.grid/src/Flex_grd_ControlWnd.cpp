/*
	Created by Tech_dog (VToropov) on 31-Aug-2014 at 11:57:48am, GMT+4, Taganrog, Sunday;
	This is Flex Grid Control Window class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 6:06:34p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Flex_grd_ControlWnd.h"

using namespace flex_grd;
using namespace flex_grd::impl_;
using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

/////////////////////////////////////////////////////////////////////////////

CFlexGridWnd::CFlexGridWnd(CFlexHeader& _header) : m_layout(*this, _header), m_renderer(m_layout) { }
CFlexGridWnd::~CFlexGridWnd(void) { }

/////////////////////////////////////////////////////////////////////////////

LRESULT   CFlexGridWnd::OnCreate (UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	wParam; lParam; bHandled = TRUE;
	return 0;
}

LRESULT   CFlexGridWnd::OnDestroy(UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	wParam; lParam; bHandled = FALSE;
	return 0;
}

LRESULT   CFlexGridWnd::OnEraseBg(UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	wParam; lParam; bHandled = TRUE;
	RECT rc_ = m_layout.GridArea();
	//
	// TODO: create the image cache
	//
	CZBuffer dc_((HDC)wParam, rc_);
	m_renderer.Draw(dc_);

	return 1;
}

/////////////////////////////////////////////////////////////////////////////

CFlexGridLayout&    CFlexGridWnd::Layout(void) { return m_layout; }