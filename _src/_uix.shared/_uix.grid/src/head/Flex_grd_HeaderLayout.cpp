/*
	Created by Tech_dog (VToropov) on 1-Sep-2014 at 11:17:46am, GMT+4, Taganrog, Monday;
	This is Flex Grid Header Layout class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 4:41:13p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Flex_grd_HeaderLayout.h"

using namespace flex_grd;
using namespace flex_grd::impl_;

CFlexHeaderLayout::CFlexHeaderLayout(CWindow& header_wnd, CFlexColumns& _cols) : m_header_wnd(header_wnd), m_cols(_cols)
{
	::SetRectEmpty(&m_rcDrawArea);
}

CFlexHeaderLayout::~CFlexHeaderLayout(void) { }

/////////////////////////////////////////////////////////////////////////////

const
CFlexColumns& CFlexHeaderLayout::Columns(void)const  { return m_cols; }
const RECT&   CFlexHeaderLayout::DrawArea(void)const { return m_rcDrawArea; }

HRESULT       CFlexHeaderLayout::Recalc(void)
{
	const RECT& rc_ = m_rcDrawArea;

	INT n_left = rc_.left;
	TColumns& cols_ = m_cols.Objects();
	for (size_t i_ = 0; i_ < cols_.size(); i_++)
	{
		CFlexColumn& col_ = cols_[i_];
		::SetRect(&(col_.DrawArea()), n_left, rc_.top, n_left + ((const IColumn&)col_).Width(), rc_.bottom);
		n_left += ((const IColumn&)col_).Width();
	}
	return (cols_.size() ? S_OK : S_FALSE);
}

HRESULT       CFlexHeaderLayout::Update(void)
{
	if (!m_header_wnd.IsWindow())
		return OLE_E_BLANK;
	RECT rc_ = {0};
	m_header_wnd.GetParent().GetClientRect(&rc_);
	if (::IsRectEmpty(&rc_))
		return OLE_E_INVALIDRECT;
	rc_.bottom = rc_.top + CFlexHeaderLayout::GetDefaultHeight();
	if (::EqualRect(&m_rcDrawArea, & rc_))
		return S_FALSE;
	m_rcDrawArea = rc_;

	this->Recalc();

	return S_OK;
}

////////////////////////////////////////////////////////////////////////////

INT           CFlexHeaderLayout::GetDefaultHeight(void)
{
	return 25;
}