/*
	Created by Tech_dog (VToropov) on 2-Sep-2014 at 12:27:56pm, GMT+4, Taganrog, Tuesday;
	This is Flex Grid Header Shaper class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 4:49:12p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Flex_grd_HeaderRenderer.h"

using namespace flex_grd;
using namespace flex_grd::impl_;
using namespace flex_grd::impl_::rendering;

using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

/////////////////////////////////////////////////////////////////////////////

namespace flex_grd { namespace impl_ { namespace rendering { namespace details
{
	typedef ex_ui::draw::common::CColour CColorEx;

	HFONT FlexHeaderRenderer_DefaultFontObject(void)
	{
		static CFont default_(_T("Tahoma"), eCreateFontOption::eNone, 0);
		return default_.GetHandle();
	}

	void  FlexHeaderRenderer_DrawBackground(Gdiplus::Graphics& _gp, const RECT _rc)
	{
		{
			CColorEx c_0(RGB(210, 210, 210));
			CColorEx c_1(RGB(200, 200, 200));

			CRectEx rc_e(_rc);

			Gdiplus::LinearGradientBrush br_(rc_e, c_0, c_1, Gdiplus::LinearGradientModeVertical);

			_gp.FillRectangle(&br_, rc_e);
		}
		{
			CColorEx c_0(RGB(255, 255, 255), eAlphaValue::eTransparent60);
			CColorEx c_1(RGB(255, 255, 255), eAlphaValue::eTransparent90);

			RECT rc_ = _rc; rc_.bottom = rc_.top + __H(rc_) / 2;

			CRectEx rc_e(rc_);

			Gdiplus::LinearGradientBrush br_(rc_e, c_0, c_1, Gdiplus::LinearGradientModeVertical);

			_gp.FillRectangle(&br_, rc_e);
		}
		{
			CColorEx c_0(RGB(186, 186, 186));
			CColorEx c_1(RGB(230, 230, 230));
			Gdiplus::Pen pen_0(c_0);
			Gdiplus::Pen pen_1(c_1);

			RECT rc_ = _rc;

			_gp.DrawLine(&pen_0, rc_.left, rc_.bottom - 1, rc_.right, rc_.bottom - 1);
			_gp.DrawLine(&pen_1, rc_.left, rc_.top    + 0, rc_.right, rc_.top    + 0);
		}
	}

	void  FlexHeaderRenderer_DrawColCaption(CZBuffer& _dc, const CFlexColumn& _col)
	{
		LPCTSTR p_cap = ((const IColumn&)_col).Caption();
		if (!::_tcslen(p_cap))
			return;
		RECT rc_ = _col.DrawArea(); rc_.top += 3;

		const HFONT default_ = FlexHeaderRenderer_DefaultFontObject();
		_dc.DrawTextExt(p_cap, default_, rc_, RGB(32, 32, 32), DT_CENTER|DT_VCENTER);
	}

	void  FlexHeaderRenderer_DrawColDivider(Gdiplus::Graphics& _gp, const CFlexColumn& _col)
	{
		CColorEx c_0(RGB(186, 186, 186));
		CColorEx c_1(RGB(230, 230, 230));

		const RECT& rc_ = _col.DrawArea();
		Gdiplus::Pen pen_0(c_0);
		Gdiplus::Pen pen_1(c_1);

		_gp.DrawLine(&pen_0, rc_.right - 1, rc_.top, rc_.right - 1, rc_.bottom);
		_gp.DrawLine(&pen_1, rc_.right - 0, rc_.top, rc_.right - 0, rc_.bottom);
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CFlexHeaderRenderer::CFlexHeaderRenderer(const CFlexHeaderLayout& _layout) : m_layout(_layout) { }
CFlexHeaderRenderer::~CFlexHeaderRenderer(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT    CFlexHeaderRenderer::Draw(CZBuffer& _dc)
{
	RECT rc_ = m_layout.DrawArea();
	Gdiplus::Graphics gp_(_dc);

	details::FlexHeaderRenderer_DrawBackground(gp_, rc_);

	const TColumns& cols_ = m_layout.Columns().Objects();
	for (size_t i_ = 0; i_ < cols_.size(); i_++)
	{
		const CFlexColumn& col_ = cols_[i_];
		details::FlexHeaderRenderer_DrawColDivider(gp_, col_);
		details::FlexHeaderRenderer_DrawColCaption(_dc, col_);
	}

	return S_OK;
}