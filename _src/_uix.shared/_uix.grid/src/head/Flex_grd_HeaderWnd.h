#ifndef _FLEXGRIDHEADERWINDOW_H_F5C7A4F9_A532_4577_B005_CA476A0F8558_INCLUDED
#define _FLEXGRIDHEADERWINDOW_H_F5C7A4F9_A532_4577_B005_CA476A0F8558_INCLUDED
/*
	Created by Tech_dog (VToropov) on 1-Sep-2014 at 9:51:59am, GMT+4, Taganrog, Monday;
	This is Flex Grid Header Window class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 4:46:45p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "Flex_grd_Column.h"
#include "Flex_grd_HeaderLayout.h"
#include "Flex_grd_HeaderRenderer.h"

namespace flex_grd { namespace impl_
{
	using flex_grd::impl_::rendering::CFlexHeaderRenderer;

	class CFlexHeaderWnd :
		public  ::ATL::CWindowImpl<CFlexHeaderWnd>
	{
		typedef ::ATL::CWindowImpl<CFlexHeaderWnd> TWindow;
	private:
		CFlexHeaderLayout    m_layout;
		CFlexHeaderRenderer  m_renderer;
	public:
		DECLARE_WND_CLASS(_T("flex_grd::impl_::CFlexHeaderWnd"));
		BEGIN_MSG_MAP(CFlexHeaderWnd)
			MESSAGE_HANDLER(WM_CREATE    , OnCreate )
			MESSAGE_HANDLER(WM_DESTROY   , OnDestroy)
			MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBg)
		END_MSG_MAP()
	private:
		LRESULT   OnCreate (UINT, WPARAM, LPARAM, BOOL&);
		LRESULT   OnDestroy(UINT, WPARAM, LPARAM, BOOL&);
		LRESULT   OnEraseBg(UINT, WPARAM, LPARAM, BOOL&);
	public:
		CFlexHeaderWnd(CFlexColumns&);
		~CFlexHeaderWnd(void);
	public:
		CFlexHeaderLayout& Layout(void);
	};
}}

#endif/*_FLEXGRIDHEADERWINDOW_H_F5C7A4F9_A532_4577_B005_CA476A0F8558_INCLUDED*/