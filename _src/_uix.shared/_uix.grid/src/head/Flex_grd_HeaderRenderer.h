#ifndef _FLEXGRIDHEADERRENDERER_H_900146D2_4D48_4536_8EF7_4A093536895A_INCLUDED
#define _FLEXGRIDHEADERRENDERER_H_900146D2_4D48_4536_8EF7_4A093536895A_INCLUDED
/*
	Created by Tech_dog (VToropov) on 2-Sep-2014 at 1:46:34am, GMT+4, Taganrog, Tuesday;
	This is Flex Grid Header Renderer class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 4:49:10p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "UIX_GdiProvider.h"
#include "UIX_GdiObject.h"
#include "Flex_grd_HeaderLayout.h"

namespace flex_grd { namespace impl_ { namespace rendering
{
	using flex_grd::impl_::CFlexHeaderLayout;

	using ex_ui::draw::CZBuffer;
	using ex_ui::draw::common::CFont;

	class CFlexHeaderRenderer
	{
	private:
		const CFlexHeaderLayout&  m_layout;
	public:
		CFlexHeaderRenderer(const CFlexHeaderLayout&);
		~CFlexHeaderRenderer(void);
	public:
		HRESULT    Draw(CZBuffer&);
	};
}}}

#endif/*_FLEXGRIDHEADERRENDERER_H_900146D2_4D48_4536_8EF7_4A093536895A_INCLUDED*/