/*
	Created by Tech_dog (VToropov) on 31-Aug-2014 at 8:05:17am, GMT+4, Taganrog, Sunday;
	This is Flex Grid Control Header class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 4:47:03p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Flex_grd_Header.h"
#include "Flex_grd_HeaderLayout.h"

using namespace flex_grd;
using namespace flex_grd::impl_;

/////////////////////////////////////////////////////////////////////////////

namespace flex_grd { namespace impl_ { namespace details
{
}}}

/////////////////////////////////////////////////////////////////////////////

CFlexHeader::CFlexHeader(void) : m_wnd(m_cols), m_cols(*this) { }
CFlexHeader::~CFlexHeader(void) { }

/////////////////////////////////////////////////////////////////////////////

VOID         CFlexHeader::Visible(const bool b_set)
{
	if (!m_wnd.IsWindow())
		return;
	const DWORD dw_cmd = (b_set ? SW_SHOW : SW_HIDE);
	m_wnd.ShowWindow(dw_cmd);
}

/////////////////////////////////////////////////////////////////////////////

VOID   CFlexHeader::Columns_OnRequestRecalc(void)
{
	this->Layout().Recalc();
	m_wnd.Invalidate();
}

VOID   CFlexHeader::Columns_OnRequestRedraw(void)
{
	m_wnd.Invalidate();
}

/////////////////////////////////////////////////////////////////////////////

const
CFlexColumns&      CFlexHeader::Columns(void)const { return m_cols; }
CFlexColumns&      CFlexHeader::Columns(void)      { return m_cols; }

HRESULT            CFlexHeader::Create(const HWND hParent)
{
	if (m_wnd.IsWindow())
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	if (!::IsWindow(hParent))
		return OLE_E_INVALIDHWND;
	RECT rc_ = {0};
	::GetClientRect(hParent, &rc_);
	if (::IsRectEmpty(&rc_))
		return OLE_E_INVALIDRECT;

	const INT n_height = CFlexHeaderLayout::GetDefaultHeight();
	rc_.bottom = rc_.top + n_height;

	HWND hWindow = m_wnd.Create(hParent, &rc_, NULL, WS_VISIBLE|WS_CHILD);
	if (!hWindow)
		return HRESULT_FROM_WIN32(::GetLastError());
	else
	{
		this->Layout().Update();
		return S_OK;
	}
}

HRESULT            CFlexHeader::Destroy(void)
{
	if (m_wnd.IsWindow())
		m_wnd.SendMessage(WM_CLOSE);
	return S_OK;
}

CFlexHeaderLayout& CFlexHeader::Layout(void) { return m_wnd.Layout(); }
CWindow&           CFlexHeader::Window(void) { return m_wnd;          }