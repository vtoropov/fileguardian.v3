#ifndef _FLEXGRIDHEADERLAYOUT_H_B38608F4_B372_44bf_9C18_B45B5F0CE045_INCLUDED
#define _FLEXGRIDHEADERLAYOUT_H_B38608F4_B372_44bf_9C18_B45B5F0CE045_INCLUDED
/*
	Created by Tech_dog (VToropov) on 1-Sep-2014 at 10:14:23am, GMT+4, Taganrog, Monday;
	This is Flex Grid Header Layout class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 4:41:11p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "Flex_grd_Column.h"

namespace flex_grd { namespace impl_
{
	class CFlexHeaderLayout
	{
	private:
		CFlexColumns& m_cols;
		CWindow&      m_header_wnd;
		RECT          m_rcDrawArea;
	public:
		CFlexHeaderLayout(CWindow& header_wnd, CFlexColumns&);
		~CFlexHeaderLayout(void);
	public:
		const
		CFlexColumns& Columns(void)const;
		const RECT&   DrawArea(void)const;
		HRESULT       Recalc(void);
		HRESULT       Update(void);
	public:
		static INT    GetDefaultHeight(void);
	};
}}

#endif/*_FLEXGRIDHEADERLAYOUT_H_B38608F4_B372_44bf_9C18_B45B5F0CE045_INCLUDED*/