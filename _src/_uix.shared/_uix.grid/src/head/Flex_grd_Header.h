#ifndef _FLEXGRIDCTRLHEADER_H_AD0C0C62_DFD3_4617_B5EE_3CC0C08BD8BE_INCLUDED
#define _FLEXGRIDCTRLHEADER_H_AD0C0C62_DFD3_4617_B5EE_3CC0C08BD8BE_INCLUDED
/*
	Created by Tech_dog (VToropov) on 31-Aug-2014 at 7:48:08am, GMT+4, Taganrog, Sunday;
	This is Flex Grid Control Header class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 5:46:01p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "Flex_grd_Ifaces.h"
#include "Flex_grd_Column.h"
#include "Flex_grd_HeaderWnd.h"
#include "Flex_grd_HeaderLayout.h"

namespace flex_grd { namespace impl_
{
	class CFlexHeader : public flex_grd::IHeader, public IColumnCollectionChange
	{
	private:
		CFlexColumns           m_cols;
		CFlexHeaderWnd         m_wnd;
	public:
		CFlexHeader(void);
		~CFlexHeader(void);
	private:
		virtual VOID           Visible(const bool) override sealed;
	private:
		virtual VOID   Columns_OnRequestRecalc(void) override sealed;
		virtual VOID   Columns_OnRequestRedraw(void) override sealed;
	public:
		const CFlexColumns&    Columns(void)const;
		CFlexColumns&          Columns(void);
		HRESULT                Create(const HWND hParent);
		HRESULT                Destroy(void);
		CFlexHeaderLayout&     Layout(void);
		CWindow&               Window(void);
	private:
		CFlexHeader(const CFlexHeader&);
		CFlexHeader& operator= (const CFlexHeader&);
	};
}}

#endif/*_FLEXGRIDCTRLHEADER_H_AD0C0C62_DFD3_4617_B5EE_3CC0C08BD8BE_INCLUDED*/