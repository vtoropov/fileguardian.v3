/*
	Created by Tech_dog (VToropov) on 1-Sep-2014 at 10:02:29am, GMT+4, Taganrog, Monday;
	This is Flex Grid Header Window class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 4:47:07p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Flex_grd_HeaderWnd.h"

using namespace flex_grd;
using namespace flex_grd::impl_;
using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

/////////////////////////////////////////////////////////////////////////////

CFlexHeaderWnd::CFlexHeaderWnd(CFlexColumns& _cols) : m_layout(*this, _cols), m_renderer(m_layout) { }
CFlexHeaderWnd::~CFlexHeaderWnd(void) { }

/////////////////////////////////////////////////////////////////////////////

LRESULT   CFlexHeaderWnd::OnCreate (UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	wParam; lParam; bHandled = TRUE;
	return 0;
}

LRESULT   CFlexHeaderWnd::OnDestroy(UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	wParam; lParam; bHandled = FALSE;
	return 0;
}

LRESULT   CFlexHeaderWnd::OnEraseBg(UINT, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	wParam; lParam; bHandled = TRUE;
	RECT rc_ = m_layout.DrawArea();

	//
	// TODO: create the image cache
	//
	CZBuffer dc_((HDC)wParam, rc_);

	m_renderer.Draw(dc_);

	return 1;
}

/////////////////////////////////////////////////////////////////////////////

CFlexHeaderLayout& CFlexHeaderWnd::Layout(void) { return m_layout; }