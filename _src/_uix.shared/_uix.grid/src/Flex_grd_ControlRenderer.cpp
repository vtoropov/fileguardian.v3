/*
	Created by Tech_dog (VToropov) on 5-Sep-2014 at 6:40:31pm, GMT+4, Taganrog, Friday;
	This is Flex Grid Control Renderer class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 5:57:23p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Flex_grd_ControlRenderer.h"

using namespace flex_grd;
using namespace flex_grd::impl_;
using namespace flex_grd::impl_::rendering;

using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

/////////////////////////////////////////////////////////////////////////////

namespace flex_grd { namespace impl_ { namespace rendering { namespace details
{
	typedef ex_ui::draw::common::CColour CColorEx;

	void  FlexGridRenderer_DrawColBkgnd(Gdiplus::Graphics& _gp, const CFlexColumn& _col, const RECT _area)
	{
		const bool b_even = (0 != (_col.Index() % 2));

		const RECT rc_ = {_col.DrawArea().left, _area.top, _col.DrawArea().right, _area.bottom};
		CRectEx rc_e(rc_);

		if (b_even)
		{
			CColorEx c_(RGB(230, 230, 230));
			Gdiplus::SolidBrush br_(c_);

			_gp.FillRectangle(&br_, rc_e);
		}
		else
		{
			CColorEx c_(RGB(240, 240, 240));
			Gdiplus::SolidBrush br_(c_);

			_gp.FillRectangle(&br_, rc_e);
		}
	}

	void  FlexGridRenderer_DrawRestBkgnd(Gdiplus::Graphics& _gp, const CFlexColumn& _last_col, const RECT _area)
	{
		const RECT& rc_col = _last_col.DrawArea();
		if (rc_col.right >= _area.right)
			return;
		const RECT rc_draw = {rc_col.right, _area.top, _area.right, _area.bottom};
		const bool b_even  = (0 != (_last_col.Index() % 2));

		CRectEx rc_e(rc_draw);

		if (!b_even)
		{
			CColorEx c_(RGB(230, 230, 230));
			Gdiplus::SolidBrush br_(c_);

			_gp.FillRectangle(&br_, rc_e);
		}
		else
		{
			CColorEx c_(RGB(240, 240, 240));
			Gdiplus::SolidBrush br_(c_);

			_gp.FillRectangle(&br_, rc_e);
		}
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CFlexGridRenderer::CFlexGridRenderer(const CFlexGridLayout& _layout) : m_layout(_layout) { }
CFlexGridRenderer::~CFlexGridRenderer(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT    CFlexGridRenderer::Draw(CZBuffer& _dc)
{
	RECT rc_ = m_layout.GridArea();
	Gdiplus::Graphics gp_(_dc);

	const TColumns& cols_ = m_layout.Header().Columns().Objects();
	for (size_t i_ = 0; i_ < cols_.size(); i_++)
	{
		const CFlexColumn& col_ = cols_[i_];
		details::FlexGridRenderer_DrawColBkgnd(gp_, col_, rc_);
	}
	if (cols_.size() > 0)
	{
		const CFlexColumn& last_col_ = cols_[cols_.size() - 1];
		details::FlexGridRenderer_DrawRestBkgnd(gp_, last_col_, rc_);
	}
	return S_OK;
}