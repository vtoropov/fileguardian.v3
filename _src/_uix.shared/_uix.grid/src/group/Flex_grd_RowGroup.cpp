/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jul-2018 at 0:15:39a, UTC+7, Phuket, Rawai, Monday;
	This is Flex Grid Control row group interface class implementation file.
*/
#include "StdAfx.h"
#include "Flex_grd_RowGroup.h"

using namespace flex_grd;
using namespace flex_grd::impl_;

/////////////////////////////////////////////////////////////////////////////

CFlexRowGroup::CFlexRowGroup(void) : m_expand(true), m_id(::GetTickCount()) {}
CFlexRowGroup::CFlexRowGroup(LPCTSTR lpszName, bool bExpand) : m_expand(bExpand), m_name(lpszName), m_id(::GetTickCount()) {}
CFlexRowGroup::~CFlexRowGroup(void) {}

/////////////////////////////////////////////////////////////////////////////

VOID       CFlexRowGroup::Collapse(void) { m_expand = false; }
VOID       CFlexRowGroup::Expand  (void) { m_expand = true;  }
bool       CFlexRowGroup::Expanded(void)   const { return m_expand; }
DWORD      CFlexRowGroup::Identifier(void) const { return m_id;     }
bool       CFlexRowGroup::IsValid (void)   const { return (0 < m_id && m_name.IsEmpty() == false); }
LPCTSTR    CFlexRowGroup::Name    (void)   const { return m_name.GetString(); }
VOID       CFlexRowGroup::Name    (LPCTSTR _val) { m_name = _val;   }
VOID       CFlexRowGroup::Toggle  (void)         { m_expand = !m_expand; }