#ifndef _FLEXGRDROWGROUP_H_02BE1FB6_3A72_4E01_BFD7_908208ABCD4C_INCLUDED
#define _FLEXGRDROWGROUP_H_02BE1FB6_3A72_4E01_BFD7_908208ABCD4C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jul-2018 at 0:04:18a, UTC+7, Phuket, Rawai, Monday;
	This is Flex Grid Control row group interface class declaration file.
*/
#include "Flex_grd_Ifaces.h"

namespace flex_grd { namespace impl_
{
	using flex_grd::IRowGroup;

	class CFlexRowGroup : public IRowGroup {
	private:
		bool       m_expand;
		DWORD      m_id;
		CAtlString m_name;

	public:
		CFlexRowGroup(void);
		CFlexRowGroup(LPCTSTR lpszName, bool bExpand);
		~CFlexRowGroup(void);

	public:
		virtual VOID             Collapse(void)                 override sealed;
		virtual VOID             Expand  (void)                 override sealed;
		virtual bool             Expanded(void)           const override sealed;
		virtual DWORD            Identifier(void)         const override sealed;
		virtual bool             IsValid (void)           const override sealed;
		virtual LPCTSTR          Name    (void)           const override sealed;
		virtual VOID             Name    (LPCTSTR)              override sealed;
		virtual VOID             Toggle  (void)                 override sealed;
	};
}}

#endif/*_FLEXGRDROWGROUP_H_02BE1FB6_3A72_4E01_BFD7_908208ABCD4C_INCLUDED*/