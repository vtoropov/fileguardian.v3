/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jul-2018 at 11:49:52a, UTC+7, Phuket, Rawai, Monday;
	This is Flex Grid Control row group enumerator interface class implementation file.
*/
#include "StdAfx.h"
#include "Flex_grd_RowGroupEnum.h"

using namespace flex_grd;
using namespace flex_grd::impl_;

/////////////////////////////////////////////////////////////////////////////

CFlexRowGroupEnum::CFlexRowGroupEnum(void) {}
CFlexRowGroupEnum::~CFlexRowGroupEnum(void) {}

/////////////////////////////////////////////////////////////////////////////

VOID       CFlexRowGroupEnum::Clear  (void) {
	if (m_groups.empty() == false)
		m_groups.clear();
}

DWORD      CFlexRowGroupEnum::Count  (void) const { return static_cast<DWORD>(m_groups.size()); }

DWORD      CFlexRowGroupEnum::Insert (LPCTSTR lpszName, const bool bExpanded) {
	DWORD id_ = 0; 
	CFlexRowGroup group_(lpszName, bExpanded);
	try {
		m_groups.push_back(group_);
		id_ = group_.Identifier();
	}
	catch(::std::bad_alloc&) {
	}
	return id_;
}

IRowGroup& CFlexRowGroupEnum::Item   (const INT nIndex) {
	if (0 > nIndex || nIndex > (INT)this->Count() - 1) {
		static CFlexRowGroup invalid_(NULL, false);
		return invalid_;
	}
	else
		return m_groups[nIndex];
}

HRESULT    CFlexRowGroupEnum::Remove (const INT nIndex) {
	if (0 > nIndex || nIndex > (INT)this->Count() - 1)
		return DISP_E_BADINDEX;
	m_groups.erase(m_groups.begin() + nIndex);
	return S_OK;
}