#ifndef _FLEXGRDROWGROUPENUM_H_6F1C25DF_3566_4388_80AD_797A897EC92A_INCLUDED
#define _FLEXGRDROWGROUPENUM_H_6F1C25DF_3566_4388_80AD_797A897EC92A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jul-2018 at 11:16:28a, UTC+7, Phuket, Rawai, Monday;
	This is Flex Grid Control row group enumerator interface class declaration file.
*/
#include "Flex_grd_Ifaces.h"
#include "Flex_grd_RowGroup.h"

namespace flex_grd { namespace impl_
{
	using flex_grd::IRowGroup;
	using flex_grd::IRowGroupEnum;

	class CFlexRowGroupEnum : public IRowGroupEnum {

		typedef ::std::vector<CFlexRowGroup> TRowGroups;

	private:
		TRowGroups       m_groups;

	public:
		CFlexRowGroupEnum(void);
		~CFlexRowGroupEnum(void);

	public:
		virtual VOID             Clear  (void)                  override sealed;
		virtual DWORD            Count  (void)            const override sealed;
		virtual DWORD            Insert (LPCTSTR lpszName,const bool bExpanded)   override sealed; // returns group identifier
		virtual IRowGroup&       Item   (const INT nIndex)      override sealed;
		virtual HRESULT          Remove (const INT nIndex)      override sealed;
	};
}}

#endif/*_FLEXGRDROWGROUPENUM_H_6F1C25DF_3566_4388_80AD_797A897EC92A_INCLUDED*/