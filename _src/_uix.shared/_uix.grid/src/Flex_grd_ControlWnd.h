#ifndef _FLEXGRIDCTRLWINDOW_H_9B88AA9E_56A1_449d_905F_F01C331BD3AB_INCLUDED
#define _FLEXGRIDCTRLWINDOW_H_9B88AA9E_56A1_449d_905F_F01C331BD3AB_INCLUDED
/*
	Created by Tech_dog (VToropov) on 31-Aug-2014 at 11:38:51am, GMT+4, Taganrog, Sunday;
	This is Flex Grid Control Window class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 6:06:01p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "Flex_grd_Header.h"
#include "Flex_grd_ControlLayout.h"
#include "Flex_grd_ControlRenderer.h"

namespace flex_grd { namespace impl_
{
	using flex_grd::impl_::rendering::CFlexGridRenderer;

	class CFlexGridWnd :
		public  ::ATL::CWindowImpl<CFlexGridWnd>
	{
		typedef ::ATL::CWindowImpl<CFlexGridWnd> TWindow;
	private:
		CFlexGridLayout     m_layout;
		CFlexGridRenderer   m_renderer;
	public:
		DECLARE_WND_CLASS(_T("flex_grd::impl_::CFlexGridWnd"));
		BEGIN_MSG_MAP(CFlexGridWnd)
			MESSAGE_HANDLER(WM_CREATE    , OnCreate )
			MESSAGE_HANDLER(WM_DESTROY   , OnDestroy)
			MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBg)
		END_MSG_MAP()
	private:
		LRESULT   OnCreate (UINT, WPARAM, LPARAM, BOOL&);
		LRESULT   OnDestroy(UINT, WPARAM, LPARAM, BOOL&);
		LRESULT   OnEraseBg(UINT, WPARAM, LPARAM, BOOL&);
	public:
		CFlexGridWnd(CFlexHeader&);
		~CFlexGridWnd(void);
	public:
		CFlexGridLayout&    Layout(void);
	};
}}

#endif/*_FLEXGRIDCTRLWINDOW_H_9B88AA9E_56A1_449d_905F_F01C331BD3AB_INCLUDED*/