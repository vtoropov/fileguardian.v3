#ifndef _FLEXGRDIFORMAT_H_9A495DF3_E9CD_4591_A3CD_A15509D21E3E_INCLUDED
#define _FLEXGRDIFORMAT_H_9A495DF3_E9CD_4591_A3CD_A15509D21E3E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jul-2018 at 2:29:37p, UTC+7, Phuket, Rawai, Monday;
	This is Flex Grid control draw format interface declaration file. 
*/
#include "UIX_GdiObject.h"
#include "UIX_GdiDrawShape.h"

namespace flex_grd { namespace format {

	using ex_ui::draw::common::CColour;
	using ex_ui::draw::shape::CRectEdge;

	class eFacetType {
	public:
		enum _e : ULONG {
			eLeft   = 0,
			eTop    = 1,
			eRight  = 2,
			eBottom = 4,
			eAll    = eLeft | eTop | eRight | eBottom
		};
	};

	interface IFacet {
		virtual const CColour&   Colour(void)       const PURE;
		virtual       CColour&   Colour(void)             PURE;
		virtual eFacetType::_e   Type  (void)       const PURE;
	};

	interface IFacets {
		virtual       DWORD      Count(void) const PURE;
		virtual const IFacet&    Facet(const INT nIndex) const PURE;
		virtual       IFacet&    Facet(const INT nIndex)       PURE;
	};

	class eGradientStyle {
	public:
		enum _e : ULONG {
			eConcave = 0,       // more 3-D effects (for gradient themes)
			eFlatten = 1        // little 3-D effects (for non-gradient themes)
		};
	};

	class eGradientType {
	public:
		enum _e : ULONG {
			eVertial    = 0x0, // default;
			eHorizontal = 0x1,
			eStripped   = 0x2,
		};
	};

	interface IGradient {       
		virtual const CColour&   First (void)   const PURE;
		virtual       CColour&   First (void)         PURE;
		virtual const CColour&   Second(void)   const PURE;
		virtual       CColour&   Second(void)         PURE;
		virtual const eGradientStyle::_e
		                         Style (void)   const PURE;
		virtual       VOID       Style (const eGradientStyle::_e) PURE;
		virtual eGradientType::_e  Type(void)   const PURE;
		virtual       VOID         Type(const eGradientType::_e) PURE;
	};

	interface IRowGroupFormat {
		virtual const IFacets&   Facets(void)   const PURE;
		virtual       IFacets&   Facets(void)         PURE;
		virtual const IGradient& Gradient(void) const PURE;
		virtual       IGradient& Gradient(void)       PURE;
	};
}}

#endif/*_FLEXGRDIFORMAT_H_9A495DF3_E9CD_4591_A3CD_A15509D21E3E_INCLUDED*/