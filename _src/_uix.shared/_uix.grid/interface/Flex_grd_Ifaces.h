#ifndef _FLEXGRIDCTRLINTERFACE_H_C5D9E411_ADEF_4b1a_A8B8_78C8546EB7BB_INCLUDED
#define _FLEXGRIDCTRLINTERFACE_H_C5D9E411_ADEF_4b1a_A8B8_78C8546EB7BB_INCLUDED
/*
	Created by Tech_dog (VToropov) on 31-Aug-2014 at 7:08:47am, GMT+4, Taganrog, Sunday;
	This is Flex Grid Control Interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jul-2018 at 4:25:53p, UTC+7, Phuket, Rawai, Sunday;
*/

namespace flex_grd
{
	class eColumnAlign
	{
	public:
		enum _e : LONG {
			eGeneric = 0, // default: text to left, date to center, number to right
			eLeft    = 1,
			eCenter  = 2,
			eRight   = 3
		};
	};

	interface IColumn
	{
		virtual eColumnAlign::_e Align(void)              const PURE;
		virtual HRESULT          Align(const eColumnAlign::_e)  PURE;
		virtual LPCTSTR          Caption(void)            const PURE;
		virtual HRESULT          Caption(LPCTSTR)               PURE;
		virtual LONG             Identifier(void)         const PURE;
		virtual HRESULT          Identifier(const LONG)         PURE;
		virtual INT              Width(void)              const PURE;
		virtual HRESULT          Width(const INT)               PURE;
	};

	interface IColumns
	{
		virtual HRESULT          Append(const LONG nId, LPCTSTR szCaption, const INT nWidth, const eColumnAlign::_e = eColumnAlign::eGeneric) PURE;
		virtual const IColumn*   Column(const INT nIndex) const PURE;
		virtual INT              Count(void)              const PURE;
		virtual const IColumn*   Find(const LONG nId)     const PURE;
	};

	interface IHeader
	{
		virtual VOID             Visible(const bool)            PURE;
	};

	interface IRowGroup {
		virtual VOID             Collapse(void)                 PURE;
		virtual VOID             Expand  (void)                 PURE;
		virtual bool             Expanded(void)           const PURE;
		virtual DWORD            Identifier(void)         const PURE;
		virtual bool             IsValid (void)           const PURE;
		virtual LPCTSTR          Name    (void)           const PURE;
		virtual VOID             Name    (LPCTSTR)              PURE;
		virtual VOID             Toggle  (void)                 PURE;
	};

	interface IRowGroupEnum {
		virtual VOID             Clear  (void)                  PURE;
		virtual DWORD            Count  (void)            const PURE;
		virtual DWORD            Insert (LPCTSTR lpszName,const bool bExpanded)   PURE; // returns group identifier
		virtual IRowGroup&       Item   (const INT nIndex)      PURE;
		virtual HRESULT          Remove (const INT nIndex)      PURE;
	};

	typedef IRowGroupEnum        IRowGroups;

	interface IFlex_Grid
	{
		virtual const IColumns&  Columns(void)            const PURE;
		virtual IColumns&        Columns(void)                  PURE;
		virtual IRowGroups&      Groups(void)                   PURE;
		virtual IHeader&         Header(void)                   PURE;
		virtual HRESULT          UpdateLayout(const RECT&)      PURE;
	};

	HRESULT    CreateFlexGrid(const HWND hParent, const RECT& rcArea, IFlex_Grid*&);
	HRESULT    DestroyFlexGrid(IFlex_Grid*&);
}

#endif/*_FLEXGRIDCTRLINTERFACE_H_C5D9E411_ADEF_4b1a_A8B8_78C8546EB7BB_INCLUDED*/