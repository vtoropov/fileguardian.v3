/*
	Created by Tech_dog (VToropov) on 7-Feb-2015 at 10:33:49pm, GMT+3, Taganrog, Saturday;
	This is UIX Draw library GDI related object(s) class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 10:56:51p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

typedef Gdiplus::Bitmap TBitmap;
/////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace draw { namespace common
{
	CFont::CFont(LPCTSTR pszFamily, const DWORD dwOptions, const LONG lParam): m_handle(NULL), m_bManaged(false)
	{
		LOGFONT lgf = {0};
		if (::SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(LOGFONT), &lgf, 0))
		{
			if (pszFamily)
				::_tcscpy_s(lgf.lfFaceName, _countof(lgf.lfFaceName), pszFamily);
			if (0 != (eCreateFontOption::eExactSize & dwOptions) && lParam)
			{
				lgf.lfHeight = lParam;
			}
			else if ( 0 != (eCreateFontOption::eRelativeSize & dwOptions) && lParam)
			{
				if (lgf.lfHeight < 0)
				{
					lgf.lfHeight -= lParam;
				}
				else
				{
					lgf.lfHeight += lParam;
				}
			}
			m_handle = ::CreateFontIndirect(&lgf);
			m_bManaged = true;
		}
		else
		{
			m_handle = reinterpret_cast<HFONT>(::GetStockObject(DEFAULT_GUI_FONT));
		}
	}

	CFont::~CFont(void)
	{
		if (m_bManaged)
		{
			::DeleteObject((HGDIOBJ)m_handle);
			m_handle = NULL;
			m_bManaged = false;
		}
	}

	HFONT CFont::Detach(void)
	{
		HFONT hFont = m_handle;
		{
			m_handle = NULL;
			m_bManaged = false;
		}
		return hFont;
	}

	HFONT CFont::GetHandle(void) const { return m_handle; }
}}}

/////////////////////////////////////////////////////////////////////////////

CColour::CColour(void) : m_clr(0, 0, 0, 0) { }
CColour::CColour(const COLORREF clr, const BYTE alpha): m_clr(alpha, GetRValue(clr), GetGValue(clr), GetBValue(clr)) { }
CColour::~CColour(void) { }

/////////////////////////////////////////////////////////////////////////////

VOID     CColour::SetColorFromRGBA(const COLORREF clr, const BYTE _alpha)
{
	m_clr  = Gdiplus::Color::MakeARGB(_alpha, (BYTE)GetRValue(clr), (BYTE)GetGValue(clr), (BYTE)GetBValue(clr));
}

/////////////////////////////////////////////////////////////////////////////

CColour::operator const Gdiplus::Color&(void)const { return m_clr;  }
CColour::operator const Gdiplus::Color*(void)const { return &m_clr; }
CColour::operator const COLORREF(void)const { return m_clr.ToCOLORREF(); }

/////////////////////////////////////////////////////////////////////////////

CFontScalable::CFontScalable(const HDC hDC, LPCTSTR lpszFontFamily, const INT nSize, const DWORD dwOptions) : m_handle(NULL)
{
	if (!hDC)
		return;
	const INT nVertScale = (::GetDeviceCaps(hDC, LOGPIXELSY) * nSize) / 72;
	const INT nHeight = -nVertScale;

	m_handle = ::CreateFont(
			nHeight,
			0,
			0,
			0,
			0 != (eCreateFontOption::eBold      & dwOptions) ? FW_BOLD : FW_NORMAL,
			0 != (eCreateFontOption::eItalic    & dwOptions),
			0 != (eCreateFontOption::eUnderline & dwOptions),
			0,
			DEFAULT_CHARSET    ,
			OUT_DEVICE_PRECIS  ,
			CLIP_DEFAULT_PRECIS,
			DEFAULT_QUALITY    ,
			DEFAULT_PITCH      ,
			lpszFontFamily
		);
}

CFontScalable::~CFontScalable(void)
{
	if (m_handle)
	{
		::DeleteObject((HGDIOBJ)m_handle);
		m_handle = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

HFONT     CFontScalable::Detach(void)
{
	HFONT hFont = m_handle;
	{
		m_handle = NULL;
	}
	return hFont;
}

HFONT     CFontScalable::GetHandle(void) const { return m_handle; }
bool      CFontScalable::IsValid  (void) const { return (m_handle && (OBJ_FONT == ::GetObjectType((HGDIOBJ)m_handle))); }

/////////////////////////////////////////////////////////////////////////////

CFontScalable::operator HFONT(void)const { return m_handle; }

/////////////////////////////////////////////////////////////////////////////

CText::CText(const HDC hDC, const HFONT hFont):
	m_hdc(hDC),
	m_fnt(hFont),
	m_bInitialized(false)
{
	switch(::GetObjectType((HGDIOBJ) m_hdc))
	{
	case OBJ_DC:
	case OBJ_MEMDC:
		{
			m_bInitialized = (OBJ_FONT==::GetObjectType((HGDIOBJ) m_fnt));
		}break;
	}	
	ATLASSERT(m_bInitialized);
}

CText::~CText(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT   CText::GetSize(LPCTSTR lpText, SIZE& szText)
{
	if (!this->IsValid())
		return OLE_E_BLANK;

	HRESULT hr_ = S_OK;

	if (!lpText || (::_tcslen(lpText) < 1))
		return (hr_ = E_INVALIDARG);

	HGDIOBJ hobj_ = ::SelectObject(m_hdc, (HGDIOBJ)m_fnt);

	const BOOL bResult = ::GetTextExtentPoint32(m_hdc, lpText, (int)::_tcslen(lpText), &szText);
	if (!bResult)
		hr_ =  HRESULT_FROM_WIN32(::GetLastError());

	if (hobj_)
		::SelectObject(m_hdc, hobj_);

	return hr_;
}

bool      CText::IsValid(void)const { return m_bInitialized; }

/////////////////////////////////////////////////////////////////////////////

CBitmapInfo::CBitmapInfo(void) : m_UID(0), m_hBitmap(NULL), m_hResult(OLE_E_BLANK) { this->Detach(); }
CBitmapInfo::CBitmapInfo(const HBITMAP hBitmap, const UINT UID) : m_UID(UID) { this->AttachTo(hBitmap); }
CBitmapInfo::~CBitmapInfo(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT      CBitmapInfo::AttachTo(const HBITMAP hBitmap)
{
	this->Detach();
	m_hBitmap = hBitmap;
	if (NULL == hBitmap)
	{
		m_hResult = E_INVALIDARG;
	}
	else if (OBJ_BITMAP != ::GetObjectType((HGDIOBJ)hBitmap))
	{
		m_hResult = DISP_E_TYPEMISMATCH;
	}
	else if (!::GetObject(hBitmap, sizeof(BITMAP), (LPVOID)(BITMAP*)this))
	{
		m_hResult = HRESULT_FROM_WIN32(::GetLastError());
	}
	else
		m_hResult = S_OK;
	if (S_OK == m_hResult && NULL == _inherited::bmBits)
	{
		m_hResult = HRESULT_FROM_WIN32(ERROR_ACCESS_DENIED);
	}
	return m_hResult;
}

HBITMAP      CBitmapInfo::Detach(void)
{
	HBITMAP hTmp = m_hBitmap;
	m_hBitmap   = NULL;
	m_hResult   = OLE_E_BLANK;
	m_UID       = 0;
	::memset((void*)static_cast<BITMAP*>(this), 0, sizeof(BITMAP));
	return  hTmp;
}

UINT         CBitmapInfo::GetID(void) const { return m_UID; }
HRESULT      CBitmapInfo::GetLastResult(void) const { return m_hResult; }

HRESULT      CBitmapInfo::GetSize(SIZE& size) const
{
	::memset((void*)&size, 0, sizeof(SIZE));

	if (S_OK != m_hResult)
		return  m_hResult;

	size.cx = ((BITMAP&)(*this)).bmWidth;
	size.cy = ((BITMAP&)(*this)).bmHeight;
	return m_hResult;
}

/////////////////////////////////////////////////////////////////////////////

CGdiplusBitmapWrap::CGdiplusBitmapWrap(const HBITMAP hBitmap):
	m_pBitmap(NULL),
	m_bManaged(true)
{
	CBitmapInfo info_(hBitmap);
	if (S_OK != info_.GetLastResult())
		return;

	SIZE sz_ = {0};
	info_.GetSize(sz_);
	try
	{
		m_pBitmap = new Gdiplus::Bitmap(sz_.cx, sz_.cy, PixelFormat32bppARGB);
		Gdiplus::BitmapData cBits;
		Gdiplus::Rect   rc_(0, 0, sz_.cx, sz_.cy);
		Gdiplus::Status st_ = (*m_pBitmap).LockBits(&rc_, Gdiplus::ImageLockModeWrite, PixelFormat32bppARGB, &cBits);
		if (Gdiplus::Ok != st_)
		{
			delete m_pBitmap; m_pBitmap = NULL;
		}
		else
		{
			LONG chunk_  = sz_.cx * info_.bmBitsPixel/8;
			PBYTE pSrc   = (PBYTE)info_.bmBits;
			PBYTE pDest  = (PBYTE)cBits.Scan0;
			for (LONG y_ = 0; y_ < sz_.cy; y_++)
			{
				::memcpy((PVOID)(pDest + (y_ * chunk_)), pSrc + (sz_.cy - y_ - 1) * chunk_, chunk_);
			}
			(*m_pBitmap).UnlockBits(&cBits);
		}
	}
	catch(std::bad_alloc&)
	{
		ATLASSERT(FALSE);
	}
}

CGdiplusBitmapWrap::CGdiplusBitmapWrap(Gdiplus::Bitmap* pBitmap, const bool bManaged): m_pBitmap(pBitmap), m_bManaged(bManaged) {}
CGdiplusBitmapWrap::~CGdiplusBitmapWrap(void) { this->Reset(); }

/////////////////////////////////////////////////////////////////////////////

HRESULT      CGdiplusBitmapWrap::Clip(const RECT& rc_dst, Gdiplus::Bitmap*& _out_ptr)
{
	if (!m_pBitmap)
		return OLE_E_BLANK;

	if (::IsRectEmpty(&rc_dst))
		return E_INVALIDARG;

	const RECT rc_src = {0, 0, static_cast<LONG>(m_pBitmap->GetWidth()), static_cast<LONG>(m_pBitmap->GetHeight())};
	RECT rc_clip   = {0};
	if (!::IntersectRect(&rc_clip, &rc_src, &rc_dst))
		return HRESULT_FROM_WIN32(::GetLastError());

	if (::IsRectEmpty(&rc_clip))
		return S_OK;

	try
	{
		const LONG l_dst_py = (rc_clip.top > rc_dst.top ? (rc_clip.top - rc_dst.top) : 0);   // destination y margin in pixels
		const LONG l_src_py = (rc_clip.top > rc_src.top ? (rc_clip.top - rc_src.top) : 0);   // source y margin in pixels

		const LONG l_dst_px = (rc_clip.left > rc_dst.left ? rc_clip.left - rc_dst.left : 0); // destination x margin in pixels
		const LONG l_src_px = (rc_clip.left > rc_src.left ? rc_clip.left - rc_src.left : 0); // source x margin in pixels

		Gdiplus::Bitmap* pTarget = new Gdiplus::Bitmap(__W(rc_dst), __H(rc_dst), PixelFormat32bppARGB);
		Gdiplus::BitmapData cDstData;
		{
			Gdiplus::Rect rc_lock(l_dst_px, l_dst_py, __W(rc_clip), __H(rc_clip));
			Gdiplus::Status st_ = ((*pTarget).LockBits(&rc_lock, Gdiplus::ImageLockModeWrite, PixelFormat32bppARGB, &cDstData));
			if (Gdiplus::Ok != st_)
			{
				delete pTarget; pTarget = NULL;
				return GdiplusStatusToHresult(st_);
			}
		}
		Gdiplus::BitmapData cSrcData;
		{
			Gdiplus::Rect rc_lock(l_src_px, l_src_py, __W(rc_clip), __H(rc_clip));
			Gdiplus::Status st_ = ((*m_pBitmap).LockBits(&rc_lock, Gdiplus::ImageLockModeRead, PixelFormat32bppARGB, &cSrcData));
			if (Gdiplus::Ok != st_)
				return GdiplusStatusToHresult(st_);
		}

		PBYTE p_dst  = (PBYTE)cDstData.Scan0;
		PBYTE p_src  = (PBYTE)cSrcData.Scan0;

		const LONG l_chunk  = (__W(rc_clip) * 4); // the length in bytes of one clipped row
        const LONG l_iters  = (__H(rc_clip));

		for (LONG it_ = 0; it_ < l_iters; it_++)
		{
			::memcpy((PVOID)p_dst, p_src, l_chunk);
			p_dst += cDstData.Stride;
			p_src += cSrcData.Stride;
		}
		(*pTarget).UnlockBits(&cDstData);
		(*m_pBitmap).UnlockBits(&cSrcData);
		_out_ptr = pTarget;
	}
	catch(::std::bad_alloc&)
	{
		ATLASSERT(0);
		return E_OUTOFMEMORY;
	}
	return S_OK;
}

TBitmap*     CGdiplusBitmapWrap::Detach(void)
{
	Gdiplus::Bitmap* pReturnObject = m_pBitmap;
	m_pBitmap = NULL;
	m_bManaged = false;
	return pReturnObject;
}

HRESULT      CGdiplusBitmapWrap::Reset(void)
{
	if (m_pBitmap && m_bManaged)
	{
		try
		{
			delete m_pBitmap;
		}
		catch(...)
		{
			ATLASSERT(0);
			return E_OUTOFMEMORY;
		}
	}
	m_pBitmap = NULL;
	m_bManaged = false;
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace draw { namespace common { namespace details
{
	static HRESULT ImageCache_CreateItem(const SIZE& sz_, Gdiplus::Bitmap*& ptr_ref)
	{
		if (ptr_ref)
			return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
		try
		{
			ptr_ref = new Gdiplus::Bitmap(sz_.cx, sz_.cy, PixelFormat32bppARGB);
		}
		catch (::std::bad_alloc&){ ATLASSERT(0); return E_OUTOFMEMORY; }
		return S_OK;
	}

	static HRESULT ImageCache_DestroyItem(Gdiplus::Bitmap*& ptr_ref)
	{
		if (!ptr_ref)
			return S_FALSE;
		try
		{
			delete ptr_ref; ptr_ref = NULL;
		}
		catch(...){ return E_OUTOFMEMORY; }
		return S_OK;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CImageCache::CImageCache(void) { }
CImageCache::~CImageCache(void) { this->Clear(); }

/////////////////////////////////////////////////////////////////////////////

HRESULT      CImageCache::Add(const ULONG uKey, Gdiplus::Bitmap* pb)
{
	if (this->Has(uKey))
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	try
	{
		m_cache.insert(std::make_pair(uKey, pb));
	}
	catch(::std::bad_alloc&){ ATLASSERT(0); return E_OUTOFMEMORY; }
	return S_OK;
}

HRESULT      CImageCache::Add(const ULONG uKey, const SIZE& sz_, Gdiplus::Bitmap** ppb)
{
	TImageCache::const_iterator cit_ = m_cache.find(uKey);
	if (cit_ != m_cache.end())
	{
		if (ppb)
			*ppb = cit_->second;
		return S_OK;
	}
	Gdiplus::Bitmap* pb_ = NULL;
	HRESULT hr_ = details::ImageCache_CreateItem(sz_, pb_);
	if (S_OK != hr_)
		return  hr_;
	if ( ppb )
		*ppb  = pb_;
	try
	{
		m_cache.insert(std::make_pair(uKey, pb_));
	}
	catch(::std::bad_alloc&){ ATLASSERT(0); details::ImageCache_DestroyItem(pb_); return E_OUTOFMEMORY; }
	return hr_;
}

HRESULT      CImageCache::Clear(void)
{
	if (m_cache.empty())
		return S_FALSE;
	for (TImageCache::const_iterator it_ = m_cache.begin(); it_ != m_cache.end(); ++it_)
	{
		Gdiplus::Bitmap* pb_ = it_->second;
		details::ImageCache_DestroyItem(pb_);
	}
	m_cache.clear();
	return S_OK;
}

INT          CImageCache::Count(void) const
{
	return (INT)m_cache.size();
}

TBitmap*     CImageCache::Get(const ULONG uKey) const
{
	Gdiplus::Bitmap* pb_ = NULL;
	TImageCache::const_iterator it_ = m_cache.find(uKey);
	if (it_ != m_cache.end())
	{
		pb_  = it_->second;
	}
	return pb_;
}

bool         CImageCache::Has(const ULONG uKey) const
{
	TImageCache::const_iterator it_ = m_cache.find(uKey);
	return (it_ != m_cache.end());
}

HRESULT      CImageCache::Remove(const ULONG uKey)
{
	TImageCache::iterator it_ = m_cache.find(uKey);
	if (it_ != m_cache.end())
	{
		Gdiplus::Bitmap* pb_ = it_->second;
		details::ImageCache_DestroyItem(pb_);
		m_cache.erase(it_);
		return S_OK;
	}
	else
		return TYPE_E_ELEMENTNOTFOUND;
}