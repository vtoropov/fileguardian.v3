/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Jul-2018 at 3:21:50p, UTC+7, Phuket, Rawai, Monday;
	This is UIX Draw library generic shape interface implementation file.
*/
#include "StdAfx.h"
#include "UIX_GdiDrawShape.h"

using namespace ex_ui::draw;
using namespace ex_ui::draw::shape;

/////////////////////////////////////////////////////////////////////////////

CRectEx::CRectEx(const RECT& rc_) : m_rc((INT)rc_.left, (INT)rc_.top, (INT)__W(rc_), (INT)__H(rc_)) { }
CRectEx::CRectEx(const INT left_, const INT top_, const INT width_, const INT height_):
	m_rc((INT)left_, (INT)top_, (INT)width_, (INT)height_) {
}

CRectEx::~CRectEx(void) { }

/////////////////////////////////////////////////////////////////////////////

CRectEx::operator Gdiplus::Rect*(void) { return &m_rc; }
CRectEx::operator Gdiplus::Rect&(void) { return  m_rc; }

/////////////////////////////////////////////////////////////////////////////

CRectFEx::CRectFEx(const RECT& rc_) :
	m_rc((Gdiplus::REAL)rc_.left, (Gdiplus::REAL)rc_.top, (Gdiplus::REAL)__W(rc_), (Gdiplus::REAL)__H(rc_)) {
}

CRectFEx::CRectFEx(const INT left_, const INT top_, const INT width_, const INT height_):
	m_rc((Gdiplus::REAL)left_, (Gdiplus::REAL)top_, (Gdiplus::REAL)width_, (Gdiplus::REAL)height_) {
}

CRectFEx::~CRectFEx(void) { }

/////////////////////////////////////////////////////////////////////////////

CRectFEx::operator Gdiplus::RectF*(void) { return &m_rc; }
CRectFEx::operator Gdiplus::RectF&(void) { return  m_rc; }