/*
	Created by Tech_dog (VToropov) on 7-Aug-2015 at 3:23:38pm, GMT+7, Phuket, Rawai, Friday;
	This is UIX library GDI+ image related class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 11:14:08p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "UIX_ImgWrap.h"

using namespace ex_ui::draw::common;

#include "generic.stg.data.h"
using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace draw { namespace common { namespace details
{
	static LPCTSTR __MIME_TYPE_PNG = _T("image/png");
	static LPCTSTR __MIME_TYPE_JPG = _T("image/jpg");
	static LPCTSTR __MIME_TYPE_BMP = _T("image/bmp");

	static LPCTSTR CImageEncoderEnum_GetImageMimeAsString(const CImageEncoderMime::_e eType)
	{
		switch (eType)
		{
		case CImageEncoderMime::eBmp:   return __MIME_TYPE_BMP;
		case CImageEncoderMime::eJpg:   return __MIME_TYPE_JPG;
		case CImageEncoderMime::ePng:   return __MIME_TYPE_PNG;
		default:
			ATLASSERT(FALSE);
		}
		return __MIME_TYPE_PNG;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CImageEncoderEnum::CImageEncoderEnum(void)
{
}

CImageEncoderEnum::~CImageEncoderEnum(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CImageEncoderEnum::GetEncoderClsid(const CImageEncoderMime::_e eMimeType, CLSID& __in_out_ref)
{
	UINT cnt_ = NULL;
	UINT size_ = NULL;
	Gdiplus::Status st_ = Gdiplus::GetImageEncodersSize(&cnt_, &size_);
	HRESULT hr_ = (Gdiplus::Ok == st_ ? S_OK : E_FAIL);
	if (S_OK != hr_)
		return  hr_;
	CRawData raw_encoders(static_cast<DWORD>(size_));
	hr_ = raw_encoders.Error();
	if (S_OK != hr_)
		return  hr_;
	Gdiplus::ImageCodecInfo* info_ = reinterpret_cast<Gdiplus::ImageCodecInfo*>(raw_encoders.GetData());
	st_ = Gdiplus::GetImageEncoders(cnt_, size_, info_);
	hr_ =(Gdiplus::Ok == st_ ? S_OK : E_FAIL);
	if (S_OK != hr_)
		return  hr_;
	{
		for (UINT i_ = 0; i_ < cnt_; i_++)
		{
			if (NULL == ::_tcscmp(info_[i_].MimeType, details::CImageEncoderEnum_GetImageMimeAsString(eMimeType)))
			{
				::memcpy((void*)&__in_out_ref, (void*)&(info_[i_].Clsid), sizeof(CLSID));
				return S_OK;
			}
		}
	}
	return  S_FALSE;
}