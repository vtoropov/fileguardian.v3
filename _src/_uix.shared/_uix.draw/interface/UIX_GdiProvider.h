#ifndef __PLATINUMSHAREDGDIPROVIDER_H_C659CA0D_5756_44c3_A5DC_2BFD16EFA8D7_INCLUDED
#define __PLATINUMSHAREDGDIPROVIDER_H_C659CA0D_5756_44c3_A5DC_2BFD16EFA8D7_INCLUDED
/*
	Created by Tech_dog (VToropov) on 23-Mar-2014 at 8:45:09pm, GMT+4, Saint-Petersburg, Sunday;
	This is UIX draw library GDI Provider class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 11:07:04p, UTC+7, Phuket, Rawai, Monday;
*/
#include <gdiplus.h>
#include <atlapp.h>
#include <atlgdi.h>

#include "UIX_GdiDrawDefs.h"
#include "UIX_GdiDrawShape.h"
#include "UIX_GdiObject.h"

#pragma comment(lib, "gdiplus.lib")

namespace ex_ui { namespace draw
{
	HRESULT   GdiplusStatusToHresult(const Gdiplus::Status);

	using ex_ui::draw::common::eAlphaValue;
	using ex_ui::draw::common::CColour;

	using ex_ui::draw::shape::CRectEdge;

	class CZBuffer : public ::WTL::CDC
	{
		typedef ::WTL::CDC   TBaseDC;
	private:
		HDC            m_hOrigin;
		RECT           m_rcPaint;
		CBitmap        m_surface;
		HBITMAP        m_hBmpOld;
	public:
		CZBuffer(void);
		CZBuffer(const HDC hDC, const RECT& rcPaint);
		~CZBuffer(VOID);
	public:
		HRESULT        CopyTo(HBITMAP& hBitmap);
		HRESULT        CopyTo(
		                    CONST HDC hCompatibleDC,
		                    const INT _x,
		                    const INT _y,
		                    const BYTE _alpha = eAlphaValue::eOpaque
		               );
		HRESULT        Create(const HDC hDC, const RECT& rcPaint);
		VOID           DrawGragRect(
		                    const RECT& rcDraw,
		                    const COLORREF clrFrom,
		                    const COLORREF clrUpto,
		                    const bool bVertical,
		                    const BYTE ba
		               ) CONST;
		VOID           DrawLine(
		                    const INT _x0,
		                    const INT _y0,
		                    const INT _x1,
		                    const INT _y1,
		                    const COLORREF _clr,
		                    const INT nThickness = 1
		               );
		VOID           DrawRectangle(
		                    const RECT& rcDraw,
		                    const COLORREF clrBorder,
		                    const INT nThickness = 1,
							const DWORD dEdges = CRectEdge::eAll
		               );
		VOID           DrawSolidRect(
		                    const RECT& rcDraw,
		                    const CColour&
		               ) CONST;
		VOID           DrawSolidRect(
		                    const RECT& rcDraw,
		                    const COLORREF clrFill,
		                    const BYTE _alpha = eAlphaValue::eOpaque
		               ) CONST;
		VOID           DrawTextExt(
		                    LPCTSTR pszText,
		                    const HFONT fnt_,
		                    const RECT& rcDraw,
		                    const COLORREF clrFore,
		                    const DWORD fmt_
		               );
		VOID           DrawTextExt(
		                    LPCTSTR pszText,
		                    LPCTSTR pszFontFamily,
		                    const DWORD dwFontSize,
		                    const RECT& rcDraw,
		                    const COLORREF clrFore,
		                    const DWORD dwFormat
		               );
		bool           IsValid(void)const;
		VOID           Reset(void);                // copies the buffer content to original device and resets the buffer to uninitialized state
	public:
		operator HDC(void) const { return TBaseDC::m_hDC; }
	private:
		CZBuffer(const CZBuffer&);
		CZBuffer& operator= (const CZBuffer&);
	};

	class CGdiPlusLibLoader
	{
	private:
		ULONG_PTR   m_gdiPlusToken;
	public:
		CGdiPlusLibLoader(void);
		~CGdiPlusLibLoader(void);
	private:
		CGdiPlusLibLoader(const CGdiPlusLibLoader&);
		CGdiPlusLibLoader& operator= (const CGdiPlusLibLoader&);
	};

	class CGdiPlusPngLoader
	{
	public:
		static HRESULT     CreateImages(const ATL::_U_STRINGorID RID, const HMODULE hModule, const INT nImageExpect, HIMAGELIST& hImageList);
		static HRESULT     LoadResource(const ATL::_U_STRINGorID RID, const HMODULE hModule, Gdiplus::Bitmap*&);
		static HRESULT     LoadResource(const ATL::_U_STRINGorID RID, const HMODULE hModule, HBITMAP&);
	};
}}

#endif/*__PLATINUMSHAREDGDIPROVIDER_H_C659CA0D_5756_44c3_A5DC_2BFD16EFA8D7_INCLUDED*/