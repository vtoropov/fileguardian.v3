#ifndef _UIXFRAMELIBRARYPRECOMPILEDHEADER_H_85F87D30_AC8C_4495_9377_51EE3FBBBF2E_INCLUDED
#define _UIXFRAMELIBRARYPRECOMPILEDHEADER_H_85F87D30_AC8C_4495_9377_51EE3FBBBF2E_INCLUDED
/*
	Created by Tech_dog(VToropov) on 9-Feb-2015 at 8:18:04pm, GMT+3, Taganrog, Monday;
	This is UIX Frame library precompiled headers definition file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 11:47:16p, UTC+7, Phuket, Rawai, Monday;
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe
#pragma warning(disable: 4458)  // declaration of 'abcd' hides class member (GDI+)

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <atlapp.h>
#include <atlgdi.h>
#include <atlctrls.h>
#include <atldlgs.h>
#include <comdef.h>
#include <comutil.h>
#include <vector>
#include <map>


#endif/*_UIXFRAMELIBRARYPRECOMPILEDHEADER_H_85F87D30_AC8C_4495_9377_51EE3FBBBF2E_INCLUDED*/