#ifndef _UIXLISTVIEWEX_H_9B9CB45D_9EDD_4be7_AD2E_2971B0DD80E4_INCLUDED
#define _UIXLISTVIEWEX_H_9B9CB45D_9EDD_4be7_AD2E_2971B0DD80E4_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Mar-2018 at 10:17:04a, UTC+7, Phuket, Rawai, Tuesday;
	This is UIX Control library extended standard list view control interface declaration file.
*/
#include "UIX_ListView_Ex_Group.h"
#include "UIX_ListView_Ex_Footer.h"

#define CListViewCol_DefMask (LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM)

namespace ex_ui { namespace controls {

	interface IListView_Ex_DataSource {

		virtual HRESULT IListView_OnCacheHint( NMLVCACHEHINT* _pCacheHint) PURE;
		virtual HRESULT IListView_OnDispQuery( NMLVDISPINFO* _pQueryInfo ) PURE;
		virtual HRESULT IListView_OnFindItem ( LPNMLVFINDITEM _pFindItem ) PURE;
	};

	class CListViewCol_Ex {

	private:
		CAtlString    m_title;
		LVCOLUMN      m_column;
	public:
		CListViewCol_Ex(void);
		CListViewCol_Ex(const CListViewCol_Ex&);
		CListViewCol_Ex(const UINT _format, const UINT _width, const UINT _index, LPCTSTR _title, const UINT _mask = CListViewCol_DefMask);
		~CListViewCol_Ex(void);
	public:
		const LVCOLUMN& ColumnData(void)const;
	public:
		operator const LVCOLUMN&(void) const;
		operator LVCOLUMN&(void);
		operator CAtlString&(void);
	public:
		CListViewCol_Ex& operator= (const CListViewCol_Ex&);
	};

	typedef ::std::vector<CListViewCol_Ex> TListViewCols_Ex;

	//
	// TODO: Unfortunately, group feature does not work in 'report' style mode of a list view control;
	//       it is confirmed by building original sample from:
	//       https://www.codeproject.com/Articles/35197/Undocumented-List-View-Features
	//
	class CListViewCtrl_Ex {

	private:
		UINT            m_ctrlId;
		WTL::CListViewCtrl
		                m_control;
		CListViewGroups m_groups;
		CListViewFooter m_footer;

	private:
		IListView_Ex_DataSource& m_DataSource;
	public:
		CListViewCtrl_Ex(IListView_Ex_DataSource&, IListView_Ex_Footer&);
		CListViewCtrl_Ex(IListView_Ex_DataSource&, IListView_Ex_Footer&, HWND _list, const UINT nLvwId);
		~CListViewCtrl_Ex(void);
	public:
		HRESULT       Attach  (const HWND hLvCtrl, const UINT nLvwId);
		const
		CListViewCtrl&Control (void) const;
		CListViewCtrl&Control (void);
		HRESULT       Create  (const HWND hDialog, const UINT nLvwId, const RECT& rcArea);
		HRESULT       Destroy (void);
		const
		CListViewFooter& Footer(void)const;
		CListViewFooter& Footer(void);
		const
		CListViewGroups& Groups(void)const;
		CListViewGroups& Groups(void);
		HRESULT       Insert  (const TListViewCols_Ex&);               // inserts list view column set;
		bool          IsValid (void) const;
		HRESULT       Redraw  (void) const;
		HRESULT       Replace (const HWND hDialog, const UINT nLvwId); // replaces/destroys standard list view control in dialog panel window provided;
	public:
		LRESULT       OnMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	public:
		operator HWND(void)const;
	private:
		CListViewCtrl_Ex(const CListViewCtrl_Ex&);
		CListViewCtrl_Ex& operator= (const CListViewCtrl_Ex&);
	};
}}

#endif/*_UIXLISTVIEWEX_H_9B9CB45D_9EDD_4be7_AD2E_2971B0DD80E4_INCLUDED*/