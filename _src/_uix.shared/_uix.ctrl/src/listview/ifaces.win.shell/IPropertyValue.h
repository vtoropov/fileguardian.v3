#ifndef _IPROPERTYVALUE_H_B44D39E0_1463_4a7d_868F_6B43B4C7DA42_INCLUDED
#define _IPROPERTYVALUE_H_B44D39E0_1463_4a7d_868F_6B43B4C7DA42_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Mar-2018 at 10:47:07a, UTC+7, Phuket, Rawai, Sunday;
	This is Windows Shell ListView control common property value interface declaration file;
	-----------------------------------------------------------------------------

	This code is borrowed(and adopted) from the project 'Undocumented List View Features' of Timo Kunze;
	https://www.codeproject.com/Articles/35197/Undocumented-List-View-Features?display=Print

	Initially, the research has been made by Geoff Chappell;
	http://www.geoffchappell.com/index.htm
*/

namespace ex_ui { namespace controls { namespace lvw
{
	// the interface's GUID
	const IID IID_IPropertyValue = {0x7AF7F355, 0x1066, 0x4E17, {0xB1, 0xF2, 0x19, 0xFE, 0x2F, 0x09, 0x9C, 0xD2}};


	class IPropertyValue :
    		public IUnknown
	{
	public:
		/// \brief <em>Sets the \c PROPERTYKEY structure that identifies the property wrapped by the object</em>
		///
		/// \param[in] propKey The property identifier. It will be stored by the object.
		///
		/// \return An \c HRESULT error code.
		///
		/// \sa GetPropertyKey,
		///     <a href="http://msdn.microsoft.com/en-us/library/bb773381.aspx">PROPERTYKEY</a>
		virtual HRESULT STDMETHODCALLTYPE SetPropertyKey(PROPERTYKEY const& propKey) PURE;

		/// \brief <em>Retrieves the \c PROPERTYKEY structure that identifies the property wrapped by the object</em>
		///
		/// \param[out] pPropKey Receives the property identifier.
		///
		/// \return An \c HRESULT error code.
		///
		/// \sa SetPropertyKey,
		///     <a href="http://msdn.microsoft.com/en-us/library/bb773381.aspx">PROPERTYKEY</a>
		virtual HRESULT STDMETHODCALLTYPE GetPropertyKey(PROPERTYKEY* pPropKey) PURE;

		/// \brief <em>Retrieves the current value of the property wrapped by the object</em>
		///
		/// \param[out] pPropValue Receives the property value.
		///
		/// \return An \c HRESULT error code.
		///
		/// \sa InitValue
		virtual HRESULT STDMETHODCALLTYPE GetValue(PROPVARIANT* pPropValue) PURE;

		/// \brief <em>Initializes the object with the property's current value</em>
		///
		/// \param[in] propValue The property's current value. It will be stored by the object.
		///
		/// \return An \c HRESULT error code.
		///
		/// \sa GetValue
		virtual HRESULT STDMETHODCALLTYPE InitValue(PROPVARIANT const& propValue) PURE;
	};
}}}

#endif/*_IPROPERTYVALUE_H_B44D39E0_1463_4a7d_868F_6B43B4C7DA42_INCLUDED*/