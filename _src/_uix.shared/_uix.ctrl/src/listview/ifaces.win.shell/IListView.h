#ifndef _ILISTVIEW_H_B8AB7FDD_9E83_472b_AC70_A3DDAEB1A5F6_INCLUDED
#define _ILISTVIEW_H_B8AB7FDD_9E83_472b_AC70_A3DDAEB1A5F6_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Mar-2018 at 10:53:58a, UTC+7, Bangkok, Suvarnabh, Monday;
	This is Windows Shell ListView control main interface declaration file;
	-----------------------------------------------------------------------------

	This code is borrowed(and adopted) from the project 'Undocumented List View Features' of Timo Kunze;
	https://www.codeproject.com/Articles/35197/Undocumented-List-View-Features?display=Print

	Initially, the research has been made by Geoff Chappell;
	http://www.geoffchappell.com/index.htm
*/
#include "IOwnerDataCallback.h"

namespace ex_ui { namespace controls { namespace lvw
{
	// the interface's GUID
	const IID IID_IListView_WinVista = {0x2FFE2979, 0x5928, 0x4386, {0x9C, 0xDB, 0x8E, 0x1F, 0x15, 0xB7, 0x2F, 0xB4}};
	const IID IID_IListView_Win7 = {0xE5B16AF2, 0x3990, 0x4681, {0xA6, 0x09, 0x1F, 0x06, 0x0C, 0xD1, 0x42, 0x69}};


	class IListView_WinVista :
		public IOleWindow
	{
	public:
		virtual HRESULT STDMETHODCALLTYPE GetImageList(int imageList, HIMAGELIST* pHImageList) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetImageList(int imageList, HIMAGELIST hNewImageList, HIMAGELIST* pHOldImageList) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetBackgroundColor(COLORREF* pColor) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetBackgroundColor(COLORREF color) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetTextColor(COLORREF* pColor) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetTextColor(COLORREF color) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetTextBackgroundColor(COLORREF* pColor) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetTextBackgroundColor(COLORREF color) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetHotLightColor(COLORREF* pColor) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetHotLightColor(COLORREF color) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetItemCount(PINT pItemCount) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetItemCount(int itemCount, DWORD flags) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetItem(LVITEMW* pItem) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetItem(LVITEMW* const pItem) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetItemState(int itemIndex, int subItemIndex, ULONG mask, ULONG* pState) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetItemState(int itemIndex, int subItemIndex, ULONG mask, ULONG state) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetItemText(int itemIndex, int subItemIndex, LPWSTR pBuffer, int bufferSize) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetItemText(int itemIndex, int subItemIndex, LPCWSTR pText) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetBackgroundImage(LVBKIMAGEW* pBkImage) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetBackgroundImage(LVBKIMAGEW* const pBkImage) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetFocusedColumn(PINT pColumnIndex) PURE;

		// parameters may be in wrong order
		virtual HRESULT STDMETHODCALLTYPE SetSelectionFlags(ULONG mask, ULONG flags) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetSelectedColumn(PINT pColumnIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetSelectedColumn(int columnIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetView(DWORD* pView) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetView(DWORD view) PURE;
		virtual HRESULT STDMETHODCALLTYPE InsertItem(LVITEMW* const pItem, PINT pItemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE DeleteItem(int itemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE DeleteAllItems(void) PURE;
		virtual HRESULT STDMETHODCALLTYPE UpdateItem(int itemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetItemRect(LVITEMINDEX itemIndex, int rectangleType, LPRECT pRectangle) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetSubItemRect(LVITEMINDEX itemIndex, int subItemIndex, int rectangleType, LPRECT pRectangle) PURE;
		virtual HRESULT STDMETHODCALLTYPE HitTestSubItem(LVHITTESTINFO* pHitTestData) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetIncrSearchString(PWSTR pBuffer, int bufferSize, PINT pCopiedChars) PURE;

		// pHorizontalSpacing and pVerticalSpacing may be in wrong order
		virtual HRESULT STDMETHODCALLTYPE GetItemSpacing(BOOL smallIconView, PINT pHorizontalSpacing, PINT pVerticalSpacing) PURE;

		// parameters may be in wrong order
		virtual HRESULT STDMETHODCALLTYPE SetIconSpacing(int horizontalSpacing, int verticalSpacing, PINT pHorizontalSpacing, PINT pVerticalSpacing) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetNextItem(LVITEMINDEX itemIndex, ULONG flags, LVITEMINDEX* pNextItemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE FindItem(LVITEMINDEX startItemIndex, LVFINDINFOW const* pFindInfo, LVITEMINDEX* pFoundItemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetSelectionMark(LVITEMINDEX* pSelectionMark) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetSelectionMark(LVITEMINDEX newSelectionMark, LVITEMINDEX* pOldSelectionMark) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetItemPosition(LVITEMINDEX itemIndex, POINT* pPosition) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetItemPosition(int itemIndex, POINT const* pPosition) PURE;

		// parameters may be in wrong order
		virtual HRESULT STDMETHODCALLTYPE ScrollView(int horizontalScrollDistance, int verticalScrollDistance) PURE;
		virtual HRESULT STDMETHODCALLTYPE EnsureItemVisible(LVITEMINDEX itemIndex, BOOL partialOk) PURE;
		virtual HRESULT STDMETHODCALLTYPE EnsureSubItemVisible(LVITEMINDEX itemIndex, int subItemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE EditSubItem(LVITEMINDEX itemIndex, int subItemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE RedrawItems(int firstItemIndex, int lastItemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE ArrangeItems(int mode) PURE;
		virtual HRESULT STDMETHODCALLTYPE RecomputeItems(int unknown) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetEditControl(HWND* pHWndEdit) PURE;

		// TODO: verify that 'initialEditText' really is used to specify the initial text
		virtual HRESULT STDMETHODCALLTYPE EditLabel(LVITEMINDEX itemIndex, LPCWSTR initialEditText, HWND* phWndEdit) PURE;
		virtual HRESULT STDMETHODCALLTYPE EditGroupLabel(int groupIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE CancelEditLabel(void) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetEditItem(LVITEMINDEX* itemIndex, PINT subItemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE HitTest(LVHITTESTINFO* pHitTestData) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetStringWidth(PCWSTR pString, PINT pWidth) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetColumn(int columnIndex, LVCOLUMNW* pColumn) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetColumn(int columnIndex, LVCOLUMNW* const pColumn) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetColumnOrderArray(int numberOfColumns, PINT pColumns) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetColumnOrderArray(int numberOfColumns, int const* pColumns) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetHeaderControl(HWND* pHWndHeader) PURE;
		virtual HRESULT STDMETHODCALLTYPE InsertColumn(int insertAt, LVCOLUMNW* const pColumn, PINT pColumnIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE DeleteColumn(int columnIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE CreateDragImage(int itemIndex, POINT const* pUpperLeft, HIMAGELIST* pHImageList) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetViewRect(RECT* pRectangle) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetClientRect(BOOL unknown, RECT* pClientRectangle) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetColumnWidth(int columnIndex, PINT pWidth) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetColumnWidth(int columnIndex, int width) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetCallbackMask(ULONG* pMask) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetCallbackMask(ULONG mask) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetTopIndex(PINT pTopIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetCountPerPage(PINT pCountPerPage) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetOrigin(POINT* pOrigin) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetSelectedCount(PINT pSelectedCount) PURE;

		// 'unknown' might specify whether to pass items' data or indexes
		virtual HRESULT STDMETHODCALLTYPE SortItems(BOOL unknown, LPARAM lParam, PFNLVCOMPARE pComparisonFunction) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetExtendedStyle(DWORD* pStyle) PURE;

		// parameters may be in wrong order
		virtual HRESULT STDMETHODCALLTYPE SetExtendedStyle(DWORD mask, DWORD style, DWORD* pOldStyle) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetHoverTime(UINT* pTime) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetHoverTime(UINT time, UINT* pOldSetting) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetToolTip(HWND* pHWndToolTip) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetToolTip(HWND hWndToolTip, HWND* pHWndOldToolTip) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetHotItem(LVITEMINDEX* pHotItem) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetHotItem(LVITEMINDEX newHotItem, LVITEMINDEX* pOldHotItem) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetHotCursor(HCURSOR* pHCursor) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetHotCursor(HCURSOR hCursor, HCURSOR* pHOldCursor) PURE;

		// parameters may be in wrong order
		virtual HRESULT STDMETHODCALLTYPE ApproximateViewRect(int itemCount, PINT pWidth, PINT pHeight) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetRangeObject(int unknown, LPVOID/*ILVRange**/ pObject) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetWorkAreas(int numberOfWorkAreas, RECT* pWorkAreas) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetWorkAreas(int numberOfWorkAreas, RECT const* pWorkAreas) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetWorkAreaCount(PINT pNumberOfWorkAreas) PURE;
		virtual HRESULT STDMETHODCALLTYPE ResetEmptyText(void) PURE;
		virtual HRESULT STDMETHODCALLTYPE EnableGroupView(BOOL enable) PURE;
		virtual HRESULT STDMETHODCALLTYPE IsGroupViewEnabled(BOOL* pIsEnabled) PURE;
		virtual HRESULT STDMETHODCALLTYPE SortGroups(PFNLVGROUPCOMPARE pComparisonFunction, PVOID lParam) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetGroupInfo(int unknown1, int unknown2, LVGROUP* pGroup) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetGroupInfo(int unknown, int groupID, LVGROUP* const pGroup) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetGroupRect(BOOL unknown, int groupID, int rectangleType, RECT* pRectangle) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetGroupState(int groupID, ULONG mask, ULONG* pState) PURE;
		virtual HRESULT STDMETHODCALLTYPE HasGroup(int groupID, BOOL* pHasGroup) PURE;
		virtual HRESULT STDMETHODCALLTYPE InsertGroup(int insertAt, LVGROUP* const pGroup, PINT pGroupID) PURE;
		virtual HRESULT STDMETHODCALLTYPE RemoveGroup(int groupID) PURE;
		virtual HRESULT STDMETHODCALLTYPE InsertGroupSorted(LVINSERTGROUPSORTED const* pGroup, PINT pGroupID) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetGroupMetrics(LVGROUPMETRICS* pMetrics) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetGroupMetrics(LVGROUPMETRICS* const pMetrics) PURE;
		virtual HRESULT STDMETHODCALLTYPE RemoveAllGroups(void) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetFocusedGroup(PINT pGroupID) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetGroupCount(PINT pCount) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetOwnerDataCallback(IOwnerDataCallback* pCallback) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetTileViewInfo(LVTILEVIEWINFO* pInfo) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetTileViewInfo(LVTILEVIEWINFO* const pInfo) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetTileInfo(LVTILEINFO* pTileInfo) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetTileInfo(LVTILEINFO* const pTileInfo) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetInsertMark(LVINSERTMARK* pInsertMarkDetails) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetInsertMark(LVINSERTMARK const* pInsertMarkDetails) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetInsertMarkRect(LPRECT pInsertMarkRectangle) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetInsertMarkColor(COLORREF* pColor) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetInsertMarkColor(COLORREF color, COLORREF* pOldColor) PURE;
		virtual HRESULT STDMETHODCALLTYPE HitTestInsertMark(POINT const* pPoint, LVINSERTMARK* pInsertMarkDetails) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetInfoTip(LVSETINFOTIP* const pInfoTip) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetOutlineColor(COLORREF* pColor) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetOutlineColor(COLORREF color, COLORREF* pOldColor) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetFrozenItem(PINT pItemIndex) PURE;

		// one parameter will be the item index; works in Icons view only
		virtual HRESULT STDMETHODCALLTYPE SetFrozenItem(int unknown1, int unknown2) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetFrozenSlot(RECT* pUnknown) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetFrozenSlot(int unknown1, POINT const* pUnknown2) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetViewMargin(RECT* pMargin) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetViewMargin(RECT const* pMargin) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetKeyboardSelected(LVITEMINDEX itemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE MapIndexToId(int itemIndex, PINT pItemID) PURE;
		virtual HRESULT STDMETHODCALLTYPE MapIdToIndex(int itemID, PINT pItemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE IsItemVisible(LVITEMINDEX itemIndex, BOOL* pVisible) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetGroupSubsetCount(PINT pNumberOfRowsDisplayed) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetGroupSubsetCount(int numberOfRowsToDisplay) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetVisibleSlotCount(PINT pCount) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetColumnMargin(RECT* pMargin) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetSubItemCallback(LPVOID/*ISubItemCallback**/ pCallback) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetVisibleItemRange(LVITEMINDEX* pFirstItem, LVITEMINDEX* pLastItem) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetTypeAheadFlags(UINT mask, UINT flags) PURE;
	};


	class IListView_Win7 :
		public IOleWindow
	{
	public:
		virtual HRESULT STDMETHODCALLTYPE GetImageList(int imageList, HIMAGELIST* pHImageList) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetImageList(int imageList, HIMAGELIST hNewImageList, HIMAGELIST* pHOldImageList) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetBackgroundColor(COLORREF* pColor) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetBackgroundColor(COLORREF color) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetTextColor(COLORREF* pColor) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetTextColor(COLORREF color) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetTextBackgroundColor(COLORREF* pColor) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetTextBackgroundColor(COLORREF color) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetHotLightColor(COLORREF* pColor) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetHotLightColor(COLORREF color) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetItemCount(PINT pItemCount) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetItemCount(int itemCount, DWORD flags) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetItem(LVITEMW* pItem) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetItem(LVITEMW* const pItem) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetItemState(int itemIndex, int subItemIndex, ULONG mask, ULONG* pState) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetItemState(int itemIndex, int subItemIndex, ULONG mask, ULONG state) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetItemText(int itemIndex, int subItemIndex, LPWSTR pBuffer, int bufferSize) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetItemText(int itemIndex, int subItemIndex, LPCWSTR pText) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetBackgroundImage(LVBKIMAGEW* pBkImage) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetBackgroundImage(LVBKIMAGEW* const pBkImage) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetFocusedColumn(PINT pColumnIndex) PURE;

		// parameters may be in wrong order
		virtual HRESULT STDMETHODCALLTYPE SetSelectionFlags(ULONG mask, ULONG flags) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetSelectedColumn(PINT pColumnIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetSelectedColumn(int columnIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetView(DWORD* pView) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetView(DWORD view) PURE;
		virtual HRESULT STDMETHODCALLTYPE InsertItem(LVITEMW* const pItem, PINT pItemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE DeleteItem(int itemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE DeleteAllItems(void) PURE;
		virtual HRESULT STDMETHODCALLTYPE UpdateItem(int itemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetItemRect(LVITEMINDEX itemIndex, int rectangleType, LPRECT pRectangle) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetSubItemRect(LVITEMINDEX itemIndex, int subItemIndex, int rectangleType, LPRECT pRectangle) PURE;
		virtual HRESULT STDMETHODCALLTYPE HitTestSubItem(LVHITTESTINFO* pHitTestData) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetIncrSearchString(PWSTR pBuffer, int bufferSize, PINT pCopiedChars) PURE;

		// pHorizontalSpacing and pVerticalSpacing may be in wrong order
		virtual HRESULT STDMETHODCALLTYPE GetItemSpacing(BOOL smallIconView, PINT pHorizontalSpacing, PINT pVerticalSpacing) PURE;

		// parameters may be in wrong order
		virtual HRESULT STDMETHODCALLTYPE SetIconSpacing(int horizontalSpacing, int verticalSpacing, PINT pHorizontalSpacing, PINT pVerticalSpacing) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetNextItem(LVITEMINDEX itemIndex, ULONG flags, LVITEMINDEX* pNextItemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE FindItem(LVITEMINDEX startItemIndex, LVFINDINFOW const* pFindInfo, LVITEMINDEX* pFoundItemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetSelectionMark(LVITEMINDEX* pSelectionMark) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetSelectionMark(LVITEMINDEX newSelectionMark, LVITEMINDEX* pOldSelectionMark) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetItemPosition(LVITEMINDEX itemIndex, POINT* pPosition) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetItemPosition(int itemIndex, POINT const* pPosition) PURE;

		// parameters may be in wrong order
		virtual HRESULT STDMETHODCALLTYPE ScrollView(int horizontalScrollDistance, int verticalScrollDistance) PURE;
		virtual HRESULT STDMETHODCALLTYPE EnsureItemVisible(LVITEMINDEX itemIndex, BOOL partialOk) PURE;
		virtual HRESULT STDMETHODCALLTYPE EnsureSubItemVisible(LVITEMINDEX itemIndex, int subItemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE EditSubItem(LVITEMINDEX itemIndex, int subItemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE RedrawItems(int firstItemIndex, int lastItemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE ArrangeItems(int mode) PURE;
		virtual HRESULT STDMETHODCALLTYPE RecomputeItems(int unknown) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetEditControl(HWND* pHWndEdit) PURE;

		// TODO: verify that 'initialEditText' really is used to specify the initial text
		virtual HRESULT STDMETHODCALLTYPE EditLabel(LVITEMINDEX itemIndex, LPCWSTR initialEditText, HWND* phWndEdit) PURE;
		virtual HRESULT STDMETHODCALLTYPE EditGroupLabel(int groupIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE CancelEditLabel(void) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetEditItem(LVITEMINDEX* itemIndex, PINT subItemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE HitTest(LVHITTESTINFO* pHitTestData) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetStringWidth(PCWSTR pString, PINT pWidth) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetColumn(int columnIndex, LVCOLUMNW* pColumn) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetColumn(int columnIndex, LVCOLUMNW* const pColumn) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetColumnOrderArray(int numberOfColumns, PINT pColumns) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetColumnOrderArray(int numberOfColumns, int const* pColumns) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetHeaderControl(HWND* pHWndHeader) PURE;
		virtual HRESULT STDMETHODCALLTYPE InsertColumn(int insertAt, LVCOLUMNW* const pColumn, PINT pColumnIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE DeleteColumn(int columnIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE CreateDragImage(int itemIndex, POINT const* pUpperLeft, HIMAGELIST* pHImageList) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetViewRect(RECT* pRectangle) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetClientRect(BOOL unknown, RECT* pClientRectangle) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetColumnWidth(int columnIndex, PINT pWidth) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetColumnWidth(int columnIndex, int width) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetCallbackMask(ULONG* pMask) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetCallbackMask(ULONG mask) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetTopIndex(PINT pTopIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetCountPerPage(PINT pCountPerPage) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetOrigin(POINT* pOrigin) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetSelectedCount(PINT pSelectedCount) PURE;

		// 'unknown' might specify whether to pass items' data or indexes
		virtual HRESULT STDMETHODCALLTYPE SortItems(BOOL unknown, LPARAM lParam, PFNLVCOMPARE pComparisonFunction) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetExtendedStyle(DWORD* pStyle) PURE;

		// parameters may be in wrong order
		virtual HRESULT STDMETHODCALLTYPE SetExtendedStyle(DWORD mask, DWORD style, DWORD* pOldStyle) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetHoverTime(UINT* pTime) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetHoverTime(UINT time, UINT* pOldSetting) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetToolTip(HWND* pHWndToolTip) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetToolTip(HWND hWndToolTip, HWND* pHWndOldToolTip) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetHotItem(LVITEMINDEX* pHotItem) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetHotItem(LVITEMINDEX newHotItem, LVITEMINDEX* pOldHotItem) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetHotCursor(HCURSOR* pHCursor) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetHotCursor(HCURSOR hCursor, HCURSOR* pHOldCursor) PURE;

		// parameters may be in wrong order
		virtual HRESULT STDMETHODCALLTYPE ApproximateViewRect(int itemCount, PINT pWidth, PINT pHeight) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetRangeObject(int unknown, LPVOID/*ILVRange**/ pObject) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetWorkAreas(int numberOfWorkAreas, RECT* pWorkAreas) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetWorkAreas(int numberOfWorkAreas, RECT const* pWorkAreas) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetWorkAreaCount(PINT pNumberOfWorkAreas) PURE;
		virtual HRESULT STDMETHODCALLTYPE ResetEmptyText(void) PURE;
		virtual HRESULT STDMETHODCALLTYPE EnableGroupView(BOOL enable) PURE;
		virtual HRESULT STDMETHODCALLTYPE IsGroupViewEnabled(BOOL* pIsEnabled) PURE;
		virtual HRESULT STDMETHODCALLTYPE SortGroups(PFNLVGROUPCOMPARE pComparisonFunction, PVOID lParam) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetGroupInfo(int unknown1, int unknown2, LVGROUP* pGroup) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetGroupInfo(int unknown, int groupID, LVGROUP* const pGroup) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetGroupRect(BOOL unknown, int groupID, int rectangleType, RECT* pRectangle) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetGroupState(int groupID, ULONG mask, ULONG* pState) PURE;
		virtual HRESULT STDMETHODCALLTYPE HasGroup(int groupID, BOOL* pHasGroup) PURE;
		virtual HRESULT STDMETHODCALLTYPE InsertGroup(int insertAt, LVGROUP* const pGroup, PINT pGroupID) PURE;
		virtual HRESULT STDMETHODCALLTYPE RemoveGroup(int groupID) PURE;
		virtual HRESULT STDMETHODCALLTYPE InsertGroupSorted(LVINSERTGROUPSORTED const* pGroup, PINT pGroupID) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetGroupMetrics(LVGROUPMETRICS* pMetrics) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetGroupMetrics(LVGROUPMETRICS* const pMetrics) PURE;
		virtual HRESULT STDMETHODCALLTYPE RemoveAllGroups(void) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetFocusedGroup(PINT pGroupID) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetGroupCount(PINT pCount) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetOwnerDataCallback(IOwnerDataCallback* pCallback) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetTileViewInfo(LVTILEVIEWINFO* pInfo) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetTileViewInfo(LVTILEVIEWINFO* const pInfo) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetTileInfo(LVTILEINFO* pTileInfo) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetTileInfo(LVTILEINFO* const pTileInfo) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetInsertMark(LVINSERTMARK* pInsertMarkDetails) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetInsertMark(LVINSERTMARK const* pInsertMarkDetails) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetInsertMarkRect(LPRECT pInsertMarkRectangle) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetInsertMarkColor(COLORREF* pColor) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetInsertMarkColor(COLORREF color, COLORREF* pOldColor) PURE;
		virtual HRESULT STDMETHODCALLTYPE HitTestInsertMark(POINT const* pPoint, LVINSERTMARK* pInsertMarkDetails) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetInfoTip(LVSETINFOTIP* const pInfoTip) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetOutlineColor(COLORREF* pColor) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetOutlineColor(COLORREF color, COLORREF* pOldColor) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetFrozenItem(PINT pItemIndex) PURE;

		// one parameter will be the item index; works in Icons view only
		virtual HRESULT STDMETHODCALLTYPE SetFrozenItem(int unknown1, int unknown2) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetFrozenSlot(RECT* pUnknown) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetFrozenSlot(int unknown1, POINT const* pUnknown2) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetViewMargin(RECT* pMargin) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetViewMargin(RECT const* pMargin) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetKeyboardSelected(LVITEMINDEX itemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE MapIndexToId(int itemIndex, PINT pItemID) PURE;
		virtual HRESULT STDMETHODCALLTYPE MapIdToIndex(int itemID, PINT pItemIndex) PURE;
		virtual HRESULT STDMETHODCALLTYPE IsItemVisible(LVITEMINDEX itemIndex, BOOL* pVisible) PURE;
		virtual HRESULT STDMETHODCALLTYPE EnableAlphaShadow(BOOL enable) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetGroupSubsetCount(PINT pNumberOfRowsDisplayed) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetGroupSubsetCount(int numberOfRowsToDisplay) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetVisibleSlotCount(PINT pCount) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetColumnMargin(RECT* pMargin) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetSubItemCallback(LPVOID/*ISubItemCallback**/ pCallback) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetVisibleItemRange(LVITEMINDEX* pFirstItem, LVITEMINDEX* pLastItem) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetTypeAheadFlags(UINT mask, UINT flags) PURE;
	};
}}}

#endif/*_ILISTVIEW_H_B8AB7FDD_9E83_472b_AC70_A3DDAEB1A5F6_INCLUDED*/