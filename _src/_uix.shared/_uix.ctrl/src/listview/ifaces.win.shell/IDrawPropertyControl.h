#ifndef _IDRAWPROPERYCONTROL_H_7AB47F26_F6C8_4c93_B5DC_75352B9B8118_INCLUDED
#define _IDRAWPROPERYCONTROL_H_7AB47F26_F6C8_4c93_B5DC_75352B9B8118_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Mar-2018 at 04:40:25p, UTC+7, Phuket, Rawai, Sunday;
	This is Windows Shell ListView embedded control draw property interface declaration file;
	-----------------------------------------------------------------------------

	This code is borrowed(and adopted) from the project 'Undocumented List View Features' of Timo Kunze;
	https://www.codeproject.com/Articles/35197/Undocumented-List-View-Features?display=Print

	Initially, the research has been made by Geoff Chappell;
	http://www.geoffchappell.com/index.htm
*/		

#include "IPropertyControlBase.h"

namespace ex_ui { namespace controls { namespace lvw
{
	// the interface's GUID
	const IID IID_IDrawPropertyControl = {0xE6DFF6FD, 0xBCD5, 0x4162, {0x9C, 0x65, 0xA3, 0xB1, 0x8C, 0x61, 0x6F, 0xDB}};


	class IDrawPropertyControl :
    		public IPropertyControlBase
	{
	public:
		// All parameter names have been guessed!
		virtual HRESULT STDMETHODCALLTYPE GetDrawFlags(PINT) PURE;
		virtual HRESULT STDMETHODCALLTYPE WindowlessDraw(HDC hDC, RECT const* pDrawingRectangle, int a) PURE;
		virtual HRESULT STDMETHODCALLTYPE HasVisibleContent(void) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetDisplayText(LPWSTR*) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetTooltipInfo(HDC, SIZE const*, PINT) PURE;
	};
}}}

#endif/*_IDRAWPROPERYCONTROL_H_7AB47F26_F6C8_4c93_B5DC_75352B9B8118_INCLUDED*/