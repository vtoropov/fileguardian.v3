#ifndef _IPROPERTYCONTROL_H_7175A7CD_9854_4718_B749_AF25B16EA5B3_INCLUDED
#define _IPROPERTYCONTROL_H_7175A7CD_9854_4718_B749_AF25B16EA5B3_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Mar-2018 at 09:18:01a, UTC+7, Phuket, Rawai, Sunday;
	This is Windows Shell ListView embedded control property interface declaration file;
	-----------------------------------------------------------------------------

	This code is borrowed(and adopted) from the project 'Undocumented List View Features' of Timo Kunze;
	https://www.codeproject.com/Articles/35197/Undocumented-List-View-Features?display=Print

	Initially, the research has been made by Geoff Chappell;
	http://www.geoffchappell.com/index.htm
*/
#include "IPropertyControlBase.h"

namespace ex_ui { namespace controls { namespace lvw
{
	// the interface's GUID
	const IID IID_IPropertyControl = {0x5E82A4DD, 0x9561, 0x476A, {0x86, 0x34, 0x1B, 0xEB, 0xAC, 0xBA, 0x4A, 0x38}};


	class IPropertyControl :
		public IPropertyControlBase
	{
	public:
		virtual HRESULT STDMETHODCALLTYPE GetValue(REFIID requiredInterface, LPVOID* ppObject) PURE;
		// possible values for unknown2 (list may be incomplete):
		//   0x02 - mouse has been moved over the sub-item
		//   0x0C - ISubItemCallBack::EditSubItem has been called or a slow double click has been made
		virtual HRESULT STDMETHODCALLTYPE Create(HWND hWndParent, RECT const* pItemRectangle, RECT const* pUnknown1, int unknown2) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetPosition(RECT const*, RECT const*) PURE;
		virtual HRESULT STDMETHODCALLTYPE IsModified(LPBOOL pModified) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetModified(BOOL modified) PURE;
		virtual HRESULT STDMETHODCALLTYPE ValidationFailed(LPCWSTR) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetState(PINT pState) PURE;
	};

}}}

#endif/*_IPROPERTYCONTROL_H_7175A7CD_9854_4718_B749_AF25B16EA5B3_INCLUDED*/