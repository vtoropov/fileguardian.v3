#ifndef _IPROPERTYCONTROLBASE_H_BB144FC6_21BA_44e6_8C52_5D62420CF663_INCLUDED
#define _IPROPERTYCONTROLBASE_H_BB144FC6_21BA_44e6_8C52_5D62420CF663_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Mar-2018 at 10:59:28a, UTC+7, Phuket, Rawai, Sunday;
	This is Windows Shell ListView embedded control base property base interface declaration file;
	-----------------------------------------------------------------------------

	This code is borrowed(and adopted) from the project 'Undocumented List View Features' of Timo Kunze;
	https://www.codeproject.com/Articles/35197/Undocumented-List-View-Features?display=Print

	Initially, the research has been made by Geoff Chappell;
	http://www.geoffchappell.com/index.htm
*/

namespace ex_ui { namespace controls { namespace lvw
{
// the interface's GUID
const IID IID_IPropertyControlBase = {0x6E71A510, 0x732A, 0x4557, {0x95, 0x96, 0xA8, 0x27, 0xE3, 0x6D, 0xAF, 0x8F}};


	class IPropertyControlBase :
    		public IUnknown
	{
	public:
		// All parameter names have been guessed!
		// 2 -> Calendar control becomes a real calendar control instead of a date picker (but with a height of only 1 line)
		// 3 -> Calendar control becomes a simple text box
		typedef enum tagPROPDESC_CONTROL_TYPE PROPDESC_CONTROL_TYPE;
		typedef enum tagPROPCTL_RECT_TYPE PROPCTL_RECT_TYPE;
		virtual HRESULT STDMETHODCALLTYPE Initialize(LPUNKNOWN, PROPDESC_CONTROL_TYPE) PURE;

		// called when editing group labels
		// unknown1 might be a LVGGR_* value
		// hDC seems to be always NULL
		// pUnknown2 seems to be always NULL
		// pUnknown3 seems to receive the size set by the method
		virtual HRESULT STDMETHODCALLTYPE GetSize(PROPCTL_RECT_TYPE unknown1, HDC hDC, SIZE const* pUnknown2, LPSIZE pUnknown3) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetWindowTheme(LPCWSTR, LPCWSTR) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetFont(HFONT hFont) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetTextColor(COLORREF color) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetFlags(PINT) PURE;

		// IPropertyControl:
		//   called before the edit control is created and before it is dismissed
		//   flags is 1 on first call and 0 on second call (mask is always 1)
		virtual HRESULT STDMETHODCALLTYPE SetFlags(int flags, int mask) PURE;

		// possible values for unknown2 (list may be incomplete):
		//   0x02 - mouse has been moved over the sub-item
		//   0x0C - ISubItemCallBack::EditSubItem has been called or a slow double click has been made
		virtual HRESULT STDMETHODCALLTYPE AdjustWindowRectPCB(HWND hWndListView, LPRECT pItemRectangle, RECT const* pUnknown1, int unknown2) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetValue(LPUNKNOWN) PURE;
		virtual HRESULT STDMETHODCALLTYPE InvokeDefaultAction(void) PURE;
		virtual HRESULT STDMETHODCALLTYPE Destroy(void) PURE;
		virtual HRESULT STDMETHODCALLTYPE SetFormatFlags(int) PURE;
		virtual HRESULT STDMETHODCALLTYPE GetFormatFlags(PINT) PURE;
};

}}}

#endif/*_IPROPERTYCONTROLBASE_H_BB144FC6_21BA_44e6_8C52_5D62420CF663_INCLUDED*/