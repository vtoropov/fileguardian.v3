#ifndef _IPROPERTYVALUEIMPL_H_318D13FE_77AC_470e_AF37_4AE108797E33_INCLUDED
#define _IPROPERTYVALUEIMPL_H_318D13FE_77AC_470e_AF37_4AE108797E33_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Mar-2018 at 11:24:24a, UTC+7, Bangkok, Suvarnabh, Monday;
	This is Windows Shell ListView value property interface implementing class declaration file;
	-----------------------------------------------------------------------------

	This code is borrowed(and adopted) from the project 'Undocumented List View Features' of Timo Kunze;
	https://www.codeproject.com/Articles/35197/Undocumented-List-View-Features?display=Print

	Initially, the research has been made by Geoff Chappell;
	http://www.geoffchappell.com/index.htm
*/

#include "IPropertyValue.h"

namespace ex_ui { namespace controls { namespace lvw
{

	class IPropertyValueImpl :
		public IPropertyValue
	{
	public:
		//////////////////////////////////////////////////////////////////////
		/// \name Implementation of IUnknown
		///
		//@{
		/// \brief <em>Queries for an interface implementation</em>
		///
		/// Queries this object for its implementation of a given interface.
		///
		/// \param[in] requiredInterface The IID of the interface of which the object's implementation will
		///            be returned.
		/// \param[out] ppInterfaceImpl Receives the object's implementation of the interface identified
		///             by \c requiredInterface.
		///
		/// \return An \c HRESULT error code.
		///
		/// \sa AddRef, Release,
		///     <a href="http://msdn.microsoft.com/en-us/library/ms682521.aspx">IUnknown::QueryInterface</a>
		HRESULT STDMETHODCALLTYPE QueryInterface(REFIID requiredInterface, LPVOID* ppInterfaceImpl);

		/// \brief <em>Adds a reference to this object</em>
		///
		/// Increases the references counter of this object by 1.
		///
		/// \return The new reference count.
		///
		/// \sa Release, QueryInterface,
		///     <a href="http://msdn.microsoft.com/en-us/library/ms691379.aspx">IUnknown::AddRef</a>
		ULONG STDMETHODCALLTYPE AddRef();

		/// \brief <em>Removes a reference from this object</em>
		///
		/// Decreases the references counter of this object by 1. If the counter reaches 0, the object
		/// is destroyed.
		///
		/// \return The new reference count.
		///
		/// \sa AddRef, QueryInterface,
		///     <a href="http://msdn.microsoft.com/en-us/library/ms682317.aspx">IUnknown::Release</a>
		ULONG STDMETHODCALLTYPE Release();

		//@}
		//////////////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////////////
		/// \name Implementation of IPropertyValue
		///
		//@{
		/// \brief <em>Sets the \c PROPERTYKEY structure that identifies the property wrapped by the object</em>
		///
		/// \param[in] propKey The property identifier. It will be stored by the object.
		///
		/// \return An \c HRESULT error code.
		///
		/// \sa GetPropertyKey,
		///     <a href="http://msdn.microsoft.com/en-us/library/bb773381.aspx">PROPERTYKEY</a>
		HRESULT STDMETHODCALLTYPE SetPropertyKey(PROPERTYKEY const& propKey);

		/// \brief <em>Retrieves the \c PROPERTYKEY structure that identifies the property wrapped by the object</em>
		///
		/// \param[out] pPropKey Receives the property identifier.
		///
		/// \return An \c HRESULT error code.
		///
		/// \sa SetPropertyKey,
		///     <a href="http://msdn.microsoft.com/en-us/library/bb773381.aspx">PROPERTYKEY</a>
		HRESULT STDMETHODCALLTYPE GetPropertyKey(PROPERTYKEY* pPropKey);

		/// \brief <em>Retrieves the current value of the property wrapped by the object</em>
		///
		/// \param[out] pPropValue Receives the property value.
		///
		/// \return An \c HRESULT error code.
		///
		/// \sa InitValue
		HRESULT STDMETHODCALLTYPE GetValue(PROPVARIANT* pPropValue);

		/// \brief <em>Initializes the object with the property's current value</em>
		///
		/// \param[in] propValue The property's current value. It will be stored by the object.
		///
		/// \return An \c HRESULT error code.
		///
		/// \sa GetValue
		HRESULT STDMETHODCALLTYPE InitValue(PROPVARIANT const& propValue);

		//@}
		//////////////////////////////////////////////////////////////////////
	
		/// \brief <em>Creates an instance of this class</em>
		///
		/// Creates an instance of this class, initializes the object with the given values and returns
		/// the implementation of a given interface.
		///
		/// \param[in] pOuter The outer unknown or controlling unknown of the aggregate.
		/// \param[in] requiredInterface The IID of the interface of which the object's implementation will
		///            be returned.
		/// \param[out] ppInterfaceImpl Receives the object's implementation of the interface identified
		///             by \c requiredInterface.
		///
		/// \return An \c HRESULT error code.
		static HRESULT CreateInstance(IUnknown* pOuter, REFIID requiredInterface, LPVOID* ppInterfaceImpl);

		/// \brief <em>Creates an instance of this class</em>
		///
		/// Creates an instance of this class, initializes the object with the given values and returns
		/// the \c IPropertyValue implementation.
		///
		/// \param[out] ppPropertyValue Receives the object's implementation of the \c IPropertyValue interface.
		///
		/// \return An \c HRESULT error code.
		static HRESULT CreateInstance(IPropertyValue** ppPropertyValue);

	protected:
		/// \brief <em>Holds the object's properties' settings</em>
		struct Properties
		{
			/// \brief <em>Holds the object's reference count</em>
			volatile LONG referenceCount;
			/// \brief <em>Holds the \c PROPERTYKEY structure that identifies the wrapped property</em>
			///
			/// \sa SetPropertyKey, GetPropertyKey,
			///     <a href="http://msdn.microsoft.com/en-us/library/bb773381.aspx">PROPERTYKEY</a>
			PROPERTYKEY propertyKey;
			/// \brief <em>Holds the current value of the wrapped property</em>
			///
			/// \sa InitValue, GetValue
			PROPVARIANT propertyValue;

			Properties()
			{
				referenceCount = 0;
				PropVariantInit(&propertyValue);
			}

			~Properties()
			{
				referenceCount = 0;
				PropVariantClear(&propertyValue);
			}
		} properties;
	};// IPropertyValueImpl

}}}
#endif/*_IPROPERTYVALUEIMPL_H_318D13FE_77AC_470e_AF37_4AE108797E33_INCLUDED*/