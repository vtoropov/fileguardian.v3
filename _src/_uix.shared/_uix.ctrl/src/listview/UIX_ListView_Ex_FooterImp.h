#ifndef _UIXLISTVIEWEXFOOTERIMP_H_7600BFC8_7316_4818_9E72_9380EB31E9A9_INCLUDED
#define _UIXLISTVIEWEXFOOTERIMP_H_7600BFC8_7316_4818_9E72_9380EB31E9A9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Mar-2018 at 12:16:54p, UTC+7, Bangkok, Suvarnabh, Monday;
	This is This is UIX Control library extended standard list view control footer ifaces declaration file.
	-----------------------------------------------------------------------------

	This code is borrowed(and adopted) from the project 'Undocumented List View Features' of Timo Kunze;
	https://www.codeproject.com/Articles/35197/Undocumented-List-View-Features?display=Print

	Initially, the research has been made by Geoff Chappell;
	http://www.geoffchappell.com/index.htm
*/
#include "IListViewFooter.h"

namespace ex_ui { namespace controls { namespace lvw
{
	// {EBE684BF-3301-4a6d-83CE-E4D6851BE881}
	DEFINE_GUID(CLSID_CFooterView, 0xebe684bf, 0x3301, 0x4a6d, 0x83, 0xce, 0xe4, 0xd6, 0x85, 0x1b, 0xe8, 0x81);

	class CFooterView :
		public CComObjectRootEx<CComMultiThreadModel>,
		public CComCoClass<CFooterView, &CLSID_CFooterView>,
		public IListViewFooterCallback,
		public IListViewFooter
	{
	private:
		::ATL::CWindow&       m_lvw_wnd;
	public:
		BOOL PreTranslateMessage(MSG* pMsg);

		BEGIN_COM_MAP(CFooterView)
			COM_INTERFACE_ENTRY_IID(IID_IListViewFooterCallback, IListViewFooterCallback)
			COM_INTERFACE_ENTRY_IID(IID_IListViewFooter, IListViewFooter)
		END_COM_MAP()
	public:
		CFooterView(::ATL::CWindow& hLvwHandle);
		~CFooterView(void);

	private: // IListViewFooterCallback
		virtual STDMETHODIMP OnButtonClicked(int itemIndex, LPARAM lParam, PINT pRemoveFooter) override sealed;
		virtual STDMETHODIMP OnDestroyButton(int itemIndex, LPARAM lParam) override sealed;

	private: // IListViewFooter
		virtual HRESULT STDMETHODCALLTYPE IsVisible(PINT pnVisible) override sealed;
		virtual HRESULT STDMETHODCALLTYPE GetFooterFocus(PINT pnItemIndex) override sealed;
		virtual HRESULT STDMETHODCALLTYPE SetFooterFocus(INT pItemIndex) override sealed;
		virtual HRESULT STDMETHODCALLTYPE SetIntroText(LPCWSTR lpszText) override sealed;
		virtual HRESULT STDMETHODCALLTYPE Show(IListViewFooterCallback* plvCallbackObject) override sealed;
		virtual HRESULT STDMETHODCALLTYPE RemoveAllButtons(void) override sealed;
		virtual HRESULT STDMETHODCALLTYPE InsertButton(INT nInsertAt, LPCWSTR lpszText, LPCWSTR lpszUnknown, UINT nIconIndex, LONG lParam) override sealed;
		virtual HRESULT STDMETHODCALLTYPE GetButtonLParam(INT nItemIndex, LONG* plParam) override sealed;

	public:
		LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	};
}}}

#endif/*_UIXLISTVIEWEXFOOTERIMP_H_7600BFC8_7316_4818_9E72_9380EB31E9A9_INCLUDED*/