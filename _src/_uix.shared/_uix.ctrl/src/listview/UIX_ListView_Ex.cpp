/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Mar-2018 at 10:32:20a, UTC+7, Phuket, Rawai, Tuesday;
	This is UIX Control library extended standard list view control interface implementation file.
*/
#include "StdAfx.h"
#include "UIX_ListView_Ex.h"

using namespace ex_ui::controls;

/////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace controls { namespace details {

	static const DWORD CListViewCol_Ex_Style = WS_CHILD|WS_VISIBLE|LVS_REPORT|LVS_NOSORTHEADER|LVS_SINGLESEL|LVS_SHOWSELALWAYS;
	static const DWORD CListViewCol_Ex_Style_Ex = LVS_EX_FULLROWSELECT|LVS_EX_LABELTIP|LVS_EX_INFOTIP;

	HRESULT   CListViewCol_Ex_ValidateArgs(const HWND hDialog, const UINT nLvwId) {
		HRESULT hr_ = S_OK;

		if (NULL == hDialog ||
		   !::IsWindow(hDialog)) return (hr_ = OLE_E_INVALIDHWND);

		if (!nLvwId)
			return (hr_ = E_INVALIDARG);

		return hr_;
	}

	HRESULT   CListViewCol_Ex_ValidateArgs(const HWND hDialog, const UINT nLvwId, const RECT& rcArea) {
		HRESULT hr_ = CListViewCol_Ex_ValidateArgs(hDialog, nLvwId);
		if (FAILED(hr_))
			return hr_;

		if (::IsRectEmpty(&rcArea))
			return (hr_ = E_INVALIDARG);

		return hr_;
	}

}}}

/////////////////////////////////////////////////////////////////////////////

CListViewCol_Ex::CListViewCol_Ex(void) {
	::memset(&m_column, 0, sizeof(LVCOLUMN));
	m_column.pszText = m_title.GetBuffer();
}

CListViewCol_Ex::CListViewCol_Ex(const CListViewCol_Ex& _col) {
	::memset(&m_column, 0, sizeof(LVCOLUMN));
	*this = _col;
}

CListViewCol_Ex::CListViewCol_Ex(const UINT _format, const UINT _width, const UINT _index, LPCTSTR _title, const UINT _mask) {
	::memset(&m_column, 0, sizeof(LVCOLUMN));

	m_title = _title;

	m_column.pszText = m_title.GetBuffer();

	m_column.cx       = _width;
	m_column.fmt      = _format;
	m_column.iSubItem = _index;
	m_column.mask     = _mask;
}

CListViewCol_Ex::~CListViewCol_Ex(void) {}

/////////////////////////////////////////////////////////////////////////////

const LVCOLUMN& CListViewCol_Ex::ColumnData(void)const { return m_column; }

/////////////////////////////////////////////////////////////////////////////

CListViewCol_Ex::operator LVCOLUMN&(void) { return m_column; }
CListViewCol_Ex::operator CAtlString&(void) { return m_title; }
CListViewCol_Ex::operator const LVCOLUMN&(void) const { return m_column; }

/////////////////////////////////////////////////////////////////////////////

CListViewCol_Ex& CListViewCol_Ex::operator= (const CListViewCol_Ex& _col) {
	m_title  = _col.m_title;
	m_column = _col.m_column;

	m_column.pszText = m_title.GetBuffer();

	return (*this);
}

/////////////////////////////////////////////////////////////////////////////

CListViewCtrl_Ex::CListViewCtrl_Ex(IListView_Ex_DataSource& _src, IListView_Ex_Footer& _footer) :
	m_ctrlId(0), m_DataSource(_src), m_footer(_footer), m_groups(m_control) {}

CListViewCtrl_Ex::CListViewCtrl_Ex(IListView_Ex_DataSource& _src, IListView_Ex_Footer& _footer, HWND _list, const UINT nLvwId) :
	m_ctrlId(nLvwId), m_DataSource(_src), m_control(_list), m_footer(_footer), m_groups(m_control) {}

CListViewCtrl_Ex::~CListViewCtrl_Ex(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CListViewCtrl_Ex::Attach  (const HWND hLvCtrl, const UINT nLvwId)
{
	HRESULT hr_ = S_OK;

	if (IsValid())
		return (hr_ = HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED));

	m_control = hLvCtrl;
	m_ctrlId = nLvwId;

	if (::WTL::RunTimeHelper::IsVista())
		ListView_SetExtendedListViewStyle(m_control, details::CListViewCol_Ex_Style_Ex|LVS_EX_DOUBLEBUFFER);
	else
		ListView_SetExtendedListViewStyle(m_control, details::CListViewCol_Ex_Style_Ex);

	LONG lStyle = m_control.GetWindowLong(GWL_STYLE);
	lStyle &= ~WS_HSCROLL;
	m_control.SetWindowLong(GWL_STYLE, lStyle);

	return hr_;
}

const
CListViewCtrl&CListViewCtrl_Ex::Control (void) const { return m_control; }
CListViewCtrl&CListViewCtrl_Ex::Control (void)       { return m_control; }

HRESULT       CListViewCtrl_Ex::Create  (const HWND hDialog, const UINT nLvwId, const RECT& rcArea)
{
	HRESULT hr_ = S_OK;

	if (IsValid())
		return (hr_ = HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED));

	if (0 == nLvwId || nLvwId == m_ctrlId)
		return (hr_ = E_INVALIDARG);

	hr_ = details::CListViewCol_Ex_ValidateArgs(hDialog, nLvwId, rcArea);
	if (FAILED(hr_))
		return hr_;

	RECT rc_ = rcArea;

	m_control.Create(
			hDialog, rc_, NULL, details::CListViewCol_Ex_Style, WS_EX_CLIENTEDGE, m_ctrlId
		);

	if (!m_control)
		return (hr_ = HRESULT_FROM_WIN32(::GetLastError()));
	else {
		const HFONT hFont = CWindow(hDialog).GetFont();
		m_control.SetFont(hFont);

		CWindow header_ = ListView_GetHeader(m_control);
		if (header_)
			header_.SetFont(hFont);
	}

	if (::WTL::RunTimeHelper::IsVista())
		ListView_SetExtendedListViewStyle(m_control, details::CListViewCol_Ex_Style_Ex|LVS_EX_DOUBLEBUFFER);
	else
		ListView_SetExtendedListViewStyle(m_control, details::CListViewCol_Ex_Style_Ex);

	m_ctrlId = nLvwId;

	return hr_;
}

HRESULT       CListViewCtrl_Ex::Destroy (void)
{
	HRESULT hr_ = S_OK;

	m_ctrlId = 0;

	if (!m_control)
		return (hr_ = OLE_E_BLANK);
	m_control.SendMessage(WM_CLOSE);
	m_control = (HWND)NULL;

	return hr_;
}

const
CListViewFooter& CListViewCtrl_Ex::Footer(void)const{ return m_footer; }
CListViewFooter& CListViewCtrl_Ex::Footer(void)     { return m_footer; }
const
CListViewGroups& CListViewCtrl_Ex::Groups(void)const{ return m_groups; }
CListViewGroups& CListViewCtrl_Ex::Groups(void)     { return m_groups; }

HRESULT       CListViewCtrl_Ex::Insert  (const TListViewCols_Ex& _cols)
{
	HRESULT hr_ = S_OK;

	if (_cols.empty())
		return (hr_ = E_INVALIDARG);

	if (!IsValid())
		return (hr_ = OLE_E_BLANK);

	for (size_t i_ = 0; i_ < _cols.size(); i_++) {

		CListViewCol_Ex col_ = _cols[i_];
		ListView_InsertColumn(m_control, col_.ColumnData().iSubItem, &((LVCOLUMN&)col_));
	}

	return  hr_;
}

bool          CListViewCtrl_Ex::IsValid (void) const
{
	return (!!m_control.IsWindow());
}

HRESULT       CListViewCtrl_Ex::Redraw  (void) const {
	HRESULT hr_ = S_OK;
	if (!this->IsValid())
		return (hr_ = OLE_E_BLANK);
	WTL::CListViewCtrl lvw_cached = m_control;
	lvw_cached.RedrawItems(
			m_control.GetTopIndex(), m_control.GetTopIndex() + m_control.GetCountPerPage()
		);
	lvw_cached.RedrawWindow();
	return  hr_;
}

HRESULT       CListViewCtrl_Ex::Replace (const HWND hDialog, const UINT nLvwId)
{
	HRESULT hr_ = S_OK;

	CWindow list_ =  ::GetDlgItem(hDialog, nLvwId);
	if (!list_)
		return (hr_ = HRESULT_FROM_WIN32(ERROR_NOT_FOUND));

	RECT rc_ = {0};
	list_.GetWindowRect(&rc_);
	::MapWindowPoints(HWND_DESKTOP, hDialog, (LPPOINT)&rc_, 0x2);

	list_.DestroyWindow();

	hr_ = Create(hDialog, nLvwId, rc_);
	
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CListViewCtrl_Ex::operator HWND(void)const {
	return m_control;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT       CListViewCtrl_Ex::OnMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	LRESULT result_ = NO_ERROR;

	if (WM_MOUSEWHEEL == uMsg) {

		const PMOUSEHOOKSTRUCTEX pData = reinterpret_cast<PMOUSEHOOKSTRUCTEX>(lParam);
		if (NULL != pData) {

			const SHORT nDelta = static_cast<SHORT>(HIWORD(pData->mouseData));
			const INT nIndex   = m_control.GetSelectedIndex() + (0 > nDelta ? +1 : -1);

			m_control.SetItemState(nIndex, LVIS_FOCUSED | LVIS_SELECTED, 0x000F);
		}

		return result_;
	}

	if (WM_NOTIFY != uMsg)
		return result_;

	NMHDR* header_ = reinterpret_cast<NMHDR*>(lParam);
	if (NULL == header_)
		return result_;

	if (m_ctrlId != header_->idFrom)
		return result_;

	switch (header_->code)
	{
	case LVN_GETDISPINFO:
		{
			 NMLVDISPINFO* pDispInfo = reinterpret_cast<NMLVDISPINFO*>(header_);
			 if (NULL !=  pDispInfo)
			 {
				 HRESULT hr_ = m_DataSource.IListView_OnDispQuery(pDispInfo);
				 if (SUCCEEDED(hr_)){
				 }
				 bHandled = TRUE;
			 }
		} break;
	case LVN_ODCACHEHINT:
		{
			NMLVCACHEHINT* pCacheHint = reinterpret_cast<NMLVCACHEHINT*>(header_);
			if (NULL != pCacheHint)
			{
				HRESULT hr_ = m_DataSource.IListView_OnCacheHint(pCacheHint);
				if (SUCCEEDED(hr_)){
				}
				bHandled = TRUE;
			}
		} break;
	case LVN_ODFINDITEM :
		{
			LPNMLVFINDITEM pFindItem = reinterpret_cast<LPNMLVFINDITEM>(header_);
			if (NULL != pFindItem)
			{
				HRESULT hr_ = m_DataSource.IListView_OnFindItem(pFindItem);
				if (SUCCEEDED(hr_)){
				}
				bHandled = TRUE;
			}
		} break;
	default:
		break;
	}

	return result_;
}