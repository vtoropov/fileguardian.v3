/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Apr-2018 at 9:43:40p, UTC+7, Novosibirsk, Rodniki, Wednesday;
	This is UIX Control library extended standard list view control group interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 11:34:21p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "UIX_ListView_Ex_Group.h"

using namespace ex_ui::controls;
/////////////////////////////////////////////////////////////////////////////

CListViewGroup::CListViewGroup(void) : m_items(0), m_n_id(0) {}
CListViewGroup::CListViewGroup(LPCTSTR lpszTitle, const UINT nItemCount, const INT nId) : m_title(lpszTitle), m_items(nItemCount), m_n_id(nId) {}

/////////////////////////////////////////////////////////////////////////////

INT             CListViewGroup::Identifier(void)const   { return   m_n_id;  }
VOID            CListViewGroup::Identifier(const INT _v){ m_n_id  = _v;     }
bool            CListViewGroup::IsValid(void)const      { return   m_title.IsEmpty() == false && 0 != m_n_id; }
UINT            CListViewGroup::Items(void)const        { return   m_items; }
HRESULT         CListViewGroup::Items(const UINT _v)    {
	m_items = _v;
	m_title_fmt.Format(
			_T("%s (%d)"), this->m_title.GetString(), this->m_items
		);
	return S_OK;
}

LPCTSTR         CListViewGroup::Title(void)const        { return   m_title.GetString(); }
HRESULT         CListViewGroup::Title(LPCTSTR _lpsz)    {
	m_title = _lpsz;
	m_title_fmt.Format(
			_T("%s (%d)"), this->m_title.GetString(), this->m_items
		);
	return S_OK;
}

LPCTSTR         CListViewGroup::TitleAsString(void) const {
	
	return   m_title_fmt.GetString();
}

/////////////////////////////////////////////////////////////////////////////

LVGROUP         CListViewGroup::ToObject(void) const {
	LVGROUP native_   = {0};
	native_.cbSize    = sizeof(LVGROUP);
	native_.mask      = CListViewGroup::eDefaultMask;
	native_.iGroupId  = this->Identifier();
	native_.uAlign    = LVGA_HEADER_LEFT;
	native_.cItems    = this->Items();
	native_.state     = LVGS_COLLAPSIBLE;
	native_.stateMask = LVGS_COLLAPSIBLE;

	native_.pszHeader = NULL;

	return  native_;
}

/////////////////////////////////////////////////////////////////////////////

INT      CListViewGroup::GenerateId(void) {

	static bool bInitialized = false;
	if (bInitialized == false) {
		bInitialized  = true ;

		::srand((unsigned)::time(NULL));
	}
	INT nResult = 0;
	return nResult;
}

/////////////////////////////////////////////////////////////////////////////

CListViewGroups::CListViewGroups(::ATL::CWindow& _lvw_wnd) : m_lvw_wnd(_lvw_wnd), m_enabled(false) {
}

/////////////////////////////////////////////////////////////////////////////

HRESULT         CListViewGroups::Append(const CListViewGroup& _group) {
	HRESULT hr_ = S_OK;

	if (_group.IsValid() == false)
		return (hr_ = HRESULT_FROM_WIN32(ERROR_INVALID_DATA));

	if (this->Find(_group.Identifier()) != -1)
		return (hr_ = HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS));

	if (this->IsValid() == false)
		return (hr_ = OLE_E_BLANK);

	CAtlString cs_title = _group.Title();

	LVGROUP native_   = _group.ToObject();
	native_.pszHeader = cs_title.GetBuffer();
	native_.cchHeader = cs_title.GetAllocLength();

	const LRESULT lResult = m_lvw_wnd.SendMessage(
			LVM_INSERTGROUP, (WPARAM)-1, (LPARAM)&native_ // -1 index shows that new group is added at the end of others;
		);
	if (-1 == lResult)
		return HRESULT_FROM_WIN32(::GetLastError());

	try {
		m_groups.push_back(_group);
	}
	catch (std::bad_alloc&) {
		hr_ = E_OUTOFMEMORY;
	}
	return  hr_;
}

VOID            CListViewGroups::Clear(void)        { if (m_groups.empty() == false) m_groups.clear(); }
INT             CListViewGroups::Count(void)const   { return static_cast<INT>(m_groups.size());        }
bool            CListViewGroups::Enabled(void)const { return m_enabled; }
HRESULT         CListViewGroups::Enabled(const bool bValue) {
	HRESULT hr_ = S_OK;
	if (this->IsValid() == false)
		return (hr_ = OLE_E_BLANK);

	if (this->Enabled() && bValue)
		return hr_;

	if (this->Enabled() == false && !bValue)
		return hr_;

	const INT nResult = static_cast<INT>(ListView_EnableGroupView(m_lvw_wnd, static_cast<BOOL>(bValue)));
	if (1 ==  nResult) {
		m_enabled = bValue;
	}
	else if (-1 == nResult)
		hr_ = E_FAIL;

	return  hr_;
}

TErrorRef       CListViewGroups::Error(void)const   { return m_error; }
INT             CListViewGroups::Find (const INT nId) const {
	for (INT i_ = 0; i_ < this->Count(); i_++) {
		if (nId == m_groups[i_].Identifier())
			return i_;
	}

	return CListViewGroups::eInvalidIndex;
}

bool            CListViewGroups::IsValid(void)const { return !!m_lvw_wnd.IsWindow(); }
const
CListViewGroup& CListViewGroups::Item (const INT nIndex)const {

	if (0 > nIndex || nIndex > this->Count()) {
		static CListViewGroup na_group;
		return na_group;
	}
	else
		return m_groups[nIndex];
}

CListViewGroup& CListViewGroups::Item (const INT nIndex) {

	if (0 > nIndex || nIndex > this->Count()) {
		static CListViewGroup na_group;
		return na_group;
	}
	else
		return m_groups[nIndex];
}