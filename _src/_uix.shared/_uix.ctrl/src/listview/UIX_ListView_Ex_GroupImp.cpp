/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Apr-2018 at 10:32:40p, UTC+7, Novosibirsk, Rodniki, Wednesday;
	This is UIX Control library extended standard list view control group native interface implementation file.
	-----------------------------------------------------------------------------

	This code is borrowed(and adopted) from the project 'Undocumented List View Features' of Timo Kunze;
	https://www.codeproject.com/Articles/35197/Undocumented-List-View-Features?display=Print

	Initially, the research has been made by Geoff Chappell;
	http://www.geoffchappell.com/index.htm
*/
#include "StdAfx.h"
#include "UIX_ListView_Ex_GroupImp.h"

using namespace ex_ui::controls::lvw;

/////////////////////////////////////////////////////////////////////////////

CGroupedModeView::CGroupedModeView(::ATL::CWindow& hLvwHandle) : m_lvw_wnd(hLvwHandle) {}
CGroupedModeView::~CGroupedModeView(void) {}

/////////////////////////////////////////////////////////////////////////////

STDMETHODIMP CGroupedModeView::GetItemPosition  (int itemIndex  , LPPOINT pPosition) { itemIndex; pPosition; return E_NOTIMPL; }
STDMETHODIMP CGroupedModeView::SetItemPosition  (int itemIndex  , POINT position)    { itemIndex;  position; return E_NOTIMPL; }
STDMETHODIMP CGroupedModeView::GetItemInGroup   (int groupIndex , int groupWideItemIndex, PINT pTotalItemIndex) {
	groupIndex; groupWideItemIndex; pTotalItemIndex;
	return E_NOTIMPL;
}

STDMETHODIMP CGroupedModeView::GetItemGroup     (int itemIndex  , int occurenceIndex, PINT pGroupIndex) {
	itemIndex; occurenceIndex; pGroupIndex;
	return E_NOTIMPL;
}

STDMETHODIMP CGroupedModeView::GetItemGroupCount(int itemIndex  , PINT pOccurenceCount) {
	itemIndex; pOccurenceCount;
	return E_NOTIMPL;
}

STDMETHODIMP CGroupedModeView::OnCacheHint(LVITEMINDEX firstItem, LVITEMINDEX lastItem) {
	firstItem; lastItem;
	return E_NOTIMPL;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CGroupedModeView::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled = FALSE;

	IOwnerDataCallback* pCallback = NULL;
	HRESULT hr_ = QueryInterface(IID_IOwnerDataCallback, reinterpret_cast<LPVOID*>(&pCallback));
	if (FAILED(hr_))
		return 0;

	if(IsWin7()) {
		::ATL::CComPtr<IListView_Win7> pLvw = NULL;
		m_lvw_wnd.SendMessage(LVM_QUERYINTERFACE, reinterpret_cast<WPARAM>(&IID_IListView_Win7), reinterpret_cast<LPARAM>(&pLvw));
		if (pLvw) {
			pLvw->SetOwnerDataCallback(pCallback);
		}
	}
	else {
		::ATL::CComPtr<IListView_WinVista> pLvw = NULL;
		m_lvw_wnd.SendMessage(LVM_QUERYINTERFACE, reinterpret_cast<WPARAM>(&IID_IListView_WinVista), reinterpret_cast<LPARAM>(&pLvw));
		if (pLvw) {
			pLvw->SetOwnerDataCallback(pCallback);
		}
	}

	return 0;
}

LRESULT CGroupedModeView::OnGetDispInfo(INT idCtrl, LPNMHDR pnmh, BOOL& bHandled) {
	idCtrl; pnmh; bHandled = FALSE;

	NMLVDISPINFO* pDetails = reinterpret_cast<NMLVDISPINFO*>(pnmh);
	if (NULL == pDetails)
		return 0;
	if(pDetails->item.mask & LVIF_TEXT) {
		::StringCchPrintf(pDetails->item.pszText, pDetails->item.cchTextMax, _T("Item %i"), pDetails->item.iItem + 1);
	}

	if(pDetails->item.mask & LVIF_IMAGE) {
		pDetails->item.iImage = 0;
	}

	bHandled = TRUE;
	return 0;
}
