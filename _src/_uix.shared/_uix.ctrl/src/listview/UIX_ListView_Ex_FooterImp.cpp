/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Mar-2018 at 1:14:42p, UTC+7, Bangkok, Suvarnabh, Monday;
	This is This is UIX Control library extended standard list view control footer ifaces implementation file.
	-----------------------------------------------------------------------------

	This code is borrowed(and adopted) from the project 'Undocumented List View Features' of Timo Kunze;
	https://www.codeproject.com/Articles/35197/Undocumented-List-View-Features?display=Print

	Initially, the research has been made by Geoff Chappell;
	http://www.geoffchappell.com/index.htm
*/
#include "StdAfx.h"
#include "UIX_ListView_Ex_FooterImp.h"

using namespace ex_ui::controls;
using namespace ex_ui::controls::lvw;

/////////////////////////////////////////////////////////////////////////////

CFooterView::CFooterView(::ATL::CWindow& hLvwHandle) : m_lvw_wnd(hLvwHandle)
{
}

CFooterView::~CFooterView(void)
{
}

/////////////////////////////////////////////////////////////////////////////

BOOL CFooterView::PreTranslateMessage(MSG* pMsg)
{
	pMsg; return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

STDMETHODIMP CFooterView::OnButtonClicked(int itemIndex, LPARAM lParam, PINT pRemoveFooter)
{
	itemIndex; lParam; pRemoveFooter;
	HRESULT hr_ = S_OK;
	return  hr_;
}

STDMETHODIMP CFooterView::OnDestroyButton(int itemIndex, LPARAM lParam)
{
	itemIndex; lParam;
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT STDMETHODCALLTYPE CFooterView::IsVisible(PINT pnVisible)
{
	pnVisible;
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT STDMETHODCALLTYPE CFooterView::GetFooterFocus(PINT pnItemIndex)
{
	pnItemIndex;
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT STDMETHODCALLTYPE CFooterView::SetFooterFocus(INT nItemIndex)
{
	nItemIndex;
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT STDMETHODCALLTYPE CFooterView::SetIntroText(LPCWSTR lpszText)
{
	lpszText;
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT STDMETHODCALLTYPE CFooterView::Show(IListViewFooterCallback* plvCallbackObject)
{
	plvCallbackObject;
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT STDMETHODCALLTYPE CFooterView::RemoveAllButtons(void)
{
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT STDMETHODCALLTYPE CFooterView::InsertButton(INT nInsertAt, LPCWSTR lpszText, LPCWSTR lpszUnknown, UINT nIconIndex, LONG lParam)
{
	nInsertAt; lpszText; lpszUnknown; nIconIndex; lParam;
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT STDMETHODCALLTYPE CFooterView::GetButtonLParam(INT nItemIndex, LONG* plParam)
{
	nItemIndex; plParam;
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CFooterView::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	LRESULT lResult = 0;
	return  lResult;
}