#ifndef _UIXLISTVIEWEXGROUPIMP_H_2CAC1EA3_0531_4fc4_986C_15488B76391E_INCLUDED
#define _UIXLISTVIEWEXGROUPIMP_H_2CAC1EA3_0531_4fc4_986C_15488B76391E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Apr-2018 at 9:50:40p, UTC+7, Novosibirsk, Rodniki, Wednesday;
	This is UIX Control library extended standard list view control group native interface declaration file.
	-----------------------------------------------------------------------------

	This code is borrowed(and adopted) from the project 'Undocumented List View Features' of Timo Kunze;
	https://www.codeproject.com/Articles/35197/Undocumented-List-View-Features?display=Print

	Initially, the research has been made by Geoff Chappell;
	http://www.geoffchappell.com/index.htm
*/
#include "IListView.h"

namespace ex_ui { namespace controls { namespace lvw
{
	// {A08A0F2D-0647-4443-9450-C460F4791046}
	DEFINE_GUID(CLSID_CGroupedModeView, 0xa08a0f2d, 0x647, 0x4443, 0x94, 0x50, 0xc4, 0x60, 0xf4, 0x79, 0x10, 0x46);

	class CGroupedModeView :
		public ::ATL::CComObjectRootEx<::ATL::CComMultiThreadModel>,
		public ::ATL::CComCoClass<CGroupedModeView, &CLSID_CGroupedModeView>,
		public IOwnerDataCallback
	{
	private:
		::ATL::CWindow&       m_lvw_wnd;

	public:
		BEGIN_COM_MAP(CGroupedModeView)
			COM_INTERFACE_ENTRY_IID(IID_IOwnerDataCallback, IOwnerDataCallback)
		END_COM_MAP()

	public:
		CGroupedModeView(::ATL::CWindow& hLvwHandle);
		~CGroupedModeView(void);

	private: // IOwnerDataCallback
		virtual STDMETHODIMP GetItemPosition  (int itemIndex  , LPPOINT pPosition) override;
		virtual STDMETHODIMP SetItemPosition  (int itemIndex  , POINT position) override;
		virtual STDMETHODIMP GetItemInGroup   (int groupIndex , int groupWideItemIndex, PINT pTotalItemIndex) override;
		virtual STDMETHODIMP GetItemGroup     (int itemIndex  , int occurenceIndex, PINT pGroupIndex) override;
		virtual STDMETHODIMP GetItemGroupCount(int itemIndex  , PINT pOccurenceCount) override;
		virtual STDMETHODIMP OnCacheHint(LVITEMINDEX firstItem, LVITEMINDEX lastItem) override;

	public:
		LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		LRESULT OnGetDispInfo(INT idCtrl, LPNMHDR pnmh, BOOL& bHandled);
	};
}}}

#endif/*_UIXLISTVIEWEXGROUPIMP_H_2CAC1EA3_0531_4fc4_986C_15488B76391E_INCLUDED*/