/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Mar-2018 at 9:59:29p, UTC+7, Bangkok, Suvarnabh, Monday;
	This is UIX Control library extended standard list view control interface implementation file.
*/
#include "StdAfx.h"
#include "UIX_ListView_Ex_Footer.h"
#include "IListViewFooter.h"

using namespace ex_ui::controls;
using namespace ex_ui::controls::lvw;

/////////////////////////////////////////////////////////////////////////////

CListViewFooterItem::CListViewFooterItem(void) : m_data(0), m_icon(0) {}

CListViewFooterItem::~CListViewFooterItem(void) {}

/////////////////////////////////////////////////////////////////////////////

LONG          CListViewFooterItem::Data (void)const       { return  m_data; }
VOID          CListViewFooterItem::Data (const LONG _val) { m_data  = _val; }
LONG          CListViewFooterItem::Icon (void)const       { return  m_icon; }
VOID          CListViewFooterItem::Icon (const LONG _val) { m_icon  = _val; }
LPCTSTR       CListViewFooterItem::Title(void)const       { return m_title.GetString(); }
VOID          CListViewFooterItem::Title(LPCTSTR    _val) { m_title = _val; }

/////////////////////////////////////////////////////////////////////////////

CListViewFooterCtrls::CListViewFooterCtrls(void) {}
CListViewFooterCtrls::~CListViewFooterCtrls(void) {}

/////////////////////////////////////////////////////////////////////////////

INT           CListViewFooterCtrls::Active(void)const
{
	INT nIndex = -1;
	return nIndex;
}

HRESULT       CListViewFooterCtrls::Active(const INT nIndex)
{
	nIndex;
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT       CListViewFooterCtrls::Insert(const CListViewFooterItem&)
{
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT       CListViewFooterCtrls::Remove(const INT nIndex)
{
	nIndex;
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT       CListViewFooterCtrls::RemoveAll(void)
{
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

#include "UIX_Browser.h"

namespace ex_ui { namespace controls { namespace details {

	using ex_ui::CWebBrowser;
	using ex_ui::IWebBrowserEventHandler;

	using shared::lite::common::CSysError;

	class CListViewFooter_Wnd : public ATL::CWindowImpl<CListViewFooter_Wnd>, public IWebBrowserEventHandler {

		typedef ATL::CWindowImpl<CListViewFooter_Wnd> TWindow;

	protected:
		IListView_Ex_Footer&
		                  m_handler;
		CWebBrowser       m_browser;      // embedded web browser object
		CAtlString        m_def_url;      // the default URL
		CSysError         m_error;        // the last error state
	public:
		CListViewFooter_Wnd(IListView_Ex_Footer& _handler) : m_browser(*this), m_handler(_handler) {
			m_error.Source(_T("CListViewFooter_Wnd"));
		}
	public:
		BEGIN_MSG_MAP(CListViewFooter_Wnd)
			MESSAGE_HANDLER (WM_CREATE ,     OnCreate )
			MESSAGE_HANDLER (WM_DESTROY,     OnDestroy)
			MESSAGE_HANDLER (WM_SIZE   ,     OnSize   )
		END_MSG_MAP()
	protected:
		virtual LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&)
		{
			LRESULT lResult = NO_ERROR;

			m_error.Module(_T(__FUNCTION__));
			m_error = S_OK;

			RECT rc_ = {0};
			TWindow::GetClientRect(&rc_);

			HRESULT hr_ = m_browser.Create(*this, rc_);
			if (FAILED(hr_)) {
				m_error = m_browser.Error();
			}
			else {
				m_browser.Open(m_def_url);
				m_browser.SetInitialized();
			}

			return  lResult;
		}
		virtual LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL& bHandled)
		{
			bHandled = FALSE;
			m_browser.Destroy();

			LRESULT lResult = NO_ERROR;
			return  lResult;
		}
		virtual LRESULT   OnSize    (UINT, WPARAM, LPARAM lParam, BOOL&)
		{
			const RECT area_ = {
				0, 0, LOWORD(lParam), HIWORD(lParam)
			};
			if (m_browser.IsValid()){
				m_browser.GetAxWindowRef().MoveWindow(&area_);
			}
			LRESULT lResult = NO_ERROR;
			return  lResult;
		}
	protected: // IWebBrowserEventHandler
		virtual HRESULT BrowserEvent_BeforeNavigate(LPCTSTR lpszUrl) override
		{
			CAtlString cs_url(lpszUrl);
			HRESULT hr_ = m_handler.IListView_OnLoadBefore(cs_url);
			return  hr_;
		}
		virtual HRESULT BrowserEvent_DocumentComplete(LPCTSTR lpszUrl, const bool bStreamObject) override
		{
			lpszUrl; bStreamObject;
			HRESULT hr_ = m_handler.IListView_OnLoadComplete();
			return  hr_;
		}
		virtual HRESULT BrowserEvent_OnMouseMessage(const MSG&) override
		{
			HRESULT hr_ = S_OK;
			return  hr_;
		}
	public:
		const
		CAxWindow&      Browser(void) const { return m_browser.GetAxWindowRef(); }
		CAxWindow&      Browser(void)       { return m_browser.GetAxWindowRef(); }
		TErrorRef       Error(void)const { return m_error; }
		VOID            URL  (LPCTSTR lpszValue) { m_def_url = lpszValue; }
		LPCTSTR         URL  (void)const { return m_def_url.GetString();  }
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CListViewFooter::CListViewFooter(IListView_Ex_Footer& _evt_handler) : m_evt_handler(_evt_handler), m_handle(NULL)
{
	m_error.Source(_T("CListViewFooter"));
	m_error = S_OK;

	try {
		m_handle = new details::CListViewFooter_Wnd(_evt_handler);
	}
	catch(::std::bad_alloc&){
		m_error = E_OUTOFMEMORY;
	}
}

CListViewFooter::~CListViewFooter(void) {}

/////////////////////////////////////////////////////////////////////////////

const
CAxWindow&    CListViewFooter::Control(void)const
{
	details::CListViewFooter_Wnd* pWnd = reinterpret_cast<details::CListViewFooter_Wnd*>(m_handle);
	if (NULL == pWnd) {
		static CAxWindow na_;
		return na_;
	}

	return pWnd->Browser();
}

CAxWindow&    CListViewFooter::Control(void)
{
	details::CListViewFooter_Wnd* pWnd = reinterpret_cast<details::CListViewFooter_Wnd*>(m_handle);
	if (NULL == pWnd) {
		static CAxWindow na_;
		return na_;
	}

	return pWnd->Browser();
}

HRESULT       CListViewFooter::Create(const HWND hParent, LPCTSTR lpszURL, const RECT& rcArea)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;
	if (::IsRectEmpty(&rcArea))
		return (m_error = OLE_E_INVALIDRECT);

	if (!::IsWindow(hParent))
		return (m_error = OLE_E_INVALIDHWND);

#if defined(__use_xp_hidden_ifaces__)
	CComPtr<IListViewFooter> pLvwFooter;

	if (m_lvw_ctrl.IsWindow() == false)
		return (hr_ = OLE_E_INVALIDHWND);

	m_lvw_ctrl.SendMessage(
			LVM_QUERYINTERFACE, reinterpret_cast<WPARAM>(&IID_IListViewFooter), reinterpret_cast<LPARAM>(&pLvwFooter)
		);

	if (!pLvwFooter)
		return (hr_ = E_NOINTERFACE);

	m_title = lpszTitle;

	pLvwFooter->SetIntroText(lpszTitle);

	CComPtr<IListViewFooterCallback> plvCallback;
	hr_ = pLvwFooter->QueryInterface(IID_IListViewFooterCallback, reinterpret_cast<LPVOID*>(&plvCallback));
	if (FAILED(hr_))
		return hr_;

	hr_ = pLvwFooter->Show(plvCallback);
#else

	if (NULL == m_handle)
		return (m_error = OLE_E_BLANK);

	details::CListViewFooter_Wnd* pWnd = reinterpret_cast<details::CListViewFooter_Wnd*>(m_handle);
	if (NULL == pWnd)
		return (m_error = (DWORD)ERROR_INVALID_DATA);
	else
		pWnd->URL(lpszURL);

	RECT rc_ = rcArea;
	const DWORD dwStyle = WS_CHILD | WS_CLIPCHILDREN | WS_VISIBLE;

	HWND hWnd = pWnd->Create(	
			hParent, rc_, _T("ListViewFooter"), dwStyle
		);
	if (!::IsWindow(hWnd))
		m_error = (DWORD)::GetLastError();

#endif
	return  m_error;
}

HRESULT       CListViewFooter::Destroy(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == m_handle)
		return (m_error = OLE_E_BLANK);

	details::CListViewFooter_Wnd* pWnd = reinterpret_cast<details::CListViewFooter_Wnd*>(m_handle);
	if (NULL == pWnd)
		return (m_error = (DWORD)ERROR_INVALID_DATA);

	if (pWnd->IsWindow())
		pWnd->SendMessage(WM_CLOSE);

	return m_error;
}

TErrorRef     CListViewFooter::Error (void)const { return m_error; }

const
CListViewFooterCtrls& CListViewFooter::Items(void)const { return m_items; }
CListViewFooterCtrls& CListViewFooter::Items(void)      { return m_items; }

bool          CListViewFooter::Visible(void)const
{
	details::CListViewFooter_Wnd* pWnd = reinterpret_cast<details::CListViewFooter_Wnd*>(m_handle);
	if (NULL == pWnd)
		return false;
	else if (!pWnd->IsWindow())
		return false;
	else {
		LONG_PTR lStyle = pWnd->GetWindowLongPtr(GWL_STYLE);
		return (0 != (WS_VISIBLE & lStyle));
	}
}

VOID          CListViewFooter::Visible(const bool _val)
{
	details::CListViewFooter_Wnd* pWnd = reinterpret_cast<details::CListViewFooter_Wnd*>(m_handle);
	if (NULL != pWnd)
		pWnd->ShowWindow(_val ? SW_SHOW : SW_HIDE);
}

HWND          CListViewFooter::Window(void)const
{
	HWND hWnd = NULL;

	if (NULL == m_handle)
		return hWnd;

	details::CListViewFooter_Wnd* pWnd = reinterpret_cast<details::CListViewFooter_Wnd*>(m_handle);
	if (NULL == pWnd)
		return  hWnd;
	else
		hWnd = pWnd->m_hWnd;

	return hWnd;
}