#ifndef _UIXCTRLLIBRARYPRECOMPILEDHEADER_H_6B4C4186_CC8B_45ca_98D7_F7703D25F571_INCLUDED
#define _UIXCTRLLIBRARYPRECOMPILEDHEADER_H_6B4C4186_CC8B_45ca_98D7_F7703D25F571_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Feb-2015 at 7:12:59pm, GMT+3, Taganrog, Monday;
	This is UIX Control library precompiled headers definition file.
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 SP0 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>
#include <comutil.h>

#include <atlapp.h>
#include <atlctrls.h>
#include <atlframe.h>

#if defined(__USE_WIN__XP__compatibility__)
	#include <commctrl.h>
	#include <commoncontrols.h>
#endif

#include <uxtheme.h>
#include <strsafe.h>
#include <propsys.h>
#include <propvarutil.h>

#if defined(__USE_WIN__XP__compatibility__)
	#pragma comment(lib, "strsafe.lib")
	#pragma comment(lib, "uxtheme.lib")
	#pragma comment(lib, "propsys.lib")
#endif

#if (1)

#pragma warning(disable: 4995)  // no deprecate warning(s)!
#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <vector>
#include <map>
#include <time.h>

#else

//
// otherwise, the following warning(s) block a build process:
// warning C4995: 'sprintf': name was marked as #pragma deprecated
// 

#endif

#define LVM_QUERYINTERFACE (LVM_FIRST + 189)

__inline BOOL IsWin7(void)
{
	OSVERSIONINFO ovi = { sizeof(OSVERSIONINFO) };
	if(GetVersionEx(&ovi)) {
		return ((ovi.dwMajorVersion == 6 && ovi.dwMinorVersion >= 1) || ovi.dwMajorVersion > 6);
	}
	return FALSE;
}

#endif/*_UIXCTRLLIBRARYPRECOMPILEDHEADER_H_6B4C4186_CC8B_45ca_98D7_F7703D25F571_INCLUDED*/