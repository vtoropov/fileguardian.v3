#ifndef _SHAREDPRINTVIEW_H_F42567EC_BB07_486a_B2ED_8587B7D77DE9_INCLUDED
#define _SHAREDPRINTVIEW_H_F42567EC_BB07_486a_B2ED_8587B7D77DE9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Apr-2016 at 4:15:13pm, GMT+7, Phuket, Rawai, Wednesday;
	This is Shared simple print library view interface declaration file.
*/
#define WM_PRINTVIEW  (WM_APP + 0x1)

#include "Shared_SystemError.h"

namespace ex_ui { namespace printing
{
	using shared::lite::common::CSysError;

	class CPrintViewMode
	{
	public:
		enum _e{
			eRealSize   = 0x0, // 100%
			ePageWidth  = 0x1,
			eWholePage  = 0x2,
		};
	private:
		_e          m_id;
	public:
		CPrintViewMode(void);
		CPrintViewMode(const CPrintViewMode::_e);
	public:
		_e          Identifier(void)const;
		CAtlString  Title(void)const;
	};

	interface IPrintEventSink
	{
		virtual HRESULT  IPrintEvent_OnClose(void) PURE;
		virtual HRESULT  IPrintEvent_OnError(const CSysError&) PURE;
		virtual HRESULT  IPrintEvent_OnModeChanged(const CPrintViewMode) { return E_NOTIMPL; }
		virtual HRESULT  IPrintEvent_OnPageChanged(const INT nPage, const INT nPages) { nPage; nPages; return E_NOTIMPL; }
		virtual HRESULT  IPrintEvent_OnPagePrint(const INT nPage, const INT nPages) { nPage; nPages; return E_NOTIMPL; }
		virtual HRESULT  IPrintEvent_OnPrint(void) { return E_NOTIMPL; }
		virtual HRESULT  IPrintEvent_OnPrintAborted(void) { return E_NOTIMPL; }
		virtual HRESULT  IPrintEvent_OnPrintComplete(void) { return E_NOTIMPL; }
	};

	interface IPrintDataProvider
	{
		virtual HRESULT  IPrintDataProvider_DocumentName(CAtlString&) PURE;
		virtual HRESULT  IPrintDataProvider_FooterText(CAtlString&) PURE;
		virtual HRESULT  IPrintDataProvider_HeaderText(CAtlString&) PURE;
		virtual HRESULT  IPrintDataProvider_PageData(const INT nRow, const INT nCount, ::std::vector<::std::vector<CAtlString>>&) PURE;
		virtual HRESULT  IPrintDataProvider_RowCount(INT&) PURE;
		virtual HRESULT  IPrintDataProvider_RowData (const INT nRow, ::std::vector<CAtlString>&) { nRow; return E_NOTIMPL; } // deprecated/condemned
		virtual HRESULT  IPrintDataProvider_TableHeader(::std::vector<CAtlString>&) PURE;
	};

	class CPrintView
	{
	private:
		HANDLE           m_wnd;
	public:
		CPrintView(IPrintEventSink&, IPrintDataProvider&);
		~CPrintView(void);
	public:
		HRESULT          Create (const HWND hParent, const RECT& rcArea);
		HRESULT          Destroy(void);
		bool             HasDefaultPrinter(void);
		HRESULT          Hide   (void);
		HRESULT          Show   (void);
		bool             Visible(void)const;
	private:
		CPrintView(const CPrintView&);
		CPrintView& operator= (const CPrintView&);
	};
}}

#endif/*_SHAREDPRINTVIEW_H_F42567EC_BB07_486a_B2ED_8587B7D77DE9_INCLUDED*/