#ifndef _SHAREDPRINTMETRICS_H_9D50BD20_7424_4feb_B62D_8E8A54E501AF_INCLUDED
#define _SHAREDPRINTMETRICS_H_9D50BD20_7424_4feb_B62D_8E8A54E501AF_INCLUDED
/*
	Created by Tech_dog (VToropov) on 7-Apr-2016 at 10:08:18pm, GMT+7, Phuket, Rawai, Thursday;
	This is Shared print library view metrics class(es) declaration file.
*/
#include "Shared_PrintView.h"

namespace ex_ui { namespace printing { namespace _impl
{
	class CDeviceMetrics
	{
	private:
		SIZE            m_dev_res;    // printing device resolution in pixels
		SIZE            m_dev_size;   // printing device page size in millimeters
	public:
		CDeviceMetrics(void);
	public:
		HRESULT         Initialize(const HDC); // initializes data from printing device handle
		const SIZE&     Resolution(void)const; // gets printing device resolution
		const SIZE&     PaperSize(void)const;  // gets printing device paper physical size in millimeters (usually A4 is default)
	};

	class CPreviewMetrics
	{
	private:
		CDeviceMetrics  m_device;     // printing device metrics
		INT             m_line_hight; // average text line height
		SIZE            m_char_size;  // single char cell dimensions
		SIZE            m_page;       // mapped paper size to screen resolution
		INT             m_page_cnt;   // calculated page count
	public:
		CPreviewMetrics(void);
	public:
		const SIZE&     CharSize(void)const;
		const
		CDeviceMetrics& Device(void)const;
		HRESULT         Initialize(const HDC hDevice);
		INT             LineHeight(void)const;
		VOID            LineHeight(const INT);  // is calculated by canvas object
		INT             PageCount(void)const;
		VOID            PageCount(const INT);   // is calculated by canvas object
		const SIZE&     PageSize (void)const;
		VOID            PageSize (const SIZE&); // is calculated by canvas object;
	};

	class CPrintViewModeList
	{
		typedef ::std::vector<CPrintViewMode> TModes;
	private:
		TModes          m_modes;
	public:
		CPrintViewModeList(void);
	public:
		INT             Default(void)const;            // returns the index of default item
		const
		CPrintViewMode& Item(const INT nIndex)const;   // returns a reference to item by an index provided
		INT             Size(void)const;               // returns a size of list
	};

}}}
#endif/*_SHAREDPRINTMETRICS_H_9D50BD20_7424_4feb_B62D_8E8A54E501AF_INCLUDED*/