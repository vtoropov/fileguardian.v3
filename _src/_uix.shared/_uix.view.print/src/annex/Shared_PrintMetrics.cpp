/*
	Created by Tech_dog (VToropov) on 7-Apr-2016 at 10:15:00pm, GMT+7, Phuket, Rawai, Thursday;
	This is Shared print library view metrics class(es) implementation file.
*/
#include "StdAfx.h"
#include "Shared_PrintMetrics.h"

using namespace ex_ui::printing;
using namespace ex_ui::printing::_impl;

#include "UIX_GdiObject.h"

using namespace ex_ui::draw::common;

/////////////////////////////////////////////////////////////////////////////

CDeviceMetrics::CDeviceMetrics(void)
{
	m_dev_res.cx = m_dev_res.cy = m_dev_size.cx = m_dev_size.cy = 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CDeviceMetrics::Initialize(const HDC _dc)
{
	m_dev_res.cx  = ::GetDeviceCaps(_dc, HORZRES );
	m_dev_res.cy  = ::GetDeviceCaps(_dc, VERTRES );
	m_dev_size.cx = ::GetDeviceCaps(_dc, HORZSIZE);
	m_dev_size.cy = ::GetDeviceCaps(_dc, VERTSIZE);

	HRESULT hr_ = S_OK;
	return  hr_;
}

const SIZE& CDeviceMetrics::Resolution(void)const
{
	return m_dev_res;
}

const SIZE& CDeviceMetrics::PaperSize(void)const
{
	return m_dev_size;
}

/////////////////////////////////////////////////////////////////////////////

CPreviewMetrics::CPreviewMetrics(void) : m_line_hight(0), m_page_cnt(0)
{
	m_char_size.cx = m_char_size.cy = m_page.cy = m_page.cx = 0;
}

/////////////////////////////////////////////////////////////////////////////

const SIZE& CPreviewMetrics::CharSize(void)const
{
	return m_char_size;
}

const
CDeviceMetrics& CPreviewMetrics::Device(void)const
{
	return m_device;
}

HRESULT     CPreviewMetrics::Initialize(const HDC hDevice)
{
	const DWORD dwType = ::GetObjectType(hDevice);
	switch (dwType)
	{
	case OBJ_DC:
	case OBJ_MEMDC:
		break;
	default:
		return E_INVALIDARG;
	}

	HRESULT hr_ = m_device.Initialize(hDevice);
	return  hr_;
}

INT         CPreviewMetrics::LineHeight(void)const
{
	return m_line_hight;
}

VOID        CPreviewMetrics::LineHeight(const INT _line)
{
	m_line_hight = _line;
}

INT         CPreviewMetrics::PageCount(void)const
{
	return m_page_cnt;
}

VOID        CPreviewMetrics::PageCount(const INT _pages)
{
	m_page_cnt = _pages;
}

const SIZE& CPreviewMetrics::PageSize(void)const
{
	return m_page;
}

VOID        CPreviewMetrics::PageSize(const SIZE& sz_)
{
	m_page = sz_;
}

/////////////////////////////////////////////////////////////////////////////

CPrintViewModeList::CPrintViewModeList(void)
{
	try
	{
		m_modes.push_back(CPrintViewMode(CPrintViewMode::eRealSize ));
		m_modes.push_back(CPrintViewMode(CPrintViewMode::ePageWidth));
		m_modes.push_back(CPrintViewMode(CPrintViewMode::eWholePage));
	}
	catch(::std::bad_alloc&)
	{
	}
}

INT             CPrintViewModeList::Default(void)const
{
	return 0;
}

const
CPrintViewMode& CPrintViewModeList::Item(const INT nIndex)const
{
	if (-1 < nIndex && nIndex < this->Size())
		return m_modes[nIndex];
	else
	{
		static CPrintViewMode na_;
		return na_;
	}
}

INT             CPrintViewModeList::Size(void)const
{
	return static_cast<INT>(m_modes.size());
}