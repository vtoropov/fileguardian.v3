#ifndef _SHAREDPRINTVIEWWND_H_F016AEA4_F96A_422d_97DE_180DE4C8F8E6_INCLUDED
#define _SHAREDPRINTVIEWWND_H_F016AEA4_F96A_422d_97DE_180DE4C8F8E6_INCLUDED
/*
	Created by Tech_dog (VToropov) on 7-Apr-2016 at 12:42:29pm, GMT+7, Phuket, Rawai, Thursday;
	This is Shared print library view window class declaration file.
*/
#include "Shared_PrintView.h"
#include "Shared_PrintViewPanel.h"
#include "Shared_PrintViewSurface.h"
#include "Shared_PrintMetrics.h"

#include "Shared_SystemError.h"

namespace ex_ui { namespace printing { namespace _impl
{
	using ex_ui::printing::IPrintEventSink;
	using ex_ui::printing::CPrintViewMode;
	using ex_ui::printing::IPrintDataProvider;

	using shared::lite::common::CSysError;

	class CPrintViewWnd :
			public ::ATL::CWindowImpl<CPrintViewWnd>,
			public IPrintEventSink
	{
		typedef  ATL::CWindowImpl<CPrintViewWnd> TWindow;
	private:
		CPrintViewMode      m_mode;
		IPrintEventSink&    m_sink;              // UI sink
		IPrintDataProvider& m_provider;
		CPrintViewPanel     m_panel;
		CPrintViewSurface   m_surface;           // scrollable surface
		CPreviewMetrics     m_metrics;
	public:
		BEGIN_MSG_MAP(CPrintViewWnd)
			MESSAGE_HANDLER(WM_CREATE    ,   OnCreate )
			MESSAGE_HANDLER(WM_DESTROY   ,   OnDestroy)
			MESSAGE_HANDLER(WM_ERASEBKGND,   OnErase  )
		END_MSG_MAP()
	public:
		CPrintViewWnd(IPrintEventSink&, IPrintDataProvider&);
		~CPrintViewWnd(void);
	private: // IPrintEventSink
		HRESULT    IPrintEvent_OnClose(void) override sealed;
		HRESULT    IPrintEvent_OnError(const CSysError&) override sealed;
		HRESULT    IPrintEvent_OnModeChanged(const CPrintViewMode) override sealed;
		HRESULT    IPrintEvent_OnPageChanged(const INT nPage, const INT nPages) override sealed;
		HRESULT    IPrintEvent_OnPrint(void) override sealed;
	private:
		LRESULT    OnCreate (UINT, WPARAM, LPARAM, BOOL&);
		LRESULT    OnDestroy(UINT, WPARAM, LPARAM, BOOL&);
		LRESULT    OnErase  (UINT, WPARAM, LPARAM, BOOL&);
	public:
		HRESULT    Initialize(HDC hDevice);
		HRESULT    DoPreview(void);
		HRESULT    DoPrint(const HDC hPrinter);
	};
}}}

#endif/*_SHAREDPRINTVIEWWND_H_F016AEA4_F96A_422d_97DE_180DE4C8F8E6_INCLUDED*/