/*
	Created by Tech_dog (VToropov) on 6-Apr-2016 at 4:29:00pm, GMT+7, Phuket, Rawai, Wednesday;
	This is Shared print library view class implementation file.
*/
#include "StdAfx.h"
#include "Shared_PrintView.h"

using namespace ex_ui::printing;

#include "Shared_PrintViewWnd.h"

using namespace ex_ui::printing::_impl;

/////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace printing { namespace details
{
}}}

/////////////////////////////////////////////////////////////////////////////

CPrintViewMode::CPrintViewMode(void) : m_id(CPrintViewMode::eWholePage)
{
}

CPrintViewMode::CPrintViewMode(const CPrintViewMode::_e _id) : m_id(_id)
{
}

/////////////////////////////////////////////////////////////////////////////

CPrintViewMode::_e CPrintViewMode::Identifier(void)const
{
	return m_id;
}

CAtlString         CPrintViewMode::Title(void)const
{
	switch (m_id)
	{
	case CPrintViewMode::ePageWidth: return CAtlString(_T("Page Width"));
	case CPrintViewMode::eRealSize : return CAtlString(_T("Real Size (100%)"));
	case CPrintViewMode::eWholePage: return CAtlString(_T("Whole Page"));
	}
	return CAtlString(_T("#na"));
}

/////////////////////////////////////////////////////////////////////////////

#define _wnd_ref(_pvoid) (*(reinterpret_cast<CPrintViewWnd*>(_pvoid)))

/////////////////////////////////////////////////////////////////////////////

CPrintView::CPrintView(IPrintEventSink& _sink, IPrintDataProvider& _provider) : m_wnd(NULL)
{
	try
	{
		m_wnd = new CPrintViewWnd(_sink, _provider);
	}
	catch (::std::bad_alloc&){}
}

CPrintView::~CPrintView(void)
{
	if (m_wnd)
	{
		try
		{
			delete m_wnd;
		} catch(...){}
		m_wnd = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CPrintView::Create (const HWND hParent, const RECT& rcArea)
{
	if (!m_wnd)
		return OLE_E_BLANK;
	if (_wnd_ref(m_wnd))
		return HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED);

	CWindow parent_(hParent);

	if (!parent_)
		return OLE_E_INVALIDHWND;
	if (::IsRectEmpty(&rcArea))
		return OLE_E_INVALIDRECT;

	RECT rc_ = rcArea;

	_wnd_ref(m_wnd).Create(
			hParent,
			rc_,
			_T("PrintView"),
			WS_CHILD|WS_CLIPCHILDREN
		);
	if (!_wnd_ref(m_wnd))
		return HRESULT_FROM_WIN32(::GetLastError());
	else
		return S_OK;
}

HRESULT    CPrintView::Destroy(void)
{
	if (!m_wnd)
		return OLE_E_BLANK;
	if (!_wnd_ref(m_wnd))
		return S_OK;
	_wnd_ref(m_wnd).SendMessage(WM_CLOSE);
	return S_OK;
}

bool       CPrintView::HasDefaultPrinter(void)
{
	if (!m_wnd)
		return false;
	::WTL::CPrintDialog dlg_;

	const BOOL bResult = dlg_.GetDefaults();
	if (bResult)
	{
		_wnd_ref(m_wnd).Initialize(dlg_.m_pdActual.hDC);
		::DeleteDC(dlg_.m_pdActual.hDC);
		if (dlg_.m_pdActual.hDevMode)
			::GlobalFree(dlg_.m_pdActual.hDevMode);
		if (dlg_.m_pdActual.hDevNames)
			::GlobalFree(dlg_.m_pdActual.hDevNames);
	}

	return !!bResult;
}

HRESULT    CPrintView::Hide(void)
{
	if (!m_wnd)
		return OLE_E_BLANK;
	if (!_wnd_ref(m_wnd))
		return OLE_E_BLANK;

	_wnd_ref(m_wnd).SetWindowPos(HWND_BOTTOM, 0, 0, 0, 0, SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW);
	return S_OK;
}

HRESULT    CPrintView::Show(void)
{
	if (!m_wnd)
		return OLE_E_BLANK;
	if (!_wnd_ref(m_wnd))
		return OLE_E_BLANK;

	_wnd_ref(m_wnd).DoPreview();
	return S_OK;
}

bool       CPrintView::Visible(void)const
{
	if (!m_wnd)
		return false;
	if (!_wnd_ref(m_wnd))
		return false;

	return !!_wnd_ref(m_wnd).IsWindowVisible();
}