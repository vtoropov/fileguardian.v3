/*
	Created by Tech_dog (VToropov) on 7-Apr-2016 at 4:51:39pm, GMT+7, Phuket, Rawai, Thursday;
	This is Shared print library view control panel class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 9:52:52a, UTC+7, Phuket, Rawai, Thursday;
*/
#include "StdAfx.h"
#include "Shared_PrintViewPanel.h"
#include "Shared_PrintMetrics.h"

using namespace ex_ui::printing::_impl;

#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace printing { namespace _impl { namespace details
{
	class CPrintViewPanel_CtrlId
	{
	public:
		enum _e{
			ePrintButton    = 0x1,
			eNextButton     = 0x2,
			ePrevButton     = 0x3,
			eCloseButton    = 0x4,
			eModeList       = 0xf
		};
	};

	class CPrintViewPanel_Layout
	{
	private:
		enum _e{
			ctl_gap_h   =   3,
			ctl_gap_w   =   4,
			def_btn_h   =  25,
			def_btn_w   = 100,
			cbo_mod_w   = 130,
		};
	private:
		CWindow&    m_panel;
		RECT        m_client_area;
	public:
		CPrintViewPanel_Layout(CWindow& _panel):m_panel(_panel)
		{
			if (!m_panel)
				::SetRectEmpty(&m_client_area);
			else
				m_panel.GetClientRect(&m_client_area);
		}
	public:
		VOID        AdjustCombo(CWindow& _cbo, const INT _height)
		{
			if (!_cbo)
				return;

			static const INT _COMBOBOX_EDIT_MARGIN = 3;

			const INT pixHeight = _height - _COMBOBOX_EDIT_MARGIN * 2;
			LRESULT res__ = ERROR_SUCCESS;
			res__ = _cbo.SendMessage(CB_SETITEMHEIGHT, (WPARAM)-1, pixHeight); ATLASSERT(res__ != CB_ERR);
			res__ = _cbo.SendMessage(CB_SETITEMHEIGHT, (WPARAM) 0, pixHeight); ATLASSERT(res__ != CB_ERR);
			{
				POINT pt_ = {5, 5};
				CWindow edt_ = _cbo.ChildWindowFromPoint(pt_);
				if (edt_)
				{
					RECT rc_ = {0};
					edt_.GetWindowRect(&rc_);
					::MapWindowPoints(HWND_DESKTOP, _cbo, (LPPOINT)&rc_, 0x2);
				}
			}
		}

		RECT        GetButtonRect(const INT nIndex)const
		{
			const INT left_ = CPrintViewPanel_Layout::ctl_gap_w   * 2
			                + CPrintViewPanel_Layout::def_btn_w   * nIndex
							+ CPrintViewPanel_Layout::ctl_gap_w   * nIndex
			                + CPrintViewPanel_Layout::cbo_mod_w   *(nIndex > 2 ? 1 : 0);
			const RECT rc_ = {
				left_,
				CPrintViewPanel_Layout::ctl_gap_h,
				CPrintViewPanel_Layout::def_btn_w + left_,
				CPrintViewPanel_Layout::ctl_gap_h + CPrintViewPanel_Layout::def_btn_h
			};
			return rc_;
		}

		RECT        GetComboRect(void)const
		{
			RECT rc_   = this->GetButtonRect(2);
			rc_.left   = CPrintViewPanel_Layout::ctl_gap_w   + rc_.right;
			rc_.right  = CPrintViewPanel_Layout::cbo_mod_w   + rc_.left  - CPrintViewPanel_Layout::ctl_gap_w;

			::OffsetRect(&rc_, 0, 1);

			return rc_;
		}
	public:
		static SIZE GetDefaultSize(void)
		{
			const SIZE sz_ = {650,
				CPrintViewPanel_Layout::def_btn_h + CPrintViewPanel_Layout::ctl_gap_h * 2};
			return sz_;
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CPrintViewPanel::CPrintViewPanelWnd::CPrintViewPanelWnd(IPrintEventSink& _sink) : m_sink(_sink), m_current(0), m_pages(0)
{
}

CPrintViewPanel::CPrintViewPanelWnd::~CPrintViewPanelWnd(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPrintViewPanel::CPrintViewPanelWnd::OnCommand(UINT, WPARAM wParam, LPARAM, BOOL& bResult)
{
	bResult = TRUE;
	const WORD ctrlId  = LOWORD(wParam);
	const WORD wCode   = HIWORD(wParam);
	switch (ctrlId)
	{
	case details::CPrintViewPanel_CtrlId::ePrintButton:
		{
			m_sink.IPrintEvent_OnPrint();
		} break;
	case details::CPrintViewPanel_CtrlId::eCloseButton:
		{
			m_sink.IPrintEvent_OnClose();
		} break;
	case details::CPrintViewPanel_CtrlId::eNextButton:
		{
			if (m_current < m_pages)
				m_current+= 1;
			m_prev.EnableWindow(TRUE);
			if (m_current == m_pages)
				m_next.EnableWindow(FALSE);
			m_sink.IPrintEvent_OnPageChanged(m_current, m_pages);
		} break;
	case details::CPrintViewPanel_CtrlId::ePrevButton:
		{
			if (m_current > 1)
				m_current-= 1;
			m_next.EnableWindow(TRUE);
			if (m_current == 1)
				m_prev.EnableWindow(FALSE);
			m_sink.IPrintEvent_OnPageChanged(m_current, m_pages);
		} break;
	case details::CPrintViewPanel_CtrlId::eModeList:
		{
			if (CBN_SELCHANGE == wCode)
			{
				::WTL::CComboBox cbo_ = (HWND)TWindow::GetDlgItem(ctrlId);
				if (cbo_)
				{
					CPrintViewModeList lst_;
					CPrintViewMode current_ = lst_.Item(cbo_.GetCurSel());

					m_sink.IPrintEvent_OnModeChanged(current_);
				}
			}
		} break;
	default:
		bResult = FALSE;
	}
	return 0;
}

LRESULT    CPrintViewPanel::CPrintViewPanelWnd::OnCreate (UINT, WPARAM, LPARAM, BOOL& bResult)
{
	bResult = TRUE;
	details::CPrintViewPanel_Layout layout_(*this);

	const HFONT hFont = TWindow::GetTopLevelWindow().GetFont();
	TWindow::SetFont(hFont);

	::WTL::CButton* btn_[] = {
		&m_print,
		&m_next ,
		&m_prev ,
		&m_close
	};
	LPCTSTR lpszTitle[] = {
		_T("Print"),
		_T("Next Page"),
		_T("Prev Page"),
		_T("Close")
	};
	const UINT ctrlId[] = {
		details::CPrintViewPanel_CtrlId::ePrintButton,
		details::CPrintViewPanel_CtrlId::eNextButton ,
		details::CPrintViewPanel_CtrlId::ePrevButton ,
		details::CPrintViewPanel_CtrlId::eCloseButton,
	};

	for (INT i_ = 0; i_ < _countof(btn_) && i_ < _countof(lpszTitle); i_++)
	{
		RECT rc_ = layout_.GetButtonRect(i_);
		btn_[i_]->Create(
			    TWindow::m_hWnd,
				rc_,
				lpszTitle[i_],
				WS_CHILD|WS_VISIBLE,
				0,
				(UINT)(ctrlId[i_])
			);
		if ((*btn_[i_]))
			(*btn_[i_]).SetFont(hFont);
		if (i_)
			(*btn_[i_]).SetWindowPos((*btn_[i_ - 1]), 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE);
	}

	{
		RECT rc_ = layout_.GetComboRect();
		m_modes.Create(
				TWindow::m_hWnd,
				rc_,
				NULL,
				WS_CHILD|WS_VISIBLE|CBS_DROPDOWNLIST,
				0,
				(UINT)0xf
			);
		if (m_modes)
		{
			m_modes.SetFont(hFont);

			CPrintViewModeList lst_;
			for (INT i_ = 0; i_ < lst_.Size(); i_++)
				m_modes.AddString(lst_.Item(i_).Title());
			m_modes.SetCurSel(lst_.Default());

			layout_.AdjustCombo(m_modes, 23);
		}
	}

	return 0;
}

LRESULT    CPrintViewPanel::CPrintViewPanelWnd::OnDestroy(UINT, WPARAM, LPARAM, BOOL& bResult)
{
	bResult = FALSE;
	::WTL::CButton* btn_[] = {
		&m_print,
		&m_next ,
		&m_prev ,
		&m_close
	};
	for (INT i_ = 0; i_ < _countof(btn_); i_++)
		btn_[i_]->SendMessage(WM_CLOSE);
	return 0;
}

LRESULT    CPrintViewPanel::CPrintViewPanelWnd::OnErase  (UINT, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	bHandled = TRUE;
	HDC hDC  = (HDC)wParam;
	RECT rc_ = {0};
	TWindow::GetClientRect(&rc_);
	CZBuffer dc_(hDC, rc_);

	dc_.DrawSolidRect(
			rc_,
			::GetSysColor(COLOR_BTNFACE),
			eAlphaValue::eOpaque
		);
	dc_.DrawLine(
			rc_.left     ,
			rc_.bottom -1,
			rc_.right    ,
			rc_.bottom -1,
			::GetSysColor(COLOR_3DDKSHADOW),
			1
		);

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

CPrintViewPanel::CPrintViewPanel(IPrintEventSink& _sink) : m_wnd(_sink)
{
}

CPrintViewPanel::~CPrintViewPanel(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CPrintViewPanel::Create (const HWND hParent, const RECT& rcArea)
{
	if (m_wnd)
		return HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED);
	if (!CWindow(hParent))
		return OLE_E_INVALIDHWND;
	if (::IsRectEmpty(&rcArea))
		return OLE_E_INVALIDRECT;

	RECT rc_ = rcArea;

	m_wnd.Create(
			hParent,
			rc_,
			_T("PrintViewPanel"),
			WS_CHILD|WS_CLIPCHILDREN|WS_VISIBLE
		);
	if (!m_wnd)
		return HRESULT_FROM_WIN32(::GetLastError());
	else
		return S_OK;
}

HRESULT    CPrintViewPanel::Destroy(void)
{
	if (!m_wnd)
		return S_OK;
	m_wnd.SendMessage(WM_CLOSE);
	return S_OK;
}

HRESULT    CPrintViewPanel::UpdateState(const CPreviewMetrics& _metrics)
{
	if (!m_wnd)
		return OLE_E_BLANK;
	m_wnd.m_pages = _metrics.PageCount();
	m_wnd.m_current = (m_wnd.m_pages > 0 ? 1 : 0);

	if (m_wnd.m_prev) m_wnd.m_prev.EnableWindow(FALSE);
	if (m_wnd.m_next) m_wnd.m_next.EnableWindow(m_wnd.m_pages > 1);

	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

SIZE       CPrintViewPanel::GetDefaultSize(void)
{
	return details::CPrintViewPanel_Layout::GetDefaultSize();
}