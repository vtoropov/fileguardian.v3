#ifndef _SHAREDPRINTVIEWPANEL_H_0A5A95DD_47FB_4b9a_8062_4CD97AF18B88_INCLUDED
#define _SHAREDPRINTVIEWPANEL_H_0A5A95DD_47FB_4b9a_8062_4CD97AF18B88_INCLUDED
/*
	Created by Tech_dog (VToropov) on 7-Apr-2016 at 4:39:24pm, GMT+7, Phuket, Rawai, Thursday;
	This is Shared print library view control panel class declaration file.
*/
#include "Shared_PrintView.h"
#include "Shared_PrintMetrics.h"

namespace ex_ui { namespace printing { namespace _impl
{
	using ex_ui::printing::IPrintEventSink;

	class CPrintViewPanel
	{
	private:
		class CPrintViewPanelWnd:
			public ::ATL::CWindowImpl<CPrintViewPanelWnd>
		{
			friend class CPrintViewPanel;
			typedef  ATL::CWindowImpl<CPrintViewPanelWnd> TWindow;
		private:
			IPrintEventSink&  m_sink;
			::WTL::CButton    m_print;
			::WTL::CButton    m_close;
			::WTL::CComboBox  m_modes;
			::WTL::CButton    m_prev;
			::WTL::CButton    m_next;
			INT               m_current;   // current page index (1-based)
			INT               m_pages;     // total number of pages
		public:
			DECLARE_WND_CLASS_EX(NULL, CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS, COLOR_BTNFACE);

			BEGIN_MSG_MAP(CPrintViewPanelWnd)
				MESSAGE_HANDLER(WM_CREATE    ,   OnCreate )
				MESSAGE_HANDLER(WM_DESTROY   ,   OnDestroy)
				MESSAGE_HANDLER(WM_COMMAND   ,   OnCommand)
				MESSAGE_HANDLER(WM_ERASEBKGND,   OnErase  )
			END_MSG_MAP()
		public:
			CPrintViewPanelWnd(IPrintEventSink&);
			~CPrintViewPanelWnd(void);
		private:
			LRESULT    OnCommand(UINT, WPARAM, LPARAM, BOOL&);
			LRESULT    OnCreate (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT    OnDestroy(UINT, WPARAM, LPARAM, BOOL&);
			LRESULT    OnErase  (UINT, WPARAM, LPARAM, BOOL&);
		};
	private:
		CPrintViewPanelWnd  m_wnd;
	public:
		CPrintViewPanel(IPrintEventSink&);
		~CPrintViewPanel(void);
	public:
		HRESULT        Create (const HWND hParent, const RECT& rcArea);
		HRESULT        Destroy(void);
		HRESULT        UpdateState(const CPreviewMetrics&);
	public:
		static SIZE    GetDefaultSize(void);
	};

}}}
#endif/*_SHAREDPRINTVIEWPANEL_H_0A5A95DD_47FB_4b9a_8062_4CD97AF18B88_INCLUDED*/