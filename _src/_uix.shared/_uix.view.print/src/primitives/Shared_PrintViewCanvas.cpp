/*
	Created by Tech_dog (VToropov) on 8-Apr-2016 at 5:10:44pm, GMT+7, Phuket, Rawai, Friday;
	This is Shared print library view draw canvas class implementation file.
*/
#include "StdAfx.h"
#include "Shared_PrintViewCanvas.h"

using namespace ex_ui::printing::_impl;

#include "UIX_GdiProvider.h"
#include "UIX_GdiObject.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace printing { namespace _impl { namespace details
{
}}}}

/////////////////////////////////////////////////////////////////////////////

CPrintViewCanvas::CPrintViewCanvasWnd::CPrintViewCanvasWnd(IPrintDataProvider& _provider) :
	m_provider(_provider),
	m_render  (_provider),
	m_page_no (1),
	m_mode(CPrintViewMode::eWholePage)
{
	m_page_sz.cx = m_page_sz.cy = 0;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPrintViewCanvas::CPrintViewCanvasWnd::OnCreate (UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = TRUE;
	return 0;
}

LRESULT    CPrintViewCanvas::CPrintViewCanvasWnd::OnDestroy(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	return 0;
}

LRESULT    CPrintViewCanvas::CPrintViewCanvasWnd::OnErase  (UINT, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	bHandled = TRUE;
	HDC hDC  = (HDC)wParam;
	RECT rc_ = {0};
	TWindow::GetClientRect(&rc_);

	CZBuffer dc_(hDC, rc_);

	dc_.DrawSolidRect(
			rc_,
			RGB(0xff, 0xff, 0xff)
		);
	dc_.DrawRectangle(
			rc_,
			RGB(0x10, 0x10, 0x10)
		);

	m_render.SetPageNumber(m_page_no);

	if (CPrintViewMode::eRealSize == m_mode)
	{
		m_render.DrawHeader(dc_, rc_);
		m_render.DrawFooter(dc_, rc_);
		m_render.DrawTable (dc_, rc_);
	}
	else
	{
		RECT rcPage = {
			0,
			0,
			m_page_sz.cx,
			m_page_sz.cy
		};
		CZBuffer buf_(hDC, rcPage);
		m_render.DrawBkgnd (buf_, rcPage);
		m_render.DrawHeader(buf_, rcPage);
		m_render.DrawFooter(buf_, rcPage);
		m_render.DrawTable (buf_, rcPage);

		::InflateRect(&rc_, -0x1, -0x1); // excludes canvas borders

		::StretchBlt(
				dc_,
				1,
				1,
				__W(rc_),
				__H(rc_),
				buf_,
				0,
				0,
				m_page_sz.cx,
				m_page_sz.cy,
				SRCCOPY
			);
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

CPrintViewCanvas::CPrintViewCanvas(IPrintDataProvider& _provider) : m_wnd(_provider)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CPrintViewCanvas::Create (const HWND hParent, const RECT& rcArea)
{
	if (m_wnd)
		return HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED);
	if (!CWindow(hParent))
		return OLE_E_INVALIDHWND;
	if (::IsRectEmpty(&rcArea))
		return OLE_E_INVALIDRECT;

	RECT rc_ = rcArea;

	m_wnd.Create(
			hParent,
			rc_,
			_T("PrintViewCanvas"),
			WS_CHILD|WS_CLIPCHILDREN|WS_VISIBLE
		);
	if (!m_wnd)
		return HRESULT_FROM_WIN32(::GetLastError());
	else
		return S_OK;
}

HRESULT    CPrintViewCanvas::Destroy(void)
{
	if (!m_wnd)
		return S_OK;
	m_wnd.SendMessage(WM_CLOSE);
	return S_OK;
}

HRESULT    CPrintViewCanvas::DrawPage(const INT nPage)
{
	if (!m_wnd)
		return OLE_E_BLANK;
	if (m_wnd.m_page_no != nPage )
	{
		m_wnd.m_page_no  = nPage;
		m_wnd.Invalidate();
	}
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT    CPrintViewCanvas::UpdateLayout(CPreviewMetrics& _metrics, const CPrintViewMode& _mode)
{
	if (!m_wnd)
		return OLE_E_BLANK;
	{
		HDC canvas_ = m_wnd.GetDC();                 ::SelectClipRgn(canvas_, (HRGN)NULLREGION);
		HDC compat_ = ::CreateCompatibleDC(canvas_); ::SelectClipRgn(compat_, (HRGN)NULLREGION);

		::SetMapMode(compat_, MM_TEXT);
		m_wnd.m_page_sz.cx = _metrics.Device().PaperSize().cx * ::GetDeviceCaps(canvas_, LOGPIXELSX) / 25;
		m_wnd.m_page_sz.cy = _metrics.Device().PaperSize().cy * ::GetDeviceCaps(canvas_, LOGPIXELSY) / 25;

		_metrics.PageSize(m_wnd.m_page_sz);

		INT line_  = 16;
		INT pages_ = 1;
		m_wnd.m_render.CountPages(compat_, m_wnd.m_page_sz, line_, pages_);

		_metrics.PageCount(pages_);
		_metrics.LineHeight(line_);

		m_wnd.ReleaseDC(canvas_);
		::DeleteDC(compat_);

		m_wnd.m_mode = _mode.Identifier();
	}
	return S_OK;
}

CWindow    CPrintViewCanvas::Window (void)const
{
	return m_wnd;
}