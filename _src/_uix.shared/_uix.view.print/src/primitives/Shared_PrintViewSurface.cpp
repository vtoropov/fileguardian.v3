/*
	Created by Tech_dog (VToropov) on 8-Apr-2016 at 4:30:42pm, GMT+7, Phuket, Rawai, Friday;
	This is Shared print library view scrollable surface class implementation file.
*/
#include "StdAfx.h"
#include "Shared_PrintViewSurface.h"

using namespace ex_ui::printing;
using namespace ex_ui::printing::_impl;

#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

 namespace ex_ui { namespace printing { namespace _impl { namespace details
{
	class CPrintViewSurface_Layout
	{
	private:
		CWindow& m_surface;
		RECT     m_client_area;
	public:
		CPrintViewSurface_Layout(CWindow& _surface) : m_surface(_surface)
		{
			if (!m_surface)
				::SetRectEmpty(&m_client_area);
			else
				m_surface.GetClientRect(&m_client_area);
		}
	public:
		RECT     GetCanvasRect(const CPrintViewMode& _mode, const CPreviewMetrics& _metrics)const
		{
			RECT rc_ = {0};
			RECT rc_client = m_client_area;

			const INT scroll_ = ::GetSystemMetrics(SM_CXVSCROLL);

			switch (_mode.Identifier())
			{
			case CPrintViewMode::ePageWidth:
				{
					::InflateRect(&rc_client, -scroll_, 0);

					const SIZE res_ = _metrics.Device().Resolution();
					const FLOAT factor_  = FLOAT(__W(rc_client)) / FLOAT(res_.cx);

					const SIZE szPage = {
							static_cast<LONG>(FLOAT(res_.cx) * factor_),
							static_cast<LONG>(FLOAT(res_.cy) * factor_)
						};

					const INT left_ = rc_client.left + (__W(rc_client) - szPage.cx) / 2;
					const INT  top_ = rc_client.top  + 0x4; // initially the page is scrolled up
					::SetRect(
							&rc_ ,
							left_,
							top_ ,
							left_ + szPage.cx,
							top_  + szPage.cy
						);

				} break;
			case CPrintViewMode::eWholePage:
				{
					const SIZE res_ = _metrics.Device().Resolution();
					const FLOAT factor_v = FLOAT(__H(rc_client)) / FLOAT(res_.cy);
					const FLOAT factor_h = FLOAT(__W(rc_client)) / FLOAT(res_.cx);
					const FLOAT factor_  = min(factor_h, factor_v);

					const SIZE szPage = {
							static_cast<LONG>(FLOAT(res_.cx) * factor_),
							static_cast<LONG>(FLOAT(res_.cy) * factor_)
						};
					const INT left_ = rc_client.left + (__W(rc_client) - szPage.cx) / 2;
					const INT  top_ = rc_client.top  + (__H(rc_client) - szPage.cy) / 2;
					::SetRect(
							&rc_ ,
							left_,
							top_ ,
							left_ + szPage.cx,
							top_  + szPage.cy
						);
					::InflateRect(&rc_, -0x4, -0x4);
				} break;
			case CPrintViewMode::eRealSize:
				{
					const SIZE page_ = _metrics.PageSize();
					const INT left_  = 4; // left margin
					const INT  top_  = 4; // top margin
					::SetRect(
							&rc_ ,
							left_,
							top_ ,
							left_ + page_.cx,
							top_  + page_.cy
						);
				} break;
			}
			return rc_;
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CPrintViewSurface::CPrintViewSurfaceWnd::CPrintViewSurfaceWnd(IPrintDataProvider& _provider) :
	m_mode(CPrintViewMode::eWholePage),
	m_canvas(_provider)
{
	HDC hDC = ::GetDC(NULL);
	{
		m_metrics.Initialize(hDC);
		::ReleaseDC(NULL, hDC);
	}
	hDC = NULL;
	m_anchor.cx = m_anchor.cy = 0;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPrintViewSurface::CPrintViewSurfaceWnd::OnCreate (UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = TRUE;
	details::CPrintViewSurface_Layout layout_(*this);

	HRESULT hr_ = S_OK;
	{
		const RECT rc_ = layout_.GetCanvasRect(m_mode, m_metrics);
		hr_ = m_canvas.Create(
				*this,
				rc_
			);
		ATLASSERT(SUCCEEDED(hr_));
	}
	return 0;
}

LRESULT    CPrintViewSurface::CPrintViewSurfaceWnd::OnDestroy(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	m_canvas.Destroy();
	bHandled = FALSE;
	return 0;
}

LRESULT    CPrintViewSurface::CPrintViewSurfaceWnd::OnErase  (UINT, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	bHandled = TRUE;
	HDC hDC  = (HDC)wParam;
	RECT rc_ = {0};
	TWindow::GetClientRect(&rc_);

	CZBuffer dc_(hDC, rc_);

	dc_.DrawSolidRect(
			rc_,
			::GetSysColor(COLOR_APPWORKSPACE),
			eAlphaValue::eOpaque
		);
	return TRUE;
}

LRESULT    CPrintViewSurface::CPrintViewSurfaceWnd::OnHScroll(UINT, WPARAM wParam, LPARAM, BOOL&)
{
	details::CPrintViewSurface_Layout layout_(*this);
	RECT rc_ = layout_.GetCanvasRect(
					m_mode,
					m_metrics
				);
	INT pos_ = TWindow::GetScrollPos(SB_HORZ);

	const WORD nCode = LOWORD(wParam);
	switch (nCode)
	{
	case SB_LEFT         : pos_  = 0;      break;
	case SB_RIGHT        : pos_  = 100;    break;
	case SB_LINELEFT     : pos_ -= 1;      break;
	case SB_LINERIGHT    : pos_ += 1;      break;
	case SB_PAGELEFT     : pos_ -= 50;     break;
	case SB_PAGERIGHT    : pos_ += 50;     break;
	case SB_THUMBTRACK   : pos_  = HIWORD(wParam); break;
	}

	if (pos_ < 0)
		pos_ = 0;
	if (pos_ > 100)
		pos_ = 100;

	TWindow::SetScrollPos(SB_HORZ, pos_);

	RECT client_ = {0};
	TWindow::GetClientRect(&client_);

	m_anchor.cx = ((__W(rc_) - __W(client_) + 8) * pos_) / 100;

	::OffsetRect(&rc_, -(m_anchor.cx), -(m_anchor.cy));
	m_canvas.Window().MoveWindow(&rc_, TRUE);

	return 0;
}

LRESULT    CPrintViewSurface::CPrintViewSurfaceWnd::OnVScroll(UINT, WPARAM wParam, LPARAM, BOOL&)
{
	details::CPrintViewSurface_Layout layout_(*this);
	RECT rc_ = layout_.GetCanvasRect(
					m_mode,
					m_metrics
				);
	INT pos_ = TWindow::GetScrollPos(SB_VERT);

	const WORD nCode = LOWORD(wParam);
	switch (nCode)
	{
	case SB_TOP          : pos_  = 0;      break; // [home] keyboard key
	case SB_BOTTOM       : pos_  = 100;    break; // [end]  keyboard key
	case SB_LINEUP       : pos_ -= 1;      break; // scroll bar top arrow
	case SB_LINEDOWN     : pos_ += 1;      break; // scroll bar bottom arrow
	case SB_PAGEUP       : pos_ -= 50;     break; // user clicked the scroll bar shaft above the scroll box
	case SB_PAGEDOWN     : pos_ += 50;     break; // user clicked the scroll bar shaft below the scroll box
	case SB_THUMBTRACK   : pos_  = HIWORD(wParam); break; // user dragged the scroll box
	}

	if (pos_ < 0)
		pos_ = 0;
	if (pos_ > 100)
		pos_ = 100;

	TWindow::SetScrollPos(SB_VERT, pos_);

	RECT client_ = {0};
	TWindow::GetClientRect(&client_);

	m_anchor.cy = ((__H(rc_) - __H(client_) + 8) * pos_) / 100;

	::OffsetRect(&rc_, -(m_anchor.cx), -(m_anchor.cy));
	m_canvas.Window().MoveWindow(&rc_, TRUE);

	return 0;
}

LRESULT    CPrintViewSurface::CPrintViewSurfaceWnd::OnWheel  (UINT, WPARAM, LPARAM lParam, BOOL&) {

	const PMOUSEHOOKSTRUCTEX pData = reinterpret_cast<PMOUSEHOOKSTRUCTEX>(lParam);
	if (NULL != pData) {

		details::CPrintViewSurface_Layout layout_(*this);

		INT pos_ = TWindow::GetScrollPos(SB_VERT);
		RECT rc_ = layout_.GetCanvasRect(
			m_mode,
			m_metrics
		);

		const SHORT nDelta = static_cast<SHORT>(HIWORD(pData->mouseData));
		const static INT n_step = 0x3;

		if (0 > nDelta) pos_ += n_step;
		else            pos_ -= n_step;
		if (pos_ < 0x0) pos_ = 0x0;
		if (pos_ > 100) pos_ = 100;

		TWindow::SetScrollPos(SB_VERT, pos_);

		RECT client_ = {0};
		TWindow::GetClientRect(&client_);

		m_anchor.cy = ((__H(rc_) - __H(client_) + 8) * pos_) / 100;

		::OffsetRect(&rc_, -(m_anchor.cx), -(m_anchor.cy));
		m_canvas.Window().MoveWindow(&rc_, TRUE);
	}

	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CPrintViewSurface::CPrintViewSurface(IPrintDataProvider& _provider) : m_wnd(_provider)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT        CPrintViewSurface::Create (const HWND hParent, const RECT& rcArea)
{
	if (m_wnd)
		return HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED);
	if (!CWindow(hParent))
		return OLE_E_INVALIDHWND;
	if (::IsRectEmpty(&rcArea))
		return OLE_E_INVALIDRECT;

	RECT rc_ = rcArea;

	m_wnd.Create(
			hParent,
			rc_,
			_T("PrintViewSurface"),
			WS_CHILD|WS_CLIPCHILDREN|WS_VISIBLE|WS_VSCROLL|WS_HSCROLL
		);
	if (!m_wnd)
		return HRESULT_FROM_WIN32(::GetLastError());
	else {
		CMouseEvtHandler::GetObjectRef().Subscribe(m_wnd);
		return S_OK;
	}
}

HRESULT        CPrintViewSurface::Destroy(void)
{
	if (!m_wnd)
		return S_OK;
	CMouseEvtHandler::GetObjectRef().Unsubscribe(m_wnd);
	m_wnd.SendMessage(WM_CLOSE);
	return S_OK;
}

HRESULT        CPrintViewSurface::ShowPage(const INT nPage)
{
	if (!m_wnd)
		return OLE_E_BLANK;
	HRESULT hr_ = m_wnd.m_canvas.DrawPage(nPage);
	return  hr_;
}

HRESULT        CPrintViewSurface::UpdateLayout(CPreviewMetrics& _metrics, const CPrintViewMode& _mode)
{
	if (!m_wnd)
		return OLE_E_BLANK;

	m_wnd.m_metrics = _metrics;
	m_wnd.m_mode = _mode;

	m_wnd.m_canvas.UpdateLayout(m_wnd.m_metrics, m_wnd.m_mode);
	_metrics = m_wnd.m_metrics;

	SCROLLINFO si_ = {0};
	si_.cbSize = sizeof(si_);
	si_.fMask  = SIF_ALL;
	si_.nMax   = 150;
	si_.nPage  =  50;

	LONG_PTR lpStyle = m_wnd.GetWindowLongPtrW(GWL_STYLE);

	switch (_mode.Identifier())
	{
	case CPrintViewMode::ePageWidth:
		{
			lpStyle &= ~WS_HSCROLL;
			lpStyle |=  WS_VSCROLL;
		} break;
	case CPrintViewMode::eRealSize :
		{
			lpStyle |=  WS_HSCROLL;
			lpStyle |=  WS_VSCROLL;
		} break;
	case CPrintViewMode::eWholePage:
		{
			lpStyle &= ~WS_HSCROLL;
			lpStyle &= ~WS_VSCROLL;
		} break;
	}

	m_wnd.SetWindowLongPtr(GWL_STYLE, lpStyle);
	m_wnd.SetWindowPos (NULL, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE|SWP_FRAMECHANGED);
	m_wnd.SetScrollInfo(SB_VERT, &si_);
	m_wnd.SetScrollInfo(SB_HORZ, &si_);

	details::CPrintViewSurface_Layout layout_(m_wnd);
	RECT rcCanvas = layout_.GetCanvasRect(m_wnd.m_mode, m_wnd.m_metrics); // canvas re-calculates line height

	m_wnd.m_canvas.Window().MoveWindow(&rcCanvas);
	m_wnd.m_anchor.cx = m_wnd.m_anchor.cy = 0;

	m_wnd.Invalidate();

	return S_OK;
}