#ifndef _SHAREDPRINTVIEWCANVAS_H_60106AE5_C0C5_4919_BD16_91A5047E29C7_INCLUDED
#define _SHAREDPRINTVIEWCANVAS_H_60106AE5_C0C5_4919_BD16_91A5047E29C7_INCLUDED
/*
	Created by Tech_dog (VToropov) on 8-Apr-2016 at 5:07:40pm, GMT+7, Phuket, Rawai, Friday;
	This is Shared print library view draw canvas class declaration file.
*/
#include "Shared_PrintView.h"
#include "Shared_PrintMetrics.h"
#include "Shared_PrintViewRender.h"

namespace ex_ui { namespace printing { namespace _impl
{
	using ex_ui::printing::CPrintViewMode;
	using ex_ui::printing::IPrintDataProvider;

	class CPrintViewCanvas
	{
	private:
		class CPrintViewCanvasWnd:
			public ::ATL::CWindowImpl<CPrintViewCanvasWnd>
		{
			friend class CPrintViewCanvas;
			typedef  ATL::CWindowImpl<CPrintViewCanvasWnd> TWindow;
		private:
			SIZE                m_page_sz;  // size of a page in screen resolution; is calculated from device physical page size;
			INT                 m_page_no;  // number of the page is being drawn (1 - based)
			CPrintViewMode::_e  m_mode;
			IPrintDataProvider& m_provider;
			CPrintViewRender    m_render;
		public:
			BEGIN_MSG_MAP(CPrintViewCanvasWnd)
				MESSAGE_HANDLER(WM_CREATE    ,   OnCreate )
				MESSAGE_HANDLER(WM_DESTROY   ,   OnDestroy)
				MESSAGE_HANDLER(WM_ERASEBKGND,   OnErase  )
			END_MSG_MAP()
		public:
			CPrintViewCanvasWnd(IPrintDataProvider&);
		private:
			LRESULT    OnCreate (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT    OnDestroy(UINT, WPARAM, LPARAM, BOOL&);
			LRESULT    OnErase  (UINT, WPARAM, LPARAM, BOOL&);
		};
	private:
		CPrintViewCanvasWnd m_wnd;
	public:
		CPrintViewCanvas(IPrintDataProvider&);
	public:
		HRESULT        Create (const HWND hParent, const RECT& rcArea);
		HRESULT        Destroy(void);
		HRESULT        DrawPage(const INT nPage);
		HRESULT        UpdateLayout(CPreviewMetrics&, const CPrintViewMode&);
		CWindow        Window (void)const;
	};
}}}

#endif/*_SHAREDPRINTVIEWCANVAS_H_60106AE5_C0C5_4919_BD16_91A5047E29C7_INCLUDED*/