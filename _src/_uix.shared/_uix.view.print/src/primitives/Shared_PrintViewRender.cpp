/*
	Created by Tech_dog (VToropov) on 12-Apr-2016 at 2:15:42am, GMT+7, Phuket, Rawai, Tuesday;
	This is Shared print library default render class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 9:23:42a, UTC+7, Phuket, Rawai, Thursday;
*/
#include "StdAfx.h"
#include "Shared_PrintViewRender.h"

using namespace ex_ui::printing;
using namespace ex_ui::printing::_impl;

#include "UIX_GdiObject.h"

using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

/////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace printing { namespace _impl { namespace details
{
	class CPrintViewRender_Layout
	{
	private:
		const HDC  m_hDC;
	public:
		CPrintViewRender_Layout(const HDC hDC) : m_hDC(hDC){}
	public:
		INT   GetDeviceScaleX(INT _size)
		{
			return (::GetDeviceCaps(m_hDC, LOGPIXELSX) * _size) / 72;
		}

		INT   GetDeviceScaleY(INT _size)
		{
			return (::GetDeviceCaps(m_hDC, LOGPIXELSY) * _size) / 72;
		}
	};
}}}}

using ex_ui::printing::_impl::details::CPrintViewRender_Layout;

/////////////////////////////////////////////////////////////////////////////

CPrintViewRender::CPrintViewRender(IPrintDataProvider& _provider) :
	m_provider(_provider),
	m_header_cy(25),
	m_footer_cy(30),
	m_pages(0),
	m_cur_page(0),
	m_ver_gap(10),
	m_row_per_page(0),
	m_row_cy (16)
{
	::SetRect(
			&m_margins,
			40,
			20,
			20,
			20
		);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CPrintViewRender::CountPages(const HDC hDC, INT& _count)
{
	SIZE page_ = {
		::GetDeviceCaps(hDC, HORZRES),
		::GetDeviceCaps(hDC, VERTRES)
	};

	INT line_ = 0;

	HRESULT hr_ = this->CountPages(hDC, page_, line_, _count);
	return  hr_;
}

HRESULT       CPrintViewRender::CountPages(const HDC hDC, const SIZE _page, INT& _line, INT& _count)
{
	CPrintViewRender_Layout layout_(hDC);

	const INT height_ = _page.cy         // available height for table body
		- layout_.GetDeviceScaleY(
	                    m_margins.top
	                  + m_margins.bottom
	                  + m_header_cy * 2  // table header has the same height as page header
	                  + m_footer_cy
	                  + m_ver_gap * 2);

	draw::common::CFontScalable fnt_(hDC, _T("Verdana"), 9, eCreateFontOption::eNone);
	draw::common::CText text_(hDC, fnt_);

	SIZE sz_ = {0};
	text_.GetSize(_T("WWq"), sz_);

	m_row_cy = sz_.cy + sz_.cy / 5;
	_line  = m_row_cy;

	m_row_per_page  = height_ / _line; // table rows per page

	INT total_ = 0;
	m_provider.IPrintDataProvider_RowCount(total_);

	m_pages = total_ / m_row_per_page + 1;
	_count  = m_pages;

	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT       CPrintViewRender::DrawBkgnd (CZBuffer& _buf, const RECT& _area)
{
	_buf.DrawSolidRect(
			_area,
			RGB(0xff, 0xff, 0xff)
		);
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT       CPrintViewRender::DrawFooter(CZBuffer& _buf, const RECT& _area)
{
	CPrintViewRender_Layout layout_(_buf);

	draw::common::CFontScalable fnt_(_buf, _T("Verdana"), 9);
	RECT rc_ = _area;
	rc_.bottom -= layout_.GetDeviceScaleY(m_margins.bottom);
	rc_.top     =-layout_.GetDeviceScaleY(m_footer_cy) + rc_.bottom;
	rc_.left   += layout_.GetDeviceScaleX(m_margins.left);
	rc_.right  -= layout_.GetDeviceScaleX(m_margins.right);

	CAtlString cs_footer;
	m_provider.IPrintDataProvider_FooterText(cs_footer);

	CAtlString cs_buffer;
	cs_buffer.Format(
			_T("%d"),
			m_cur_page
		);
	cs_footer.Replace(_T("${page}"), cs_buffer);
	cs_buffer.Format(
			_T("%d"),
			m_pages
		);
	cs_footer.Replace(_T("${pages}"), cs_buffer);

	_buf.DrawTextExt(
			cs_footer,
			fnt_,
			rc_,
			0,
			DT_RIGHT|DT_VCENTER|DT_SINGLELINE|DT_NOCLIP
		);

	_buf.DrawLine(
			rc_.left ,
			rc_.top  ,
			rc_.right,
			rc_.top  ,
			RGB(0x0,0x0,0x0),   // color is not applied, excepting black or white;
			layout_.GetDeviceScaleY(1)
		);

	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT       CPrintViewRender::DrawHeader(CZBuffer& _buf, const RECT& _area)
{
	CPrintViewRender_Layout layout_(_buf);

	draw::common::CFontScalable fnt_(_buf, _T("Verdana"), 9);

	RECT rc_ = _area;
	rc_.top    += layout_.GetDeviceScaleY(m_margins.top);
	rc_.bottom  = layout_.GetDeviceScaleY(m_header_cy) + rc_.top;
	rc_.left   += layout_.GetDeviceScaleX(m_margins.left);
	rc_.right  -= layout_.GetDeviceScaleX(m_margins.right);

	CAtlString cs_header;
	m_provider.IPrintDataProvider_HeaderText(cs_header);

	_buf.DrawTextExt(
			cs_header,
			fnt_,
			rc_,
			0,
			DT_LEFT|DT_VCENTER|DT_SINGLELINE|DT_NOCLIP
		);

	_buf.DrawLine(
			rc_.left,
			rc_.bottom - layout_.GetDeviceScaleY(0x1),
			rc_.right,
			rc_.bottom - layout_.GetDeviceScaleY(0x1),
			0,
			layout_.GetDeviceScaleY(1)
		);

	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT       CPrintViewRender::DrawTable (CZBuffer& _buf, const RECT& _area)
{

	CPrintViewRender_Layout layout_(_buf);

	static ::std::vector<CAtlString> header_;
	if (header_.empty())
		m_provider.IPrintDataProvider_TableHeader(header_);
	if (header_.empty())
		return HRESULT_FROM_WIN32(ERROR_INVALID_DATA);

	RECT rcHeader = {0};
	if (::IsRectEmpty(&rcHeader))
	{
		const INT left_ = _area.left + layout_.GetDeviceScaleX(m_margins.left);
		const INT top_  = _area.top  + layout_.GetDeviceScaleY(m_margins.top + m_header_cy + m_ver_gap);
		::SetRect(
			&rcHeader,
			left_,
			top_ ,
			_area.right - layout_.GetDeviceScaleX(m_margins.right),
			top_        + layout_.GetDeviceScaleY(m_header_cy)
		);
	}

	static FLOAT ratio_[] = {(FLOAT )2.7, (FLOAT) 7.6, (FLOAT) 1.7};
	static FLOAT total_   =  2.7 +7.6 +1.7 ;

	const FLOAT factor_ = FLOAT(__W(rcHeader)) / total_;

	INT left_cell = rcHeader.left;
	draw::common::CFontScalable fnt_0(_buf, _T("Verdana"), 8, eCreateFontOption::eBold);

	for (size_t i_ = 0; i_ < header_.size() && i_ < _countof(ratio_); i_++)
	{
		RECT cell_  = rcHeader;
		cell_.left  = left_cell;
		cell_.right = left_cell + static_cast<INT>(factor_ * ratio_[i_]);

		_buf.DrawRectangle(
				cell_,
				0,
				layout_.GetDeviceScaleX(1),
				i_ == 0 ? CRectEdge::eAll : CRectEdge::eBottom|CRectEdge::eRight|CRectEdge::eTop
			);

		left_cell = cell_.right - 1;

		_buf.DrawTextExt(
			header_[i_],
			fnt_0,
			cell_,
			0,
			DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_NOCLIP
		);
	}
	const INT bottom_ = _area.bottom - layout_.GetDeviceScaleY(m_margins.bottom - m_footer_cy - m_ver_gap);

	INT top_ = rcHeader.bottom - layout_.GetDeviceScaleY(1);
	INT row_ = (m_cur_page - 1) * m_row_per_page; // start row;
	INT itr_ = 0;  // iterator that counts number of rows that are being drawn;

	draw::common::CFontScalable fnt_1(_buf, _T("Verdana"), 8, eCreateFontOption::eNone);
	::std::vector<::std::vector<CAtlString>> data_;

	HRESULT hr_ = m_provider.IPrintDataProvider_PageData(row_, m_row_per_page, data_);
	if (FAILED(hr_))
		return hr_;

	while (itr_ < m_row_per_page && itr_ < static_cast<INT>(data_.size()))
	{
		const ::std::vector<CAtlString>& cells_ = data_[itr_];

		INT left_ = rcHeader.left;

		RECT cell_   = {0};
		cell_.top    = top_;
		cell_.bottom = top_ + m_row_cy;

		if (bottom_ < cell_.bottom)
			break;

		for (size_t i_ = 0; i_ < cells_.size() && i_ < _countof(ratio_); i_++)
		{
			cell_.left   = left_;
			cell_.right  = left_ + static_cast<INT>(factor_ * ratio_[i_]);
			// TODO: replace cell drawing by table mesh (to draw in one pass a column and in another pass a row)
			DWORD dwEdges = CRectEdge::eRight|CRectEdge::eBottom;
			if (i_ == 0)
				dwEdges |= CRectEdge::eLeft;

			_buf.DrawRectangle(
					cell_,
					0,
					layout_.GetDeviceScaleX(1),
					dwEdges
				);

			left_ = cell_.right - 1;

			::InflateRect(&cell_, -layout_.GetDeviceScaleX(0x7), 0x0);

			DWORD style_ = DT_VCENTER|DT_SINGLELINE|DT_NOCLIP;
			switch (i_)
			{
			case 0:  style_ |= DT_CENTER; break;
			case 1:  style_ |= DT_LEFT|DT_PATH_ELLIPSIS|DT_MODIFYSTRING;   break;
			case 2:  style_ |= DT_CENTER; break;
			}

			_buf.DrawTextExt(
				cells_[i_].GetString(),
				fnt_1,
				cell_,
				0,
				style_
			);
		}

		top_ = cell_.bottom - 1;

		itr_++;
	}

	return  hr_;
}

VOID          CPrintViewRender::SetPageNumber(const INT _page)
{
	m_cur_page = _page;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CPrintViewRender::DoPrint(const HDC hPrinter, IPrintEventSink& _sink)
{
	INT pages_ = 0;

	::SetMapMode(hPrinter, MM_TEXT);

	HRESULT hr_ = this->CountPages(hPrinter, pages_);

	RECT rcPage = {
		0,
		0,
		::GetDeviceCaps(hPrinter, HORZRES),
		::GetDeviceCaps(hPrinter, VERTRES)
	};
	
	CAtlString cs_name;
	m_provider.IPrintDataProvider_DocumentName(cs_name);

	DOCINFO di_ = {0};
	di_.cbSize = sizeof(di_);
	di_.lpszDocName = cs_name.GetBuffer();

	bool bContinue = true;

	if (::StartDoc(hPrinter, &di_))
	{
		for (INT i_ = 0; i_ < pages_; i_++)
		{
			_sink.IPrintEvent_OnPagePrint(i_ + 1, pages_);
			::StartPage(hPrinter);
			{
				CZBuffer buf_(hPrinter, rcPage);

				this->SetPageNumber(i_ + 1);
				this->DrawBkgnd (buf_, rcPage);
				this->DrawHeader(buf_, rcPage);
				this->DrawFooter(buf_, rcPage);
				this->DrawTable (buf_, rcPage);
			}
			if (::EndPage(hPrinter) < 1)
			{
				bContinue = false;
				break;
			}
		}
		if (bContinue)
		{
			::EndDoc(hPrinter);
			_sink.IPrintEvent_OnPrintComplete();
		}
		else
		{
			::AbortDoc(hPrinter);
			_sink.IPrintEvent_OnPrintAborted();
		}
	}

	return  hr_;
}