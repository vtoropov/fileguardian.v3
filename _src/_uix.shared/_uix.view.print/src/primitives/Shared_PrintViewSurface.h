#ifndef _SHAREDPRINTVIEWSURFACE_H_A60ECE1A_3255_4d0f_8265_09EEC1235C58_INCLUDED
#define _SHAREDPRINTVIEWSURFACE_H_A60ECE1A_3255_4d0f_8265_09EEC1235C58_INCLUDED
/*
	Created by Tech_dog (VToropov) on 8-Apr-2016 at 1:44:32pm, GMT+7, Phuket, Rawai, Friday;
	This is Shared print library view scrollable surface class declaration file.
*/
#include "Shared_PrintView.h"
#include "Shared_PrintViewCanvas.h"
#include "Shared_PrintMetrics.h"
#include "UIX_MouseHandler.h"

namespace ex_ui { namespace printing { namespace _impl
{
	typedef POINT ScrollPosition;

	using ex_ui::printing::IPrintDataProvider;
	using ex_ui::frames::CMouseEvtHandler;
	using ex_ui::frames::CMouseEvtSubscribers;

	class CPrintViewSurface
	{
	private:
		class CPrintViewSurfaceWnd:
			public ::ATL::CWindowImpl<CPrintViewSurfaceWnd>
		{
			friend class CPrintViewSurface;
			typedef  ATL::CWindowImpl<CPrintViewSurfaceWnd> TWindow;
		private:
			WTL::CScrollBar    m_vscroll;
			CPrintViewCanvas   m_canvas;
			CPrintViewMode     m_mode;
			CPreviewMetrics    m_metrics;
			SIZE               m_anchor;   // saves current shifts of the scrolled page
		public:
			BEGIN_MSG_MAP(CPrintViewSurfaceWnd)
				MESSAGE_HANDLER(WM_CREATE    ,   OnCreate )
				MESSAGE_HANDLER(WM_DESTROY   ,   OnDestroy)
				MESSAGE_HANDLER(WM_ERASEBKGND,   OnErase  )
				MESSAGE_HANDLER(WM_HSCROLL   ,   OnHScroll)
				MESSAGE_HANDLER(WM_VSCROLL   ,   OnVScroll)
				MESSAGE_HANDLER(WM_MOUSEWHEEL_EX, OnWheel )
			END_MSG_MAP()
		public:
			CPrintViewSurfaceWnd(IPrintDataProvider&);
		private:
			LRESULT    OnCreate (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT    OnDestroy(UINT, WPARAM, LPARAM, BOOL&);
			LRESULT    OnErase  (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT    OnHScroll(UINT, WPARAM, LPARAM, BOOL&);
			LRESULT    OnVScroll(UINT, WPARAM, LPARAM, BOOL&);
			LRESULT    OnWheel  (UINT, WPARAM, LPARAM, BOOL&);
		};
	private:
		CPrintViewSurfaceWnd m_wnd;
	public:
		CPrintViewSurface(IPrintDataProvider&);
	public:
		HRESULT        Create (const HWND hParent, const RECT& rcArea);
		HRESULT        Destroy(void);
		HRESULT        ShowPage(const INT nPage);
		HRESULT        UpdateLayout(CPreviewMetrics&, const CPrintViewMode&);
	};
}}}

#endif/*_SHAREDPRINTVIEWSURFACE_H_A60ECE1A_3255_4d0f_8265_09EEC1235C58_INCLUDED*/