#ifndef _SHAREDPRINTVIEWRENDER_D68BA7B5_2629_473a_8A43_F0BA5239EFC6_INCLUDED
#define _SHAREDPRINTVIEWRENDER_D68BA7B5_2629_473a_8A43_F0BA5239EFC6_INCLUDED
/*
	Created by Tech_dog (VToropov) on 12-Apr-2016 at 2:06:02am, GMT+7, Phuket, Rawai, Tuesday;
	This is Shared print library default render class declaration file.
*/
#include "Shared_PrintView.h"
#include "Shared_PrintMetrics.h"

#include "UIX_GdiProvider.h"

namespace ex_ui { namespace printing { namespace _impl
{
	using ex_ui::draw::CZBuffer;
	using ex_ui::printing::IPrintDataProvider;

	typedef RECT PageMargins;

	class CPrintViewRender
	{
	private:
		IPrintDataProvider& m_provider;
		PageMargins         m_margins;
		INT                 m_header_cy;    // header height
		INT                 m_footer_cy;    // footer height
		INT                 m_pages;        // pages count
		INT                 m_cur_page;     // current page number
		INT                 m_ver_gap;      // a gap between table body and page header and footer
		INT                 m_row_per_page;
		INT                 m_row_cy;       // table row height is calculated by canvas object
	public:
		CPrintViewRender(IPrintDataProvider&);
	public:
		HRESULT       CountPages(const HDC, INT& _count);
		HRESULT       CountPages(const HDC, const SIZE _page, INT& _line, INT& _count); // calculates pages and line/row height by page size provided
		HRESULT       DrawBkgnd (CZBuffer&, const RECT& _area);
		HRESULT       DrawFooter(CZBuffer&, const RECT& _area);
		HRESULT       DrawHeader(CZBuffer&, const RECT& _area);
		HRESULT       DrawTable (CZBuffer&, const RECT& _area);
		VOID          SetPageNumber(const INT); // set current page number
	public:
		HRESULT       DoPrint(const HDC hPrinter, IPrintEventSink&); // outputs drawings to printer device provider
	};
}}}

#endif/*_SHAREDPRINTVIEWRENDER_D68BA7B5_2629_473a_8A43_F0BA5239EFC6_INCLUDED*/