/*
	Created by Tech_dog (VToropov) on 7-Apr-2016 at 3:50:29pm, GMT+7, Phuket, Rawai, Thursday;
	his is Shared print library view window class implementation file.
*/
#include "StdAfx.h"
#include "Shared_PrintViewWnd.h"
#include "Shared_PrintViewRender.h"

using namespace ex_ui::printing::_impl;

#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;

#include "Shared_GenericAppObject.h"
#include "Shared_GenericAppResource.h"

using namespace shared::lite::common;
using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace printing { namespace _impl { namespace details
{
	class CPrintView_Layout
	{
	private:
		CWindow& m_view;
		RECT     m_client_area;
	public:
		CPrintView_Layout(CWindow& _view) : m_view(_view)
		{
			if (!m_view)
				::SetRectEmpty(&m_client_area);
			else
				m_view.GetClientRect(&m_client_area);
		}
	public:
		RECT     GetPanelRect(void)const
		{
			RECT rc_ = m_client_area;
			rc_.bottom = rc_.top + CPrintViewPanel::GetDefaultSize().cy;
			return rc_;
		}

		RECT     GetSurfaceRect(void)const
		{
			RECT rc_ = m_client_area;
			rc_.top  = this->GetPanelRect().bottom;
			return rc_;
		}

	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CPrintViewWnd::CPrintViewWnd(IPrintEventSink& _sink, IPrintDataProvider& _provider):
m_sink(_sink), m_panel(*this), m_mode(CPrintViewMode::eWholePage), m_provider(_provider), m_surface(_provider)
{
}

CPrintViewWnd::~CPrintViewWnd(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CPrintViewWnd::IPrintEvent_OnClose(void)
{
	m_sink.IPrintEvent_OnClose();
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT    CPrintViewWnd::IPrintEvent_OnError(const CSysError& _error)
{
	m_sink.IPrintEvent_OnError(_error);
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT    CPrintViewWnd::IPrintEvent_OnModeChanged(const CPrintViewMode _mode)
{
	m_mode = _mode;

	m_surface.UpdateLayout(m_metrics, m_mode);

	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT    CPrintViewWnd::IPrintEvent_OnPageChanged(const INT nPage, const INT nPages)
{
	m_surface.ShowPage(nPage);
	m_sink.IPrintEvent_OnPageChanged(nPage, nPages);
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT    CPrintViewWnd::IPrintEvent_OnPrint(void)
{
	const DWORD flags_ = PD_ALLPAGES|PD_NOPAGENUMS|PD_NOSELECTION|PD_RETURNDC|PD_USEDEVMODECOPIES;

	::WTL::CPrintDialog dlg_(FALSE, flags_, NULL);
	
	INT_PTR res_ = dlg_.DoModal();
	if (IDOK == res_)
	{
		CApplicationCursor wc_;

		this->DoPrint(dlg_.m_pdActual.hDC);

		::DeleteDC(dlg_.m_pdActual.hDC);
		if (dlg_.m_pdActual.hDevMode)
			::GlobalFree(dlg_.m_pdActual.hDevMode);
		if (dlg_.m_pdActual.hDevNames)
			::GlobalFree(dlg_.m_pdActual.hDevNames);
	}
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPrintViewWnd::OnCreate (UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = TRUE;
	details::CPrintView_Layout layout_(*this);
	HRESULT hr_ = S_OK;
	{
		const RECT rc_ = layout_.GetPanelRect();
		hr_ = m_panel.Create(*this, rc_); ATLASSERT(SUCCEEDED(hr_));
	}
	{
		const RECT rc_ = layout_.GetSurfaceRect();
		hr_ = m_surface.Create(*this, rc_); ATLASSERT(SUCCEEDED(hr_));
	}
	return 0;
}

LRESULT    CPrintViewWnd::OnDestroy(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	m_surface.Destroy();
	m_panel.Destroy();
	return 0;
}

LRESULT    CPrintViewWnd::OnErase  (UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = TRUE;
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CPrintViewWnd::DoPreview(void)
{
	CPrintViewMode mode_(CPrintViewMode::eRealSize);
	m_mode = mode_;

	m_surface.UpdateLayout(m_metrics, m_mode);

	HRESULT hr_ = m_surface.ShowPage(1);
	TWindow::SetWindowPos(HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE|SWP_NOMOVE|SWP_SHOWWINDOW);
	return  hr_;
}

HRESULT    CPrintViewWnd::DoPrint(const HDC hPrinter)
{
	CPrintViewRender render_(m_provider);
	HRESULT hr_ = render_.DoPrint(hPrinter, m_sink);
	return  hr_;
}

HRESULT    CPrintViewWnd::Initialize(HDC hDevice)
{
	HRESULT hr_ = m_metrics.Initialize(hDevice);
	if (SUCCEEDED(hr_))
		hr_ = m_surface.UpdateLayout(m_metrics, m_mode);
	if (SUCCEEDED(hr_))
	{
		m_panel.UpdateState(m_metrics);
		m_sink.IPrintEvent_OnPageChanged(
				1,
				m_metrics.PageCount()
			);
	}
	else
	{
		CSysError err_obj;
		err_obj.Source(_T("CPrintView"));
		err_obj.Module(_T(__FUNCTION__));
		err_obj.SetState(
				hr_,
				_T("Print view initialization error")
			);
		m_sink.IPrintEvent_OnError(err_obj);
	}
	return  hr_;
}