#ifndef _SHAREDPRINTSTDAFX_H_DD5DA274_F341_4253_903C_61132700EE27_INCLUDED
#define _SHAREDPRINTSTDAFX_H_DD5DA274_F341_4253_903C_61132700EE27_INCLUDED
/*
	Created by Tech_dog (VToropov) on 11-Apr-2016 at 7:34:43pm, GMT+7, Phuket, Rawai, Monday;
	This is Shared Print Library precompiled headers declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 9:18:17a, UTC+7, Phuket, Rawai, Thursday;
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe
#pragma warning(disable: 4458)  // declaration of '{decl}' hides class member; this warning is related to GDI+ header;

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <atlapp.h>
#include <atlgdi.h>
#include <atlctrls.h>
#include <atldlgs.h>
#include <comdef.h>
#include <vector>
#include <map>

#endif/*_SHAREDPRINTSTDAFX_H_DD5DA274_F341_4253_903C_61132700EE27_INCLUDED*/