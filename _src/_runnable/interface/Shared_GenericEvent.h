#ifndef _SHAREDLITELIBGENERICEVENT_H_DF255B0D_D53A_4917_B1EC_8F7D231AA515_INCLUDED
#define _SHAREDLITELIBGENERICEVENT_H_DF255B0D_D53A_4917_B1EC_8F7D231AA515_INCLUDED
/*
	Created by Tech_dog (VToropov) on 18-Jul-2013 at 10:13:32pm, GMT+3, Taganrog, Sunday;
	This is Shared Lite Library Generic Event class declaration file.
*/
namespace shared { namespace runnable
{
	interface IGenericEventNotify
	{
		virtual HRESULT  GenericEvent_OnNotify(const _variant_t v_evt_id) PURE;
		virtual HRESULT  GenericEvent_OnNotify(const LONG n_evt_id, const _variant_t v_data) {n_evt_id; v_data; return E_NOTIMPL; }
	};
	class CGenericMarshaller
	{
	protected:
		HANDLE           m_handler;
	public:
		CGenericMarshaller(IGenericEventNotify&, const _variant_t& v_evt_id);
		virtual ~CGenericMarshaller(void);
	public:
		virtual HRESULT  Create(void);
		virtual HRESULT  Destroy(void);
		virtual HWND     GetHandle_Safe(void) const;
	private:
		CGenericMarshaller(const CGenericMarshaller&);
		CGenericMarshaller& operator= (const CGenericMarshaller&);
	public:
		virtual HRESULT  Fire(const bool bAsynch = true);
		static  HRESULT  Fire(const HWND hHandler, const bool bAsynch = true);
	public:
		virtual HRESULT  Fire2(void);
	};

	class CGenericWaitCounter
	{
	public:
		enum{
			eInfinite = (DWORD)-1
		};
	protected:
		DWORD     m_nTimeSlice;    // in milliseconds
		DWORD     m_nCurrent;      // current time
		DWORD     m_nTimeFrame;    // total time to wait for
	public:
		CGenericWaitCounter(const DWORD nTimeSlice, const DWORD nTimeFrame);
		virtual ~CGenericWaitCounter(void);
	public:
		virtual bool IsElapsed(void) const;
		virtual bool IsReset  (void) const;
		virtual VOID Reset(const DWORD nTimeSlice = eInfinite, const DWORD nTimeFrame = eInfinite);
		virtual VOID Wait (void);
	};
}}

#endif/*_SHAREDLITELIBGENERICEVENT_H_DF255B0D_D53A_4917_B1EC_8F7D231AA515_INCLUDED*/