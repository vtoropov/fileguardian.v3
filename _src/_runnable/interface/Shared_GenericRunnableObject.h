#ifndef _SHAREDGENERICRUNNABLEOBJECT_H_E4F97D98_9FE4_4b50_ACF6_CC1841682CAF_INCLUDED
#define _SHAREDGENERICRUNNABLEOBJECT_H_E4F97D98_9FE4_4b50_ACF6_CC1841682CAF_INCLUDED
/*
	Created by Tech_dog (VToropov) on 18-Jul-2013 at 10:36:50pm, GMT+3, Taganrog, Thursday;
	This is Shared generic asynchronous runnable object class declaration file.
*/
#include "Shared_GenericRunnableDefs.h"
#include "Shared_GenericEvent.h"

namespace shared { namespace runnable
{
	typedef unsigned int (__stdcall *TRunnableFunc)(void*);

	class CGenericRunnableObject
	{
	private:
		CGenericMarshaller       m_async_evt;
		HANDLE                   m_hThread;             // thread handle that runs a procedure
		volatile HANDLE          m_hEvent;              // synch primitive
		volatile mutable bool    m_bStopped;
		TRunnableFunc            m_function;
	protected:
		CGenericRunnableObject(TRunnableFunc, IGenericEventNotify&, const _variant_t& v_evt_id);
		virtual ~CGenericRunnableObject(void);
	public:
		virtual CGenericMarshaller&   Event(void);
		virtual bool             IsRunning(void) const;
		virtual bool             IsStopped(void) const;
		virtual void             MarkCompleted(void);
		virtual HRESULT          Start(const eRunPriority::_e = eRunPriority::eLow);
		virtual HRESULT          Stop(const bool bForced);
		virtual HRESULT          Wait(const DWORD = 0);
	public:
		HANDLE                   EventHandle(void)const;
	private:
		CGenericRunnableObject(const CGenericRunnableObject&);
		CGenericRunnableObject& operator= (const CGenericRunnableObject&);
	};
}}

#endif/*_SHAREDGENERICRUNNABLEOBJECT_H_E4F97D98_9FE4_4b50_ACF6_CC1841682CAF_INCLUDED*/