#ifndef _SHAREDGENERICRUNNABLEDEFS_H_20DC498A_24EF_4afc_826F_D923C39583D5_INCLUDED
#define _SHAREDGENERICRUNNABLEDEFS_H_20DC498A_24EF_4afc_826F_D923C39583D5_INCLUDED
/*
	Created by Tech_dog (VToropov) on 21-Aug-2014 at 0:04:39a, GMT+3, Taganrog, Thursday;
	This is Shared generic runnable object definition(s) declaration file.
*/

namespace shared { namespace runnable
{
	class eRunPriority
	{
	public:
		enum _e{
			eLow    = 0, //default
			eNormal = 1,
			eHigh   = 2
		};
	};
}}

#endif/*_SHAREDGENERICRUNNABLEDEFS_H_20DC498A_24EF_4afc_826F_D923C39583D5_INCLUDED*/