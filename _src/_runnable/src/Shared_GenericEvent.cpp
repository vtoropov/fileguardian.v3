/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Aug-2014 at 8:04:23pm, GMT+3, Taganrog, Wednesday;
	This is Shared Lite Library Generic Event class implementation file.
*/
#include "StdAfx.h"
#include "Shared_GenericEvent.h"
#include "Shared_SystemError.h"

using namespace shared::runnable;
using namespace shared::lite::common;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace runnable { namespace details
{
	class CMessageHandler:
		public ::ATL::CWindowImpl<CMessageHandler>
	{
	public:
		enum {
			eInternalMsgId = WM_USER + 1,
		};
	private:
		IGenericEventNotify&    m_sink_ref;
		const _variant_t        m_event_id;
		DWORD                   m_threadId;
	public:
		BEGIN_MSG_MAP(CMessageHandler)
			MESSAGE_HANDLER(CMessageHandler::eInternalMsgId, OnGenericEventNotify)
		END_MSG_MAP()
	private:
		virtual LRESULT  OnGenericEventNotify(UINT, WPARAM, LPARAM, BOOL&)
		{
			m_sink_ref.GenericEvent_OnNotify(m_event_id);
			return 0;
		}
	public:
		CMessageHandler(IGenericEventNotify& sink_ref, const _variant_t& v_evt_id):
			m_sink_ref(sink_ref),
			m_event_id(v_evt_id),
			m_threadId(::GetCurrentThreadId())
		{
		}
		virtual ~CMessageHandler(void)
		{
		}
	public:
		DWORD       _OwnerThreadId(void)const
		{
			return m_threadId;
		}
	};
}}}

#define HANDLER_PTR()  reinterpret_cast<details::CMessageHandler*>(this->m_handler)
/////////////////////////////////////////////////////////////////////////////

CGenericMarshaller::CGenericMarshaller(IGenericEventNotify& sink_ref, const _variant_t& v_evt_id):
	m_handler(NULL)
{
	try
	{
		m_handler = new details::CMessageHandler(sink_ref, v_evt_id);
	}
	catch(::std::bad_alloc&){}
}

CGenericMarshaller::~CGenericMarshaller(void)
{
	Destroy();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CGenericMarshaller::Create(void)
{
	if (!m_handler)
		return OLE_E_BLANK;

	if (HANDLER_PTR()->IsWindow())
		return S_OK;

	HANDLER_PTR()->Create(HWND_MESSAGE);
	return (HANDLER_PTR()->IsWindow() ? S_OK : HRESULT_FROM_WIN32(::GetLastError()));
}

HRESULT  CGenericMarshaller::Destroy(void)
{
	if (!m_handler)
		return OLE_E_BLANK;

	if (!HANDLER_PTR()->IsWindow())
		return S_OK;

	if (HANDLER_PTR()->DestroyWindow())
		HANDLER_PTR()->m_hWnd = NULL;
	else
	{
		CSysError err_obj(::GetLastError());
		ATLASSERT(0);
	}
	try
	{
		delete m_handler; m_handler = NULL;
	}
	catch(...){return E_OUTOFMEMORY;}
	return S_OK;
}

HWND     CGenericMarshaller::GetHandle_Safe(void) const
{
	if (!m_handler)
		return NULL;
	return (HANDLER_PTR()->IsWindow() ? HANDLER_PTR()->m_hWnd : NULL);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CGenericMarshaller::Fire(const bool bAsynch)
{
	const HWND hHandler = this->GetHandle_Safe();
	return CGenericMarshaller::Fire(hHandler, bAsynch);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CGenericMarshaller::Fire2(void)
{
	const HWND  hHandler = this->GetHandle_Safe();
	if (NULL == hHandler)
		return OLE_E_INVALIDHWND;
	if (!::PostThreadMessage(
			HANDLER_PTR()->_OwnerThreadId(),
			details::CMessageHandler::eInternalMsgId,
			NULL,
			NULL
		))
		return __HRESULT_FROM_WIN32(::GetLastError());
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CGenericMarshaller::Fire(const HWND hHandler, const bool bAsynch)
{
	if (!::IsWindow(hHandler))
		return OLE_E_INVALIDHWND;
	if (bAsynch)
		::PostMessage(hHandler, details::CMessageHandler::eInternalMsgId, (WPARAM)0, (LPARAM)0);
	else
		::SendMessage(hHandler, details::CMessageHandler::eInternalMsgId, (WPARAM)0, (LPARAM)0);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CGenericWaitCounter::CGenericWaitCounter(const DWORD nTimeSlice, const DWORD nTimeFrame):
m_nTimeSlice(nTimeSlice),
m_nTimeFrame(nTimeFrame),
m_nCurrent(0)
{
	ATLASSERT(m_nTimeSlice);
	ATLASSERT(m_nTimeFrame);
}

CGenericWaitCounter::~CGenericWaitCounter(void)
{
}

/////////////////////////////////////////////////////////////////////////////

bool CGenericWaitCounter::IsElapsed(void) const
{
	return (m_nCurrent >= m_nTimeFrame);
}

bool CGenericWaitCounter::IsReset(void) const
{
	return (m_nCurrent == 0);
}

VOID CGenericWaitCounter::Reset(const DWORD nTimeSlice, const DWORD nTimeFrame)
{
	if (nTimeSlice != CGenericWaitCounter::eInfinite) m_nTimeSlice = nTimeSlice;
	if (nTimeFrame != CGenericWaitCounter::eInfinite) m_nTimeFrame = nTimeFrame;
	ATLASSERT(m_nTimeSlice);
	ATLASSERT(m_nTimeFrame);
	m_nCurrent = 0;
}

void CGenericWaitCounter::Wait(void)
{
	::Sleep(m_nTimeSlice);
	m_nCurrent += m_nTimeSlice;
}