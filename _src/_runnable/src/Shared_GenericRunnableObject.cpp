/*
	Created by Tech_dog (VToropov) on 18-Jul-2013 at 10:42:06pm, GMT+3, Taganrog, Thursday;
	This is Shared generic asynchronous runnable object class implementation file.
*/
#include "StdAfx.h"
#include "Shared_GenericRunnableObject.h"

using namespace shared::runnable;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace runnable { namespace details
{
	HRESULT GenericRunnableObject_ForceToTerminate(HANDLE event_, HANDLE thread_)
	{
		HRESULT hr_ = S_OK;
		const DWORD dwRet = ::WaitForSingleObject(event_, 10);
		if (WAIT_OBJECT_0!= dwRet)
		{
			if (thread_)
			{
				::TerminateThread(thread_, DWORD(-1));
			}
		}
		return  hr_;
	}

	DWORD   GenericRunnableObject_RunPriorityToDword(const eRunPriority::_e ePriority)
	{
		switch (ePriority)
		{
		case eRunPriority::eHigh:     return ABOVE_NORMAL_PRIORITY_CLASS;
		case eRunPriority::eNormal:   return NORMAL_PRIORITY_CLASS;
		}
		return BELOW_NORMAL_PRIORITY_CLASS;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CGenericRunnableObject::CGenericRunnableObject(TRunnableFunc func, IGenericEventNotify& sink_ref, const _variant_t& v_evt_id):
	m_async_evt(sink_ref, v_evt_id),
	m_hThread(NULL),
	m_hEvent(NULL),
	m_bStopped(true),
	m_function(func)
{
	m_hEvent = ::CreateEvent(0, TRUE, TRUE, 0);
	if (INVALID_HANDLE_VALUE == m_hEvent || NULL == m_hEvent)
	{
		ATLASSERT(FALSE); m_hEvent = NULL;
	}
}

CGenericRunnableObject::~CGenericRunnableObject(void)
{
	if (m_hEvent)
	{
		::CloseHandle(m_hEvent); 
		m_hEvent = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

CGenericMarshaller&   CGenericRunnableObject::Event(void)
{
	return m_async_evt;
}

bool             CGenericRunnableObject::IsRunning(void) const
{
	return (m_hThread && !m_bStopped);
}

bool             CGenericRunnableObject::IsStopped(void) const
{
	return m_bStopped;
}

void             CGenericRunnableObject::MarkCompleted(void)
{
	if (m_hEvent)
	{
		::SetEvent(m_hEvent);
	}
}

HRESULT          CGenericRunnableObject::Start(const eRunPriority::_e ePriority)
{
	if (NULL == m_hEvent) return OLE_E_BLANK;
	if (!IsStopped()) return S_OK;
	if (m_hThread)
	{
		::CloseHandle(m_hThread);
		m_hThread = NULL;
	}
	m_hThread  = (HANDLE)::_beginthreadex(NULL, NULL, m_function, this, CREATE_SUSPENDED, 0);
	if (INVALID_HANDLE_VALUE == m_hThread || NULL == m_hThread)
	{
		return E_OUTOFMEMORY;
	}
	m_bStopped = false;
	::ResetEvent(m_hEvent);
	::SetThreadPriority(m_hThread, details::GenericRunnableObject_RunPriorityToDword(ePriority));

	m_async_evt.Create();

	::ResumeThread(m_hThread);

	HRESULT hr__ = S_OK;
	return  hr__;
}

HRESULT          CGenericRunnableObject::Stop(const bool bForced)
{
	m_async_evt.Destroy();

	if (IsStopped())
		return S_OK;

	m_bStopped = true;

	if (NULL == m_hThread) return S_OK;
	if (bForced)
	{
		details::GenericRunnableObject_ForceToTerminate(m_hEvent, m_hThread);
	}
	else
	{
		INT cnt_ = 0;
		while (WAIT_OBJECT_0 != ::WaitForSingleObject(m_hEvent, 200))
		{
			::Sleep(100);
			cnt_ ++;
			if (cnt_ > 10) // anti-blocker, actually should never happen
			{
				return details::GenericRunnableObject_ForceToTerminate(m_hEvent, m_hThread);
			}
		}
	}
	if (m_hThread)
	{
		::CloseHandle(m_hThread);
		m_hThread = NULL;
	}
	HRESULT hr__ = S_OK;
	return  hr__;
}

HRESULT          CGenericRunnableObject::Wait(const DWORD dPeriod)
{
	const DWORD dResult = ::WaitForSingleObject(m_hEvent, (dPeriod < 1 ? INFINITE : dPeriod));
	return (WAIT_OBJECT_0 == dResult ? S_OK : S_FALSE);
}

/////////////////////////////////////////////////////////////////////////////

HANDLE           CGenericRunnableObject::EventHandle(void)const
{
	return m_hEvent;
}