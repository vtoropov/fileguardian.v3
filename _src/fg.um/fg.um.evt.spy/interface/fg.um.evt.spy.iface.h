#ifndef _FGUMEVTSPYIFACE_H_C18C92F8_59D5_43ED_A815_35C50085C4F8_INCLUDED
#define _FGUMEVTSPYIFACE_H_C18C92F8_59D5_43ED_A815_35C50085C4F8_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jun-2018 at 1:08:37a, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver event spy user mode interface declaration file.
*/
#include "fg.fs.evt.spy.iface.h"

namespace fg { namespace um { namespace spy {

	using shared::lite::common::CSysError;

	class CEvtSpyError :
		public  CSysError
	{
		typedef  CSysError TBaseError;
	public:
		explicit CEvtSpyError(void);
		explicit CEvtSpyError(const DWORD dwError);
		explicit CEvtSpyError(const HRESULT);
	public:
		CEvtSpyError&    operator= (const DWORD);
		CEvtSpyError&    operator= (const HRESULT);
		CEvtSpyError&    operator= (const CSysError&);
	};

	class CEvtSpyState
	{
	public:
		enum _e{
			eUndefined   = 0x0,
			eInitialized = 0x1,
			eInstalled   = 0x2,
			eConnected   = 0x4,
		};
	private:
		DWORD      m_value;
	public:
		CEvtSpyState(void);
	public:
		VOID       Append(const _e);
		VOID       Clear(void);
		bool       IsConnected(void)const;
		bool       IsInitialized(void)const;
		bool       IsInstalled(void)const;
		VOID       Modify(const _e, const bool _set); // if _set is true the state is appended, otherwise is removed;
		VOID       Remove(const _e);
	};

	class CEvtSpy_Base {

	protected:
		mutable
		CEvtSpyError    m_error;

	protected:
		CEvtSpy_Base(LPCTSTR lpszModuleName);

	public:
		TErrorRef       Error(void)const;             // the last operation error object
	};

	class CEvtSpyInstaller : public CEvtSpy_Base {

		typedef CEvtSpy_Base TBase;

	private:
		CEvtSpyState    m_state;

	public:
		CEvtSpyInstaller(void);
	public:
		HRESULT         Install(const bool bIgonreIfInstalled);     // installs event spy driver, if it is not done yet;
		bool            IsInstalled(void);                          // checks event spy driver installation by querying service manager;
		const
		CEvtSpyState&   State(void)const;                           // gets current state of the installation;
		HRESULT         Uninstall(const bool bIgonreIfUninstalled); // deletes filter driver from registry
	};

	class CEvtSpyModule : public CEvtSpy_Base {

		typedef CEvtSpy_Base TBase;

	private:
		CEvtSpyState    m_state;
		HANDLE          m_reader;                       // reads data from filter
	public:
		CEvtSpyModule(void);
		~CEvtSpyModule(void);
	public:
		HRESULT         Connect(const bool bIgnoreIfConnected); // connects to event spy port for data exchanging;
		HRESULT         Disconnect(void);               // unloads the service manager and makes other cleaning;
		const
		CEvtSpyState&   State(void)const;               // current state of event spy driver;
	};
}}}

#endif/*_FGUMEVTSPYIFACE_H_C18C92F8_59D5_43ED_A815_35C50085C4F8_INCLUDED*/