#ifndef _FGUMEVTSPYDATADUMPER_H_1AAF4B6C_E5D6_4b92_ACAE_F96A11D565A4_INCLUDED
#define _FGUMEVTSPYDATADUMPER_H_1AAF4B6C_E5D6_4b92_ACAE_F96A11D565A4_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Jun-2017 at 12:20:55p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver data output/dump class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 9-Jun-2018 at 0:37:11a, UTC+7, Phuket, Rawai, Saturday;
*/
#include "fg.fs.evt.spy.iface.h"

namespace fg { namespace um { namespace spy
{
	using shared::km::spy::TFs_EvtData;

	class CDataDumper
	{
	public:
		static VOID ToConsole(
				__in const TFs_EvtData&
			);
	};

}}}

#endif/*_FGUMEVTSPYDATADUMPER_H_1AAF4B6C_E5D6_4b92_ACAE_F96A11D565A4_INCLUDED*/