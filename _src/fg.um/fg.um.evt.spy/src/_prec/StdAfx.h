#ifndef _FGUMEVTSPYSTDAFX_H_DD5DA274_F341_4253_903C_61132700EE27_INCLUDED
#define _FGUMEVTSPYSTDAFX_H_DD5DA274_F341_4253_903C_61132700EE27_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Jun-2017 at 3:47:48a, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver bridge library precompiled headers declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 at 9-Jun-2018 on 0:32:44a, UTC+7, Phuket, Rawai, Saturday;
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>
#include <comutil.h>
#include <vector>
#include <typeinfo.h> // for bad_cast;
#include <map>
#include <queue>

#include <Fltuser.h>

#include "Shared_LogJournal.h"

namespace global
{
	class _out : public shared::log::CEventJournal{};
}

#include "Shared_GenericHandle.h"

namespace global {
	using shared::lite::common::CHandleSafe;

	CHandleSafe&  GetDriverPort(void);
	CAtlString    GetLocalFromUnc(LPCTSTR lpszUncPath);
	CAtlString    GetUncFromLocal(LPCTSTR lpszDosPath);
}

#pragma comment(lib, "FltLib.lib")

#endif/*_FGUMEVTSPYSTDAFX_H_DD5DA274_F341_4253_903C_61132700EE27_INCLUDED*/