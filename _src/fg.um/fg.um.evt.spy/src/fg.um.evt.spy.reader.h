#ifndef _FGUMEVTSPYREADER_H_14B9274B_EA0B_46c6_B108_5FDF13CC7CD1_INCLUDED
#define _FGUMEVTSPYREADER_H_14B9274B_EA0B_46c6_B108_5FDF13CC7CD1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jun-2018 at 2:29:16a, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver event spy data reader class declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_GenericRunnableObject.h"

namespace fg { namespace um { namespace spy
{
	using shared::lite::common::CSysError;
	using shared::lite::common::CSysErrorSafe;

	using shared::runnable::CGenericRunnableObject;
	using shared::runnable::IGenericEventNotify;
	using shared::lite::sync::CGenericSyncObject;

	namespace details {
		UINT WINAPI CDriverReader_ThreadFunc(PVOID pObject);
	}

	class CDriverReader :
		protected CGenericRunnableObject,
		private   IGenericEventNotify
	{
		typedef   CGenericRunnableObject TRunnable;
		friend    UINT WINAPI details::CDriverReader_ThreadFunc(PVOID pObject);

	private:
		CGenericSyncObject   m_sync_obj;
		CSysErrorSafe        m_error;

	public:
		CDriverReader(void);

	private:
		virtual HRESULT  GenericEvent_OnNotify(const _variant_t v_evt_id) override sealed;

	public:
		TErrorRef            Error(void)const;
		HRESULT              Interrupt(void);
		HRESULT              Start(void);
		HRESULT              Stop (void);

	private:
		VOID                _MarkCompleted(void);
	};

}}}

#endif/*_FGUMEVTSPYREADER_H_14B9274B_EA0B_46c6_B108_5FDF13CC7CD1_INCLUDED*/