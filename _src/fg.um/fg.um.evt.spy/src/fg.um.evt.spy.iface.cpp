/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jun-2018 at 3:02:43a, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver event spy user mode interface implementation file.
*/
#include "StdAfx.h"
#include "fg.um.evt.spy.iface.h"
#include "fg.um.evt.spy.reader.h"
#include "fg.um.evt.spy.resource.h"

using namespace fg::um::spy;

#include "Shared_DateTime.h"

using namespace shared::lite::data;

#include "Shared_GenericHandle.h"
#include "Shared_SystemService.h"
#include "Shared_SystemServiceMan.h"
#include "Shared_Registry.h"
#include "Shared_FS_CommonDefs.h"

using namespace shared::service;
using namespace shared::registry;
using namespace shared::ntfs;

#include "FG_Generic_Defs.h"

using namespace fg::common::data;

#include "fg.fs.evt.spy.iface.h"

using namespace shared::km::spy;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace um { namespace spy { namespace details
{
	CAtlString CEvtSpyError_FormatMessage(const DWORD dwError)
	{
		TCHAR szBuffer[_MAX_PATH] = {0};

		HMODULE hModule = ::LoadLibraryExW( _T("fltlib.dll"), NULL, LOAD_LIBRARY_AS_DATAFILE );

		::FormatMessage(
			FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_FROM_HMODULE,
			hModule,
			dwError,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL),
			szBuffer,
			_MAX_PATH - 1,
			NULL
		);

		::ATL::CAtlString msg_(szBuffer);

		if (hModule != NULL)
			::FreeLibrary( hModule );

		return msg_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CEvtSpyError::CEvtSpyError(void) : TBaseError() { }
CEvtSpyError::CEvtSpyError(const DWORD dwError) : TBaseError(dwError) { }
CEvtSpyError::CEvtSpyError(const HRESULT hError) : TBaseError(hError) { }

/////////////////////////////////////////////////////////////////////////////

CEvtSpyError&    CEvtSpyError::operator= (const DWORD dwError)
{
	::ATL::CAtlString cs_error = details::CEvtSpyError_FormatMessage(dwError);
	if (cs_error.IsEmpty()){
		(CSysError)(*this) = dwError; // uses default format implementation ;
		cs_error = this->m_buffer;
	}
	if (!cs_error.IsEmpty())
		TBaseError::SetState(dwError, cs_error.GetString());
	else
		TBaseError::SetHresult(HRESULT_FROM_WIN32(dwError));
	return *this;
}

CEvtSpyError&    CEvtSpyError::operator= (const HRESULT hError)
{
	*((CSysError*)this) = hError;
	return *this;
}

CEvtSpyError&    CEvtSpyError::operator= (const CSysError& err_ref)
{
	*((CSysError*)this) = err_ref;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CEvtSpyState::CEvtSpyState(void) : m_value(CEvtSpyState::eUndefined)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID     CEvtSpyState::Append(const CEvtSpyState::_e _state) { m_value |= _state; }
VOID     CEvtSpyState::Clear(void) { m_value = CEvtSpyState::eUndefined; }

bool     CEvtSpyState::IsConnected(void)const { return (0 != (CEvtSpyState::eConnected & m_value) && global::GetDriverPort().IsValid()); }
bool     CEvtSpyState::IsInitialized(void)const { return (0 != (CEvtSpyState::eInitialized & m_value)); }
bool     CEvtSpyState::IsInstalled(void)const { return (0 != (CEvtSpyState::eInstalled & m_value)); }
VOID     CEvtSpyState::Modify(const CEvtSpyState::_e _state, const bool _set) { if (_set) this->Append(_state); else this->Remove(_state); }
VOID     CEvtSpyState::Remove(const CEvtSpyState::_e _state) { m_value &= ~_state; }

/////////////////////////////////////////////////////////////////////////////

CEvtSpy_Base::CEvtSpy_Base(LPCTSTR lpszModuleName) { m_error.Source(lpszModuleName); }

/////////////////////////////////////////////////////////////////////////////

TErrorRef       CEvtSpy_Base::Error(void)const { return m_error; }

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace um { namespace spy { namespace details
{
	HRESULT  CEvtSpy_CrtData(CServiceCrtData& _crt)
	{
		CEvtSpyNames spy_names_;

		_crt.Account()     = _T("NT AUTHORITY\\LocalSystem");
		_crt.Description() = _T("Initec system file event spy minifilter driver");
		_crt.DisplayName() = _T("Initec System Protection File Guardian");
		_crt.Name()        =  spy_names_.Name();
		_crt.Options()     = (CServiceCrtOption::eStoppable
		                    | CServiceCrtOption::eCanShutdown
							| CServiceCrtOption::eStartOnDemand
							| CServiceCrtOption::eFileSysDriver);
		_crt.Path()        =  spy_names_.Executable();

		HRESULT hr_ = spy_names_.LastResult();
		return  hr_;
	}

	CServiceManager&
		CEvtSpy_GetSvcManRef(void)
	{
		static CServiceManager svc_man;
		return svc_man;
	}

	class    CEvtSpy_Regestry
	{
	public:
		CEvtSpy_Regestry(void){}
	public:
		TErrorRef      ApplyInstallScript(CSysError& _err)
		{
			_err =  S_OK;

			CEvtSpyNames evt_spy_;

			TRegistryPathChanges changes_;
			try {
				changes_.insert(
					::std::make_pair(
						CAtlString(_T("{$DriverName}")), evt_spy_.Name()
					)
				);
			}
			catch (::std::bad_alloc&){
				return (_err = E_OUTOFMEMORY);
			}
			//
			// TODO: storage extention must have registry root key and options;
			//
			CRegistryStorage_Ext ext_;
			HRESULT hr_ = ext_.ImportFromResource(IDR_REG_DRIVE_INSTALL, changes_);
			if (FAILED(hr_)) {
				_err.SetState(
					hr_, _T("%s; Driver registry script is required;"), ext_.Error().GetDescription()
				);
			}
			return _err;
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CEvtSpyInstaller::CEvtSpyInstaller(void) : TBase(_T("CEvtSpyInstaller")) {}

/////////////-////////////////////////////////////////////////////////////////

HRESULT       CEvtSpyInstaller::Install(const bool bIgonreIfInstalled)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const bool bIgnoreIfInited = true;
	CServiceManager& manager_  = details::CEvtSpy_GetSvcManRef();
	HRESULT hr_ = manager_.Initialize(
		SC_MANAGER_ALL_ACCESS, bIgnoreIfInited
	);
	if (FAILED(hr_))
		return (m_error = manager_.Error());

	if (this->State().IsInstalled()){
		if (bIgonreIfInstalled == false)
			m_error.SetState(
			(DWORD)ERROR_ALREADY_INITIALIZED,
				_T("The event spy driver is already installed.")
			);
		return m_error;
	}

	CServiceCrtData crt_;
	hr_ = details::CEvtSpy_CrtData(crt_);
	if (FAILED(hr_))
		return (m_error = hr_);

	const bool bInstalled = manager_.IsInstalled(crt_);
	hr_ = manager_.Error();
	if (FAILED(hr_))
		return (m_error = manager_.Error());

	if (bInstalled == false)
	{
		CService filter_;
		hr_ = manager_.Create(
			crt_, filter_
		);
		if (FAILED(hr_))
			return (m_error = manager_.Error());
	}
	else {
	}

	details::CEvtSpy_Regestry stg_;
	stg_.ApplyInstallScript(m_error);
	if (!m_error)
		m_state.Append(CEvtSpyState::eInstalled);

	return m_error;
}

bool          CEvtSpyInstaller::IsInstalled(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;
	bool bInstalled = false;

	if (this->State().IsInstalled()){
		return (bInstalled = true);
	}

	const bool bIgnoreIfInited = true;
	CServiceManager& manager_  = details::CEvtSpy_GetSvcManRef();
	HRESULT hr_ = manager_.Initialize(
		SC_MANAGER_ALL_ACCESS, bIgnoreIfInited
	);
	if (FAILED(hr_)) {
		m_error = manager_.Error();
		return bInstalled;
	}

	CServiceCrtData crt_;
	hr_ = details::CEvtSpy_CrtData(crt_);
	if (FAILED(hr_))
		return (m_error = hr_);

	bInstalled = manager_.IsInstalled(crt_);
	hr_ = manager_.Error();
	if (FAILED(hr_)){
		bInstalled = false;
		m_error = manager_.Error();
	}

	if (bInstalled)
		m_state.Append(CEvtSpyState::eInstalled);

	return bInstalled;
}

const
CEvtSpyState& CEvtSpyInstaller::State(void)const { return m_state; }

HRESULT       CEvtSpyInstaller::Uninstall(const bool bIgonreIfUninstalled)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const bool bInstalled = this->IsInstalled();
	if (bInstalled == false){
		if (bIgonreIfUninstalled == false)
			m_error.SetState(
			(DWORD)ERROR_INVALID_STATE,
				_T("The event spy driver is already uninstalled.")
			);
		return m_error;
	}

	CServiceCrtData crt_;
	HRESULT hr_ = details::CEvtSpy_CrtData(crt_);
	if (FAILED(hr_))
		return (m_error = hr_);

	const bool bIgnoreIfInited = true;
	CServiceManager& manager_  = details::CEvtSpy_GetSvcManRef();
	hr_ = manager_.Initialize(
		SC_MANAGER_ALL_ACCESS, bIgnoreIfInited
	);
	if (FAILED(hr_)) {
		return (m_error = manager_.Error());
	}

	hr_ = manager_.Stop(crt_.Name());
	if (FAILED(hr_)){
		if (ERROR_SERVICE_NOT_ACTIVE != manager_.Error().GetCode()){
			m_error = manager_.Error();
			return m_error;
		}
	}

	hr_ = manager_.Remove(crt_.Name());
	if (FAILED(hr_))
		m_error = manager_.Error();

	m_state.Remove(CEvtSpyState::eInstalled);

	const bool bIgnoreIfUninited = true;
	manager_.Uninitialize(bIgnoreIfUninited);

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CEvtSpyModule::CEvtSpyModule(void) : TBase(_T("CEvtSpyModule")), m_reader(NULL)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	try {
		m_reader = new CDriverReader();
	}
	catch(::std::bad_alloc&) {
		m_error = E_OUTOFMEMORY;
	}
}

CEvtSpyModule::~CEvtSpyModule(void)
{
	try {
		if (m_reader){
			delete m_reader; m_reader = NULL;
		}
	}
	catch(...){}
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CEvtSpyModule::Connect(const bool bIgnoreIfConnected)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (this->State().IsConnected()){
		if (!bIgnoreIfConnected)
			m_error.SetState(
			(DWORD)ERROR_ALREADY_INITIALIZED,
				_T("FG event spy driver is already connected.")
			);
		return m_error;
	}

	CServiceCrtData crt_;
	HRESULT hr_ = details::CEvtSpy_CrtData(crt_);
	if (FAILED(hr_))
		return (m_error = hr_);

	const bool bIgnoreIfInited = true;
	CServiceManager& manager_  = details::CEvtSpy_GetSvcManRef();
	hr_ = manager_.Initialize(
		SC_MANAGER_ALL_ACCESS, bIgnoreIfInited
	);
	if (FAILED(hr_)) {
		return (m_error = manager_.Error());
	}

	hr_ = manager_.Start(crt_.Name());
	if (FAILED(hr_)){
		if (ERROR_SERVICE_ALREADY_RUNNING != manager_.Error().GetCode()){
			m_error = manager_.Error();
			return m_error;
		}
		else {
		}
	}

	hr_ = ::FilterConnectCommunicationPort(
		_EVT_SPY_PORT_RAW_NAME, 0, NULL, 0, NULL, &global::GetDriverPort()
	);
	if (FAILED(hr_))
		return (m_error = hr_);

	m_state.Append(CEvtSpyState::eConnected);

	if (NULL != m_reader){
		CDriverReader* pReader = (reinterpret_cast<CDriverReader*>(m_reader));
		hr_ = pReader->Start();
		if (FAILED(hr_))
			m_error = pReader->Error();
	}
	return m_error;
}

HRESULT       CEvtSpyModule::Disconnect(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (this->State().IsConnected() == false)
		return m_error;

	CServiceCrtData crt_;
	HRESULT hr_ = details::CEvtSpy_CrtData(crt_);
	if (FAILED(hr_))
		return (m_error = hr_);

	if (NULL != m_reader){
		CDriverReader* pReader = (reinterpret_cast<CDriverReader*>(m_reader));
		if (pReader)
			hr_ = pReader->Stop();
	}

	const bool bIgnoreIfInited = true;
	CServiceManager& manager_  = details::CEvtSpy_GetSvcManRef();
	hr_ = manager_.Initialize(
		SC_MANAGER_ALL_ACCESS, bIgnoreIfInited
	);
	if (FAILED(hr_)) {
		return (m_error = manager_.Error());
	}

	hr_ = manager_.Stop(crt_.Name());
	if (FAILED(hr_)){
		if (ERROR_SERVICE_NOT_ACTIVE != manager_.Error().GetCode()){
			m_error = manager_.Error();
			return m_error;
		}
	}
	m_state.Remove(CEvtSpyState::eConnected);
	global::GetDriverPort().Reset();

	const bool bIgnoreIfUninited = true;
	manager_.Uninitialize(bIgnoreIfUninited);

	return m_error;
}

const
CEvtSpyState& CEvtSpyModule::State(void)const { return m_state;     }