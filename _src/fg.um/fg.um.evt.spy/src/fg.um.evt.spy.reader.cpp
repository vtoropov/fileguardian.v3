/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jun-2018 at 2:31:20a, UTC+7, Phuket, Rawai, Saturday;
	This is File Guardian minifilter driver event spy data reader class implementation file.
*/
#include "StdAfx.h"
#include "fg.um.evt.spy.reader.h"
#include "fg.um.evt.spy.dumper.h"

using namespace	fg::um::spy;

#include "Shared_SystemCore.h"
#include "Shared_GenericEvent.h"
#include "generic.stg.data.h"

using namespace shared::lite::sys_core;
using namespace shared::runnable;
using namespace shared::lite::data;

#include "fg.fs.evt.spy.iface.h"

using namespace shared::km::spy;

#if defined(_DEBUG)
	#include <intrin.h>
#endif
/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace um { namespace spy { namespace details
{
	UINT WINAPI CDriverReader_ThreadFunc(PVOID pObject)
	{
		if (NULL == pObject)
			return 1;

		CGenericRunnableObject* pRunnable  = NULL;
		CDriverReader*          pReader    = NULL;
		try
		{
			pRunnable  = reinterpret_cast<CGenericRunnableObject*>(pObject);
			pReader    = reinterpret_cast<CDriverReader*>(pRunnable);
		}
		catch (::std::bad_cast&) { return 1; }

		CCoInitializer com_lib(false);
		if (!com_lib.IsSuccess())
			return 1;

		DWORD bytesReturned   = 0;
		CRawData raw_recs(sizeof(TFs_EvtData) * _EVT_RECORD_BUFFER_CAP);
		if (!raw_recs.IsValid())
			return 1;

		TFs_EvtBuffer buffer_ = {
			_EVT_RECORD_BUFFER_CAP, reinterpret_cast<PTFs_EvtData>(raw_recs.GetData())
		};

		HRESULT hr_  = S_OK;

		TFs_Command cmd_ = {
			eFsCommandType::eGetEvents, 0
		};

		CDriverReader&  runner_ = *pReader;
		CGenericWaitCounter waiter_(50, 500);

		while(runner_.IsRunning())
		{
			waiter_.Wait();
			if (waiter_.IsElapsed())
				waiter_.Reset();
			else
				continue;
			//
			// restores *available* records count in the buffer;
			//
			buffer_.uRecCount = _EVT_RECORD_BUFFER_CAP;
			//
			// NOTE: the actual buffer size is not provided, because of use a pointer to record data;
			//
			hr_ = ::FilterSendMessage(
				global::GetDriverPort(), &cmd_, sizeof(TFs_Command), &buffer_, sizeof(buffer_), &bytesReturned
			);
			if (IS_ERROR(hr_))
			{
				if (HRESULT_FROM_WIN32( ERROR_INVALID_HANDLE ) == hr_)
				{
					runner_.m_error.SetState(
							(DWORD) ERROR_INVALID_STATE,
							_T("File Guardian event spy has unloaded.")
						);
					global::_out::LogError(runner_.m_error);
					break;
				}
				else
				if (HRESULT_FROM_WIN32( ERROR_NO_MORE_ITEMS ) != hr_)
				{
					runner_.m_error = hr_;
					global::_out::LogError(runner_.m_error);
					break;
				}
				continue;
			}

			//
			//  Logic to decode records and output them to console or database;
			//
			for (ULONG l_ = 0; l_ < buffer_.uRecCount; l_++)
			{
				CDataDumper::ToConsole(
						buffer_.pRecords[l_]
					);

			} // for(;;)
		} // while

		runner_._MarkCompleted();

		return 0;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CDriverReader::CDriverReader(void) : 
	TRunnable(details::CDriverReader_ThreadFunc, *this, _variant_t((LONG)1)),
	m_error(this->m_sync_obj)
{
	m_error.Source(_T("CDriverReader"));
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CDriverReader::GenericEvent_OnNotify(const _variant_t v_evt_id)
{
	HRESULT hr_ = TRunnable::Stop(false);
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef         CDriverReader::Error(void)const { return m_error; }

HRESULT           CDriverReader::Interrupt(void)
{
	if (TRunnable::IsStopped())
		return S_OK;
	const bool bForce = true;
	HRESULT hr_ = TRunnable::Stop(bForce);
	return  hr_;
}

HRESULT           CDriverReader::Start(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = TRunnable::Start(); if (FAILED(hr_)) m_error = hr_;
	return  hr_;
}

HRESULT           CDriverReader::Stop (void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const bool bForce = false;
	HRESULT hr_ = TRunnable::Stop(bForce); if (FAILED(hr_)) m_error = hr_;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

VOID              CDriverReader::_MarkCompleted(void)
{
	const bool bAsync = true;

	TRunnable::MarkCompleted();
	TRunnable::Event().Fire(bAsync);
}