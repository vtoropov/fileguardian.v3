/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Jun-2017 at 12:31:31p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver data output/dump class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 9-Jun-2018 at 0:41:26a, UTC+7, Phuket, Rawai, Saturday;
*/
#include "StdAfx.h"
#include "fg.um.evt.spy.dumper.h"
#include "fg.um.evt.spy.resource.h"

using namespace fg::um::spy;
using namespace shared::km::spy;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace um { namespace spy { namespace details
{
	CAtlString DataDamper_FormatSystemTime(SYSTEMTIME& _time)
	{
		CAtlString cs_time;
		cs_time.Format(
				_T("%02d/%02d/%04d %02d:%02d:%02d:%03d"), _time.wMonth, _time.wDay, _time.wYear, _time.wHour, _time.wMinute, _time.wSecond, _time.wMilliseconds
			);
		return cs_time;
	}

	CAtlString DataDumper_IrpMinorToText(const ULONG _major, const ULONG _minor)
	{
		CAtlString cs_minor;
		switch (_major)
		{
		case IRP_MJ_READ :
		case IRP_MJ_WRITE:
			if      (IRP_MN_NORMAL           == _minor) cs_minor.LoadString(IDS_MN_NORMAL);
			else if (IRP_MN_DPC              == _minor) cs_minor.LoadString(IDS_MN_DPC);
			else if (IRP_MN_MDL              == _minor) cs_minor.LoadString(IDS_MN_MDL);
			else if (IRP_MN_COMPLETE         == _minor) cs_minor.LoadString(IDS_MN_COMPLETE);
			else if (IRP_MN_COMPRESSED       == _minor) cs_minor.LoadString(IDS_MN_COMPRESSED);
			else if (IRP_MN_MDL_DPC          == _minor) cs_minor.LoadString(IDS_MN_MDL_DPC);
			else if (IRP_MN_COMPLETE_MDL     == _minor) cs_minor.LoadString(IDS_MN_COMPLETE_MDL);
			else if (IRP_MN_COMPLETE_MDL_DPC == _minor) cs_minor.LoadString(IDS_MN_COMPLETE_MDL_DPC);
			break;
		case IRP_MJ_DIRECTORY_CONTROL:
			if      (IRP_MN_QUERY_DIRECTORY  == _minor) cs_minor.LoadString(IDS_MN_QUERY_DIRECTORY);
			else if (IRP_MN_NOTIFY_CHANGE_DIRECTORY == _minor) cs_minor.LoadString(IDS_MN_NOTIFY_CHANGE_DIRECTORY);
			break;
		case IRP_MJ_FILE_SYSTEM_CONTROL:
			if      (IRP_MN_USER_FS_REQUEST  == _minor) cs_minor.LoadString(IDS_MN_USER_FS_REQUEST);
			else if (IRP_MN_MOUNT_VOLUME     == _minor) cs_minor.LoadString(IDS_MN_MOUNT_VOLUME);
			else if (IRP_MN_VERIFY_VOLUME    == _minor) cs_minor.LoadString(IDS_MN_VERIFY_VOLUME);
			else if (IRP_MN_LOAD_FILE_SYSTEM == _minor) cs_minor.LoadString(IDS_MN_LOAD_FILE_SYSTEM);
			else if (IRP_MN_TRACK_LINK       == _minor) cs_minor.LoadString(IDS_MN_TRACK_LINK);
			break;
		case IRP_MJ_DEVICE_CONTROL:
			if      (IRP_MN_SCSI_CLASS       == _minor) cs_minor.LoadString(IDS_MN_SCSI_CLASS);
			break;
		case IRP_MJ_LOCK_CONTROL:
			if      (IRP_MN_LOCK             == _minor) cs_minor.LoadString(IDS_MN_LOCK);
			else if (IRP_MN_UNLOCK_SINGLE    == _minor) cs_minor.LoadString(IDS_MN_UNLOCK_SINGLE);
			else if (IRP_MN_UNLOCK_ALL       == _minor) cs_minor.LoadString(IDS_MN_UNLOCK_ALL);
			else if (IRP_MN_UNLOCK_ALL_BY_KEY== _minor) cs_minor.LoadString(IDS_MN_UNLOCK_ALL_BY_KEY);
			break;
		case IRP_MJ_POWER:
			if      (IRP_MN_WAIT_WAKE        == _minor) cs_minor.LoadString(IDS_MN_WAIT_WAKE);
			else if (IRP_MN_POWER_SEQUENCE   == _minor) cs_minor.LoadString(IDS_MN_POWER_SEQUENCE);
			else if (IRP_MN_SET_POWER        == _minor) cs_minor.LoadString(IDS_MN_SET_POWER);
			else if (IRP_MN_QUERY_POWER      == _minor) cs_minor.LoadString(IDS_MN_QUERY_POWER);
			break;
		case IRP_MJ_SYSTEM_CONTROL:
			if      (IRP_MN_QUERY_ALL_DATA   == _minor) cs_minor.LoadString(IDS_MN_QUERY_ALL_DATA);
			else if (IRP_MN_QUERY_SINGLE_INSTANCE   == _minor) cs_minor.LoadString(IDS_MN_QUERY_SINGLE_INSTANCE);
			else if (IRP_MN_CHANGE_SINGLE_INSTANCE  == _minor) cs_minor.LoadString(IDS_MN_CHANGE_SINGLE_INSTANCE);
			else if (IRP_MN_CHANGE_SINGLE_ITEM      == _minor) cs_minor.LoadString(IDS_MN_CHANGE_SINGLE_ITEM);
			else if (IRP_MN_ENABLE_EVENTS           == _minor) cs_minor.LoadString(IDS_MN_ENABLE_EVENTS);
			else if (IRP_MN_DISABLE_EVENTS          == _minor) cs_minor.LoadString(IDS_MN_DISABLE_EVENTS);
			else if (IRP_MN_ENABLE_COLLECTION       == _minor) cs_minor.LoadString(IDS_MN_ENABLE_COLLECTION);
			else if (IRP_MN_DISABLE_COLLECTION      == _minor) cs_minor.LoadString(IDS_MN_DISABLE_COLLECTION);
			else if (IRP_MN_REGINFO          == _minor) cs_minor.LoadString(IDS_MN_REGINFO);
			else if (IRP_MN_EXECUTE_METHOD   == _minor) cs_minor.LoadString(IDS_MN_EXECUTE_METHOD);
			break;
		case IRP_MJ_PNP:
			if      (IRP_MN_START_DEVICE     == _minor) cs_minor.LoadString(IDS_MN_START_DEVICE);
			else if (IRP_MN_QUERY_REMOVE_DEVICE     == _minor) cs_minor.LoadString(IDS_MN_QUERY_REMOVE_DEVICE);
			else if (IRP_MN_REMOVE_DEVICE    == _minor) cs_minor.LoadString(IDS_MN_REMOVE_DEVICE);
			else if (IRP_MN_CANCEL_REMOVE_DEVICE    == _minor) cs_minor.LoadString(IDS_MN_CANCEL_REMOVE_DEVICE);
			else if (IRP_MN_STOP_DEVICE      == _minor) cs_minor.LoadString(IDS_MN_STOP_DEVICE);
			else if (IRP_MN_QUERY_STOP_DEVICE       == _minor) cs_minor.LoadString(IDS_MN_QUERY_STOP_DEVICE);
			else if (IRP_MN_CANCEL_STOP_DEVICE      == _minor) cs_minor.LoadString(IDS_MN_CANCEL_STOP_DEVICE);
			else if (IRP_MN_QUERY_DEVICE_RELATIONS  == _minor) cs_minor.LoadString(IDS_MN_QUERY_DEVICE_RELATIONS);
			else if (IRP_MN_QUERY_INTERFACE  == _minor) cs_minor.LoadString(IDS_MN_QUERY_INTERFACE);
			else if (IRP_MN_QUERY_CAPABILITIES      == _minor) cs_minor.LoadString(IDS_MN_QUERY_CAPABILITIES);
			else if (IRP_MN_QUERY_RESOURCES  == _minor) cs_minor.LoadString(IDS_MN_QUERY_RESOURCES);
			else if (IRP_MN_QUERY_RESOURCE_REQUIREMENTS  == _minor) cs_minor.LoadString(IDS_MN_QUERY_RESOURCE_REQUIREMENTS);
			else if (IRP_MN_QUERY_DEVICE_TEXT       == _minor) cs_minor.LoadString(IDS_MN_QUERY_DEVICE_TEXT);
			else if (IRP_MN_FILTER_RESOURCE_REQUIREMENTS == _minor) cs_minor.LoadString(IDS_MN_FILTER_RESOURCE_REQUIREMENTS);
			else if (IRP_MN_READ_CONFIG      == _minor) cs_minor.LoadString(IDS_MN_READ_CONFIG);
			else if (IRP_MN_WRITE_CONFIG     == _minor) cs_minor.LoadString(IDS_MN_WRITE_CONFIG);
			else if (IRP_MN_EJECT            == _minor) cs_minor.LoadString(IDS_MN_EJECT);
			else if (IRP_MN_SET_LOCK         == _minor) cs_minor.LoadString(IDS_MN_SET_LOCK);
			else if (IRP_MN_QUERY_ID         == _minor) cs_minor.LoadString(IDS_MN_QUERY_ID);
			else if (IRP_MN_QUERY_PNP_DEVICE_STATE  == _minor) cs_minor.LoadString(IDS_MN_QUERY_PNP_DEVICE_STATE);
			else if (IRP_MN_QUERY_BUS_INFORMATION   == _minor) cs_minor.LoadString(IDS_MN_QUERY_BUS_INFORMATION);
			else if (IRP_MN_DEVICE_USAGE_NOTIFICATION    == _minor) cs_minor.LoadString(IDS_MN_DEVICE_USAGE_NOTIFICATION);
			else if (IRP_MN_SURPRISE_REMOVAL == _minor) cs_minor.LoadString(IDS_MN_SURPRISE_REMOVAL);
			else if (IRP_MN_QUERY_LEGACY_BUS_INFORMATION == _minor) cs_minor.LoadString(IDS_MN_QUERY_LEGACY_BUS_INFORMATION);
			break;
		case IRP_MJ_TRANSACTION_NOTIFY:
			if      (TRANSACTION_NOTIFY_BEGIN       == _minor) cs_minor.LoadString(IDS_TRANS_BEGIN);
			else if (TRANSACTION_NOTIFY_PREPREPARE_CODE  == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_PREPREPARE);
			else if (TRANSACTION_NOTIFY_PREPARE_CODE     == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_PREPARE);
			else if (TRANSACTION_NOTIFY_COMMIT_CODE      == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_COMMIT);
			else if (TRANSACTION_NOTIFY_COMMIT_FINALIZE_CODE     == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_COMMIT_FINALIZE);
			else if (TRANSACTION_NOTIFY_ROLLBACK_CODE    == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_ROLLBACK);
			else if (TRANSACTION_NOTIFY_PREPREPARE_COMPLETE_CODE == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_PREPREPARE_COMPLETE);
			else if (TRANSACTION_NOTIFY_PREPARE_COMPLETE_CODE    == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_PREPARE_COMPLETE);
			else if (TRANSACTION_NOTIFY_ROLLBACK_COMPLETE_CODE   == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_ROLLBACK_COMPLETE);
			else if (TRANSACTION_NOTIFY_RECOVER_CODE     == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_RECOVER);
			else if (TRANSACTION_NOTIFY_SINGLE_PHASE_COMMIT_CODE == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_SINGLE_PHASE_COMMIT);
			else if (TRANSACTION_NOTIFY_DELEGATE_COMMIT_CODE     == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_DELEGATE_COMMIT);
			else if (TRANSACTION_NOTIFY_RECOVER_QUERY_CODE       == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_RECOVER_QUERY);
			else if (TRANSACTION_NOTIFY_ENLIST_PREPREPARE_CODE   == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_ENLIST_PREPREPARE);
			else if (TRANSACTION_NOTIFY_LAST_RECOVER_CODE        == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_LAST_RECOVER);
			else if (TRANSACTION_NOTIFY_INDOUBT_CODE     == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_INDOUBT);
			else if (TRANSACTION_NOTIFY_PROPAGATE_PULL_CODE      == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_PROPAGATE_PULL);
			else if (TRANSACTION_NOTIFY_PROPAGATE_PUSH_CODE      == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_PROPAGATE_PUSH);
			else if (TRANSACTION_NOTIFY_MARSHAL_CODE     == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_MARSHAL);
			else if (TRANSACTION_NOTIFY_ENLIST_MASK_CODE == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_ENLIST_MASK);
			break;
		}
		if (cs_minor.IsEmpty())
			cs_minor.Format(
				_T("Unknown Irp minor code (%u) for major one (%u)"), _minor, _major
			);
		return cs_minor;
	}

	VOID DataDumper_IrpCodeToText(const ULONG _major, const ULONG _minor, CAtlString& _cs_major, CAtlString& _cs_minor)
	{
		switch (_major)
		{
		case IRP_MJ_CREATE                   : _cs_major.LoadString(IDS_MJ_CREATE);            break;
		case IRP_MJ_CREATE_NAMED_PIPE        : _cs_major.LoadString(IDS_MJ_CREATE_NAMED_PIPE); break;
		case IRP_MJ_CLOSE                    : _cs_major.LoadString(IDS_MJ_CLOSE);             break;
		case IRP_MJ_READ                     : _cs_major.LoadString(IDS_MJ_READ);
			_cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_WRITE                    : _cs_major.LoadString(IDS_MJ_WRITE);
			_cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_QUERY_INFORMATION        : _cs_major.LoadString(IDS_MJ_QUERY_INFORMATION); break;
		case IRP_MJ_SET_INFORMATION          : _cs_major.LoadString(IDS_MJ_SET_INFORMATION);   break;
		case IRP_MJ_QUERY_EA                 : _cs_major.LoadString(IDS_MJ_QUERY_EA);          break;
		case IRP_MJ_SET_EA                   : _cs_major.LoadString(IDS_MJ_SET_EA);            break;
		case IRP_MJ_FLUSH_BUFFERS            : _cs_major.LoadString(IDS_MJ_FLUSH_BUFFERS);     break;
		case IRP_MJ_QUERY_VOLUME_INFORMATION : _cs_major.LoadString(IDS_MJ_QUERY_VOLUME_INFORMATION); break;
		case IRP_MJ_SET_VOLUME_INFORMATION   : _cs_major.LoadString(IDS_MJ_SET_VOLUME_INFORMATION);   break;
		case IRP_MJ_DIRECTORY_CONTROL        : _cs_major.LoadString(IDS_MJ_DIRECTORY_CONTROL);
			_cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_FILE_SYSTEM_CONTROL      : _cs_major.LoadString(IDS_MJ_FILE_SYSTEM_CONTROL);
			_cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_DEVICE_CONTROL           : _cs_major.LoadString(IDS_MJ_DEVICE_CONTROL);
			_cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_INTERNAL_DEVICE_CONTROL  : _cs_major.LoadString(IDS_MJ_INTERNAL_DEVICE_CONTROL);  break;
		case IRP_MJ_SHUTDOWN                 : _cs_major.LoadString(IDS_MJ_SHUTDOWN);                 break;
		case IRP_MJ_LOCK_CONTROL             : _cs_major.LoadString(IDS_MJ_LOCK_CONTROL);
			_cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_CLEANUP                  : _cs_major.LoadString(IDS_MJ_CLEANUP);                  break;
		case IRP_MJ_CREATE_MAILSLOT          : _cs_major.LoadString(IDS_MJ_CREATE_MAILSLOT);          break;
		case IRP_MJ_QUERY_SECURITY           : _cs_major.LoadString(IDS_MJ_QUERY_SECURITY);           break;
		case IRP_MJ_SET_SECURITY             : _cs_major.LoadString(IDS_MJ_SET_SECURITY);             break;
		case IRP_MJ_POWER                    : _cs_major.LoadString(IDS_MJ_POWER);
			_cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_SYSTEM_CONTROL           : _cs_major.LoadString(IDS_MJ_SYSTEM_CONTROL);
			_cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_DEVICE_CHANGE            : _cs_major.LoadString(IDS_MJ_DEVICE_CHANGE);            break;
		case IRP_MJ_QUERY_QUOTA              : _cs_major.LoadString(IDS_MJ_QUERY_QUOTA);              break;
		case IRP_MJ_SET_QUOTA                : _cs_major.LoadString(IDS_MJ_SET_QUOTA);                break;
		case IRP_MJ_PNP                      : _cs_major.LoadString(IDS_MJ_PNP);
			_cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_ACQUIRE_FOR_SECTION_SYNCHRONIZATION : _cs_major.LoadString(IDS_MJ_ACQUIRE_FOR_SECTION_SYNC);   break;
		case IRP_MJ_RELEASE_FOR_SECTION_SYNCHRONIZATION : _cs_major.LoadString(IDS_MJ_RELEASE_FOR_SECTION_SYNC);   break;
		case IRP_MJ_ACQUIRE_FOR_MOD_WRITE    : _cs_major.LoadString(IDS_MJ_ACQUIRE_FOR_MOD_WRITE);    break;
		case IRP_MJ_RELEASE_FOR_MOD_WRITE    : _cs_major.LoadString(IDS_MJ_RELEASE_FOR_MOD_WRITE);    break;
		case IRP_MJ_ACQUIRE_FOR_CC_FLUSH     : _cs_major.LoadString(IDS_MJ_ACQUIRE_FOR_CC_FLUSH);     break;
		case IRP_MJ_RELEASE_FOR_CC_FLUSH     : _cs_major.LoadString(IDS_MJ_RELEASE_FOR_CC_FLUSH);     break;
		case IRP_MJ_NOTIFY_STREAM_FO_CREATION: _cs_major.LoadString(IDS_MJ_NOTIFY_STREAM_FO_CREATION);break;
		case IRP_MJ_FAST_IO_CHECK_IF_POSSIBLE: _cs_major.LoadString(IDS_MJ_FAST_IO_CHECK_IF_POSSIBLE);break;
		case IRP_MJ_NETWORK_QUERY_OPEN       : _cs_major.LoadString(IDS_MJ_NETWORK_QUERY_OPEN);       break;
		case IRP_MJ_MDL_READ                 : _cs_major.LoadString(IDS_MJ_MDL_READ);                 break;
		case IRP_MJ_MDL_READ_COMPLETE        : _cs_major.LoadString(IDS_MJ_MDL_READ_COMPLETE);        break;
		case IRP_MJ_PREPARE_MDL_WRITE        : _cs_major.LoadString(IDS_MJ_PREPARE_MDL_WRITE);        break;
		case IRP_MJ_MDL_WRITE_COMPLETE       : _cs_major.LoadString(IDS_MJ_MDL_WRITE_COMPLETE);       break;
		case IRP_MJ_VOLUME_MOUNT             : _cs_major.LoadString(IDS_MJ_VOLUME_MOUNT);             break;
		case IRP_MJ_VOLUME_DISMOUNT          : _cs_major.LoadString(IDS_MJ_VOLUME_DISMOUNT);          break;
		case IRP_MJ_TRANSACTION_NOTIFY       : _cs_major.LoadString(IDS_MJ_TRANSACTION_NOTIFY);
			_cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		}

		if (_cs_major.IsEmpty())_cs_major = _T("#n/a");
		if (_cs_minor.IsEmpty())_cs_minor = _T("#n/a");
	}

	CAtlString DataDumper_OriginTimeToText(const LARGE_INTEGER _time)
	{
		FILETIME   local_  = {0};
		SYSTEMTIME system_ = {0};

		FileTimeToLocalFileTime((FILETIME *)&_time, &local_);
		FileTimeToSystemTime(&local_, &system_);

		CAtlString cs_time = DataDamper_FormatSystemTime(system_);
		return cs_time;
	}

	CAtlString DataDumper_SeqNumberToText(const ULONG _seq_n)
	{
		CAtlString cs_seq;
		cs_seq.Format(
				_T("%08X"), _seq_n
			);
		return cs_seq;
	}

}}}}

/////////////////////////////////////////////////////////////////////////////

VOID CDataDumper::ToConsole(
			__in const TFs_EvtData& _rec
			)
{
	CAtlString cs_maj;
	CAtlString cs_min;

	details::DataDumper_IrpCodeToText(_rec.uFuncMajor, _rec.uFuncMinor, cs_maj, cs_min); 

	CAtlString cs_dump;
	cs_dump.Format(
		_T("\n\t[Timestamp]\t\t%s\n\t[Function Major]\t\t%s\n\t[Function Minor]\t%s\n\t[Affected]\t%s\n\t[ProcessId]\t%u\n"),
		details::DataDumper_OriginTimeToText(_rec.luTimestamp).GetString(),
		cs_maj.GetString(),
		cs_min.GetString(),
		global::GetLocalFromUnc(_rec.wFileAffected).GetString(),
		_rec.luProcessId

	);
	global::_out::LogInfo( cs_dump );
	return;
}