#ifndef _SHAREDCFGSTDAFX_H_E9B06BCA_1A30_48FB_9046_218A18467053_INCLUDED
#define _SHAREDCFGSTDAFX_H_E9B06BCA_1A30_48FB_9046_218A18467053_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Aug-2017 at 2:56:48pm, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver configuration precompiled headers declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 29-May-2018 at 7:26:21p, UTC+7, Phuket, Rawai, Tuesday;
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600 // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600 // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700 // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>
#include <vector>
#include <map>

#endif/*_SHAREDCFGSTDAFX_H_E9B06BCA_1A30_48FB_9046_218A18467053_INCLUDED*/