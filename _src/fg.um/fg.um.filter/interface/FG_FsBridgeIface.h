#ifndef _FGFSBRIDGEIFACE_B87A1A3E_96EF_4814_8CD0_7EDF2353D515_INCLUDED
#define _FGFSBRIDGEIFACE_B87A1A3E_96EF_4814_8CD0_7EDF2353D515_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Jun-2017 at 7:27:28a, UTC+7, Phuket, Rawai, Thursday;
	This is File Guardian minifilter driver bridge interface declaration file.
*/
#include "FG_FsBridgeDefs.h"
#include "FG_FsBridgeCache.h"

namespace fg { namespace common { namespace filter
{
	class CDriverState
	{
	public:
		enum _e{
			eUndefined   = 0x0,
			eInitialized = 0x1,
			eInstalled   = 0x2,
			eConnected   = 0x4,
		};
	private:
		DWORD           m_value;
	public:
		CDriverState(void);
	public:
		VOID            Add(const _e);
		VOID            Clear(void);
		bool            IsConnected(void)const;
		bool            IsInitialized(void)const;
		bool            IsInstalled(void)const;
		VOID            Remove(const _e);
		VOID            Set(const _e, const bool _add); // if _add is true the state is appended, otherwise is removed;
	};

	class CDriverIface_Base {
	protected:
		mutable
		CDriverError    m_error;
	protected:
		CDriverIface_Base(LPCTSTR lpszModuleName);
	public:
		TErrorRef       Error(void)const;                           // the last operation error object
	};

	class CDriverInstaller : public CDriverIface_Base {
		typedef CDriverIface_Base TBase;
	private:
		CDriverState    m_state;
	public:
		CDriverInstaller(void);
	public:
		HRESULT         Install(const bool bIgonreIfInstalled);     // installs the filter driver, if it is not done yet;
		bool            IsInstalled(void);                          // checks the filter driver installtion by querying service manager;
		const
		CDriverState&   State(void)const;                           // gets current state of the installation;
		HRESULT         Uninstall(const bool bIgonreIfUninstalled); // deletes filter driver from registry
	};

	class CDriverFolders : public CDriverIface_Base {
		typedef CDriverIface_Base TBase;
	public:
		CDriverFolders(void);
		~CDriverFolders(void);
	public:
		HRESULT         Compare(LPCTSTR lpszUncPath);   // sends a command to the filter for comparing a path provided with currently watched folders; output to log file;
		HRESULT         UpdateFolders(void);            // sends a command to the filter driver to update the include/exclude lists from the registry;
	};

	class CDriverLog : public CDriverIface_Base {
		typedef CDriverIface_Base TBase;
	public:
		CDriverLog(void);
		~CDriverLog(void);
	public:
		DWORD           Options(void)const;
		HRESULT         Options(const DWORD);
		CAtlString      Path(void)const;
		HRESULT         Path(LPCTSTR);
		DWORD           Verbose(void)const;
		HRESULT         Verbose(const DWORD);
	};

	class CDriverRemovable : public CDriverIface_Base {
		typedef CDriverIface_Base TBase;
	public:
		CDriverRemovable(void);
	public:
		HRESULT         Append(LPCTSTR lpszDosPath);
		HRESULT         Delete(LPCTSTR lpszDosPath);
	};

	class CDriverBridge : public CDriverIface_Base {
		typedef CDriverIface_Base TBase;
	private:
		CDriverState    m_state;
		HANDLE          m_reader;                       // reads data from filter
		CDriverFolders  m_folders;
		CDriverLog      m_log;
		CDriverRemovable
		                m_removable;
	public:
		CDriverBridge(IDriverEventAdoptedSink&);
		~CDriverBridge(void);
	public:
		HRESULT         Connect(const bool bIgnoreIfConnected); // connects to the server port for data exchanging;
		HRESULT         Disconnect(void);               // unloads the service manager and makes other cleaning;
		CDriverFolders& Folders(void);                  // gets indirect access to minifilter watch folder list;
		CDriverLog&     Log  (void);                    // gets driver log command set object;
		CDriverRemovable&
		                Removable(void);                // gets driver removable list direct access;
		const
		CDriverState&   State(void)const;               // current state of the filter driver;
	};
}}}

#endif/*_FGFSBRIDGEIFACE_B87A1A3E_96EF_4814_8CD0_7EDF2353D515_INCLUDED*/