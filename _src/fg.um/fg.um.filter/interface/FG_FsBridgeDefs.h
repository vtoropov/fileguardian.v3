#ifndef _FGFSBRIDGEDEFS_H_EF114B63_B985_4a1e_988F_E3A68BC681D2_INCLUDED
#define _FGFSBRIDGEDEFS_H_EF114B63_B985_4a1e_988F_E3A68BC681D2_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Jun-2017 at 04:27:27a, UTC+7, Phuket, Rawai, Thursday;
	This is File Guardian minifilter driver bridge common definition(s) declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_FS_CommonDefs.h"
#include "FG_FsFilterIface.Defs.h"

namespace fg { namespace common { namespace filter
{
	using shared::lite::common::CSysError;
	using shared::ntfs::COperateType;
	using shared::ntfs::CObjectType;

	class CDriverError :
		 public  CSysError
	{
		typedef  CSysError TBaseError;
	public:
		explicit CDriverError(void);
		explicit CDriverError(const DWORD dwError);
		explicit CDriverError(const HRESULT);
	public:
		CDriverError&    operator= (const DWORD);
		CDriverError&    operator= (const HRESULT);
		CDriverError&    operator= (const CSysError&);
	};

	class CEventTime {
	private:
		time_t       m_origin_stamp;
		time_t       m_finish_stamp;

	public:
		CEventTime(void);
		CEventTime(const LARGE_INTEGER& _origin, const LARGE_INTEGER& _finish);
		CEventTime(const time_t _origin, const time_t _finish);
		~CEventTime(void);

	public:
		time_t       Completion(void)const;
		HRESULT      Completion(const time_t);
		VOID         CompletionAsSysTime(SYSTEMTIME&)const;
		INT          GapWithCurrent(const bool bFinish = true)const;
		time_t       Originated(void)const;
		HRESULT      Originated(const time_t);
		VOID         OriginatedAsSysTime(SYSTEMTIME&)const;
		HRESULT      Set(const LARGE_INTEGER& _origin, const LARGE_INTEGER& _finish, const bool bTimezone = true);
	};

	class CEventProc {
	private:
		ULONG        m_pid;
		CAtlString   m_path;
	public:
		CEventProc(void);
		CEventProc(const CEventProc&);
		~CEventProc(void);
	public:
		ULONG        Id(void)const;
		HRESULT      Id(const ULONG);
		bool         IsValid(void)const;
		LPCTSTR      Path(void)const;
		HRESULT      Path(LPCTSTR);
		const
		CAtlString&  PathAsObject(void)const;
		CAtlString&  PathAsObject(void);
	public:
		CEventProc&  operator=(const CEventProc&);
	};
	/*
		***Important***
		Action type is set to kernel mode file event enumeration instead of regular user mode ones; 
	*/
	class CEventItem {

	private:
		CEventTime       m_time;
		CObjectType::_e  m_eType;
		eFsEventType::_e m_eAction;
		CAtlString       m_source;
		CAtlString       m_target;
		LONGLONG         m_size;
		LONGLONG         m_time_ex;
		CEventProc       m_proc;      // a process info; the process initiated this event;

	public:
		CEventItem(void);
		~CEventItem(void);

	public:
		const
		eFsEventType::_e Action(void)const;
		HRESULT          Action(const eFsEventType::_e);
		const
		CEventProc&      Process(void)const;
		CEventProc&      Process(void);
		LONGLONG         Size(void)const;
		VOID             Size(const LONGLONG);
		LPCTSTR          Source(void)const;
		HRESULT          Source(LPCTSTR lpszPath);
		LPCTSTR          Target(void)const;
		HRESULT          Target(LPCTSTR lpszPath);
		const
		CAtlString&      TargetAsObject(void)const;
		const
		CEventTime&      Time(void)const;
		CEventTime&      Time(void);
		LONGLONG         TimeEx(void)const;
		VOID             TimeEx(const LONGLONG);
		const
		CObjectType::_e  Type(void) const;
		HRESULT          Type(const CObjectType::_e);
	public:
		bool operator==(LPCTSTR lpszTarget)const;
		bool operator!=(LPCTSTR lpszTarget)const;
		bool operator==(const  CEventItem&)const;
		bool operator!=(const  CEventItem&)const;
	};

	interface IDriverEventAdoptedSink {
		virtual HRESULT IDriverEventAdopted_OnError(TErrorRef) PURE;
		virtual HRESULT IDriverEventAdopted_OnEvent(const CEventItem&) PURE;
	};
}}}

#endif/*_FGFSBRIDGEDEFS_H_EF114B63_B985_4a1e_988F_E3A68BC681D2_INCLUDED*/