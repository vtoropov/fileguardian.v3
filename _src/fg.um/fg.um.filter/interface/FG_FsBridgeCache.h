#ifndef _FGFSBRIDGECACHE_H_A7243A97_B112_4d3c_AB1F_AF5B64A975B3_INCLUDED
#define _FGFSBRIDGECACHE_H_A7243A97_B112_4d3c_AB1F_AF5B64A975B3_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Apr-2018 at 3:26:15p, UTC+7, Novosibirsk, Rodniki, Wednesday;
	This is File Guardian minifilter driver bridge file event item data cache interface declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_FS_CommonDefs.h"
#include "Shared_SystemThreadPool.h"
#include "FG_FsBridgeDefs.h"

namespace fg { namespace common { namespace filter
{
	using shared::ntfs::CObjectType;
	using shared::ntfs::COperateType;
	using shared::lite::common::CSysError;
	using shared::lite::common::CSysErrorSafe;
	using shared::lite::sys_core::CThreadBase;

	class CCacheCompare {
	public:
		bool operator() (const CEventTime& _left, const CEventTime& _right) const {
			return (_left.Originated() < _right.Originated()
				|| (_left.Originated() == _right.Originated() && _left.Completion() < _right.Completion())
				);
		} 
	};

	interface ICacheCallback {
	public:
		virtual HRESULT ICacheCallback_OnError(TErrorRef) PURE;
		virtual HRESULT ICacheCallback_OnItemArrive(const CEventItem&) PURE;
	};

	typedef ::std::queue<time_t>             TCacheItemIndex;
	typedef ::std::map<time_t, CEventItem>   TCacheItemMap;

	class CEventCache : public IDriverEventAdoptedSink {
	public:
		enum _e {
			eAcceptableSize = 100, // is not used for now;
#if defined(_DEBUG)
			eAcceptableTime =   3,
#else
			eAcceptableTime =  50, // time in seconds when cached item will be flushed from the cache;
#endif
		};
	private:
		ICacheCallback& m_sink;
		TCacheItemMap   m_items;
	public:
		CEventCache(ICacheCallback& _sink);
		~CEventCache(void);
	private:
		CEventCache(const CEventCache&);
		CEventCache& operator=(const CEventCache&);
	private:
		virtual HRESULT IDriverEventAdopted_OnError(TErrorRef) override sealed;
		virtual HRESULT IDriverEventAdopted_OnEvent(const CEventItem&) override sealed;
	};

}}}

#endif/*_FGFSBRIDGECACHE_H_A7243A97_B112_4d3c_AB1F_AF5B64A975B3_INCLUDED*/