#ifndef _FGFSBRIDGEREADER_H_14B9274B_EA0B_46c6_B108_5FDF13CC7CD1_INCLUDED
#define _FGFSBRIDGEREADER_H_14B9274B_EA0B_46c6_B108_5FDF13CC7CD1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Jun-2017 at 8:47:44p, UTC+7, Phuket, Rawai, Thursday;
	This is File Guardian minifilter driver data reader class declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_GenericRunnableObject.h"

#include "FG_FsBridgeDefs.h"

namespace fg { namespace common { namespace filter { namespace _impl
{
	using shared::lite::common::CSysError;
	using shared::lite::common::CSysErrorSafe;

	using shared::runnable::CGenericRunnableObject;
	using shared::runnable::IGenericEventNotify;
	using shared::lite::sync::CGenericSyncObject;

	using fg::common::filter::IDriverEventAdoptedSink;

	namespace details {
		UINT WINAPI DriverReaderThread_Func(VOID* pObject);
	}

	class CDriverReader :
		protected CGenericRunnableObject,
		private   IGenericEventNotify
	{
		typedef   CGenericRunnableObject TRunnable;
		friend    UINT WINAPI details::DriverReaderThread_Func(VOID* pObject);
	private:
		CGenericSyncObject   m_sync_obj;
		CSysErrorSafe        m_error;
	private:
		IDriverEventAdoptedSink&
		                     m_evt_sink;
	public:
		CDriverReader(IDriverEventAdoptedSink&);
	private:
		virtual HRESULT  GenericEvent_OnNotify(const _variant_t v_evt_id) override sealed;
	public:
		TErrorRef            Error(void)const;
		HRESULT              Interrupt(void);
		HRESULT              Start(void);
		HRESULT              Stop (void);
	private:
		VOID                _MarkCompleted(void);
	};
}}}}
#endif/*_FGFSBRIDGEREADER_H_14B9274B_EA0B_46c6_B108_5FDF13CC7CD1_INCLUDED*/