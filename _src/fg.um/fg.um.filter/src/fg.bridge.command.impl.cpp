/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-May-2018 at 1:08:50p, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian minifilter driver bridge command interface implementation file.
*/
#include "StdAfx.h"
#include "fg.bridge.command.impl.h"

using namespace fg::common::filter::_impl;

#include "generic.stg.data.h"

using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

CDriverCommand_Base::CDriverCommand_Base(LPCTSTR lpszModuleName) { m_error.Source(lpszModuleName); }
CDriverCommand_Base::~CDriverCommand_Base(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef     CDriverCommand_Base::Error(void)const { return m_error; }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDriverCommand_Base::_ExchangeDword (const eFsCommandType::_e _type,  const ULONG _flags, ULONG& _value) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	TFs_Command cmd_;
	::memset(&cmd_, 0, sizeof(TFs_Command));

	cmd_.eCmdType  = _type;
	cmd_.nFlags    = _flags;
	cmd_.nNumeric  = _value;
	HRESULT hr_ =  this->_Perform(&cmd_);
	if (SUCCEEDED(hr_))
		_value = cmd_.nNumeric;
	return hr_;
}

HRESULT       CDriverCommand_Base::_ExchangeString(const eFsCommandType::_e _type,  const ULONG _flags, CAtlString& _data ) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	TFs_Command cmd_;
	::memset(&cmd_, 0, sizeof(TFs_Command));

	cmd_.eCmdType  = _type;
	cmd_.nFlags    = _flags;

	const size_t tRequired = (_data.GetLength() > eFsCommandOpt::eWDataLen - 1 ? 
		eFsCommandOpt::eWDataLen - 1 : _data.GetLength());

	if (tRequired) {

		const DWORD dw_req_ = static_cast<DWORD>(tRequired) * sizeof(TCHAR);

		::_tcscpy_s(
			cmd_.wData, _countof(cmd_.wData) - 1, _data.GetString()
		);
		cmd_.nDataLen = static_cast<ULONG>(tRequired) * sizeof(WCHAR);
	}
	return this->_Perform(&cmd_);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDriverCommand_Base::_Perform(PTFs_Command _p_cmd) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;
	const
	DWORD dwRequired = sizeof(TFs_Command);
	DWORD dwReturned = 0;
	//
	// TODO: dwReturned is always zero; it seems to be a care of the driver response message function to assign this value;
	//
	HRESULT hr_ = ::FilterSendMessage(
		global::GetDriverPort(), _p_cmd, dwRequired, NULL, 0, &dwReturned
	);

	if (FAILED(hr_))
		m_error = hr_;

	return  m_error;
}

/////////////////////////////////////////////////////////////////////////////

CDriverFodlerCommand::CDriverFodlerCommand(void) : TBase(_T("CDriverFodlerCommand")) {}
CDriverFodlerCommand::~CDriverFodlerCommand(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDriverFodlerCommand::Compare(const CCmdAction::_e, LPCTSTR lpszUncPath) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_path(lpszUncPath);
	if (cs_path.IsEmpty())
		return (m_error = E_INVALIDARG);

	HRESULT hr_ = TBase::_ExchangeString(
		eFsCommandType::eComparePath, TRUE, cs_path
	);
	return hr_;
}

HRESULT       CDriverFodlerCommand::Update (const CCmdAction::_e, DWORD& _val) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = TBase::_ExchangeDword(
		eFsCommandType::eUpdateFilter, TRUE, _val
	);
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

CDriverLogCommand::CDriverLogCommand(void) : TBase(_T("CDriverLogCommand"))  {}
CDriverLogCommand::~CDriverLogCommand(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDriverLogCommand::Options(const CCmdAction::_e _act, DWORD& _val) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = TBase::_ExchangeDword(
		eFsCommandType::eLogOptions, _act, _val
	);
	return hr_;
}

HRESULT       CDriverLogCommand::Path(const CCmdAction::_e _act, CAtlString& _val) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = TBase::_ExchangeString(
		eFsCommandType::eLogFilePath, _act, _val
	);
	return hr_;
}

HRESULT       CDriverLogCommand::Verbose(const CCmdAction::_e _act, ULONG& _val) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = this->_ExchangeDword(
		eFsCommandType::eLogVerbose, _act, _val
	);
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

CDriverRemoveCommand::CDriverRemoveCommand(void) : TBase(_T("CDriverRemoveCommand")){}
CDriverRemoveCommand::~CDriverRemoveCommand(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDriverRemoveCommand::Append(const CCmdAction::_e, CAtlString& _val) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = TBase::_ExchangeString(
		eFsCommandType::eRemovableAppend, TRUE, _val
	);
	return hr_;
}

HRESULT       CDriverRemoveCommand::Delete(const CCmdAction::_e, CAtlString& _val) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = TBase::_ExchangeString(
		eFsCommandType::eRemovableDelete, FALSE, _val
	);
	return hr_;
}