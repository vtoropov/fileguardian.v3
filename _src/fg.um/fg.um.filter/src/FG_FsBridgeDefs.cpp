/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Jun-2017 at 06:32:56a, UTC+7, Phuket, Rawai, Thursday;
	This is File Guardian minifilter driver bridge common definition(s) implementation file.
*/
#include "StdAfx.h"
#include "FG_FsBridgeDefs.h"

using namespace fg::common::filter;

#include "Shared_DateTime.h"

using namespace shared::lite::data;
/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace filter { namespace details
{
	CAtlString DriverError_FormatMessage(const DWORD dwError)
	{
		TCHAR szBuffer[_MAX_PATH] = {0};

		HMODULE hModule = ::LoadLibraryExW( _T("fltlib.dll"), NULL, LOAD_LIBRARY_AS_DATAFILE );

		::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_FROM_HMODULE,
						hModule,
						dwError,
						MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL),
						szBuffer,
						_MAX_PATH - 1,
						NULL);

		::ATL::CAtlString msg_(szBuffer);

		if (hModule != NULL)
			::FreeLibrary( hModule );
		return msg_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CDriverError::CDriverError(void) : TBaseError()
{
}

CDriverError::CDriverError(const DWORD dwError) : TBaseError(dwError)
{
}

CDriverError::CDriverError(const HRESULT hError) : TBaseError(hError)
{
}

/////////////////////////////////////////////////////////////////////////////

CDriverError&    CDriverError::operator= (const DWORD dwError)
{
	::ATL::CAtlString cs_error = details::DriverError_FormatMessage(dwError);
	if (cs_error.IsEmpty()){
		(CSysError)(*this) = dwError; // uses default format implementation ;
		cs_error = this->m_buffer;
	}
	if (!cs_error.IsEmpty())
		TBaseError::SetState(dwError, cs_error.GetString());
	else
		TBaseError::SetHresult(HRESULT_FROM_WIN32(dwError));
	return *this;
}

CDriverError&    CDriverError::operator= (const HRESULT hError)
{
	*((CSysError*)this) = hError;
	return *this;
}

CDriverError&    CDriverError::operator= (const CSysError& err_ref)
{
	*((CSysError*)this) = err_ref;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CEventTime::CEventTime(void) : m_origin_stamp(NULL), m_finish_stamp(NULL)
{
}

CEventTime::CEventTime(const LARGE_INTEGER& _origin, const LARGE_INTEGER& _finish) : m_origin_stamp(NULL), m_finish_stamp(NULL)
{
	this->Set(_origin, _finish);
}

CEventTime::CEventTime(const time_t _origin, const time_t _finish) : m_origin_stamp(_origin), m_finish_stamp(_finish)
{
}

CEventTime::~CEventTime(void)
{
}

/////////////////////////////////////////////////////////////////////////////

time_t       CEventTime::Completion(void)const      { return m_finish_stamp; }
HRESULT      CEventTime::Completion(const time_t _v){ if (NULL == _v) return E_INVALIDARG; m_finish_stamp = _v; return S_OK; }
VOID         CEventTime::CompletionAsSysTime(SYSTEMTIME& _time)const
{
	CSystemTime time_(m_finish_stamp);
	_time = time_;
}

INT          CEventTime::GapWithCurrent(const bool bFinish)const {
	//
	// TODO: checks for using local time; it is true by default for now;
	//
	INT n_sec = (bFinish ? CUnixTime(m_finish_stamp, true).GapWithCurrent() : CUnixTime(m_origin_stamp, true).GapWithCurrent());
	return n_sec;
}

time_t       CEventTime::Originated(void)const      { return m_origin_stamp; }
HRESULT      CEventTime::Originated(const time_t _v){ if (NULL == _v) return E_INVALIDARG; m_origin_stamp = _v; return S_OK; }
VOID         CEventTime::OriginatedAsSysTime(SYSTEMTIME& _time)const
{
	CSystemTime time_(m_origin_stamp);
	_time = time_;
}

HRESULT      CEventTime::Set(const LARGE_INTEGER& _origin, const LARGE_INTEGER& _finish, const bool bTimezone)
{
	CUnixTime origin_(bTimezone); origin_ = _origin; m_origin_stamp = origin_;
	CUnixTime finish_(bTimezone); finish_ = _finish; m_finish_stamp = finish_;
	//
	// TODO: handling an error can be important; (done)
	//
	HRESULT hr_ = (origin_.IsValid() && finish_.IsValid() ? S_OK : HRESULT_FROM_WIN32(ERROR_INVALID_DATA));
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CEventProc::CEventProc(void) : m_pid(0) {}
CEventProc::CEventProc(const CEventProc& _proc) : m_pid(0) {  *this = _proc; }
CEventProc::~CEventProc(void) {}

/////////////////////////////////////////////////////////////////////////////

ULONG        CEventProc::Id(void)const        { return m_pid; }
HRESULT      CEventProc::Id(const ULONG _pid) { if (0 == _pid) return E_INVALIDARG; m_pid = _pid; return S_OK; }
bool         CEventProc::IsValid(void)const   { return (0 < m_pid && m_path.IsEmpty() == false);  }
LPCTSTR      CEventProc::Path(void)const      { return m_path.GetString(); }
HRESULT      CEventProc::Path(LPCTSTR _val)   { if (NULL == _val) return E_INVALIDARG; m_path = _val;  return S_OK; }
const
CAtlString&  CEventProc::PathAsObject(void)const { return m_path; }
CAtlString&  CEventProc::PathAsObject(void)      { return m_path; }

/////////////////////////////////////////////////////////////////////////////

CEventProc&  CEventProc::operator=(const CEventProc& _proc) {
	this->Id(_proc.Id());
	this->Path(_proc.Path());
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CEventItem::CEventItem(void) : m_eType(CObjectType::eUnknown), m_eAction(eFsEventType::eFsEventUnknown), m_size(0), m_time_ex(0) { }
CEventItem::~CEventItem(void) { }

/////////////////////////////////////////////////////////////////////////////

const
eFsEventType::_e CEventItem::Action(void)const                   { return m_eAction; }
HRESULT          CEventItem::Action(const eFsEventType::_e _v)   { if (eFsEventType::eFsEventUnknown == _v) return E_INVALIDARG; m_eAction = _v; return S_OK; }
const
CEventProc&      CEventItem::Process (void)const                 { return m_proc; }
CEventProc&      CEventItem::Process (void)                      { return m_proc; }
LONGLONG         CEventItem::Size(void)const                     { return m_size; }
VOID             CEventItem::Size(const LONGLONG _v)             { m_size = _v;   }
LPCTSTR          CEventItem::Source(void)const                   { return m_source.GetString(); }
HRESULT          CEventItem::Source(LPCTSTR lpszPath)            { if (NULL == lpszPath) return E_INVALIDARG; m_source = lpszPath; return S_OK; }
LPCTSTR          CEventItem::Target(void)const                   { return m_target.GetString(); }
HRESULT          CEventItem::Target(LPCTSTR lpszPath)            { if (NULL == lpszPath) return E_INVALIDARG; m_target = lpszPath; return S_OK; }
const CAtlString&CEventItem::TargetAsObject(void)const           { return m_target;}
const
CEventTime&      CEventItem::Time(void)const                     { return m_time;  }
CEventTime&      CEventItem::Time(void)                          { return m_time;  }
LONGLONG         CEventItem::TimeEx(void)const                   { return m_time_ex; }
VOID             CEventItem::TimeEx(const LONGLONG _val)         { m_time_ex = _val; }
const
CObjectType::_e  CEventItem::Type(void) const                    { return m_eType; }
HRESULT          CEventItem::Type(const CObjectType::_e _v)      { if (CObjectType::eUnknown == _v) return E_INVALIDARG; m_eType = _v; return S_OK; }

/////////////////////////////////////////////////////////////////////////////

bool CEventItem::operator==(LPCTSTR lpszTarget)const { return (NULL != lpszTarget && 0 == m_target.CompareNoCase(lpszTarget)); }
bool CEventItem::operator!=(LPCTSTR lpszTarget)const { return (NULL == lpszTarget || 0 != m_target.CompareNoCase(lpszTarget)); }

bool CEventItem::operator==(const  CEventItem& _right)const { return *this==_right.Target(); }
bool CEventItem::operator!=(const  CEventItem& _right)const { return *this!=_right.Target(); }