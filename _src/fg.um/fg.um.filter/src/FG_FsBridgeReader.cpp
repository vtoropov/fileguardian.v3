/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Jun-2017 at 6:40:13a, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver data reader class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 7:23:21p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "FG_FsBridgeReader.h"
#include "FG_FsBridgeDumper.h"

using namespace	fg::common::filter;
using namespace	fg::common::filter::_impl;

#include "Shared_SystemCore.h"
#include "Shared_GenericEvent.h"
#include "generic.stg.data.h"

using namespace shared::lite::sys_core;
using namespace shared::runnable;
using namespace shared::lite::data;

#include "FG_FsFilterIface.h"
#include "FG_FsBridgeDumper.h"

#if defined(_DEBUG)
#include <intrin.h>
#endif
/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace filter { namespace _impl { namespace details
{
	UINT WINAPI DriverReaderThread_Func(VOID* pObject)
	{
		if (NULL == pObject)
			return 1;

#if defined(___DEBUG)
		__debugbreak();
#endif

		CGenericRunnableObject* pRunnable  = NULL;
		CDriverReader*          pReader    = NULL;
		try
		{
			pRunnable  = reinterpret_cast<CGenericRunnableObject*>(pObject);
			pReader    = reinterpret_cast<CDriverReader*>(pRunnable);
		}
		catch (::std::bad_cast&) { return 1; }
		CCoInitializer com_lib(false);
		if (!com_lib.IsSuccess())
			return 1;

		DWORD bytesReturned   = 0;
		CRawData raw_recs(sizeof(TFs_Record) * _LOG_RECORD_BUFFER_CAP);
		if (!raw_recs.IsValid())
			return 1;

		TFs_Buffer buffer_ = {
			_LOG_RECORD_BUFFER_CAP, reinterpret_cast<PTFs_Record>(raw_recs.GetData())
		};

		HRESULT hr_  = S_OK;

		TFs_Command cmd_ = {
			eFsCommandType::eGetEvents, 0, 0, {0}
		};

		CDriverReader&  runner_ = *pReader;
		CGenericWaitCounter waiter_(50, 500);

		while(runner_.IsRunning())
		{
			waiter_.Wait();
			if (waiter_.IsElapsed())
				waiter_.Reset();
			else
				continue;
			//
			// restores *available* records count in the buffer;
			//
			buffer_.uRecCount = _LOG_RECORD_BUFFER_CAP;
			//
			// NOTE: the actual buffer size is not provided, because of use a pointer to record data;
			//
			hr_ = ::FilterSendMessage(
				global::GetDriverPort(), &cmd_, sizeof(TFs_Command), &buffer_, sizeof(buffer_), &bytesReturned
			);
			if (IS_ERROR(hr_))
			{
				if (HRESULT_FROM_WIN32( ERROR_INVALID_HANDLE ) == hr_)
				{
					runner_.m_error.SetState(
							(DWORD) ERROR_INVALID_STATE,
							_T("File Guardian driver has unloaded.")
						);
#if defined(__OUTPUT_TO_CONSOLE)
					global::_out::LogError(runner_.m_error);
#endif
					runner_.m_evt_sink.IDriverEventAdopted_OnError(runner_.m_error);
					break;
				}
				else
				if (HRESULT_FROM_WIN32( ERROR_NO_MORE_ITEMS ) != hr_)
				{
					runner_.m_error = hr_;
#if defined(__OUTPUT_TO_CONSOLE)
					global::_out::LogError(runner_.m_error);
#endif
					runner_.m_evt_sink.IDriverEventAdopted_OnError(runner_.m_error);

					break;
				}
				continue;
			}

			//
			//  Logic to decode records and output them to console or database;
			//
			for (ULONG l_ = 0; l_ < buffer_.uRecCount; l_++)
			{
#if defined(__OUTPUT_TO_CONSOLE)
				CDataDumper::DumpToConsole(
						buffer_.pRecords[l_]
					);
#endif
				CEventItem event_;
				event_.Time().Set(
						buffer_.pRecords[l_].OriginatingTime,
						buffer_.pRecords[l_].CompletionTime
					);

				event_.Source(CDataDumperEx::DriverUncNameToLocal(buffer_.pRecords[l_].SourceObject ));
				event_.Target(CDataDumperEx::DriverUncNameToLocal(buffer_.pRecords[l_].TargetObject ));
				event_.Action(CDataDumperEx::DriverEventToOperate(buffer_.pRecords[l_].uFsEventType ));
				event_.Type  (CDataDumperEx::DriverObjectToType  (buffer_.pRecords[l_].uFsObjectType));
				event_.Size  (buffer_.pRecords[l_].liSize.QuadPart);
				event_.TimeEx(buffer_.pRecords[l_].CompletionTime.QuadPart);
				event_.Process().Id(buffer_.pRecords[l_].uProcId);

				runner_.m_evt_sink.IDriverEventAdopted_OnEvent(
						event_
					);

			} // for(;;)
		} // while

		runner_._MarkCompleted();

		return 0;
	}
}}}}}

/////////////////////////////////////////////////////////////////////////////

CDriverReader::CDriverReader(IDriverEventAdoptedSink& _sink) : 
	TRunnable(details::DriverReaderThread_Func, *this, _variant_t((LONG)1)),
	m_error(this->m_sync_obj),
	m_evt_sink(_sink)
{
	m_error.Source(_T("CDriverReader"));
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CDriverReader::GenericEvent_OnNotify(const _variant_t v_evt_id)
{
	HRESULT hr_ = TRunnable::Stop(false);
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef         CDriverReader::Error(void)const
{
	return m_error;
}

HRESULT           CDriverReader::Interrupt(void)
{
	if (TRunnable::IsStopped())
		return S_OK;
	const bool bForce = true;
	HRESULT hr_ = TRunnable::Stop(bForce);
	return  hr_;
}

HRESULT           CDriverReader::Start(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = TRunnable::Start(); if (FAILED(hr_)) m_error = hr_;
	return  hr_;
}

HRESULT           CDriverReader::Stop (void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const bool bForce = false;
	HRESULT hr_ = TRunnable::Stop(bForce); if (FAILED(hr_)) m_error = hr_;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

VOID              CDriverReader::_MarkCompleted(void)
{
	const bool bAsync = true;

	TRunnable::MarkCompleted();
	TRunnable::Event().Fire(bAsync);
}