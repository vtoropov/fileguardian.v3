#ifndef _FGBRIDGECOMMANDIMPL_H_F1DD359A_6E63_4122_B3F3_F13FC1742F52_INCLUDED
#define _FGBRIDGECOMMANDIMPL_H_F1DD359A_6E63_4122_B3F3_F13FC1742F52_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-May-2018 at 1:00:07p, UTC+7, Phuket, Rawai, Wednesday;
	This is File Guardian minifilter driver bridge command interface declaration file.
*/
#include "Shared_SystemError.h"
#include "FG_FsFilterIface.h"

namespace fg { namespace common { namespace filter { namespace _impl
{
	using shared::lite::common::CSysError;

	class CCmdAction {
	public:
		enum _e {
			eGet = 0,
			eSet = 1,
		};
	};

	class CDriverCommand_Base {
	protected:
		CSysError       m_error;
	public:
		CDriverCommand_Base(LPCTSTR lpszModuleName);
		~CDriverCommand_Base(void);
	public:
		TErrorRef     Error(void)const;
	protected:
		HRESULT       _ExchangeDword (const eFsCommandType::_e _type,  const ULONG _flags, ULONG& _value);
		HRESULT       _ExchangeString(const eFsCommandType::_e _type,  const ULONG _flags, CAtlString& _data );
	private:
		HRESULT       _Perform(PTFs_Command _p_cmd);
	};

	class CDriverFodlerCommand : public CDriverCommand_Base {
		typedef CDriverCommand_Base TBase;
	public:
		CDriverFodlerCommand(void);
		~CDriverFodlerCommand(void);
	public:
		HRESULT       Compare(const CCmdAction::_e, LPCTSTR lpszUncPath);
		HRESULT       Update (const CCmdAction::_e, DWORD& _val);
	};

	class CDriverLogCommand : public CDriverCommand_Base {
		typedef CDriverCommand_Base TBase;
	public:
		CDriverLogCommand(void);
		~CDriverLogCommand(void);
	public:
		HRESULT       Options(const CCmdAction::_e, DWORD& _val);
		HRESULT       Path   (const CCmdAction::_e, CAtlString& _val);
		HRESULT       Verbose(const CCmdAction::_e, ULONG& _val);
	};

	class CDriverRemoveCommand : public CDriverCommand_Base {
		typedef CDriverCommand_Base TBase;
	public:
		CDriverRemoveCommand(void);
		~CDriverRemoveCommand(void);
	public:
		HRESULT       Append(const CCmdAction::_e, CAtlString& _val);
		HRESULT       Delete(const CCmdAction::_e, CAtlString& _val);
	};
}}}}
#endif /*_FGBRIDGECOMMANDIMPL_H_F1DD359A_6E63_4122_B3F3_F13FC1742F52_INCLUDED*/