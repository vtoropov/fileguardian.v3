/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Apr-2018 at 7:38:58a, UTC+7, Novosibirsk, Rodniki, Wednesday;
	This is File Guardian minifilter driver bridge file event item data cache interface implementation file.
*/
#include "StdAfx.h"
#include "FG_FsBridgeCache.h"

using namespace fg::common::filter;

#include "Shared_FS_CommonDefs.h"

using namespace shared::ntfs;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace filter { namespace details {

	HRESULT CEventCache_GetExePath (const ULONG _pid, CAtlString& _path) {

		typedef ::std::map<ULONG, CAtlString> t_paths;
		static t_paths paths_;
		
		HRESULT hr_ = S_OK;
		if (0 == _pid)
			return (hr_ = E_INVALIDARG);

		t_paths::const_iterator it_ = paths_.find(_pid);
		if (it_ != paths_.end())
			_path = it_->second;
		else {
			hr_ = CGenericPath::PathFromHandle(_pid, _path);
			if (FAILED(hr_))
				return hr_;
			try {
				paths_.insert(
					::std::make_pair(_pid, _path)
				);
			}
			catch(::std::bad_alloc&){
				hr_ = E_OUTOFMEMORY;
			}
		}

		return  hr_;
	}

	HRESULT CEventCache_GetExePath (CEventItem& _evt) {
		return CEventCache_GetExePath(_evt.Process().Id(), _evt.Process().PathAsObject());
	}

	HRESULT CEventCache_LookForSize(TCacheItemIndex& _ndx, TCacheItemMap& _items) {
		HRESULT hr_ = S_OK;
		if (_items.size() < CEventCache::eAcceptableSize)
			return hr_;

		const time_t time_ = _ndx.front(); _ndx.pop();
		TCacheItemMap::const_iterator it_ = _items.find(time_);
		if (it_ != _items.end())
			_items.erase(it_);

		return  hr_;
	}

	HRESULT CEventCache_FlushEvents(TCacheItemMap& _items, const INT nAccepted, const bool bFinishTime = true) {
		HRESULT hr_ = S_OK;
		if (!nAccepted)
			return hr_;

		for (TCacheItemMap::iterator it_ = _items.begin(); it_ != _items.end();) {
			const CEventItem& item_ = it_->second;
			if (nAccepted < item_.Time().GapWithCurrent(bFinishTime))
				_items.erase(it_++);
			else
				 ++it_;
		}

		return  hr_;
	}

	HRESULT CEventCache_OnUIFooter(const CEventItem& _event) {
		HRESULT hr_ = S_OK;
		if (-1 == _event.TargetAsObject().Find(_T("INetCache\\IE\\")))
			return hr_;
		if (-1 == _event.TargetAsObject().Find(_T("fg_footer_")))
			return hr_;

		hr_ = S_FALSE;

		return  hr_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CEventCache::CEventCache(ICacheCallback& _sink) : m_sink(_sink) {
}

CEventCache::~CEventCache(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT CEventCache::IDriverEventAdopted_OnError(TErrorRef _err)
{
	return m_sink.ICacheCallback_OnError(_err);
}

HRESULT CEventCache::IDriverEventAdopted_OnEvent(const CEventItem& _event)
{
	HRESULT hr_ = S_OK;

	TCacheItemMap::const_iterator it_;
#if(0)
	CAtlString cs_path;
	details::CEventCache_GetExePath(_event.Process().Id(), cs_path);
#endif

	switch (_event.Action()) {
		//
		// (a) cached a recycle event for waitng post delete operation;
		//
		case eFsEventType::eFsEventMovedToRecycle:
			{
				it_ = m_items.find(_event.Time().Completion());
				if (it_ == m_items.end()) {

					CEventItem evt_ = _event;
					details::CEventCache_GetExePath(evt_);

					m_items.insert(
						::std::make_pair(_event.Time().Completion(), evt_)
					);
				}
			} break;
		//
		// (a) removes a duplication of delete event;
		// (b) finds event of moving to recycle;
		//
		case eFsEventType::eFsEventDeleted:
			{
				// IE web control generates cache of UI footer, this event must be discarded;
				if (S_FALSE == details::CEventCache_OnUIFooter(_event))
					return hr_;

				it_ = m_items.find(_event.Time().Completion());
				if (it_ == m_items.end()) {

					CEventItem evt_ = _event;
					details::CEventCache_GetExePath(evt_);

					m_items.insert(
						::std::make_pair(_event.Time().Completion(), evt_)
					);
					hr_ = m_sink.ICacheCallback_OnItemArrive(evt_);
				}
				else if (eFsEventType::eFsEventMovedToRecycle == it_->second.Action()) {
					// (1) sends an updated recycle event;
					CEventItem evt_(it_->second);
					evt_.Target(_event.Target());

					hr_ = m_sink.ICacheCallback_OnItemArrive(evt_);
					// (2) removes prior recycle event;
					m_items.erase(it_);
					// (3) stores in cache a delete event for removing a duplication;
					m_items.insert(
						::std::make_pair(_event.Time().Completion(), _event)
					);
				}
				else if (eFsEventType::eFsEventDeleted == it_->second.Action()) {
					// (1) removes a duplication;
					m_items.erase(it_);
				}
			} break;
		//
		// the copy operation consists of two events on the same object at one time:
		// (a) create event, (b) alter/modify event;
		//
		case eFsEventType::eFsEventCreated:
			{
				if (S_FALSE == details::CEventCache_OnUIFooter(_event))
					return hr_;

				it_ = m_items.find(_event.Time().Completion());
				if (it_ == m_items.end()) {

					CEventItem evt_ = _event;
					details::CEventCache_GetExePath(evt_);

					hr_ = m_sink.ICacheCallback_OnItemArrive(evt_);
				}
				else if (eFsEventType::eFsEventAltered == it_->second.Action()
				     &&  _event == it_->second) {

					CEventItem evt_ = _event;
					details::CEventCache_GetExePath(evt_);

					evt_.Action(eFsEventType::eFsEventCopied);

					m_items.erase(it_);
					hr_ = m_sink.ICacheCallback_OnItemArrive(evt_);
				}
			} break;
		//
		// this use case: 
		// (a) excludes multiple change notifications that are applied to particular object at one time;
		// (b) searches simultaneous create event for the same object in order to recognize copy operation;
		//
		case eFsEventType::eFsEventAltered:
			{
				if (S_FALSE == details::CEventCache_OnUIFooter(_event))
					return hr_;

				it_ = m_items.find(_event.Time().Completion());
				// (0) generates original event;
				if (it_ == m_items.end()) {

					CEventItem evt_ = _event;
					details::CEventCache_GetExePath(evt_);

					m_items.insert(
						::std::make_pair(_event.Time().Completion(), evt_)
					);
					hr_ = m_sink.ICacheCallback_OnItemArrive(evt_);
				}
				// (b) searches for create event object
				else if (eFsEventType::eFsEventCreated == it_->second.Action()
				     &&  _event == it_->second) {

					CEventItem evt_ = _event;
					details::CEventCache_GetExePath(evt_);

					evt_.Action(eFsEventType::eFsEventCopied);

					m_items.erase(it_);
					hr_ = m_sink.ICacheCallback_OnItemArrive(evt_);
				}
				else if (_event != it_->second.Target()) {

					CEventItem evt_ = _event;
					details::CEventCache_GetExePath(evt_);

					hr_ = m_sink.ICacheCallback_OnItemArrive(evt_);
				}
			} break;
		default:

			CEventItem evt_ = _event;
			details::CEventCache_GetExePath(evt_);

			hr_ = m_sink.ICacheCallback_OnItemArrive(evt_);
	}

	hr_ = details::CEventCache_FlushEvents(m_items, CEventCache::eAcceptableTime, true);

	return  hr_;
}