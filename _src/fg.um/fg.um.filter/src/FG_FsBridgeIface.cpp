/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Jun-2017 at 7:52:56a, UTC+7, Phuket, Rawai, Thursday;
	This is File Guardian minifilter driver bridge interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 01:07:06a, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "FG_FsBridgeIface.h"
#include "FG_FsFilterIface.h"
#include "FG_FsBridgeReader.h"
#include "FG_FsBridgeResource.h"
#include "FG_Generic_Defs.h"

using namespace fg::common::filter;
using namespace fg::common::filter::_impl;
using namespace fg::common::data;

#include "Shared_GenericHandle.h"
#include "Shared_SystemService.h"
#include "Shared_SystemServiceMan.h"
#include "Shared_Registry.h"
#include "Shared_FS_CommonDefs.h"

using namespace shared::service;
using namespace shared::registry;
using namespace shared::ntfs;

#include "fg.bridge.command.impl.h"

using namespace fg::common::filter::_impl;

/////////////////////////////////////////////////////////////////////////////

CDriverState::CDriverState(void) : m_value(CDriverState::eUndefined)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID     CDriverState::Add(const CDriverState::_e _state) { m_value |= _state; }
VOID     CDriverState::Clear(void) { m_value = CDriverState::eUndefined; }

bool     CDriverState::IsConnected(void)const {
	return (0 != (CDriverState::eConnected & m_value) && global::GetDriverPort().IsValid());
}

bool     CDriverState::IsInitialized(void)const {
	return (0 != (CDriverState::eInitialized & m_value));
}

bool     CDriverState::IsInstalled(void)const {
	return (0 != (CDriverState::eInstalled & m_value));
}

VOID     CDriverState::Remove(const CDriverState::_e _state) { m_value &= ~_state; }
VOID     CDriverState::Set(const CDriverState::_e _state, const bool _add) {
	if (_add)
		this->Add(_state);
	else
		this->Remove(_state);
}

/////////////////////////////////////////////////////////////////////////////

CDriverIface_Base::CDriverIface_Base(LPCTSTR lpszModuleName) {
	m_error.Source(lpszModuleName);
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef       CDriverIface_Base::Error(void)const { return m_error; }

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace filter { namespace details
{
	HRESULT  DriverBridge_GetDriverCrtData(CServiceCrtData& _crt)
	{
		CDriverNames driver_;

		_crt.Account()     = _T("NT AUTHORITY\\LocalSystem");
		_crt.Description() = _T("Initec system file monitor makes a user aware about all file operations");
		_crt.DisplayName() = _T("Initec System Protection File Guardian");
		_crt.Name()        =  driver_.Name();
		_crt.Options()     = (CServiceCrtOption::eStoppable
		                    | CServiceCrtOption::eCanShutdown
							| CServiceCrtOption::eStartOnDemand
							| CServiceCrtOption::eFileSysDriver);
		_crt.Path()        = driver_.Executable();

		HRESULT hr_ = driver_.LastResult();
		return  hr_;
	}

	CServiceManager&
	         DriverBridge_GetServiceManRef(void)
	{
		static CServiceManager svc_man;
		return svc_man;
	}

	class    DriverBridge_Regestry
	{
	public:
		DriverBridge_Regestry(void){}
	public:
		TErrorRef      ApplyInstallScript(CSysError& _err)
		{
			_err =  S_OK;

			CDriverNames driver_;

			TRegistryPathChanges changes_;
			try {
				changes_.insert(
					::std::make_pair(
								CAtlString(_T("{$DriverName}")), driver_.Name()
							)
					);
			}
			catch (::std::bad_alloc&){
				return (_err = E_OUTOFMEMORY);
			}
			//
			// TODO: storage must have predefined root key and options!
			//
			CRegistryStorage_Ext ext_;
			HRESULT hr_ = ext_.ImportFromResource(IDR_REG_DRIVE_INSTALL, changes_);
			if (FAILED(hr_)) {
				_err.SetState(
					hr_, _T("%s; Driver registry script is required;"), ext_.Error().GetDescription()
				);
			}
			return _err;
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CDriverInstaller::CDriverInstaller(void) : TBase(_T("CDriverInstaller")) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDriverInstaller::Install(const bool bIgonreIfInstalled)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

#if defined(__OUTPUT_TO_CONSOLE)
	global::_out::LogInfo(_T("Installing the filter driver..."));
#endif

	const bool bIgnoreIfInited = true;
	CServiceManager& manager_  = details::DriverBridge_GetServiceManRef();
	HRESULT hr_ = manager_.Initialize(
				SC_MANAGER_ALL_ACCESS, bIgnoreIfInited
			);
	if (FAILED(hr_))
		return (m_error = manager_.Error());

	if (this->State().IsInstalled()){
		if (bIgonreIfInstalled == false)
			m_error.SetState(
				(DWORD)ERROR_ALREADY_INITIALIZED,
				_T("The filter driver is already installed.")
			);
		return m_error;
	}

	CServiceCrtData crt_;
	hr_ = details::DriverBridge_GetDriverCrtData(crt_);
	if (FAILED(hr_))
		return (m_error = hr_);

	const bool bInstalled = manager_.IsInstalled(crt_);
	hr_ = manager_.Error();
	if (FAILED(hr_))
		return (m_error = manager_.Error());

	if (bInstalled == false)
	{
#if defined(__OUTPUT_TO_CONSOLE)
	global::_out::LogInfo(_T("The filter driver is not installed. Create the service..."));
#endif
		CService filter_;
		hr_ = manager_.Create(
				crt_, filter_
			);
		if (FAILED(hr_))
			return (m_error = manager_.Error());

#if defined(__OUTPUT_TO_CONSOLE)
	global::_out::LogInfo(_T("The filter driver is installed successfully."));
#endif

	}
	else {
#if defined(__OUTPUT_TO_CONSOLE)
	global::_out::LogWarn(_T("The filter driver is already installed."));
#endif
	}

	details::DriverBridge_Regestry stg_;
	stg_.ApplyInstallScript(m_error);
	if (!m_error)
		 m_state.Add(CDriverState::eInstalled);

	return m_error;
}

bool          CDriverInstaller::IsInstalled(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;
	bool bInstalled = false;

#if defined(__OUTPUT_TO_CONSOLE)
	global::_out::LogInfo(_T("Getting the filter driver installation info..."));
#endif

	if (this->State().IsInstalled()){
#if defined(__OUTPUT_TO_CONSOLE)
	global::_out::LogInfo(_T("The filter driver is installed."));
#endif
		return (bInstalled = true);
	}

	const bool bIgnoreIfInited = true;
	CServiceManager& manager_  = details::DriverBridge_GetServiceManRef();
	HRESULT hr_ = manager_.Initialize(
				SC_MANAGER_ALL_ACCESS, bIgnoreIfInited
			);
	if (FAILED(hr_)) {
		m_error = manager_.Error();
		return bInstalled;
	}

	CServiceCrtData crt_;
	hr_ = details::DriverBridge_GetDriverCrtData(crt_);
	if (FAILED(hr_))
		return (m_error = hr_);

	bInstalled = manager_.IsInstalled(crt_);
	hr_ = manager_.Error();
	if (FAILED(hr_)){
		bInstalled = false;
		m_error = manager_.Error();
	}

#if defined(__OUTPUT_TO_CONSOLE)
	global::_out::LogInfo(
		_T("The filter driver is%sinstalled."), (bInstalled ? _T(" ") : _T(" not "))
	);
#endif
	if (bInstalled)
		m_state.Add(CDriverState::eInstalled);

	return bInstalled;
}

const
CDriverState& CDriverInstaller::State(void)const { return m_state; }

HRESULT       CDriverInstaller::Uninstall(const bool bIgonreIfUninstalled)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

#if defined(__OUTPUT_TO_CONSOLE)
	global::_out::LogInfo(_T("Uninstalling the filter driver..."));
#endif

	const bool bInstalled = this->IsInstalled();
	if (bInstalled == false){
		if (bIgonreIfUninstalled == false)
			m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("The filter driver is already uninstalled.")
			);
		return m_error;
	}

	CServiceCrtData crt_;
	HRESULT hr_ = details::DriverBridge_GetDriverCrtData(crt_);
	if (FAILED(hr_))
		return (m_error = hr_);

	const bool bIgnoreIfInited = true;
	CServiceManager& manager_  = details::DriverBridge_GetServiceManRef();
	hr_ = manager_.Initialize(
				SC_MANAGER_ALL_ACCESS, bIgnoreIfInited
			);
	if (FAILED(hr_)) {
		return (m_error = manager_.Error());
	}

#if defined(__OUTPUT_TO_CONSOLE)
	global::_out::LogInfo(_T("Trying to stop filter driver..."));
#endif

	hr_ = manager_.Stop(crt_.Name());
	if (FAILED(hr_)){
		if (ERROR_SERVICE_NOT_ACTIVE != manager_.Error().GetCode()){
			m_error = manager_.Error();
			return m_error;
		}
	}

#if defined(__OUTPUT_TO_CONSOLE)
	global::_out::LogInfo(_T("The filter driver is stopped."));
	global::_out::LogInfo(_T("Trying to unregister filter driver..."));
#endif

	hr_ = manager_.Remove(crt_.Name());
	if (FAILED(hr_))
		m_error = manager_.Error();

#if defined(__OUTPUT_TO_CONSOLE)
	global::_out::LogInfo(_T("The filter driver is unregistered."));
#endif

	m_state.Remove(CDriverState::eInstalled);

	const bool bIgnoreIfUninited = true;
	manager_.Uninitialize(bIgnoreIfUninited);

	return m_error;
}
/////////////////////////////////////////////////////////////////////////////

CDriverFolders::CDriverFolders(void) : TBase(_T("CDriverFolders")) {}
CDriverFolders::~CDriverFolders(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDriverFolders::Compare(LPCTSTR lpszUncPath) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDriverFodlerCommand cmd_;
	HRESULT hr_ = cmd_.Compare(CCmdAction::eGet, lpszUncPath);
	if (FAILED(hr_))
		m_error = cmd_.Error();

	return m_error;
}

HRESULT       CDriverFolders::UpdateFolders(void) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDriverFodlerCommand cmd_;

	DWORD dummy_= TRUE;
	HRESULT hr_ = cmd_.Update(CCmdAction::eSet, dummy_);
	if (FAILED(hr_))
		m_error = cmd_.Error();
	
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CDriverLog::CDriverLog(void) : TBase(_T("CDriverLog")) {}
CDriverLog::~CDriverLog(void) {}

/////////////////////////////////////////////////////////////////////////////

DWORD         CDriverLog::Options(void)const {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDriverLogCommand cmd_;

	DWORD opts_ = 0;
	HRESULT hr_ = cmd_.Options(CCmdAction::eGet, opts_);
	if (FAILED(hr_))
		m_error = cmd_.Error();

	return opts_;
}

HRESULT       CDriverLog::Options(const DWORD _val){
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDriverLogCommand cmd_;

	DWORD opts_ = _val;
	HRESULT hr_ = cmd_.Options(CCmdAction::eSet, opts_);
	if (FAILED(hr_))
		m_error = cmd_.Error();

	return hr_;
}

CAtlString    CDriverLog::Path(void)const {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_path;

	CDriverLogCommand cmd_;

	HRESULT hr_ = cmd_.Path(CCmdAction::eGet, cs_path);
	if (FAILED(hr_))
		m_error = cmd_.Error();

	hr_ = CGenericPath::ConvertUncToDos(cs_path.GetString(), cs_path);
	if (FAILED(hr_))
		m_error = hr_;
	
	return cs_path;
}

HRESULT       CDriverLog::Path(LPCTSTR lpszDosPath) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == lpszDosPath || ::_tcslen(lpszDosPath) < 1)
		return (m_error = E_INVALIDARG);

	CAtlString cs_path;

	HRESULT hr_ = CGenericPath::ConvertDosToUnc(lpszDosPath, cs_path);
	if (FAILED(hr_))
		return (m_error = hr_);

	CDriverLogCommand cmd_;

	hr_ = cmd_.Path(CCmdAction::eSet, cs_path);
	if (FAILED(hr_))
		m_error = cmd_.Error();

	return m_error;
}

DWORD         CDriverLog::Verbose(void)const {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	ULONG eVerbose = 0;

	CDriverLogCommand cmd_;

	HRESULT hr_ = cmd_.Verbose(CCmdAction::eGet, eVerbose);
	if (FAILED(hr_))
		m_error = cmd_.Error();

	return eVerbose;
}

HRESULT       CDriverLog::Verbose(const DWORD _val) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDriverLogCommand cmd_;
	DWORD eVerbose = _val ;

	HRESULT hr_ = cmd_.Verbose(CCmdAction::eSet, eVerbose);
	if (FAILED(hr_))
		m_error = cmd_.Error();

	return m_error;
}
/////////////////////////////////////////////////////////////////////////////

CDriverRemovable::CDriverRemovable(void) : TBase(_T("CDriverCommand")) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDriverRemovable::Append(LPCTSTR lpszDosPath) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == lpszDosPath || ::_tcslen(lpszDosPath) < 1)
		return (m_error = E_INVALIDARG);

	CAtlString cs_path;

	HRESULT hr_ = CGenericPath::ConvertDosToUnc(lpszDosPath, cs_path);
	if (FAILED(hr_))
		return (m_error = hr_);

	CDriverRemoveCommand cmd_;

	hr_ = cmd_.Append(CCmdAction::eSet, cs_path);
	if (FAILED(hr_))
		m_error = cmd_.Error();

	return m_error;
}

HRESULT       CDriverRemovable::Delete(LPCTSTR lpszDosPath)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == lpszDosPath || ::_tcslen(lpszDosPath) < 1)
		return (m_error = E_INVALIDARG);

	CAtlString cs_path;

	HRESULT hr_ = CGenericPath::ConvertDosToUnc(lpszDosPath, cs_path);
	if (FAILED(hr_))
		return (m_error = hr_);

	CDriverRemoveCommand cmd_;

	hr_ = cmd_.Delete(CCmdAction::eSet, cs_path);
	if (FAILED(hr_))
		m_error = cmd_.Error();

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CDriverBridge::CDriverBridge(IDriverEventAdoptedSink& _evt_sink) : TBase(_T("CDriverBridge")), m_reader(NULL)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	try
	{
		m_reader = new CDriverReader(_evt_sink);
	}
	catch(::std::bad_alloc&)
	{
		m_error = E_OUTOFMEMORY;
	}
}

CDriverBridge::~CDriverBridge(void)
{
	try
	{
		if (m_reader){
			delete m_reader; m_reader = NULL;
		}
	}
	catch(...){}
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDriverBridge::Connect(const bool bIgnoreIfConnected)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (this->State().IsConnected()){
		if (!bIgnoreIfConnected)
			m_error.SetState(
				(DWORD)ERROR_ALREADY_INITIALIZED,
				_T("The filter is already connected.")
			);
		return m_error;
	}

	CServiceCrtData crt_;
	HRESULT hr_ = details::DriverBridge_GetDriverCrtData(crt_);
	if (FAILED(hr_))
		return (m_error = hr_);

	const bool bIgnoreIfInited = true;
	CServiceManager& manager_  = details::DriverBridge_GetServiceManRef();
	hr_ = manager_.Initialize(
				SC_MANAGER_ALL_ACCESS, bIgnoreIfInited
			);
	if (FAILED(hr_)) {
		return (m_error = manager_.Error());
	}

#if defined(__OUTPUT_TO_CONSOLE)
	global::_out::LogInfo(_T("Starting filter driver..."));
	global::_out::LogInfo(crt_.ToString());
#endif

	hr_ = manager_.Start(crt_.Name());
	if (FAILED(hr_)){
		if (ERROR_SERVICE_ALREADY_RUNNING != manager_.Error().GetCode()){
			m_error = manager_.Error();
			return m_error;
		}
		else {
#if defined(__OUTPUT_TO_CONSOLE)
	global::_out::LogWarn(_T("The filter driver is already started."));
#endif
		}
	}

#if defined(__OUTPUT_TO_CONSOLE)
	global::_out::LogInfo(_T("Connecting to driver port..."));
#endif

	hr_ = FilterConnectCommunicationPort(
			DriverPortName, 0, NULL, 0, NULL, &global::GetDriverPort()
		);
	if (FAILED(hr_))
		return (m_error = hr_);

	m_state.Add(CDriverState::eConnected);

	if (NULL != m_reader){
		CDriverReader* pReader = (reinterpret_cast<CDriverReader*>(m_reader));
		hr_ = pReader->Start();
		if (FAILED(hr_))
			m_error = pReader->Error();
	}
	return m_error;
}

HRESULT       CDriverBridge::Disconnect(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (this->State().IsConnected() == false)
		return m_error;

#if defined(__OUTPUT_TO_CONSOLE)
	global::_out::LogInfo(_T("Trying to disconnect from filter driver..."));
#endif

	CServiceCrtData crt_;
	HRESULT hr_ = details::DriverBridge_GetDriverCrtData(crt_);
	if (FAILED(hr_))
		return (m_error = hr_);

	if (NULL != m_reader){
		CDriverReader* pReader = (reinterpret_cast<CDriverReader*>(m_reader));
		if (pReader)
			hr_ = pReader->Stop();
	}

	const bool bIgnoreIfInited = true;
	CServiceManager& manager_  = details::DriverBridge_GetServiceManRef();
	hr_ = manager_.Initialize(
				SC_MANAGER_ALL_ACCESS, bIgnoreIfInited
			);
	if (FAILED(hr_)) {
		return (m_error = manager_.Error());
	}

#if defined(__OUTPUT_TO_CONSOLE)
	global::_out::LogInfo(_T("Trying to stop filter driver..."));
#endif

	hr_ = manager_.Stop(crt_.Name());
	if (FAILED(hr_)){
		if (ERROR_SERVICE_NOT_ACTIVE != manager_.Error().GetCode()){
			m_error = manager_.Error();
			return m_error;
		}
	}
	m_state.Remove(CDriverState::eConnected);
	global::GetDriverPort().Reset();

	const bool bIgnoreIfUninited = true;
	manager_.Uninitialize(bIgnoreIfUninited);

	return m_error;
}

CDriverFolders& 
              CDriverBridge::Folders(void)    { return m_folders;   }
CDriverLog&   CDriverBridge::Log  (void)      { return m_log;       }
CDriverRemovable&
              CDriverBridge::Removable(void)  { return m_removable; }
const
CDriverState& CDriverBridge::State(void)const { return m_state;     }