//
// Created by Tech_dog (ebontrop@gmail.com) on 30-Jun-2017 at 5:37:28p, GMT+7, Phuket, Rawai, Friday;
// This is File Guardian minifilter driver bridge resource identifier declaration file. 
//


#define IDR_REG_DRIVE_INSTALL                         7000
//
// standard IRP_MJ string definitions
//

#define IDS_MJ_CREATE                                 7001
#define IDS_MJ_CREATE_NAMED_PIPE                      7003
#define IDS_MJ_CLOSE                                  7005
#define IDS_MJ_READ                                   7007
#define IDS_MJ_WRITE                                  7009
#define IDS_MJ_QUERY_INFORMATION                      7011
#define IDS_MJ_SET_INFORMATION                        7013
#define IDS_MJ_QUERY_EA                               7015
#define IDS_MJ_SET_EA                                 7017
#define IDS_MJ_FLUSH_BUFFERS                          7019
#define IDS_MJ_QUERY_VOLUME_INFORMATION               7021
#define IDS_MJ_SET_VOLUME_INFORMATION                 7023
#define IDS_MJ_DIRECTORY_CONTROL                      7025
#define IDS_MJ_FILE_SYSTEM_CONTROL                    7027
#define IDS_MJ_DEVICE_CONTROL                         7029
#define IDS_MJ_INTERNAL_DEVICE_CONTROL                7031
#define IDS_MJ_SHUTDOWN                               7033
#define IDS_MJ_LOCK_CONTROL                           7035
#define IDS_MJ_CLEANUP                                7037
#define IDS_MJ_CREATE_MAILSLOT                        7039
#define IDS_MJ_QUERY_SECURITY                         7041
#define IDS_MJ_SET_SECURITY                           7043
#define IDS_MJ_POWER                                  7045
#define IDS_MJ_SYSTEM_CONTROL                         7047
#define IDS_MJ_DEVICE_CHANGE                          7049
#define IDS_MJ_QUERY_QUOTA                            7051
#define IDS_MJ_SET_QUOTA                              7053
#define IDS_MJ_PNP                                    7055
#define IDS_MJ_MAXIMUM_FUNCTION                       7057

//
//  FSFilter string definitions
//

#define IDS_MJ_ACQUIRE_FOR_SECTION_SYNC               7059
#define IDS_MJ_RELEASE_FOR_SECTION_SYNC               7061
#define IDS_MJ_ACQUIRE_FOR_MOD_WRITE                  7063
#define IDS_MJ_RELEASE_FOR_MOD_WRITE                  7065
#define IDS_MJ_ACQUIRE_FOR_CC_FLUSH                   7067
#define IDS_MJ_RELEASE_FOR_CC_FLUSH                   7069
#define IDS_MJ_NOTIFY_STREAM_FO_CREATION              7071

//
//  FAST_IO and other string definitions
//

#define IDS_MJ_FAST_IO_CHECK_IF_POSSIBLE              7073
#define IDS_MJ_DETACH_DEVICE                          7075
#define IDS_MJ_NETWORK_QUERY_OPEN                     7077
#define IDS_MJ_MDL_READ                               7079
#define IDS_MJ_MDL_READ_COMPLETE                      7081
#define IDS_MJ_PREPARE_MDL_WRITE                      7083
#define IDS_MJ_MDL_WRITE_COMPLETE                     7085
#define IDS_MJ_VOLUME_MOUNT                           7087
#define IDS_MJ_VOLUME_DISMOUNT                        7089

//
// Strings for the Irp minor codes
//

#define IDS_MN_QUERY_DIRECTORY                        7091
#define IDS_MN_NOTIFY_CHANGE_DIRECTORY                7093
#define IDS_MN_USER_FS_REQUEST                        7095
#define IDS_MN_MOUNT_VOLUME                           7097
#define IDS_MN_VERIFY_VOLUME                          7099
#define IDS_MN_LOAD_FILE_SYSTEM                       7101
#define IDS_MN_TRACK_LINK                             7103
#define IDS_MN_LOCK                                   7105
#define IDS_MN_UNLOCK_SINGLE                          7107
#define IDS_MN_UNLOCK_ALL                             7109
#define IDS_MN_UNLOCK_ALL_BY_KEY                      7111
#define IDS_MN_NORMAL                                 7113
#define IDS_MN_DPC                                    7115
#define IDS_MN_MDL                                    7117
#define IDS_MN_COMPLETE                               7119
#define IDS_MN_COMPRESSED                             7121
#define IDS_MN_MDL_DPC                                7123
#define IDS_MN_COMPLETE_MDL                           7125
#define IDS_MN_COMPLETE_MDL_DPC                       7127
#define IDS_MN_SCSI_CLASS                             7129
#define IDS_MN_START_DEVICE                           7131
#define IDS_MN_QUERY_REMOVE_DEVICE                    7133
#define IDS_MN_REMOVE_DEVICE                          7135
#define IDS_MN_CANCEL_REMOVE_DEVICE                   7137
#define IDS_MN_STOP_DEVICE                            7139
#define IDS_MN_QUERY_STOP_DEVICE                      7141
#define IDS_MN_CANCEL_STOP_DEVICE                     7143
#define IDS_MN_QUERY_DEVICE_RELATIONS                 7145
#define IDS_MN_QUERY_INTERFACE                        7147
#define IDS_MN_QUERY_CAPABILITIES                     7149
#define IDS_MN_QUERY_RESOURCES                        7151
#define IDS_MN_QUERY_RESOURCE_REQUIREMENTS            7153
#define IDS_MN_QUERY_DEVICE_TEXT                      7155
#define IDS_MN_FILTER_RESOURCE_REQUIREMENTS           7157
#define IDS_MN_READ_CONFIG                            7159
#define IDS_MN_WRITE_CONFIG                           7161
#define IDS_MN_EJECT                                  7163
#define IDS_MN_SET_LOCK                               7165
#define IDS_MN_QUERY_ID                               7167
#define IDS_MN_QUERY_PNP_DEVICE_STATE                 7169
#define IDS_MN_QUERY_BUS_INFORMATION                  7171
#define IDS_MN_DEVICE_USAGE_NOTIFICATION              7173
#define IDS_MN_SURPRISE_REMOVAL                       7175
#define IDS_MN_QUERY_LEGACY_BUS_INFORMATION           7177
#define IDS_MN_WAIT_WAKE                              7179
#define IDS_MN_POWER_SEQUENCE                         7181
#define IDS_MN_SET_POWER                              7183
#define IDS_MN_QUERY_POWER                            7185
#define IDS_MN_QUERY_ALL_DATA                         7187
#define IDS_MN_QUERY_SINGLE_INSTANCE                  7189
#define IDS_MN_CHANGE_SINGLE_INSTANCE                 7191
#define IDS_MN_CHANGE_SINGLE_ITEM                     7193
#define IDS_MN_ENABLE_EVENTS                          7195
#define IDS_MN_DISABLE_EVENTS                         7197
#define IDS_MN_ENABLE_COLLECTION                      7199
#define IDS_MN_DISABLE_COLLECTION                     7201
#define IDS_MN_REGINFO                                7203
#define IDS_MN_EXECUTE_METHOD                         7205

//
//  Transaction notification string definitions.
//

#define IDS_MJ_TRANSACTION_NOTIFY                     7207

#define IDS_TRANS_BEGIN                               7209
#define IDS_TRANS_NOTIFY_PREPREPARE                   7211
#define IDS_TRANS_NOTIFY_PREPARE                      7213
#define IDS_TRANS_NOTIFY_COMMIT                       7215
#define IDS_TRANS_NOTIFY_ROLLBACK                     7217
#define IDS_TRANS_NOTIFY_PREPREPARE_COMPLETE          7219
#define IDS_TRANS_NOTIFY_PREPARE_COMPLETE             7221
#define IDS_TRANS_NOTIFY_COMMIT_COMPLETE              7223
#define IDS_TRANS_NOTIFY_COMMIT_FINALIZE              7225
#define IDS_TRANS_NOTIFY_ROLLBACK_COMPLETE            7227
#define IDS_TRANS_NOTIFY_RECOVER                      7229
#define IDS_TRANS_NOTIFY_SINGLE_PHASE_COMMIT          7231
#define IDS_TRANS_NOTIFY_DELEGATE_COMMIT              7233
#define IDS_TRANS_NOTIFY_RECOVER_QUERY                7235
#define IDS_TRANS_NOTIFY_ENLIST_PREPREPARE            7237
#define IDS_TRANS_NOTIFY_LAST_RECOVER                 7239
#define IDS_TRANS_NOTIFY_INDOUBT                      7241
#define IDS_TRANS_NOTIFY_PROPAGATE_PULL               7243
#define IDS_TRANS_NOTIFY_PROPAGATE_PUSH               7245
#define IDS_TRANS_NOTIFY_MARSHAL                      7247
#define IDS_TRANS_NOTIFY_ENLIST_MASK                  7249
