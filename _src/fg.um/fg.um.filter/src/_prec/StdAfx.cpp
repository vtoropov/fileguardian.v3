/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Jun-2017 at 3:58:19a, GMT+7, Phuket, Rawai, Thursday;
	This is File Guardian minifilter driver bridge library precompiled headers implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 at 27-May-2018 on 9:08:47a, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)

namespace global
{
	CHandleSafe&  GetDriverPort(void)
	{
		static CHandleSafe drv_port = INVALID_HANDLE_VALUE;
		return drv_port;
	}

	CAtlString    GetLocalFromUnc(LPCTSTR lpszUncPath)
	{
		CAtlString cs_path;
		CAtlString cs_origin(lpszUncPath);

		INT iter_ = 0;
		INT n_pos = 0;
		// we need to extract device name from the full path: \\Device\\HarddisksVolumeX\\;
		// maybe, it's better to get such info from driver side (there is such info as volume name),
		// but if would increase an event info package size and requires additional work on driver side;
		// i.e. we need to find a position of 3rd backslash from the start of a path;
		const INT n_parts = 3;

		while (iter_ < n_parts){
			n_pos = cs_origin.Find(_T("\\"), n_pos);
			if (-1 == n_pos)
				break;
			iter_ += 1;
			n_pos += 1;
		}
		if (n_pos !=-1){
			cs_path = cs_origin.Left(n_pos);
		}
		else
			cs_path = lpszUncPath;

		TCHAR buffer_[_MAX_PATH] = {0};
		HRESULT hr_ = FilterGetDosName(cs_path, buffer_, _MAX_PATH);
		if (FAILED(hr_))
			return CAtlString(lpszUncPath);

		cs_path.Format(
			_T("%s\\%s"), buffer_, cs_origin.Right(cs_origin.GetLength() - n_pos)
		);

		return cs_path;
	}

	CAtlString    GetUncFromLocal(LPCTSTR lpszDosPath)
	{
		CAtlString cs_unc;
		CAtlString cs_loc(lpszDosPath);
		return cs_unc;
	}
}