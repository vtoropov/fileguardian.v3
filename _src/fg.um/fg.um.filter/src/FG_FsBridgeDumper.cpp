/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Jun-2017 at 12:31:31p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver data output/dump class(es) implementation file.
*/
#include "StdAfx.h"
#include "FG_FsBridgeDumper.h"
#include "FG_FsBridgeResource.h"

using namespace fg::common::filter;
using namespace fg::common::filter::_impl;

#include "Shared_FS_GenericFile.h"
#include "Shared_FS_GenericFolder.h"

using namespace shared::ntfs;

//
//  Values set for the Flags field in a RECORD_DATA structure.
//  These flags come from the FLT_CALLBACK_DATA structure.
//

#define FLT_CALLBACK_DATA_IRP_OPERATION         0x00000001  //  set for Irp operations
#define FLT_CALLBACK_DATA_FAST_IO_OPERATION     0x00000002  //  set for Fast Io operations
#define FLT_CALLBACK_DATA_FS_FILTER_OPERATION   0x00000004  //  set for FsFilter operations

#define TIME_BUFFER_LENGTH 20
#define TIME_ERROR         "#time_error"

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace filter { namespace details
{
#if defined(__OUTPUT_TO_CONSOLE)
	CAtlString DataDumper_EventTypeToText(const ULONG _type)
	{
		CAtlString cs_type;

		if (eFsEventType::eFsEventAltered & _type) { if (!cs_type.IsEmpty()) cs_type += _T("|"); cs_type += _T("eFsEventAltered"); }
		if (eFsEventType::eFsEventCopied  & _type) { if (!cs_type.IsEmpty()) cs_type += _T("|"); cs_type += _T("eFsEventCopied" ); }
		if (eFsEventType::eFsEventCreated & _type) { if (!cs_type.IsEmpty()) cs_type += _T("|"); cs_type += _T("eFsEventCreated"); }
		if (eFsEventType::eFsEventDeleted & _type) { if (!cs_type.IsEmpty()) cs_type += _T("|"); cs_type += _T("eFsEventDeleted"); }
		if (eFsEventType::eFsEventRenamed & _type) { if (!cs_type.IsEmpty()) cs_type += _T("|"); cs_type += _T("eFsEventRenamed"); }
		if (eFsEventType::eFsEventMoved   & _type) { if (!cs_type.IsEmpty()) cs_type += _T("|"); cs_type += _T("eFsEventMoved"  ); }
		if (eFsEventType::eFsEventMovedToRecycle      & _type) { if (!cs_type.IsEmpty()) cs_type += _T("|"); cs_type += _T("eFsEventMovedToRecycle"  ); }
		if (eFsEventType::eFsEventRestoredFromRecycle & _type) { if (!cs_type.IsEmpty()) cs_type += _T("|"); cs_type += _T("eFsEventRestoredFromRecycle"  ); }

		if (cs_type.IsEmpty())
			cs_type = _T("eFsEventUnknown");

		return cs_type;
	}

	CAtlString DataDumper_ObjectTypeToText(const ULONG _type)
	{
		CAtlString cs_type;

		switch (_type)
		{
		case eFsObjectType::eFsFileObject  :  cs_type = _T("eFsFileObject"); break;
		case eFsObjectType::eFsFolderObject:  cs_type = _T("eFsFolderObject"); break;
		case eFsObjectType::eFsStreamObject:  cs_type = _T("eFsStreamObject"); break;

		default:
			cs_type = _T("eFsUnknownType");
		}

		return cs_type;
	}

	CAtlString DataDumper_FlagToOperName(const ULONG _flags)
	{
		CAtlString cs_oper;
		if (false){}
		else if (_flags & FLT_CALLBACK_DATA_IRP_OPERATION)       cs_oper = _T("IRP");
		else if (_flags & FLT_CALLBACK_DATA_FAST_IO_OPERATION)   cs_oper = _T("FIO");
		else if (_flags & FLT_CALLBACK_DATA_FS_FILTER_OPERATION) cs_oper = _T("FSF");
		else                                                     cs_oper = _T("ERR");

		return cs_oper;
	}

	CAtlString DataDamper_FormatSystemTime(SYSTEMTIME& _time)
	{
		CAtlString cs_time;
		cs_time.Format(
				_T("%02d/%02d/%04d %02d:%02d:%02d:%03d"), _time.wMonth, _time.wDay, _time.wYear, _time.wHour, _time.wMinute, _time.wSecond, _time.wMilliseconds
			);
		return cs_time;
	}

	CAtlString DataDumper_IrpFlagsToText(const ULONG _flags)
	{
		CAtlString cs_flags;
		cs_flags.Format(
			_T("bitmask=%08lx"), _flags
			);
		cs_flags += _T(" {");
		if (IRP_NOCACHE   & _flags) cs_flags += _T("NOCACHE|");
		if (IRP_PAGING_IO & _flags) cs_flags += _T("PAGING_IO|");
		if (IRP_SYNCHRONOUS_API & _flags) cs_flags += _T("SYNC_API|");
		if (IRP_SYNCHRONOUS_PAGING_IO & _flags) cs_flags += _T("SYNC_PAGING_IO|");

		if (cs_flags.Right(1) == _T("|"))
			cs_flags.Left(cs_flags.GetLength() - 1);

		cs_flags += _T("} ");
		return cs_flags;
	}

	CAtlString DataDumper_IrpMinorToText(const UCHAR _major, const UCHAR _minor)
	{
		CAtlString cs_minor;
		switch (_major)
		{
		case IRP_MJ_READ :
		case IRP_MJ_WRITE:
			if      (IRP_MN_NORMAL           == _minor) cs_minor.LoadString(IDS_MN_NORMAL);
			else if (IRP_MN_DPC              == _minor) cs_minor.LoadString(IDS_MN_DPC);
			else if (IRP_MN_MDL              == _minor) cs_minor.LoadString(IDS_MN_MDL);
			else if (IRP_MN_COMPLETE         == _minor) cs_minor.LoadString(IDS_MN_COMPLETE);
			else if (IRP_MN_COMPRESSED       == _minor) cs_minor.LoadString(IDS_MN_COMPRESSED);
			else if (IRP_MN_MDL_DPC          == _minor) cs_minor.LoadString(IDS_MN_MDL_DPC);
			else if (IRP_MN_COMPLETE_MDL     == _minor) cs_minor.LoadString(IDS_MN_COMPLETE_MDL);
			else if (IRP_MN_COMPLETE_MDL_DPC == _minor) cs_minor.LoadString(IDS_MN_COMPLETE_MDL_DPC);
			break;
		case IRP_MJ_DIRECTORY_CONTROL:
			if      (IRP_MN_QUERY_DIRECTORY  == _minor) cs_minor.LoadString(IDS_MN_QUERY_DIRECTORY);
			else if (IRP_MN_NOTIFY_CHANGE_DIRECTORY == _minor) cs_minor.LoadString(IDS_MN_NOTIFY_CHANGE_DIRECTORY);
			break;
		case IRP_MJ_FILE_SYSTEM_CONTROL:
			if      (IRP_MN_USER_FS_REQUEST  == _minor) cs_minor.LoadString(IDS_MN_USER_FS_REQUEST);
			else if (IRP_MN_MOUNT_VOLUME     == _minor) cs_minor.LoadString(IDS_MN_MOUNT_VOLUME);
			else if (IRP_MN_VERIFY_VOLUME    == _minor) cs_minor.LoadString(IDS_MN_VERIFY_VOLUME);
			else if (IRP_MN_LOAD_FILE_SYSTEM == _minor) cs_minor.LoadString(IDS_MN_LOAD_FILE_SYSTEM);
			else if (IRP_MN_TRACK_LINK       == _minor) cs_minor.LoadString(IDS_MN_TRACK_LINK);
			break;
		case IRP_MJ_DEVICE_CONTROL:
			if      (IRP_MN_SCSI_CLASS       == _minor) cs_minor.LoadString(IDS_MN_SCSI_CLASS);
			break;
		case IRP_MJ_LOCK_CONTROL:
			if      (IRP_MN_LOCK             == _minor) cs_minor.LoadString(IDS_MN_LOCK);
			else if (IRP_MN_UNLOCK_SINGLE    == _minor) cs_minor.LoadString(IDS_MN_UNLOCK_SINGLE);
			else if (IRP_MN_UNLOCK_ALL       == _minor) cs_minor.LoadString(IDS_MN_UNLOCK_ALL);
			else if (IRP_MN_UNLOCK_ALL_BY_KEY== _minor) cs_minor.LoadString(IDS_MN_UNLOCK_ALL_BY_KEY);
			break;
		case IRP_MJ_POWER:
			if      (IRP_MN_WAIT_WAKE        == _minor) cs_minor.LoadString(IDS_MN_WAIT_WAKE);
			else if (IRP_MN_POWER_SEQUENCE   == _minor) cs_minor.LoadString(IDS_MN_POWER_SEQUENCE);
			else if (IRP_MN_SET_POWER        == _minor) cs_minor.LoadString(IDS_MN_SET_POWER);
			else if (IRP_MN_QUERY_POWER      == _minor) cs_minor.LoadString(IDS_MN_QUERY_POWER);
			break;
		case IRP_MJ_SYSTEM_CONTROL:
			if      (IRP_MN_QUERY_ALL_DATA   == _minor) cs_minor.LoadString(IDS_MN_QUERY_ALL_DATA);
			else if (IRP_MN_QUERY_SINGLE_INSTANCE   == _minor) cs_minor.LoadString(IDS_MN_QUERY_SINGLE_INSTANCE);
			else if (IRP_MN_CHANGE_SINGLE_INSTANCE  == _minor) cs_minor.LoadString(IDS_MN_CHANGE_SINGLE_INSTANCE);
			else if (IRP_MN_CHANGE_SINGLE_ITEM      == _minor) cs_minor.LoadString(IDS_MN_CHANGE_SINGLE_ITEM);
			else if (IRP_MN_ENABLE_EVENTS           == _minor) cs_minor.LoadString(IDS_MN_ENABLE_EVENTS);
			else if (IRP_MN_DISABLE_EVENTS          == _minor) cs_minor.LoadString(IDS_MN_DISABLE_EVENTS);
			else if (IRP_MN_ENABLE_COLLECTION       == _minor) cs_minor.LoadString(IDS_MN_ENABLE_COLLECTION);
			else if (IRP_MN_DISABLE_COLLECTION      == _minor) cs_minor.LoadString(IDS_MN_DISABLE_COLLECTION);
			else if (IRP_MN_REGINFO          == _minor) cs_minor.LoadString(IDS_MN_REGINFO);
			else if (IRP_MN_EXECUTE_METHOD   == _minor) cs_minor.LoadString(IDS_MN_EXECUTE_METHOD);
			break;
		case IRP_MJ_PNP:
			if      (IRP_MN_START_DEVICE     == _minor) cs_minor.LoadString(IDS_MN_START_DEVICE);
			else if (IRP_MN_QUERY_REMOVE_DEVICE     == _minor) cs_minor.LoadString(IDS_MN_QUERY_REMOVE_DEVICE);
			else if (IRP_MN_REMOVE_DEVICE    == _minor) cs_minor.LoadString(IDS_MN_REMOVE_DEVICE);
			else if (IRP_MN_CANCEL_REMOVE_DEVICE    == _minor) cs_minor.LoadString(IDS_MN_CANCEL_REMOVE_DEVICE);
			else if (IRP_MN_STOP_DEVICE      == _minor) cs_minor.LoadString(IDS_MN_STOP_DEVICE);
			else if (IRP_MN_QUERY_STOP_DEVICE       == _minor) cs_minor.LoadString(IDS_MN_QUERY_STOP_DEVICE);
			else if (IRP_MN_CANCEL_STOP_DEVICE      == _minor) cs_minor.LoadString(IDS_MN_CANCEL_STOP_DEVICE);
			else if (IRP_MN_QUERY_DEVICE_RELATIONS  == _minor) cs_minor.LoadString(IDS_MN_QUERY_DEVICE_RELATIONS);
			else if (IRP_MN_QUERY_INTERFACE  == _minor) cs_minor.LoadString(IDS_MN_QUERY_INTERFACE);
			else if (IRP_MN_QUERY_CAPABILITIES      == _minor) cs_minor.LoadString(IDS_MN_QUERY_CAPABILITIES);
			else if (IRP_MN_QUERY_RESOURCES  == _minor) cs_minor.LoadString(IDS_MN_QUERY_RESOURCES);
			else if (IRP_MN_QUERY_RESOURCE_REQUIREMENTS  == _minor) cs_minor.LoadString(IDS_MN_QUERY_RESOURCE_REQUIREMENTS);
			else if (IRP_MN_QUERY_DEVICE_TEXT       == _minor) cs_minor.LoadString(IDS_MN_QUERY_DEVICE_TEXT);
			else if (IRP_MN_FILTER_RESOURCE_REQUIREMENTS == _minor) cs_minor.LoadString(IDS_MN_FILTER_RESOURCE_REQUIREMENTS);
			else if (IRP_MN_READ_CONFIG      == _minor) cs_minor.LoadString(IDS_MN_READ_CONFIG);
			else if (IRP_MN_WRITE_CONFIG     == _minor) cs_minor.LoadString(IDS_MN_WRITE_CONFIG);
			else if (IRP_MN_EJECT            == _minor) cs_minor.LoadString(IDS_MN_EJECT);
			else if (IRP_MN_SET_LOCK         == _minor) cs_minor.LoadString(IDS_MN_SET_LOCK);
			else if (IRP_MN_QUERY_ID         == _minor) cs_minor.LoadString(IDS_MN_QUERY_ID);
			else if (IRP_MN_QUERY_PNP_DEVICE_STATE  == _minor) cs_minor.LoadString(IDS_MN_QUERY_PNP_DEVICE_STATE);
			else if (IRP_MN_QUERY_BUS_INFORMATION   == _minor) cs_minor.LoadString(IDS_MN_QUERY_BUS_INFORMATION);
			else if (IRP_MN_DEVICE_USAGE_NOTIFICATION    == _minor) cs_minor.LoadString(IDS_MN_DEVICE_USAGE_NOTIFICATION);
			else if (IRP_MN_SURPRISE_REMOVAL == _minor) cs_minor.LoadString(IDS_MN_SURPRISE_REMOVAL);
			else if (IRP_MN_QUERY_LEGACY_BUS_INFORMATION == _minor) cs_minor.LoadString(IDS_MN_QUERY_LEGACY_BUS_INFORMATION);
			break;
		case IRP_MJ_TRANSACTION_NOTIFY:
			if      (TRANSACTION_NOTIFY_BEGIN       == _minor) cs_minor.LoadString(IDS_TRANS_BEGIN);
			else if (TRANSACTION_NOTIFY_PREPREPARE_CODE  == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_PREPREPARE);
			else if (TRANSACTION_NOTIFY_PREPARE_CODE     == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_PREPARE);
			else if (TRANSACTION_NOTIFY_COMMIT_CODE      == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_COMMIT);
			else if (TRANSACTION_NOTIFY_COMMIT_FINALIZE_CODE     == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_COMMIT_FINALIZE);
			else if (TRANSACTION_NOTIFY_ROLLBACK_CODE    == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_ROLLBACK);
			else if (TRANSACTION_NOTIFY_PREPREPARE_COMPLETE_CODE == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_PREPREPARE_COMPLETE);
			else if (TRANSACTION_NOTIFY_PREPARE_COMPLETE_CODE    == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_PREPARE_COMPLETE);
			else if (TRANSACTION_NOTIFY_ROLLBACK_COMPLETE_CODE   == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_ROLLBACK_COMPLETE);
			else if (TRANSACTION_NOTIFY_RECOVER_CODE     == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_RECOVER);
			else if (TRANSACTION_NOTIFY_SINGLE_PHASE_COMMIT_CODE == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_SINGLE_PHASE_COMMIT);
			else if (TRANSACTION_NOTIFY_DELEGATE_COMMIT_CODE     == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_DELEGATE_COMMIT);
			else if (TRANSACTION_NOTIFY_RECOVER_QUERY_CODE       == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_RECOVER_QUERY);
			else if (TRANSACTION_NOTIFY_ENLIST_PREPREPARE_CODE   == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_ENLIST_PREPREPARE);
			else if (TRANSACTION_NOTIFY_LAST_RECOVER_CODE        == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_LAST_RECOVER);
			else if (TRANSACTION_NOTIFY_INDOUBT_CODE     == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_INDOUBT);
			else if (TRANSACTION_NOTIFY_PROPAGATE_PULL_CODE      == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_PROPAGATE_PULL);
			else if (TRANSACTION_NOTIFY_PROPAGATE_PUSH_CODE      == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_PROPAGATE_PUSH);
			else if (TRANSACTION_NOTIFY_MARSHAL_CODE     == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_MARSHAL);
			else if (TRANSACTION_NOTIFY_ENLIST_MASK_CODE == _minor) cs_minor.LoadString(IDS_TRANS_NOTIFY_ENLIST_MASK);
			break;
		}
		if (cs_minor.IsEmpty())
			cs_minor.Format(
				_T("Unknown Irp minor code (%u) for major one (%u)"), _minor, _major
			);
		return cs_minor;
	}

    CAtlString DataDumper_IrpCodeToText(const UCHAR _major, const UCHAR _minor)
	{
		CAtlString cs_major;
		CAtlString cs_minor;

		switch (_major)
		{
		case IRP_MJ_CREATE                   : cs_major.LoadString(IDS_MJ_CREATE);            break;
		case IRP_MJ_CREATE_NAMED_PIPE        : cs_major.LoadString(IDS_MJ_CREATE_NAMED_PIPE); break;
		case IRP_MJ_CLOSE                    : cs_major.LoadString(IDS_MJ_CLOSE);             break;
		case IRP_MJ_READ                     : cs_major.LoadString(IDS_MJ_READ);
			cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_WRITE                    : cs_major.LoadString(IDS_MJ_WRITE);
			cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_QUERY_INFORMATION        : cs_major.LoadString(IDS_MJ_QUERY_INFORMATION); break;
		case IRP_MJ_SET_INFORMATION          : cs_major.LoadString(IDS_MJ_SET_INFORMATION);   break;
		case IRP_MJ_QUERY_EA                 : cs_major.LoadString(IDS_MJ_QUERY_EA);          break;
		case IRP_MJ_SET_EA                   : cs_major.LoadString(IDS_MJ_SET_EA);            break;
		case IRP_MJ_FLUSH_BUFFERS            : cs_major.LoadString(IDS_MJ_FLUSH_BUFFERS);     break;
		case IRP_MJ_QUERY_VOLUME_INFORMATION : cs_major.LoadString(IDS_MJ_QUERY_VOLUME_INFORMATION); break;
		case IRP_MJ_SET_VOLUME_INFORMATION   : cs_major.LoadString(IDS_MJ_SET_VOLUME_INFORMATION);   break;
		case IRP_MJ_DIRECTORY_CONTROL        : cs_major.LoadString(IDS_MJ_DIRECTORY_CONTROL);
			cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_FILE_SYSTEM_CONTROL      : cs_major.LoadString(IDS_MJ_FILE_SYSTEM_CONTROL);
			cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_DEVICE_CONTROL           : cs_major.LoadString(IDS_MJ_DEVICE_CONTROL);
			cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_INTERNAL_DEVICE_CONTROL  : cs_major.LoadString(IDS_MJ_INTERNAL_DEVICE_CONTROL);  break;
		case IRP_MJ_SHUTDOWN                 : cs_major.LoadString(IDS_MJ_SHUTDOWN);                 break;
		case IRP_MJ_LOCK_CONTROL             : cs_major.LoadString(IDS_MJ_LOCK_CONTROL);
			cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_CLEANUP                  : cs_major.LoadString(IDS_MJ_CLEANUP);                  break;
		case IRP_MJ_CREATE_MAILSLOT          : cs_major.LoadString(IDS_MJ_CREATE_MAILSLOT);          break;
		case IRP_MJ_QUERY_SECURITY           : cs_major.LoadString(IDS_MJ_QUERY_SECURITY);           break;
		case IRP_MJ_SET_SECURITY             : cs_major.LoadString(IDS_MJ_SET_SECURITY);             break;
		case IRP_MJ_POWER                    : cs_major.LoadString(IDS_MJ_POWER);
			cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_SYSTEM_CONTROL           : cs_major.LoadString(IDS_MJ_SYSTEM_CONTROL);
			cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_DEVICE_CHANGE            : cs_major.LoadString(IDS_MJ_DEVICE_CHANGE);            break;
		case IRP_MJ_QUERY_QUOTA              : cs_major.LoadString(IDS_MJ_QUERY_QUOTA);              break;
		case IRP_MJ_SET_QUOTA                : cs_major.LoadString(IDS_MJ_SET_QUOTA);                break;
		case IRP_MJ_PNP                      : cs_major.LoadString(IDS_MJ_PNP);
			cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		case IRP_MJ_ACQUIRE_FOR_SECTION_SYNCHRONIZATION : cs_major.LoadString(IDS_MJ_ACQUIRE_FOR_SECTION_SYNC);   break;
		case IRP_MJ_RELEASE_FOR_SECTION_SYNCHRONIZATION : cs_major.LoadString(IDS_MJ_RELEASE_FOR_SECTION_SYNC);   break;
		case IRP_MJ_ACQUIRE_FOR_MOD_WRITE    : cs_major.LoadString(IDS_MJ_ACQUIRE_FOR_MOD_WRITE);    break;
		case IRP_MJ_RELEASE_FOR_MOD_WRITE    : cs_major.LoadString(IDS_MJ_RELEASE_FOR_MOD_WRITE);    break;
		case IRP_MJ_ACQUIRE_FOR_CC_FLUSH     : cs_major.LoadString(IDS_MJ_ACQUIRE_FOR_CC_FLUSH);     break;
		case IRP_MJ_RELEASE_FOR_CC_FLUSH     : cs_major.LoadString(IDS_MJ_RELEASE_FOR_CC_FLUSH);     break;
		case IRP_MJ_NOTIFY_STREAM_FO_CREATION: cs_major.LoadString(IDS_MJ_NOTIFY_STREAM_FO_CREATION);break;
		case IRP_MJ_FAST_IO_CHECK_IF_POSSIBLE: cs_major.LoadString(IDS_MJ_FAST_IO_CHECK_IF_POSSIBLE);break;
		case IRP_MJ_NETWORK_QUERY_OPEN       : cs_major.LoadString(IDS_MJ_NETWORK_QUERY_OPEN);       break;
		case IRP_MJ_MDL_READ                 : cs_major.LoadString(IDS_MJ_MDL_READ);                 break;
		case IRP_MJ_MDL_READ_COMPLETE        : cs_major.LoadString(IDS_MJ_MDL_READ_COMPLETE);        break;
		case IRP_MJ_PREPARE_MDL_WRITE        : cs_major.LoadString(IDS_MJ_PREPARE_MDL_WRITE);        break;
		case IRP_MJ_MDL_WRITE_COMPLETE       : cs_major.LoadString(IDS_MJ_MDL_WRITE_COMPLETE);       break;
		case IRP_MJ_VOLUME_MOUNT             : cs_major.LoadString(IDS_MJ_VOLUME_MOUNT);             break;
		case IRP_MJ_VOLUME_DISMOUNT          : cs_major.LoadString(IDS_MJ_VOLUME_DISMOUNT);          break;
		case IRP_MJ_TRANSACTION_NOTIFY       : cs_major.LoadString(IDS_MJ_TRANSACTION_NOTIFY);
			cs_minor = DataDumper_IrpMinorToText(_major, _minor);
			break;
		}

		if (cs_major.IsEmpty())cs_major = _T("#n/a");
		if (cs_minor.IsEmpty())cs_minor = _T("#n/a");

		CAtlString cs_irp;
		cs_irp.Format(
			_T("%s::%s"), cs_major, cs_minor
			);

		return cs_irp;
	}

	CAtlString DataDumper_OriginTimeToText(const LARGE_INTEGER _time)
	{
		FILETIME   local_  = {0};
		SYSTEMTIME system_ = {0};

		FileTimeToLocalFileTime((FILETIME *)&_time, &local_);
		FileTimeToSystemTime(&local_, &system_);

		CAtlString cs_time = DataDamper_FormatSystemTime(system_);
		return cs_time;
	}
	//
	// This doesn't work because it requires opening a file system object;
	// it's very ofthen, objects are not available at the moment of calling this routine;
	//
	CAtlString DataDumper_NormalizePath(const ULONG _obj_type, const WCHAR* _path)
	{
		CAtlString cs_norm;
		if (false){}
		else if (eFsObjectType::eFsFolderObject == _obj_type){
			CGenericFolder folder_(_path);
			cs_norm = folder_.UncName();
		}
		else if (eFsObjectType::eFsFileObject   == _obj_type){
			CGenericFile file_(_path);
			cs_norm = file_.UncName();
		}

		if (cs_norm.IsEmpty())
			cs_norm = _path;    // returns path as is;
		return cs_norm;
	}

	CAtlString DataDumper_SeqNumberToText(const ULONG _seq_n)
	{
		CAtlString cs_seq;
		cs_seq.Format(
				_T("%08X"), _seq_n
			);
		return cs_seq;
	}
#endif
}}}}

/////////////////////////////////////////////////////////////////////////////

#if defined(__OUTPUT_TO_CONSOLE)
VOID CDataDumper::DumpToConsole(
				__in const TRecordData& _rec
			)
{
	CAtlString cs_dump;
	if ((eFsEventType::eFsEventCopied   & _rec.uFsEventType) ||
	    (eFsEventType::eFsEventMoved    & _rec.uFsEventType) ||
	    (eFsEventType::eFsEventRenamed  & _rec.uFsEventType) ||
	    (eFsEventType::eFsEventMovedToRecycle & _rec.uFsEventType) ||
		(eFsEventType::eFsEventRestoredFromRecycle & _rec.uFsEventType)){

		cs_dump.Format(
				_T("Event Record\n\t[Event Type]\t\t%s\n\t[Object Type]\t\t%s\n\t[Origin Timestamp]\t%s\n\t[Completion Time]\t%s\n\t[Object Path]\n\tsource= %s\n\ttarget= %s\n\tsize=%lld\n"),
				details::DataDumper_EventTypeToText (_rec.uFsEventType)   ,
				details::DataDumper_ObjectTypeToText(_rec.uFsObjectType)  ,
				details::DataDumper_OriginTimeToText(_rec.OriginatingTime),
				details::DataDumper_OriginTimeToText(_rec.CompletionTime ),
				global::GetLocalFromUnc(_rec.SourceObject),
				global::GetLocalFromUnc(_rec.TargetObject),
				_rec.liSize.QuadPart
			);
	}
	else {
		cs_dump.Format(
				_T("Event Record\n\t[Event Type]\t\t%s\n\t[Object Type]\t\t%s\n\t[Origin Timestamp]\t%s\n\t[Completion Time]\t%s\n\t[Object Path]\n\t%s\n\tsize= %lld\n"),
				details::DataDumper_EventTypeToText (_rec.uFsEventType)   ,
				details::DataDumper_ObjectTypeToText(_rec.uFsObjectType)  ,
				details::DataDumper_OriginTimeToText(_rec.OriginatingTime),
				details::DataDumper_OriginTimeToText(_rec.CompletionTime ),
				global::GetLocalFromUnc(_rec.TargetObject),
				_rec.liSize.QuadPart
			);
	}
	global::_out::LogInfo(
			cs_dump
		);
	return;
}
#endif

#if !defined(_USE_EXT_OPERATE)
    #define  _USE_EXT_OPERATE
#endif

eFsEventType::_e CDataDumperEx::DriverEventToOperate(const ULONG uDriverEvent)
{
	eFsEventType::_e type_ = eFsEventType::eFsEventUnknown;
	if (false){}
	//
	// TODO: it's possible to have mixed event types, but they are not analyzed for now;
	//       types are placed in priority order (descending);
	//
#if defined(_USE_EXT_OPERATE)
	else if (eFsEventType::eFsEventMoved   & uDriverEvent) { type_ = eFsEventType::eFsEventMoved; }
	else if (eFsEventType::eFsEventMovedToRecycle      & uDriverEvent) { type_ = eFsEventType::eFsEventMovedToRecycle; }
	else if (eFsEventType::eFsEventRestoredFromRecycle & uDriverEvent) { type_ = eFsEventType::eFsEventRestoredFromRecycle; }
#endif
	else if (eFsEventType::eFsEventCreated & uDriverEvent) { type_ = eFsEventType::eFsEventCreated; }
	else if (eFsEventType::eFsEventCopied  & uDriverEvent) { type_ = eFsEventType::eFsEventCopied;  }
	else if (eFsEventType::eFsEventAltered & uDriverEvent) { type_ = eFsEventType::eFsEventAltered; }
	else if (eFsEventType::eFsEventDeleted & uDriverEvent) { type_ = eFsEventType::eFsEventDeleted; }
	else if (eFsEventType::eFsEventRenamed & uDriverEvent) { type_ = eFsEventType::eFsEventRenamed; }
	return type_;
}

CObjectType::_e  CDataDumperEx::DriverObjectToType(const ULONG uDriverObject)
{
	CObjectType::_e type_ = CObjectType::eFile;
	if (eFsObjectType::eFsFolderObject == uDriverObject) type_ = CObjectType::eFolder;

	return type_;
}

CAtlString       CDataDumperEx::DriverUncNameToLocal(const WCHAR* _path)
{
	CAtlString local_ = global::GetLocalFromUnc(_path);
	return local_;
}