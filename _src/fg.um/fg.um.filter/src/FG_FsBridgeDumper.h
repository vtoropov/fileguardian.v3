#ifndef _FGFSBRIDGEDATADUMPER_H_1AAF4B6C_E5D6_4b92_ACAE_F96A11D565A4_INCLUDED
#define _FGFSBRIDGEDATADUMPER_H_1AAF4B6C_E5D6_4b92_ACAE_F96A11D565A4_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Jun-2017 at 12:20:55p, UTC+7, Phuket, Rawai, Friday;
	This is File Guardian minifilter driver data output/dump class(es) declaration file.
*/
#include "FG_FsFilterIface.h"
#include "Shared_FS_CommonDefs.h"

namespace fg { namespace common { namespace filter { namespace _impl
{
#if defined(__OUTPUT_TO_CONSOLE)
	class CDataDumper
	{
	public:
		static VOID DumpToConsole(
				__in const TRecordData&
			);
	};
#endif

	using shared::ntfs::COperateType;
	using shared::ntfs::CObjectType;

	class CDataDumperEx
	{
	public:
		static eFsEventType::_e DriverEventToOperate(const ULONG uDriverEvent);
		static CObjectType::_e  DriverObjectToType(const ULONG uDriverObject);
		static CAtlString       DriverUncNameToLocal(const WCHAR* _path);
	};

}}}}

#endif/*_FGFSBRIDGEDATADUMPER_H_1AAF4B6C_E5D6_4b92_ACAE_F96A11D565A4_INCLUDED*/