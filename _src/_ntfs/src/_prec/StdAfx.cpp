/*
	Created by Tech_dog (VToropov) on 1-Mar-2016 at 10:23:26pm, GMT+7, Phuket, Rawai, Tuesday;
	This is Shared NTFS Wrapper Library precompiled headers implementation file.
*/
#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)