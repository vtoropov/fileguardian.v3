/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2016 at 1:34:11pm, GMT+7, Phuket, Rawai, Saturday;
	This is Shared NTFS library generic drive wrapper class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 27-May-2018 at 3:33:28p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Shared_FS_GenericDrive.h"

using namespace shared::ntfs;

/////////////////////////////////////////////////////////////////////////////

TDriveTypes    CGenericDriveType::GetAllTypes(void)
{
	TDriveTypes dwAllTypes;
	try {
		dwAllTypes.push_back(CGenericDriveType::eRemovable);
		dwAllTypes.push_back(CGenericDriveType::eFixed    );
		dwAllTypes.push_back(CGenericDriveType::eRemote   );
		dwAllTypes.push_back(CGenericDriveType::eOptical  );
		dwAllTypes.push_back(CGenericDriveType::eMemory   );
	}
	catch (::std::bad_alloc&){
	}

	return dwAllTypes;
}

/////////////////////////////////////////////////////////////////////////////

CGenericDrive::CGenericDrive(void): m_type(CGenericDriveType::eUnknown)
{
}

CGenericDrive::CGenericDrive(LPCTSTR lpszLetter): m_letter(lpszLetter), m_type(CGenericDriveType::eUnknown)
{
}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR               CGenericDrive::DosName(void)const
{
	return this->m_dos_name.GetString();
}

CAtlString            CGenericDrive::Letter(void)const
{
	return m_letter;
}

CAtlString            CGenericDrive::Path(void)const
{
	if (m_letter.IsEmpty())
		return CAtlString();

	CAtlString cs_path;
	cs_path.Format(
			_T("%s:\\"),
			m_letter.GetString()
		);
	return cs_path;
}

CAtlString            CGenericDrive::Pattern(void)const
{
	if (m_letter.IsEmpty())
		return CAtlString();

	CAtlString cs_pattern;
	cs_pattern.Format(
			_T("%s:"),
			m_letter.GetString()
		);
	return cs_pattern;
}

CGenericDriveType::_e CGenericDrive::Type(void)const
{
	return m_type;
}

VOID                  CGenericDrive::Type(const UINT _type)
{
	switch (_type)
	{
	case DRIVE_NO_ROOT_DIR: m_type = CGenericDriveType::eNoRoot;    break; 
	case DRIVE_REMOVABLE  : m_type = CGenericDriveType::eRemovable; break;
	case DRIVE_FIXED      : m_type = CGenericDriveType::eFixed;     break;
	case DRIVE_REMOTE     : m_type = CGenericDriveType::eRemote;    break;
	case DRIVE_CDROM      : m_type = CGenericDriveType::eOptical;   break;
	case DRIVE_RAMDISK    : m_type = CGenericDriveType::eMemory;    break;
	default               : m_type = CGenericDriveType::eUnknown;
	}
}

/////////////////////////////////////////////////////////////////////////////

CGenericDriveEnum::CGenericDriveEnum(void)
{
	m_error.Source(_T("CGenericDriveEnum"));
}

/////////////////////////////////////////////////////////////////////////////

TLogicalDrives    CGenericDriveEnum::Enumerate(const TDriveTypes& _eUnitMask)
{
	TLogicalDrives drives_;

	static UINT nLettersCount = _T('Z') - _T('A') + 1;

	TCHAR letter_[] = _T("A");

	for (UINT j_ = 0; j_ < nLettersCount; j_++) {

		CAtlString cs_letter(letter_);
		CAtlString cs_pattern;
		cs_pattern.Format(
				_T("%s:"), cs_letter.GetString()
			);

		CGenericDrive drive_(cs_letter);

		bool bIsType = false;

		const UINT  uType = ::GetDriveType(drive_.Path());
		if (CGenericDriveType::eRemovable > uType)
			goto __continue;

		for (size_t i_ = 0; i_ < _eUnitMask.size(); i_++)
		{
			if (_eUnitMask[i_] == uType){
				bIsType= true;
				break;
			}
		}

		if (bIsType) {

			TCHAR szDosName[_MAX_PATH] = {0};

			if (::QueryDosDevice(cs_pattern.GetString(), szDosName, _countof(szDosName))){
				drive_.m_dos_name = szDosName;

				drive_.Type(uType);
				try
				{
					drives_.push_back(drive_);
				}
				catch (::std::bad_alloc&)
				{
					m_error = E_OUTOFMEMORY;
					break;
				}
			}
		}
__continue:
		cs_letter = ++letter_[0];
	}
	return drives_;
}

TDriveList        CGenericDriveEnum::Enumerate(const TDriveFilter& _filter)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	TDriveList lst_;

	DWORD mask_ = ::GetLogicalDrives();
	if ( !mask_)
	{
		m_error = ::GetLastError();
		return lst_;
	}

	const TLogicalDrives drives_ = this->Enumerate(CGenericDriveType::GetAllTypes());

	for (size_t i_ = 0; i_ < drives_.size(); i_++)
	{
		const CGenericDrive& drive_ = drives_[i_];
		bool bInclude = false;

		for (size_t j_ = 0; j_ < _filter.size(); j_++) {
			if (drive_.Type() == _filter[j_]) {
				bInclude = true; break;
			}
		}
		if (!bInclude)
			continue;
		CAtlString cs_drive = drive_.Path();
		try
		{
			lst_.push_back(cs_drive);
		}
		catch (::std::bad_alloc&)
		{
			m_error = E_OUTOFMEMORY;
			break;
		}
	}
	return lst_;
}

TErrorRef         CGenericDriveEnum::Error(void)const
{
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString        CGenericDriveEnum::ConvertLocalToUNC(LPCTSTR lpszDosPath, const TLogicalDrives& _drives)
{
	if (NULL == lpszDosPath || ::_tcslen(lpszDosPath) < _countof(_T("a:")))
		return CAtlString();

	if (_drives.empty())
		return CAtlString();

	CAtlString cs_unc(lpszDosPath);

	for (size_t i_ = 0; i_ < _drives.size(); i_++){

		CAtlString cs_pattern = _drives[i_].Pattern();
		//
		// TODO: the found position must be clarified;
		//
		INT n_pos = cs_unc.Find(cs_pattern);
		if (n_pos!= -1) {
			cs_unc.Replace(cs_pattern, _drives[i_].DosName());
			break;
		}

	}

	return cs_unc;
}

CAtlString        CGenericDriveEnum::ConvertUNCToLocal(LPCTSTR lpszUncPath, const TLogicalDrives& _drives)
{
	if (NULL == lpszUncPath || ::_tcslen(lpszUncPath) < _countof(_T("a:")))
		return CAtlString();

	if (_drives.empty())
		return CAtlString();

	CAtlString cs_dos(lpszUncPath);

	for (size_t i_ = 0; i_ < _drives.size(); i_++){
		//
		// TODO: the found position must be clarified;
		//
		if (-1 != cs_dos.Find(_drives[i_].DosName())) {

			cs_dos.Replace(_drives[i_].DosName(), _drives[i_].Pattern());
			break;
		}

	}

	return cs_dos;
}

/////////////////////////////////////////////////////////////////////////////

CGenericDriveEvent::CGenericDriveEvent(void) : m_id(CGenericDriveEvent::eUnknown)
{
}

CGenericDriveEvent::CGenericDriveEvent(const CGenericDriveEvent::Id _id) : m_id(_id)
{
}

/////////////////////////////////////////////////////////////////////////////

CGenericDriveEvent& CGenericDriveEvent::operator= (const CGenericDriveEvent::Id _id)
{
	m_id = _id;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString        CGenericDriveEvent::Title(void)const
{
	switch (m_id)
	{
	case CGenericDriveEvent::ePlugged      : return CAtlString(_T("Plugged new device"));
	case CGenericDriveEvent::eRemoveAborted: return CAtlString(_T("Ejection is aborted"));
	case CGenericDriveEvent::eRemoveDone   : return CAtlString(_T("Ejection is complete"));
	case CGenericDriveEvent::eRemovePending: return CAtlString(_T("Ejection is pending"));
	case CGenericDriveEvent::eRemoveQueried: return CAtlString(_T("Ejection is waiting"));
	case CGenericDriveEvent::eTypeSpecific : return CAtlString(_T("Device specific"));
	}
	return CAtlString(_T("Unknown"));
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace ntfs { namespace details
{
	class CGenericDeviceNotifier_Handler:
			public  ::ATL::CWindowImpl<CGenericDeviceNotifier_Handler>
		{
			typedef ::ATL::CWindowImpl<CGenericDeviceNotifier_Handler> TBase;
		private:
			IGenericDeviceCallback&  m_sink;
		public:
			BEGIN_MSG_MAP(CMessageHandler)
				MESSAGE_HANDLER(WM_DEVICECHANGE, OnDeviceChanged)
			END_MSG_MAP()
		private:
			LRESULT  OnDeviceChanged(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
			{
				uMsg; wParam; lParam; bHandled = TRUE;
				const CGenericDriveEvent::Id evt_id_ = CGenericDeviceNotifier_Handler::WparamToEventId(wParam);
				switch (evt_id_)
				{
				case CGenericDriveEvent::ePlugged:
				case CGenericDriveEvent::eRemoveDone:
					{
						CGenericDrive drive_;
						DEV_BROADCAST_HDR* pHeader = reinterpret_cast<DEV_BROADCAST_HDR*>(lParam);
						if (pHeader)
						{
							if (DBT_DEVTYP_DEVICEINTERFACE == pHeader->dbch_devicetype)
							{
								DEV_BROADCAST_DEVICEINTERFACE* pIface = reinterpret_cast<DEV_BROADCAST_DEVICEINTERFACE*>(lParam);
								if (pIface)
								{
								}
							}
						}
						m_sink.IGenericDriveCallback_OnNotify(
									evt_id_,
									drive_
								);
					} break;
				}

				return TRUE/*BROADCAST_QUERY_DENY*/;
			}
		public:
			CGenericDeviceNotifier_Handler(IGenericDeviceCallback& _sink): m_sink(_sink)
			{
			}
		private:
			static CGenericDriveEvent::Id WparamToEventId(const WPARAM wParam)
			{
				CGenericDriveEvent::Id id_ = CGenericDriveEvent::eUnknown;

				switch (static_cast<UINT>(wParam))
				{
				case DBT_DEVICEARRIVAL       :    id_ = CGenericDriveEvent::ePlugged;       break;
				case DBT_DEVICEQUERYREMOVE   :    id_ = CGenericDriveEvent::eRemoveQueried; break;
				case DBT_DEVICEQUERYREMOVEFAILED: id_ = CGenericDriveEvent::eRemoveAborted; break;
				case DBT_DEVICEREMOVEPENDING :    id_ = CGenericDriveEvent::eRemovePending; break;
				case DBT_DEVICEREMOVECOMPLETE:    id_ = CGenericDriveEvent::eRemoveDone;    break;
				case DBT_DEVICETYPESPECIFIC  :    id_ = CGenericDriveEvent::eTypeSpecific;  break;
					{
					} break;
				default:;
				}
				return id_;
			};
		};
}}}

using details::CGenericDeviceNotifier_Handler;

#define _handler_ref(_pvoid) *(reinterpret_cast<CGenericDeviceNotifier_Handler*>(_pvoid))
/////////////////////////////////////////////////////////////////////////////

CGenericDeviceNotifier::CGenericDeviceNotifier(IGenericDeviceCallback& _sink): m_handler(NULL), m_bInitialized(false), m_notify(NULL)
{
	m_error.Source(_T("CGenericDeviceNotifier"));

	try
	{
		m_handler = new CGenericDeviceNotifier_Handler(_sink);
	}
	catch(::std::bad_alloc&)
	{
		m_error = E_OUTOFMEMORY;
	}
}

CGenericDeviceNotifier::~CGenericDeviceNotifier(void)
{
	if (m_handler)
	{
		CGenericDeviceNotifier_Handler* pHandler = reinterpret_cast<CGenericDeviceNotifier_Handler*>(m_handler);
		try
		{
			delete pHandler; pHandler = NULL;
		}
		catch(...)
		{
		}
	}
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef    CGenericDeviceNotifier::Error(void)const
{
	return m_error;
}

HRESULT      CGenericDeviceNotifier::Initialize(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (this->IsInitialized())
		return (m_error = (DWORD)ERROR_ALREADY_INITIALIZED);

	if (!m_handler)
		return (m_error = OLE_E_BLANK);

	CGenericDeviceNotifier_Handler& handler_ = _handler_ref(m_handler);

	if (handler_ == FALSE) // maybe message handler is already created
	{
		RECT rc_ = {0};
		handler_.Create(NULL, rc_, NULL, WS_POPUP);
	}

	if (handler_ == FALSE) // if message handler creation failed
		return (m_error = ::GetLastError());

	DEV_BROADCAST_DEVICEINTERFACE filter_ = {0};
	filter_.dbcc_size       = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
	filter_.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;

	HDEVNOTIFY notify = ::RegisterDeviceNotification(
							static_cast<HWND>(handler_),
							&filter_,
							DEVICE_NOTIFY_WINDOW_HANDLE|DEVICE_NOTIFY_ALL_INTERFACE_CLASSES
						);
	if (!notify)
		return (m_error = ::GetLastError());
	else
	{
		m_notify = notify;
		m_bInitialized = true;
	}

	return m_error;
}

bool         CGenericDeviceNotifier::IsInitialized(void)const
{
	return m_bInitialized;
}

HRESULT      CGenericDeviceNotifier::Terminate(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (m_notify)
	{
		::UnregisterDeviceNotification(
				reinterpret_cast<HDEVNOTIFY>(m_notify)
			);
		m_notify = NULL;
	}
	if (m_handler)
	{
		CGenericDeviceNotifier_Handler& handler_ = _handler_ref(m_handler);
		if (handler_)
			handler_.SendMessage(WM_CLOSE);
	}
	m_bInitialized = false;
	return m_error;
}