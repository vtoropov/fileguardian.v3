/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-May-2016 at 6:22:13pm, GMT+7, Phuket, Rawai, Sunday;
	This is File Guardian removable device monitor class implementation file.
*/
#include "StdAfx.h"
#include "Shared_FS_GenericDrive.Monitor.h"

#include "Shared_SystemCore.h"
#include "Shared_GenericSyncObject.h"
#include "Shared_GenericEvent.h"
#include "Shared_LogJournal.h"

using namespace shared::lite::sys_core;
using namespace shared::runnable;
using namespace shared::log;

#include "Shared_FS_GenericDrive.h"

using namespace shared::ntfs;

#include <algorithm>

/////////////////////////////////////////////////////////////////////////////

CRemovableDriveMonitor::CRemovableDriveMonitor(IRemovableDriveEventSink& _sink) : m_sink(_sink)
{
	m_error.Source(_T("CRemovableDriveMonitor"));
}

/////////////////////////////////////////////////////////////////////////////

VOID    CRemovableDriveMonitor::ThreadFunction(void)
{
	CCoInitializer com_core(false);
#if defined(_DEBUG)
	CGenericWaitCounter wait_(10, 1000 * 01); // 1 sec(s)
#else
	CGenericWaitCounter wait_(10, 1000 * 02); // 2 sec(s)
#endif

	CEventJournal::LogInfo(_T("Removable device monitor has started"));

	TDriveList prev_;
	TDriveFilter filter_;
	
	filter_.push_back(CGenericDriveType::eOptical);
	filter_.push_back(CGenericDriveType::eRemovable);

	while (!m_crt.IsStopped())
	{
		wait_.Wait();
		if (wait_.IsElapsed())
			wait_.Reset();
		else
			continue;

		CGenericDriveEnum drives_;
		TDriveList current_ = drives_.Enumerate(filter_);

		TDriveList arrived_;
		::std::set_difference(current_.begin(), current_.end(), prev_.begin(), prev_.end(), ::std::back_inserter(arrived_));

		TDriveList removed_;
		::std::set_difference(prev_.begin(), prev_.end(), current_.begin(), current_.end(), ::std::back_inserter(removed_));

		if (!removed_.empty())
			m_sink.IRemovableDrive_BeingEjected(removed_);
	
		if (!arrived_.empty())
			m_sink.IRemovableDrive_BeingAdded(arrived_);

		prev_ = current_;
	}
	CEventJournal::LogInfo(_T("Removable device monitor has stopped"));
	::SetEvent(m_crt.EventObject());
}