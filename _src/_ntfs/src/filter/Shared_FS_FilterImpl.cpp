/*
	Created by Tech_dog (VToropov) on 13-May-2016 at 1:43:29pm, GMT+7, Phuket, Rawai, Friday;
	This is System Shell browser dialog folder filter class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 7:35:17p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "Shared_FS_FilterImpl.h"

using namespace shared::ntfs::_impl;

/////////////////////////////////////////////////////////////////////////////

CFolderFilterImpl::CFolderFilterImpl(void) : m_lRef(0)
{
}

CFolderFilterImpl::~CFolderFilterImpl(void)
{
}

/////////////////////////////////////////////////////////////////////////////

ULONG    CFolderFilterImpl::AddRef (void)
{
	return ::InterlockedIncrement(&m_lRef);
}

ULONG    CFolderFilterImpl::Release(void)
{
	LONG lRef = ::InterlockedDecrement(&m_lRef);
	if (!lRef)
		delete this;
	return lRef;
}

HRESULT  CFolderFilterImpl::QueryInterface(REFIID riid, void** ppv)
{
	if (riid == IID_IUnknown || riid == IID_IFolderFilter)
	{
		*ppv = reinterpret_cast<void*>(this);
		this->AddRef();
		return S_OK;
	}
	else
		return E_NOINTERFACE;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CFolderFilterImpl::GetEnumFlags(IShellFolder* _psf, LPCITEMIDLIST pidlFolder, HWND* pWindow, DWORD* pFlags)
{
	_psf; pidlFolder; pWindow; pFlags;
	return S_OK;
}

HRESULT  CFolderFilterImpl::ShouldShow  (IShellFolder* _psf, LPCITEMIDLIST pidlFolder, LPCITEMIDLIST pidlItem)
{
	_psf; pidlFolder; pidlItem;

	HRESULT hr_ = S_OK;
#if 0
	if (!_psf)
		return (hr_ = E_INVALIDARG);

	ULONG atts_ = 0UL;
	hr_ = _psf->GetAttributesOf(1, &pidlItem, &atts_);
	if (FAILED(hr_))
		return hr_;

	if (false){}
	else if (atts_ & SFGAO_FOLDER){}
	else if (atts_ & SFGAO_LINK  ){}
	else
		hr_ = S_FALSE;
#endif
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CFolderFilterImpl::CreateObject(::ATL::CComPtr<IFolderFilter>& pObject)
{
	CFolderFilterImpl* pFilter = NULL;
	try
	{
		pFilter = new CFolderFilterImpl();
		pObject = pFilter;
	}
	catch (::std::bad_alloc&)
	{
		return E_OUTOFMEMORY;
	}
	return S_OK;
}