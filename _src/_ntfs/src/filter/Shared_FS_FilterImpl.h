#ifndef _SHAREDFOLDERFILTERIMPL_H_4EC51E6F_401E_4517_9F6C_43F3B553B231_INCLUDED
#define _SHAREDFOLDERFILTERIMPL_H_4EC51E6F_401E_4517_9F6C_43F3B553B231_INCLUDED
/*
	Created by Tech_dog (VToropov) on 13-May-2016 at 12:56:54pm, GMT+7, Phuket, Rawai, Friday;
	This is System Shell browser dialog folder filter class declaration file.
*/
#include "Shared_FS_CommonDefs.h"

namespace shared { namespace ntfs { namespace _impl
{
	class CFolderFilterImpl : public IFolderFilter
	{
	private:
		LONG       m_lRef;
	private:
		CFolderFilterImpl(void);
		~CFolderFilterImpl(void);
	private: // IUnknown
		HRESULT    STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppv) override;
		ULONG      STDMETHODCALLTYPE AddRef (void) override sealed;
		ULONG      STDMETHODCALLTYPE Release(void) override sealed;
	private: // IFolderFilter
		HRESULT    STDMETHODCALLTYPE GetEnumFlags(IShellFolder*, LPCITEMIDLIST pidlFolder, HWND* pWindow, DWORD* pFlags) override sealed;
		HRESULT    STDMETHODCALLTYPE ShouldShow  (IShellFolder*, LPCITEMIDLIST pidlFolder, LPCITEMIDLIST pidlItem) override sealed;
	public:
		static
		HRESULT    CreateObject(::ATL::CComPtr<IFolderFilter>& pObject);
	};
}}}

#endif/*_SHAREDFOLDERFILTERIMPL_H_4EC51E6F_401E_4517_9F6C_43F3B553B231_INCLUDED*/