/*
	Created by Tech_dog (VToropov) on 13-May-2016 at 2:28:14pm, GMT+7, Phuket, Rawai, Friday;
	This is Shared Lite Library Extended File/Folder Browser Dialog class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 7:39:06p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "Shared_FS_Browser.h"

using namespace shared::ntfs;

#include "Shared_FS_FilterImpl.h"
#include "Shared_GenericRunnableObject.h"
#include "Shared_SystemCore.h"

using namespace shared::ntfs::_impl;
using namespace shared::runnable;
using namespace shared::lite::sys_core;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace ntfs { namespace details
{
	class CFolderBrowserTreeView
	{
	private:
		HWND               m_parent;
		WTL::CTreeViewCtrl m_tree; 
	public:
		CFolderBrowserTreeView(HWND _parent): m_parent(_parent)
		{
			::EnumChildWindows(
						m_parent,
						__enum  ,
						reinterpret_cast<LPARAM>(&m_tree.m_hWnd)
					);
		}
	public:
		VOID               EnsureVisible(void)
		{
			if (m_tree)
				m_tree.EnsureVisible(
					m_tree.GetSelectedItem()
				);
		}
	private:
		static
		BOOL CALLBACK __enum(HWND hWndChild, LPARAM lParam)
		{
			TCHAR cls_[_MAX_PATH] = {0};

			const INT len_ = ::GetClassName(
								hWndChild,
								cls_,
								_countof(cls_)
							);
			if (len_ && 0 == ::_tcscmp(cls_, WC_TREEVIEW))
			{
				PVOID void_ = reinterpret_cast<PVOID>(lParam);
				*(reinterpret_cast<HWND*>(void_)) = hWndChild;
				return FALSE;
			}
			else
				return TRUE;
		}
	};

	class CFolderBrowserDiffered:
		protected CGenericRunnableObject,
		private   IGenericEventNotify
	{
		typedef   CGenericRunnableObject TRunnable;
	private:
		CWindow   m_dlg;
	public:
		CFolderBrowserDiffered(void) : TRunnable(CFolderBrowserDiffered::DifferThread_Func, *this, _variant_t((LONG)1)){
		}
		~CFolderBrowserDiffered(void){
		}
	public:
		VOID        BrowserHandle(const HWND hBrowser){
			m_dlg = hBrowser;
		}
		HRESULT     Interrupt(void) {
			if (TRunnable::IsStopped())
				return S_OK;
			const bool bForce = true;
			HRESULT hr_ = TRunnable::Stop(bForce);
			return  hr_;
		}
		HRESULT     Start(void) {
			HRESULT hr_ = TRunnable::Start();
			return  hr_;
		}
		HRESULT     Stop (void) {
			const bool bForce = false;
			HRESULT hr_ = TRunnable::Stop(bForce);
			return  hr_;
		}
	private:
		HRESULT     GenericEvent_OnNotify(const _variant_t v_evt_id) override sealed
		{
			HRESULT hr_ = TRunnable::Stop(false);
			return  hr_;
		}
	private:
		static
		UINT WINAPI DifferThread_Func(VOID* pObject){
			if (NULL == pObject)
				return 1;

			CGenericRunnableObject* pRunnable  = NULL;
			CFolderBrowserDiffered* pDiffered  = NULL;
			try
			{
				pRunnable  = reinterpret_cast<CGenericRunnableObject*>(pObject);
				pDiffered  = reinterpret_cast<CFolderBrowserDiffered*>(pRunnable);
			}
			catch (::std::bad_cast&) { return 1; }

			CCoInitializer com_lib(false);
			if (!com_lib.IsSuccess())
				return 1;

			CGenericWaitCounter waiter_(50, 100);

			while(pDiffered->IsRunning()){

				waiter_.Wait();

				if (waiter_.IsElapsed())
					waiter_.Reset();
				else
					continue;

#if (1)
				CWindow dialog_(pDiffered->m_dlg);
				if (!dialog_)
					continue;

				CWindow parent_ = dialog_.GetParent();

				if (!parent_)
					continue;

				// (1) getting dialog window rectangle;
				RECT rcDialog = {0};
				{
					dialog_.GetWindowRect(&rcDialog);
				}
				// (2) getting parent window rectangle;
				RECT rcParent = {0};
				{
					parent_.GetWindowRect(&rcParent);
				}
				// (3) getting the real position of the browse dialog;
				const SIZE size_ = {
						rcDialog.right - rcDialog.left, rcDialog.bottom - rcDialog.top
					};
				const LONG left_ = rcParent.left + (rcParent.right - rcParent.left) / 2 - size_.cx / 2;
				const LONG top_  = rcParent.top  + (rcParent.bottom - rcParent.top) / 2 - size_.cy / 2;
				
				dialog_.SetWindowPos(
						NULL, left_, top_, size_.cx, size_.cy, SWP_NOSIZE|SWP_NOZORDER
					);

				break;
#endif

			}
	//
	// this things are not handled and it is not necessary to do;
	//
#if (0)
			const bool bAsync = true;
			pRunnable->MarkCompleted();
			pRunnable->Event().Fire(bAsync);
#endif
			return 0;
		}
	};

	class CFolderBrowserDlg:
		public  WTL::CFolderDialogImpl<CFolderBrowserDlg>
	{
		typedef WTL::CFolderDialogImpl<CFolderBrowserDlg> TBaseDialog;
	private:
		CFolderBrowserDiffered m_differ;
	public:
		CFolderBrowserDlg(
				HWND    hWndParent,
				LPCTSTR lpstrTitle,
				UINT    uFlags
			) : TBaseDialog(hWndParent, lpstrTitle, uFlags)
		{
			TBaseDialog::m_bExpandInitialSelection = true;
		}
		~CFolderBrowserDlg(void)
		{
			m_differ.Interrupt();
		}
	public:
		VOID    OnInitialized(VOID)
		{
			m_differ.BrowserHandle(TBaseDialog::m_hWnd);
			m_differ.Start();
		}

		VOID    OnIUnknown(IUnknown* pUnknown)
		{
			if (!pUnknown)
				return;

			::ATL::CComQIPtr<IFolderFilterSite> pSite = pUnknown;
			if (pSite)
			{
				::ATL::CComPtr<IFolderFilter> filter_;

				HRESULT hr_ = CFolderFilterImpl::CreateObject(filter_);
				if (!FAILED(hr_))
					hr_ = pSite->SetFilter(filter_);
			}
		}
		VOID    OnSelChanged(LPITEMIDLIST lpItemIdList)
		{
			lpItemIdList;
			CFolderBrowserTreeView tree_(TBaseDialog::m_hWnd);
			tree_.EnsureVisible();
		}
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CFolderBrowser::CFolderBrowser(const HWND _parent, LPCTSTR lpszTitle) : m_parent(_parent), m_title(lpszTitle)
{
	m_error.Source(_T("CFolderBrowser"));
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CFolderBrowser::Error(void)const
{
	return m_error;
}

HRESULT     CFolderBrowser::SelectFolder(LPCTSTR lpszInitFolder, CAtlString& _selected)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_folder(lpszInitFolder);

	details::CFolderBrowserDlg dlg_(
				m_parent,
				m_title ,
				BIF_RETURNONLYFSDIRS|BIF_USENEWUI|BIF_NONEWFOLDERBUTTON|BIF_VALIDATE
			);

	dlg_.SetInitialFolder(cs_folder);

	const INT_PTR nResult = dlg_.DoModal();
	if (IDOK == nResult)
		_selected = dlg_.GetFolderPath();
	else
		m_error.SetState(
				S_FALSE,
				_T("User has cancelled the dialog")
			);

	return m_error;
}