/*
	Created by Tech_dog (ebontrop@gmail.com) on 7-Jun-2016 at 4:55:53p, GMT+7, Phuket, Rawai, Tuesday;
	This is shared NTFS library generic file class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 27-May-2018 at 3:25:45p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Shared_FS_GenericFolder.h"

using namespace shared::ntfs;

#include "Shared_DateTime.h"

using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

CGenericFolder::CGenericFolder(LPCTSTR lpszPath) : TObject(CObjectType::eFolder, lpszPath)
{
	m_error.Source(_T("CGenericFolder"));
}

CGenericFolder::~CGenericFolder(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CGenericFolder::Create  (VOID)
{
	m_error.Module(_T(__FUNCTION__));
	
	TFolderList folders_ = m_path.ToFolders();
	if (folders_.empty())
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("The folder path is empty")
			);
		return m_error;
	}

	TCHAR drv_[_MAX_DRIVE] = {0};
	{
		::_tsplitpath_s(
				m_path,
				drv_  ,
				_countof(drv_),
				NULL  ,
				0     ,
				NULL  ,
				0     ,
				NULL  ,
				0
			);
	}

	::ATL::CAtlString cs_long_path;
	cs_long_path += _T("\\\\?\\");
	cs_long_path += drv_;

	for (size_t i_ = 0; i_ < folders_.size(); i_++)
	{
		cs_long_path += _T("\\");
		cs_long_path += folders_[i_];
		if (::PathFileExists(cs_long_path))
			continue;
		if (!::CreateDirectory(cs_long_path.GetString(), NULL))
			return (m_error = ::GetLastError());
	}
	if (m_error)
		m_error.Clear();

	return m_error;
}

WIN32_FIND_DATA CGenericFolder::FindLastModified(VOID)const
{
	WIN32_FIND_DATA data_ = {0};
	WIN32_FIND_DATA find_ = {0};
	SYSTEMTIME temp_  = {0};
	SYSTEMTIME last_  = {0};

	CAtlString cs_mask;
	cs_mask.Format(
			_T("%s*"),
			(LPCTSTR)m_path
		);

	HANDLE hFind = ::FindFirstFile(cs_mask, &data_);
	if (INVALID_HANDLE_VALUE == hFind)
		return find_;

	do
	{
		CAtlString cs_path = data_.cFileName;
		if ('.' != data_.cFileName[0])
		{
			CSystemTime sys_(data_.ftLastAccessTime);
			::FileTimeToSystemTime(&data_.ftLastAccessTime, &temp_);

			if (sys_.Compare(last_) == -1)
			{
				last_ = sys_;
				find_ = data_;
			}
		}
	} while (::FindNextFile(hFind, &data_));

	return find_;
}

HRESULT      CGenericFolder::EnumerateItems(TFileList& _files, TFolderList& _subfolders, LPCTSTR lpszMask)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (m_path.IsEmpty())
		return (m_error = OLE_E_BLANK);

	CGenericPath path_(this->Path());
	if (!path_.Normalize(CObjectType::eFolder))
		return (m_error = DISP_E_TYPEMISMATCH);

	CAtlString query_;
	query_ += path_;
	query_ += lpszMask;

	WIN32_FIND_DATA data_ = {0};
	HANDLE hFind = ::FindFirstFile(query_, &data_);
	if (INVALID_HANDLE_VALUE == hFind)
	{
		const DWORD dwError = ::GetLastError();
		if (ERROR_FILE_NOT_FOUND == dwError)
			return m_error;
		else
			return (m_error = dwError);
	}
	do
	{
		const bool bIsFolder = (0 != (FILE_ATTRIBUTE_DIRECTORY & data_.dwFileAttributes));
		if (bIsFolder)
		{
			CAtlString cs_folder = data_.cFileName;
			if (cs_folder.GetAt(0) != _T('.')) // checks for special folder names (parent and root folder)
			{
				try
				{
					CAtlString cs_path = (LPCTSTR)m_path;
					cs_path += cs_folder;

					_subfolders.push_back(cs_path);
				}
				catch(::std::bad_alloc&)
				{
					return m_error = E_OUTOFMEMORY;
				}
			}
		}
		else
		{
			CAtlString cs_path = (LPCTSTR)m_path;
			CAtlString cs_file = data_.cFileName;
			cs_path += cs_file;
			try
			{
				_files.push_back(cs_file);
			}
			catch(::std::bad_alloc&)
			{
				return m_error = E_OUTOFMEMORY;
			}
		}
	} while (::FindNextFile(hFind, &data_));
	::FindClose(hFind);

	return m_error;
}

HRESULT      CGenericFolder::EnumerateItems(TFolderItemCollection& _items)const
{
	m_error = S_OK;

	if (!_items.empty())_items.clear();

	WIN32_FIND_DATA data_ = {0};

	HANDLE hFind = ::FindFirstFile(m_path, &data_);
	if (INVALID_HANDLE_VALUE == hFind)
		return (m_error = ::GetLastError());

	do
	{
		try
		{
			_items.push_back(data_);
		}
		catch(::std::bad_alloc&)
		{
			m_error = E_OUTOFMEMORY;
			break;
		}
	}
	while (::FindNextFile(hFind, &data_));

	::FindClose(hFind);

	return m_error;
}

bool         CGenericFolder::SelectObject(LPCTSTR lpszFileName)const
{
	::ATL::CAtlString cs_obj_path;
	cs_obj_path += m_path;
	cs_obj_path.Replace(_T('/'), _T('\\'));

	if (lpszFileName && ::_tcslen(lpszFileName))
	{
		if ('\\' != cs_obj_path.GetAt(cs_obj_path.GetLength() - 1))
			cs_obj_path += _T("\\");

		cs_obj_path += lpszFileName;

		LPITEMIDLIST pidl = ::ILCreateFromPath(cs_obj_path.GetString());
		if (pidl)
		{
			::SHOpenFolderAndSelectItems(pidl, 0, NULL, 0);
			::ILFree(pidl); pidl = NULL;
			return true;
		}
		m_error = ERROR_PATH_NOT_FOUND;
	}
	else
	{
		const DWORD dwResult = 
			( DWORD )(DWORD_PTR)::ShellExecute(
									NULL       ,
									_T("open") ,
									cs_obj_path,
									NULL       ,
									NULL       ,
									SW_SHOWDEFAULT
								);
		if (ERROR_SHARING_VIOLATION < dwResult)
		{
			m_error = S_OK;
			return true;
		}
		m_error = ERROR_PATH_NOT_FOUND;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace ntfs { namespace details
{

	HRESULT  FileSystem_GetSpecialFolder(const INT _id, CAtlString& _path)
	{
		TCHAR szPath[MAX_PATH] = {0};
		HRESULT hr_ = ::SHGetFolderPath(
							NULL,
							_id|CSIDL_FLAG_CREATE, 
							NULL,
							0,
							szPath
						);
		if (SUCCEEDED(hr_))
			_path = szPath;

		return hr_;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CSpecialFolder::CSpecialFolder(void)
{
	m_error.Source(_T("CSpecialFolder"));
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef    CSpecialFolder::Error(void)const
{
	return m_error;
}

CAtlString   CSpecialFolder::PersonalDocs (void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_path;
	HRESULT hr_ = details::FileSystem_GetSpecialFolder(CSIDL_PERSONAL, cs_path);
	if (FAILED(hr_))
		m_error = hr_;

	return cs_path;
}

CAtlString   CSpecialFolder::ProgramFiles (void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_path;
	HRESULT hr_ = details::FileSystem_GetSpecialFolder(CSIDL_PROGRAM_FILES, cs_path);
	if (FAILED(hr_))
		m_error = hr_;

	return cs_path;
}

CAtlString   CSpecialFolder::ProgramFiles86(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_path;
	HRESULT hr_ = details::FileSystem_GetSpecialFolder(CSIDL_PROGRAM_FILESX86 , cs_path);
	if (FAILED(hr_))
		m_error = hr_;

	return cs_path;
}

CAtlString   CSpecialFolder::PublicUserData(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_path;
	HRESULT hr_ = details::FileSystem_GetSpecialFolder(CSIDL_COMMON_APPDATA, cs_path);
	if (FAILED(hr_))
		m_error = hr_;

	return cs_path;
}

CAtlString   CSpecialFolder::UserTemporary (void)
{
	CAtlString cs_temp;
	{
		TCHAR buffer[2048] = {0};
		::GetEnvironmentVariable(_T("TEMP"), buffer, _countof(buffer));
		cs_temp = buffer;
	}
	if (cs_temp.Right(1) != _T("\\"))
		cs_temp += _T("\\");
	return cs_temp;
}

/////////////////////////////////////////////////////////////////////////////

CCurrentFolder::CCurrentFolder(void) : TObject(CObjectType::eFolder, NULL)
{
	TCHAR buffer[_LONG_PATH] = {0};

	const BOOL ret__ = ::GetModuleFileName(
							NULL  ,
							buffer,
							_LONG_PATH
						);
	if (ret__)
	{
		TObject::m_path.BuildFrom(buffer, CObjectType::eFile);
	}
}

/////////////////////////////////////////////////////////////////////////////

CAtlString   CCurrentFolder::GetPath(void)const
{
	return CAtlString((LPCTSTR)TObject::m_path);
}

CAtlString   CCurrentFolder::GetPathFromPattern(LPCTSTR lpszPattern)const
{
	CAtlString cs_path;

	if (!lpszPattern || !::_tcslen(lpszPattern))
	{
		TObject::m_error = E_INVALIDARG;
		return cs_path;
	}

	cs_path = this->Path();

	const bool bUseCurrentFolder = this->IsRelative(lpszPattern);
	if (!bUseCurrentFolder)
	{
		TObject::m_error = S_FALSE;
		return cs_path;
	}

	CAtlString cs_pattern(lpszPattern);

	if (_T(".") == cs_pattern.Left(1))
		cs_path+= cs_pattern.Right(cs_pattern.GetLength() - 1);  // adds the pattern without the dot symbol
	else
		cs_path+= cs_pattern.Right(cs_pattern.GetLength() - static_cast<INT>(::_tcslen(_T("$(ApplicationFolder)"))));
	cs_path.Replace(_T("\\\\"), _T("\\"));
	cs_path.Replace(_T("\\/") , _T("\\"));

	TObject::m_error = S_OK;

	return  cs_path;
}

bool         CCurrentFolder::IsRelative(LPCTSTR lpszPath) const
{
	if (!lpszPath || !::_tcslen(lpszPath))
		return false;

	::ATL::CAtlString cs_relative(lpszPath);
	bool bIsRelative = (0 == cs_relative.Find(_T(".\\"))
						|| 0 == cs_relative.Find(_T("./"))
							|| 0 == cs_relative.Find(_T("$(ApplicationFolder)")));

	return bIsRelative;
}