/*
	Created by Tech_dog (ebontrop@gmail.com) on 7-Jun-2016 at 8:34:13p, GMT+7, Phuket, Rawai, Tuesday;
	This is shared NTFS library common definitions implementation file.
*/
#include "StdAfx.h"
#include "Shared_FS_CommonDefs.h"
#include "Shared_FS_GenericDrive.h"
#include "Shared_FS_GenericFolder.h"

using namespace shared::ntfs;

#include "Shared_GenericHandle.h"
//#include "Shared_RawData.h"

using namespace shared::lite::common;
//using namespace shared::lite::data;

#pragma comment(lib, "mpr.lib") // for WNetGetUniversalName routine;

/////////////////////////////////////////////////////////////////////////////

COperateType::COperateType(void) {}

/////////////////////////////////////////////////////////////////////////////

INT        COperateType::Count(void) { return COperateType::nTypes; }
INT        COperateType::Index (const COperateType::_e _type) {

	switch (_type) {
	case COperateType::eFileCreated: return 0;
	case COperateType::eFileDeleted: return 1;
	case COperateType::eFileChanged: return 2;
	case COperateType::eFileRenamed: return 3;
	}
	return COperateType::eUndefined - 1;
}
COperateType::_e
COperateType::Type (const INT nIndex)
{
	if (0 > nIndex || nIndex > COperateType::Count() - 1)
		return COperateType::eUndefined;
	switch (nIndex) {
	case 0: return COperateType::eFileCreated;
	case 1: return COperateType::eFileDeleted;
	case 2: return COperateType::eFileChanged;
	case 3: return COperateType::eFileRenamed;
	}

	return COperateType::eUndefined;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace ntfs
{
	HRESULT  FileSystem_CreateLongPath(LPCTSTR lpszPath, ::ATL::CAtlString& cs_path)
	{
		if (!lpszPath || ::_tcslen(lpszPath) < _MAX_DRIVE)
			return E_INVALIDARG;

		if (cs_path.IsEmpty() == false)
			cs_path.Empty();
		cs_path += _T("\\\\?\\");
		cs_path += lpszPath;
		cs_path.Replace(_T('/'), _T('\\'));
		return S_OK;
	}

	HRESULT  FileSystem_GetAttributes (LPCTSTR lpszPath, WIN32_FILE_ATTRIBUTE_DATA& _data)
	{
		::ATL::CAtlString cs_long_path;
		HRESULT hr_ = FileSystem_CreateLongPath(lpszPath, cs_long_path);
		if (S_OK != hr_)
			return  hr_;

		const BOOL bResult = ::GetFileAttributesEx(cs_long_path, GetFileExInfoStandard, &_data);
		if (!bResult)
			hr_ = HRESULT_FROM_WIN32(::GetLastError());

		return hr_;
	}

namespace details {

	HRESULT  FileSystem_SplitPath(LPCTSTR lpszPath, CAtlString& _drv, CAtlString& _path)
	{
		if (!lpszPath || !::_tcslen(lpszPath))
			return E_INVALIDARG;

		TCHAR drv_[_MAX_DRIVE] = {0};
		TCHAR fld_[_LONG_PATH] = {0};
		{
			::_tsplitpath_s(
					lpszPath,
					drv_  ,
					_countof(drv_),
					fld_  ,
					_countof(fld_),
					NULL  ,
					0     ,
					NULL  ,
					0
				);
		}
		_drv = drv_;
		_path = fld_;

		HRESULT hr_ = S_OK;
		return  hr_;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CGenericPath::CGenericPath(LPCTSTR lpszPath) : m_path(lpszPath)
{
	if (!m_path.IsEmpty())
	{
		m_path.Replace(_T('/'), _T('\\'));
		if (this->IsFolder())
			if (_T('\\') != m_path.Right(1))
				m_path += _T('\\');
	}
}

CGenericPath::CGenericPath(LPCTSTR lpszFile, LPCTSTR lpszFolder)
{
	m_path = lpszFolder;
	if (!m_path.IsEmpty())
	{
		m_path.Replace(_T('/'), _T('\\'));
		if (_T('\\') != m_path.Right(1))
			m_path += _T('\\');
		m_path += lpszFile;
	}
}

CGenericPath::~CGenericPath(void)
{
}

/////////////////////////////////////////////////////////////////////////////

bool          CGenericPath::BuildFrom (LPCTSTR lpszPath, const CObjectType::_e _expected)
{
	if (!lpszPath)
		return false;

	switch (_expected)
	{
	case CObjectType::eFile:
		{
			m_path = lpszPath;
			m_path.Replace(_T('/'), _T('\\'));
#if(0) // incorrect behavior
			m_path.Replace(_T("..\\"), _T(""));
			m_path.Replace(_T(".\\"), _T(""));
#endif
			const INT n_pos = m_path.ReverseFind(_T('\\'));
			if (-1 != n_pos && n_pos < m_path.GetLength())
			{
				m_path = m_path.Left(n_pos + 1);
			}
		} break;
	default:
		m_path = lpszPath;
	}
	if (_T('\\') != m_path.Right(1))
		m_path += _T('\\');
	return true;
}

bool          CGenericPath::IsAbsolute(VOID)const
{
	bool bAbsolutePath = false;
	
	if (!bAbsolutePath) bAbsolutePath = ( 0 == m_path.Find(_T("\\\\"))); // network path or long path
	if (!bAbsolutePath) bAbsolutePath = (-1 != m_path.Find(_T(":")));    // logical drive

	return bAbsolutePath;
}

bool          CGenericPath::IsFolder  (VOID)const
{
	//
	// Unfortunately GetFileAttributesEx() fails on paths, which contain dot(s) inside, like c:\\folder_1\\..\\
	//
	// WIN32_FILE_ATTRIBUTE_DATA data_ = {0};
	// HRESULT hr_ = FileSystem_GetAttributes(m_path, data_);
	// if (FAILED(hr_))
	//     return false;

	const DWORD atts_ = ::GetFileAttributes(m_path.GetString());
	if (INVALID_FILE_ATTRIBUTES == atts_) {
		HRESULT hr_ = HRESULT_FROM_WIN32(::GetLastError()); hr_;
		return false;
	}
	const bool bValid = (0 != (FILE_ATTRIBUTE_DIRECTORY & atts_));
	return bValid;
}

bool          CGenericPath::IsEmpty   (VOID)const
{
	return m_path.IsEmpty();
}

bool          CGenericPath::IsExist   (VOID)const
{
	return !!::PathFileExists(m_path);
}

bool          CGenericPath::Normalize (const CObjectType::_e _type)
{
	if (this->IsEmpty())
		return false;

	bool bResult = false;

	switch (_type)
	{
	case CObjectType::eFolder:
		{
			bResult = true;
			if (this->IsExist())
			{
				bResult = this->IsFolder();
				if (!bResult) // it looks like a file
				{
					CAtlString cs_drive;
					CAtlString cs_folder;

					details::FileSystem_SplitPath(*this, cs_drive, cs_folder);

					m_path = cs_drive;
					if (!cs_folder.IsEmpty())
						m_path += cs_folder;

					bResult = true;
				}
			}
			if (m_path.Right(1) != _T("\\"))
				m_path += _T("\\");
		} break;
	}
	return bResult;
}

CAtlString    CGenericPath::ToAbsolute(VOID)const
{
	if (this->IsAbsolute())
		return CAtlString(*this);
	//
	// TODO: actually, this function did not perform its work;
	//       current folder is used for this now;
	//
	CAtlString abs_;

	CCurrentFolder current_;
	abs_ = current_.GetPathFromPattern(m_path);
	return abs_;
}

TFolderList   CGenericPath::ToFolders (VOID)const
{
	TFolderList folders_;
	if (m_path.IsEmpty())
		return folders_;

	CAtlString cs_path;

	TCHAR path_[_LONG_PATH] = {0};
	{
		::_tsplitpath_s(
				m_path,
				NULL  ,
				0     ,
				path_ ,
				_countof(path_),
				NULL  ,
				0     ,
				NULL  ,
				0
			);
		cs_path = path_;
	}
	if (_T('\\') == cs_path.GetAt(0))
		cs_path = cs_path.Right(cs_path.GetLength() - 1);

	::ATL::CAtlString cs_folder;
	INT n_pos  = 0;
	cs_folder  = cs_path.Tokenize(_T("\\"), n_pos);

	while (!cs_folder.IsEmpty())
	{
		try
		{
			folders_.push_back(cs_folder);
		}
		catch (::std::bad_alloc&)
		{
			break;
		}
		cs_folder = cs_path.Tokenize(_T("\\"), n_pos);
	}

	return folders_;
}

/////////////////////////////////////////////////////////////////////////////

CGenericPath::operator LPCTSTR(VOID)const
{
	return m_path.GetString();
}

CGenericPath& CGenericPath::operator= (LPCTSTR lpszPath)
{
	m_path = lpszPath;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CGenericPath::ConvertDosToUnc(LPCTSTR pszDosPath, CAtlString& _unc_result)
{
	HRESULT hr_ = S_OK;
	if (NULL == pszDosPath || ::_tcslen(pszDosPath) < 2)
		return (hr_ = E_INVALIDARG);

	
	if (_unc_result.IsEmpty() == false)
		_unc_result.Empty();

	TLogicalDrives drives_ = CGenericDriveEnum().Enumerate(CGenericDriveType::GetAllTypes());

	_unc_result = CGenericDriveEnum::ConvertLocalToUNC(pszDosPath, drives_);
	if (_unc_result.IsEmpty())
		hr_ = HRESULT_FROM_WIN32(ERROR_INVALID_DATA);

#if (0)
	DWORD dwBufferSize = sizeof(WCHAR);
	CRawData unc_raw(dwBufferSize);

	// (1) gets required buffer size;
	DWORD dwResult = WNetGetUniversalName(
							pszDosPath, UNIVERSAL_NAME_INFO_LEVEL, unc_raw.GetData(), &dwBufferSize
						);
	if (ERROR_MORE_DATA == dwResult) {
		hr_ = unc_raw.Create((dwBufferSize + 1) * sizeof(WCHAR));
		if (FAILED(hr_))
			return hr_;
	}
	else if (NO_ERROR != dwResult)
		return (hr_ = HRESULT_FROM_WIN32(dwResult));

	// (2) gets UNC path
	dwResult = WNetGetUniversalName(
					pszDosPath, UNIVERSAL_NAME_INFO_LEVEL, unc_raw.GetData(), &dwBufferSize
				);
	if (NO_ERROR != dwResult)
		return (hr_ = HRESULT_FROM_WIN32(dwResult));

	hr_ = unc_raw.ToStringUtf16(_unc_result);
#endif
	return hr_;
}

HRESULT       CGenericPath::ConvertUncToDos(LPCTSTR pszUncPath, CAtlString& _dos_result)
{
	HRESULT hr_ = S_OK;
	if (NULL == pszUncPath || ::_tcslen(pszUncPath) < 2)
		return (hr_ = E_INVALIDARG);

	if (_dos_result.IsEmpty() == false)
		_dos_result.Empty();

	TLogicalDrives drives_ = CGenericDriveEnum().Enumerate(CGenericDriveType::GetAllTypes());

	_dos_result = CGenericDriveEnum::ConvertUNCToLocal(pszUncPath, drives_);
	if (_dos_result.IsEmpty())
		hr_ = HRESULT_FROM_WIN32(ERROR_INVALID_DATA);

	return hr_;
}

HRESULT       CGenericPath::PathFromHandle (const ULONG _pid  , CAtlString& _dos_result) {

	HRESULT hr_ = S_OK;
	if (0 ==  _pid)
		return (hr_ = E_INVALIDARG);

	HANDLE hProcess = ::OpenProcess(
		PROCESS_QUERY_LIMITED_INFORMATION, FALSE, _pid
	);
	if (NULL == hProcess)
		return (hr_ = HRESULT_FROM_WIN32(::GetLastError()));

	TCHAR path_[_MAX_PATH] = {0};
	DWORD size_ = _countof(path_);

	const BOOL b_result = QueryFullProcessImageName(
		hProcess, 0, path_, &size_
	);
	if (FALSE == b_result)
		return (hr_ = HRESULT_FROM_WIN32(::GetLastError()));
	else
		_dos_result = path_;

	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CBaseObject::CBaseObject(const CObjectType::_e _type, LPCTSTR pszPath) : m_path(pszPath), m_type(_type)
{
}

CBaseObject::~CBaseObject(void)
{
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef     CBaseObject::Error  (VOID)const
{
	return m_error;
}

bool          CBaseObject::IsValid(VOID)const
{
	m_error.Module(_T(__FUNCTION__));

	WIN32_FILE_ATTRIBUTE_DATA data_ = {0};
	m_error = FileSystem_GetAttributes(m_path, data_);
	if (m_error)
		return false;

	bool bValid = false;
	switch (m_type)
	{
	case CObjectType::eFile  :   bValid = (0 == (FILE_ATTRIBUTE_DIRECTORY & data_.dwFileAttributes)); break;
	case CObjectType::eFolder:   bValid = (0 != (FILE_ATTRIBUTE_DIRECTORY & data_.dwFileAttributes)); break;
	}
	if (!bValid)
		m_error.SetState(
				DISP_E_TYPEMISMATCH,
				_T("The object is wrong type")
			);
	else if (m_error)
		m_error.Clear();

	return bValid;
}

const
CGenericPath& CBaseObject::Path   (VOID)const { return m_path; }
CObjectType::_e
              CBaseObject::Type   (VOID)const { return m_type; }

CAtlString    CBaseObject::UncName(VOID)const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_unc;

	CAutoHandle handle;

	switch (m_type)
	{
	case CObjectType::eFile:
		{
			handle = ::CreateFile(
						m_path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL
						); 
		} break;
	case CObjectType::eFolder:
		{
			handle = ::CreateFile(
						m_path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, NULL
						); 
		} break;
	default:;
	}
	if (handle.IsValid()){

		TCHAR buffer[_LONG_PATH] = {0};
		const DWORD dwResult = ::GetFinalPathNameByHandle(   // available from Vista
				handle, buffer, _countof(buffer), VOLUME_NAME_NT
			);
		if (!dwResult)
			m_error = ::GetLastError();
		else
			cs_unc = buffer;
	}

	return cs_unc;
}