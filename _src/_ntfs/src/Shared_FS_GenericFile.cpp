/*
	Created by Tech_dog (VToropov) 7-Jun-2016 at 8:27:02p, GMT+7, Phuket, Rawai, Tuesday;
	This is shared NTFS library generic file class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 27-May-2018 at 5:24:40p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "Shared_FS_GenericFile.h"

using namespace shared::ntfs;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace ntfs { namespace details
{
	HRESULT  FileSystem_IsTextBuffer(const PBYTE pBuffer, const DWORD dwSize)
	{
		if (!pBuffer || !dwSize)
			return E_INVALIDARG;
		HRESULT hr_ = S_OK;
		for (DWORD i_ = 0; i_ < dwSize; i_++)
		{
			const BYTE& bt_ = pBuffer[i_];
			if ((bt_ < 9) || (bt_ > 13 && bt_ < 32) || (bt_ == 127) || (bt_ == 256)) // checks for ASCII & UTF-8
			{
				hr_ = S_FALSE;
				break;
			}
		}
		return  hr_;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CGenericFile::CGenericFile(LPCTSTR lpszPath) : TObject(CObjectType::eFile, lpszPath)
{
	m_error.Source(_T("CGenericFile"));
}

CGenericFile::CGenericFile(LPCTSTR lpszFileName, LPCTSTR lpszFolder) :
	TObject(
			CObjectType::eFile,
			CAtlString(lpszFolder) + CAtlString(_T("\\")) + CAtlString(lpszFileName)
		)
{
	m_error.Source(_T("CGenericFile"));
}

CGenericFile::~CGenericFile(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CGenericFile::CopyTo(LPCTSTR lpszDestPath, const bool bOverride)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;
	if (!lpszDestPath || !::_tcslen(lpszDestPath))
		return (m_error = E_INVALIDARG);

	const BOOL bResult = ::CopyFile(
							m_path,
							lpszDestPath,
							!static_cast<BOOL>(bOverride)
						);
	if (!bResult)
		m_error = ::GetLastError();

	return m_error;
}

HRESULT       CGenericFile::CopyTo(LPCTSTR lpszDestPath, const DWORD dwStartPos, const DWORD dwLength)
{
	m_error = S_OK;
	CAutoHandle hSource  = ::CreateFile(
								TObject::m_path,
								GENERIC_READ   ,
								FILE_SHARE_READ,
								NULL           ,
								OPEN_EXISTING  ,
								FILE_ATTRIBUTE_NORMAL,
								NULL
							);
	if (!hSource.IsValid())
		return (m_error  = ::GetLastError());

	const DWORD dwResult = ::SetFilePointer(hSource, dwStartPos, NULL, FILE_CURRENT);
	if (INVALID_SET_FILE_POINTER == dwResult)
		return (m_error  = ::GetLastError());

	CAutoHandle hTarget  = ::CreateFile(
								lpszDestPath ,
								GENERIC_WRITE,
								0,
								NULL,
								CREATE_ALWAYS,
								FILE_ATTRIBUTE_NORMAL,
								NULL
							);
	if (!hTarget.IsValid())
		return (m_error  = ::GetLastError());

	CRawData raw_data(dwLength);
	if (!raw_data.IsValid())
		return (m_error  = raw_data.Error());
	{
		DWORD dwBytesRead = 0;
		const BOOL  bResult  = ::ReadFile(
								hSource,
								raw_data.GetData(),
								raw_data.GetSize(),
								&dwBytesRead,
								NULL
							);
		if (!bResult)
			return (m_error  = ::GetLastError());
	}
	{
		DWORD dwBytesWritten = 0;
		const BOOL  bResult  = ::WriteFile(
								hTarget,
								raw_data.GetData(),
								raw_data.GetSize(),
								&dwBytesWritten,
								NULL
							);
		if (!bResult)
			return (m_error  = ::GetLastError());
		else
			::FlushFileBuffers(hTarget);
	}

	return m_error;
}

HRESULT       CGenericFile::Create(const _bstr_t& bstrContent)
{
	BSTR& ref_ = (const_cast<_bstr_t&>(bstrContent)).GetBSTR();
	return this->Create(
			(PBYTE)ref_,
			(DWORD)::SysStringByteLen(ref_)
		);
}

HRESULT       CGenericFile::Create(const _variant_t& vContent)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (VT_NULL == vContent.vt || VT_EMPTY == vContent.vt)
		return (m_error = E_INVALIDARG);
	
	if (VT_BSTR != vContent.vt)
	{
		_variant_t var;
		try{
			var.ChangeType(VT_BSTR, &vContent);
		}
		catch(_com_error())
		{}

		if (VT_BSTR != var.vt)
			return (m_error = DISP_E_TYPEMISMATCH);

		this->Create((PBYTE)var.bstrVal, (DWORD)::SysStringByteLen(var.bstrVal));
	}
	else
	{
		const _variant_t& var = vContent;
		this->Create((PBYTE)var.bstrVal, (DWORD)::SysStringByteLen(var.bstrVal));
	}
	return m_error;
}

HRESULT       CGenericFile::Create(const CRawData&   _content)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!_content.IsValid())
		return (m_error = _content.Error());

	return this->Create(
			_content.GetData(),
			_content.GetSize()
		);
}

HRESULT       CGenericFile::Create(const DWORD dwSize)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;
	
	CAutoHandle hFile = ::CreateFile(
							m_path,
							GENERIC_WRITE,
							0,
							NULL,
							CREATE_ALWAYS,
							FILE_ATTRIBUTE_NORMAL,
							NULL
						);
	if (!hFile.IsValid())
		return (m_error = ::GetLastError());
	if (dwSize < 1)
		return (m_error = S_OK); // the empty file is created

	const DWORD dwDefaultChunkSize = 1024;
	const DWORD dwInitialChunkSize = (dwSize > dwDefaultChunkSize ? dwDefaultChunkSize : dwSize);
	CRawData raw_data(dwInitialChunkSize);
	if (!raw_data.IsValid())
		return (m_error = raw_data.Error());

	DWORD dwBytesWritten = 0;
	DWORD dwTotalBytesWritten = 0;
	while (dwTotalBytesWritten < dwSize)
	{
		if (!::WriteFile(
					hFile,
					raw_data.GetData(),
					raw_data.GetSize(),
					&dwBytesWritten,
					NULL
				))
			return (m_error  = ::GetLastError());

		dwTotalBytesWritten += dwBytesWritten;
		const DWORD dwBytesRest = dwSize - dwTotalBytesWritten;

		if (dwBytesRest && dwBytesRest < dwInitialChunkSize) // checks the file tail;
		{
			raw_data.Create(dwBytesRest);
			if (!raw_data.IsValid())
				return (m_error  = ::GetLastError());
		}
	}
	if (!m_error)
		::FlushFileBuffers(hFile);
	return  m_error;
}

HRESULT       CGenericFile::Create(const PBYTE pContent, const DWORD dwSize)
{
	if (!pContent || !dwSize)
		return (m_error = E_INVALIDARG);

	CAutoHandle hFile = ::CreateFile(
							m_path,
							GENERIC_WRITE,
							0,
							NULL,
							CREATE_ALWAYS,
							FILE_ATTRIBUTE_NORMAL,
							NULL
						);
	if (!hFile.IsValid())
		return (m_error = ::GetLastError());

	DWORD dwBytesWritten = 0;
	if (!::WriteFile(
				hFile,
				pContent,
				dwSize,
				&dwBytesWritten,
				NULL)
			)
		return (m_error = ::GetLastError());
	else
	{
		m_error.Clear();
		::FlushFileBuffers(hFile);
	}
	return m_error;
}

HRESULT       CGenericFile::Delete   (VOID)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsValid())
		return (m_error = OLE_E_BLANK);

	const BOOL res_ = ::DeleteFile(
							m_path
						);
	if (!res_)
		m_error = ::GetLastError();

	return m_error;
}

CAtlString    CGenericFile::FullName (VOID)const
{
	CAtlString ext_ = this->Extension();
	if (ext_.IsEmpty())
		return this->Name();
	else
	{
		CAtlString name_;
		name_.Format(
				_T("%s.%s"),
				this->Name().GetString(),
				ext_.GetString()
			);
		return name_;
	}
}

CAtlString    CGenericFile::Extension(VOID)const
{
	CAtlString cs_ext;

	TCHAR ext_[_MAX_EXT] = {0};
	{
		::_tsplitpath_s(
				m_path,
				NULL  ,
				0     ,
				NULL  ,
				0     ,
				NULL  ,
				0     ,
				ext_  ,
				_countof(ext_)
			);
		cs_ext = ext_;
	}

	return cs_ext;
}

bool          CGenericFile::IsText   (VOID)const
{
	bool bResult = false;
	CRawData buffer_;

	HRESULT hr_ = this->Read(buffer_);
	if (FAILED(hr_))
		return bResult;
	hr_ = details::FileSystem_IsTextBuffer(
						buffer_.GetData(),
						buffer_.GetSize()
					);
	if (FAILED(hr_))
	{
		m_error = hr_;
		return bResult;
	}
	else
		bResult = (S_OK == hr_);
	return bResult;
}

CAtlString    CGenericFile::Name     (VOID)const
{
	CAtlString cs_name;

	TCHAR name_[_MAX_FNAME] = {0};
	{
		::_tsplitpath_s(
				m_path,
				NULL  ,
				0     ,
				NULL  ,
				0     ,
				name_ ,
				_countof(name_),
				NULL  ,
				0
			);
		cs_name = name_;
	}

	return cs_name;
}

HRESULT       CGenericFile::Read     (CRawData& _raw_data)const
{
	m_error = S_OK;

	CAutoHandle hFile = ::CreateFile(
						m_path,
						GENERIC_READ,
						FILE_SHARE_READ,
						NULL,
						OPEN_EXISTING,
						FILE_ATTRIBUTE_NORMAL,
						NULL
					);
	if (!hFile.IsValid())
		return (m_error = ::GetLastError());

	const DWORD dwSize  = ::GetFileSize(hFile, NULL);
	if (INVALID_FILE_SIZE == dwSize)
		return (m_error = ::GetLastError());

	m_error = _raw_data.Create(dwSize);
	if (m_error)
		return  m_error;

	DWORD dwBytesRead = 0;
	if (::ReadFile(
				hFile,
				_raw_data.GetData(),
				_raw_data.GetSize(),
				&dwBytesRead,
				NULL
			))
		m_error = ::GetLastError();

	return m_error;
}

HRESULT       CGenericFile::ReadAsText(_variant_t& vContent)const
{
	CAtlString cs_data;

	HRESULT hr_ = this->ReadAsText(cs_data);
	if (!FAILED(hr_))
	{
		vContent.Clear();
		vContent.bstrVal = cs_data.AllocSysString();
		vContent.vt      = VT_BSTR;
	}
	else
		vContent = _T("");
	return hr_;
}

HRESULT       CGenericFile::ReadAsText(CAtlString& _content, const bool bAssumeUtf8)const
{
	CRawData raw_data_;
	HRESULT hr_ = this->Read(raw_data_);
	if (FAILED(hr_))
		return hr_;
	
	if (!CGenericFile::IsTextBuffer(
				raw_data_,
				raw_data_.GetSize()
			))
		return (m_error = (DWORD)ERROR_INVALID_DATA);

	if (bAssumeUtf8){
		CAtlStringA buffer_(
				reinterpret_cast<char*>(raw_data_.GetData()),
				static_cast<INT>(raw_data_.GetSize())
			);

		_content = buffer_;
	}
	else {
		_content.Append(
				reinterpret_cast<WCHAR*>(raw_data_.GetData()),
				static_cast<INT>(raw_data_.GetSize()/sizeof(WCHAR))
			);
	}
	return m_error;
}

LONGLONG      CGenericFile::Size     (VOID)const
{
	m_error.Module(_T(__FUNCTION__));

	if (!TObject::IsValid())
		return 0;

	LARGE_INTEGER sz_ = {0};

	WIN32_FILE_ATTRIBUTE_DATA data_ = {0};
	m_error = FileSystem_GetAttributes(m_path, data_);

	if (!m_error)
	{
		sz_.HighPart = data_.nFileSizeHigh;
		sz_.LowPart  = data_.nFileSizeLow;
	}

	return sz_.QuadPart;
}

/////////////////////////////////////////////////////////////////////////////

bool          CGenericFile::IsTextBuffer(const CRawData _buf,  const DWORD dwSizeToCheck)
{
	if (!_buf.IsValid() || !dwSizeToCheck)
		return false;

	const DWORD dw_len = (_buf.GetSize() < dwSizeToCheck ? _buf.GetSize() : dwSizeToCheck);
	return (S_OK == details::FileSystem_IsTextBuffer(_buf.GetData(), dw_len));
}

/////////////////////////////////////////////////////////////////////////////

CFileLocker::CFileLocker(void)
{
}

CFileLocker::CFileLocker(LPCTSTR pszFilePath) : m_path(pszFilePath)
{
}

CFileLocker::~CFileLocker(void)
{
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef     CFileLocker::Error(void)const
{
	return m_error;
}

bool          CFileLocker::IsLocked(void)const
{
	return m_file.IsValid();
}

HRESULT       CFileLocker::Lock(void)
{
	if (this->IsLocked())
		return S_OK;
	m_file = ::CreateFile(
				m_path.GetString(),
				GENERIC_READ,
				0,
				NULL,
				OPEN_EXISTING,
				FILE_ATTRIBUTE_NORMAL,
				NULL
			);
	if (!m_file.IsValid())
		return (m_error = ::GetLastError());
	else
		return (m_error = S_OK);
}

LPCTSTR       CFileLocker::LockedPath(void)const
{
	return m_path.GetString();
}

HRESULT       CFileLocker::LockedPath(LPCTSTR pszFilePath)
{
	if (this->IsLocked())
		this->Unlock();
	m_path = pszFilePath;
	if (m_path.IsEmpty())
		return (m_error = E_INVALIDARG);
	else
		return  S_OK;
}

HRESULT       CFileLocker::Unlock(void)
{
	if (!this->IsLocked())
	{
		if (OLE_E_BLANK == m_error.GetHresult())
			m_error = ERROR_NOT_LOCKED;
		return m_error;
	}
	m_file.Reset();
	m_error.Reset();
	return S_OK;
}