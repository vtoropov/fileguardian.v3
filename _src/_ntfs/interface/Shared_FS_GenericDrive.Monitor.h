#ifndef _SHAREDFSGEMERICDRIVEMONITOR_H_3FF6F9A5_A8BF_4ae4_8FBB_32130C36132D_INCLUDED
#define _SHAREDFSGEMERICDRIVEMONITOR_H_3FF6F9A5_A8BF_4ae4_8FBB_32130C36132D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-May-2016 at 4:42:54pm, GMT+7, Phuket, Rawai, Sunday;
	This is Shared NTFS library generic drive monitor wrapper class(es) interface declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_SystemThreadPool.h"

#include "Shared_FS_CommonDefs.h"

namespace shared { namespace ntfs
{
	using shared::lite::common::CSysError;
	using shared::lite::common::CSysErrorSafe;
	using shared::lite::sys_core::CThreadBase;

	interface IRemovableDriveEventSink {
		virtual HRESULT IRemovableDrive_BeingAdded(const TDriveList&) PURE;
		virtual HRESULT IRemovableDrive_BeingEjected(const TDriveList&) PURE;
	};

	class CRemovableDriveMonitor : public CThreadBase
	{
		typedef CThreadBase TBase;
	private:
		IRemovableDriveEventSink&       m_sink;
	public:
		CRemovableDriveMonitor(IRemovableDriveEventSink&);
	private:
		VOID       ThreadFunction(void) override sealed;
	};
}}

#endif/*_SHAREDFSGEMERICDRIVEMONITOR_H_3FF6F9A5_A8BF_4ae4_8FBB_32130C36132D_INCLUDED*/