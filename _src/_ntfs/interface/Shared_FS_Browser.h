#ifndef _SHAREDFILEDIALOG_H_5E64C1CC_F15B_4e39_BEC3_F2165915AA84_INCLUDED
#define _SHAREDFILEDIALOG_H_5E64C1CC_F15B_4e39_BEC3_F2165915AA84_INCLUDED
/*
	Created by Tech_dog (VToropov) on 13-May-2016 at 0:13:13am, GMT+7, Phuket, Rawai, Friday;
	This is Shared Lite Library Extended File/Folder Browser Dialog class(es) declaration file.
*/
#include "Shared_FS_CommonDefs.h"

namespace shared { namespace ntfs
{
	using shared::lite::common::CSysError;

	class CFolderBrowser
	{
	private:
		CSysError   m_error;
		CWindow     m_parent;
		CAtlString  m_title;
	public:
		CFolderBrowser(const HWND _parent, LPCTSTR lpszTitle);
	public:
		TErrorRef   Error(void)const;
		HRESULT     SelectFolder(LPCTSTR lpszInitFolder, CAtlString& _selected);
	};
}}

#endif/*_SHAREDFILEDIALOG_H_5E64C1CC_F15B_4e39_BEC3_F2165915AA84_INCLUDED*/