#ifndef _SHAREDFSGENERICFILE_H_6C735A1D_F6B4_4724_8586_AAFE0E22DF44_INCLUDED
#define _SHAREDFSGENERICFILE_H_6C735A1D_F6B4_4724_8586_AAFE0E22DF44_INCLUDED
/*
	Created by Tech_dog (VToropov) on 8-Jun-2016 at 11:35:37p, GMT+7, Phuket, Rawai, Wednesday;
	This is shared NTFS library generic file class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 5:33:14p, UTC+7, Phuket, Rawai, Monday;
*/
#include "Shared_FS_CommonDefs.h"
#include "generic.stg.data.h"
#include "Shared_GenericHandle.h"

namespace shared { namespace ntfs
{
	using shared::lite::common::CSysError;
	using shared::lite::common::CAutoHandle;

	using shared::lite::data::CRawData;

	class CGenericFile : public CBaseObject
	{
		typedef CBaseObject TObject;
	public:
		CGenericFile(LPCTSTR lpszPath);
		CGenericFile(LPCTSTR lpszFileName, LPCTSTR lpszFolder);
		~CGenericFile(void);
	public:
		HRESULT       CopyTo(LPCTSTR lpszDestPath, const bool bOverride = false);                 // copies this file object to the destination file specified;
		HRESULT       CopyTo(LPCTSTR lpszDestPath, const DWORD dwStartPos, const DWORD dwLength); // copies data from specified position and length to the destination file;
		HRESULT       Create(const _bstr_t& bstrContent);                                         // creates a file with content provided
		HRESULT       Create(const _variant_t& vContent);                                         // creates a file with content provided, VT_BSTR is expected
		HRESULT       Create(const CRawData&   _content);                                         // creates a file from memory block
		HRESULT       Create(const DWORD dwSize);                       // creates file (filled by zeros) of specified size
		HRESULT       Create(const PBYTE pContent, const DWORD dwSize); // creates file with content provided
		HRESULT       Delete   (VOID)     ;                             // deletes this file
		CAtlString    FullName (VOID)const;                             // returns file name.extension (if any)
		CAtlString    Extension(VOID)const;                             // returns file extension if any
		bool          IsText   (VOID)const;                             // checks file content for text only; returns true if text, otherwise, false;
		CAtlString    Name     (VOID)const;                             // returns file name (without folder(s) and file extension)
		HRESULT       Read     (CRawData&)const;                        // reads file content to a buffer provided
		HRESULT       ReadAsText(_variant_t& vContent)const;            // not intended for reading large files
		HRESULT       ReadAsText(CAtlString& _content, const bool bAssumeUtf8 = true)const; // not intended for reading large files
		LONGLONG      Size     (VOID)const;                             // returns file size
	public:
		static bool   IsTextBuffer(const CRawData _buf,  const DWORD dwSizeToCheck);
	};

	class CFileLocker
	{
	private:
		CAtlString        m_path;
		CAutoHandle       m_file;
		CSysError         m_error;
	public:
		CFileLocker(void);
		CFileLocker(LPCTSTR pszFilePath);
		~CFileLocker(void);
	public:
		TErrorRef         Error(void) const;
		bool              IsLocked(void)const;
		HRESULT           Lock(void);
		LPCTSTR           LockedPath(void)const;
		HRESULT           LockedPath(LPCTSTR lpszFilePath);
		HRESULT           Unlock(void);
	};
}}

#endif/*_SHAREDFSGENERICFILE_H_6C735A1D_F6B4_4724_8586_AAFE0E22DF44_INCLUDED*/