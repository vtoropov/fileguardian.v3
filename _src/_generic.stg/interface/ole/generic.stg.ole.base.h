#ifndef _GENERICSTGOLEBASE_H_A1EA0E18_4329_44E4_BB0D_D3994A8460E1_INCLUDED
#define _GENERICSTGOLEBASE_H_A1EA0E18_4329_44E4_BB0D_D3994A8460E1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Jul-2018 at 6:44:57p, UTC+7, Phuket, Rawai, Wednesday;
	This is shared lite generic OLE data provider base interface declaration file.
*/
#include "Shared_SystemError.h"

namespace shared { namespace stg { namespace ole {

	using shared::lite::common::CSysError;

	class COleAccLevel {
	public:
		enum _e : ULONG {
			eRead     = STGM_READ | STGM_SHARE_DENY_WRITE,
			eRead_Ex  = STGM_READ | STGM_SHARE_EXCLUSIVE ,
		};
	};

	class COleType {
	public:
		enum _e : ULONG {
			eStorage  = STGTY_STORAGE   ,  // indicates that the storage element is a storage object;
			eStream   = STGTY_STREAM    ,  // indicates that the storage element is a stream object;
			eBinData  = STGTY_LOCKBYTES ,  // indicates that the storage element is a byte-array object;
			eProperty = STGTY_PROPERTY  ,  // indicates that the storage element is a property storage object;
		};

	public:
		static _e DwordToEnum(const DWORD);
	};

	class COleStats {
		// https://docs.microsoft.com/en-us/windows/desktop/api/objidl/ns-objidl-tagstatstg
	protected:
		CAtlString      m_name;            // an object name;
		FILETIME        m_access;          // indicates the last access time for this storage, stream, or byte array;
		FILETIME        m_create;          // indicates the creation time for this storage, stream, or byte array;
		FILETIME        m_modify;          // indicates the last modification time for this storage, stream, or byte array;
		COleType::_e    m_type;            // indicates the type of storage object;
		ULARGE_INTEGER  m_size;            // specifies the size, in bytes, of the stream or byte array;
		CLSID           m_uuid;            // indicates the class identifier for the storage object;
		                                   // set to CLSID_NULL for new storage objects; this member is not used for streams or byte arrays;
	public:
		COleStats(void);
		~COleStats(void);

	public:
		CLSID           Class     (void) const;
		FILETIME        Created   (void) const;
		FILETIME        LastAccess(void) const;
		FILETIME        Modified  (void) const;
		LPCTSTR         Name      (void) const;
		ULARGE_INTEGER  Size      (void) const;
		COleType::_e    Type      (void) const;
	};

	class COleStats_Ex : public COleStats {

		typedef COleStats TBase;

	public:
		COleStats_Ex(void);
		~COleStats_Ex(void);

	public:
		CAtlString      ClassAsText(void) const;
		SYSTEMTIME      Created    (void) const;
		SYSTEMTIME      LastAccess (void) const;
		SYSTEMTIME      Modified   (void) const;
		DOUBLE          SizeInKb   (void) const;
		DOUBLE          SizeInMb   (void) const;
		CAtlString      TypeAsText (void) const;
	public:
		COleStats_Ex& operator=(const STATSTG&); // object name is released by this operator;
	};

	class COleBase {
	protected:
		DWORD           m_level;           // a level of an object in data hierarchy;
		mutable
		CSysError       m_error;
		CAtlString      m_name ;
		COleStats_Ex    m_stats;

	protected:
		COleBase(LPCTSTR lpszModule);
	public:
		TErrorRef       Error(void) const;
		DWORD           Level(void) const;
		LPCTSTR         Name (void) const;
		const
		COleStats_Ex&   Stats(void) const;
	};
	//
	// COM interface pointer type definitions;
	//
	typedef CComPtr<IStream>  t_ole_stream;
	typedef CComPtr<IPropertyStorage> t_ole_prop_stg;
	typedef CComPtr<IPropertySetStorage> t_ole_prop_set_stg;
	typedef CComPtr<IStorage> t_ole_stg_ptr;
}}}

#endif/*_GENERICSTGOLEBASE_H_A1EA0E18_4329_44E4_BB0D_D3994A8460E1_INCLUDED*/