#ifndef _GENERICSTGOLE_H_5C0765CB_CC71_4549_B39E_007BCE7C789E_INCLUDED
#define _GENERICSTGOLE_H_5C0765CB_CC71_4549_B39E_007BCE7C789E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Jul-2018 at 4:56:13p, UTC+7, Phuket, Rawai, Tuesday;
	This is shared lite generic OLE data provider interface declaration file.
*/
#include "generic.stg.ole.base.h"
#include "generic.stg.ole.prop.h"
#include "generic.stg.ole.stream.h"

namespace shared { namespace stg { namespace ole {

	class COleStorage;
	typedef ::std::vector<COleStorage>     t_ole_sub_stg;

	class COleStgType {
		// https://msdn.microsoft.com/en-us/library/windows/desktop/aa380330(v=vs.85).aspx
	public:
		enum _e {
			eStorage = STGFMT_STORAGE, // indicates that the file must be a compound file;
			eFile    = STGFMT_FILE   , // indicates that the file must not be a compound file;
			eAny     = STGFMT_ANY    , // indicates that the system will determine the file type;
			eOleDoc  = STGFMT_DOCFILE, // indicates that the file must be a compound file;
		};
	public:
		static FMTID   StgTypeToId(const _e);
	};

	class COleStorage : public COleBase {

		typedef COleBase TBase;

	private:
		t_ole_stg_ptr   m_stg_ptr;     // storage pointer;
		t_ole_prop_sets m_props;       // storage properties;
		t_ole_sub_stg   m_stg_set;     // set of child storage(s);
		t_ole_streams   m_streams;     // stream objects;
		CAtlString      m_name;        // storage name;

	public:
		COleStorage(const DWORD _level = 0);
		~COleStorage(void);

	public:
		HRESULT         Close (void)      ;
		bool            IsOpen(void) const;
		bool            IsRoot(void) const;
		HRESULT         Open  (LPCTSTR lpszPath, const DWORD dwFlags = COleAccLevel::eRead_Ex); // opens ole storage without providing security level;
		HRESULT         Open  (CComPtr<IStorage>& _p_stg, const DWORD dwFlags);                 // opens sub-storage for enumerating its content;
		HRESULT         OpenEx(LPCTSTR lpszPath, const DWORD dwFlags = COleAccLevel::eRead_Ex);
		const
		t_ole_sub_stg&  Storages(void) const;
		const
		t_ole_streams&  Streams (void) const;
	};
}}}

#endif/*_GENERICSTGOLE_H_5C0765CB_CC71_4549_B39E_007BCE7C789E_INCLUDED*/