#ifndef _GENERICSTGOLEPROP_H_5F5BE918_71C7_4E33_8D7D_0A079A291B52_INCLUDED
#define _GENERICSTGOLEPROP_H_5F5BE918_71C7_4E33_8D7D_0A079A291B52_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Jul-2018 at 9:30:37p, UTC+7, Phuket, Rawai, Wednesday;
	This is shared lite generic OLE data property interface declaration file.
*/
#include "generic.stg.ole.base.h"

namespace shared { namespace stg { namespace ole {

	class COlePropType {
		// https://docs.microsoft.com/en-us/windows/desktop/Stg/predefined-property-set-format-identifiers
	public:
		enum _e {
			eSummary   = 0x0,  // generic summary property set;
			eDocument  = 0x1,  // document summary information;
			eCustom    = 0x2,  // user defined or custom property set;
			eDiscarded = 0x3,  // discardable information property set;
			eImageInfo = 0x4,  // image information summary;
			eAudioInfo = 0x5,  // audio information summary;
			eVideoInfo = 0x6,  // video information summary;
			eMediaInfo = 0x7,  // media file information property set;
		};
	public:
		static GUID     TypeToFormatId(const _e);
	};

	class COleProperty {
	private:
		CAtlString      m_name;
		_variant_t      m_value;
		PROPID          m_p_id;   // property identifier, actually, ULONG data type;

	public:
		COleProperty(void);
		~COleProperty(void);

	public:
		LPCTSTR        Name(void)const;
	};

	typedef ::std::vector<COleProperty>  t_ole_p;
	class COlePropertySet : public COleBase {

	private:
		DWORD         m_dwFlags;
	public:
		COlePropertySet(void);
		~COlePropertySet(void);

	public:
		HRESULT       Enumerate(t_ole_prop_stg& _stg, const GUID&);  // enumerates all properties for provided set;
		HRESULT       Enumerate(t_ole_prop_set_stg& _stg);           // enumerates all available property sets;
		HRESULT       Enumerate(t_ole_stg_ptr& _stg);
	};

	typedef ::std::vector<COlePropertySet> t_ole_prop_sets;

}}}

#endif/*_GENERICSTGOLEPROP_H_5F5BE918_71C7_4E33_8D7D_0A079A291B52_INCLUDED*/