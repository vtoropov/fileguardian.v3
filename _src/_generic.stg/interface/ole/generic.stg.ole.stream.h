#ifndef _GENERICSTGOLESTREAM_H_412CB62E_C774_48BA_8591_2BB1DC73DCDC_INCLUDED
#define _GENERICSTGOLESTREAM_H_412CB62E_C774_48BA_8591_2BB1DC73DCDC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Jul-2018 at 9:59:10p, UTC+7, Phuket, Rawai, Wednesday;
	This is shared lite generic OLE data stream interface declaration file.
*/
#include "generic.stg.ole.base.h"
#include "generic.stg.ole.prop.h"
#include "generic.stg.data.h"

namespace shared { namespace stg { namespace ole {

	using shared::lite::data::CRawData;

	class COleStream : public COleBase {

		typedef COleBase TBase;
	private:
		t_ole_stream  m_strm_ptr;
	public:
		COleStream(const DWORD dwLevel = 0);
		~COleStream(void);

	public:
		HRESULT       Open  (t_ole_stream& _p_stg, const DWORD dwFlags);
		HRESULT       Read  (CRawData& _data) const;
	};

	typedef ::std::vector<COleStream> t_ole_streams;
}}}

#endif/*_GENERICSTGOLESTREAM_H_412CB62E_C774_48BA_8591_2BB1DC73DCDC_INCLUDED*/