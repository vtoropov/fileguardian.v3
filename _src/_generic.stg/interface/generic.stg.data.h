#ifndef _SHAREDLITELIBRARYRAWDATA_H_4916FDBD_664D_48f6_A467_B1F03F356B69_INCLUDED
#define _SHAREDLITELIBRARYRAWDATA_H_4916FDBD_664D_48f6_A467_B1F03F356B69_INCLUDED
/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 6:56:06am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Library raw data class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 11:58:22a, UTC+7, Phuket, Rawai, Monday;
*/
#include "Shared_SystemError.h"

namespace shared { namespace lite { namespace data
{
	using shared::lite::common::CSysError;

	class CRawBuffer {
	protected:
		PBYTE             m_pData;
		DWORD             m_dwSize;
	protected:
		CRawBuffer(void);
	};

	class CRawData : private CRawBuffer
	{
		typedef CRawBuffer TBuffer;
	private:
		mutable
		CSysError         m_error;
	public:
		CRawData(void);
		CRawData(const _variant_t&);       // creates from bstr data
		CRawData(const DWORD dwSize);
		CRawData(const PBYTE pData, const DWORD dwSize);
		~CRawData(void);
	public:
		HRESULT           Append (const CRawData&);
		HRESULT           Append (const PBYTE pData, const DWORD dwSize);
		HRESULT           CopyToVariantAsArray(_variant_t&)const; // data is interpreted as binary and is copied to safe array as is;
		HRESULT           CopyToVariantAsUtf16(_variant_t&)const; // data is interpreted as Utf8 text and is converted to Utf16 bstr;
		HRESULT           CopyToVariantAsUtf8 (_variant_t&)const; // data is interpreted as Utf8 text and is copied to bstr as is;
		HRESULT           Create (const DWORD dwSize);
		HRESULT           Create (const PBYTE pData, const DWORD dwSize);
		TErrorRef         Error  (void) const;
		const PBYTE       GetData(void) const;
		PBYTE             GetData(void)      ;
		DWORD             GetSize(void) const;
		bool              IsValid(void) const;
		HRESULT           Reset  (void)      ;
		HRESULT           ToStringUtf16(CAtlStringW&)const;  // assumes the buffer contains unicode string
		HRESULT           ToStringUtf8 (CAtlStringA&)const;  // assumes the buffer contains ansi string
	public:
		operator const    PBYTE(void)const;
		operator          PBYTE(void);
	public:
		CRawData(const CRawData&);
		CRawData& operator+=(const CRawData&);
		CRawData& operator= (const CRawData&);
		CRawData& operator= (const _variant_t&); // creates from bstr data
	};

	class CSafeArrayAccessor
	{
	private:
		mutable
		CSysError         m_error;
		SAFEARRAY*        m_sa_ptr;
	private:
		UCHAR HUGEP*      m_data_ptr;
	public:
		CSafeArrayAccessor(const _variant_t&);             // variant type must be VT_ARRAY|VT_UI1
		CSafeArrayAccessor(SAFEARRAY*);                    // safe array must contain VT_UI1 data
		~CSafeArrayAccessor(void);
	public:
		PBYTE             AccessData(void);                // gets access to safe array data pointer
		TErrorRef         Error(VOID)   const;             // gets error object reference
		HRESULT           GetSize(LONG& n_size_ref) const; // gets the size of the safe array data in bytes
		bool              IsValid(void) const;             // checks the validity of the data accessor object
	private:
		CSafeArrayAccessor(const CSafeArrayAccessor&);
		CSafeArrayAccessor& operator= (const CSafeArrayAccessor&);
	};
}}}

#endif/*_SHAREDLITELIBRARYRAWDATA_H_4916FDBD_664D_48f6_A467_B1F03F356B69_INCLUDED*/