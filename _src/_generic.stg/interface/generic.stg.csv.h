#ifndef _SHAREDGENERICCSVPROVIDER_H_7E2C8E5E_F55B_40f8_9A14_AF78E1ABA691_INCLUDED
#define _SHAREDGENERICCSVPROVIDER_H_7E2C8E5E_F55B_40f8_9A14_AF78E1ABA691_INCLUDED
/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 11:44:29am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Generic CSV data Provider class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 11:55:06a, UTC+7, Phuket, Rawai, Monday;
*/
namespace shared { namespace lite { namespace data
{
	class eCsvFileOption
	{
	public:
		enum _enum{
			eNone              = 0x0,
			eUseCommaSeparator = 0x1,
			eUseSemicolon      = 0x2,
		};
	};

	class CCsvFile
	{
	public:
		typedef ::std::vector<::ATL::CAtlString>  THeader;
		typedef ::std::vector<::ATL::CAtlString>  TRow;
	private:
		typedef ::std::vector<TRow>               TTable;
	private:
		const DWORD     m_dwOptions;
		mutable HRESULT m_result;
		THeader         m_header;
		TTable          m_table;
		bool            m_has_header;
		DWORD           m_src_size;        // source file size in bytes;
		CAtlString      m_src_path;
	public:
		CCsvFile(const DWORD dwOptions = eCsvFileOption::eNone);
		~CCsvFile(void);
	public:
		HRESULT         AddRow(const TRow&);
		HRESULT         AppendToFile(LPCTSTR pFile, const TRow&);         // opens the file specified and appends the row to the file
		LPCTSTR         Cell(const INT row__, const INT col__) const;
		HRESULT         Clear(void);
		HRESULT         Create(const ::ATL::CAtlString& buffer_ref, const bool bHasHeader = false);
		DOUBLE          Double(const INT row__, const INT col__) const;
		LONG            FieldCount(void) const;
		HRESULT         GetLastResult(void) const;
		bool            HasHeader(void) const;
		const THeader&  Header(void) const;
		HRESULT         Header(const THeader&);
		HRESULT         Load(LPCTSTR pFile, const bool bHasHeader = false);
		const TRow&     Row(const INT row__) const;
		LONG            RowCount(void) const;
		HRESULT         Save(LPCTSTR pFile, const bool bHasHeader = false);
		LPCTSTR         SourceFilePath(void)const;
		DWORD           SourceFileSize(void)const;                        // gets source file size in bytes
	public:
		static HRESULT  CreateFileFromHeader(LPCTSTR lpszFilePath, const THeader&, const DWORD dwOptions);
		static bool     IsFileExist(LPCTSTR lpszFilePath);
	};

	class CCsvRecordSpecBase
	{
	protected:
		typedef ::std::vector<CAtlString>  TFields;
		typedef ::std::pair<DWORD, DWORD>  TAcceptRange;
	protected:
		TFields           m_fields;
		TAcceptRange      m_range;
	protected:
		CCsvRecordSpecBase(void);
	public:
		CCsvFile::THeader CreateHeader(void)const;
		INT               FieldCount(void)const;
		CAtlString        FieldNameOf(const INT nIndex)const;
		INT               GetHeaderLen(void)const;
		HRESULT           ValidateData(const CCsvFile&, const bool bShowError);
	};
}}}

#endif/*_SHAREDGENERICCSVPROVIDER_H_7E2C8E5E_F55B_40f8_9A14_AF78E1ABA691_INCLUDED*/