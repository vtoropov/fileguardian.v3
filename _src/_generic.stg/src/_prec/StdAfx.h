#ifndef _SHAREDGENERICSTGSTDAFX_H_657257D1_C6E5_4B62_9AAF_A8B32FDDE71A_INCLUDED
#define _SHAREDGENERICSTGSTDAFX_H_657257D1_C6E5_4B62_9AAF_A8B32FDDE71A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-May-2018 at 10:48:57a, UTC+7, Phuket, Rawai, Monday; 
	This is Shared Generic Storage Library Precompiled Header declaration file.
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600 // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600 // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700 // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481) // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996) // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlfile.h>
#include <comdef.h> // for _com_error

#include <vector>
#include <map>

#pragma comment( lib, "ole32.lib" )

#endif/*_SHAREDGENERICSTGSTDAFX_H_657257D1_C6E5_4B62_9AAF_A8B32FDDE71A_INCLUDED*/