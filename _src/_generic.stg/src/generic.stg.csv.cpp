/*
	Created by Tech_dog (VToropov) at 19-Mar-2014 at 12:12:15pm, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Generic CSV data Provider class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 12:15:53p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "generic.stg.csv.h"
#include "generic.stg.data.h"

using namespace shared::lite::data;

#include "Shared_GenericAppObject.h"
#include "Shared_SystemError.h"

using namespace shared::lite::common;
using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace data { namespace details
{
	const LPCTSTR CsvFile__CSV_SEP_TOKEN  = _T("\t");
	const LPCTSTR CsvFile__CSV_ROW_TOKEN  = _T("\n");
	const LPCTSTR CsvFile__CSV_END_TOKEN  = _T("\r");
	const LPCTSTR CsvFile__CSV_EMPTY_DATA = _T("n/a");
	const LPCTSTR CsvFile__CSV_SEP_TOKEN2 = _T(";");
	const LPCTSTR CsvFile__CSV_SEP_TOKEN4 = _T(",");

	static const CCsvFile::TRow&  CsvFile_InvalidCsvRow_Ref(void)
	{
		static const CCsvFile::TRow row__;
		return row__;
	}
	static CCsvFile::TRow&        CsvFile_InvalidCsvRow_Ref2(void)
	{
		static CCsvFile::TRow row__;
		return row__;
	}

	// this function is based on assumption that ANSI text (english base character set) is represented
	// by UTF-16; that means the follower byte is always 0x0;
	static bool    CsvFile_IsUnicodeData(const CRawData& _data, const DWORD _byte_to_check = 0x64)
	{
		const PBYTE pbData = _data.GetData();
		const DWORD dwSize = _data.GetSize();
		for ( DWORD i_ = 1; i_ < dwSize && i_ < _byte_to_check; i_ += 2)
		{
			if (0 == (pbData[i_ - 1] & 0x80) &&
				0 == (pbData[i_ - 0]))
				return true; // UTF-16 is detected
		}
		return false;
	}

	static LPCTSTR CsvFile_GetSeparator(const DWORD dwOptions)
	{
		return (eCsvFileOption::eUseSemicolon & dwOptions ? CsvFile__CSV_SEP_TOKEN2 :
				(eCsvFileOption::eUseCommaSeparator & dwOptions ? CsvFile__CSV_SEP_TOKEN4 : CsvFile__CSV_SEP_TOKEN));
	}
	static HRESULT CsvFile_WriteLine(::ATL::CAtlFile& file_, const ::std::vector<::ATL::CAtlString>& line_, LPCTSTR pFieldDivider)
	{
		HRESULT hr_ = S_FALSE;
		::ATL::CAtlString cs_buffer;
		const LONG nFields = (LONG)line_.size();
		for ( LONG i_ = 0; i_ < nFields; i_++)
		{
			cs_buffer += line_[i_].GetString();
			if (i_ < nFields - 1)
				cs_buffer += pFieldDivider;
			else
				cs_buffer += CsvFile__CSV_ROW_TOKEN;
		}
		if (!cs_buffer.IsEmpty())
		{
			const DWORD dwSize = sizeof(TCHAR) * cs_buffer.GetLength();
			hr_ = file_.Write((LPCVOID)cs_buffer.GetBuffer(), dwSize);
		}
		return hr_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CCsvFile::CCsvFile(const DWORD dwOptions):
	m_result(OLE_E_BLANK),
	m_has_header(false),
	m_dwOptions(dwOptions),
	m_src_size(0)
{
}

CCsvFile::~CCsvFile(void)
{
	Clear();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CCsvFile::AddRow(const TRow& row_ref)
{
	try
	{
		m_table.push_back(row_ref);
	}
	catch(::std::bad_alloc&){ return E_OUTOFMEMORY;}
	return S_OK;
}

HRESULT    CCsvFile::AppendToFile(LPCTSTR pFile, const TRow& row_ref)
{
	m_src_path = pFile;

	::ATL::CAtlFile file__;
	HRESULT hr__ =  file__.Create(pFile, FILE_APPEND_DATA, FILE_SHARE_WRITE, OPEN_ALWAYS);
	if (S_OK != hr__)
		return  hr__;
	hr__ = file__.Seek(0, FILE_END);
	if (S_OK != hr__)
		return  hr__;
	LPCTSTR pFieldDivider = details::CsvFile_GetSeparator(m_dwOptions);
	hr__ = details::CsvFile_WriteLine(file__, row_ref, pFieldDivider);
	if (S_OK == hr__)
		hr__ = file__.Flush();
	return  hr__;
}

LPCTSTR    CCsvFile::Cell(const INT row__, const INT col__) const
{
	if (row__ < 0 || col__ < 0)
		return NULL;
	if (row__ > INT(m_table.size()) - 1)
		return NULL;
	const CCsvFile::TRow& row_ref = m_table[row__];
	if (col__ > (LONG)row_ref.size() - 1)
		return NULL;
	const ::ATL::CAtlString& cell_ref = row_ref[col__];
	return  cell_ref.GetString();
}

HRESULT    CCsvFile::Clear(void)
{
	m_result = OLE_E_BLANK;
	if (!m_header.empty())
		 m_header.clear();
	m_has_header = false;
	if (!RowCount())
		return S_FALSE;
	for (TTable::iterator t_iter = m_table.begin(); t_iter != m_table.end(); ++t_iter)
	{
		CCsvFile::TRow&  row_ref = *t_iter;
		row_ref.clear();
	}
	m_table.clear();
	m_src_size = 0;
	m_src_path.Empty();

	return S_OK;
}

HRESULT    CCsvFile::Create(const ::ATL::CAtlString& buffer_ref, const bool bHasHeader)
{
	Clear();
	if (true == buffer_ref.IsEmpty())
		return S_FALSE;
	m_has_header = bHasHeader;
	HRESULT hr__ = S_OK;
	LPCTSTR pFieldDivider = details::CsvFile_GetSeparator(m_dwOptions);
	try
	{
		INT row__ = NULL;
		::ATL::CAtlString row_token__;
		row_token__ = buffer_ref.Tokenize(details::CsvFile__CSV_ROW_TOKEN, row__);
		bool bHeaderIsInitialized = false;
		while (!row_token__.IsEmpty())
		{
			CCsvFile::TRow row_data_;
			INT col__ = NULL;
			::ATL::CAtlString col_token__;
			col_token__ = row_token__.Tokenize(pFieldDivider, col__);
			while (!col_token__.IsEmpty())
			{
				row_data_.push_back(col_token__);
				col_token__ = row_token__.Tokenize(pFieldDivider, col__);
			}
			if (m_has_header && !bHeaderIsInitialized)
			{
				bHeaderIsInitialized = true;
				if (m_header.empty() == false)
					m_header.clear();
				m_header = row_data_;
			}
			else
			{
				m_table.push_back(row_data_);
			}
			row_token__ = buffer_ref.Tokenize(details::CsvFile__CSV_ROW_TOKEN, row__);
		}
	}
	catch(::std::bad_alloc&)
	{
		ATLASSERT(FALSE);
		hr__ = E_OUTOFMEMORY;
	}
	m_result = hr__;
	return  hr__;
}

DOUBLE     CCsvFile::Double(const INT row__, const INT col__) const
{
	LPCTSTR pCell = Cell(row__, col__);
	if (pCell && ::_tcslen(pCell))
		return ::_tstof(pCell);
	else
		return 0.0f;
}

LONG       CCsvFile::FieldCount(void) const
{
	return (LONG)m_header.size();
}

HRESULT    CCsvFile::GetLastResult(void) const
{
	return m_result;
}

bool       CCsvFile::HasHeader(void) const
{
	return m_has_header;
}

const CCsvFile::THeader&
           CCsvFile::Header(void) const
{
	return m_header;
}

HRESULT    CCsvFile::Header(const THeader& hdr_ref)
{
	if (!m_header.empty() || !m_table.empty())
		return HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED);
	m_header = hdr_ref;
	m_has_header = !m_header.empty();
	return (m_result = S_OK);
}

HRESULT    CCsvFile::Load(LPCTSTR pFile, const bool bHasHeader)
{
	m_src_path = pFile;

	::ATL::CAtlFile file__;
	HRESULT hr__ = file__.Create(pFile, FILE_READ_DATA, FILE_SHARE_READ, OPEN_EXISTING);
	if (S_OK != hr__)
		return  hr__;
	ULONGLONG size__ = 0;
	hr__ = file__.Seek(0, FILE_BEGIN);
	if (S_OK != hr__)
		return  hr__;
	hr__ = file__.GetSize(size__);
	if (S_OK != hr__)
		return  hr__;

	m_src_size = (size__ & 0xFFFFFFFF);
	if (m_src_size)
	{
		const INT n__sz__  = static_cast<INT>(m_src_size);
		CRawData ptr__(n__sz__);
		hr__ = ptr__.Error();
		if (S_OK != hr__)
			return  hr__;
		hr__ = file__.Read((LPVOID)ptr__.GetData(), n__sz__);
		if (S_OK != hr__)
			return  hr__;

		const bool bIsUnicode = details::CsvFile_IsUnicodeData(ptr__);

		CAtlString cs_buffer;
		if (bIsUnicode)
			cs_buffer.Append(
				reinterpret_cast<TCHAR*>(ptr__.GetData()),
				n__sz__ / sizeof(TCHAR)
			);
		else
		{
			CAtlStringA cs_buffer_a(
					reinterpret_cast<char*>(ptr__.GetData()),
					n__sz__ / sizeof(char)
				);
			cs_buffer = cs_buffer_a;
		}
		cs_buffer.Replace(_T("\n\r"), _T(""));
		hr__ = this->Create(cs_buffer, bHasHeader);
	}
	else
		hr__ = S_FALSE;
	return hr__;
}

const CCsvFile::TRow&
           CCsvFile::Row(const INT row__) const
{
	if (row__ < 0 || row__ > (INT)m_table.size() - 1 || S_OK != m_result)
	{
		return details::CsvFile_InvalidCsvRow_Ref();
	}
	const CCsvFile::TRow& row_ref = m_table[row__];
	return row_ref;
}

LONG       CCsvFile::RowCount(void) const
{
	const LONG cnt__ = (LONG)m_table.size();
	return cnt__;
}

HRESULT    CCsvFile::Save(LPCTSTR pFile, const bool bHasHeader)
{
	m_src_path = pFile;

	::ATL::CAtlFile file__;
	HRESULT hr__ =  file__.Create(pFile, FILE_WRITE_DATA, FILE_WRITE_DATA, CREATE_ALWAYS);
	if (S_OK != hr__)
		return  hr__;
	hr__ = file__.Seek(0, FILE_BEGIN);
	if (S_OK != hr__)
		return  hr__;
	LPCTSTR pFieldDivider = details::CsvFile_GetSeparator(m_dwOptions);
	if (bHasHeader && m_has_header)
	{
		hr__ = details::CsvFile_WriteLine(file__, m_header, pFieldDivider);
		if (S_OK != hr__)
			return  hr__;
	}
	const LONG nRows = this->RowCount();
	for ( LONG i_ = 0; i_ < nRows; i_++)
	{
		const TRow& row_ref = m_table[i_];
		hr__ = details::CsvFile_WriteLine(file__, row_ref, pFieldDivider);
		if (S_OK != hr__)
			return  hr__;
	}
	hr__ = file__.Flush();
	return hr__;
}

LPCTSTR    CCsvFile::SourceFilePath(void)const
{
	return m_src_path;
}

DWORD      CCsvFile::SourceFileSize(void)const
{
	return m_src_size;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CCsvFile::CreateFileFromHeader(LPCTSTR pFile, const THeader& header_ref, const DWORD dwOptions)
{
	::ATL::CAtlFile file__;
	HRESULT hr__ =  file__.Create(pFile, FILE_WRITE_DATA, FILE_WRITE_DATA, CREATE_ALWAYS);
	if (S_OK != hr__)
		return  hr__;
	hr__ = file__.Seek(0, FILE_BEGIN);
	if (S_OK != hr__)
		return  hr__;
	LPCTSTR pFieldDivider = details::CsvFile_GetSeparator(dwOptions);
	hr__ = details::CsvFile_WriteLine(file__, header_ref, pFieldDivider);
	return hr__;
}

bool       CCsvFile::IsFileExist(LPCTSTR pFile)
{
	::ATL::CAtlFile file__;
	HRESULT hr__ =  file__.Create(pFile, FILE_READ_DATA, FILE_WRITE_DATA, OPEN_EXISTING);
	return (hr__ == S_OK);
}

/////////////////////////////////////////////////////////////////////////////

CCsvRecordSpecBase::CCsvRecordSpecBase(void)
{
	m_range = ::std::make_pair(0, 0);
}

/////////////////////////////////////////////////////////////////////////////

CCsvFile::THeader CCsvRecordSpecBase::CreateHeader(void)const
{
	CCsvFile::THeader header_;

	const INT count_ = this->FieldCount();
	for ( INT i_ = 0; i_ < count_; i_++ )
	{
		try
		{
			header_.push_back(this->FieldNameOf(i_));
		}
		catch(::std::bad_alloc&)
		{
			break;
		}
	}
	return header_;
}

INT               CCsvRecordSpecBase::FieldCount(void)const
{
	return static_cast<INT>(m_fields.size());
}

CAtlString        CCsvRecordSpecBase::FieldNameOf(const INT nIndex)const
{
	if (nIndex < 0 || nIndex > this->FieldCount() - 1)
		return CAtlString();
	else
		return m_fields[nIndex];
}

INT               CCsvRecordSpecBase::GetHeaderLen(void)const
{
	INT len_ = 0;
	const INT count_ = this->FieldCount();
	for ( INT i_ = 0; i_ < count_; i_++ )
	{
		len_ += m_fields[i_].GetLength();
	}
	len_ += sizeof(TCHAR) * (count_ - 1); // takes into account field separator
	return len_;
}

HRESULT           CCsvRecordSpecBase::ValidateData(const CCsvFile& _csv, const bool bShowError)
{
	const DWORD dwHeaderSize = this->GetHeaderLen();
	const DWORD dwFieldCount = static_cast<DWORD>(_csv.FieldCount());

	bool bSuccess = true;
	// (1) checks the acceptable range of header field count
	if (dwFieldCount < m_range.first || dwFieldCount > m_range.second)
		bSuccess = false;
	else
	{
		if (_csv.RowCount() < 1                        // (2) checks row count;
		    && (_csv.SourceFileSize() > dwHeaderSize)) // (3) checks data that exceeds the header size
			bSuccess = false;
	}

	if (!bSuccess)
	{
		if (bShowError)
		{
			CAtlString cs_err;
			cs_err.Format(
					_T("Data file:\n%s\nhas invalid data or incorrect code page."),
					_csv.SourceFilePath()
				);
			::MessageBox(
					NULL,
					cs_err.GetString(),
					GetAppObjectRef().GetName(),
					MB_ICONSTOP|MB_OK|MB_SETFOREGROUND|MB_TOPMOST
				);
		}
		return HRESULT_FROM_WIN32(ERROR_INVALID_DATA);
	}
	else
		return S_OK;
}