/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Jul-2018 at 9:30:37p, UTC+7, Phuket, Rawai, Wednesday;
	This is shared lite generic OLE data property interface implementation file.
*/
#include "StdAfx.h"
#include "generic.stg.ole.prop.h"

using namespace shared::stg::ole;

/////////////////////////////////////////////////////////////////////////////

GUID     COlePropType::TypeToFormatId(const _e _type) {

	switch (_type) {
	case COlePropType::eAudioInfo: return FMTID_AudioSummaryInformation;
	case COlePropType::eCustom   : return FMTID_UserDefinedProperties  ;
	case COlePropType::eDiscarded: return FMTID_DiscardableInformation ;
	case COlePropType::eDocument : return FMTID_DocSummaryInformation  ;
	case COlePropType::eImageInfo: return FMTID_ImageSummaryInformation;
	case COlePropType::eMediaInfo: return FMTID_MediaFileSummaryInformation;
	case COlePropType::eVideoInfo: return FMTID_VideoSummaryInformation;
	default:
		return FMTID_SummaryInformation;
	}
}

/////////////////////////////////////////////////////////////////////////////

COleProperty::COleProperty(void) { m_value.vt = VT_EMPTY; m_p_id = m_value.vt; }
COleProperty::~COleProperty(void) {}

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////

COlePropertySet::COlePropertySet(void) : COleBase(_T("COlePropertySet")), m_dwFlags(COleAccLevel::eRead_Ex) {}
COlePropertySet::~COlePropertySet(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT   COlePropertySet::Enumerate(t_ole_prop_stg& _stg, const GUID& _fmt_id) {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!_stg)
		return (m_error = E_POINTER);

	HRESULT hr_ = S_OK;
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}

HRESULT   COlePropertySet::Enumerate(t_ole_stg_ptr& _stg) {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!_stg) {
		m_error.SetState(
			E_POINTER, _T("OLE storage is not provided.")
		);
		return m_error;
	}

	t_ole_prop_set_stg set_stg;
	HRESULT hr_ = _stg->QueryInterface(IID_IPropertySetStorage, reinterpret_cast<void**>(&set_stg));
	if (FAILED(hr_))
		return (m_error = hr_);

	CComPtr<IEnumSTATPROPSETSTG> pEnum;

	hr_ = set_stg->Enum(&pEnum);
	if (FAILED(hr_))
		return (m_error = hr_);

	STATPROPSETSTG stats_stg = {0};
	hr_ = pEnum->Next( 1, &stats_stg, NULL );

	while (SUCCEEDED(hr_) && S_FALSE != hr_) {

		t_ole_prop_stg props_;

		hr_ = set_stg->Open(
			stats_stg.fmtid, m_dwFlags, &props_
		);
		if (FAILED(hr_))
			break;

		hr_ = this->Enumerate(props_, stats_stg.fmtid);
		if (SUCCEEDED(hr_))
		hr_ = pEnum->Next( 1, &stats_stg, NULL );
	}

	if (FAILED(hr_))
		m_error = hr_;
	else
	if (hr_ == S_FALSE)
		hr_ =  S_OK;

	return m_error;
}