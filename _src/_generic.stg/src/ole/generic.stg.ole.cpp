/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Jul-2018 at 5:34:39p, UTC+7, Phuket, Rawai, Tuesday;
	This is shared lite generic OLE data provider interface implementation file.
*/
#include "StdAfx.h"
#include "generic.stg.ole.h"

using namespace shared::stg::ole;

/////////////////////////////////////////////////////////////////////////////

FMTID   COleStgType::StgTypeToId(const _e _type) {
	switch (_type) {
	case COleStgType::eStorage:
	case COleStgType::eOleDoc :
		{
			return IID_IStorage;
		} break;	
	}
	return IID_IPropertySetStorage;
}

/////////////////////////////////////////////////////////////////////////////

COleStorage::COleStorage(const DWORD _level) : TBase(_T("COleStorage")) { TBase::m_level = _level; }
COleStorage::~COleStorage(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT      COleStorage::Close (void) {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (this->IsOpen() == false)
		return (m_error = (DWORD)ERROR_INVALID_STATE);

	m_stg_ptr = NULL;

	return m_error;
}

bool         COleStorage::IsOpen(void) const { return !!m_stg_ptr; }
bool         COleStorage::IsRoot(void) const { return (0==m_level);}

HRESULT      COleStorage::Open (LPCTSTR lpszPath, const DWORD dwFlags) {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == lpszPath || 0 == ::_tcslen(lpszPath))
		return (m_error = E_INVALIDARG);

	if (this->IsOpen() == true)
		return (m_error = (DWORD)ERROR_ALREADY_INITIALIZED);

	HRESULT hr_ = ::StgOpenStorage(
			lpszPath, NULL, dwFlags, NULL, 0, &m_stg_ptr
		);
	if (FAILED(hr_))
		m_error = hr_;
	else {

		COlePropertySet prop_set;
		hr_ = prop_set.Enumerate(m_stg_ptr);
		if (SUCCEEDED(hr_)) {
			try {
				m_props.push_back(prop_set);
			}
			catch (::std::bad_alloc&) {
				hr_ = E_OUTOFMEMORY;
			}
		}

		this->Open(m_stg_ptr, dwFlags);
	}
	return m_error;
}

HRESULT      COleStorage::Open  (CComPtr<IStorage>& _p_stg, const DWORD dwFlags) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == _p_stg) {
		m_error.SetState(
			E_POINTER, _T("Parent storage object is not provided.")
		);
		return m_error;
	}

	if (m_stg_ptr == NULL)
		m_stg_ptr = _p_stg;

	if (m_stg_set.empty() == false)
		m_stg_set.clear();

	if (m_streams.empty() == false)
		m_streams.clear();

	STATSTG stat_stg = {0};
	HRESULT hr_ = _p_stg->Stat(&stat_stg, STATFLAG_DEFAULT);
	if (SUCCEEDED(hr_)) {
		TBase::m_stats = stat_stg;
	}

	CComPtr<IEnumSTATSTG> pEnum;
	hr_ = _p_stg->EnumElements(0, NULL, 0, &pEnum);
	if (FAILED(hr_))
		return (m_error = hr_);

	hr_ = pEnum->Next(1, &stat_stg, NULL);
	if (FAILED(hr_))
		return (m_error = hr_);

	while (SUCCEEDED(hr_) && (hr_ != S_FALSE)) {
		switch ( stat_stg.type ) {
		case STGTY_STORAGE:
			{
				CComPtr<IStorage> stg_child;
				hr_ = _p_stg->OpenStorage(
					stat_stg.pwcsName, 0, dwFlags, 0, 0, &stg_child
					);
				if (SUCCEEDED(hr_)) {
					COleStorage stg_ole(this->Level() + 1);
					try {
						hr_ = stg_ole.Open(stg_child, dwFlags);
						m_stg_set.push_back(stg_ole);
					}
					catch(::std::bad_alloc&) {
						hr_ = E_OUTOFMEMORY;
					}
				}

			} break;
		case STGTY_STREAM :
			{
				t_ole_stream p_stream;
				hr_ = _p_stg->OpenStream(
					stat_stg.pwcsName, NULL, dwFlags, NULL, &p_stream
					);
				if (SUCCEEDED(hr_)) {
					COleStream str_ole(this->Level() + 1);
					try {
						hr_ = str_ole.Open(p_stream, dwFlags);
						m_streams.push_back(str_ole);
					}
					catch(::std::bad_alloc&) {
						hr_ = E_OUTOFMEMORY;
					}
				}
			} break;
		case STGTY_PROPERTY :
			{
				COlePropertySet prop_set;
				hr_ = prop_set.Enumerate(_p_stg);
				if (SUCCEEDED(hr_)) {
					try {
						m_props.push_back(prop_set);
					}
					catch (::std::bad_alloc&) {
						hr_ = E_OUTOFMEMORY;
					}
				}
			} break;
		}
		::CoTaskMemFree(stat_stg.pwcsName);
		if (SUCCEEDED(hr_))
			hr_ = pEnum->Next(1, &stat_stg, NULL);
		if (FAILED(hr_))
			m_error = hr_;
	}

	return m_error;
}

HRESULT         COleStorage::OpenEx(LPCTSTR lpszPath, const DWORD dwFlags) {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == lpszPath || 0 == ::_tcslen(lpszPath))
		return (m_error = E_INVALIDARG);

	if (this->IsOpen() == true)
		return (m_error = (DWORD)ERROR_ALREADY_INITIALIZED);

	HRESULT hr_ = ::StgOpenStorageEx(
		lpszPath, dwFlags, STGFMT_STORAGE, 0, NULL, NULL, IID_IStorage, reinterpret_cast<void**>(&m_stg_ptr)
	);
	if (FAILED(hr_))
		m_error = hr_;
	else {

		COlePropertySet prop_set;
		hr_ = prop_set.Enumerate(m_stg_ptr);
		if (SUCCEEDED(hr_)) {
			try {
				m_props.push_back(prop_set);
			}
			catch (::std::bad_alloc&) {
				hr_ = E_OUTOFMEMORY;
			}
		}

		this->Open(m_stg_ptr, dwFlags);
	}
	return m_error;
}

const
t_ole_sub_stg&  COleStorage::Storages(void) const { return m_stg_set; }
const
t_ole_streams&  COleStorage::Streams (void) const { return m_streams; }