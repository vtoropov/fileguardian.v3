/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Jul-2018 at 7:04:48p, UTC+7, Phuket, Rawai, Wednesday;
	This is shared lite generic OLE data provider base interface implementation file.
*/
#include "StdAfx.h"
#include "generic.stg.ole.base.h"

using namespace shared::stg::ole;

#include "Shared_DateTime.h"

using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

COleType::_e COleType::DwordToEnum(const DWORD _val) {

	COleType::_e type_ = COleType::eStorage;

	switch (_val) {
	case STGTY_STORAGE  : type_ = COleType::eStorage;  break;
	case STGTY_STREAM   : type_ = COleType::eStream;   break;
	case STGTY_LOCKBYTES: type_ = COleType::eBinData;  break;
	case STGTY_PROPERTY : type_ = COleType::eProperty; break;
	}

	return type_;
}

/////////////////////////////////////////////////////////////////////////////

COleStats::COleStats(void) : m_type(COleType::eStorage), m_uuid(CLSID_NULL) {

	m_access = {0};
	m_create = {0};
	m_modify = {0};
	m_size   = {0};
}

COleStats::~COleStats(void) {}

/////////////////////////////////////////////////////////////////////////////

CLSID      COleStats::Class  (void)    const { return m_uuid;   }
FILETIME   COleStats::Created(void)    const { return m_create; }
FILETIME   COleStats::LastAccess(void) const { return m_access; }
FILETIME   COleStats::Modified  (void) const { return m_modify; }
LPCTSTR    COleStats::Name      (void) const { return m_name;   }
ULARGE_INTEGER
           COleStats::Size(void)       const { return m_size;   }
COleType::_e
           COleStats::Type(void)       const { return m_type;   }

/////////////////////////////////////////////////////////////////////////////

COleStats_Ex::COleStats_Ex(void) : TBase() {}
COleStats_Ex::~COleStats_Ex(void) {}

/////////////////////////////////////////////////////////////////////////////

CAtlString   COleStats_Ex::ClassAsText(void) const {

	CAtlString cs_guid;

	TCHAR sz_guid[_MAX_PATH] = {0};

	const INT n_copied = ::StringFromGUID2(TBase::m_uuid, sz_guid, _MAX_PATH);
	if (0 < n_copied) {
		cs_guid = sz_guid;
	}

	return cs_guid;
}

SYSTEMTIME   COleStats_Ex::Created    (void) const { SYSTEMTIME sys_tm = CSystemTime(m_create); return sys_tm; }
SYSTEMTIME   COleStats_Ex::LastAccess (void) const { SYSTEMTIME sys_tm = CSystemTime(m_access); return sys_tm; }
SYSTEMTIME   COleStats_Ex::Modified   (void) const { SYSTEMTIME sys_tm = CSystemTime(m_modify); return sys_tm; }

DOUBLE       COleStats_Ex::SizeInKb   (void) const { return m_size.QuadPart / 1000.0; }
DOUBLE       COleStats_Ex::SizeInMb   (void) const { return m_size.QuadPart / 1000000.0; }

CAtlString   COleStats_Ex::TypeAsText (void) const {

	CAtlString cs_type;

	switch (m_type) {
	case COleType::eBinData :  cs_type = _T("Binary");   break;
	case COleType::eProperty:  cs_type = _T("Property"); break;
	case COleType::eStorage :  cs_type = _T("Storage");  break;
	case COleType::eStream  :  cs_type = _T("Stream");   break;
	}

	return cs_type;
}

/////////////////////////////////////////////////////////////////////////////

COleStats_Ex& COleStats_Ex::operator=(const STATSTG& _stats) {

	if (_stats.pwcsName) {
		m_name = _stats.pwcsName;
		::CoTaskMemFree(_stats.pwcsName);
	}

	TBase::m_access = _stats.atime;
	TBase::m_create = _stats.ctime;
	TBase::m_modify = _stats.mtime;
	TBase::m_size   = _stats.cbSize;
	TBase::m_type   = COleType::DwordToEnum(_stats.type);
	TBase::m_uuid   = _stats.clsid;

	return *this;
}

/////////////////////////////////////////////////////////////////////////////

COleBase::COleBase(LPCTSTR lpszModule) : m_level(0) { m_error.Source(lpszModule);}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   COleBase::Error(void) const { return m_error; }
DWORD       COleBase::Level(void) const { return m_level; }
LPCTSTR     COleBase::Name (void) const { return m_name.GetString(); }
const
COleStats_Ex&
            COleBase::Stats(void) const { return m_stats; }