/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Jul-2018 at 9:59:10p, UTC+7, Phuket, Rawai, Wednesday;
	This is shared lite generic OLE data stream interface implementation file.
*/
#include "StdAfx.h"
#include "generic.stg.ole.stream.h"

using namespace shared::stg::ole;

/////////////////////////////////////////////////////////////////////////////

COleStream::COleStream(const DWORD dwLevel) : TBase(_T("COleStream")) { TBase::m_level = dwLevel; }
COleStream::~COleStream(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT     COleStream::Open  (t_ole_stream& _p_stg, const DWORD dwFlags) {

	m_error.Module(_T(__FUNCTION__)); dwFlags;
	m_error = S_OK;

	if (NULL == _p_stg) {
		m_error.SetState(
			E_POINTER, _T("Provided stream object is invalid.")
		);
		return m_error;
	}
	m_strm_ptr = _p_stg;

	STATSTG stat_stg = {0};
	HRESULT hr_ = _p_stg->Stat(&stat_stg, STATFLAG_DEFAULT);
	if (SUCCEEDED(hr_)) {
		TBase::m_stats = stat_stg;
	}

	return m_error;
}

HRESULT     COleStream::Read  (CRawData& _data) const {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (_data.IsValid() == false)
		return (m_error = _data.Error());

	if (m_strm_ptr == NULL)
		return (m_error = OLE_E_BLANK);

	ULONG l_req = 0;

	HRESULT hr_ = m_strm_ptr->Read(
			_data.GetData(), _data.GetSize(), &l_req
		);
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}