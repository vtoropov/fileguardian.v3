/*
	Created by Tech_dog (ebontrop@gmail.com) at 19-Mar-2014 at 12:12:15pm, GMT+4, Taganrog, Wednesday;
	This is shared lite library MS Windows private profile (INI) wrapper interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 12:33:54p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "generic.stg.profile.h"
#include "generic.stg.data.h"

using namespace shared::lite::data;
using namespace shared::lite::data::factories;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace data { namespace details
{
	const LPCTSTR Profile__ROW_TOKEN       = _T("\n");
	const LPCTSTR Profile__END_TOKEN       = _T("\r");
	const LPCTSTR Profile__EMPTY_DATA      = _T("n/a");
	const LPCTSTR Profile__COMMENT_TOKEN   = _T(";");
	const LPCTSTR Profile__SEC_BEGIN_TOKEN = _T("[");
	const LPCTSTR Profile__SEC_END_TOKEN   = _T("]");
	const LPCTSTR Profile__PAIR_DEV_TOKEN  = _T("=");

	using shared::lite::data::CPrivateProfileItem;
	using shared::lite::data::CPrivateProfileSection;

	static CPrivateProfileItem&     Profile_ItemInvalidObject_Ref(void)
	{
		static CPrivateProfileItem pair__na;
		return pair__na;
	}

	static CPrivateProfileSection&  Profile_SectionInvalidObject_Ref(void)
	{
		static CPrivateProfileSection sec__na;
		return sec__na;
	}

	static HRESULT  Profile_LoadDataFrom(LPCTSTR pFile, ::ATL::CAtlString& __in_out_ref)
	{
		::ATL::CAtlFile file__;
		HRESULT hr__ = file__.Create(pFile, FILE_READ_DATA, FILE_SHARE_READ, OPEN_EXISTING);
		if (S_OK != hr__)
			return  hr__;
		ULONGLONG size__ = 0;
		hr__ = file__.Seek(0, FILE_BEGIN);
		if (S_OK != hr__)
			return  hr__;
		hr__ = file__.GetSize(size__);
		if (S_OK != hr__)
			return  hr__;
		if (size__ & 0x0000ffff)
		{
			const INT n__sz__  = (INT)size__ + 1;
			shared::lite::data::CRawData ptr__(n__sz__);
			hr__ = ptr__.Error();
			if (S_OK != hr__)
				return  hr__;
			hr__ = file__.Read((LPVOID)ptr__.GetData(), n__sz__ - 1);
			if (S_OK != hr__)
				return  hr__;
			__in_out_ref = (reinterpret_cast<char*>(ptr__.GetData()));
		}
		else
			hr__ = S_FALSE;
		return  hr__;
	}

	static bool  Profile_IsCommentLine(const ::ATL::CAtlString& ln__ref)
	{
		return (ln__ref.GetLength() > 0 && ln__ref.Find(Profile__COMMENT_TOKEN) == 0);
	}

	static bool  Profile_IsSection(const ::ATL::CAtlString& ln__ref, ::ATL::CAtlString& sec__ref, bool& err__ref)
	{
		err__ref = false;
		if (ln__ref.GetLength() > 0 && ln__ref.Find(Profile__SEC_BEGIN_TOKEN) == 0)
		{
			const INT n__close_bracket = ln__ref.Find(Profile__SEC_END_TOKEN);
			if (n__close_bracket == -1)
				return false;
			sec__ref = ln__ref.Left(n__close_bracket);
			sec__ref.Replace(Profile__SEC_BEGIN_TOKEN, _T(""));
			return true;
		}
		else
			return false;
	}

	static bool  Profile_LineToItem(const ::ATL::CAtlString& ln__ref, CPrivateProfileItem& pair__ref, LPCTSTR prefix__ = NULL)
	{
		const INT n__div__pos__ = ln__ref.Find(Profile__PAIR_DEV_TOKEN);
		if (n__div__pos__ < 0)
			return false;
		if (prefix__)
		{
			pair__ref.Name() = prefix__;
			pair__ref.Name()+= ln__ref.Left(n__div__pos__);
		}
		else
			pair__ref.Name() = ln__ref.Left(n__div__pos__);
		if (pair__ref.Name().IsEmpty() == false)
			pair__ref.Name().Trim();
		pair__ref.Value() = ln__ref.Right(ln__ref.GetLength() - n__div__pos__ - 1);
		if (pair__ref.Value().IsEmpty()== false)
			pair__ref.Value().Trim();
		const INT n__comment__pos__ = pair__ref.Value().Find(Profile__COMMENT_TOKEN);
		if (n__comment__pos__ > -1)
		{
			pair__ref.Comment() = pair__ref.Value().Right(pair__ref.Value().GetLength() - n__comment__pos__ - 1);
			pair__ref.Value() = pair__ref.Value().Left(n__comment__pos__);
			if (pair__ref.Value().IsEmpty()== false)
				pair__ref.Value().Trim();
		}
		return true;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CPrivateProfileItem::CPrivateProfileItem(void)
{
}

CPrivateProfileItem::CPrivateProfileItem(LPCTSTR pName, LPCTSTR pValue, LPCTSTR pComment) : 
	m_name(pName),
	m_value(pValue),
	m_comment(pComment)
{
}

CPrivateProfileItem::~CPrivateProfileItem(void)
{
}

/////////////////////////////////////////////////////////////////////////////

const CAtlString&     CPrivateProfileItem::Comment(void) const
{
	return m_comment;
}

::ATL::CAtlString&    CPrivateProfileItem::Comment(void)
{
	return m_comment;
}

bool                  CPrivateProfileItem::IsValid(void) const
{
	return !m_name.IsEmpty();
}

const CAtlString&     CPrivateProfileItem::Name(void) const
{
	return m_name;
}

::ATL::CAtlString&    CPrivateProfileItem::Name(void)
{
	return m_name;
}

const CAtlString&     CPrivateProfileItem::Value(void) const
{
	return m_value;
}

::ATL::CAtlString&    CPrivateProfileItem::Value(void)
{
	return m_value;
}

/////////////////////////////////////////////////////////////////////////////

CPrivateProfileItem::CPrivateProfileItem(const CPrivateProfileItem& rh__ref)
{
	this->m_comment = rh__ref.m_comment;
	this->m_name    = rh__ref.m_name;
	this->m_value   = rh__ref.m_value;
}

CPrivateProfileItem& CPrivateProfileItem::operator= (const CPrivateProfileItem& rh__ref)
{
	this->m_comment = rh__ref.m_comment;
	this->m_name    = rh__ref.m_name;
	this->m_value   = rh__ref.m_value;
	return (*this);
}

/////////////////////////////////////////////////////////////////////////////

CPrivateProfileSection::CPrivateProfileSection(void)
{
}

CPrivateProfileSection::CPrivateProfileSection(LPCTSTR pName) : m_name(pName)
{
}

CPrivateProfileSection::~CPrivateProfileSection(void)
{
	if (!m_index.empty()) m_index.clear();
	if (!m_items.empty()) m_items.clear();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT               CPrivateProfileSection::AddItem(LPCTSTR pszName)
{
	if (!pszName || !::_tcslen(pszName))
		return E_INVALIDARG;

	const CPrivateProfileItem& item_ = this->ItemOf(pszName);
	if (item_.IsValid())
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);

	try
	{
		CPrivateProfileItem new_item;
		new_item.Name() = pszName;
		const INT nIndex = this->ItemCount();
		m_items.push_back(new_item);
		m_index.insert(
			::std::make_pair(CAtlString(pszName), nIndex)
			);
	}
	catch(::std::bad_alloc&)
	{
		return E_OUTOFMEMORY;
	}
	HRESULT hr_ = S_OK;
	return  hr_;
}

bool                  CPrivateProfileSection::IsEmpty(void) const
{
	return m_items.empty();
}

bool                  CPrivateProfileSection::IsValid(void) const
{
	return !m_name.IsEmpty();
}

INT                   CPrivateProfileSection::ItemCount(void) const
{
	return (INT)m_items.size();
}

const
CPrivateProfileItem&  CPrivateProfileSection::ItemOf(const INT nIndex) const
{
	if (0 > nIndex || nIndex > ItemCount() - 1)
		return details::Profile_ItemInvalidObject_Ref();
	else
		return m_items[nIndex];
}

CPrivateProfileItem&  CPrivateProfileSection::ItemOf(const INT nIndex)
{
	if (0 > nIndex || nIndex > ItemCount() - 1)
		return details::Profile_ItemInvalidObject_Ref();
	else
		return m_items[nIndex];
}

const
CPrivateProfileItem&  CPrivateProfileSection::ItemOf(LPCTSTR pName) const
{
	TItemsIndex::const_iterator it__ = m_index.find(pName);
	if (it__ == m_index.end())
		return details::Profile_ItemInvalidObject_Ref();
	const INT nIndex = it__->second;
	return this->ItemOf(nIndex);
}

CPrivateProfileItem&  CPrivateProfileSection::ItemOf(LPCTSTR pName)
{
	TItemsIndex::const_iterator it__ = m_index.find(pName);
	if (it__ == m_index.end())
		return details::Profile_ItemInvalidObject_Ref();
	const INT nIndex = it__->second;
	return this->ItemOf(nIndex);
}

const CAtlString&     CPrivateProfileSection::Name(void) const
{
	return m_name;
}

::ATL::CAtlString&    CPrivateProfileSection::Name(void)
{
	return m_name;
}

/////////////////////////////////////////////////////////////////////////////

CPrivateProfileSection::CPrivateProfileSection(const CPrivateProfileSection& rh__ref)
{
	this->m_index = rh__ref.m_index;
	this->m_name  = rh__ref.m_name;
	this->m_items = rh__ref.m_items;
}

CPrivateProfileSection& CPrivateProfileSection::operator= (const CPrivateProfileSection& rh__ref)
{
	this->m_index = rh__ref.m_index;
	this->m_name  = rh__ref.m_name;
	this->m_items = rh__ref.m_items;
	return (*this);
}

/////////////////////////////////////////////////////////////////////////////

CPrivateProfile::CPrivateProfile(void) : m_result(OLE_E_BLANK), m_store_mode(CPrivateProfile::SM_UseRegular)
{
}

CPrivateProfile::CPrivateProfile(LPCTSTR pFile) : m_result(OLE_E_BLANK)
{
	this->Create(pFile);
}

CPrivateProfile::~CPrivateProfile(void)
{
	if (!m_index.empty()) m_index.clear();
	if (!m_sections.empty()) m_sections.clear();
	m_result = OLE_E_BLANK;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT               CPrivateProfile::AddSection(LPCTSTR pszSection)
{
	if (!pszSection || !::_tcslen(pszSection))
		return E_INVALIDARG;

	const CPrivateProfileSection& sec_ = this->SectionOf(pszSection);
	if (sec_.IsValid())
		return HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS);
	try
	{
		CPrivateProfileSection new_sec;
		new_sec.Name() = pszSection;

		const INT count_ = this->SectionCount();
		m_sections.push_back(new_sec);
		m_index.insert(
				::std::make_pair(CAtlString(pszSection), count_)
			);
	}
	catch(::std::bad_alloc&)
	{
		return E_OUTOFMEMORY;
	}
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT               CPrivateProfile::Create(LPCTSTR pFile)
{
	if (!m_index.empty()) m_index.clear();
	if (!m_sections.empty()) m_sections.clear();

	m_result     = S_OK;
	m_store_mode = CPrivateProfile::SM_UseRegular;
	m_store_path = pFile;

	::ATL::CAtlString buffer__;
	m_result = details::Profile_LoadDataFrom(pFile, buffer__);
	if (S_OK != m_result)
		return  m_result;
	
	return CreateFromText(buffer__.GetString());
}

HRESULT               CPrivateProfile::CreateFromText(const CAtlString& _buffer)
{
	m_result  = S_OK;
	try
	{
		bool err__ = false;
		INT line__ = NULL;
		::ATL::CAtlString line__token__;
		::ATL::CAtlString section__;
		CPrivateProfileSection sec__obj__;
		CPrivateProfileSectionFactory sec__fty__(sec__obj__);
		line__token__ = _buffer.Tokenize(details::Profile__ROW_TOKEN, line__);
		while (!line__token__.IsEmpty())
		{
			line__token__.Trim();
			if (!details::Profile_IsCommentLine(line__token__))
			{
				if (details::Profile_IsSection(line__token__, section__, err__) && !err__)
				{
					if (sec__obj__.IsValid()) // previous section is valid, we need to add the section to the collection and to re-create it
					{
						m_sections.push_back(sec__obj__);
						m_index.insert(::std::make_pair(sec__obj__.Name(), SectionCount() - 1));
					}
					sec__fty__.Clear();
					sec__obj__.Name() = section__.Trim();
				}
				else if (!err__ && sec__obj__.IsValid())
				{
					CPrivateProfileItem pair__obj__;
					if (details::Profile_LineToItem(line__token__, pair__obj__))
					{
						sec__obj__.m_items.push_back(pair__obj__);
						::std::pair<::ATL::CAtlString, INT> p__ =::std::make_pair(pair__obj__.Name(), sec__obj__.ItemCount() - 1);
						sec__obj__.m_index.insert(p__);
					}
				}
			}
			line__token__ = _buffer.Tokenize(details::Profile__ROW_TOKEN, line__);
		}
		if (sec__obj__.IsValid()) // inserts the last valid section
		{
			m_sections.push_back(sec__obj__);
			m_index.insert(::std::make_pair(sec__obj__.Name(), SectionCount() - 1));
		}
	}
	catch(::std::bad_alloc&)
	{
		ATLASSERT(FALSE);
		m_result = E_OUTOFMEMORY;
	}
	return m_result;
}

HRESULT               CPrivateProfile::CreateGlobal(LPCTSTR pFile, LPCTSTR p__key__separator)
{
	if (!m_index.empty()) m_index.clear();
	if (!m_sections.empty()) m_sections.clear();
	m_result = OLE_E_BLANK;
	m_store_mode = CPrivateProfile::SM_UseGlobalIndex;

	::ATL::CAtlString buffer__;
	m_result = details::Profile_LoadDataFrom(pFile, buffer__);
	if (S_OK != m_result)
		return  m_result;
	CPrivateProfileSection sec__obj__(_T("GlobalIndex"));
	if (sec__obj__.IsValid())
	{
		m_sections.push_back(sec__obj__);
		m_index.insert(::std::make_pair(sec__obj__.Name(), SectionCount() - 1));
	}
	else
		return (m_result = E_OUTOFMEMORY);
	CPrivateProfileSection& sec__obj__ref = SectionOf(0);
	if (!sec__obj__ref.IsValid())
		return (m_result = E_OUTOFMEMORY);
	try
	{
		bool err__ = false;
		INT line__ = NULL;
		::ATL::CAtlString line__token__;
		::ATL::CAtlString prefix__;
		line__token__ = buffer__.Tokenize(details::Profile__ROW_TOKEN, line__);
		while (!line__token__.IsEmpty())
		{
			line__token__.Trim();
			if (!details::Profile_IsCommentLine(line__token__))
			{
				if (details::Profile_IsSection(line__token__, prefix__, err__) && !err__)
				{
					if (p__key__separator) prefix__ += p__key__separator;
				}
				else
				{
					CPrivateProfileItem pair__obj__;
					if (details::Profile_LineToItem(line__token__, pair__obj__, prefix__.GetString()))
					{
						sec__obj__ref.m_items.push_back(pair__obj__);
						::std::pair<::ATL::CAtlString, INT> p__ =::std::make_pair(pair__obj__.Name(), sec__obj__ref.ItemCount() - 1);
						sec__obj__ref.m_index.insert(p__);
					}
				}
			}
			line__token__ = buffer__.Tokenize(details::Profile__ROW_TOKEN, line__);
		}
	}
	catch(::std::bad_alloc&)
	{
		ATLASSERT(FALSE);
		m_result = E_OUTOFMEMORY;
	}
	return m_result;
}

bool                  CPrivateProfile::GetBoolValue(LPCTSTR pszSection, LPCTSTR pszItem, const bool bDefaultValue)const
{
	bool bResult = bDefaultValue;
	if (this->IsEmpty())
		return bResult;

	const CPrivateProfileSection& sec_ref = this->SectionOf(pszSection);
	if (sec_ref.IsValid())
	{
		const CPrivateProfileItem& item_ref = sec_ref.ItemOf(pszItem);
		if (item_ref.IsValid())
		{
			::ATL::CAtlString cs_flag = item_ref.Value();
			if (!cs_flag.IsEmpty())
			{
				const INT n_pos = cs_flag.CompareNoCase(_T("TRUE")); 
				bResult = (0 == n_pos);
			}
		}
	}
	return bResult;
}

DWORD                 CPrivateProfile::GetDwordValue(LPCTSTR pszSection, LPCTSTR pszItem, const DWORD dwDefaultValue)const
{
	DWORD dwValue = dwDefaultValue;
	::ATL::CAtlString cs_value = this->GetStringValue(pszSection, pszItem, NULL);
	if (!cs_value.IsEmpty())
		dwValue = static_cast<DWORD>(::_tstol(cs_value.GetString()));
	return dwValue;
}

HRESULT               CPrivateProfile::GetLastResult(void) const
{
	return m_result;
}

CPrivateProfile::eStorageMode
                      CPrivateProfile::GetStorageMode(void) const
{
	return m_store_mode;
}

CAtlString            CPrivateProfile::GetStringValue(LPCTSTR pszSection, LPCTSTR pszItem, LPCTSTR pszDefaultValue)const
{
	CAtlString cs_result = pszDefaultValue;
	if (this->IsEmpty())
		return  cs_result;

	const CPrivateProfileSection& sec_ref = this->SectionOf(pszSection);
	if (sec_ref.IsValid())
	{
		const CPrivateProfileItem& item_ref = sec_ref.ItemOf(pszItem);
		if (item_ref.IsValid())
		{
			cs_result = item_ref.Value();
			if (!cs_result.IsEmpty())cs_result.Trim();
		}
	}
	return cs_result;
}

bool                  CPrivateProfile::IsEmpty(void) const
{
	return m_sections.empty();
}

bool                  CPrivateProfile::IsValid(void) const
{
	return (S_OK == this->GetLastResult());
}

INT                   CPrivateProfile::SectionCount(void) const
{
	return (INT)m_sections.size();
}

const CPrivateProfileSection&
                      CPrivateProfile::SectionOf(const INT nIndex)const
{
	if (0 > nIndex || nIndex > SectionCount() - 1)
		return details::Profile_SectionInvalidObject_Ref();
	else
		return m_sections[nIndex];
}

CPrivateProfileSection&
                      CPrivateProfile::SectionOf(const INT nIndex)
{
	if (0 > nIndex || nIndex > SectionCount() - 1)
		return details::Profile_SectionInvalidObject_Ref();
	else
		return m_sections[nIndex];
}

const CPrivateProfileSection&
                      CPrivateProfile::SectionOf(LPCTSTR pName)const
{
	TSectionIndex::const_iterator it__ = m_index.find(pName);
	if (it__ == m_index.end())
		return details::Profile_SectionInvalidObject_Ref();
	const INT nIndex = it__->second;
	return this->SectionOf(nIndex);
}

CPrivateProfileSection&
                      CPrivateProfile::SectionOf(LPCTSTR pName)
{
	TSectionIndex::const_iterator it__ = m_index.find(pName);
	if (it__ == m_index.end())
		return details::Profile_SectionInvalidObject_Ref();
	const INT nIndex = it__->second;
	return this->SectionOf(nIndex);
}

HRESULT               CPrivateProfile::SetStringValue(LPCTSTR pszSection, LPCTSTR pszItem, LPCTSTR pszValue)
{
	if (!this->IsValid())
		return OLE_E_BLANK;

	this->AddSection(pszSection);

	CPrivateProfileSection& sec_ = this->SectionOf(pszSection);
	if (sec_.IsValid())
		return E_OUTOFMEMORY;

	sec_.AddItem(pszItem);

	CPrivateProfileItem& item_ = sec_.ItemOf(pszItem);
	if (!item_.IsValid())
		return E_OUTOFMEMORY;

	item_.Value() = pszValue;

	const BOOL bResult = ::WritePrivateProfileString(
							pszSection,
							pszItem,
							pszValue,
							m_store_path
						);

	HRESULT hr_ = (bResult ? S_OK : HRESULT_FROM_WIN32(::GetLastError()));
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CPrivateProfileSectionFactory::CPrivateProfileSectionFactory(CPrivateProfileSection& sec__ref) : m__sec__ref(sec__ref)
{
}

CPrivateProfileSectionFactory::~CPrivateProfileSectionFactory(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT               CPrivateProfileSectionFactory::Clear(void)
{
	if (m__sec__ref.IsEmpty())
		return S_FALSE;
	if (!m__sec__ref.m_items.empty()) m__sec__ref.m_items.clear();
	if (!m__sec__ref.m_index.empty()) m__sec__ref.m_index.clear();
	m__sec__ref.m_name.Empty();
	return S_OK;
}