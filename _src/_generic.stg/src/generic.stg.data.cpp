/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 8:55:09am, GMT+3, Taganrog, Wednesday;
	This is Shared Lite Library raw data class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 12:00:40p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "generic.stg.data.h"
#include "Shared_GenericHandle.h"

using namespace shared::lite::common;
using namespace shared::lite::data;

#define ERR_SRC_NAME _T("CRawData")
/////////////////////////////////////////////////////////////////////////////

CRawBuffer::CRawBuffer(void):
	m_pData(NULL),
	m_dwSize(0)
{
}

/////////////////////////////////////////////////////////////////////////////

CRawData::CRawData(void)
{
	m_error.Reset();
	m_error.Source(ERR_SRC_NAME);
}

CRawData::CRawData(const _variant_t& _data)
{
	m_error.Source(ERR_SRC_NAME);
	if (_data.vt == VT_BSTR)
	{
		this->Create(
				reinterpret_cast<PBYTE>(_data.bstrVal),
				static_cast<DWORD>(::SysStringByteLen(_data.bstrVal))
			);
	}
}

CRawData::CRawData(const DWORD dwSize)
{
	m_error.Source(ERR_SRC_NAME);
	this->Create(dwSize);
}

CRawData::CRawData(const PBYTE pData, const DWORD dwSize)
{
	m_error.Source(ERR_SRC_NAME);
	this->Create(pData, dwSize);
}

CRawData::~CRawData(void)
{
	this->Reset();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CRawData::Append (const CRawData& _raw_data)
{
	return this->Append(
				_raw_data.GetData(),
				_raw_data.GetSize()
			);
}

HRESULT     CRawData::Append(const PBYTE pData, const DWORD dwSize)
{
	if (!pData || !dwSize)
		return E_INVALIDARG;
	if (m_error)
		m_error = S_OK;

	PBYTE pCurrentLocator = NULL;

	if (this->IsValid())
	{
		const DWORD dwNewSize = m_dwSize + dwSize;
		pCurrentLocator = reinterpret_cast<PBYTE>(
					::HeapReAlloc(::GetProcessHeap(), HEAP_ZERO_MEMORY, m_pData, dwNewSize)
				);
		if (pCurrentLocator)
		{
			m_pData = pCurrentLocator;
			pCurrentLocator += m_dwSize;
			m_dwSize = dwNewSize;
		}
		else
			m_error = E_OUTOFMEMORY;
	}
	else
	{
		m_dwSize = dwSize;
		m_pData = reinterpret_cast<PBYTE>(
					::HeapAlloc(::GetProcessHeap(), HEAP_ZERO_MEMORY, dwSize)
				);
		if (m_pData)
			pCurrentLocator = m_pData;
		else
			m_error = E_OUTOFMEMORY;
	}
	if (!m_error)
	{
		const errno_t err_ = ::memcpy_s(pCurrentLocator, dwSize, pData, dwSize);
		if (err_)
			m_error = E_OUTOFMEMORY;
	}
	return m_error;
}

HRESULT     CRawData::CopyToVariantAsArray(_variant_t& v_data)const
{
	if (!this->IsValid())
		return (m_error = OLE_E_BLANK);

	if (VT_EMPTY != v_data.vt)v_data.Clear();

	v_data.vt = VT_ARRAY|VT_UI1;
	v_data.parray = NULL;

	SAFEARRAYBOUND bound[1] ={0};
	bound[0].lLbound        = 0;
	bound[0].cElements      = m_dwSize;

	v_data.parray = ::SafeArrayCreate(VT_UI1, 1, bound);
	if (!v_data.parray)
		return (m_error = E_OUTOFMEMORY);

	UCHAR HUGEP* pData = NULL;
	m_error = ::SafeArrayAccessData(v_data.parray, (void HUGEP**)&pData);
	if (!m_error)
	{
		const errno_t err_no = ::memcpy_s((void*)pData, m_dwSize, (const void*)v_data.parray, m_dwSize);
		if (err_no)
			m_error = E_OUTOFMEMORY;
		::SafeArrayUnaccessData(v_data.parray);
		pData = NULL;
	}

	if (m_error)
	{
		::SafeArrayDestroy(v_data.parray);
		v_data.parray = NULL;
		v_data.vt = VT_EMPTY;
	}

	return m_error;
}

HRESULT     CRawData::CopyToVariantAsUtf16(_variant_t& v_data)const
{
	if (!this->IsValid())
		return (m_error = OLE_E_BLANK);

	if (VT_EMPTY != v_data.vt)v_data.Clear();

	USES_CONVERSION;
	v_data.Clear();
	v_data.vt = VT_BSTR;
	v_data.bstrVal = ::SysAllocStringLen(
				::A2BSTR(reinterpret_cast<LPCSTR>(m_pData)),
				static_cast<UINT>(m_dwSize)
			);
	return m_error;
}

HRESULT     CRawData::CopyToVariantAsUtf8 (_variant_t& v_data)const
{
	if (!this->IsValid())
		return (m_error = OLE_E_BLANK);

	if (VT_EMPTY != v_data.vt)v_data.Clear();

	v_data.vt = VT_BSTR;
	v_data.bstrVal = ::SysAllocStringByteLen((LPCSTR)m_pData, m_dwSize);

	if (v_data.bstrVal)
		m_error = S_OK;
	else
	{
		m_error = E_OUTOFMEMORY;
		v_data.vt = VT_EMPTY;
	}
	return m_error;
}

HRESULT     CRawData::Create(const DWORD dwSize)
{
	if (this->IsValid())
		this->Reset();
	if (dwSize)
	{
		m_pData = reinterpret_cast<PBYTE>(
					::HeapAlloc(::GetProcessHeap(), HEAP_ZERO_MEMORY, dwSize)
				);
		if (!m_pData)
			m_error = ::GetLastError();
		else
		{
			m_error.Clear();
			m_dwSize = dwSize;
		}
	}
	return m_error;
}

HRESULT     CRawData::Create(const PBYTE pData, const DWORD dwSize)
{
	this->Reset();
	return this->Append(pData, dwSize);
}

TErrorRef   CRawData::Error(void)const
{
	return m_error;
}

const PBYTE CRawData::GetData(void) const
{
	return m_pData;
}

PBYTE       CRawData::GetData(void)
{
	return m_pData;
}

DWORD       CRawData::GetSize(void) const
{
	return m_dwSize;
}

bool        CRawData::IsValid(void) const
{
	return (NULL != m_pData && 0 != m_dwSize);
}

HRESULT     CRawData::Reset(void)
{
	m_dwSize = 0;
	if (m_pData)
	{
		if (!::HeapFree(::GetProcessHeap(), 0, m_pData))
			m_error = ::GetLastError();
		else
			m_error = S_OK;
		m_pData = NULL;
		if (m_error)
			return m_error;
	}
	m_error.Reset();
	return S_OK;
}

HRESULT     CRawData::ToStringUtf16(CAtlStringW& _utf16)const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsValid())
		return (m_error = OLE_E_BLANK);

	_utf16.Append(
			reinterpret_cast<LPCTSTR>(this->GetData()),
			static_cast<INT>(this->GetSize()/sizeof(TCHAR))
		);

	return m_error;
}

HRESULT     CRawData::ToStringUtf8 (CAtlStringA& _utf8)const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsValid())
		return (m_error = OLE_E_BLANK);

	_utf8.Append(
			reinterpret_cast<LPCSTR>(this->GetData()),
			static_cast<INT>(this->GetSize())
		);

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CRawData::operator const    PBYTE(void)const
{
	return m_pData;
}

CRawData::operator          PBYTE(void)
{
	return m_pData;
}

/////////////////////////////////////////////////////////////////////////////

CRawData::CRawData(const CRawData& raw_ref)
{
	*this = raw_ref;
}

CRawData& CRawData::operator+=(const CRawData& raw_ref)
{
	this->Append(
			raw_ref
		);
	return *this;
}

CRawData& CRawData::operator= (const CRawData& raw_ref)
{
	this->Create(raw_ref.GetData(), raw_ref.GetSize());
	return *this;
}

CRawData& CRawData::operator= (const _variant_t& _data)
{
	if (_data.vt == VT_BSTR)
	{
		this->Create(
				reinterpret_cast<PBYTE>(_data.bstrVal),
				static_cast<DWORD>(::SysStringByteLen(_data.bstrVal))
			);
	}
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CSafeArrayAccessor::CSafeArrayAccessor(const _variant_t& var_ref):
	m_sa_ptr(NULL),
	m_data_ptr(NULL)
{
	m_error.Source(_T("CSafeArrayAccessor"));
	if (!(VT_ARRAY & var_ref.vt) || !(VT_UI1 & var_ref.vt))
	{
		m_error = DISP_E_TYPEMISMATCH;
	}
	else if (!var_ref.parray)
	{
		m_error = E_INVALIDARG;
	}
	else
	{
		m_error = ::SafeArrayAccessData(var_ref.parray, (void HUGEP**)&m_data_ptr);
		if (!m_error)
			m_sa_ptr = var_ref.parray;
	}
}

CSafeArrayAccessor::CSafeArrayAccessor(SAFEARRAY* psa):
	m_sa_ptr(psa),
	m_data_ptr(NULL)
{
	m_error.Source(_T("CSafeArrayAccessor"));
	if (m_sa_ptr)
		m_error = ::SafeArrayAccessData(m_sa_ptr, (void HUGEP**)&m_data_ptr);
}

CSafeArrayAccessor::~CSafeArrayAccessor(void)
{
	if (!m_error)
	{
		::SafeArrayUnaccessData(m_sa_ptr);
		m_sa_ptr = NULL;
		m_error.Reset();
	}
	m_data_ptr = NULL;
}

/////////////////////////////////////////////////////////////////////////////

PBYTE     CSafeArrayAccessor::AccessData(void)
{
	return (PBYTE)m_data_ptr;
}

TErrorRef CSafeArrayAccessor::Error(void) const
{
	return m_error;
}

HRESULT   CSafeArrayAccessor::GetSize(LONG& n_size_ref) const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsValid())
		return (m_error = OLE_E_BLANK);

	HRESULT hr_ = S_OK;
	n_size_ref  = 0;
	{
		const UINT n_el_size = ::SafeArrayGetElemsize(m_sa_ptr);
		if (!n_el_size)
			return (m_error = (DWORD)ERROR_INVALID_DATA);

		LONG n_lbound = 0;
		hr_ = ::SafeArrayGetLBound(m_sa_ptr, 1, &n_lbound);
		if (FAILED(hr_))
			return (m_error = hr_);

		LONG n_ubound = 0;
		hr_ = ::SafeArrayGetUBound(m_sa_ptr, 1, &n_ubound);
		if (FAILED(hr_))
			return (m_error = hr_);

		n_size_ref = (n_ubound - n_lbound + 1) * (LONG)n_el_size;
	}
	return m_error;
}

bool      CSafeArrayAccessor::IsValid(void) const
{
	return (!m_error);
}