#ifndef _FGCRYPROVIDER_H_2BD73B2F_F5A7_4a4a_9FC4_A5827CA3A45F_INCLUDED
#define _FGCRYPROVIDER_H_2BD73B2F_F5A7_4a4a_9FC4_A5827CA3A45F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 5:57:31am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian generic shared library crypto data class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 7:27:13p, UTC+7, Phuket, Rawai, Monday;
*/
#include "Shared_SystemError.h"
#include "shared.crypto.base64.h"
#include "shared.crypto.mcrypt.h"

namespace fg { namespace common { namespace data
{
	using shared::crypto::CCryptoDataCrt;
	using shared::crypto::CCryptoDataProvider;
	using shared::lite::common::CSysError;

	class CCryptoDataProviderBase
	{
	protected:
		CSysError             m_error;
		CCryptoDataProvider   m_provider;
	public:
		CCryptoDataProviderBase(const CCryptoDataCrt&);
	public:
		HRESULT      Decrypt(const CAtlString& _encoded, CAtlString& _decoded);
		HRESULT      Encrypt(const CAtlString& _decoded, CAtlString& _encoded);
		TErrorRef    Error(void)const;
	};
}}}

#endif/*_FGCRYPROVIDER_H_2BD73B2F_F5A7_4a4a_9FC4_A5827CA3A45F_INCLUDED*/