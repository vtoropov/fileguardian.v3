#ifndef _FGSERVICELAUNCHER_H_CD6C001D_5712_4a55_9E98_56F425A940A0_INCLUDED
#define _FGSERVICELAUNCHER_H_CD6C001D_5712_4a55_9E98_56F425A940A0_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Jan-2016 at 11:31:37pm, GMT+7, Phuket, Rawai, Monday;
	This is File Watcher service launcher class declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_GenericSyncObject.h"
#include "Shared_SystemThreadPool.h"

namespace fg { namespace common
{
	using shared::lite::common::CSysError;
	using shared::lite::common::CSysErrorSafe;
	using shared::lite::sync::CGenericSyncObject;

	using shared::lite::sys_core::CThreadCrtData;
	using shared::lite::sys_core::CThreadState;


	class CLauncherKey
	{
	private:
		INT                  m_id;
		CAtlString           m_name;
	public:
		CLauncherKey(void);
		CLauncherKey(const INT _id);
	public:
		INT                  Identifier(void)const;
		VOID                 Identifier(const INT);
		CAtlString           Name(void)const;
		VOID                 Name(LPCTSTR);
	};

	class CLauncherKeyEnum
	{
		typedef ::std::vector<CLauncherKey> TLauncherKeys;
	private:
		static
		TLauncherKeys       m_keys;
	public:
		CLauncherKeyEnum(void);
	public:
		INT                 Count(void)const;
		INT                 Find (const UINT _vkey)const; // if not found, returns -1
		const
		CLauncherKey&       Item (const INT nIndex)const;
		CLauncherKey&       Item (const INT nIndex);
	};

	class CLauncherKeyModifier
	{
	public:
		enum _e{
			eKeyModifierNone =  -1,
			eKeyModifierCtrl = 0x0,
			eKeyModifierShft = 0x1,
			eKeyModifierAlt  = 0x2,
		};
	private:
		_e                  m_id;
		CAtlString          m_name;
	public:
		CLauncherKeyModifier(void);
		CLauncherKeyModifier(const CLauncherKeyModifier::_e);
	public:
		CLauncherKeyModifier::_e  Identifier(void)const;
		VOID                      Identifier(const CLauncherKeyModifier::_e);
		CAtlString                Name(void)const;
		VOID                      Name(LPCTSTR);
	};

	class CLauncherKeyModifierEnum
	{
		typedef ::std::vector<CLauncherKeyModifier> TKeyModifiers;
	private:
		static
		TKeyModifiers         m_mods;
	public:
		CLauncherKeyModifierEnum(void);
	public:
		INT                   Count(void)const;
		const
		CLauncherKeyModifier& Item (const INT nIndex)const;
		CLauncherKeyModifier& Item (const INT nIndex);
	};


	class CDesktopLauncher
	{
	private:
		CThreadCrtData      m_crt;
		CGenericSyncObject  m_sync_obj;
	private:
		volatile
		DWORD               m_state;
		CSysErrorSafe       m_error;
	public:
		CDesktopLauncher(void);
		~CDesktopLauncher(void);
	public:
		CSysError           Error(void)const;
		bool                IsRunning(void)const;
		HRESULT             Start(void);
		DWORD               State(void)const;
		HRESULT             Stop (void);
	public:
		HRESULT             RegisterHotKey(HWND _target, const UINT _evtId, const UINT _modifiers, const UINT _vkey);
		HRESULT             UnregisterHotKey(HWND _target, const UINT _evtId);
	private:
		VOID                LauncherWorkerThread(void);
	};
}}

#endif/*_FGSERVICELAUNCHER_H_CD6C001D_5712_4a55_9E98_56F425A940A0_INCLUDED*/