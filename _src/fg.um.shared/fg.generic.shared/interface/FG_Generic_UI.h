#ifndef _FGUISHAREDCFG_H_A6D0DBF2_482D_4bb9_8753_DE274E84DEE6_INCLUDED
#define _FGUISHAREDCFG_H_A6D0DBF2_482D_4bb9_8753_DE274E84DEE6_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Apr-2016 at 9:02:05pm, GMT+7, Phuket, Rawai, Thursday;
	This is File Guardian generic shared library UI shared settings class declaration file.
*/
#include "Shared_SystemError.h"

namespace fg { namespace common { namespace data
{
	using shared::lite::common::CSysError;

	class CHotKeys
	{
	private:
		UINT         m_modifiers;
		UINT         m_vkey;
	public:
		CHotKeys(void);
	public:
		UINT         Modifiers (void)const;
		VOID         Modifiers (const UINT);
		UINT         VirtualKey(void)const;
		VOID         VirtualKey(const UINT);
	};

	class CUISharedCfg
	{
	protected:
		CSysError    m_error;
		CHotKeys     m_keys;
	public:
		CUISharedCfg(void);
	public:
		TErrorRef    Error (void)const;
		const
		CHotKeys&    HotKeys(void)const;
		CHotKeys&    HotKeys(void);
	};

	class CUISharedCfgPersistent : public CUISharedCfg
	{
		typedef CUISharedCfg TBase;
	public:
		CUISharedCfgPersistent(void);
	public:
		HRESULT      Load(void);
		HRESULT      Save(void);
	};
}}}

#endif/*_FGUISHAREDCFG_H_A6D0DBF2_482D_4bb9_8753_DE274E84DEE6_INCLUDED*/