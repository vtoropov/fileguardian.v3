//
//	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 5:44:23a, GMT+7, Phuket, Rawai, Friday;
//	This is File Guardian application shared version info declaration file.
//

#define FG_VER_MAJOR       1
#define FG_VER_MINOR       5
#define FG_VER_SUBMINOR    0
#define FG_VER_BUGFIX      1

#define FG_VER_RLS_DATE    6/13/2018

//
//	Please synchronize the software version with FG_Generic_Defs.manufest file;
//
