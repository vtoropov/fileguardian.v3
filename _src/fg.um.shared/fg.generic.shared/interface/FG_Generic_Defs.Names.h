#ifndef _FGGENERICDEFSNAMES_H_E60F89ED_7D0F_4bb1_8D87_E0E9FF81497D_INCLUDED
#define _FGGENERICDEFSNAMES_H_E60F89ED_7D0F_4bb1_8D87_E0E9FF81497D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Feb-2018 at 4:44:43p, GMT+7, Phuket, Rawai, Thursday;
	This is File Guardian generic shared sofrware name definition declaration file.
*/

#define _FG_SERVICE_NAME_COMMON        _T("FG_Service")
#define _FG_DRIVER_NAME_COMMON         _T("FG_FsFilter")
#define _FG_DESKTOP_NAME_COMMON        _T("FG_Desktop")
#define _FG_EVTSPY_NAME_COMMON         _T("FG_FsEvtSpy")


#endif/*_FGGENERICDEFSNAMES_H_E60F89ED_7D0F_4bb1_8D87_E0E9FF81497D_INCLUDED*/

