#ifndef _FGCOMMONDEFS_H_AE1D4539_8DA4_4b69_B114_F653D9B0D394_INCLUDED
#define _FGCOMMONDEFS_H_AE1D4539_8DA4_4b69_B114_F653D9B0D394_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Feb-2016 at 12:50:51am, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian generic shared definitions declaration file.
*/

#define _FW_BROADCAST_MESSAGE_CLOSE    _T("fw_desktop_close")
#define _FW_GLOBAL_MUTEX_DESKTOP       _T("Global\\file_watcher_sft")
#define _FW_GLOBAL_MUTEX_AGENT         _T("Global\\file_agent_sft")

#define _FW_BROADCAST_MESSAGE_CLOSE_ID  (WM_APP + 0x777)
#define _FW_BROADCAST_MESSAGE_HOTKEY_ID (WM_APP + 0x778)

#define _FW_AGENT_WINDOW_TITLE         _T("Fw_agent")

// crypto args must be synchronized with web licensing service
#define _FW_LIC_CRYPTO_ALGO            _T("rijndael-128")
#define _FW_LIC_CRYPTO_KEY             _T("BZHS3Lu8PjyGLeBKDnDfr75T1WPvONHMRsLjnfkg")
#define _FW_LIC_CRYPTO_MODE            _T("cbc")
#define _FW_LIC_CRYPTO_VECTOR          _T("WWN7Gq5NeYFktsXhaubqbqP1UH1LbkQEIMBwZKJp")

#if !defined(_DEBUG_DO_NOT_USE_LOCAL__IIS)
#define _FW_LIC_WEB_REGISTER_QUERY     _T("https://thefileguardian.com/inbin/lic_reg.php")
#define _FW_LIC_WEB_REMOVE_QUERY       _T("https://thefileguardian.com/inbin/lic_rem.php")
#define _FW_LIC_WEB_CHECK_QUERY        _T("https://thefileguardian.com/inbin/lic_chk.php")
#define _FW_DAT_WEB_UPLOAD_QUERY       _T("https://thefileguardian.com/inbin/event_pro.php")
#define _FG_UPDATE_DOWNLOAD_URL        _T("https://thefileguardian.com/dlfiles/fg-setup.exe")
#define _FG_UPDATE_DOWNLOAD_FILE       _T("fg-setup.exe")
#else
#define _FW_LIC_WEB_REGISTER_QUERY     _T("http://localhost/FG/lic_reg.php")
#define _FW_LIC_WEB_REMOVE_QUERY       _T("http://localhost/FG/lic_rem.php")
#define _FW_LIC_WEB_CHECK_QUERY        _T("http://localhost/FG/lic_chk.php")
#define _FW_DAT_WEB_UPLOAD_QUERY       _T("http://localhost/FG/event_pro.php")
#define _FG_UPDATE_DOWNLOAD_URL        _T("https://thefileguardian.com/dlfiles/fg-setup.exe")
#define _FG_UPDATE_DOWNLOAD_FILE       _T("fg-setup.exe")
#endif

#define _FW_CONSOLE_CODE_SUCCESS       (0x0)
#define _FW_CONSOLE_CODE_PASSED        (0x1)
#define _FW_CONSOLE_CODE_FAILURE       (0x2)

#define _FW_LAST_ERROR_REG_PATH        _T("\\LastError")
#define _FW_LAST_ERROR_REG_NAME        _T("Details")

#include "FG_Generic_Defs.Names.h"

namespace fg { namespace common { namespace data
{
	class CModuleNames {
	protected:
		CAtlString     m_exec;
		CAtlString     m_name;
		HRESULT        m_result;
	protected:
		CModuleNames(void);
	public:
		LPCTSTR        Executable(void) const;
		HRESULT        LastResult(void) const;
		LPCTSTR        Name(void) const;
	};

	class CDriverNames : public CModuleNames
	{
		typedef CModuleNames TBase;
	public:
		CDriverNames(void);
	};

	class CServiceNames : public CModuleNames
	{
		typedef CModuleNames TBase;
	public:
		CServiceNames(void);
	};

	class CEvtSpyNames : public CModuleNames {
		typedef CModuleNames TBase;
	public:
		CEvtSpyNames(void);
	};

	class CRegistryPathFinder
	{
	public:
		class CExtension {
		public:
			static CAtlString Mode(void);
			static CAtlString Path(void);
		};
		class CService {
		public:
			static CAtlString SettingsPath(void);
			static HKEY       SettingsRoot(void);
		};
	};
}}}

//
// this code moved here from FG_desktop_version header file because inno setup doesn't recognize some C++ preprocessor directives;
// for the code below, it is DO_STRINGIFY macro that leads to 'Invalid # symbol' inno setup error;
//
#include "FG_Generic_Version.h"

#define DO_STRINGIFY(x)    #x
#define STRINGIFY(x)       DO_STRINGIFY(x)


#define FG_PRODUCT_VER    (STRINGIFY(FG_VER_MAJOR)    "." \
                           STRINGIFY(FG_VER_MINOR)    "." \
                           STRINGIFY(FG_VER_SUBMINOR) "." \
                           STRINGIFY(FG_VER_BUGFIX))

#define FG_DESKTOP_TITLE  ("File Guardian ["              \
                           STRINGIFY(FG_VER_MAJOR)    "." \
                           STRINGIFY(FG_VER_MINOR)    "." \
                           STRINGIFY(FG_VER_SUBMINOR) "." \
                           STRINGIFY(FG_VER_BUGFIX)   "]" )


#endif/*_FGCOMMONDEFS_H_AE1D4539_8DA4_4b69_B114_F653D9B0D394_INCLUDED*/
