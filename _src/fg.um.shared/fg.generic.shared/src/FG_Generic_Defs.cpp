/*
	Created by Tech_dog (VToropov) on 29-Dec-2017 at 5:48:17am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian generic shared definitions implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 27-May-2018 at 9:51:34p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "FG_Generic_Defs.h"

using namespace fg::common::data;

#include "Shared_SystemSecurity.h"
#include "Shared_GenericAppObject.h"

using namespace shared::lite::security;
using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

CModuleNames::CModuleNames(void) : m_result(OLE_E_BLANK) {}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR    CModuleNames::Executable(void) const { return m_exec.GetString(); }
HRESULT    CModuleNames::LastResult(void) const { return m_result;     }
LPCTSTR    CModuleNames::Name(void) const { return m_name.GetString(); }

/////////////////////////////////////////////////////////////////////////////

CDriverNames::CDriverNames(void)
{
	//
	// actually, driver name must be the same for both architecture;
	//
	TBase::m_name = _FG_DRIVER_NAME_COMMON;

	CAtlString cs_path;
	TBase::m_result = GetAppObjectRef().GetPathFromAppFolder(_T(".\\"), cs_path);
	if (!FAILED(TBase::m_result))
	{
		TBase::m_exec.Format(
				_T("%s.sys"), TBase::m_name.GetString()
			);
		cs_path      += TBase::m_exec;
		TBase::m_exec = cs_path;
	}
}

/////////////////////////////////////////////////////////////////////////////

CServiceNames::CServiceNames(void)
{
	//
	// the service executable name must be consistent with application bitness;
	// otherwise, it will cause system error during ::StartService command: 'The system cannot find the file specified.'
	//
	// the other important part is right service executable name, if name is changed, it also can
	// lead to system error as described above;
	//
	TBase::m_name =_FG_SERVICE_NAME_COMMON;

	CAtlString cs_path;
	TBase::m_result = GetAppObjectRef().GetPathFromAppFolder(_T(".\\"), cs_path);
	if (!FAILED(TBase::m_result))
	{
		TBase::m_exec.Format(
			_T("%s.exe"), TBase::m_name.GetString()
			);
		cs_path      += TBase::m_exec;
		TBase::m_exec = cs_path;
	}
}

/////////////////////////////////////////////////////////////////////////////

CEvtSpyNames::CEvtSpyNames(void)
{
	//
	// actually, driver name must be the same for both architecture;
	//
	TBase::m_name = _FG_EVTSPY_NAME_COMMON;

	CAtlString cs_path;
	TBase::m_result = GetAppObjectRef().GetPathFromAppFolder(_T(".\\"), cs_path);
	if (!FAILED(TBase::m_result))
	{
		TBase::m_exec.Format(
			_T("%s.sys"), TBase::m_name.GetString()
		);
		cs_path      += TBase::m_exec;
		TBase::m_exec = cs_path;
	}
}

/////////////////////////////////////////////////////////////////////////////

CAtlString CRegistryPathFinder::CExtension::Mode(void) {
	CAtlString cs_mode(_T("CompareMode"));
	return cs_mode;
}

CAtlString CRegistryPathFinder::CExtension::Path(void) {

	CAtlString cs_path;
	cs_path.Format(
		_T("%s\\Folders\\Options"), CRegistryPathFinder::CService::SettingsPath().GetString()
	);
	return cs_path;
}

CAtlString CRegistryPathFinder::CService::SettingsPath(void)
{
	CServiceNames service_;

	CAtlString sid_ = CSystemSIDs::GetSidAsText(CSystemSIDs::eLocalSystem);
	CAtlString path_;
	path_.Format(
			_T("%s\\Software\\%s"), sid_.GetString(), service_.Name()
		);
	return path_;
}

HKEY       CRegistryPathFinder::CService::SettingsRoot(void)
{
	return HKEY_USERS;
}