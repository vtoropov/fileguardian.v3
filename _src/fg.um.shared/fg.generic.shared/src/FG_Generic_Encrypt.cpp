/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 5:59:40am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian generic shared library crypto data class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 27-May-2018 at 11:30:50p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "StdAfx.h"
#include "FG_Generic_Encrypt.h"

using namespace fg::common::data;
using namespace shared::crypto;

/////////////////////////////////////////////////////////////////////////////

CCryptoDataProviderBase::CCryptoDataProviderBase(const CCryptoDataCrt& _crt) : m_provider(_crt)
{
	m_error.Source(_T("CCryptoDataProviderBase"));
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CCryptoDataProviderBase::Decrypt(const CAtlString& _encoded, CAtlString& _decoded)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (_encoded.IsEmpty())
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Encoded data cannot be empty")
			);
		return m_error;
	}

	_variant_t v_dec_b64;
	_variant_t v_enc_b64 = _encoded.GetString();

	CBase64Provider b64_;
	HRESULT hr_ = b64_.Decode(v_enc_b64, v_dec_b64);
	if (FAILED(hr_))
		return (m_error = b64_.Error());

	hr_ = m_provider.Initialize();
	if (FAILED(hr_))
		return (m_error = m_provider.Error());

	_variant_t v_dec_;
	hr_ = m_provider.Decrypt(v_dec_b64, v_dec_);
	if (FAILED(hr_))
		m_error = m_provider.Error();
	else
		_decoded = v_dec_;

	m_provider.Terminate();

	return m_error;
}

HRESULT      CCryptoDataProviderBase::Encrypt(const CAtlString& _decoded, CAtlString& _encoded)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = m_provider.Initialize();
	if (FAILED(hr_))
		return (m_error = m_provider.Error());

	_variant_t v_enc_;
	_variant_t v_dec_ = _decoded.GetString();

	hr_ = m_provider.Encrypt(v_dec_, v_enc_);
	if (FAILED(hr_))
		m_error = m_provider.Error();
	else
	{
#if (0)
		shared::log::DumpToFile(
				_T("e:\\enc_dump.bin"),
				v_enc_
			);
#endif
		_variant_t v_b64;
		CBase64Provider b64_;
		hr_ = b64_.Encode(v_enc_, v_b64);
		if (FAILED(hr_))
			m_error = b64_.Error();
		else
			_encoded = v_b64;
	}
	m_provider.Terminate();

	return m_error;
}

TErrorRef    CCryptoDataProviderBase::Error(void)const
{
	return m_error;
}