/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Apr-2016 at 10:45:15pm, GMT+7, Phuket, Rawai, Thursday;
	This is File Guardian generic shared library UI shared settings class implementation file.
*/
#include "StdAfx.h"
#include "FG_Generic_UI.h"
#include "FG_Generic_Defs.h"

using namespace fg::common::data;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"

using namespace shared::lite::common;
using namespace shared::registry;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data { namespace details
{
	class CUISharedCfg_Registry : private CRegistryPathFinder
	{
		typedef CRegistryPathFinder TBase;
	private:
		CRegistryStorage m_stg;
	public:
		CUISharedCfg_Registry(void) : 
		m_stg(TBase::CService::SettingsRoot(), CRegistryOptions::eDoNotModifyPath)
		{
		}
	public:
		HRESULT      Load(CHotKeys& _keys)
		{
			LONG lValue = 0;
			m_stg.Load(
					this->_GetRegPath(),
					this->_GetModifiersNamedValue(),
					lValue,
					MOD_ALT // ALT
				);
			_keys.Modifiers(static_cast<UINT>(lValue));

			m_stg.Load(
					this->_GetRegPath(),
					this->_GetVirtualKeyNamedValue(),
					lValue,
					0x30 // 0 (zero)
				);
			_keys.VirtualKey(static_cast<UINT>(lValue));

			HRESULT hr_ = S_OK;
			return  hr_;
		}

		HRESULT      Save(const CHotKeys& _keys)
		{
			m_stg.Save(
					this->_GetRegPath(),
					this->_GetModifiersNamedValue(),
					static_cast<LONG>(_keys.Modifiers())
				);

			m_stg.Save(
					this->_GetRegPath(),
					this->_GetVirtualKeyNamedValue(),
					static_cast<LONG>(_keys.VirtualKey())
				);

			HRESULT hr_ = S_OK;
			return  hr_;
		}
	private:
		CAtlString  _GetRegPath(void)const
		{
			CAtlString path_= TBase::CService::SettingsPath();
			path_ += _T("\\Desktop\\HotKeys");
			return path_;
		}
		CAtlString  _GetModifiersNamedValue (void)const   { return CAtlString(_T("Modifiers")); }
		CAtlString  _GetVirtualKeyNamedValue(void)const   { return CAtlString(_T("VirtualKey"));}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CHotKeys::CHotKeys(void) : m_modifiers(0), m_vkey(0)
{
}

/////////////////////////////////////////////////////////////////////////////

UINT       CHotKeys::Modifiers (void)const
{
	return m_modifiers;
}

VOID       CHotKeys::Modifiers (const UINT _modifiers)
{
	m_modifiers = _modifiers;
}

UINT       CHotKeys::VirtualKey(void)const
{
	return m_vkey;
}

VOID       CHotKeys::VirtualKey(const UINT _vkey)
{
	m_vkey = _vkey;
}

/////////////////////////////////////////////////////////////////////////////

CUISharedCfg::CUISharedCfg(void)
{
	m_error.Source(_T("CUISharedCfg"));
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef  CUISharedCfg::Error (void)const
{
	return m_error;
}

const
CHotKeys&  CUISharedCfg::HotKeys(void)const
{
	return m_keys;
}

CHotKeys&  CUISharedCfg::HotKeys(void)
{
	return m_keys;
}

/////////////////////////////////////////////////////////////////////////////

CUISharedCfgPersistent::CUISharedCfgPersistent(void)
{
	m_error.Source(_T("CUISharedCfgPersistent"));
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CUISharedCfgPersistent::Load(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	details::CUISharedCfg_Registry reg_;
	HRESULT hr_ = reg_.Load(TBase::m_keys);
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}

HRESULT    CUISharedCfgPersistent::Save(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	details::CUISharedCfg_Registry reg_;
	HRESULT hr_ = reg_.Save(TBase::m_keys);
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}