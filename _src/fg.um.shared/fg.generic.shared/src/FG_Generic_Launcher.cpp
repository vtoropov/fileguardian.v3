/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Jan-2016 at 11:39:15pm, GMT+7, Phuket, Rawai, Monday;
	This is File Guardian background service launcher class implementation file.
*/
#include "StdAfx.h"
#include "FG_Generic_Launcher.h"
#include "FG_Generic_UI.h"

using namespace fg::common;
using namespace fg::common::data;

#include "Shared_SystemCore.h"
#include "Shared_GenericEvent.h"
#include "Shared_DateTime.h"
#include "Shared_GenericAppObject.h"
//#include "Shared_SystemTask.h"

using namespace shared::lite::sys_core;
using namespace shared::runnable;
using namespace shared::lite::data;
using namespace shared::user32;

#include <shellapi.h>
/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace details
{
	CAtlString DesktopLauncher_GetFilePath(void)
	{
		static CAtlString cs_path;
		if (cs_path.IsEmpty())
		{
			CApplication& the_app = GetAppObjectRef();
#if defined(_DEBUG)
			the_app.GetPathFromAppFolder(_T(".\\FW_desktop_x86.exe"),  cs_path);
#else
			the_app.GetPathFromAppFolder(_T(".\\FW_desktop.exe"),  cs_path);
#endif
		}
		return cs_path;
	}

	CAtlString DesktopLauncher_GetKeyModifier(const CLauncherKeyModifier::_e _modifier)
	{
		CAtlString cs_mod;

		switch (_modifier)
		{
		case CLauncherKeyModifier::eKeyModifierAlt : cs_mod = _T("ALT"); break;
		case CLauncherKeyModifier::eKeyModifierCtrl: cs_mod = _T("CTRL"); break;
		case CLauncherKeyModifier::eKeyModifierShft: cs_mod = _T("SHIFT"); break;
		}

		return cs_mod;
	}

	CLauncherKeyModifier& DesktopLauncher_GetModifierInvalidObject(void)
	{
		static CLauncherKeyModifier mod_;
		return mod_;
	}

	CAtlString DesktopLauncher_GetKeyTitle(const INT _key)
	{
		/*
		 * VK_0 - VK_9 are the same as ASCII '0' - '9' (0x30 - 0x39)
		 * 0x40 : unassigned
		 * VK_A - VK_Z are the same as ASCII 'A' - 'Z' (0x41 - 0x5A)
		 */
		const char ch_ = (char)::MapVirtualKey(_key, MAPVK_VK_TO_CHAR);
		CAtlString title_;
		title_.Format(
				_T(" %c"), ch_
			);
		return title_;
	}

	CLauncherKey& DesktopLauncher_GetKeyInvalidObject(void)
	{
		static CLauncherKey key_;
		return key_;
	}

	DWORD&     DesktopLauncher_ThreadIdRef(void)
	{
		static DWORD id_ = 0;
		return id_;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CLauncherKey::CLauncherKey(void) : m_id(0)
{
}

CLauncherKey::CLauncherKey(const INT _id):m_id(_id)
{
	m_name = details::DesktopLauncher_GetKeyTitle(m_id);
}

/////////////////////////////////////////////////////////////////////////////

INT         CLauncherKey::Identifier(void)const
{
	return m_id;
}

VOID        CLauncherKey::Identifier(const INT _id)
{
	m_id = _id;
}

CAtlString  CLauncherKey::Name(void)const
{
	return m_name;
}

VOID        CLauncherKey::Name(LPCTSTR _name)
{
	m_name = _name;
}

/////////////////////////////////////////////////////////////////////////////
CLauncherKeyEnum::TLauncherKeys CLauncherKeyEnum::m_keys;
/////////////////////////////////////////////////////////////////////////////

CLauncherKeyEnum::CLauncherKeyEnum(void)
{
	if (m_keys.empty())
	{
		/*
		 * VK_0 - VK_9 are the same as ASCII '0' - '9' (0x30 - 0x39)
		 * 0x40 : unassigned
		 * VK_A - VK_Z are the same as ASCII 'A' - 'Z' (0x41 - 0x5A)
		 */
		try
		{
			for (INT i_ = 0x30; i_ <= 0x39; i_++)
				m_keys.push_back(CLauncherKey(i_));
			for (INT i_ = 0x41; i_ <= 0x5A; i_++)
				m_keys.push_back(CLauncherKey(i_));
		}
		catch(::std::bad_alloc&)
		{
		}
	}
}

/////////////////////////////////////////////////////////////////////////////

INT                 CLauncherKeyEnum::Count(void)const
{
	return static_cast<INT>(m_keys.size());
}

INT                 CLauncherKeyEnum::Find (const UINT _vkey)const
{
	for (size_t i_ = 0; i_ < m_keys.size(); i_++)
	{
		if (m_keys[i_].Identifier() == static_cast<INT>(_vkey))
			return static_cast<INT>(i_);
	}

	return -1;
}

const
CLauncherKey&       CLauncherKeyEnum::Item (const INT nIndex)const
{
	if (-1 < nIndex && nIndex < this->Count())
		return m_keys[nIndex];
	else
		return details::DesktopLauncher_GetKeyInvalidObject();
}

CLauncherKey&       CLauncherKeyEnum::Item (const INT nIndex)
{
	if (-1 < nIndex && nIndex < this->Count())
		return m_keys[nIndex];
	else
		return details::DesktopLauncher_GetKeyInvalidObject();
}

/////////////////////////////////////////////////////////////////////////////

CLauncherKeyModifier::CLauncherKeyModifier(void) : m_id(CLauncherKeyModifier::eKeyModifierNone)
{
}

CLauncherKeyModifier::CLauncherKeyModifier(const CLauncherKeyModifier::_e _id):m_id(_id)
{
	m_name  = details::DesktopLauncher_GetKeyModifier(m_id);
}

/////////////////////////////////////////////////////////////////////////////

CLauncherKeyModifier::_e
              CLauncherKeyModifier::Identifier(void)const
{
	return m_id;
}

VOID          CLauncherKeyModifier::Identifier(const CLauncherKeyModifier::_e _id)
{
	m_id = _id;
}

CAtlString    CLauncherKeyModifier::Name(void)const
{
	return m_name;
}

VOID          CLauncherKeyModifier::Name(LPCTSTR _name)
{
	m_name = _name;
}

/////////////////////////////////////////////////////////////////////////////

CLauncherKeyModifierEnum::TKeyModifiers CLauncherKeyModifierEnum::m_mods;
/////////////////////////////////////////////////////////////////////////////

CLauncherKeyModifierEnum::CLauncherKeyModifierEnum(void)
{
	if (m_mods.empty())
	{
		try
		{
			m_mods.push_back(CLauncherKeyModifier(CLauncherKeyModifier::eKeyModifierAlt));
			m_mods.push_back(CLauncherKeyModifier(CLauncherKeyModifier::eKeyModifierShft));
			m_mods.push_back(CLauncherKeyModifier(CLauncherKeyModifier::eKeyModifierCtrl));
		}
		catch(::std::bad_alloc&)
		{
		}
	}
}

/////////////////////////////////////////////////////////////////////////////

INT                   CLauncherKeyModifierEnum::Count(void)const
{
	return static_cast<INT>(m_mods.size());
}

const
CLauncherKeyModifier& CLauncherKeyModifierEnum::Item (const INT nIndex)const
{
	if (-1 < nIndex && nIndex < this->Count())
		return m_mods[nIndex];
	else
		return details::DesktopLauncher_GetModifierInvalidObject();
}

CLauncherKeyModifier& CLauncherKeyModifierEnum::Item (const INT nIndex)
{
	if (-1 < nIndex && nIndex < this->Count())
		return m_mods[nIndex];
	else
		return details::DesktopLauncher_GetModifierInvalidObject();
}

/////////////////////////////////////////////////////////////////////////////

CDesktopLauncher::CDesktopLauncher(void) : m_error(m_sync_obj), m_state(CThreadState::eStopped)
{
	m_error.Source(_T("CDesktopLauncher"));
}

CDesktopLauncher::~CDesktopLauncher(void)
{
}

/////////////////////////////////////////////////////////////////////////////

CSysError     CDesktopLauncher::Error(void)const
{
	CSysError err_obj;
	{
		SAFE_LOCK(m_sync_obj);
		err_obj = m_error;
	}
	return err_obj;
}

bool          CDesktopLauncher::IsRunning(void)const
{
	bool bRunning = false;
	{
		SAFE_LOCK(m_sync_obj);
		bRunning = !!(CThreadState::eWorking & m_state);
	}
	return bRunning;
}

HRESULT       CDesktopLauncher::Start(void)
{
	m_error.Module(_T(__FUNCTION__));

	if (this->IsRunning())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("Launcher is already running")
			);
		return m_error;
	}

	m_crt.IsStopped(false);

	if (!CThreadPool::QueueUserWorkItem(&CDesktopLauncher::LauncherWorkerThread, this))
		m_error = ::GetLastError();
	else if (m_error)
		m_error.Clear();

	if (m_error)
		m_state = CThreadState::eError;
	else
		m_state = CThreadState::eWorking;

	return m_error;
}

DWORD         CDesktopLauncher::State(void)const
{
	DWORD state_ = 0;
	{
		SAFE_LOCK(m_sync_obj);
		state_ = m_state;
	}
	return state_;
}

HRESULT       CDesktopLauncher::Stop (void)
{
	m_error.Module(_T(__FUNCTION__));

	if (!this->IsRunning())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("Launcher is already stopped")
			);
		return m_error;
	}

	const DWORD threadId_ = details::DesktopLauncher_ThreadIdRef();
	if (threadId_)
		::PostThreadMessage(
				threadId_,
				WM_QUIT,
				0,
				0
			);

	m_crt.IsStopped(true);

	const DWORD dwResult = ::WaitForSingleObject(m_crt.EventObject(), INFINITE);

	if (dwResult != WAIT_OBJECT_0)
		m_error = ::GetLastError();
	else if (m_error)
		m_error.Clear();

	if (m_error)
	{
		SAFE_LOCK(m_sync_obj);
		m_state = CThreadState::eError;
	}
	else
	{
		SAFE_LOCK(m_sync_obj);
		m_state = CThreadState::eStopped;
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDesktopLauncher::RegisterHotKey(HWND _target, const UINT _evtId, const UINT _modifiers, const UINT _vkey)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const BOOL bResult = ::RegisterHotKey(_target, _evtId, _modifiers, _vkey);
	if (!bResult)
		m_error = ::GetLastError();

	return m_error;
}

HRESULT       CDesktopLauncher::UnregisterHotKey(HWND _target, const UINT _evtId)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const BOOL bResult = ::UnregisterHotKey(_target, _evtId);
	if (!bResult)
		m_error = ::GetLastError();

	return m_error;
}


/////////////////////////////////////////////////////////////////////////////

VOID          CDesktopLauncher::LauncherWorkerThread(void)
{
	CCoInitializer com_core(false);

	details::DesktopLauncher_ThreadIdRef() = ::GetCurrentThreadId();

	CGenericWaitCounter wait_(10, 10);

	MSG  msg_ = {0};
	BOOL ret_ = FALSE;

	CUISharedCfgPersistent pers_;

	HRESULT hr_ = pers_.Load();
	if (FAILED(hr_))
		goto __end_sub__;

	hr_ = this->RegisterHotKey(
			NULL,
			1   ,
			pers_.HotKeys().Modifiers(),
			pers_.HotKeys().VirtualKey()
		);

	if (FAILED(hr_))
	{
		CEventJournal::LogError(
				this->Error()
			);
		goto __end_sub__;
	}

	CEventJournal::LogInfo(
			_T("Desktop launcher has successfully registered hot key")
		);

	while( (ret_ = ::GetMessage( &msg_, NULL, 0, 0 )) > 0)
	{
		if (m_crt.IsStopped())
			break;
		if (WM_HOTKEY == msg_.message)
		{
			CEventJournal::LogInfo(
					_T("Desktop launcher has received the hot key")
				);

			const CAtlString cs_path = details::DesktopLauncher_GetFilePath();
			::ShellExecute(
					NULL,
					_T("open"),
					cs_path.GetString(),
					NULL,
					NULL,
					SW_SHOWDEFAULT
				);
		}
		::TranslateMessage(&msg_);
		::DispatchMessage(&msg_);
	}

	hr_ = this->UnregisterHotKey(NULL, 1);

__end_sub__:
	details::DesktopLauncher_ThreadIdRef() = 0;

	::SetEvent(m_crt.EventObject());
}