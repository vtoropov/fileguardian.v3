/*
	Created by Tech_dog (VToropov) on 22-Apr-2016 at 8:56:25pm, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian Agent utility startup task related command handler class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jun-2018 at 9:48:34p, UTC+7, Phuket, Rawai, Friday;
*/
#include "StdAfx.h"
#include "fg.ui.agent.handler.task.h"

using namespace fg::agent::handlers;

#include "Shared_SystemTask.h"

using namespace shared::lite::sys_core;

#include "FG_Generic_Defs.h"

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace agent { namespace handlers { namespace details
{
	HRESULT  StartupTaskCmdHandler_OnCheck (INT& _result)
	{
		CStartupTask task_;
		const bool bIsOn = task_.IsOn();

		_result = (bIsOn ? CHandlerResult::eSuccess : CHandlerResult::eFailure);

		HRESULT hr_ = S_OK;
		return  hr_;
	}

	HRESULT  StartupTaskCmdHandler_OnCreate(INT& _result, const TCommandLine& _cmd_ln)
	{
		const CAtlString exe_ = _cmd_ln.Argument(_T("exe_path"));

		CStartupTask task_(exe_);
		if (task_.IsOn()) {
			_result = CHandlerResult::ePassed;
			return S_OK;
		}

		HRESULT hr_ = task_.On();
		if (FAILED(hr_))
		{
			_result = CHandlerResult::eFailure;
			CEventJournal::LogError(
					_T("FG Agent creates startup task: %s"),
					CSysError(hr_).GetFormattedDetails().GetString()
				);
		}
		else {
			_result = CHandlerResult::eSuccess;
			CEventJournal::LogInfo(_T("FG Agent: startup task is created"));
		}
		return hr_;
	}

	HRESULT  StartupTaskCmdHandler_OnRemove(INT& _result)
	{
		CStartupTask task_;
		if (!task_.IsOn()) {
			_result = CHandlerResult::ePassed;
			return S_OK;
		}

		HRESULT hr_ = task_.Off();
		if (FAILED(hr_)) {
			_result = CHandlerResult::eFailure;
			CEventJournal::LogError(
					_T("FG Agent removing task error: %s"),
					CSysError(hr_).GetFormattedDetails().GetString()
				);
		}
		else {
			_result = CHandlerResult::eSuccess;
			CEventJournal::LogInfo(_T("FG Agent: remove task success"));
		}
		return hr_;
	}

	LPCTSTR   StartupTaskCmdHandler_Arg_0(void) { static LPCTSTR lpsz_arg_0 = _T("startup_task"); return lpsz_arg_0; }
	LPCTSTR   StartupTaskCmdHandler_Arg_1(void) { static LPCTSTR lpsz_arg_1 = _T("exe_path");     return lpsz_arg_1; }
	LPCTSTR   StartupTaskCmdHandler_Val_0(void) { static LPCTSTR lpsz_val_0 = _T("check");  return lpsz_val_0; }
	LPCTSTR   StartupTaskCmdHandler_Val_1(void) { static LPCTSTR lpsz_val_1 = _T("create"); return lpsz_val_1; }
	LPCTSTR   StartupTaskCmdHandler_Val_2(void) { static LPCTSTR lpsz_val_2 = _T("remove"); return lpsz_val_2; }
}}}}

/////////////////////////////////////////////////////////////////////////////

CStartupTaskCmdHandler::CStartupTaskCmdHandler(const TCommandLine& _cmd_ln) : TBase(_cmd_ln, _T("CStartupTaskCmdHandler")) {
	TBase::m_args.Format(
		_T("name=%s;value={%s|%s|%s};name=%s;value={exe_path}"),
		details::StartupTaskCmdHandler_Arg_0(),
		details::StartupTaskCmdHandler_Val_0(),
		details::StartupTaskCmdHandler_Val_1(),
		details::StartupTaskCmdHandler_Val_2(),
		details::StartupTaskCmdHandler_Arg_1()
	);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CStartupTaskCmdHandler::Handle(INT& _result)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsApplicable())
		return (m_error = S_FALSE);
	
	const CAtlString cs_cmd = m_cmd_ln.Argument(details::StartupTaskCmdHandler_Arg_0());

	if (0 == cs_cmd.CompareNoCase(details::StartupTaskCmdHandler_Val_0())) m_error = details::StartupTaskCmdHandler_OnCheck (_result);
	if (0 == cs_cmd.CompareNoCase(details::StartupTaskCmdHandler_Val_1())) m_error = details::StartupTaskCmdHandler_OnCreate(_result, m_cmd_ln);
	if (0 == cs_cmd.CompareNoCase(details::StartupTaskCmdHandler_Val_2())) m_error = details::StartupTaskCmdHandler_OnRemove(_result);

	return m_error;
}

bool        CStartupTaskCmdHandler::IsApplicable(void)const { return m_cmd_ln.Has(details::StartupTaskCmdHandler_Arg_0()); }