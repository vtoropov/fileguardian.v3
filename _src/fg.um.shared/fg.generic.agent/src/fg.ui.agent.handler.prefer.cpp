/*
	Created by Tech_dog (VToropov) on 20-Jul-2016 at 3:07:40a, GMT+7, Phuket, Rawai, Wednesday;
	his is File Guardian Agent utility user preference related command handler class implementation file.
*/
#include "StdAfx.h"
#include "fg.ui.agent.handler.prefer.h"

using namespace fg::agent::handlers;

#include "Shared_FS_GenericFolder.h"

using namespace shared::ntfs;

#include "FG_Service_Settings.h"

using namespace fg::common::data;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace agent { namespace handlers { namespace details
{
	TErrorRef   PreferCmdHandler_OnDefFolder(INT& _result, CAtlString _favor, CSysError& _err)
	{
		_err.Module(_T(__FUNCTION__));
		_err = S_OK;

		_result = CHandlerResult::eSuccess; // the installation process can ignore an error in this case;

		CSpecialFolder folder_;
		_favor = folder_.PersonalDocs();

		HRESULT hr_ = folder_.Error();
		if (FAILED(hr_))
			return (_err = folder_.Error());

		CServiceSettingPersistent& pers_ = GetServiceSettingsRef();
		if (pers_.IsInitialized() == false)
			hr_ = pers_.Load();
		if (FAILED(hr_))
			return (_err = pers_.Error() );

		hr_ = pers_.AppendFolder(_favor, CWatchedType::eIncluded);
		if (FAILED(hr_))
			return (_err = pers_.Error() );

		hr_ = pers_.Save();
		if (FAILED(hr_))
			_err = pers_.Error();
			
		return  _err;
	}

	LPCTSTR   PreferCmdHandler_Arg_0(void){ static LPCTSTR lpsz_arg_0 = _T("def_folder");  return lpsz_arg_0; }

}}}}

/////////////////////////////////////////////////////////////////////////////

CPreferCmdHandler::CPreferCmdHandler(const TCommandLine& _cmd_ln) : TBase(_cmd_ln, _T("CPreferCmdHandler")) {
	TBase::m_args.Format(
		_T("name=%s;value={preferable folder path};"), details::PreferCmdHandler_Arg_0()
	);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CPreferCmdHandler::Handle(INT& _result)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsApplicable())
		return (m_error = S_FALSE);

	details::PreferCmdHandler_OnDefFolder(_result, m_favor_folder, m_error);

	return m_error;
}

bool        CPreferCmdHandler::IsApplicable(void)const { return m_cmd_ln.Has(details::PreferCmdHandler_Arg_0()); }

/////////////////////////////////////////////////////////////////////////////

LPCTSTR     CPreferCmdHandler::FavorFolder(void)const { return m_favor_folder.GetString(); }