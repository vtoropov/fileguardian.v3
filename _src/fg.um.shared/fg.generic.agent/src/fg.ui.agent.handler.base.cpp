/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Jun-2018 at 2:44:24p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian command line agent utility handler base interface implementation file.
*/
#include "StdAfx.h"
#include "fg.ui.agent.handler.base.h"

using namespace fg::agent::handlers;

/////////////////////////////////////////////////////////////////////////////

CHandlerBase::CHandlerBase(const TCommandLine& _line, LPCTSTR lpszModule) : m_cmd_ln(_line) { m_error.Source(lpszModule); } 
CHandlerBase::~CHandlerBase(void) {}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR CHandlerBase::Args  (void)const { return m_args.GetString(); }

HRESULT CHandlerBase::Handle(INT& _result) {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	_result = CHandlerResult::eSuccess;

	return m_error;
}

bool    CHandlerBase::IsApplicable(void)const { return true; }

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CHandlerBase::Error (void)const { return m_error; }