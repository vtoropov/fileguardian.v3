/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Apr-2016 at 9:45:17pm, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian Agent utility license related command handler class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 5-Jun-2018 at 3:09:15p, UTC+7, Phuket, Rawai, Tuesday;
*/
#include "StdAfx.h"
#include "fg.ui.agent.handler.license.h"

using namespace fg::agent::handlers;

#include "FG_License_Info.h"
#include "FG_License_Provider.h"

using namespace fg::common;
using namespace fg::common::lic;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace agent { namespace handlers { namespace details
{

	TErrorRef LicenseCmdHandler_OnCheckState(INT& _result, CSysError& _err)
	{
		HRESULT hr_ = S_OK;

		CLicenseInfo lic_;
		CLicenseProvider prov_;
		hr_ = prov_.Storage().Load(lic_);
		if (FAILED(hr_)) {
			_result = CHandlerResult::eFailure;
			CEventJournal::LogError(
					prov_.Error()
				);
			_err = prov_.Error();
			return _err;
		}
		hr_ = prov_.CheckLicense(
				lic_
			);
		if (FAILED(hr_)) {
			_result = CHandlerResult::eFailure;
			CEventJournal::LogError(
					prov_.Error()
				);
			_err = prov_.Error();
			return _err;
		}
#if defined(_DEBUG)
		CEventJournal::LogInfo(
				_T("License Details:\n\t\t%s"), lic_.ToString(_T("\n\t\t")).GetString()
			);
#endif
		_result = CHandlerResult::eSuccess;
		if (lic_.IsActive())
			_err.SetState(
				(DWORD)NO_ERROR, _T("S_OK")
			);
		return  _err;
	}

	HRESULT   LicenseCmdHandler_OnRegister(INT& _result, const CAtlString& _sn, const CAtlString& _desc)
	{
		CLicenseInfo lic_;
		lic_.Serial(_sn);
		lic_.Description(_desc);

		lic_.Version().InitializeFromGlobalDef();

		CLicenseProvider prov_;

		HRESULT hr_ = prov_.RegisterLicense(lic_);
		if (FAILED(hr_))
		{
			_result = CHandlerResult::eFailure;
			CEventJournal::LogError(
					prov_.Error()
				);
			global::SaveLastError(     // saves last error for the installer
					prov_.Error()
				);
		}
		else
			_result = CHandlerResult::eSuccess;
	//
	//  The code is commented because we do not need to expose license related info
	//  in event viewer or another log capable output;
	//
	//	CEventJournal::LogWarn(
	//			_T("Installation ID: %s"),
	//			lic_.UserId()
	//		);
		return  hr_;
	}

	HRESULT   LicenseCmdHandler_OnRemove(INT& _result)
	{
		CLicenseProvider prov_;
		HRESULT hr_ = prov_.RemoveLicense();
		if (FAILED(hr_))
		{
			_result = CHandlerResult::eFailure;
			CEventJournal::LogError(
					prov_.Error()
				);
			global::SaveLastError(     // saves last error for the installer
					prov_.Error()
				);
		}
		else
			_result = CHandlerResult::eSuccess;
		return  hr_;
	}

	LPCTSTR   LicenseCmdHandler_Arg_0(void) { static LPCTSTR lpsz_arg_0 = _T("license");  return lpsz_arg_0; }
	LPCTSTR   LicenseCmdHandler_Arg_1(void) { static LPCTSTR lpsz_arg_1 = _T("serial");   return lpsz_arg_1; }
	LPCTSTR   LicenseCmdHandler_Arg_2(void) { static LPCTSTR lpsz_arg_2 = _T("desc");     return lpsz_arg_2; }
}}}}

/////////////////////////////////////////////////////////////////////////////

CLicenseCmdHandler::CLicenseCmdHandler(const TCommandLine& _cmd_ln) : TBase(_cmd_ln, _T("CLicenseCmdHandler")) {
	TBase::m_args.Format(
		_T("name=%s;value={register|remove|checkin};\n\tname=%s;value={serial};\n\tname=%s;value={details}"),
		details::LicenseCmdHandler_Arg_0(),
		details::LicenseCmdHandler_Arg_1(),
		details::LicenseCmdHandler_Arg_2()
	);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CLicenseCmdHandler::Handle(INT& _result)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsApplicable())
		return (m_error = S_FALSE);
	
	const CAtlString cs_cmd  = m_cmd_ln.Argument(details::LicenseCmdHandler_Arg_0());
	const CAtlString cs_sn   = m_cmd_ln.Argument(details::LicenseCmdHandler_Arg_1());
	const CAtlString cs_desc = m_cmd_ln.Argument(details::LicenseCmdHandler_Arg_2());

	if (0 == cs_cmd.CompareNoCase(_T("register"))) m_error = details::LicenseCmdHandler_OnRegister(_result, cs_sn, cs_desc);
	if (0 == cs_cmd.CompareNoCase(_T("remove")))   m_error = details::LicenseCmdHandler_OnRemove  (_result);
	if (0 == cs_cmd.CompareNoCase(_T("checkin")))  m_error = details::LicenseCmdHandler_OnCheckState(_result, m_error);

	return m_error;
}

bool        CLicenseCmdHandler::IsApplicable(void)const { return m_cmd_ln.Has(details::LicenseCmdHandler_Arg_0()); }