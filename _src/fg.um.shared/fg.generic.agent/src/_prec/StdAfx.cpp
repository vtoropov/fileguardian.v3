/*
	Created by Tech_dog (VToropov) on 2-Feb-2016 at 10:08:34pm, GMT+7, Phuket, Rawai, Tuesday;
	This is File Watcher system tray agent precompiled headers implementation file.
*/

#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)

#include "Shared_Registry.h"

using namespace shared::registry;

#include "FG_Generic_Defs.h"

using namespace fg::common::data;

namespace global
{
	VOID  SaveLastError(TErrorRef _error)
	{
		CRegistryStorage stg_(
				CRegistryPathFinder::CService::SettingsRoot(),
				CRegistryOptions::eDoNotModifyPath
			);

		CAtlString reg_path;

		reg_path += CRegistryPathFinder::CService::SettingsPath();
		reg_path += _FW_LAST_ERROR_REG_PATH;

		stg_.Save(
				reg_path,
				_FW_LAST_ERROR_REG_NAME,
				_error.GetDescription()
			);
	}
}