#ifndef _FWAGENTPRECOMPILEDHEADER_H_763B71B5_ABCD_4480_A737_C1100328D3D4_INCLUDED
#define _FWAGENTPRECOMPILEDHEADER_H_763B71B5_ABCD_4480_A737_C1100328D3D4_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Jun-2018 at 1:58:24p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian agent utility shared library precompiled headers definition file.
*/
#ifndef WINVER                  // Specifies that the minimum required platform is Windows Vista.
#define WINVER 0x0600           // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT            // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINNT 0x0600     // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINDOWS          // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINDOWS 0x0600   // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE               // Specifies that the minimum required platform is Internet Explorer 7.0.
#define _WIN32_IE 0x0700        // Change this to the appropriate value to target other versions of IE.
#endif


#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>             // important order, this file must be included before any includes of the WTL headers
#include <comdef.h>

#include <map>
#include <vector>
#include <conio.h>

#include "Shared_SystemError.h"
#include "Shared_LogJournal.h"

using shared::log::CEventJournal;

namespace global
{
	VOID  SaveLastError(TErrorRef);
}

#pragma comment(lib, "__shared.lite_v15.lib")
#pragma comment(lib, "_crypto_v15.lib")
#pragma comment(lib, "_generic.stg_v15.lib")
#pragma comment(lib, "_log_v15.lib")
#pragma comment(lib, "_net_v15.lib")
#pragma comment(lib, "_ntfs_v15.lib")
#pragma comment(lib, "_registry_v15.lib")
#pragma comment(lib, "_service_v15.lib")
#pragma comment(lib, "_user.32_v15.lib")
#pragma comment(lib, "_wmi.service_v15.lib")
#pragma comment(lib, "fg.generic.shared_v15.lib")
#pragma comment(lib, "fg.service.shared_v15.lib")
#pragma comment(lib, "lib.mcrypt.adopted_v15.lib")

#endif/*_FWAGENTPRECOMPILEDHEADER_H_763B71B5_ABCD_4480_A737_C1100328D3D4_INCLUDED*/