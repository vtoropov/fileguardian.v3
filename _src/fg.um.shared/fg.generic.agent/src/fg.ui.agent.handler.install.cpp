/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Apr-2017 at 2:57:19a, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian Agent utility installer related command handler class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 5-Jun-2018 at 2:17:36p, UTC+7, Phuket, Rawai, Tuesday;
*/
#include "StdAfx.h"
#include "fg.ui.agent.handler.install.h"

using namespace fg::agent::handlers;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace agent { namespace handlers { namespace details
{
	class CInstallerCmdHandler_Runner {
	private:
		CSysError       m_error;
		bool            m_no_args;
	public:
		CInstallerCmdHandler_Runner(const bool bNoArgs) : m_no_args(bNoArgs) {
			m_error.Source(_T("CInstallerCmdHandler_Runner"));
		}
	public:
		TErrorRef       Error(void) const { return m_error; }
		HRESULT         Run  (INT& _result, LPCTSTR _installer_pack) {
			m_error.Module(_T(__FUNCTION__));
			m_error = S_OK;

			CAtlString cs_title(FG_DESKTOP_TITLE);

			HWND hDesktop = NULL;
			do {
				::Sleep(500);
				hDesktop = ::FindWindow(NULL, cs_title);
			} while (::IsWindow(hDesktop));

			SHELLEXECUTEINFO info_ = {0};
			this->_PrepareData(_installer_pack, info_);

			const BOOL result_ = ::ShellExecuteEx(&info_);
			if (!result_){
				_result = CHandlerResult::eFailure;
				m_error = ::GetLastError();
			}
			else
				_result = CHandlerResult::eSuccess;

			return m_error;
		}
	private:
		VOID    _PrepareData(LPCTSTR _lpsz_app, SHELLEXECUTEINFO& _info) {

			::memset(&_info, 0,  sizeof(SHELLEXECUTEINFO));
			_info.cbSize       = sizeof(SHELLEXECUTEINFO);
			_info.fMask        = SEE_MASK_NOCLOSEPROCESS ;
			_info.lpVerb       = _T("runas");
			_info.lpFile       = _lpsz_app;

			if (m_no_args == false) {
				_info.lpParameters = _T("/SILENT");
			}

			_info.hwnd         = HWND_DESKTOP;

			if (m_no_args == false) {
			_info.nShow        = SW_NORMAL;
			}
			else {
			_info.nShow        = SW_MINIMIZE|SW_SHOWNOACTIVATE;
			}
		}
	};

	LPCTSTR   InstallerCmdHandler_CmdArg_0(void) {
		static LPCTSTR lpsz_arg_0 = _T("run");
		return lpsz_arg_0;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CInstallerCmdHandler::CInstallerCmdHandler(const TCommandLine& _cmd_ln) : TBase(_cmd_ln, _T("CInstallerCmdHandler")), b_dis_args(false) {

	m_args.Format(
		_T("Arg_0: name=%s, value={exe path for running in silent mode}"), details::InstallerCmdHandler_CmdArg_0()
		);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CInstallerCmdHandler::Handle(INT& _result)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsApplicable())
		return (m_error = S_FALSE);
	
	const CAtlString cs_file  = TBase::m_cmd_ln.Argument(details::InstallerCmdHandler_CmdArg_0());
	if (cs_file.IsEmpty()) {
		m_error.SetState (
			E_INVALIDARG, _T("Target file path is empty.")
		);
		return m_error;
	}

	details::CInstallerCmdHandler_Runner runner_(b_dis_args);

	HRESULT hr_ = runner_.Run(_result, cs_file.GetString());
	if (FAILED(hr_))
		m_error = runner_.Error();

	return m_error;
}

bool        CInstallerCmdHandler::IsApplicable(void)const { return TBase::m_cmd_ln.Has(details::InstallerCmdHandler_CmdArg_0()); }

/////////////////////////////////////////////////////////////////////////////

VOID        CInstallerCmdHandler::DisableTargetAppArgs(const bool _val) { b_dis_args = _val; }