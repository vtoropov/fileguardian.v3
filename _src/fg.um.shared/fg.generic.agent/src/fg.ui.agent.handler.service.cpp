/*
	Created by Tech_dog (VToropov) on 22-Apr-2016 at 8:10:17pm, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian Agent utility service related command handler class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jun-2018 at 9:27:25p, UTC+7, Phuket, Rawai, Friday;
*/
#include "StdAfx.h"
#include "fg.ui.agent.handler.service.h"

using namespace fg::agent::handlers;

#include "FG_Service_Manager.h"
#include "FG_Generic_Defs.h"

using namespace fg::common;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace agent { namespace handlers { namespace details
{
	HRESULT ServiceCmdHandler_OnInstall(INT& _result, const TCommandLine& _cmd_ln)
	{
		const CAtlString  exe_ = _cmd_ln.Argument(_T("exe_path"));
		_result = CHandlerResult::eSuccess;

		CFwServiceManager mgr_(exe_);

		HRESULT hr_ = mgr_.Error();
		if (SUCCEEDED(hr_)) {

			if (mgr_.InstallService() == false) {
				hr_ = mgr_.Error();

				CEventJournal::LogError(
					_T("FG Agent: %s"),
					mgr_.Error().GetFormattedDetails().GetString()
				);
				global::SaveLastError(
					mgr_.Error()
				);
				_result = CHandlerResult::eFailure;
			}
		}	
		return hr_;
	}

	HRESULT ServiceCmdHandler_OnRemove(INT& _result)
	{
		_result = CHandlerResult::eSuccess;

		CFwServiceManager mgr_;

		HRESULT hr_ = mgr_.Error();
		if (SUCCEEDED(hr_))
		{
			const bool bInstalled = mgr_.IsInstalled();
			if (bInstalled)
			{
				bool bSuccess = true;
				const CService& svc_ref = mgr_.GetServiceObject();

				if (svc_ref.State().IsRunning())
					bSuccess = mgr_.StopService();

				if (bSuccess)
					bSuccess = mgr_.UninstallService();

				hr_ = mgr_.Error();
			}
		}

		if (FAILED(hr_))
		{
			CEventJournal::LogError(
					_T("FG Agent: %s"),
					mgr_.Error().GetFormattedDetails().GetString()
				);
			global::SaveLastError(
					mgr_.Error()
				);
			_result = CHandlerResult::eFailure;
		}

		return hr_;
	}

	HRESULT ServiceCmdHandler_OnStart(INT& _result)
	{
		_result = CHandlerResult::eSuccess;

		CFwServiceManager mgr_;

		HRESULT hr_ = mgr_.Error();
		if (SUCCEEDED(hr_))
		{
			if (mgr_.StartService() == false)
				hr_ = mgr_.Error();
		}

		if (FAILED(hr_))
		{
			CEventJournal::LogError(
					_T("FG Agent: %s"),
					mgr_.Error().GetFormattedDetails().GetString()
				);
			global::SaveLastError(
					mgr_.Error()
				);
			_result = CHandlerResult::eFailure;
		}
		return hr_;
	}

	HRESULT ServiceCmdHandler_OnStop(INT& _result)
	{
		_result = CHandlerResult::eSuccess;

		CFwServiceManager mgr_;

		HRESULT hr_ = mgr_.Error();
		if (SUCCEEDED(hr_))
		{
			if (mgr_.StopService() == false)
				hr_ = mgr_.Error();
		}

		if (FAILED(hr_))
		{
			CEventJournal::LogError(
					_T("FG Agent: %s"),
					mgr_.Error().GetFormattedDetails().GetString()
				);
			global::SaveLastError(
					mgr_.Error()
				);
			_result = CHandlerResult::eFailure;
		}
		return hr_;
	}

	LPCTSTR   ServiceCmdHandler_Arg_0(void) { static LPCTSTR lpsz_arg_0 = _T("service");  return lpsz_arg_0; }
	LPCTSTR   ServiceCmdHandler_Arg_1(void) { static LPCTSTR lpsz_arg_1 = _T("exe_path"); return lpsz_arg_1; }
	LPCTSTR   ServiceCmdHandler_Val_0(void) { static LPCTSTR lpsz_val_0 = _T("install");  return lpsz_val_0; }
	LPCTSTR   ServiceCmdHandler_Val_1(void) { static LPCTSTR lpsz_val_1 = _T("start");    return lpsz_val_1; }
	LPCTSTR   ServiceCmdHandler_Val_2(void) { static LPCTSTR lpsz_val_2 = _T("stop");     return lpsz_val_2; }
	LPCTSTR   ServiceCmdHandler_Val_3(void) { static LPCTSTR lpsz_val_3 = _T("remove");   return lpsz_val_3; }
}}}}

/////////////////////////////////////////////////////////////////////////////

CServiceCmdHandler::CServiceCmdHandler(const TCommandLine& _cmd_ln) : TBase(_cmd_ln, _T("CServiceCmdHandler")) {
	TBase::m_args.Format(
		_T("name=%s;value={%s|%s|%s|%s}; name=%s;value={exe path};"),
		details::ServiceCmdHandler_Arg_0(),
		details::ServiceCmdHandler_Val_0(),
		details::ServiceCmdHandler_Val_1(),
		details::ServiceCmdHandler_Val_2(),
		details::ServiceCmdHandler_Val_3(),
		details::ServiceCmdHandler_Arg_1()
	);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CServiceCmdHandler::Handle(INT& _result)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsApplicable())
		return (m_error = S_FALSE);
	
	const CAtlString cs_cmd = TBase::m_cmd_ln.Argument(details::ServiceCmdHandler_Arg_0());

	if (0 == cs_cmd.CompareNoCase(details::ServiceCmdHandler_Val_0())) m_error = details::ServiceCmdHandler_OnInstall(_result, m_cmd_ln);
	if (0 == cs_cmd.CompareNoCase(details::ServiceCmdHandler_Val_3())) m_error = details::ServiceCmdHandler_OnRemove(_result);
	if (0 == cs_cmd.CompareNoCase(details::ServiceCmdHandler_Val_1())) m_error = details::ServiceCmdHandler_OnStart(_result);
	if (0 == cs_cmd.CompareNoCase(details::ServiceCmdHandler_Val_2())) m_error = details::ServiceCmdHandler_OnStop(_result);

	return m_error;
}

bool        CServiceCmdHandler::IsApplicable(void)const { return TBase::m_cmd_ln.Has(details::ServiceCmdHandler_Arg_0()); }