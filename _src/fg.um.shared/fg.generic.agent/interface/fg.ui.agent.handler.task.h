#ifndef _STARTUPTASKCMDHANDLER_H_24608CC8_58C3_46c7_986B_A12882A4566E_INCLUDED
#define _STARTUPTASKCMDHANDLER_H_24608CC8_58C3_46c7_986B_A12882A4566E_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Apr-2016 at 8:51:24pm, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian Agent utility startup task related command handler class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 5-Jun-2018 at 4:54:17p, UTC+7, Phuket, Rawai, Tuesday;
*/
#include "fg.ui.agent.handler.base.h"

namespace fg { namespace agent { namespace handlers
{
	using shared::lite::common::CSysError;

	class CStartupTaskCmdHandler : public CHandlerBase {

		typedef CHandlerBase  TBase;

	public:
		CStartupTaskCmdHandler(const TCommandLine&);

	public:
		HRESULT     Handle(INT& _result) override;
		bool        IsApplicable(void)const override;
	};
}}}

#endif/*_STARTUPTASKCMDHANDLER_H_24608CC8_58C3_46c7_986B_A12882A4566E_INCLUDED*/