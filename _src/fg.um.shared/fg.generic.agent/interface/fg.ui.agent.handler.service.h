#ifndef _SERVICECMDHANDLER_H_A784F3AD_9D1F_4acb_BA62_F99CEBAC42AA_INCLUDED
#define _SERVICECMDHANDLER_H_A784F3AD_9D1F_4acb_BA62_F99CEBAC42AA_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Apr-2016 at 7:32:09pm, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian Agent utility service related command handler class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 5-Jun-2018 at 4:46:03p, UTC+7, Phuket, Rawai, Tuesday;
*/
#include "fg.ui.agent.handler.base.h"

namespace fg { namespace agent { namespace handlers
{
	using shared::lite::common::CSysError;

	class CServiceCmdHandler : public CHandlerBase {

		typedef CHandlerBase  TBase;

	public:
		CServiceCmdHandler(const TCommandLine&);

	public:
		HRESULT     Handle(INT& _result) override;
		bool        IsApplicable(void)const override;
	};
}}}

#endif/*_SERVICECMDHANDLER_H_A784F3AD_9D1F_4acb_BA62_F99CEBAC42AA_INCLUDED*/