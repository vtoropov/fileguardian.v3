#ifndef _PREFERCMDHANDLER_H_687E1E7B_2EEB_4623_A421_2C55C32DA787_INCLUDED
#define _PREFERCMDHANDLER_H_687E1E7B_2EEB_4623_A421_2C55C32DA787_INCLUDED
/*
	Created by Tech_dog (VToropov) on 20-Jul-2016 at 3:03:56a, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian Agent utility user preference related command handler class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 5-Jun-2018 at 3:19:17p, UTC+7, Phuket, Rawai, Tuesday;
*/
#include "fg.ui.agent.handler.base.h"

namespace fg { namespace agent { namespace handlers
{
	using shared::lite::common::CSysError;

	class CPreferCmdHandler : public CHandlerBase {

		typedef CHandlerBase  TBase;
	private:
		CAtlString  m_favor_folder;

	public:
		CPreferCmdHandler(const TCommandLine&);

	public:
		HRESULT     Handle(INT& _result) override;
		bool        IsApplicable(void)const override;

	public:
		LPCTSTR     FavorFolder(void)const;
	};
}}}

#endif/*_PREFERCMDHANDLER_H_687E1E7B_2EEB_4623_A421_2C55C32DA787_INCLUDED*/