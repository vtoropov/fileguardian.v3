#ifndef _FGUIAGENTHANDLERBASE_H_7B9037B5_12C4_479A_B453_891F24A19505_INCLUDED
#define _FGUIAGENTHANDLERBASE_H_7B9037B5_12C4_479A_B453_891F24A19505_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Jun-2018 at 2:23:08p, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian command line agent utility handler base interface declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_GenericAppObject.h"

#include "Fg_Generic_Defs.h"

namespace fg { namespace agent { namespace handlers {

	using shared::lite::common::CSysError;

	class CHandlerResult {
	public:
		enum _e {
			eSuccess  = _FW_CONSOLE_CODE_SUCCESS,
			ePassed   = _FW_CONSOLE_CODE_PASSED ,
			eFailure  = _FW_CONSOLE_CODE_FAILURE,
		};
	};

	class CHandlerBase {
	protected:
		const
		TCommandLine&   m_cmd_ln;
		CSysError       m_error;
		CAtlString      m_args;

	protected:
		CHandlerBase(const TCommandLine&, LPCTSTR lpszModule);
		~CHandlerBase(void);

	public:
		virtual LPCTSTR Args  (void)const;
		virtual HRESULT Handle(INT& _result);
		virtual bool    IsApplicable(void)const;
	public:
		TErrorRef       Error (void)const;
	};
}}}

#endif/*_FGUIAGENTHANDLERBASE_H_7B9037B5_12C4_479A_B453_891F24A19505_INCLUDED*/