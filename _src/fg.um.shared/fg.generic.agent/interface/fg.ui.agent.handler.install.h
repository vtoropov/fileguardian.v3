#ifndef _INSTALLERCMDHANDLER_H_F1CE3A36_B619_423E_BDA9_1DED8870773B_INCLUDED
#define _INSTALLERCMDHANDLER_H_F1CE3A36_B619_423E_BDA9_1DED8870773B_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Apr-2017 at 2:53:35a, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian Agent utility installer related command handler class declaration file.-
	-----------------------------------------------------------------------------
	Adopted to v15 on 5-Jun-2018 at 2:21:11p, UTC+7, Phuket, Rawai, Tuesday;
*/
#include "fg.ui.agent.handler.base.h"

namespace fg { namespace agent { namespace handlers
{
	using shared::lite::common::CSysError;

	class CInstallerCmdHandler : public CHandlerBase {

		typedef CHandlerBase  TBase;

	private:
		bool        b_dis_args;
	public:
		CInstallerCmdHandler(const TCommandLine&);

	public:
		HRESULT     Handle(INT& _result) override;
		bool        IsApplicable(void)const override;

	public:
		VOID        DisableTargetAppArgs(const bool);
	};
}}}

#endif/*_INSTALLERCMDHANDLER_H_F1CE3A36_B619_423E_BDA9_1DED8870773B_INCLUDED*/