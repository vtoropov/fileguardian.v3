#ifndef _LICENSECMDHANDLER_H_6A24C628_D23D_4e52_8579_498658ED20A7_INCLUDED
#define _LICENSECMDHANDLER_H_6A24C628_D23D_4e52_8579_498658ED20A7_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Apr-2016 at 9:40:40pm, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian Agent utility license related command handler class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 5-Jun-2018 at 3:05:54p, UTC+7, Phuket, Rawai, Tuesday;
*/
#include "fg.ui.agent.handler.base.h"

namespace fg { namespace agent { namespace handlers
{
	using shared::lite::common::CSysError;

	class CLicenseCmdHandler : public CHandlerBase {

		typedef CHandlerBase  TBase;

	public:
		CLicenseCmdHandler(const TCommandLine&);

	public:
		HRESULT     Handle(INT& _result) override;
		bool        IsApplicable(void)const override;
	};
}}}

#endif/*_LICENSECMDHANDLER_H_6A24C628_D23D_4e52_8579_498658ED20A7_INCLUDED*/