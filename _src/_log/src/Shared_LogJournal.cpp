/*
	Created by Tech_dog (VToropov) on 9-Oct-2015 at 2:57:48pm, GMT+7, Phuket, Rawai, Friday;
	This is Shared Log Library Application Event Journal wrapper class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 0:20:22a, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "Shared_LogJournal.h"

using namespace shared::log;

#include "Shared_GenericAppObject.h"
#include "Shared_GenericSyncObject.h"
#include "Shared_GenericHandle.h"
#include "generic.stg.data.h"

using namespace shared::lite::sync;
using namespace shared::lite::common;
using namespace shared::lite::data;

#include <strsafe.h>

using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace log { namespace details
{
	CGenericSyncObject& 
	           EvtJournal_GetSafeGuard(void)
	{
		static CGenericSyncObject guard_;
		return guard_;
	}

	LPCTSTR    EvtJournal_GetSourceName(void)
	{
		static CAtlString source_;
		if (source_.IsEmpty())
		{
			CApplication app_;
			source_ = app_.GetName();
		}
		return source_;
	}

	CAtlString EvtJournal_FormatMsg(LPCTSTR pszFormat, va_list& _args)
	{
		CAtlString msg_;
		size_t t_size = 0;

		try {

			HRESULT hr_ = S_OK;

			do
			{
				t_size += 2048;
				TCHAR* pszBuffer = new TCHAR[t_size];

				::memset(pszBuffer, 0, t_size * sizeof(TCHAR));

				hr_ = ::StringCchVPrintfEx(
								pszBuffer,
								t_size,
								NULL,
								NULL,
								0,
								pszFormat,
								_args
							);
				if (S_OK == hr_)
					msg_ = pszBuffer;
				if (pszBuffer)
				{
					delete pszBuffer; pszBuffer = NULL;
				}
			}
			while(STRSAFE_E_INSUFFICIENT_BUFFER == hr_);
		}
		catch (::std::bad_alloc&)
		{
		}
		return msg_;
	}

	VOID       EvtJournal_WriteEntry(LPCTSTR pszMessage, const WORD wType)
	{
		LPCWSTR lpszStrings[2] = { NULL, NULL };
		LPCTSTR pszEvtSource   = EvtJournal_GetSourceName();

		HANDLE hEventSource = ::RegisterEventSource(NULL, pszEvtSource);
		if (hEventSource)
		{
			lpszStrings[0] = pszEvtSource;
			lpszStrings[1] = pszMessage;
			::ReportEvent(
					hEventSource,
					wType,
					0,
					0,
					NULL,
					_countof(lpszStrings),
					0,
					lpszStrings,
					NULL
				);

			::DeregisterEventSource(hEventSource);
			hEventSource = NULL;
		}
	}

	bool&      EvtJournal_OutputToConsoleFlag(void)
	{
		static bool bOutputToConsole = false;
		return bOutputToConsole;
	}

	bool&      EvtJournal_VerboseModeRef(void)
	{
		static bool bVerboseMode = false;
		return bVerboseMode;
	}

	VOID       EvtJournal_OutputToConsoleMessage(const eLogType::_e _type, LPCTSTR lpszDetails)
	{
		if (!EvtJournal_VerboseModeRef() && eLogType::eError != _type)
			return;

		static LPCTSTR  lpszTimestampPattern = _T("%02d/%02d/%02d %02d:%02d:%02d.%03d");

		SYSTEMTIME st = { 0 };
		::GetLocalTime(&st);

		::ATL::CAtlString cs_timestamp;
		cs_timestamp.Format(
			lpszTimestampPattern,
			st.wMonth,
			st.wDay,
			st.wYear%100,  // only 2 digits
			st.wHour,
			st.wMinute,
			st.wSecond,
			st.wMilliseconds
		);

		::ATL::CAtlString cs_out;

		switch(_type)
		{
		case eLogType::eError  : cs_out.Format(_T("\n\t[ERROR][%s] %s"), cs_timestamp.GetString(), lpszDetails); break;
		case eLogType::eInfo   : cs_out.Format(_T("\n\t[INFO] [%s] %s"), cs_timestamp.GetString(), lpszDetails); break;
		case eLogType::eWarning: cs_out.Format(_T("\n\t[WARN] [%s] %s"), cs_timestamp.GetString(), lpszDetails); break;
		default:
			return;
		}
		::_tprintf(cs_out);
	}
}}}

#define SAFE_LOCK_JOURNAL() SAFE_LOCK(details::EvtJournal_GetSafeGuard());
/////////////////////////////////////////////////////////////////////////////

VOID  CEventJournal::LogEmptyLine(void) {
	if (details::EvtJournal_OutputToConsoleFlag()) {
		CAtlString cs_out(_T("\n\n"));
		::_tprintf(cs_out);
	}
}

VOID  CEventJournal::LogError(const CSysError& err_obj)
{
	SAFE_LOCK_JOURNAL();

	LPCTSTR lpszSeparator = (details::EvtJournal_OutputToConsoleFlag() ? _T("\n\t\t") : NULL);

	CAtlString details_ = err_obj.GetFormattedDetails(lpszSeparator);

	if (details::EvtJournal_OutputToConsoleFlag())
		details::EvtJournal_OutputToConsoleMessage(eLogType::eError, details_);
	else
		details::EvtJournal_WriteEntry(
					details_,
					EVENTLOG_ERROR_TYPE
				);
}

VOID  CEventJournal::LogError(LPCTSTR pszFormat, ...)
{
	SAFE_LOCK_JOURNAL();

	va_list  args_;
	va_start(args_, pszFormat);
	
	CAtlString msg_ = details::EvtJournal_FormatMsg(pszFormat, args_);

	va_end(args_);

	if (details::EvtJournal_OutputToConsoleFlag())
		details::EvtJournal_OutputToConsoleMessage(eLogType::eError, msg_);
	else
		details::EvtJournal_WriteEntry(
				msg_,
				EVENTLOG_ERROR_TYPE
			);
}

VOID  CEventJournal::LogInfoFromResource (const UINT nResId, ...)
{
	SAFE_LOCK_JOURNAL();

	CAtlString cs_msg;
	cs_msg.LoadString(nResId);
	if (cs_msg.IsEmpty())
		return;
	
	va_list  args_;
	va_start(args_, nResId);

	cs_msg = details::EvtJournal_FormatMsg(cs_msg, args_);

	va_end(args_);

	if (details::EvtJournal_OutputToConsoleFlag())
		details::EvtJournal_OutputToConsoleMessage(eLogType::eInfo, cs_msg.GetString());
	else
		details::EvtJournal_WriteEntry(
				cs_msg.GetString(),
				EVENTLOG_INFORMATION_TYPE
			);
}

VOID  CEventJournal::LogInfo (LPCTSTR pszFormat, ...)
{
	SAFE_LOCK_JOURNAL();

	va_list  args_;
	va_start(args_, pszFormat);
	
	CAtlString cs_msg = details::EvtJournal_FormatMsg(pszFormat, args_);

	va_end(args_);

	if (details::EvtJournal_OutputToConsoleFlag())
		details::EvtJournal_OutputToConsoleMessage(eLogType::eInfo, cs_msg.GetString());
	else
		details::EvtJournal_WriteEntry(
				cs_msg.GetString(),
				EVENTLOG_INFORMATION_TYPE
			);
}

VOID  CEventJournal::LogWarn (LPCTSTR pszFormat, ...)
{
	SAFE_LOCK_JOURNAL();

	va_list  args_;
	va_start(args_, pszFormat);
	
	CAtlString cs_msg = details::EvtJournal_FormatMsg(pszFormat, args_);

	va_end(args_);

	if (details::EvtJournal_OutputToConsoleFlag())
		details::EvtJournal_OutputToConsoleMessage(eLogType::eWarning, cs_msg.GetString());
	else
		details::EvtJournal_WriteEntry(
				cs_msg.GetString(),
				EVENTLOG_WARNING_TYPE
			);
}

VOID  CEventJournal::OutputToConsole(const bool _set)
{
	SAFE_LOCK_JOURNAL();

	details::EvtJournal_OutputToConsoleFlag() = _set;
}

VOID  CEventJournal::VerboseMode(const bool _set)
{
	SAFE_LOCK_JOURNAL();

	details::EvtJournal_VerboseModeRef() = _set;
}

namespace shared { namespace log
{
	VOID DumpToFile(LPCTSTR lpszFilePath, const _variant_t& vData)
	{
		if (VT_BSTR != vData.vt || NULL == vData.bstrVal)
			return;

		CAutoHandle hFile = ::CreateFile(
								lpszFilePath,
								GENERIC_WRITE,
								0,
								NULL,
								CREATE_ALWAYS,
								FILE_ATTRIBUTE_NORMAL,
								NULL
							);
		if (!hFile.IsValid())
			return;
		
		const DWORD n_data_len = ::SysStringByteLen(vData.bstrVal);
		CRawData raw_data(n_data_len);
		if (raw_data.IsValid())
		{
			const errno_t err_  = ::memcpy_s(raw_data.GetData(), raw_data.GetSize(), vData.bstrVal, n_data_len);
			if (0 == err_)
			{
				DWORD dwBytesWritten = 0;
				::WriteFile(
					hFile,
					raw_data.GetData(),
					raw_data.GetSize(),
					&dwBytesWritten,
					NULL
				);
			}
		}
	}
}}