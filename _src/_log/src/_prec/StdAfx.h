#ifndef _SHAREDLOGSTDAFX_H_DD5DA274_F341_4253_903C_61132700EE27_INCLUDED
#define _SHAREDLOGSTDAFX_H_DD5DA274_F341_4253_903C_61132700EE27_INCLUDED
/*
	Created by Tech_dog (VToropov) on 11-Jul-2015 at 11:52:59am, GMT+7, Phuket, Rawai, Saturday;
	This is Shared Log Library precompiled headers declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 0:24:50a, UTC+7, Phuket, Rawai, Monday;
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>

#endif/*_SHAREDLOGSTDAFX_H_DD5DA274_F341_4253_903C_61132700EE27_INCLUDED*/