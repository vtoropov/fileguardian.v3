#ifndef _SHAREDLOGJOURNAL_H_1871B7EC_A14A_4277_9775_8B33073C4AF1_INCLUDED
#define _SHAREDLOGJOURNAL_H_1871B7EC_A14A_4277_9775_8B33073C4AF1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Oct-2015 at 2:33:00pm, GMT+7, Phuket, Rawai, Friday;
	This is Shared Log Library Application Event Journal wrapper class declaration file.
*/
#include "Shared_LogCommonDefs.h"
#include "Shared_SystemError.h"

namespace shared { namespace log
{
	using shared::lite::common::CSysError;

	class CEventJournal
	{
	public:
		static VOID  LogEmptyLine(void);
		static VOID  LogError(const CSysError&);
		static VOID  LogError(LPCTSTR pszFormat, ...);
		static VOID  LogInfo (LPCTSTR pszFormat, ...);
		static VOID  LogWarn (LPCTSTR pszFormat, ...);
		static VOID  LogInfoFromResource(const UINT nResId, ...);
		// sets a flag to output all messages to host console window; it's used in debug mode;
		// it must be set on an application start once;
		static VOID  OutputToConsole(const bool);
		static VOID  VerboseMode(const bool);
	};

	VOID DumpToFile(LPCTSTR lpszFilePath, const _variant_t& vData);
}}

#endif/*_SHAREDLOGJOURNAL_H_1871B7EC_A14A_4277_9775_8B33073C4AF1_INCLUDED*/