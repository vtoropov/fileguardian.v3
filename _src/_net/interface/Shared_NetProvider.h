#ifndef _SHAREDNETPROVIDER_H_4C8586EF_3E54_40d9_ADB7_500AEB9867D3_INCLUDED
#define _SHAREDNETPROVIDER_H_4C8586EF_3E54_40d9_ADB7_500AEB9867D3_INCLUDED
/*
	Created by Tech_dog (VToropov) on 12-Feb-2016 at 6:53:15pm, GMT+7, Phuket, Rawai, Friday;
	This is Shared Net library internet provider class declaration file.
*/
#include "Shared_NetCommonDefs.h"

namespace shared { namespace net
{
	class CInternetProvider
	{
	private:
		mutable
		CInternetError     m_error;
		bool               m_bManaged;
		HINTERNET          m_session;
	public:
		CInternetProvider(void);
		~CInternetProvider(void);
	public:
		const CSysError&   Error(void)const;
		DWORD              GetConnectionLimit(void)const;
		HRESULT            Initialize(void);
		bool               IsInitialized(void)const;
		HINTERNET          Session(void)const;
		HRESULT            SetConnectionLimit(const DWORD);
		HRESULT            Terminate(void);
	public:
		static HRESULT     IsAccessible(void);
	public:
		CInternetProvider(const CInternetProvider&);
	private:
		CInternetProvider& operator= (const CInternetProvider&);
	};
}}

#endif/*_SHAREDNETPROVIDER_H_4C8586EF_3E54_40d9_ADB7_500AEB9867D3_INCLUDED*/