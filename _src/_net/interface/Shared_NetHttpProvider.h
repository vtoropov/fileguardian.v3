#ifndef _SHAREDHTTPPROVIDER_H_0A3D8248_44A0_4f2f_9E05_9AF15D09021B_INCLUDED
#define _SHAREDHTTPPROVIDER_H_0A3D8248_44A0_4f2f_9E05_9AF15D09021B_INCLUDED
/*
	Created by Tech_dog (VToropov) on 11-Jan-2016 at 7:07:39pm, GMT+7, Phuket, Rawai, Monday;
	This is Shared Net library HTTP request provider class declaration file.
*/
#include "Shared_SystemError.h"
#include "httprequest.h"
#include <wininet.h>

namespace shared { namespace net
{
	using shared::lite::common::CSysError;

	class CHttpProvider
	{
		typedef ::ATL::CComPtr<IWinHttpRequest> TService;
	private:
		CSysError        m_error;
		TService         m_pWebService;
		bool             m_bInitialized;
	public:
		CHttpProvider(void);
		~CHttpProvider(void);
	public:
		HRESULT          DoGetRequest (LPCTSTR lpszURL, CAtlString& _response);
		HRESULT          DoPostRequest(LPCTSTR lpszURL, CAtlString& _response, const CAtlString& _data);
		TErrorRef        Error(void)const;
		HRESULT          Initialize(void);
		HRESULT          Terminate(void);
	};
}}

#endif/*_SHAREDHTTPPROVIDER_H_0A3D8248_44A0_4f2f_9E05_9AF15D09021B_INCLUDED*/