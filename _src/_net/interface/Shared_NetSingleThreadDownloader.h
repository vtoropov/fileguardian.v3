#ifndef _SHAREDNETSINGLETHREADDOWNLOADER_H_90CC51FC_1C02_4C4A_8030_D72E6C18C022_INCLUDED
#define _SHAREDNETSINGLETHREADDOWNLOADER_H_90CC51FC_1C02_4C4A_8030_D72E6C18C022_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Apr-2017 at 11:18:11p, UTC+7, Phuket, Rawai, Monday;
	This is Shared Net Library single threaded downloader class declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_SystemThreadPool.h"

namespace shared { namespace net
{
	using shared::lite::common::CSysError;
	using shared::lite::sys_core::CThreadBase;

	interface INetDownloaderCallback
	{
		virtual bool    INetDownloader_OnCanContinue (void) PURE;
		virtual void    INetDownloader_OnBegin (void) PURE;
		virtual void    INetDownloader_OnFinish(void) PURE;
		virtual void    INetDownloader_OnDataReceived(const DWORD dwReceived, const DWORD dwTotal) PURE;
		virtual void    INetDownloader_OnError (CSysError) PURE;
		virtual void    INetDownloader_OnInterrupt(void) PURE;
	};

	class CNetSingleThreadDownloader : 
		public  CThreadBase
	{
		typedef CThreadBase TBase;
	private:
		INetDownloaderCallback& m_sink;
		CAtlString      m_source;    // the source URL, which data is downloaded from;
		CAtlString      m_target;    // the full path to file that is saved on hard drive;
	public:
		CNetSingleThreadDownloader(INetDownloaderCallback&);
		~CNetSingleThreadDownloader(void);
	public:
		virtual HRESULT Start(void) override sealed;
	public:
		VOID            SourceURL (LPCTSTR lpszURL);
		VOID            TargetFile(LPCTSTR lpszTargetFilePath);
	private:
		virtual VOID    ThreadFunction(VOID) override sealed;
	};
}}

#endif/*_SHAREDNETSINGLETHREADDOWNLOADER_H_90CC51FC_1C02_4C4A_8030_D72E6C18C022_INCLUDED*/