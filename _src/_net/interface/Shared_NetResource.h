#ifndef _SHAREDNETRESOURCE_H_24E837C0_061A_4ac4_95DB_024A06417DFC_INCLUDED
#define _SHAREDNETRESOURCE_H_24E837C0_061A_4ac4_95DB_024A06417DFC_INCLUDED
/*
	Created by Tech_dog (VToropov) on 12-Feb-2016 at 7:02:17pm, GMT+7, Phuket, Rawai, Friday;
	This is Shared Net Library Internet Resource class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2016 at 9:55:02p, UTC+7, Phuket, Rawai, Monday;
*/
#include "Shared_NetCommonDefs.h"
#include "Shared_NetProvider.h"

#include "generic.stg.data.h"

namespace shared { namespace net
{
	using shared::lite::data::CRawData;

	class CInternetResource
	{
	private:
		mutable
		CInternetError     m_error;
		const
		CInternetProvider& m_provider;
		HINTERNET          m_connect;
		mutable HINTERNET  m_request;
		CAtlString         m_url;
		bool               m_bManaged;
		mutable
		TResponseHeaders   m_headers;
		CInternetOptions   m_options;
	public:
		CInternetResource(const CInternetProvider&, LPCTSTR pURI, const DWORD dwOptions = eInternetOption::eNone);
		~CInternetResource(void);
	public:
		const CSysError&   Error(void)const;
		HRESULT            GetContentLength(DWORD& dwLength)const;
		LONG               GetLastStatusCode(void)const;
		::ATL::CAtlString  GetLastStatusText(void)const;
		const
		TResponseHeaders&  GetResponseHeaders(void)const;
		HRESULT            IsRangeSupported(bool& bResult)const;
		bool               IsValid(void)const;
		HRESULT            Load(CRawData&)const;                    // loads the resource to the byte array
		HRESULT            Save(LPCTSTR pszDestFile,
		                        IInternetProviderCallback* = NULL
		                     );                                     // saves the resource to the file specified
		HRESULT            Save(const HANDLE hFile, 
		                        const DWORD dwPosition,
		                        const DWORD dwBytesToWrite,
		                        IInternetProviderCallback* = NULL
		                     );                                     // writes data to file, the file position is already set properly by callee
		VOID               UseOneRequest(const bool);               // forces to use one/multiple request(s)
		LPCTSTR            URL(void)const;
	public:
		CInternetResource(const CInternetResource&);
	private:
		CInternetResource& operator= (const CInternetResource&);
	};
}}

#endif/*_SHAREDNETRESOURCE_H_24E837C0_061A_4ac4_95DB_024A06417DFC_INCLUDED*/