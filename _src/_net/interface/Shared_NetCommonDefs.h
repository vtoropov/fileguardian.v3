#ifndef _SHAREDNETCOMMONDEFS_H_3479EC44_275D_4949_AE4B_3A6034E41A55_INCLUDED
#define _SHAREDNETCOMMONDEFS_H_3479EC44_275D_4949_AE4B_3A6034E41A55_INCLUDED
/*
	Created by Tech_dog (VToropov) on 26-Oct-2015 at 1:16:01pm, GMT+7, Phuket, Rawai, Monday;
	This is Shared Net library common definitions declaration file.
*/
#include <Wininet.h>
#pragma comment(lib, "Wininet.lib")

#include "Shared_SystemError.h"

#define HTTP_STATUS_ERROR (HTTP_STATUS_REDIRECT_KEEP_VERB + 1)

namespace shared { namespace net
{
	interface IInternetProviderCallback
	{
		virtual bool   InternetProvider_CanContinue(void) PURE;
		virtual void   InternetProvider_DataReceived(const DWORD dwDataSize) PURE;
		virtual void   InternetProvider_DownloadFinished(void) PURE;
		virtual void   InrernetProvider_DownloadStarted(void) PURE;
	};

	using shared::lite::common::CSysError;

	class CInternetError : public CSysError
	{
		typedef CSysError TBaseError;
	public:
		explicit CInternetError(void);
		explicit CInternetError(const DWORD dwError);
		explicit CInternetError(const HRESULT);
	public:
		CInternetError&    operator= (const DWORD);
		CInternetError&    operator= (const HRESULT);
		CInternetError&    operator= (const CSysError&);
	};

	class eInternetOption
	{
	public:
		enum _e{
			eNone        = 0x0,
			eOneRequest  = 0x1,
			eAcceptGzip  = 0x2,
		};
	};

	class CInternetOptions
	{
	private:
		DWORD              m_dwOptions;
	public:
		CInternetOptions(const DWORD dwOptions = eInternetOption::eNone);
		~CInternetOptions(void);
	public:
		VOID               Clear(void);
		bool               IsGzipAccept(void)const;
		bool               IsOneRequest(void)const;
		HRESULT            Modify(const DWORD dwOptions, const bool bSet);
	};

	typedef ::std::map<::ATL::CAtlString, ::ATL::CAtlString> TResponseHeaders;

}}

#endif/*_SHAREDNETCOMMONDEFS_H_3479EC44_275D_4949_AE4B_3A6034E41A55_INCLUDED*/