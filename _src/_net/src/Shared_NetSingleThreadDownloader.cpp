/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Apr-2017 at 11:56:48p, UTC+7, Phuket, Rawai, Monday;
	This is Shared Net Library single threaded downloader class implementation file.
*/
#include "StdAfx.h"
#include "Shared_NetSingleThreadDownloader.h"

using namespace shared::net;

#include "Shared_SystemCore.h"
#include "Shared_FS_GenericFile.h"

using namespace shared::lite::sys_core;
using namespace shared::ntfs;

#include "Shared_NetProvider.h"
#include "Shared_NetResource.h"

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace net { namespace details
{
	class INetDownloader_EventAdapter : public IInternetProviderCallback
	{
	private:
		INetDownloaderCallback& m_callback;
		DWORD                   m_received;
		DWORD                   m_total;
	public:
		INetDownloader_EventAdapter(INetDownloaderCallback& _callback, const DWORD _total) : 
		m_callback(_callback), m_received(0), m_total(_total) {}
	private: // IInternetProviderCallback
		virtual bool   InternetProvider_CanContinue(void) override sealed
		{
			return m_callback.INetDownloader_OnCanContinue();
		}

		virtual void   InternetProvider_DataReceived(const DWORD dwDataSize) override sealed
		{
			m_received += dwDataSize;
			m_callback.INetDownloader_OnDataReceived(m_received, m_total);
		}

		virtual void   InternetProvider_DownloadFinished(void) override sealed
		{
			m_callback.INetDownloader_OnFinish();
		}

		virtual void   InrernetProvider_DownloadStarted(void) override sealed
		{
			m_received = 0;
			m_callback.INetDownloader_OnBegin();
		}
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CNetSingleThreadDownloader::CNetSingleThreadDownloader(INetDownloaderCallback& _sink): m_sink(_sink)
{
	TBase::m_error.Source(_T("CNetSingleThreadDownloader"));
}

CNetSingleThreadDownloader::~CNetSingleThreadDownloader(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT CNetSingleThreadDownloader::Start(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (TBase::IsRunning()){
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("The download process is in progress!")
			);
		return m_error;
	}

	if (m_source.IsEmpty()){
		m_error.SetState(
				E_INVALIDARG,
				_T("Source URL cannot be empty!")
			);
		return m_error;
	}

	if (m_target.IsEmpty()){
		m_error.SetState(
				E_INVALIDARG,
				_T("Target file cannot be empty!")
			);
		return m_error;
	}
	return TBase::Start();
}

/////////////////////////////////////////////////////////////////////////////

VOID    CNetSingleThreadDownloader::SourceURL (LPCTSTR lpszURL)
{
	m_source = lpszURL;
}

VOID    CNetSingleThreadDownloader::TargetFile(LPCTSTR lpszTargetFilePath)
{
	m_target = lpszTargetFilePath;
}

/////////////////////////////////////////////////////////////////////////////

VOID    CNetSingleThreadDownloader::ThreadFunction(VOID)
{
	CCoInitializer com_core(false);
	if (com_core.IsSuccess() == false){
		this->m_sink.INetDownloader_OnError(com_core.Error());
		return;
	}

	INetDownloaderCallback& callback_ = this->m_sink;
	bool bCompleted = false;

	::Sleep(100);
	do {
		CInternetProvider inet_;
		HRESULT hr_ = inet_.Initialize();
		if (FAILED(hr_)){
			callback_.INetDownloader_OnError(inet_.Error());
			break;
		}
		if (TBase::m_crt.IsStopped())
			break;

		CInternetResource res_(inet_, m_source, eInternetOption::eOneRequest);
		if (!res_.IsValid()){
			callback_.INetDownloader_OnError(res_.Error());
			break;
		}
		if (TBase::m_crt.IsStopped())
			break;

		DWORD size_ = 0;
		hr_ = res_.GetContentLength(size_);
		if (FAILED(hr_)){
			callback_.INetDownloader_OnError(res_.Error());
			break;
		}
		if (TBase::m_crt.IsStopped())
			break;

		details::INetDownloader_EventAdapter adapter_(callback_, size_);
		hr_ = res_.Save(
				this->m_target, &adapter_
			);
		if (FAILED(hr_)){
			callback_.INetDownloader_OnError(res_.Error());
			break;
		}
		if (TBase::m_crt.IsStopped())
			break;

		static const DWORD expected_ = 1024 * 1024;
		if (size_ < expected_){
			TBase::m_error.SetState(
					(DWORD)ERROR_INVALID_DATA,
					_T("Unexpected end of file! Download failed.")
				);
			callback_.INetDownloader_OnError(TBase::m_error);
			break;
		}

		bCompleted = true;
	} while (false);

	if (bCompleted)
		callback_.INetDownloader_OnFinish();
	else
		callback_.INetDownloader_OnInterrupt();

	::SetEvent(m_crt.EventObject());
}