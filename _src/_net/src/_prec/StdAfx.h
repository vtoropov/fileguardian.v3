#ifndef _SHAREDNETSTDAFX_H_D8061509_9F1B_4e4d_ABA3_B26B8082FFB6_INCLUDED
#define _SHAREDNETSTDAFX_H_D8061509_9F1B_4e4d_ABA3_B26B8082FFB6_INCLUDED
/*
	Created by Tech_dog (VToropov) on 26-Oct-2015 at 1:04:04pm, GMT+7, Phuket, Rawai, Monday;
	This is Shared Net Library precompiled headers declaration file.
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>
#include <comutil.h>
#include <vector>
#include <map>

#endif/*_SHAREDNETSTDAFX_H_D8061509_9F1B_4e4d_ABA3_B26B8082FFB6_INCLUDED*/