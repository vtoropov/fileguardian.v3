/*
	Created by Tech_dog (VToropov) on 12-Feb-2016 at 7:06:59pm, GMT+7, Phuket, Rawai, Friday;
	This is Shared Net Library Internet Resource class implementation file.
*/
#include "StdAfx.h"
#include "Shared_NetResource.h"
#include "Shared_GenericHandle.h"

using namespace shared::net;
using namespace shared::lite::common;

#include <atlutil.h>
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace net { namespace details
{
	CAtlString InternetResource_ReqHeader(const CInternetOptions& opts)
	{
		CAtlString cs_header;
		if (opts.IsGzipAccept()) cs_header += _T("Accept-Encoding: gzip\r\n");
		return cs_header;
	}

	HRESULT    InternetResource_GetResponseHeaders(HINTERNET hRequest, TResponseHeaders& headers)
	{
		if (!hRequest)
			return E_INVALIDARG;
		CRawData raw_headers(256);
		if (!raw_headers.IsValid())
			return raw_headers.Error().GetHresult();

		BOOL bResult = FALSE;
		DWORD dwBufferLen  = raw_headers.GetSize();
		do
		{
			bResult = ::HttpQueryInfo(hRequest, HTTP_QUERY_RAW_HEADERS_CRLF, (LPVOID)raw_headers.GetData(), &dwBufferLen, NULL);
			if (!bResult)
			{
				const DWORD dwError = ::GetLastError();
				if (ERROR_INSUFFICIENT_BUFFER != dwError) return HRESULT_FROM_WIN32(dwError);
				// re-creates the buffer of the new size
				raw_headers.Create(dwBufferLen);
				if (!raw_headers.IsValid())
					return raw_headers.Error().GetHresult();
			}
		} while (!bResult);

		try
		{
			if (!headers.empty())headers.clear();

			::ATL::CAtlString cs_headers = reinterpret_cast<LPCTSTR>(raw_headers.GetData());
			if (cs_headers.IsEmpty())
				return S_FALSE;
			INT n_pos = 0;
			::ATL::CAtlString cs_pair = cs_headers.Tokenize(_T("\r\n"), n_pos);

			while (!cs_pair.IsEmpty())
			{
				const INT n_sep = cs_pair.Find(_T(":"));
				if (-1 != n_sep)
				{
					::ATL::CAtlString cs_key = cs_pair.Left(n_sep);
					::ATL::CAtlString cs_val = cs_pair.Right(cs_pair.GetLength() - n_sep - 1);
					if (!cs_key.IsEmpty())cs_key.Trim();
					if (!cs_val.IsEmpty())cs_val.Trim();
					headers.insert(::std::make_pair(cs_key, cs_val));
				}
				else
					headers[cs_pair] = CAtlString();
				cs_pair = cs_headers.Tokenize(_T("\r\n"), n_pos);
			}

		} catch (::std::bad_alloc&){ return E_OUTOFMEMORY; }
		return  S_OK;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CInternetResource::CInternetResource(const CInternetProvider& provider, LPCTSTR pURI, const DWORD dwOptions) : 
	m_provider(provider), m_url(pURI), m_request(NULL), m_connect(NULL), m_options(dwOptions)
{
	if (!m_provider.IsInitialized())
	{
		m_error = OLE_E_BLANK;
		return;
	}
	if (m_url.IsEmpty())
	{
		m_error = E_INVALIDARG;
		return;
	}

	if (!m_options.IsOneRequest())
	{
		::ATL::CUrl url_obj;
		url_obj.CrackUrl(m_url);
		m_connect = ::InternetConnect(
				m_provider.Session(),
				url_obj.GetHostName(),
				url_obj.GetPortNumber(),
				NULL,
				NULL,
				INTERNET_SERVICE_HTTP,
				0,
				NULL
			);
		if (!m_connect)
		{
			m_error = ::GetLastError();
			return;
		}

		m_request = ::HttpOpenRequest(m_connect, _T("HEAD"), url_obj.GetUrlPath(), NULL, NULL, NULL, 0, NULL);
		if (false){}
		else if (!m_request) m_error = ::GetLastError();
		else if (!::HttpSendRequest(m_request, NULL, 0, NULL, 0))
		{
			m_error = ::GetLastError();
			m_error.Clear();
		}
		else
			m_error.Clear();
	}
	else
	{
		::ATL::CAtlString cs_header = details::InternetResource_ReqHeader(m_options);
		m_request = ::InternetOpenUrl(
					m_provider.Session(),
					m_url,
					cs_header,
					cs_header.GetLength(),
					INTERNET_FLAG_NO_CACHE_WRITE,
					NULL
				);
		if (!m_request)
			m_error = ::GetLastError();
		else
			m_error.Clear();
	}
	m_bManaged = true;
}

CInternetResource::~CInternetResource(void)
{
	if (m_request && m_bManaged)
		::InternetCloseHandle(m_request); m_request = NULL;
	if (m_connect && m_bManaged)
		::InternetCloseHandle(m_connect); m_connect = NULL;
	 m_bManaged = false;
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef  CInternetResource::Error(void)const
{
	return m_error;
}

HRESULT    CInternetResource::GetContentLength(DWORD& dwLength)const
{
	if (!this->IsValid())
		return OLE_E_BLANK;
	m_error.Clear();

	DWORD dwBufferLen  = sizeof(DWORD);
	const BOOL bResult = ::HttpQueryInfo(
			m_request,
			HTTP_QUERY_CONTENT_LENGTH | HTTP_QUERY_FLAG_NUMBER,
			(LPVOID)&dwLength,
			&dwBufferLen,
			NULL
		);
	if (!bResult)
		m_error = ::GetLastError();
	return m_error;
}

LONG       CInternetResource::GetLastStatusCode(void)const
{
	if (!m_request)
		return -1;
	m_error.Clear();
	LONG  lCode = 0;
	DWORD dwBufferLen  = sizeof(LONG);
	const BOOL bResult = ::HttpQueryInfo(
			m_request,
			HTTP_QUERY_STATUS_CODE | HTTP_QUERY_FLAG_NUMBER,
			(LPVOID)&lCode,
			&dwBufferLen,
			NULL
		);
	if (!bResult)
		m_error = ::GetLastError();
	return lCode;
}

CAtlString CInternetResource::GetLastStatusText(void)const
{
	::ATL::CAtlString cs_status;
	if (!m_request)
		return cs_status;
	m_error.Clear();
	TCHAR szStatus[INTERNET_MAX_PATH_LENGTH] = {0};
	DWORD dwBufferLen  = _countof(szStatus) * sizeof(TCHAR);
	const BOOL bResult = ::HttpQueryInfo(m_request, HTTP_QUERY_STATUS_TEXT, (LPVOID)szStatus, &dwBufferLen, NULL);
	if (!bResult)
		m_error = ::GetLastError();
	else
		cs_status = szStatus;
	return cs_status;
}

const TResponseHeaders&
           CInternetResource::GetResponseHeaders(void)const
{
	return m_headers;
}

HRESULT    CInternetResource::IsRangeSupported(bool& bResult)const
{
	if (!this->IsValid())
		return OLE_E_BLANK;
	m_error.Clear();
	bResult = false;

	TCHAR tcBuffer[40]={0};
	DWORD dwBufferLen = _countof(tcBuffer);
	const BOOL bLocal = ::HttpQueryInfo(m_request, HTTP_QUERY_ACCEPT_RANGES, (LPVOID)&tcBuffer, &dwBufferLen, NULL);
	if (!bLocal)
		m_error = ::GetLastError();
	else
	{
		CAtlString cs_result(tcBuffer);
		bResult = (cs_result.CompareNoCase(_T("bytes")) == 0);
	}
	return m_error;
}

bool       CInternetResource::IsValid(void)const
{
	return (m_provider.IsInitialized() && !m_url.IsEmpty());
}

HRESULT    CInternetResource::Load(CRawData& load_data) const
{
	if (!this->IsValid())
		return OLE_E_BLANK;
	m_error.Clear();

	const DWORD dwChunkSize = 1024;
	CRawData raw_data(dwChunkSize);
	if (!raw_data.IsValid())
		return (m_error = raw_data.Error());

	HINTERNET hRequest = NULL;
	const bool bOneReq = m_options.IsOneRequest();

	if (bOneReq)
		hRequest = m_request;
	else
	{
		CAtlString cs_header = details::InternetResource_ReqHeader(m_options);
		hRequest = ::InternetOpenUrl(
					m_provider.Session(),
					m_url,
					cs_header,
					cs_header.GetLength(),
					INTERNET_FLAG_NO_CACHE_WRITE,
					NULL
				);
		if (!hRequest)
		{
			m_error = ::GetLastError();
			return m_error;
		}
	}
	DWORD dwBytesToRead = 0;
	do
	{
		if (!::InternetReadFile(hRequest, raw_data.GetData(), raw_data.GetSize(), &dwBytesToRead))
		{
			m_error = ::GetLastError();
			break;
		}
		if (!dwBytesToRead) // EOF
			break;
		m_error = load_data.Append(raw_data.GetData(), dwBytesToRead);
		if (m_error)
			break;
	}
	while(true);
	if (hRequest)
		details::InternetResource_GetResponseHeaders(hRequest, m_headers);
	if (!bOneReq)
		::InternetCloseHandle(hRequest); hRequest = NULL;
	return m_error;
}

HRESULT    CInternetResource::Save(LPCTSTR pszDestFile, IInternetProviderCallback* pCallback)
{
	m_error = S_OK;

	CAutoHandle hFile = ::CreateFile(
							pszDestFile,
							FILE_APPEND_DATA,
							FILE_SHARE_READ ,
							NULL,
							CREATE_ALWAYS,
							FILE_ATTRIBUTE_NORMAL,
							NULL
						);
	if (!hFile.IsValid())
		return (m_error = ::GetLastError());

	const DWORD dwChunkSize = 2048;
	CRawData raw_data(dwChunkSize);
	if (!raw_data.IsValid())
		return (m_error = raw_data.Error());

	if (pCallback)
		pCallback->InrernetProvider_DownloadStarted();

	DWORD dwBytesToRead    = 0;
	DWORD dwBytesWritten   = 0;
	do
	{
		if (pCallback && !pCallback->InternetProvider_CanContinue())
			break;
		if (!::InternetReadFile(m_request, raw_data.GetData(), raw_data.GetSize(), &dwBytesToRead))
		{
			m_error = ::GetLastError();
			break;
		}
		if (!dwBytesToRead) // EOF
			break;
		if (pCallback && !pCallback->InternetProvider_CanContinue())
			break;
		if (!::WriteFile(hFile, raw_data.GetData(), dwBytesToRead, &dwBytesWritten, NULL))
		{
			m_error = ::GetLastError();
			break;
		}
		if (pCallback && !pCallback->InternetProvider_CanContinue())
			break;
		if (pCallback)
			pCallback->InternetProvider_DataReceived(dwBytesToRead);
	}
	while(true);
	if (!m_error)
	{
		if (pCallback)
			pCallback->InternetProvider_DownloadFinished();
	}

	return m_error;
}

HRESULT    CInternetResource::Save(const HANDLE hFile, const DWORD dwPosition, const DWORD dwBytesToWrite, IInternetProviderCallback* pCallback)
{
	if (!this->IsValid())
		return OLE_E_BLANK;

	const bool bOneReq = m_options.IsOneRequest();

	m_error = S_OK;
	if (false){}
	else if (INVALID_HANDLE_VALUE == hFile)m_error = ERROR_INVALID_HANDLE;
	else if (!dwBytesToWrite && !bOneReq)m_error = E_INVALIDARG;

	if (m_error)
		return m_error;

	if (pCallback && !pCallback->InternetProvider_CanContinue())
		return m_error;

	::ATL::CAtlString cs_header;
	HINTERNET hRequest = NULL;

	if (!bOneReq)
	{
		cs_header.Format(_T("Range:bytes=%d-%d"), dwPosition, dwPosition + dwBytesToWrite - 1); // zero based range

		hRequest = ::InternetOpenUrl(
				m_provider.Session(),
				m_url,
				cs_header,
				cs_header.GetLength(),
				INTERNET_FLAG_NO_CACHE_WRITE,
				NULL
			);
		if (!hRequest)
		{
			m_error = ::GetLastError();
			return m_error;
		}
	}
	else
		hRequest = m_request;
	
	const DWORD dwChunkSize = 1024;
	CRawData raw_data(dwChunkSize);
	if (!raw_data.IsValid())
		return (m_error = raw_data.Error());

	DWORD dwBytesToRead    = 0;
	DWORD dwBytesWritten   = 0;
	do
	{
		if (pCallback && !pCallback->InternetProvider_CanContinue())
			break;
		if (!::InternetReadFile(hRequest, raw_data.GetData(), raw_data.GetSize(), &dwBytesToRead))
		{
			m_error = ::GetLastError();
			break;
		}
		if (!dwBytesToRead) // EOF
			break;
		if (pCallback && !pCallback->InternetProvider_CanContinue())
			break;
		if (!::WriteFile(hFile, raw_data.GetData(), dwBytesToRead, &dwBytesWritten, NULL))
		{
			m_error = ::GetLastError();
			break;
		}
		if (pCallback && !pCallback->InternetProvider_CanContinue())
			break;
		if (pCallback)
			pCallback->InternetProvider_DataReceived(dwBytesToRead);
	}
	while(true);
	if (!m_error && !bOneReq)
	{
		if (pCallback)
			pCallback->InternetProvider_DownloadFinished();
	}
	if (!bOneReq)
		::InternetCloseHandle(hRequest); hRequest = NULL;
	return m_error;
}

LPCTSTR    CInternetResource::URL(void)const
{
	return m_url.GetString();
}

VOID       CInternetResource::UseOneRequest(const bool bSet)
{
	const bool bChanged = (S_OK == m_options.Modify(eInternetOption::eOneRequest, bSet));
	if (!bChanged)
		return;

	const bool bOneReq = m_options.IsOneRequest();

	if (!bOneReq || !m_bManaged)
		return;
	if (m_request)
		::InternetCloseHandle(m_request);

	CAtlString cs_header = details::InternetResource_ReqHeader(m_options);
	m_request = ::InternetOpenUrl(
					m_provider.Session(),
					m_url,
					cs_header,
					cs_header.GetLength(),
					INTERNET_FLAG_NO_CACHE_WRITE,
					NULL
				);
	if (m_request)
		m_error.Clear();
	else {
		m_error = ::GetLastError();
	}
}

/////////////////////////////////////////////////////////////////////////////

CInternetResource::CInternetResource(const CInternetResource& _res_ref) : 
m_provider(_res_ref.m_provider), m_options(_res_ref.m_options)
{
	this->m_bManaged  = false;
	this->m_request   = _res_ref.m_request;
	this->m_connect   = _res_ref.m_connect;
	this->m_error     = _res_ref.m_error;
	this->m_url       = _res_ref.m_url;
}