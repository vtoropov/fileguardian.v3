/*
	Created by Tech_dog (VToropov) on 11-Jan-2016 at 7:14:14pm, GMT+7, Phuket, Rawai, Monday;
	This is Shared net common library HTTP request provider class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 9:52:06p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "Shared_NetHttpProvider.h"

using namespace shared::net;

#include "Shared_Unicode.h"

using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace net { namespace details
{
	VOID WebProvider_FormatResponseError(const LONG _code, const _bstr_t& _details, CSysError& _error)
	{
		if (_details.length())
		{
			_error.SetState(
					(DWORD)ERROR_REQUEST_ABORTED,
					_T("HTTP service error: code=%d; desc=%s"),
					_code,
					static_cast<LPCTSTR>(_details)
				);
		}
		else
		{
			_error.SetState(
					(DWORD)ERROR_REQUEST_ABORTED,
					_T("HTTP service error: code=%d; desc=no details"),
					_code
				);
		}
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CHttpProvider::CHttpProvider(void): m_bInitialized(false)
{
	m_error.Source(_T("CHttpProvider"));
}

CHttpProvider::~CHttpProvider(void)
{
	if (m_bInitialized)
		this->Terminate();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CHttpProvider::DoGetRequest (LPCTSTR lpszURL, CAtlString& _response)
{
	m_error.Module(_T(__FUNCTION__));

	if (!lpszURL || ::_tcslen(lpszURL) < 7)
		return (m_error = E_INVALIDARG);
	
	m_error = S_OK;
	try
	{
		if (!m_pWebService)
			return (m_error = OLE_E_BLANK);
		m_error = m_pWebService->Open(_bstr_t(_T("GET")), _bstr_t(lpszURL), _variant_t((bool)false));
		if (!m_error)
		{
			m_error = m_pWebService->SetRequestHeader(_bstr_t(_T("Content-Type")), _bstr_t(_T("application/xml")));
			if (!m_error)
			{
				m_error = m_pWebService->Send(_variant_t(_T("")));
				if (!m_error)
				{
					::ATL::CComBSTR bsResponse;
					m_error = m_pWebService->get_ResponseText(&bsResponse);
					if (!m_error)
						_response = bsResponse;
				}
			}
		}
	}
	catch(_com_error& err_ref)
	{
		m_error = err_ref.Error();
	}
	return m_error;
}

HRESULT    CHttpProvider::DoPostRequest(LPCTSTR lpszURL, CAtlString& _response, const CAtlString& _data)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!lpszURL || ::_tcslen(lpszURL) < 7)
		return (m_error = E_INVALIDARG);

	try
	{
		if (!m_pWebService)
			return (m_error = OLE_E_BLANK);

		HRESULT hr_ = m_pWebService->Open(
								_bstr_t(_T("POST")),
								_bstr_t(lpszURL),
								_variant_t((bool)false)
							);
		if (FAILED(hr_))
			return (m_error = hr_);
		
		hr_ = m_pWebService->SetRequestHeader(
								_bstr_t(_T("Content-Type")),
								_bstr_t(_T("application/x-www-form-urlencoded"))
							);
		if (FAILED(hr_))
			return (m_error = hr_);

		CUTF8 utf8_;
		_variant_t v_post = utf8_.ConvertWcsToMbs(_data);
#if (0)
		shared::log::DumpToFile(
				_T("e:\\post_dump.bin"),
				v_post
			);
#endif
		hr_ = m_pWebService->Send(v_post);
		if (FAILED(hr_))
			return (m_error = hr_);

		LONG lCode = 0;
		hr_ = m_pWebService->get_Status(&lCode);
		if (HTTP_STATUS_OK != lCode)
		{
			_bstr_t details_;
			hr_ = m_pWebService->get_StatusText(details_.GetAddress());
			details::WebProvider_FormatResponseError(
					lCode,
					details_,
					m_error
				);
			return m_error;
		}

		_bstr_t response_;
		hr_ = m_pWebService->get_ResponseText(response_.GetAddress());
		if (!FAILED(hr_))
			_response = static_cast<LPCTSTR>(response_);
	}
	catch(_com_error& err_ref)
	{
		m_error = err_ref.Error();
	}
	return m_error;
}

TErrorRef  CHttpProvider::Error(void)const
{
	return m_error;
}

HRESULT    CHttpProvider::Initialize(void)
{
	m_error.Module(_T(__FUNCTION__));

	if (m_bInitialized)
		return (m_error = S_OK);

	m_error = S_OK;
	try
	{
		m_error = m_pWebService.CoCreateInstance(_T("WinHttp.WinHttpRequest.5.1"));
	}
	catch(_com_error& err_)
	{
		m_error = err_.Error();
		return m_error;
	}
	m_bInitialized = !m_error;
	return  m_error;
}

HRESULT    CHttpProvider::Terminate(void)
{
	m_error.Module(_T(__FUNCTION__));

	if (!m_bInitialized)
		return (m_error = S_OK);

	try
	{
		m_pWebService = NULL;
	}
	catch(_com_error& err_ref)
	{
		return err_ref.Error();
	}

	m_bInitialized = false;
	m_error = OLE_E_BLANK;
	return S_OK;
}