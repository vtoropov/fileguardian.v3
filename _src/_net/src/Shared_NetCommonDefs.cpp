/*
	Created by Tech_dog (VToropov) on 26-Oct-2015 at 1:23:59pm, GMT+7, Phuket, Rawai, Monday;
	This is Shared Net library common definitions implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 9:47:38p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "Shared_NetCommonDefs.h"

using namespace shared;
using namespace shared::net;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace net { namespace details
{
	CAtlString InetProvider_FormatMessage(const DWORD dwError)
	{
		TCHAR szBuffer[_MAX_PATH] = {0};

		HMODULE hModule = ::GetModuleHandle(_T("Wininet.dll"));

		::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_FROM_HMODULE,
						hModule,
						dwError,
						MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL),
						szBuffer,
						_MAX_PATH - 1,
						NULL);
		::ATL::CAtlString msg_(szBuffer);
		return msg_;
	}

	CAtlString InetProvider_GetLastResponseInfo(void)
	{
		::ATL::CAtlString cs_info;
		return cs_info;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CInternetError::CInternetError(void) : TBaseError()
{
}

CInternetError::CInternetError(const DWORD dwError) : TBaseError(dwError)
{
}

CInternetError::CInternetError(const HRESULT hError) : TBaseError(hError)
{
}

/////////////////////////////////////////////////////////////////////////////

CInternetError&    CInternetError::operator= (const DWORD dwError)
{
	::ATL::CAtlString cs_error = details::InetProvider_FormatMessage(dwError);
	if (ERROR_INTERNET_EXTENDED_ERROR == dwError)
	{
		::ATL::CAtlString cs_ex_info = details::InetProvider_GetLastResponseInfo();
		if (!cs_ex_info.IsEmpty())
			if (cs_error.IsEmpty())
				cs_error = cs_ex_info;
			else
			{
				::ATL::CAtlString cs_ex_err;
				cs_ex_err.Format(_T("%s; %s"), cs_error.GetString(), cs_ex_info.GetString());
				cs_error = cs_ex_err;
			}
	}
	if (!cs_error.IsEmpty())
	{
		TBaseError::SetState(dwError, cs_error.GetString());
	}
	else
		TBaseError::SetHresult(HRESULT_FROM_WIN32(dwError));
	return *this;
}

CInternetError&    CInternetError::operator= (const HRESULT hError)
{
	*((CSysError*)this) = hError;
	return *this;
}

CInternetError&    CInternetError::operator= (const CSysError& err_ref)
{
	*((CSysError*)this) = err_ref;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CInternetOptions::CInternetOptions(const DWORD dwOptions) : m_dwOptions(dwOptions)
{
}

CInternetOptions::~CInternetOptions(void)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID       CInternetOptions::Clear(void)
{
	m_dwOptions = eInternetOption::eNone;
}

bool       CInternetOptions::IsGzipAccept(void)const
{
	return (0 != (eInternetOption::eAcceptGzip & m_dwOptions));
}

bool       CInternetOptions::IsOneRequest(void)const
{
	return (0 != (eInternetOption::eOneRequest & m_dwOptions));
}

HRESULT    CInternetOptions::Modify(const DWORD dwOptions, const bool bSet)
{
	const bool bChanged = (bSet ? 0 == (dwOptions & m_dwOptions) : 0 != (dwOptions & m_dwOptions));
	if (!bChanged)
		return S_FALSE;
	if (bSet)  m_dwOptions |=  dwOptions;
	else       m_dwOptions &= ~dwOptions;
	return S_OK;
}