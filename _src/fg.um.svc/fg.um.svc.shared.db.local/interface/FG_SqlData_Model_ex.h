#ifndef _FGSQLDATAMODELEX_H_EC63EDC3_91A8_4162_863B_593582616188_INCLUDED
#define _FGSQLDATAMODELEX_H_EC63EDC3_91A8_4162_863B_593582616188_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Apr-2018 at 5:48:34p, UTC+7, Novosibirsk, Rodniki, Monday;
	This is File Guardian background service local database structure/model interface declaration file.
*/
#include "Shared_SystemError.h"

namespace fg { namespace common { namespace data { namespace local
{
	using shared::lite::common::CSysError;

	class CDataType {
	public:
		enum _e {
			eNone    = 0x0, // default value;
			eReal    = 0x1, // http://jakegoulding.com/blog/2011/02/06/sqlite-64-bit-integers/
			eText    = 0x2,
			eInteger = 0x3,
		};
	};
	class CDataBase {
	protected:
		CAtlString     m_name;
	protected:
		CDataBase(LPCTSTR lpszName = NULL);
		~CDataBase(void);
	public:
		virtual bool   IsIdentifiable(LPCTSTR lpszName)const;
		virtual bool   IsValid(void)const;
	public:
		LPCTSTR        Name(void)const;
		HRESULT        Name(LPCTSTR);
	public:
		static bool    Identify(LPCTSTR lpszWhat, LPCTSTR lpszWith);
		static bool    Invalidate(LPCTSTR);
		static bool    Validate(LPCTSTR);
	public:
		operator LPCTSTR(void)const;
	};

	class CDataField : public CDataBase {
		typedef CDataBase TBase;
	private:
		CDataType::_e  m_type;
	public:
		CDataField(void);
		CDataField(const CDataType::_e, LPCTSTR lpszName);
		~CDataField(void);
	public:
		bool           IsValid(void)const override sealed;
		CDataType::_e  Type(void)const;
		VOID           Type(const CDataType::_e);
		CAtlString     TypeAsText(void)const;
	};

	typedef ::std::vector<CDataField>  TDataFieldEnum;

	class CDataFieldEnum {
	private:
		TDataFieldEnum    m_fields;
	public:
		CDataFieldEnum(void);
		~CDataFieldEnum(void);
	public:
		const TDataFieldEnum&
		               All(void)const;
		HRESULT        Append(const CDataField&);
		HRESULT        Append(const CDataType::_e, LPCTSTR lpszName);
		VOID           Clear(void);
		INT            Count(void)const;
		INT            Find (LPCTSTR lpszName)const; // returns -1 if not found or an error occcurs;
		bool           IsEmpty(void)const;
		CDataField     Item (const INT _ndx)const;
		CAtlString     ToString(void)const;
	};

	class CDataIndex : public CDataBase {
		typedef CDataBase TBase;
	private:
		CAtlString     m_field;
		CAtlString     m_table;
	public:
		CDataIndex(void);
		CDataIndex(LPCTSTR lpszName, LPCTSTR lpszField, LPCTSTR lpszTable);
		~CDataIndex(void);
	public:
		bool           IsIdentifiable(LPCTSTR lpszField)const override sealed;
		bool           IsValid(void)const override sealed;
	public:
		LPCTSTR        Field(void)const;
		HRESULT        Field(LPCTSTR);
		LPCTSTR        Table(void)const;
		HRESULT        Table(LPCTSTR);
	};

	typedef ::std::vector<CDataIndex> TDataIndexEnum;

	class CDataIndexEnum {
	private:
		TDataIndexEnum   m_indices;
	public:
		CDataIndexEnum(void);
		~CDataIndexEnum(void);
	public:
		HRESULT        Append(const CDataIndex&);
		HRESULT        Append(LPCTSTR lpszName, LPCTSTR lpszField, LPCTSTR lpszTable);
		VOID           Clear(void);
		INT            Count(void)const;
		INT            Find (LPCTSTR lpszField)const; // returns -1 if not found or an error occcurs;
		bool           IsEmpty(void)const;
		const
		CDataIndex     Item(const INT _ndx)const;
		CDataIndex     Item(const INT _ndx);
	};

	class CDataTable : public CDataBase {
		typedef CDataBase TBase;
	private:
		CDataFieldEnum    m_fields;
		CDataIndexEnum    m_indices;
	public:
		CDataTable(LPCTSTR lpszName = NULL);
		~CDataTable(void);
	public:
		bool              IsValid(void)const override sealed;
	public:
		const
		CDataFieldEnum&   Fields(void)const;
		CDataFieldEnum&   Fields(void);
		const
		CDataIndexEnum&   Indices(void)const;
		CDataIndexEnum&   Indices(void);
	};

	typedef ::std::vector<CDataTable> TDataTableEnum;
	
	class CDataSchema : public CDataBase {
		typedef CDataBase TBase;
	protected:
		CSysError      m_error;
		TDataTableEnum m_tables;
		CAtlString     m_source;
	public:
		CDataSchema(LPCTSTR lpszName = NULL);
		~CDataSchema(void);
	public:
		bool           IsValid(void)const override sealed;
	public:
		HRESULT        Append(const CDataTable&);
		HRESULT        Create(LPCTSTR lpszXmlStream);
		VOID           Clear (void);
		TErrorRef      Error(void)const;
		INT            Find (LPCTSTR lpszTableName)const; // returns -1, if specified table is not found;
		HRESULT        Remove (LPCTSTR lpszTable);
		const
		TDataTableEnum&Tables(void)const;
		const
		CDataTable&    TableOf (const INT _index)const;
		LPCTSTR        ToString(void)const;
	};

	class CDataSchemaLocator {
	protected:
		CSysError      m_error;
		CAtlString     m_data;
	public:
		CDataSchemaLocator(void);
	public:
		LPCTSTR        Data (void)const;
		TErrorRef      Error(void)const;
		HRESULT        LoadFromFile(LPCTSTR lpszPath);
		HRESULT        LoadFromResource(const UINT nResId);
	};

	typedef ::std::vector<CDataSchema> TDataSchemata;

	class CDataSchemaOpt {
	public:
		enum _e {
			eDoNotCare   = 0x0,
			eLoadDefault = 0x1,
		};
	};

	class CDataSchemata {
	private:
		CSysError      m_error;
		TDataSchemata  m_schemas;
		CDataSchema    m_default;

	public:
		CDataSchemata(const DWORD = CDataSchemaOpt::eLoadDefault);
		~CDataSchemata(void);

	public:
		const
		TDataSchemata& All(void)const;                   // no thread safity; no default schema is included;
		HRESULT        Append(const CDataSchema&);       // no finding schema by name in schemata set; 
		const
		CDataSchema&   Default(void)const;
		CDataSchema&   Default(void)     ;
		TErrorRef      Error  (void)const;
		const
		CDataSchema&   SchemaOf(const INT nIndex)const;
		CDataSchema&   SchemaOf(const INT nIndex)     ;
	};

	class CDbObjectBase_simplified {
	protected:
		mutable
		CSysError      m_error;
	public:
		TErrorRef      Error (void)const;
	};

	class CDbObjectBase : public CDbObjectBase_simplified {
		typedef CDbObjectBase_simplified TBase;
	protected:
		const PVOID&   m_con_ref;
	protected:
		CDbObjectBase(const PVOID& _p_conn);
	public:
		const PVOID    Connection(void)const;
		bool           IsConnected(void)const;
	};

	class CDbObject : public CDbObjectBase {

		typedef CDbObjectBase TBase;

	protected:
		CDataSchemata   m_schemata;
	public:
		CDbObject(const DWORD _opts = CDataSchemaOpt::eLoadDefault);
		~CDbObject(void);
	public:
		const
		CDataSchemata&  Schemata(void)const;
		CDataSchemata&  Schemata(void);
	};
}}}}

#endif/*_FGSQLDATAMODELEX_H_EC63EDC3_91A8_4162_863B_593582616188_INCLUDED*/