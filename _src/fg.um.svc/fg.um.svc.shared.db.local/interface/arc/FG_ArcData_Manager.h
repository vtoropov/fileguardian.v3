#ifndef _FGDATARCHIVEMGR_H_C0BD290D_D2C2_4c21_A59E_A302A24E9357_INCLUDED
#define _FGDATARCHIVEMGR_H_C0BD290D_D2C2_4c21_A59E_A302A24E9357_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-Mar-2016 at 5:50:33pm, GMT+8, Hong Kong Island, Thursday;
	This is File Guardian background service database archive management class declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_FS_GenericFile.h"
#include "Shared_FS_GenericFolder.h"
#include "Shared_SystemThreadPool.h"

#include "FG_SqlData_Provider.h"
#include "FG_ArcData_Cfg.h"

namespace fg { namespace common { namespace data { namespace local
{
	using shared::lite::common::CSysError;
	using shared::lite::common::CSysErrorSafe;
	using shared::ntfs::TFileList;
	using shared::lite::sys_core::CThreadBase;

	class CArchiveList
	{
	private:
		const
		CArchiveSettings&  m_cfg;
		mutable CSysError  m_error;
	public:
		CArchiveList(const CArchiveSettings&);
	public:
		TErrorRef      Error(void)const;
		TFileList      GetList(const bool bFileNameOnly)const;
		CAtlString     GenerateArcPath(void)const; // generates full path for new archive; the path is based on current archive settings
	public:
		static
		CAtlString     GenerateArcName(void);
	};

	class CArchiveManager : public CThreadBase
	{
	private:
		CSysError      m_error;
		CDbProvider&   m_db_provider;   // main database provider reference
	public:
		CArchiveManager(CDbProvider&);
	public:
		TErrorRef      Error (void)const;
	private:
		HRESULT        CreateVolume(LPCTSTR lpszTarget);
		HRESULT        RemoveExcessVolumes(const CArchiveSettings&);
	private:
		VOID           ThreadFunction(void) override sealed;
	};
}}}}

#endif/*_FGDATARCHIVEMGR_H_C0BD290D_D2C2_4c21_A59E_A302A24E9357_INCLUDED*/