#ifndef _FGDATARCHIVECFG_H_A95A9C17_C662_4050_B44F_52ADF7F9C62C_INCLUDED
#define _FGDATARCHIVECFG_H_A95A9C17_C662_4050_B44F_52ADF7F9C62C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Apr-2016 at 1:22:14pm, GMT+7, Phuket, Rawai, Sunday;
	This is File Guardian background service database archive configuration class(es) declaration file.
*/
#include "Shared_SystemError.h"
#include "FG_SysEvent_Model.h"

namespace fg { namespace common { namespace data { namespace local
{
	using shared::lite::common::CSysError;

	class CArchiveDefaults
	{
	public:
		enum {
			eThreshold = 150000,
			eTruncated =  50000,
			eVolumes   = 10,
		};
	public:
		static
		CAtlString    VolumeFolder(void);
	};

	class CArchiveSettings
	{
	protected:
		mutable
		CSysError     m_error;
	private:
		CRecordRange  m_threshold;
		CRecordRange  m_truncated;
		bool          m_enabled;
		INT           m_vol_num;
		CAtlString    m_vol_dir;
	public:
		CArchiveSettings(const bool bInitToDefaults = false);
	public:
		VOID          Default(void);             // resets to default settings
		bool          Enabled(void)const;
		VOID          Enabled(const bool);
		TErrorRef     Error(void)const;
		const
		CRecordRange& Threshold(void)const;
		CRecordRange& Threshold(void);
		const
		CRecordRange& Truncated(void)const;
		CRecordRange& Truncated(void);
		TErrorRef     Validate (void)const;
		CAtlString    VolumeFolder(void)const;
		HRESULT       VolumeFolder(LPCTSTR);
		INT           Volumes(void)const;
		HRESULT       Volumes(const INT);
		HRESULT       Volumes(LPCTSTR lpszVolumesAsText);
		CAtlString    VolumesAsText(void)const;
	};

	class CArchiveSettingPersistent : public CArchiveSettings
	{
		typedef CArchiveSettings TBase;
	public:
		CArchiveSettingPersistent(const bool bInitToDefaults);
	public:
		HRESULT       Load(void);
		HRESULT       Save(void);
	};
}}}}

#endif/*_FGDATARCHIVECFG_H_A95A9C17_C662_4050_B44F_52ADF7F9C62C_INCLUDED*/