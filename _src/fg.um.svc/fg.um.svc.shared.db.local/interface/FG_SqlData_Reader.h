#ifndef _FGSQLDATAREADER_H_3E9BA3A4_1085_4e8c_9634_0B8C6C272C92_INCLUDED
#define _FGSQLDATAREADER_H_3E9BA3A4_1085_4e8c_9634_0B8C6C272C92_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Apr-2018 at 7:40:29a, UTC+7, Novosibirsk, Rodniki, Thursday;
	This is File Guardian background service local database reader interface declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_SystemThreadPool.h"

#include "FG_SysEvent_Model.h"
#include "FG_SysEvent_Filter.h"
#include "FG_SqlData_Model_ex.h"
#include "FG_SqlData_Qry.h"

namespace fg { namespace common { namespace data { namespace local
{
	using shared::lite::common::CSysError;
	using shared::lite::common::CSysErrorSafe;

	using shared::lite::sys_core::CThreadBase;

	using fg::common::data::CRecord;
	using fg::common::data::CRecordCache;
	using fg::common::data::CRecordLimit;

	typedef ::std::vector<std::vector<CAtlString>> TRecordset;

	class CDbReader : public CDbObjectBase_simplified {
		typedef CDbObjectBase_simplified TBase;
	public:
		enum _e {
			e_na = -1,
		};
	private:
		const CDbObject& m_obj_ref;
	public:
		CDbReader(const CDbObject&);
	public:
		TRecords         Records(const CDbQuery&)const;
		TRecords         Records(const CFilter&, const CRecordLimit&) const;
		TRecords         Records(const CFilter&, const INT _offset = CDbReader::e_na, const INT _limit = CDbReader::e_na) const;
		HRESULT          Records(const CDataSchema& , TRecords& ) const;
		TRecords         Records(const INT _status, const INT _limit = CDbReader::e_na) const;
		HRESULT          Recordset(const CDataSchema& , TRecordset& ) const;
		HRESULT          Recordset(const CRecordLimit&, TRecordset& ) const;
		HRESULT          Recordset(const INT _offset, const INT _limit, TRecordset& ) const;
	};
}}}}

#endif/*_FGSQLDATAREADER_H_3E9BA3A4_1085_4e8c_9634_0B8C6C272C92_INCLUDED*/