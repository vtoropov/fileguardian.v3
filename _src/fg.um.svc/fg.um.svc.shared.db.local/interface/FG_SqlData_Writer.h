#ifndef _FGSQLWRITER_H_3F07F10F_4C4C_491c_BEE5_D92405991826_INCLUDED
#define _FGSQLWRITER_H_3F07F10F_4C4C_491c_BEE5_D92405991826_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Apr-2016 at 11:03:05pm, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian background service shared library database writer class(es) declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_SystemThreadPool.h"

#include "FG_SysEvent_Model.h"
#include "FG_SqlData_Model_ex.h"

namespace fg { namespace common { namespace data { namespace local
{
	using shared::lite::common::CSysError;
	using shared::lite::common::CSysErrorSafe;

	using shared::lite::sys_core::CThreadBase;

	using fg::common::data::CRecord;
	using fg::common::data::CRecordCache;

	class CDbInserter : public CThreadBase {
		typedef CThreadBase TBase;
	private:
		const
		CDataSchema&   m_schema;
		CRecordCache&  m_cache;
	public:
		CDbInserter(CRecordCache&, const CDataSchema&);
		~CDbInserter(void);
	private:
		VOID           ThreadFunction(void) override sealed;
	};

	class CDbWriter : public CDbObjectBase_simplified {
		typedef CDbObjectBase_simplified TBase;
	protected:
		const CDataSchema&   m_schema;
	public:
		CDbWriter(const CDataSchema&);
	public:
		virtual        // TODO: virtual field must go first, otherwise, performance slows down;
		HRESULT        Insert (const CRecord&);                             // sync insert method (overriden by cached writer and is not called)
#if (1)
		HRESULT        Insert (LPCTSTR lpszTable, const time_t _time);      // is used for debugging date insertion
#endif
		HRESULT        InsertSync  (const CRecord&);                        // designated sync inserting method;
		HRESULT        UpdateStatus(const CRecord&, const INT _status = 1); // updates record status by web reporting monitor
		HRESULT        UpdateStatus(const TRecords&, const INT _status);    // updates record status by web reporting monitor
#if (1)
		HRESULT        UpdateStatus(const INT _where, const INT _by_what);  // updates status entirely
#endif
	protected:
		CSysError&     ErrorW(void);     
	};

	class CDbWriterCached : public CDbWriter {
		typedef CDbWriter TBase;
	private:
		CRecordCache   m_cache;
		volatile bool  m_suspended;
		CDbInserter    m_inserter;
	public:
		CDbWriterCached(const CDataSchema&);
	public:
		CRecordCache&  Cache(void);
		HRESULT        Insert (const CRecord&) override sealed;             // deferred insert method, puts objects to cache
		HRESULT        Suspend(const bool);                                 // if true, all records are cached, but are not written to a database
	};
}}}}

#endif/*_FGSQLWRITER_H_3F07F10F_4C4C_491c_BEE5_D92405991826_INCLUDED*/