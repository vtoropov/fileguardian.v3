#ifndef _FGSQLDATAACCESS_H_2D6E9EBB_126C_4c32_8E26_70694FB1F3E5_INCLUDED
#define _FGSQLDATAACCESS_H_2D6E9EBB_126C_4c32_8E26_70694FB1F3E5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Apr-2018 at 7:48:07a, UTC+7, Novosibirsk, Rodniki, Wednesday;
	This is File Guardian background service local database access interface declaration file.
*/
#include "Shared_SystemError.h"
#include "FG_SqlData_Model_ex.h"
#include "FG_SqlData_Writer.h"

namespace fg { namespace common { namespace data { namespace local
{
	using shared::lite::common::CSysError;

	class CDbLocator {
	private:
		CSysError      m_error;
		CAtlString     m_path;
	public:
		CDbLocator(void);
		CDbLocator(LPCTSTR lpszPath);
	public:
		TErrorRef      Error(void)const;
		HRESULT        Parse(LPCTSTR lpszPath);
		LPCTSTR        Path(void)const;
	public:
		operator LPCTSTR(void)const;
	public:
		CDbLocator& operator= (LPCTSTR lpszPath);
	};

	class CDbPragma {
	public:
		enum _e {
			eNone            = 0x0,
			eJournalInMemory = 0x1,
			eLockingMode     = 0x2,
			ePageSize        = 0x3,
		};
	private:
		_e              m_id;
		variant_t       m_value;
	public:
		CDbPragma(void);
		CDbPragma(const CDbPragma::_e, const variant_t&);
	public:
		_e              Identifier(void)const;
		HRESULT         Identifier(const _e);
		bool            IsValid(void)const;
		CAtlString      Name(void)const;
		CAtlString      ToString(void)const;
		const
		variant_t&      Value(void)const;
		variant_t&      Value(void);
	public:
		CDbPragma&      operator=(const bool);
		CDbPragma&      operator=(const INT );
	};

	typedef ::std::map<CDbPragma::_e, CDbPragma> TPragmaSet;

	class CDbPragmaSet {
	private:
		TPragmaSet      m_set;
	public:
		CDbPragmaSet(void);
	public:
		HRESULT         Append(const CDbPragma&);
		HRESULT         Append(const CDbPragma::_e, const variant_t&);
		VOID            Clear (void);
		CAtlString      Get   (const CDbPragma::_e) const;
		bool            Has   (const CDbPragma::_e) const;
		HRESULT         Remove(const CDbPragma::_e);
		HRESULT         Set(const CDbPragma::_e, const bool);
		HRESULT         Set(const CDbPragma::_e, const INT);
		HRESULT         Set(const CDbPragma::_e, const variant_t&);
		CAtlString      ToString(void)const;
	};

	class CDbAccessor {
	private:
		mutable
		CSysError      m_error;
		CDbWriterCached&
		               m_writer;
		CDbObject&     m_object;
		CDbLocator     m_locator;
		CDbPragmaSet   m_prg_set;
	public:
		CDbAccessor(CDbWriterCached&, CDbObject&);
		~CDbAccessor(void);
	public:
		HRESULT        Close(void);
		TErrorRef      Error(void)const;
		bool           IsOpen(void)const;
		const
		CDbLocator&    Locator(void)const;
		CDbLocator&    Locator(void);
		HRESULT        Open(LPCTSTR lpszPath);                      // applies the default data schema automatically
		HRESULT        Open(LPCTSTR lpszPath, const CDataSchema&);  // applies data schema provided
		const
		CDbPragmaSet&  Pragma(void)const;
		CDbPragmaSet&  Pragma(void);                                // Pragma can be changed before opening a database file;
	};
}}}}

#endif/*_FGSQLDATAACCESS_H_2D6E9EBB_126C_4c32_8E26_70694FB1F3E5_INCLUDED*/