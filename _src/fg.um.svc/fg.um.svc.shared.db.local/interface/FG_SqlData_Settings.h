#ifndef _FGSQLDATALOCATOR_H_27268310_F90D_42c2_AAED_E5B128819AA1_INCLUDED
#define _FGSQLDATALOCATOR_H_27268310_F90D_42c2_AAED_E5B128819AA1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Apr-2016 at 11:07:24pm, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian background service library database settings interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 4-Jun-2018 at 9:05:00a, UTC+7, Phuket, Rawai, Monday;
*/
#include "Shared_SystemError.h"

namespace fg { namespace common { namespace data { namespace local
{
	using shared::lite::common::CSysError;

	class CDbSettings
	{
	protected:
		mutable
		CSysError         m_error;
	private:
		CAtlString        m_file;
		CAtlString        m_folder;
		bool              m_create;            // if true the folder must be created if it doesn't exist
	public:
		CDbSettings(const bool bInitToDefaults = false);
	public:
		bool              CreateFolderOpt(void)const;
		VOID              CreateFolderOpt(const bool);
		VOID              Default(void);       // resets to default settings
		TErrorRef         Error(void)const;
		CAtlString        FileName(void)const;
		HRESULT           FileName(LPCTSTR);
		CAtlString        FolderPath(const bool bUnwind = false)const;
		HRESULT           FolderPath(LPCTSTR);
		CAtlString        FullPath(void)const; // gets full path to the main database
		HRESULT           Validate(void)const;
	};

	class CDbSettingDefaults
	{
	public:
		static bool       CreateFolder(void);
		static CAtlString FileName(void);
		static CAtlString FolderPath(void);
	};

	class CDbSettingPersistent : public CDbSettings
	{
		typedef CDbSettings TBase;
	public:
		CDbSettingPersistent(const bool bInitToDefaults);
	public:
		HRESULT           Load(void);
		HRESULT           Save(void);
	};
}}}}

#endif/*_FGSQLDATALOCATOR_H_27268310_F90D_42c2_AAED_E5B128819AA1_INCLUDED*/