#ifndef _FGSQLDATAQRY_H_B8A6C03E_6865_47BC_B326_B7ECD2BF7E70_INCLUDED 
#define _FGSQLDATAQRY_H_B8A6C03E_6865_47BC_B326_B7ECD2BF7E70_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jun-2018 at 0:35:13a, UTC+7, Phuket, Rawai, Monday;
	This is File Guardian shared service library local database query interface declaration file.
*/
#include "Shared_SystemError.h"
#include "FG_SqlData_Model_ex.h"

namespace fg { namespace common { namespace data { namespace local
{
	using shared::lite::common::CSysError;

	class CDbParam {
	private:
		CDataType::_e m_type;
		CAtlString    m_field_name;
		time_t        m_time;
		CAtlString    m_text;
		LONG          m_long;
	public:
		CDbParam(void);
		~CDbParam(void);
	public:
		bool          IsIdentifiable(LPCTSTR _lpsz_name)const;
		bool          IsValid(void)const;
		LPCTSTR       Name(void)const;
		HRESULT       Name(LPCTSTR);
		CAtlString    ToString(void)const;
		LONG          ValueAsLong(void)const;
		HRESULT       ValueAsLong(const LONG);
		LPCTSTR       ValueAsText(void)const;
		VOID          ValueAsText(LPCTSTR);
		time_t        ValueAsTime(void)const;
		HRESULT       ValueAsTime(const time_t);
		CDataType::_e Type(void)const;
		VOID          Type(const CDataType::_e);
	};

	typedef ::std::vector<CDbParam> TDbQueryParams;

	class CDbParamEnum : public CDbObjectBase_simplified {
		typedef CDbObjectBase_simplified TBase;
	private:
		TDbQueryParams   m_params;
	public:
		CDbParamEnum(void);
		~CDbParamEnum(void);
	public:
		HRESULT Append (LPCTSTR _lpsz_name, const LONG _value);
		HRESULT Append (LPCTSTR _lpsz_name, LPCTSTR _value);
		HRESULT Append (LPCTSTR _lpsz_name, const time_t _value);
		INT     Find   (LPCTSTR _lpsz_name) const;    // finds a parameter by name spacicife; returns -1 if a param is not found;
		CAtlString      ToString(void)const;
		const bool      IsValid (void)const;
		const
		TDbQueryParams& RawData (void)const;
	};

	class CDbQuery : public CDbObjectBase_simplified {
		typedef CDbObjectBase_simplified TBase;
	private:
		CDbParamEnum   m_params;

	public:
		CDbQuery(void);
		~CDbQuery(void);
	public:
		const bool      IsValid(void)const;
		const
		CDbParamEnum&   Params (void)const;
		CDbParamEnum&   Params (void);
		CAtlString      ToString(const CDataSchema&)const;
	};
}}}}

#endif/*_FGSQLDATAQRY_H_B8A6C03E_6865_47BC_B326_B7ECD2BF7E70_INCLUDED*/