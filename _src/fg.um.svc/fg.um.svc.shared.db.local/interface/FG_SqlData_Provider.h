#ifndef _FGDBPROVIDER_H_825F6902_78C0_400c_BC4E_52413A347FEF_INCLUDED
#define _FGDBPROVIDER_H_825F6902_78C0_400c_BC4E_52413A347FEF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jan-2016 at 5:24:25am, GMT+7, Phuket, Rawai, Monday;
	This is File Guardian background service shared library database provider class(es) declaration file.
*/
#include "FG_SysEvent_Filter.h"
#include "FG_SysEvent_Model.h"
#include "FG_SqlData_Access.h"
#include "FG_SqlData_Writer.h"
#include "FG_SqlData_Reader.h"
#include "FG_SqlData_Settings.h"

namespace fg { namespace common { namespace data { namespace local
{
	using shared::lite::common::CSysError;
	using fg::common::data::CRecord;

	class CDbStats : public CDbObjectBase_simplified {
		typedef CDbObjectBase_simplified TBase;
	public:
		CDbStats(void);
	public:
		LONG             RowCount(const CFilter&)const;        // returns row count for SQL query provided;
		LONG             RowCount(LPCTSTR lpszTableName)const; // returns row count for a table specified, or -1 in case of error
		HRESULT          Totals  (const CFilter&, TOperateTotals&)const; // returns row count that is categorized by action;
	};

	class CDbProvider : public CDbObjectBase_simplified {
		typedef CDbObjectBase_simplified TBase;
	private:
		CDbObject        m_object;
		CDbReader        m_reader;
		CDbWriterCached  m_writer;
		CDbStats         m_stats ;
		volatile bool    m_locked;
		CDbAccessor      m_access;
	public:
		CDbProvider(const DWORD = CDataSchemaOpt::eLoadDefault);
		~CDbProvider(void);
	public:
		const
		CDbAccessor&     Accessor(void)const;
		CDbAccessor&     Accessor(void)     ;
		HRESULT          Compact(void);         // executes vacuum instruction; all writing operations must be suspended;
		const CDbObject& Database(void)const;
		CDbObject&       Database(void)     ;
		HRESULT          Delete(LPCTSTR lpszTable, const LONG _deleted, const bool _asc); // deletes the specified number of records from the beginning of ordered row set
		bool             IsLocked(void)const;   // if true, no insert/update/delete operation allowed
		VOID             Lock (const bool);     // sets lock flag to signal other clients an ability to update data
		CDbReader&       Reader(void);
		const CDbStats&  Stats(void)const;
		CDbWriter&       Writer(void);
		CDbWriterCached& WriterCached(void);
	};
}}}}

#endif/*_FGDBPROVIDER_H_825F6902_78C0_400c_BC4E_52413A347FEF_INCLUDED*/