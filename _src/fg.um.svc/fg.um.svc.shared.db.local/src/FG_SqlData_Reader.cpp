/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Apr-2018 at 7:48:25a, UTC+7, Novosibirsk, Rodniki, Thursday;
	This is File Guardian background service local database reader interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 10:37:17p, UTC+7, Phuket, Rawai, Thursday;
*/
#include "StdAfx.h"
#include "FG_SqlData_Reader.h"
#include "FG_SysEvent_Model.h"

using namespace fg::common::data;
using namespace fg::common::data::local;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data { namespace local { namespace details {

	using fg::common::data::CRecordLimit;

	class CDbReader_Limited : public CRecordLimit {
		typedef CRecordLimit TBase;
	private:
		const
		CDataSchema&     m_schema;  // assumes the default schema, but, actually, any schema can be provided;
		CDataTable       m_table;
		INT              m_prefer;
	public:
		CDbReader_Limited(const CRecordLimit& _limit, const CDataSchema& _schema, const INT nTableNdx = CDbReader::e_na) :
		   TBase(_limit), m_prefer(nTableNdx), m_schema(_schema) {
		}
	public:
		TErrorRef        Initialize(CDbConnection& _cnn, CSysError& _err){
			_err.Module(_T(__FUNCTION__)); _cnn;
			_err = S_OK;

			if (m_schema.IsValid() == false) {
				return (_err = m_schema.Error());
			}

			const TDataTableEnum& enum_ = m_schema.Tables();
			if (enum_.empty()) {
				_err.SetState(
						E_INVALIDARG, _T("Table collection is empty.")
					);
				return _err;
			}
			if (CDbReader::e_na == m_prefer)
				m_table = enum_[0];
			else if (m_prefer < static_cast<INT>(enum_.size()))
				m_table = enum_[m_prefer];
			else {
				_err = DISP_E_BADINDEX;
			}

			return _err;
		}
		const
		CDataTable&      Table(void)const { return m_table; }
		CDataTable&      Table(void)      { return m_table; }
	};

	TErrorRef CDbReader_ParseLimitedQry(CDbConnection& _cnn, CSysError& _err, CAtlString& _qry, const CDbReader_Limited& _params) {
		_err.Module(_T(__FUNCTION__)); _cnn;
		_err = S_OK;

		CAtlString cs_subquery;
		cs_subquery.Format(
			_T("select %s from"), /*(LPCTSTR)_params.Table().Fields().ToString()*/ _T("*")
			);

		_qry += cs_subquery;
		_qry += _T(" ( %s ) ");

		cs_subquery.Format(
			_T("limit %d offset %d"), _params.Limit(), _params.Offset()
			);
		_qry += cs_subquery;

		return _err;
	}

	TErrorRef CDbReader_ParseSelectQry (CDbConnection& _cnn, CSysError& _err, CAtlString& _qry, const CDbReader_Limited& _params) {
		_err.Module(_T(__FUNCTION__)); _cnn;
		_err = S_OK;
		//
		// change(s) in spec can affect this query by field name sequence, it is replaced by asterisk for now;
		//
#if(0)
		_qry.Format(
			_T("select %s from %s"), (LPCTSTR)_params.Table().Fields().ToString(), (LPCTSTR)_params.Table().Name()
			);
#else
		_qry.Format(
			_T("select * from %s"), (LPCTSTR)_params.Table().Name()
		);
#endif
		return _err;
	}
}}}}}

/////////////////////////////////////////////////////////////////////////////

CDbReader::CDbReader(const CDbObject& _obj) : m_obj_ref(_obj) { TBase::m_error.Source(_T("CDbReader")); }

/////////////////////////////////////////////////////////////////////////////

TRecords     CDbReader::Records(const CDbQuery& _qry)const {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	TRecords recs_;

	if (_qry.IsValid() == false) {
		m_error = _qry.Error(); return recs_;
	}

	CDbConnection& cnn_ = global::conn_ref();
	if (!cnn_.IsOpen()) {
		m_error = OLE_E_BLANK; return recs_;
	}

	const CDataSchema& schema_ = m_obj_ref.Schemata().Default();
	if (schema_.IsValid() == false) {
		m_error = schema_.Error(); return recs_;
	}

	try {
		sqlite3_try_lock lock_(cnn_, false);

		if (lock_.is_locked()) {
			sqlite3_command cmd_(cnn_, _qry.ToString(schema_));
			sqlite3_reader reader_ = cmd_.executereader();

			const INT n_cols_ = reader_.get_col_count();

			while (reader_.read()) {
				//
				// expects the fields are specified in a schema in the same order;
				//
				CRecord rec_;

				if (n_cols_ > 0) {
					const time_t timestamp_ = (time_t)reader_.getint64(0);
					rec_.Timestamp(timestamp_);
				}
				if (n_cols_ > 1) rec_.Source(reader_.getstring16(1).c_str());
				if (n_cols_ > 2) rec_.Target(reader_.getstring16(2).c_str());
				if (n_cols_ > 3) rec_.Action(reader_.getint(3));
				if (n_cols_ > 4) rec_.Status(reader_.getint(4));
				if (n_cols_ > 5) rec_.Process(reader_.getstring16(5).c_str());
				if (n_cols_ > 6) rec_.TypeAsLong(reader_.getint(6));
				if (n_cols_ > 7) rec_.TimestampEx(reader_.getint64(7));
				try {
					recs_.push_back(rec_);
				} catch(::std::bad_alloc&) {
					m_error = E_OUTOFMEMORY; break;
				}
			}
		}

	} catch(database_error& _ex) {
		m_error.SetState(
			(DWORD)ERROR_DATABASE_FAILURE, CAtlString(_ex.what())
		);
	}

	return recs_;
}

TRecords     CDbReader::Records(const CFilter& _flt, const CRecordLimit& _limit) const {
	return this->Records(_flt, _limit.Offset(), _limit.Limit()); 
}

TRecords     CDbReader::Records(const CFilter& _flt, const INT _offset, const INT _limit) const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDbConnection& cnn_ = global::conn_ref();
	if (!cnn_.IsOpen()) {
		m_error = OLE_E_BLANK; return TRecords();
	}

	HRESULT hr_ = S_OK;
	TRecords recs_;

	CRecordLimit rec_lmt_(_offset, _limit);
	const CDataSchema& schema_ = m_obj_ref.Schemata().Default();
	if (schema_.IsValid() == false) {
		m_error = schema_.Error(); return TRecords();
	}

	details::CDbReader_Limited params_(rec_lmt_, schema_);

	hr_ = params_.Initialize(cnn_, m_error);
	if (FAILED(hr_))
		return recs_;

	try {
		sqlite3_try_lock lock_(cnn_, false);

		CAtlString cs_query;
		hr_ = details::CDbReader_ParseSelectQry(cnn_, m_error, cs_query, params_);
		if (FAILED(hr_)) {
			return recs_;
		}

		if (_flt.Enabled()) {	
			CAtlString cs_criteria = _flt.ToString();

			if (!cs_criteria.IsEmpty()) {
				cs_query += _T(" where ");
				cs_query += cs_criteria;
			}

			if (_flt.Extension().Reversed()) {
				cs_query += _T(" order by ");
				cs_query += _flt.Extension().ReversedAsQuery();
			}

			if (_flt.Extension().RowLimit() > 0)
				cs_query += _flt.Extension().RowLimitAsQuery();
		}
		if (CDbReader::e_na != _offset &&
		    CDbReader::e_na != _limit ) {

			CAtlString cs_pattern;
			hr_ = details::CDbReader_ParseLimitedQry(cnn_, m_error, cs_pattern, params_);
			
			CAtlString cs_limited;
			cs_limited.Format(
					(LPCTSTR)cs_pattern, (LPCTSTR)cs_query
				);
			cs_query = cs_limited;
		}

		if (lock_.is_locked()) {
			sqlite3_command cmd_(cnn_, cs_query);
			sqlite3_reader reader_ = cmd_.executereader();
			//
			// TODO: db reader expects only one table; moreover, if schema has several tables, the first one is used;
			//       this is a limitation for the time being;
			//
#if (0)
			const CDataTable& table_ = schema_.Tables().at(0);
			const CDataFieldEnum& fields_ = table_.Fields(); 
#endif
			const INT n_cols = reader_.get_col_count();

			while (reader_.read()) {

				CRecord rec_;
#if (0)
				//
				// this field enumeration adopts a record object to data being retured;
				// it is assumed that a data is intended for FG data model;
				//
				// TODO: to make assumption that database schemata XML has the fixed fields order as it is described for release version;
				//       moreover, such schema must have an ability to check fields type of real data with declared ones;
				//
				for (INT i_ = 0; i_ < fields_.Count(); i_++) {
					const CDataField& field_ = fields_.Item(i_);

					switch (field_.Type()) {
					case CDataType::eReal: {const time_t timestamp_ = (time_t)reader_.getint64(i_); rec_.Timestamp(timestamp_);} break;
					case CDataType::eText: {
							if (field_.IsIdentifiable(_T("source"))) rec_.Source(reader_.getstring16(i_).c_str());
							else                                     rec_.Target(reader_.getstring16(i_).c_str());
						} break;
					case CDataType::eInteger: {
							if (field_.IsIdentifiable(_T("action_id"))) rec_.Action(reader_.getint(i_));
							else                                     rec_.Status(reader_.getint(i_));
						} break;
					}
				}
#else
				//
				// the following field order is assumed (xml schema must have the same)
				// a) time       (0)
				// b) source     (1)
				// c) target     (2)
				// d) action_id  (3)
				// e) status     (4)
				// f) proc_path  (5)
				// g) obj_type   (6)
				// h) time_ex    (7)
				//
				if (n_cols > 0) {
					const time_t timestamp_ = (time_t)reader_.getint64(0);
					rec_.Timestamp(timestamp_);
				}
				if (n_cols > 1) rec_.Source(reader_.getstring16(1).c_str());
				if (n_cols > 2) rec_.Target(reader_.getstring16(2).c_str());
				if (n_cols > 3) rec_.Action(reader_.getint(3));
				if (n_cols > 4) rec_.Status(reader_.getint(4));
				if (n_cols > 5) rec_.Process(reader_.getstring16(5).c_str());
				if (n_cols > 6) rec_.TypeAsLong(reader_.getint(6));
				if (n_cols > 7) rec_.TimestampEx(reader_.getint64(7));
#endif

				try {
					recs_.push_back(rec_);
				} catch(::std::bad_alloc&) {
					m_error = E_OUTOFMEMORY; break;
				}
			}
		}
	} catch(database_error& _ex) {
		m_error.SetState(
				(DWORD)ERROR_DATABASE_FAILURE, CAtlString(_ex.what())
			);
	}

	return recs_;
}

HRESULT      CDbReader::Records(const CDataSchema& _schema, TRecords& _recs) const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDbConnection& cnn_ = global::conn_ref();
	if (!cnn_.IsOpen()) {
		return (m_error = OLE_E_BLANK);
	}

	if (_schema.IsValid() == false)
		return (m_error = E_INVALIDARG);

	try {
		sqlite3_try_lock lock_(cnn_, false);

		const TDataTableEnum& tables = _schema.Tables();
		if (tables.empty()) {
			return (m_error = (DWORD)ERROR_INVALID_DATA);
		}

		const size_t n_prefer = 0;

		CAtlString cs_query;
		cs_query.Format(
			_T("select %s from %s"), (LPCTSTR)tables[n_prefer].Fields().ToString(), (LPCTSTR)tables[n_prefer].Name()
			);
		if (lock_.is_locked()) {
			sqlite3_command cmd_(cnn_, cs_query);
			sqlite3_reader reader_ = cmd_.executereader();

			const TDataFieldEnum& flds_ = tables[n_prefer].Fields().All();

			while (reader_.read()) {

				CRecord rec_;

				for (size_t i_ = 0; i_ < flds_.size(); i_++) {
					const CDataField& fld_ = flds_[i_];

					switch (fld_.Type()) {
						case CDataType::eReal:     rec_.Timestamp((time_t)reader_.getint64((INT)i_));    break;
						case CDataType::eText:     rec_.Target   (reader_.getstring16((INT)i_).c_str()); break;
						case CDataType::eInteger:  continue;  // TODO: must be bound to property by field name;
						default:                   continue;
					}
				}

				try {
					_recs.push_back(rec_);
				} catch(::std::bad_alloc&) {
					m_error = E_OUTOFMEMORY; break;
				}
			}
		}
	} catch(database_error& _ex) {
		m_error.SetState(
				(DWORD)ERROR_DATABASE_FAILURE, CAtlString(_ex.what())
			);
	}

	return m_error;
}

TRecords     CDbReader::Records(const INT _status, const INT _limit)const
{
	CFilter flt_;
	flt_.Status(_status);
	if (_limit > 0) {
		flt_.Enabled(true);
		flt_.Extension().RowLimit(_limit);
	}
	return this->Records(flt_);
}

HRESULT      CDbReader::Recordset(const CDataSchema& _schema, TRecordset& _recs) const {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	TRecords recs_;
	HRESULT hr_ = this->Records(_schema, recs_);
	if (FAILED(hr_))
		return hr_;

	for (size_t i_ = 0; i_ < recs_.size(); i_++) {
		const CRecord& rec_ = recs_[i_];

		::std::vector<CAtlString> rec_as_str;

		try {
			rec_as_str.push_back(rec_.TimestampAsText());
			rec_as_str.push_back(rec_.Target());
			rec_as_str.push_back(rec_.ActionAsText());
			rec_as_str.push_back(rec_.StatusAsText());
			_recs.push_back(rec_as_str);
		} catch(::std::bad_alloc&) {
			m_error = E_OUTOFMEMORY; break;
		}
	}
	return m_error;
}

HRESULT      CDbReader::Recordset(const CRecordLimit& _limit, TRecordset& _recs) const {
	return this->Recordset(_limit.Offset(), _limit.Limit(), _recs);
}

//
// is used by print preview data provider;
//
HRESULT      CDbReader::Recordset(const INT _offset, const INT _limit, TRecordset& _recs) const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDbConnection& cnn_ = global::conn_ref();
	if (!cnn_.IsOpen())
		return (m_error = OLE_E_BLANK);

	CRecordLimit rec_lmt_(_offset, _limit);

	details::CDbReader_Limited params_(rec_lmt_, m_obj_ref.Schemata().Default());

	HRESULT hr_ = params_.Initialize(cnn_, m_error);
	if (FAILED(hr_))
		return m_error;

	if (params_.IsValid() == false)
		return (m_error = E_INVALIDARG);

	try {
		sqlite3_try_lock lock_(cnn_, false);

		CAtlString cs_pattern;
		hr_ = details::CDbReader_ParseLimitedQry(
				cnn_, m_error, cs_pattern, params_
			);

		CAtlString cs_query;
		cs_query.Format(
			cs_pattern, params_.Table().Name()
			);

		if (lock_.is_locked()) {
			sqlite3_command cmd_(cnn_, cs_query);
			sqlite3_reader reader_ = cmd_.executereader();

			const INT n_cols_ = reader_.get_col_count();

			while (reader_.read()) {

				::std::vector<CAtlString> rec_as_str;
				CRecord rec_;

				if (n_cols_ > 0) {
					const time_t timestamp_ = (time_t)reader_.getint64(0);
					rec_.Timestamp(timestamp_);
				}
				if (n_cols_ > 2) rec_.Target   (reader_.getstring16(2).c_str());
				if (n_cols_ > 3) rec_.Action   (reader_.getint(3));
				if (n_cols_ > 4) rec_.Status   (reader_.getint(4));

				try {
					rec_as_str.push_back(rec_.TimestampAsText());
					rec_as_str.push_back(rec_.Target());
					rec_as_str.push_back(rec_.ActionAsText());
					rec_as_str.push_back(rec_.StatusAsText());
					_recs.push_back(rec_as_str);
				} catch(::std::bad_alloc&) {
					m_error = E_OUTOFMEMORY; break;
				}
			}
		}
	} catch(database_error& _ex) {
		m_error.SetState(
				(DWORD)ERROR_DATABASE_FAILURE, CAtlString(_ex.what())
			);
	}

	return m_error;
}