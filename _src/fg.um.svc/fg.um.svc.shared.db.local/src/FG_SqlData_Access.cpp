/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Apr-2018 at 8:33:09a, UTC+7, Novosibirsk, Rodniki, Wednesday;
	This is File Guardian background service local database access interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 10:31:00p, UTC+7, Phuket, Rawai, Thursday;
*/
#include "StdAfx.h"
#include "FG_SqlData_Access.h"
#include "FG_SqlData_Settings.h"

using namespace fg::common::data::local;

#include "Shared_GenericAppObject.h"
#include "Shared_FS_GenericFolder.h"

using namespace shared::ntfs;
using namespace shared::user32;
/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data { namespace local { namespace details
{
	bool       CDbAccess__find_object(LPCTSTR lpszObjectName, CDbConnection& _cnn, CSysError& _err) {
		_err.Module(_T(__FUNCTION__));
		_err = S_OK;

		if (CDataBase::Invalidate(
		           lpszObjectName)) { _err = E_INVALIDARG; return false; }
		if (_cnn.IsOpen() == false) { _err = OLE_E_BLANK;  return false; }

		CAtlString cs_query;
		cs_query.Format(
				_T("select count(*) from sqlite_master where name='%s';"), lpszObjectName
			);
		const bool bIsPresented = (0 != _cnn.executeint(cs_query));
		return bIsPresented;
	}

	TErrorRef  CDbAccess_CreateIndex (const CDataIndex& _ndx, CDbConnection& _cnn, CSysError& _err) {
		_err.Module(_T(__FUNCTION__));
		_err = S_OK;

		if (_ndx.IsValid() == false)  return (_err = E_INVALIDARG);
		if (_cnn.IsOpen() == false)   return (_err = OLE_E_BLANK);

		CAtlString cs_query;
		cs_query.Format(
			_T("create index %s on %s(%s)"), _ndx.Name(), _ndx.Table(), _ndx.Field()
			);
		_cnn.executenonquery(cs_query.GetString());

		return _err;
	}

	TErrorRef  CDbAccess_CreateTable (const CDataTable& _tbl, CDbConnection& _cnn, CSysError& _err) {
		_err.Module(_T(__FUNCTION__));
		_err = S_OK;

		if (_tbl.IsValid() == false)  return (_err = E_INVALIDARG);
		if (_cnn.IsOpen() == false)   return (_err = OLE_E_BLANK);

		CAtlString cs_query;
		cs_query.Format(
			_T("create table %s ("), _tbl.Name()
			);
		for (INT i_ = 0; i_ < _tbl.Fields().Count(); i_++) {
			const CDataField& fld_ = _tbl.Fields().Item(i_);

			cs_query += fld_.Name();
			cs_query += _T(" ");
			cs_query += fld_.TypeAsText();
			if (i_ == _tbl.Fields().Count() - 1)
				cs_query += _T(")");
			else
				cs_query += _T(",");
		}
		_cnn.executenonquery(cs_query.GetString());

		return _err;
	}

	CAtlString CDbAccess_PragmaToName(const CDbPragma::_e _id) {
		CAtlString cs_name;
		switch (_id) {
			case CDbPragma::eJournalInMemory: cs_name = _T("journal_mode"); break;
			case CDbPragma::eLockingMode: cs_name = _T("loking_mode"); break;
			case CDbPragma::ePageSize: cs_name = _T("page_size"); break;
		}
		return cs_name;
	}

	CAtlString CDbAccess_PragmaToStr (const variant_t&  _val) {
		CAtlString cs_val;
		switch (_val.vt) {
			case VT_BOOL: { cs_val = (VARIANT_TRUE == _val.boolVal ? _T("ON") : _T("OFF")); } break;
			case VT_I4  : { cs_val.Format(_T("%d"), _val.lVal); } break;
			case VT_BSTR: { cs_val = (LPCTSTR)_bstr_t(_val.bstrVal, FALSE); } break;
		}
		return cs_val;
	}
}}}}}

/////////////////////////////////////////////////////////////////////////////

CDbPragma::CDbPragma(void) : m_id(CDbPragma::eNone) {}
CDbPragma::CDbPragma(const CDbPragma::_e _id, const variant_t& _val) : m_id(_id), m_value(_val) {}

/////////////////////////////////////////////////////////////////////////////

CDbPragma::_e CDbPragma::Identifier(void)const     { return m_id; }
HRESULT       CDbPragma::Identifier(const _e _id)  { if (CDbPragma::eNone == _id) return E_INVALIDARG; m_id = _id; return S_OK; }
bool          CDbPragma::IsValid(void)const        { return (CDbPragma::eNone != m_id && VT_EMPTY != m_value.vt); }
CAtlString    CDbPragma::Name(void)const           { return details::CDbAccess_PragmaToName(m_id); }
CAtlString    CDbPragma::ToString(void) const {
	CAtlString cs_pragma;
	CAtlString cs_value;
	switch (m_id) {
		case CDbPragma::eJournalInMemory:
			cs_value.Format(
					_T("%s"), (VARIANT_TRUE == m_value.boolVal ? _T("memory") : _T("off"))
					); break;
		case CDbPragma::eLockingMode:
			cs_value.Format(
				_T("%s"), (VARIANT_TRUE == m_value.boolVal ? _T("exclusive") : _T("normal"))
				); break;
		case CDbPragma::ePageSize:
			cs_value = details::CDbAccess_PragmaToStr(this->Value()); break;
	}

	if (cs_value.IsEmpty() == false){
		cs_pragma.Format(
			_T("pragma %s=%s;"), (LPCTSTR)this->Name(), (LPCTSTR)cs_value);
	}
	return cs_pragma;
}

const
variant_t&    CDbPragma::Value(void)const { return m_value; }
variant_t&    CDbPragma::Value(void)      { return m_value; }


/////////////////////////////////////////////////////////////////////////////

CDbPragma&    CDbPragma::operator=(const bool _val) { m_value = (_val ? VARIANT_TRUE : VARIANT_FALSE); return *this; }
CDbPragma&    CDbPragma::operator=(const INT  _val) { m_value = (LONG)_val; return *this; }

/////////////////////////////////////////////////////////////////////////////

CDbPragmaSet::CDbPragmaSet(void) {}

///////////////////////////////////////////////////////////////////////////////

HRESULT     CDbPragmaSet::Append(const CDbPragma& _pragma) {
	HRESULT hr_ = S_OK;
	if (_pragma.IsValid() == false)
		return (hr_ = E_INVALIDARG);
	if (this->Has(_pragma.Identifier()))
		return (hr_ = HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS));
	try {
		m_set.insert(
			::std::make_pair(_pragma.Identifier(), _pragma)
			);
	} catch (::std::bad_alloc&) {
		hr_ = E_OUTOFMEMORY;
	}
	return  hr_;
}

HRESULT     CDbPragmaSet::Append(const CDbPragma::_e _id, const variant_t& _value) {
	
	CDbPragma pragma(_id, _value);
	return  this->Append(pragma);
}

VOID        CDbPragmaSet::Clear (void) { if (m_set.empty() == false) m_set.clear(); }
CAtlString  CDbPragmaSet::Get   (const CDbPragma::_e _id) const {
	CAtlString cs_pragma;
	TPragmaSet::const_iterator it_ = m_set.find(_id);
	if (it_ != m_set.end()) {
		cs_pragma = it_->second.ToString();
	}
	return cs_pragma;
}

bool        CDbPragmaSet::Has   (const CDbPragma::_e _id) const { TPragmaSet::const_iterator it_ = m_set.find(_id); return (m_set.end() != it_); }
HRESULT     CDbPragmaSet::Remove(const CDbPragma::_e _id) {
	HRESULT hr_ = S_OK;
	TPragmaSet::iterator it_ = m_set.find(_id);
	if (it_ != m_set.end())
		m_set.erase(it_);
	else
		hr_ = HRESULT_FROM_WIN32(ERROR_NOT_FOUND);
	return  hr_;
}

HRESULT     CDbPragmaSet::Set(const CDbPragma::_e _id, const bool _val) {
	variant_t val_ = (_val ? VARIANT_TRUE : VARIANT_FALSE);
	return this->Set(_id, val_);
}

HRESULT     CDbPragmaSet::Set(const CDbPragma::_e _id, const INT _val) {
	variant_t val_ = (LONG)_val;
	return this->Set(_id, val_);
}

HRESULT     CDbPragmaSet::Set(const CDbPragma::_e _id, const variant_t& _value) {
	HRESULT hr_ = S_OK;
	if (this->Has(_id) == false)
		return (hr_ = HRESULT_FROM_WIN32(ERROR_NOT_FOUND));
	TPragmaSet::iterator it_ = m_set.find(_id);
	if (it_ == m_set.end())
		return (hr_ = E_UNEXPECTED);
	it_->second.Value() = _value;
	return  hr_;
}

CAtlString  CDbPragmaSet::ToString(void)const {
	CAtlString cs_value;
	for (TPragmaSet::const_iterator it_ = m_set.begin(); it_ != m_set.end(); ++it_) {
		cs_value += it_->second.ToString();
	}
	return cs_value;
}

/////////////////////////////////////////////////////////////////////////////

CDbAccessor::CDbAccessor(CDbWriterCached& _writer, CDbObject& _obj) : m_writer(_writer), m_object(_obj) {
	m_error.Source(_T("CDbAccessor"));

	CDbPragma::_e defaults[] = {
		CDbPragma::eJournalInMemory, CDbPragma::eLockingMode, CDbPragma::ePageSize
	};

	for (INT i_ = 0; i_ < _countof(defaults); i_++) {
		CDbPragma pragma_;
		switch (defaults[i_]) {
			case CDbPragma::eJournalInMemory: { pragma_.Identifier(defaults[i_]); pragma_ =  true; } break;  // in memory
			case CDbPragma::eLockingMode:     { pragma_.Identifier(defaults[i_]); pragma_ = false; } break;  // lockin is normal
			case CDbPragma::ePageSize:        { pragma_.Identifier(defaults[i_]); pragma_ =  4096; } break;  // page size
			default:
				continue;
		}
		HRESULT hr_ = m_prg_set.Append(pragma_);
		if (FAILED(hr_))
			break;
	}
}

CDbAccessor::~CDbAccessor(void) {
	if (this->IsOpen())
		this->Close ();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDbAccessor::Close(void) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (global::conn_ref().IsOpen() == false)  return m_error;

	m_writer.Suspend(true);
	m_writer.Cache().Clear();

	CDbConnection& cnn_ = global::conn_ref();
	try {
		cnn_.close();
		cnn_.IsOpen(false);
	}
	catch(database_error& _ex) {
		m_error.SetState(
				(DWORD)ERROR_DATABASE_FAILURE,
				CAtlString(_ex.what())
			);
	}
	return m_error;
}

TErrorRef     CDbAccessor::Error(void)const   { return m_error; }
bool          CDbAccessor::IsOpen(void)const  { return global::conn_ref().IsOpen(); }
const
CDbLocator&   CDbAccessor::Locator(void)const { return m_locator; }
CDbLocator&   CDbAccessor::Locator(void)      { return m_locator; }
HRESULT       CDbAccessor::Open(LPCTSTR lpszPath) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const CDataSchemata& schemas = m_object.Schemata();

	if (schemas.Error())
		return (m_error = schemas.Error());

	return this->Open(lpszPath, schemas.Default());
}
HRESULT       CDbAccessor::Open(LPCTSTR lpszPath, const CDataSchema& _schema) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (_schema.IsValid() == false)      return (m_error = _schema.Error());
	if (global::conn_ref().IsOpen())     return (m_error = (DWORD)ERROR_ALREADY_INITIALIZED);

	HRESULT hr_ = m_locator.Parse(lpszPath);
	if (FAILED(hr_))
		return (m_error = m_locator.Error());

	CDbConnection& cnn_ = global::conn_ref();
	try {

		cnn_.open(m_locator);
		cnn_.setbusytimeout(5000);
		cnn_.setextendedresultcodes(1);
		cnn_.executenonquery(m_prg_set.Get(CDbPragma::eJournalInMemory));
		cnn_.executenonquery(m_prg_set.Get(CDbPragma::eLockingMode));
		cnn_.executenonquery(m_prg_set.Get(CDbPragma::ePageSize));
		cnn_.IsOpen(true);

		sqlite3_lock lock_(cnn_, true);

		const TDataTableEnum& tables = _schema.Tables();
		for (size_t i_ = 0; i_ < tables.size(); i_++) {
			const CDataTable& tbl_ = tables[i_];
			if (tbl_.IsValid() == false)
				continue;
			if (details::CDbAccess__find_object(tbl_.Name(), cnn_, m_error))
				continue;

			hr_ = details::CDbAccess_CreateTable(tbl_, cnn_, m_error);
			if (FAILED(hr_))
				return hr_;

			const CDataIndexEnum& indices = tbl_.Indices();
			for (INT j_ = 0; j_ < indices.Count(); j_++) {

				const CDataIndex ndx_ = indices.Item(j_);
				if (ndx_.IsValid() == false)
					continue;
				hr_ = details::CDbAccess_CreateIndex(ndx_, cnn_, m_error);
				if (FAILED(hr_))
					return hr_;
			}
		}

		lock_.commit();
		
	} catch(database_error& _ex) {
		m_error.SetState(
				(DWORD)ERROR_DATABASE_FAILURE,
				CAtlString(_ex.what())
			);
	}
	if (m_error) {
		cnn_.IsOpen(false);
		cnn_.close();
	}
	return m_error;
}

const
CDbPragmaSet&  CDbAccessor::Pragma(void)const { return m_prg_set; }
CDbPragmaSet&  CDbAccessor::Pragma(void)      { return m_prg_set; }
/////////////////////////////////////////////////////////////////////////////

CDbLocator::CDbLocator(void) { m_error.Source(_T("CDbLocator")); }
CDbLocator::CDbLocator(LPCTSTR lpszPath) : m_path(lpszPath) { m_error.Source(_T("CDbLocator")); }

/////////////////////////////////////////////////////////////////////////////

TErrorRef      CDbLocator::Error(void)const  { return m_error; }
HRESULT        CDbLocator::Parse(LPCTSTR lpszPath) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;
	m_path  = lpszPath;
	if (m_path.IsEmpty() == false)
		return m_error;

	CDbSettingPersistent pers_(true);
	HRESULT hr_ = pers_.Load();
	if (FAILED(hr_))
		return (m_error = pers_.Error());

	const bool bCreateOption = pers_.CreateFolderOpt();
	CAtlString cs_folder     = pers_.FolderPath(true);
	if (cs_folder.IsEmpty()){
		
		hr_ = CApplication::GetFullPath(m_path);
		if (FAILED(hr_))
			return (m_error = hr_);

		if (bCreateOption)
		{
			CGenericFolder folder_(m_path);
			if (folder_.Path().IsExist() == false)
				folder_.Create();
		}
		pers_.FolderPath(
				m_path.GetString()
			);
		m_path = pers_.FullPath();
	}
	else {
		m_path += pers_.FullPath();
	}
	//
	// checks for file or folder type
	//
	CGenericPath path_(m_path);
	if (path_.IsFolder()) {
		m_error.SetState(
			(DWORD)ERROR_INVALID_DATA, _T("Database file path '%s' is not valid"), (LPCTSTR)m_path
			);
	}

	return m_error;
}
LPCTSTR        CDbLocator::Path(void)const { return m_path.GetString(); }

/////////////////////////////////////////////////////////////////////////////

CDbLocator::operator LPCTSTR(void)const    { return this->Path(); }

/////////////////////////////////////////////////////////////////////////////

CDbLocator& CDbLocator::operator= (LPCTSTR lpszPath) { m_path = lpszPath; return *this; }