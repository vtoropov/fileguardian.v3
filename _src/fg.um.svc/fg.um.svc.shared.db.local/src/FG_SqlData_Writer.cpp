/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Apr-2016 at 00:07:09am, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian background service shared library database writer class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 10:40:36p, UTC+7, Phuket, Rawai, Thursday;
*/
#include "StdAfx.h"
#include "FG_SqlData_Writer.h"

using namespace fg::common::data;
using namespace fg::common::data::local;

#include "Shared_SystemCore.h"
#include "Shared_GenericSyncObject.h"
#include "Shared_GenericEvent.h"

using namespace shared::lite::sys_core;
using namespace shared::runnable;

#include "sqlite3x_threaded.hpp"

using namespace sqlite3x;
/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data { namespace local { namespace details
{
	CAtlString CDbWriter_RecsToBatchAll        (const CDataSchema& _schema, const INT _where, const INT _by_what) {

		const TDataTableEnum& tables_ = _schema.Tables();
		if (tables_.size() == 0)
			return CAtlString();

		CAtlString cs_query;
		cs_query.Format(
			_T("update %s set status = %d where status = %d"),
			tables_[0].Name(), _by_what, _where
		);
		return cs_query;
	}

	CAtlString CDbWriter_RecsToBatchUpdateQuery(const CDataSchema& _schema, const INT _status, const TRecords& _recs)
	{
		const TDataTableEnum& tables_ = _schema.Tables();
		if (tables_.size() == 0)
			return CAtlString();

		CAtlString cs_query;
		cs_query.Format(
				_T("update %s set status = %d where status = 0 and time in ("),
				tables_[0].Name(), _status
			);

		for (size_t i_ = 0; i_ < _recs.size(); i_++)
		{
			const CRecord& rec_ = _recs[i_];
			CAtlString cs_time  = rec_.TimestampAsText(false);
			cs_query += cs_time;

			const bool bLast = (_recs.size() - 1 == i_);
			if (!bLast)
				cs_query += _T(",");
		}

		cs_query += _T(");");
		return cs_query;
	}
}}}}}
/////////////////////////////////////////////////////////////////////////////

CDbInserter::CDbInserter(CRecordCache& _cache, const CDataSchema& _schema) : m_cache(_cache), m_schema(_schema) {
	TBase::m_error.Source(_T("CDbInserter"));
}
CDbInserter::~CDbInserter(void) {}

/////////////////////////////////////////////////////////////////////////////

VOID          CDbInserter::ThreadFunction(void) {
	CCoInitializer com_core(false);

	CDataSchema schema_ = this->m_schema;
	CDbWriter writer_(schema_);

	while (!m_crt.IsStopped())
	{
		if (m_cache.IsEmpty()) {
			::Sleep(10);
			continue;
		}
		else if (m_crt.IsStopped())
			break;

		const CRecord rec_ = m_cache.Get();
		HRESULT hr_ = writer_.Insert(rec_);
		if (FAILED(hr_))
		{
			CEventJournal::LogError(writer_.Error());
			break;
		}
	}
	::SetEvent(m_crt.EventObject());
}

/////////////////////////////////////////////////////////////////////////////

CDbWriter::CDbWriter(const CDataSchema& _schema) : m_schema(_schema)  { TBase::m_error.Source(_T("CDbWriter")); }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDbWriter::Insert (const CRecord& _rec) {
	return this->InsertSync(_rec);
}

#if (1)
HRESULT       CDbWriter::Insert (LPCTSTR lpszTable, const time_t _time) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDbConnection& cnn_ = global::conn_ref();
	if (!cnn_.IsOpen())
		return (m_error = OLE_E_BLANK);

	try {
		sqlite3_try_lock lock_(cnn_, true);

		if (lock_.is_locked() == false)
			lock_.enter();

		CAtlString cs_query;
		cs_query.Format(
				_T("insert into %s values(?);"), lpszTable
			);

		sqlite3_command cmd_insert(cnn_, cs_query.GetString());

		cmd_insert.bind(1, _time);
		cmd_insert.executenonquery();

		lock_.commit();

	} catch(database_error& _ex) {
		m_error.SetState(
				(DWORD)ERROR_DATABASE_FAILURE,
				CAtlString(_ex.what())
			);
	}	
	return m_error;
}
#endif

HRESULT       CDbWriter::InsertSync  (const CRecord& _rec) {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDbConnection& cnn_ = global::conn_ref();
	if (!cnn_.IsOpen())
		return (m_error = OLE_E_BLANK);

	const TDataTableEnum& tables_ = m_schema.Tables();
	if (!tables_.size()) {
		return (m_error = m_schema.Error());
	}

	const INT n_fields_ = tables_[0].Fields().Count();

	CAtlString cs_pat;
	for (INT i_ = 0; i_ < n_fields_; i_++){
		cs_pat += _T("?");
		if (i_ < n_fields_ - 1)
			cs_pat += _T(",");
	}

	CAtlString cs_qry;
	cs_qry.Format(
			_T("insert into %s values(%s);"), tables_[0].Name(), cs_pat.GetString()
		);

	try {
		sqlite3_try_lock lock_(cnn_, true);

		if (lock_.is_locked() == false)
			lock_.enter();

		sqlite3_command cmd_insert(cnn_, cs_qry.GetString());
		//
		// TODO: binding must be reviewed for taking into account a schema provided;
		//
		if (n_fields_ > 0) cmd_insert.bind(1, _rec.Timestamp());
		if (n_fields_ > 1) cmd_insert.bind(2, _rec.Source());
		if (n_fields_ > 2) cmd_insert.bind(3, _rec.Target());
		if (n_fields_ > 3) cmd_insert.bind(4, _rec.Action());
		if (n_fields_ > 4) cmd_insert.bind(5, _rec.Status());
		if (n_fields_ > 5) cmd_insert.bind(6, _rec.Process());
		if (n_fields_ > 6) cmd_insert.bind(7, _rec.Type());
		if (n_fields_ > 7) cmd_insert.bind(8, _rec.TimestampEx());

		cmd_insert.executenonquery();

		lock_.commit();

	} catch(database_error& _ex) {
		m_error.SetState(
			(DWORD)ERROR_DATABASE_FAILURE,
			CAtlString(_ex.what())
		);
	}	
	return m_error;
}

HRESULT       CDbWriter::UpdateStatus(const CRecord& _rec, const INT _status)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDbConnection& cnn_ = global::conn_ref();

	if (!cnn_.IsOpen())
		return (m_error = OLE_E_BLANK);

	const TDataTableEnum& tables_ = m_schema.Tables();
	if (!tables_.size()) {
		return (m_error = m_schema.Error());
	}

	const bool bFormatted = false;
	CAtlString cs_time = _rec.TimestampAsText(bFormatted);

	try {
		sqlite3_try_lock lock_(cnn_, true);

		if (lock_.is_locked() == false)
			lock_.enter();

		CAtlString cs_sql;
		cs_sql.Format(
				_T("update %s set status = %d where time = %s and status = 0;"),
				tables_[0].Name(),
				_status,
				(LPCTSTR)cs_time
			);

		sqlite3_command cmd_update(cnn_, cs_sql);
		cmd_update.executenonquery();

		lock_.commit();

	} catch(database_error& _ex) {
		m_error.SetState(
				(DWORD)ERROR_DATABASE_FAILURE,
				_T("Local DB update error: desc=%s; timestamp=%s; file=%s; action=%d"),
				CAtlString(_ex.what()).GetString(), (LPCTSTR)cs_time, (LPCTSTR)_rec.Target(), _rec.Action()
			);
	}
	return m_error;
}

HRESULT       CDbWriter::UpdateStatus(const TRecords& _recs, const INT _status)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDbConnection& cnn_ = global::conn_ref();
	if (!cnn_.IsOpen())
		return (m_error = OLE_E_BLANK);

	const TDataTableEnum& tables_ = m_schema.Tables();
	if (tables_.empty()) {
		return (m_error = (DWORD)ERROR_INVALID_DATA);
	}

	try {
		sqlite3_try_lock lock_(cnn_, true);
		if (lock_.is_locked() == false)
			lock_.enter();

		CAtlString cs_sql = details::CDbWriter_RecsToBatchUpdateQuery(m_schema, _status, _recs);
		sqlite3_command cmd_update(cnn_, cs_sql);
		cmd_update.executenonquery();

		lock_.commit();

	} catch(database_error& _ex) {
		m_error.SetState(
				(DWORD)ERROR_DATABASE_FAILURE, _T("Local DB batch update error: desc=%s"), CAtlString(_ex.what()).GetString()
			);
	}
	return m_error;
}

#if (1)
HRESULT       CDbWriter::UpdateStatus(const INT _where, const INT _by_what) {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDbConnection& cnn_ = global::conn_ref();
	if (!cnn_.IsOpen())
		return (m_error = OLE_E_BLANK);

	const TDataTableEnum& tables_ = m_schema.Tables();
	if (tables_.empty()) {
		return (m_error = (DWORD)ERROR_INVALID_DATA);
	}

	try {
		sqlite3_try_lock lock_(cnn_, true);
		if (lock_.is_locked() == false)
			lock_.enter();

		CAtlString cs_sql = details::CDbWriter_RecsToBatchAll(m_schema, _where, _by_what);
		sqlite3_command cmd_update(cnn_, cs_sql);
		cmd_update.executenonquery();

		lock_.commit();

	} catch(database_error& _ex) {
		m_error.SetState(
			(DWORD)ERROR_DATABASE_FAILURE, _T("Local DB batch update error: desc=%s"), CAtlString(_ex.what()).GetString()
		);
	}
	return m_error;
}
#endif

CSysError&    CDbWriter::ErrorW(void) { return m_error; }
/////////////////////////////////////////////////////////////////////////////

CDbWriterCached::CDbWriterCached(const CDataSchema& _schema) : m_inserter(m_cache, _schema), m_suspended(true), TBase(_schema) {
	m_error.Source(_T("CDbWriterCached"));
}

/////////////////////////////////////////////////////////////////////////////

CRecordCache& CDbWriterCached::Cache(void) { return m_cache; }
HRESULT       CDbWriterCached::Insert (const CRecord& _rec)  {
	HRESULT hr_ = m_cache.Put(_rec);
	return (m_error = hr_);
}

HRESULT       CDbWriterCached::Suspend(const bool _suspend)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = m_error.GetHresult();

	if (_suspend)
		if (m_inserter.IsRunning())
			hr_ = m_inserter.Stop();

	if (_suspend == false)
		if (m_inserter.IsRunning() == false)
			hr_ = m_inserter.Start();

	m_suspended = _suspend;
	if (FAILED(hr_))
		TBase::ErrorW() = m_inserter.Error();

	return m_error;
}