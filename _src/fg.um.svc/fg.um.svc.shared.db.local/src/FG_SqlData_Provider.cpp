/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jan-2016 at 5:27:38am, GMT+7, Phuket, Rawai, Monday;
	This is File Guardian background service shared library database provider class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 10:34:51p, UTC+7, Phuket, Rawai, Thursday;
*/
#include "StdAfx.h"
#include "FG_SqlData_Provider.h"
#include "FG_SqlData_Model_ex.h"
#include "FG_SqlQuery_Parser.h"

using namespace fg::common::data;
using namespace fg::common::data::local;

#include "Shared_GenericAppObject.h"
#include "Shared_DateTime.h"

using namespace shared::user32;
using namespace shared::lite::common;
using namespace shared::lite::data;

#include "Shared_FS_GenericFolder.h"

using namespace shared::ntfs;
/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data { namespace local { namespace details
{
}}}}}

/////////////////////////////////////////////////////////////////////////////

CDbProvider::CDbProvider(const DWORD _opts) : 
	m_object(_opts), m_locked(false), m_access(m_writer, m_object), m_reader(m_object), m_writer(m_object.Schemata().Default()) {
	m_error.Source(_T("CDbProvider"));
}
CDbProvider::~CDbProvider(void) { m_access.Close(); }

/////////////////////////////////////////////////////////////////////////////

const
CDbAccessor&     CDbProvider::Accessor(void)const      { return m_access;  }
CDbAccessor&     CDbProvider::Accessor(void)           { return m_access;  }
HRESULT          CDbProvider::Compact(void) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDbConnection& cnn_ = global::conn_ref();
	if (!cnn_.IsOpen())
		return (m_error = OLE_E_BLANK);

	try {
		sqlite3_lock lock(cnn_, true);
		lock.commit(); {
			cnn_.executenonquery(_T("vacuum;"));
		}
	} catch(database_error& _ex) {
		m_error.SetState(
				(DWORD)ERROR_DATABASE_FAILURE, CAtlString(_ex.what())
			);
	}

	return m_error;
}

const CDbObject& CDbProvider::Database(void)const { return m_object; }
CDbObject&       CDbProvider::Database(void)      { return m_object; }

HRESULT          CDbProvider::Delete(LPCTSTR lpszTableName, const LONG _deleted, const bool _asc)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDbConnection& cnn_ = global::conn_ref();
	if (!cnn_.IsOpen())
		return (m_error = OLE_E_BLANK);

	CAtlString cs_table(lpszTableName);
	if (cs_table.IsEmpty())
		cs_table = _T("watch_log");

	CAtlString cs_order;
	if (_asc)
		cs_order = _T("asc");
	else
		cs_order = _T("desc");

	CAtlString cs_query;
	cs_query.Format(
			_T("delete from %s where _rowid_ in (select _rowid_ from %s order by 1 %s limit %d)"),
			(LPCTSTR)cs_table,
			(LPCTSTR)cs_table,
			(LPCTSTR)cs_order,
			_deleted
		);
	try {
		sqlite3_try_lock lock(cnn_, true);

		if (lock.is_locked() == false)
			lock.enter(); {
			cnn_.executenonquery(cs_query);
			lock.commit();
		}
	} catch(database_error& _ex) {
		m_error.SetState(
				(DWORD)ERROR_DATABASE_FAILURE, CAtlString(_ex.what())
			);
	}

	return m_error;
}

bool             CDbProvider::IsLocked(void)const    { return    m_locked; }
VOID             CDbProvider::Lock (const bool _val) { m_locked = _val;    }
CDbReader&       CDbProvider::Reader(void)           { return    m_reader; }
const CDbStats&  CDbProvider::Stats(void)const       { return     m_stats; }
CDbWriter&       CDbProvider::Writer(void)           { return    static_cast<CDbWriter&>(m_writer); }
CDbWriterCached& CDbProvider::WriterCached(void)     { return    m_writer; }

/////////////////////////////////////////////////////////////////////////////

CDbStats::CDbStats(void) { TBase::m_error.Source(_T("CDbStats")); }

/////////////////////////////////////////////////////////////////////////////

LONG         CDbStats::RowCount(const CFilter& _flt)const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDbConnection& cnn_ = global::conn_ref();
	if (!cnn_.IsOpen()) {
		m_error = OLE_E_BLANK; return -1;
	}

	LONG lResult = 0;
	const bool bLimited = _flt.Enabled() && (0 < _flt.Extension().RowLimit());

	try {
		sqlite3_try_lock lock_(cnn_, false);

		CAtlString cs_query;

		if (bLimited == true)
			cs_query = _T("select (1) from watch_log ");
		else
			cs_query = _T("select count(1) from watch_log ");

		if (_flt.Enabled()) {	
			CAtlString cs_criteria = _flt.ToString();
			if (!cs_criteria.IsEmpty()) {
				cs_query += _T(" where ");
				cs_query += cs_criteria;
			}

			if (_flt.Extension().RowLimit() > 0) {
				cs_query += _flt.Extension().RowLimitAsQuery();

				CAtlString cs_temp;
				cs_temp.Format(
						_T("select count(1) from (%s)"), (LPCTSTR)cs_query
					);
				cs_query = cs_temp;
			}
		}

		if (lock_.is_locked() == false)
			lock_.enter(); {
			lResult = static_cast<LONG>(cnn_.executeint(cs_query));
		}
	} catch(database_error& _ex) {
		m_error.SetState(
				(DWORD)ERROR_DATABASE_FAILURE, CAtlString(_ex.what())
			);
		lResult = -1;
	}
	return lResult;
}

LONG         CDbStats::RowCount(LPCTSTR lpszTableName)const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDbConnection& cnn_ = global::conn_ref();
	if (!cnn_.IsOpen()) {
		m_error = OLE_E_BLANK; return -1;
	}

	LONG lResult = 0;
	CAtlString cs_table(lpszTableName);

	if (cs_table.IsEmpty())
		cs_table = _T("watch_log");

	try {
		sqlite3_try_lock lock_(cnn_, false);

		CAtlString cs_query;
		cs_query.Format(
				_T("select count(1) from %s;"),
				(LPCTSTR)cs_table
			);

		if (lock_.is_locked() == false)
			lock_.enter(); {
			lResult = static_cast<LONG>(cnn_.executeint(cs_query));
		}
	} catch(database_error& _ex) {
		m_error.SetState(
				(DWORD)ERROR_DATABASE_FAILURE, CAtlString(_ex.what())
			);
		lResult = -1;
	}
	return lResult;
}

HRESULT      CDbStats::Totals  (const CFilter& _flt, TOperateTotals& _totals)const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CDbConnection& cnn_ = global::conn_ref();
	if (!cnn_.IsOpen()) {
		m_error = OLE_E_BLANK; return -1;
	}
	CDbQueryParser parser_;

	CAtlString cs_qry;
	HRESULT hr_ = parser_.Stats().Totals(_flt, cs_qry);
	if (FAILED(hr_))
		return (m_error = parser_.Error());

	try {
		sqlite3_try_lock lock_(cnn_, false);

		if (lock_.is_locked() == false)
			lock_.enter(); {
			sqlite3_command cmd_(cnn_, cs_qry);
			sqlite3_reader reader_ = cmd_.executereader();

			INT nIndex = 0;

			while (reader_.read()) {
				try {
					_totals.insert(
						::std::make_pair(eFsEventType::Type(nIndex++), reader_.getint(0))
						);
				} catch(::std::bad_alloc&) {
					m_error = E_OUTOFMEMORY; break;
				}
			}
		}
	} catch(database_error& _ex) {
		m_error.SetState(
				(DWORD)ERROR_DATABASE_FAILURE, CAtlString(_ex.what())
			);
	}
	return m_error;
}