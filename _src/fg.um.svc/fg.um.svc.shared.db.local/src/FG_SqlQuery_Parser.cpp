/*
	Created by Tech_dog (ebontrop@gmail.com) on 31-Mar-2018 at 5:55:48p, GMT+7, Novosibirsk, Rodniki, Saturday;
	This is File Guardian local database SQL query parser interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 10:42:34p, UTC+7, Phuket, Rawai, Thursday;
*/
#include "StdAfx.h"
#include "FG_SqlQuery_Parser.h"

using namespace fg::common::data;
using namespace fg::common::data::local;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data { namespace local { namespace details
{
	class CDbQueryStats_Base {

	protected:
		const
		CFilter&      m_flt;
		CSysError&    m_error;

	protected:
		CDbQueryStats_Base(const CFilter& _flt, CSysError& _err) : m_flt(_flt), m_error(_err) {}
	};

	class CDbQueryStats_Entire : private CDbQueryStats_Base {

		typedef CDbQueryStats_Base TBase;
	public:
		CDbQueryStats_Entire(const CFilter& _flt, CSysError& _err) : TBase(_flt, _err) {}

	public:

		HRESULT       Do(CAtlString& _qry) {
			m_error.Module(_T(__FUNCTION__));
			m_error = S_OK;

			LPCTSTR lpszQry = _T("select count(*) from watch_log where (action_id = %d)");

			for (INT i_ = 0; i_ < eFsEventType::Count(); i_++) {

				CAtlString sub_qry;
				sub_qry.Format(
						lpszQry, eFsEventType::Type(i_)
					);

				_qry += sub_qry;
				if (i_ != eFsEventType::Count() - 1)
				_qry += _T(" union all ");
			}

			return m_error;
		}
	};

	class CDbQueryStats_Finite : private CDbQueryStats_Base {

		typedef CDbQueryStats_Base TBase;
	public:
		CDbQueryStats_Finite(const CFilter& _flt, CSysError& _err) : TBase(_flt, _err) {}
	public:

		HRESULT       Do(CAtlString& _qry) {
			m_error.Module(_T(__FUNCTION__));
			m_error = S_OK;

			LPCTSTR lpszQry = _T("select count(*) from (%s)");

			for (INT i_ = 0; i_ < eFsEventType::Count(); i_++) {

				CAtlString base_qry;
				this->_GetBaseQuery(eFsEventType::Type(i_), base_qry);

				CAtlString sub_qry;
				sub_qry.Format(
						(LPCTSTR)lpszQry, (LPCTSTR)base_qry
					);

				_qry += sub_qry;
				if (i_ != eFsEventType::Count() - 1)
				_qry += _T(" union all ");
			}

			return m_error;
		}

	private:
		//
		// TODO: Limiting total records does not work, all actions are shown (if exist) with total limit per each;
		//       it is necessary to re-calculate limithing each action in accorfance with total one;
		//
		VOID    _GetBaseQuery(const eFsEventType::_e _type, CAtlString& _qry) {

			LPCTSTR lpszPattern  = _T("select action_id from watch_log where %s and (action_id = %d)");

			if (m_flt.Enabled() && m_flt.Validate()) {
				_qry.Format(
					lpszPattern, (LPCTSTR)m_flt.ToString(), _type 
				);
			}
			else {
				_qry.Format(
					lpszPattern, _T("1 = 1"), _type
				);
			}

			if (m_flt.Extension().RowLimit())
				_qry += m_flt.Extension().RowLimitAsQuery();
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CDbQueryParser::CStatistic::CStatistic(CSysError& _err) : m_error(_err) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT         CDbQueryParser::CStatistic::Totals(const CFilter& _flt, CAtlString& _qry)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const bool bLimited = _flt.Enabled() && _flt.Extension().RowLimit() > 0;

	if (bLimited) {
		details::CDbQueryStats_Finite qry_(_flt, m_error);
		HRESULT hr_ = qry_.Do(_qry);
		if (FAILED(hr_))
			return (m_error);
	}
	else {
		details::CDbQueryStats_Entire qry_(_flt, m_error);
		HRESULT hr_ = qry_.Do(_qry);
		if (FAILED(hr_))
			return (m_error);
	}


	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CDbQueryParser::CDbQueryParser(void) : m_stats(m_error)
{
	m_error.Source(_T("CDbQueryParser"));
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef       CDbQueryParser::Error(void)const { return m_error; }
CDbQueryParser::CStatistic&
CDbQueryParser::Stats(void)      { return m_stats; }