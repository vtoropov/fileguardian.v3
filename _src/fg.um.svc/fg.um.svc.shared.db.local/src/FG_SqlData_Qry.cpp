/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jun-2018 at 0:40:53a, UTC+7, Phuket, Rawai, Monday;
	This is File Guardian shared service library local database query interface implementation file.
*/
#include "StdAfx.h"
#include "FG_SqlData_Qry.h"

using namespace fg::common::data::local;

#include "Shared_DateTime.h"

using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

CDbParam::CDbParam(void) : m_time(0), m_long(0) {}
CDbParam::~CDbParam(void) {}

/////////////////////////////////////////////////////////////////////////////

bool         CDbParam::IsIdentifiable(LPCTSTR _lpsz_name)const {
	if (NULL == _lpsz_name || m_field_name.IsEmpty())
		return false;
	else
		return (0 == m_field_name.CompareNoCase(_lpsz_name));
}
bool         CDbParam::IsValid(void)const { return (m_field_name.IsEmpty() == false && (0 < m_long || 0 < m_time || 0 < m_text.GetLength())); }
LPCTSTR      CDbParam::Name(void)const    { return  m_field_name.GetString(); }
HRESULT      CDbParam::Name(LPCTSTR _val) { if (NULL == _val) return E_INVALIDARG; m_field_name = _val; return S_OK; }

CAtlString   CDbParam::ToString(void)const {

	CAtlString cs_param;

	if (m_type == CDataType::eReal) {
		CAtlString cs_value;
		CUnixTime  nx_tm(m_time);
		nx_tm.ToString(false, cs_value);

		cs_param.Format(_T("%s=%s"), m_field_name.GetString(), cs_value.GetString());
	}
	else if (m_type == CDataType::eInteger) {
		cs_param.Format(_T("%s=%d"), m_field_name.GetString(), m_long);
	}
	else
		cs_param.Format(_T("%s='%s'"), m_field_name.GetString(), m_text.GetString());

	return cs_param;
}

LONG         CDbParam::ValueAsLong(void)const         { return m_long; }
HRESULT      CDbParam::ValueAsLong(const LONG _val)   {
	if (0 == _val) return E_INVALIDARG; m_long = _val; m_type = CDataType::eInteger; return S_OK;
}

LPCTSTR      CDbParam::ValueAsText(void)const { return m_text.GetString(); }
VOID         CDbParam::ValueAsText(LPCTSTR _v){ m_text = _v; ; m_type = CDataType::eText; }

time_t       CDbParam::ValueAsTime(void)const         { return m_time; }
HRESULT      CDbParam::ValueAsTime(const time_t _val) {
	if (0 == _val) return E_INVALIDARG; m_time = _val; m_type = CDataType::eReal; return S_OK; }

CDataType::_e CDbParam::Type(void)const               { return m_type; }
VOID          CDbParam::Type(const CDataType::_e _v)  { m_type = _v;   }

/////////////////////////////////////////////////////////////////////////////

CDbParamEnum::CDbParamEnum(void) : TBase() { m_error.Source(_T("CDbParamEnum")); }
CDbParamEnum::~CDbParamEnum(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT CDbParamEnum::Append (LPCTSTR _lpsz_name, const LONG _value) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (-1 != this->Find(_lpsz_name))
		return (m_error = (DWORD)ERROR_ALREADY_EXISTS);

	CDbParam param_; param_.Name(_lpsz_name); param_.ValueAsLong(_value);
	if (false == param_.IsValid())
		return (m_error = E_INVALIDARG);

	try {
		m_params.push_back(param_);
	}catch(::std::bad_alloc&){
		m_error = E_OUTOFMEMORY;
	}

	return m_error;
}

HRESULT CDbParamEnum::Append (LPCTSTR _lpsz_name, LPCTSTR _value) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (-1 != this->Find(_lpsz_name))
		return (m_error = (DWORD)ERROR_ALREADY_EXISTS);

	CDbParam param_; param_.Name(_lpsz_name); param_.ValueAsText(_value);
	if (false == param_.IsValid())
		return (m_error = E_INVALIDARG);

	try {
		m_params.push_back(param_);
	}catch(::std::bad_alloc&){
		m_error = E_OUTOFMEMORY;
	}

	return m_error;
}

HRESULT CDbParamEnum::Append (LPCTSTR _lpsz_name, const time_t _value) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (-1 != this->Find(_lpsz_name))
		return (m_error = (DWORD)ERROR_ALREADY_EXISTS);

	CDbParam param_; param_.Name(_lpsz_name); param_.ValueAsTime(_value);
	if (false == param_.IsValid())
		return (m_error = E_INVALIDARG);

	try {
		m_params.push_back(param_);
	}catch(::std::bad_alloc&){
		m_error = E_OUTOFMEMORY;
	}

	return m_error;
}

INT     CDbParamEnum::Find   (LPCTSTR _lpsz_name) const {

	INT n_index = -1;
	if (NULL == _lpsz_name)
		return n_index;

	for (size_t i_ = 0; i_ < m_params.size(); i_++) {
		if (m_params[i_].IsIdentifiable(_lpsz_name)) {
			n_index = static_cast<INT>(i_); break;
		}
	}
	return n_index;
}

CAtlString   CDbParamEnum::ToString(void)const {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_params;

	if (this->IsValid() == false) {
		return cs_params;
	}

	for (size_t i_ = 0; i_ < m_params.size(); i_++) {
		cs_params += m_params.at(i_).ToString();
		if (i_ < m_params.size() - 1)
			cs_params += _T(" and ");
	}

	return cs_params;
}

const bool   CDbParamEnum::IsValid (void)const {

	bool b_valid = false;
	if (m_params.empty())
		return b_valid;
	else
		b_valid = true;

	for (size_t i_ = 0; i_ < m_params.size(); i_++){
		b_valid = m_params.at(i_).IsValid();
		if (b_valid == false)
			break;
	}

	return b_valid;
}

const
TDbQueryParams& CDbParamEnum::RawData (void)const { return m_params; } 

/////////////////////////////////////////////////////////////////////////////

CDbQuery::CDbQuery(void) : TBase() { TBase::m_error.Source(_T("CDbQuery")); }
CDbQuery::~CDbQuery(void){}

/////////////////////////////////////////////////////////////////////////////
const bool      CDbQuery::IsValid(void)const { return m_params.IsValid(); }
const
CDbParamEnum&   CDbQuery::Params (void)const { return m_params; }
CDbParamEnum&   CDbQuery::Params (void)      { return m_params; }

CAtlString      CDbQuery::ToString(const CDataSchema& _schema)const {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_qry;

	if (_schema.IsValid() == false) {
		m_error = _schema.Error(); return cs_qry;
	}

	CAtlString cs_field_spec =  _schema.TableOf(0).Fields().ToString();

	cs_qry.Format(
		_T("select * from %s where %s"),
		/*cs_field_spec.GetString(),*/
		_schema.TableOf(0).Name(), (LPCTSTR)m_params.ToString()
	);

	return cs_qry;
}
