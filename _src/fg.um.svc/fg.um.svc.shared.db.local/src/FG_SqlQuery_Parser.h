#ifndef _FGSQLQUERYPARSER_H_21D1E051_0075_45b9_A840_6A062ED1D46F_INCLUDE
#define _FGSQLQUERYPARSER_H_21D1E051_0075_45b9_A840_6A062ED1D46F_INCLUDE
/*
	Created by Tech_dog (ebontrop@gmail.com) on 31-Mar-2018 at 12:10:34p, GMT+7, Novosibirsk, Rodniki, Saturday;
	This is File Guardian local database SQL query parser interface declaration file.
*/
#include "Shared_SystemError.h"
#include "FG_SysEvent_Model.h"
#include "FG_SysEvent_Filter.h"

namespace fg { namespace common { namespace data { namespace local
{
	using shared::lite::common::CSysError;
	using fg::common::data::CFilter;
	using fg::common::data::TOperateTotals;

	class CDbQueryParser {

	public:
		class CStatistic {
			friend class CDbQueryParser;
		private:
			CSysError&  m_error;
		private:
			CStatistic(CSysError&);
		public:
			HRESULT     Totals(const CFilter&, CAtlString& _qry);
		};
	private:
		CSysError       m_error;
		CStatistic      m_stats;
	public:
		CDbQueryParser(void);

	public:
		TErrorRef       Error(void)const;
		CStatistic&     Stats(void);
	};
}}}}

#endif/*_FGSQLQUERYPARSER_H_21D1E051_0075_45b9_A840_6A062ED1D46F_INCLUDE*/