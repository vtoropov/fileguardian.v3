/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Apr-2016 at 1:33:41pm, GMT+7, Phuket, Rawai, Sunday;
	This is File Guardian background service database archive configuration class(es) implementation file.
*/
#include "StdAfx.h"
#include "FG_ArcData_Cfg.h"
#include "FG_Generic_Defs.h"

using namespace fg::common::data;
using namespace fg::common::data::local;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"

using namespace shared::lite::common;
using namespace shared::registry;
using namespace shared::user32;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data { namespace details
{
	class CArchive_Registry : private CRegistryPathFinder
	{
		typedef CRegistryPathFinder TBase;
	private:
		CRegistryStorage m_stg;
	public:
		CArchive_Registry(void) : 
		m_stg(TBase::CService::SettingsRoot(), CRegistryOptions::eDoNotModifyPath)
		{
		}
	public:
		HRESULT      Load(CArchiveSettings& _cfg)
		{
			LONG lValue = 0;
			m_stg.Load(
					this->_GetRegPath(),
					this->_GetEnabledNamedValue(),
					lValue,
					0
				);
			_cfg.Enabled(!!lValue);

			CAtlString cs_value;
			m_stg.Load(
					this->_GetRegPath(),
					this->_GetFolderNamedValue(),
					cs_value
				);
			if (cs_value.IsEmpty())
				_cfg.VolumeFolder(CArchiveDefaults::VolumeFolder());
			else
				_cfg.VolumeFolder(cs_value);

			m_stg.Load(
					this->_GetRegPath(),
					this->_GetThresholdNamedValue(),
					lValue,
					CArchiveDefaults::eThreshold
				);
			_cfg.Threshold().Value(lValue);

			m_stg.Load(
					this->_GetRegPath(),
					this->_GetTruncatedNamedValue(),
					lValue,
					CArchiveDefaults::eTruncated
				);
			_cfg.Truncated().Value(lValue);

			m_stg.Load(
					this->_GetRegPath(),
					this->_GetVolumesNamedValue(),
					lValue,
					CArchiveDefaults::eVolumes
				);

			HRESULT hr_ = S_OK;
			return  hr_;
		}

		HRESULT      Save(const CArchiveSettings& _cfg)
		{
			m_stg.Save(
					this->_GetRegPath(),
					this->_GetEnabledNamedValue(),
					static_cast<LONG>(_cfg.Enabled())
				);

			m_stg.Save(
					this->_GetRegPath(),
					this->_GetFolderNamedValue(),
					_cfg.VolumeFolder()
				);

			m_stg.Save(
					this->_GetRegPath(),
					this->_GetThresholdNamedValue(),
					_cfg.Threshold().Value()
				);

			m_stg.Save(
					this->_GetRegPath(),
					this->_GetTruncatedNamedValue(),
					_cfg.Truncated().Value()
				);

			m_stg.Save(
					this->_GetRegPath(),
					this->_GetVolumesNamedValue(),
					static_cast<LONG>(_cfg.Volumes())
				);

			HRESULT hr_ = S_OK;
			return  hr_;
		}
	private:
		CAtlString  _GetRegPath(void)const
		{
			CAtlString path_= TBase::CService::SettingsPath();
			path_ += _T("\\Archives");
			return path_;
		}
		CAtlString  _GetEnabledNamedValue  (void)const    { return CAtlString(_T("Enabled"));   }
		CAtlString  _GetFolderNamedValue   (void)const    { return CAtlString(_T("Folder"));    }
		CAtlString  _GetThresholdNamedValue(void)const    { return CAtlString(_T("Threshold")); }
		CAtlString  _GetTruncatedNamedValue(void)const    { return CAtlString(_T("Truncated")); }
		CAtlString  _GetVolumesNamedValue  (void)const    { return CAtlString(_T("Volumes"));   }
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CAtlString    CArchiveDefaults::VolumeFolder(void)
{
	return CAtlString(_T(".\\Archives"));
}

/////////////////////////////////////////////////////////////////////////////

CArchiveSettings::CArchiveSettings(const bool bInitToDefaults) : 
	m_enabled(false), m_vol_num(0), m_threshold(0, 1000, 500000), m_truncated(0, 100, 250000) 
{
	m_error.Source(_T("CArchiveSettings"));
	if (bInitToDefaults)
		this->Default();
}

/////////////////////////////////////////////////////////////////////////////

VOID          CArchiveSettings::Default(void)
{
	m_threshold.Value(CArchiveDefaults::eThreshold);
	m_truncated.Value(CArchiveDefaults::eTruncated);
	m_enabled = true;
	m_vol_num = CArchiveDefaults::eVolumes;
	m_vol_dir = CArchiveDefaults::VolumeFolder();
}

bool          CArchiveSettings::Enabled(void)const
{
	return m_enabled;
}

VOID          CArchiveSettings::Enabled(const bool _enabled)
{
	m_enabled = _enabled;
}

TErrorRef     CArchiveSettings::Error(void)const
{
	return m_error;
}

const
CRecordRange& CArchiveSettings::Threshold(void)const
{
	return m_threshold;
}

CRecordRange& CArchiveSettings::Threshold(void)
{
	return m_threshold;
}

const
CRecordRange& CArchiveSettings::Truncated(void)const
{
	return m_truncated;
}

CRecordRange& CArchiveSettings::Truncated(void)
{
	return m_truncated;
}

TErrorRef     CArchiveSettings::Validate (void)const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!m_threshold.IsValid())
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Threshold must be a numeric value in range from %d to %d"),
				m_threshold.Min(),
				m_threshold.Max()
			);
		return m_error;
	}

	if (!m_truncated.IsValid())
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Truncated records must be a numeric value in range from %d to %d"),
				m_truncated.Min(),
				m_truncated.Max()
			);
		return m_error;
	}

	if (m_threshold.Value() < m_truncated.Value())
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Threshold cannot be less than truncated records")
			);
		return m_error;
	}

	if (m_threshold.Value() - m_truncated.Value() < m_truncated.Min())
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Threshold must be greater than truncated value at least by %d records"),
				m_truncated.Min()
			);
		return m_error;
	}

	if (this->Enabled() && m_vol_num < 1)
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Archive volumes amount must be greater than zero")
			);
		return m_error;
	}

	if (this->Enabled() && m_vol_dir.IsEmpty())
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Archive folder path cannot be empty")
			);
		return m_error;
	}

	if (this->Enabled())
	{
		CAtlString folder_ = m_vol_dir;

		if (CApplication::IsRelatedToAppFolder(folder_))
		{
			CApplication& the_app = GetAppObjectRef();
			the_app.GetPathFromAppFolder(m_vol_dir, folder_);
		}
		if (!::PathFileExists(folder_))
		{
			m_error.SetState(
					(DWORD)ERROR_PATH_NOT_FOUND,
					_T("Specified archive folder doesn't exist")
				);
			return m_error;
		}
	}

	m_error.SetState(
			S_OK,
			_T("Archive settings are valid")
		);

	return m_error;
}

CAtlString    CArchiveSettings::VolumeFolder(void)const
{
	return m_vol_dir;
}

HRESULT       CArchiveSettings::VolumeFolder(LPCTSTR pszFolder)
{
	m_vol_dir = pszFolder;

	HRESULT hr_ = S_OK;
	return  hr_;
}

INT           CArchiveSettings::Volumes(void)const
{
	return m_vol_num;
}

HRESULT       CArchiveSettings::Volumes(const INT _volumes)
{
	m_vol_num = _volumes;

	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT       CArchiveSettings::Volumes(LPCTSTR lpszVolumesAsText)
{
	const INT nValue = ::_tstoi(lpszVolumesAsText);
	HRESULT hr_ = this->Volumes(nValue);
	return  hr_;
}

CAtlString    CArchiveSettings::VolumesAsText(void)const
{
	CAtlString cs_volumes;
	cs_volumes.Format(
			_T("%d"),
			m_vol_num
		);
	return cs_volumes;
}

/////////////////////////////////////////////////////////////////////////////

CArchiveSettingPersistent::CArchiveSettingPersistent(const bool bInitToDefaults) : TBase(bInitToDefaults)
{
	m_error.Source(_T("CArchiveSettingPersistent"));
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CArchiveSettingPersistent::Load(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	details::CArchive_Registry reg_;
	m_error = reg_.Load(*this);

	return m_error;
}

HRESULT       CArchiveSettingPersistent::Save(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	details::CArchive_Registry reg_;
	m_error = reg_.Save(*this);

	return m_error;
}