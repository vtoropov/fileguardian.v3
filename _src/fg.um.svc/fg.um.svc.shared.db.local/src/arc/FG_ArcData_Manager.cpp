/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-Mar-2016 at 6:30:08pm, GMT+8, Hong Kong, Hong Kong Island, Thursday;
	This is File Guardian background service database management class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 10:26:51p, UTC+7, Phuket, Rawai, Thursday;
*/
#include "StdAfx.h"
#include "FG_ArcData_Manager.h"

using namespace fg::common::data;
using namespace fg::common::data::local;

#include "Shared_SystemCore.h"
#include "Shared_GenericSyncObject.h"
#include "Shared_GenericEvent.h"

using namespace shared::lite::sys_core;
using namespace shared::runnable;
using namespace shared::ntfs;

/////////////////////////////////////////////////////////////////////////////

CArchiveList::CArchiveList(const CArchiveSettings& _cfg) : m_cfg(_cfg)
{
	m_error.Source(_T("CArchiveList"));
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef     CArchiveList::Error(void)const
{
	return m_error;
}

CAtlString    CArchiveList::GenerateArcPath(void)const
{
	CGenericPath path_(m_cfg.VolumeFolder());
	path_ = path_.ToAbsolute();
	path_.Normalize(CObjectType::eFolder);

	CAtlString cs_path;
	cs_path += path_;
	cs_path += CArchiveList::GenerateArcName();
	return cs_path;
}

TFileList     CArchiveList::GetList(const bool bFileNameOnly)const
{
	CGenericPath path_;
	//
	// an archive list expects a folder as an input argument;
	//
	HRESULT hr_= path_.BuildFrom(m_cfg.VolumeFolder(), CObjectType::eFolder);
	if (FAILED(hr_)) {
		m_error = hr_;
		return TFileList();
	}

	CAtlString cs_folder = path_.ToAbsolute();

	CGenericFolder folder_(cs_folder);

	TFileList files_;
	TFolderList subfolders_;

	hr_ = folder_.EnumerateItems(files_, subfolders_, _T("*.db"));
	if (FAILED(hr_))
	{
		m_error = folder_.Error();
		return TFileList();
	}
	else if (m_error)
		m_error = S_OK;

	if (!bFileNameOnly)
		for (TFileList::iterator it_ = files_.begin(); it_ != files_.end(); ++it_)
		{
			CAtlString file_;
			file_ += folder_.Path();
			file_ += *it_;
			*it_   = file_;
		}

	return files_;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString    CArchiveList::GenerateArcName(void)
{
	CAtlString cs_name;
	{
		SYSTEMTIME st_ = {0};
		::GetLocalTime(&st_);

		cs_name.Format(
				_T("FG archive on %04d-%02d-%02d %02dh%02dm.db"),
				st_.wYear  ,
				st_.wMonth ,
				st_.wDay   ,
				st_.wHour  ,
				st_.wMinute
			);
	}
	return cs_name;
}

/////////////////////////////////////////////////////////////////////////////

CArchiveManager::CArchiveManager(CDbProvider& _provider):m_db_provider(_provider)
{
	m_error.Source(_T("CArchiveManager"));
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef     CArchiveManager::Error (void)const
{
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CArchiveManager::CreateVolume(LPCTSTR lpszTarget)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CGenericFile src_(m_db_provider.Accessor().Locator());
	if (!src_.IsValid())
		return (m_error = src_.Error());

	HRESULT hr_ = src_.CopyTo(lpszTarget, false);
	if (FAILED(hr_))
		m_error = src_.Error();

	return m_error;
}

HRESULT       CArchiveManager::RemoveExcessVolumes(const CArchiveSettings& _cfg)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CArchiveList lst_(_cfg);
	TFileList volumes_ = lst_.GetList(false);

	if (lst_.Error())
		return (m_error = lst_.Error());

	const INT nLimit = _cfg.Volumes();
	const INT nCount = static_cast<INT>(volumes_.size());

	if (nLimit >= nCount + 1) // we need a slot for new volume, so, +1;
		return m_error;

	INT nDeleted = nCount - nLimit + 1;
	// we assume the list is sorted in ascending order: the old files are first;
	for (INT i_ = 0; i_ < nCount && nDeleted > 0; i_++)
	{
		CGenericFile volume_ = (LPCTSTR)volumes_[i_];
		if (volume_.IsValid())
		{
			HRESULT hr_ = volume_.Delete();
			if (FAILED(hr_))
				return (m_error = volume_.Error());
		}
		nDeleted -= 1;
	}

	return m_error;
}

VOID          CArchiveManager::ThreadFunction(void)
{
	CCoInitializer com_core(false);

#if defined(_DEBUG)
	CGenericWaitCounter wait_(10, 10000 * 01); // 10 sec(s)
#else
	CGenericWaitCounter wait_(10, 10000 * 60); // 10 min(s)
#endif

	while (!m_crt.IsStopped())
	{
		wait_.Wait();
		if (wait_.IsElapsed())
			wait_.Reset();
		else
			continue;
		CArchiveSettingPersistent pers_(true);
		HRESULT hr_ = pers_.Load();
		if (FAILED(hr_))
		{
			CEventJournal::LogError(
					pers_.Error()
				);
			pers_.Default();
		}
		if (m_crt.IsStopped())
			break;
		const LONG lThreshold = pers_.Threshold().Value();
		const LONG lRowCount  = m_db_provider.Stats().RowCount(NULL); // NULL means main table [watch_log]

		if (lRowCount == -1)
		{
			CEventJournal::LogError(
					m_db_provider.Error()
				);
			continue;
		}
		if (m_crt.IsStopped())
			break;
		// (1) checks record count threshold
		if (lRowCount < lThreshold)
			continue;

		const bool bEnabled = pers_.Enabled();

		// (2) removes excess volume(s) if any
		if (bEnabled)
			hr_ = this->RemoveExcessVolumes(pers_);
		if (FAILED(hr_))
			CEventJournal::LogError(
					m_error
				);
		if (m_crt.IsStopped())
			break;
		m_db_provider.WriterCached().Suspend(true);
		m_db_provider.Lock(true);

		// (3) creates new volume if enabled
		CArchiveList lst_(pers_);
		CAtlString cs_volume = lst_.GenerateArcPath();

		if (bEnabled)
		{
			hr_ = this->CreateVolume(cs_volume);
			if (FAILED(hr_))
			{
				m_db_provider.WriterCached().Suspend(false);
				m_db_provider.Lock(false);

				CEventJournal::LogError(
						m_error
					);
				continue;
			}
		}
		if (m_crt.IsStopped())
			break;
		// (4) truncates the records in main database
		const LONG lTruncated = pers_.Truncated().Value();
		const LONG lDeleted   = lRowCount - lTruncated;
		hr_ = m_db_provider.Delete(NULL, lDeleted, true); // removes the oldest records that exceed the truncated value (sorted by timestamp);
		if (m_crt.IsStopped())
			break;
		if (!FAILED(hr_))
			hr_ = m_db_provider.Compact();

		if (FAILED(hr_))
		{
			CEventJournal::LogError(
					m_db_provider.Error()
				);
		}

		if (m_crt.IsStopped())
			break;
		m_db_provider.WriterCached().Suspend(false); // enables records writing again;
		m_db_provider.Lock(false);

		// (5) truncates and compacts newly created archive volume if enabled
		if (bEnabled)
		{
			CDbProvider arc_;

			hr_ = arc_.Accessor().Open(cs_volume);
			if (!FAILED(hr_))
				hr_ = arc_.Delete(NULL, lTruncated, false); // removes all records that are remaining in truncated main database (newest records)

			if (m_crt.IsStopped())
				break;
			if (!FAILED(hr_))
				hr_ = arc_.Compact();

			if (FAILED(hr_))
			{
				CEventJournal::LogError(
						m_db_provider.Error()
					);
			}
		}
	}
	::SetEvent(m_crt.EventObject());
}