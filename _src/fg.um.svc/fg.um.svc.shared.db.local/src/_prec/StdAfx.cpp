/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Apr-2018 at 10:28:03a, GMT+7, Novosibirsk, Rodniki, Tuesday;
	This is File Guardian background service local database precompiled headers' implementation file.
*/
#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)

/////////////////////////////////////////////////////////////////////////////

CDbConnection::CDbConnection(void) : m_is_open(false) {}

/////////////////////////////////////////////////////////////////////////////

bool      CDbConnection::IsOpen(void)const       { return m_is_open; }
void      CDbConnection::IsOpen(const bool _val) { m_is_open = _val; }

/////////////////////////////////////////////////////////////////////////////

HRESULT CDbConnCreator::Create(PVOID &_ptr) {
	HRESULT hr_ = S_OK;
	try {
		_ptr = new CDbConnection();
	}
	catch(::std::bad_alloc&) {
		hr_ = E_OUTOFMEMORY;
	}
	return  hr_;
}

HRESULT CDbConnCreator::Destroy(PVOID& _ptr) {
	HRESULT hr_ = S_OK;
	if (NULL != _ptr) {
		try {
			delete _ptr; _ptr = NULL;
		}
		catch(...) {
			hr_ = HRESULT_FROM_WIN32(ERROR_INVALID_ADDRESS);
		}
	}
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

CDbConnCreator::operator const CDbConnection&(void)const { return m_conn; }
CDbConnCreator::operator       CDbConnection&(void)      { return m_conn; }

/////////////////////////////////////////////////////////////////////////////

namespace global {
	CDbConnection& conn_ref(void) {
		static CDbConnCreator creator_;
		return creator_;
	}
}