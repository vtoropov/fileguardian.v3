#ifndef _STDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED
#define _STDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Apr-2018 at 10:23:06p, GMT+7, Novosibirsk, Rodniki, Tuesday;
	This is File Guardian background service local database precompiled headers' declaration file.
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>
#include <comutil.h>
#include <vector>
#include <map>
#include <queue>

#include <math.h>
#include <time.h>

#pragma comment(lib, "lib.sqlite.adopted_v15.lib")
#pragma comment(lib, "lib.sqlite.ext.adopted_v15.lib")

#include "Shared_LogJournal.h"

using namespace shared::log;

#include "sqlite3x_threaded.hpp"

using namespace sqlite3x;

class CDbConnection : public threaded_sqlite3_connection {
private:
	bool       m_is_open;
public:
	CDbConnection(void);
public:
	bool      IsOpen(void)const;
	void      IsOpen(const bool);
};

class CDbConnCreator {
private:
	CDbConnection       m_conn;
public:
	operator const CDbConnection&(void)const;
	operator       CDbConnection&(void);
public:
	static HRESULT Create(PVOID& _ptr);    // deprecated;
	static HRESULT Destroy(PVOID& _ptr);   // deprecated;
};
namespace global {
	CDbConnection& conn_ref(void);
}
//
// TODO: dereferencing of the pointer must be protected against NULL;
//
#define _ref(_pvoid) *(reinterpret_cast<CDbConnection*>(_pvoid))

#endif/*_STDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED*/