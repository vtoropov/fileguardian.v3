/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Apr-2016 at 00:02:03am, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian background service shared library database settings interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 4-Jun-2018 at 9:08:32a, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "FG_SqlData_Settings.h"
#include "FG_SqlData_Model_ex.h"
#include "FG_Generic_Defs.h"

using namespace fg::common::data::local;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"

using namespace shared::lite::common;
using namespace shared::registry;

#include "Shared_FS_CommonDefs.h"

using namespace shared::ntfs;
/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data { namespace details
{
	class CDbSetting_Registry : private CRegistryPathFinder
	{
		typedef CRegistryPathFinder TBase;
	private:
		CRegistryStorage m_stg;
	public:
		CDbSetting_Registry(void) : 
		m_stg(TBase::CService::SettingsRoot(), CRegistryOptions::eDoNotModifyPath)
		{
		}
	public:
		HRESULT      Load(CDbSettings& _cfg)
		{
			CAtlString cs_value;
			m_stg.Load(
					this->_GetRegPath(),
					this->_GetFileNamedValue(),
					cs_value
				);
			if (cs_value.IsEmpty())
				_cfg.FileName(CDbSettingDefaults::FileName());
			else
				_cfg.FileName(cs_value);

			m_stg.Load(
					this->_GetRegPath(),
					this->_GetFolderNamedValue(),
					cs_value
				);
			if (cs_value.IsEmpty())
				_cfg.FolderPath(CDbSettingDefaults::FolderPath());
			else
				_cfg.FolderPath(cs_value);

			LONG lValue = 0;
			m_stg.Load(
					this->_GetRegPath(),
					this->_GetCreateNamedValue(),
					lValue,
					static_cast<LONG>(CDbSettingDefaults::CreateFolder())
				);
			_cfg.CreateFolderOpt(!!lValue);

			HRESULT hr_ = S_OK;
			return  hr_;
		}

		HRESULT      Save(const CDbSettings& _cfg)
		{
			m_stg.Save(
					this->_GetRegPath(),
					this->_GetFileNamedValue(),
					_cfg.FileName()
				);

			m_stg.Save(
					this->_GetRegPath(),
					this->_GetFolderNamedValue(),
					_cfg.FolderPath()
				);
			
			m_stg.Save(
					this->_GetRegPath(),
					this->_GetCreateNamedValue(),
					static_cast<LONG>(_cfg.CreateFolderOpt())
				);

			HRESULT hr_ = S_OK;
			return  hr_;
		}
	private:
		CAtlString  _GetRegPath(void)const
		{
			CAtlString path_= TBase::CService::SettingsPath();
			path_ += _T("\\MainDB");
			return path_;
		}
		CAtlString  _GetCreateNamedValue   (void)const    { return CAtlString(_T("CreateFolder")); }
		CAtlString  _GetFileNamedValue     (void)const    { return CAtlString(_T("FileName"));     }
		CAtlString  _GetFolderNamedValue   (void)const    { return CAtlString(_T("FolderPath"));   }
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CDbSettings::CDbSettings(const bool bInitToDefaults) : m_create(false)
{
	m_error.Source(_T("CDbSetting"));
	if (bInitToDefaults)
		this->Default();
}

/////////////////////////////////////////////////////////////////////////////

bool          CDbSettings::CreateFolderOpt(void)const { return m_create; }
VOID          CDbSettings::CreateFolderOpt(const bool _val) { m_create = _val; }

VOID          CDbSettings::Default(void)
{
	m_file   = CDbSettingDefaults::FileName();
	m_folder = CDbSettingDefaults::FolderPath();
	m_create = CDbSettingDefaults::CreateFolder();
}

TErrorRef     CDbSettings::Error(void)const { return m_error; }
CAtlString    CDbSettings::FileName(void)const { return m_file; }

HRESULT       CDbSettings::FileName(LPCTSTR lpszFileName)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_name(lpszFileName);
	if (cs_name.IsEmpty() == false)
		cs_name.Trim();

	if (cs_name.IsEmpty())
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Database file name cannot be empty")
			);
		return m_error;
	}
	m_file = cs_name;

	return m_error;
}

CAtlString    CDbSettings::FolderPath(const bool bUnwind)const
{
	if (!bUnwind)
		return  m_folder;
	else
	{
		CGenericPath path_(m_folder);
		path_ = path_.ToAbsolute();
		path_.Normalize(CObjectType::eFolder);

		CAtlString cs_path = static_cast<LPCTSTR>(path_);

		return cs_path;
	}
}

HRESULT       CDbSettings::FolderPath(LPCTSTR lpszFolderPath)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_path(lpszFolderPath);
	if (cs_path.IsEmpty() == false)
		cs_path.Trim();

	if (cs_path.IsEmpty())
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Main database path cannot be empty")
			);
		return m_error;
	}
	m_folder = cs_path;

	return m_error;
}

CAtlString    CDbSettings::FullPath(void)const
{
	HRESULT hr_ = this->Validate();
	if (FAILED(hr_))
		return CAtlString();

	CAtlString cs_path;
	cs_path += this->FolderPath(true);
	cs_path += m_file;
	return cs_path;
}

HRESULT       CDbSettings::Validate(void)const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (false){}
	else if (m_file.IsEmpty())
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Main database file name cannot be empty")
			);
	}
	else if (m_folder.IsEmpty())
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Main database folder path cannot be empty")
			);
	}
	else if (!m_create)
	{
		CGenericPath path_(m_folder);
		if (!path_.IsExist())
			m_error.SetState(
				E_INVALIDARG,
				_T("Main database folder doesn't exist and create option is off")
			);
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

bool       CDbSettingDefaults::CreateFolder(void) { return true; }
CAtlString CDbSettingDefaults::FileName(void)     { return CAtlString(_T("fw_watch_log.db")); }
CAtlString CDbSettingDefaults::FolderPath(void)   { return CAtlString(_T(".\\")); }

/////////////////////////////////////////////////////////////////////////////

CDbSettingPersistent::CDbSettingPersistent(const bool bInitToDefaults) : TBase(bInitToDefaults)
{
	m_error.Source(_T("CDbSettingPersistent"));
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDbSettingPersistent::Load(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	details::CDbSetting_Registry reg_;
	m_error = reg_.Load(*this);

	return m_error;
}

HRESULT       CDbSettingPersistent::Save(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	details::CDbSetting_Registry reg_;
	m_error = reg_.Save(*this);

	return m_error;
}