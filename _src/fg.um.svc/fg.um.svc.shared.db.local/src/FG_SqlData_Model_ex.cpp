/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Apr-2018 at 9:15:55p, UTC+7, Novosibirsk, Rodniki, Monday;
	This is File Guardian background service local database structure/model interface implementation file.
*/
#include "StdAfx.h"
#include "FG_SqlData_Model_ex.h"
#include "FG_SqlData_Resource.h"

using namespace fg::common::data::local;

#include "Shared_GenericXmlProvider.h"
#include "Shared_FS_GenericFile.h"
#include "Shared_GenericAppResource.h"

using namespace shared::lite::data;
using namespace shared::ntfs;
using namespace shared::user32;

#include "sqlite3x_threaded.hpp"

using namespace sqlite3x;
/////////////////////////////////////////////////////////////////////////////

CDataBase::CDataBase(LPCTSTR lpszName) : m_name(lpszName) {}
CDataBase::~CDataBase(void) {}

/////////////////////////////////////////////////////////////////////////////

bool    CDataBase::IsIdentifiable(LPCTSTR lpszName)const { return (NULL != lpszName && !m_name.IsEmpty() && 0 == m_name.CompareNoCase(lpszName));}
bool    CDataBase::IsValid(void)const { return CDataBase::Validate(m_name);}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR CDataBase::Name(void)const { return m_name.GetString(); }
HRESULT CDataBase::Name(LPCTSTR lpszName) {
	HRESULT hr_ = S_OK;
	if (CDataBase::Invalidate(lpszName))
		return (hr_ = E_INVALIDARG);
	else
		m_name = lpszName;
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

bool  CDataBase::Identify(LPCTSTR lpszWhat, LPCTSTR lpszWith) {
	if (CDataBase::Invalidate(lpszWhat) ||
	    CDataBase::Invalidate(lpszWith))return false;
	CAtlString cs_with(lpszWith);
	return (0 == cs_with.CompareNoCase(lpszWhat));
}
bool  CDataBase::Invalidate(LPCTSTR _lp_val) { return (NULL == _lp_val || 1 > ::_tcslen(_lp_val)); }
bool  CDataBase::Validate(LPCTSTR _lp_val) { return (NULL != _lp_val && 0 < ::_tcslen(_lp_val)); }

/////////////////////////////////////////////////////////////////////////////

CDataBase::operator LPCTSTR(void)const { return m_name.GetString(); }

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data { namespace local { namespace details
{
	CDataType::_e CDataSchema_StrToType(const CAtlString& _str) {
		CDataType::_e type_ = CDataType::eNone;

		if (0 == _str.CollateNoCase(_T("real")))    type_ = CDataType::eReal;
		if (0 == _str.CollateNoCase(_T("text")))    type_ = CDataType::eText;
		if (0 == _str.CollateNoCase(_T("integer"))) type_ = CDataType::eInteger;

		return type_;
	}
}}}}}

/////////////////////////////////////////////////////////////////////////////

CDataField::CDataField(void) : m_type(CDataType::eNone) {}
CDataField::CDataField(const CDataType::_e _type, LPCTSTR lpszName) : m_type(_type), TBase(lpszName) {}
CDataField::~CDataField(void) {}

/////////////////////////////////////////////////////////////////////////////

bool          CDataField::IsValid(void)const             { return (CDataType::eNone != m_type && TBase::IsValid()); }
CDataType::_e CDataField::Type(void)const                { return m_type; }
VOID          CDataField::Type(const CDataType::_e _val) { m_type = _val; }
CAtlString    CDataField::TypeAsText(void)const          {
	CAtlString cs_type;
	switch (this->m_type) {
		case CDataType::eInteger: cs_type = _T("integer"); break;
		case CDataType::eReal:    cs_type = _T("real");    break;
		case CDataType::eText:    cs_type = _T("text");    break;
	}
	return cs_type;
}

/////////////////////////////////////////////////////////////////////////////

CDataFieldEnum::CDataFieldEnum(void) {}
CDataFieldEnum::~CDataFieldEnum(void) {}

/////////////////////////////////////////////////////////////////////////////

const TDataFieldEnum&
              CDataFieldEnum::All(void)const { return m_fields; }
HRESULT       CDataFieldEnum::Append(const CDataField& _fld) {

	HRESULT hr_ = S_OK;

	if (!_fld.IsValid())
		return (hr_ = HRESULT_FROM_WIN32(ERROR_INVALID_DATA));
	if (-1 != this->Find(_fld.Name())) {
		return (hr_ = HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS));
	}
	try {
		m_fields.push_back(_fld);
	}
	catch (::std::bad_alloc&) {
		hr_ = E_OUTOFMEMORY;
	}
	
	return  hr_;
}

HRESULT       CDataFieldEnum::Append(const CDataType::_e _type, LPCTSTR lpszName) {

	CDataField fld_(_type, lpszName);

	return this->Append(fld_);
}

VOID          CDataFieldEnum::Clear(void) {
	if (m_fields.empty() == false)
		m_fields.clear();
}

INT           CDataFieldEnum::Count(void)const {
	return static_cast<INT>(m_fields.size());
}

INT           CDataFieldEnum::Find (LPCTSTR lpszName)const {
	INT result_= -1;
	for (size_t i_ = 0; i_ < m_fields.size(); i_++) {
		const CDataField& fld_ = m_fields[i_];
		if (fld_.IsIdentifiable(lpszName))
			return (result_ = static_cast<INT>(i_));
	}
	return result_;
}

bool          CDataFieldEnum::IsEmpty(void)const {
	return m_fields.empty();
}

CDataField    CDataFieldEnum::Item (const INT _ndx)const {
	if (0 > _ndx || _ndx > this->Count() - 1)
		return CDataField();
	else
		return m_fields[_ndx];
}

CAtlString    CDataFieldEnum::ToString(void)const {
	CAtlString cs_enum;
	for (size_t i_ = 0; i_ < m_fields.size(); i_++) {
		cs_enum += m_fields[i_];
		if (i_ != m_fields.size() - 1)
			cs_enum += _T(",");
	}
	return cs_enum;
}

/////////////////////////////////////////////////////////////////////////////

CDataIndex::CDataIndex(void) {}
CDataIndex::CDataIndex(LPCTSTR lpszName, LPCTSTR lpszField, LPCTSTR lpszTable) : TBase(lpszName), m_field(lpszField), m_table(lpszTable) {}
CDataIndex::~CDataIndex(void) {}

/////////////////////////////////////////////////////////////////////////////

bool          CDataIndex::IsIdentifiable(LPCTSTR lpszField)const {
	if (CDataBase::Invalidate(lpszField))
		return false;
	else
		return CDataBase::Identify(m_field, lpszField);
}

bool          CDataIndex::IsValid(void)const      {
	return (TBase::IsValid() && CDataBase::Validate(m_field.GetString())  && CDataBase::Validate(m_table.GetString()));
}
LPCTSTR       CDataIndex::Field(void) const       { return m_field.GetString(); }
HRESULT       CDataIndex::Field(LPCTSTR lpszName) {
	HRESULT hr_ = S_OK;
	if (CDataBase::Validate(lpszName))
		hr_ = HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS);
	else
		m_field = lpszName;
	return  hr_;
}
LPCTSTR       CDataIndex::Table(void) const       { return m_table.GetString(); }
HRESULT       CDataIndex::Table(LPCTSTR lpszName) {
	HRESULT hr_ = S_OK;
	if (CDataBase::Invalidate(lpszName))
		hr_ = E_INVALIDARG;
	else
		m_table = lpszName;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CDataIndexEnum::CDataIndexEnum(void) {}
CDataIndexEnum::~CDataIndexEnum(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDataIndexEnum::Append(const CDataIndex& _ndx) {
	HRESULT hr_ = S_OK;
	if (!_ndx.IsValid())
		return (hr_ = HRESULT_FROM_WIN32(ERROR_INVALID_DATA));
	//
	// TODO: is it possible to have more than one index per field?
	//
	if (-1 != this->Find(_ndx.Field()))
		return (hr_ = HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS));
	try {
		m_indices.push_back(_ndx);
	}catch(::std::bad_alloc&) {
		hr_ = E_OUTOFMEMORY;
	}
	return hr_;
}

HRESULT       CDataIndexEnum::Append(LPCTSTR lpszName, LPCTSTR lpszField, LPCTSTR lpszTable) {
	HRESULT hr_ = S_OK;
	if (CDataBase::Invalidate(lpszField) ||
	    CDataBase::Invalidate(lpszName)  ||
	    CDataBase::Invalidate(lpszTable))
		return (hr_ = E_INVALIDARG);

	CDataIndex ndx_(lpszName, lpszField, lpszTable);
	
	return (hr_ = this->Append(ndx_));
}

VOID          CDataIndexEnum::Clear(void) {
	if (m_indices.empty() == false)
		m_indices.clear();
}

INT           CDataIndexEnum::Count(void)const      { return static_cast<INT>(m_indices.size()); }
INT           CDataIndexEnum::Find (LPCTSTR lpszField)const {
	INT result_= -1;
	for (size_t i_ = 0; i_ < m_indices.size(); i_++) {
		const CDataIndex& ndx_ = m_indices[i_];

		if (CDataBase::Identify(lpszField, ndx_.Field()))
			return (result_ = static_cast<INT>(i_));
	}
	return result_;
}

bool          CDataIndexEnum::IsEmpty(void)const    { return m_indices.empty(); }
const
CDataIndex    CDataIndexEnum::Item(const INT _ndx)const {
	if (0 > _ndx || _ndx > this->Count() - 1)
		return CDataIndex();
	else
		return m_indices[_ndx];
}

CDataIndex    CDataIndexEnum::Item(const INT _ndx) {
	if (0 > _ndx || _ndx > this->Count() - 1)
		return CDataIndex();
	else
		return m_indices[_ndx];
}

/////////////////////////////////////////////////////////////////////////////

CDataTable::CDataTable(LPCTSTR lpszName) : TBase(lpszName) {}
CDataTable::~CDataTable(void) {}

/////////////////////////////////////////////////////////////////////////////

bool              CDataTable::IsValid(void)const    {
	return (TBase::IsValid() && !m_fields.IsEmpty());
}

/////////////////////////////////////////////////////////////////////////////

const
CDataFieldEnum&   CDataTable::Fields(void)const     { return m_fields;  }
CDataFieldEnum&   CDataTable::Fields(void)          { return m_fields;  }
const
CDataIndexEnum&   CDataTable::Indices(void)const    { return m_indices; }
CDataIndexEnum&   CDataTable::Indices(void)         { return m_indices; }

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data { namespace local { namespace details
{
	TErrorRef CDataSchema_CreateField(const CXmlNode& _node, CDataTable& _tbl, CSysError& _err) {
		_err.Module(_T(__FUNCTION__));
		_err = S_OK;

		const CXmlNode& xml_nodes = _node.Child(_T("fields"));
		if (!xml_nodes.IsValid()) {
			_err.SetState(
				(DWORD)ERROR_INVALID_DATA, _T("Table node does not have <fields> child")
				);
			return _err;
		}

		const TXmlNodes& xml_array = xml_nodes.Children();
		if (xml_array.empty()) {
			_err.SetState(
				(DWORD)ERROR_INVALID_DATA, _T("Field children collection is empty")
				);
			return _err;
		}

		for (size_t i_ = 0; i_ < xml_array.size(); i_++) {

			const CXmlNode& xml_item = xml_array[i_];
			CDataField fld_(
				details::CDataSchema_StrToType(xml_item.Attribute(_T("type"))), xml_item.Attribute(_T("name"))
				);

			HRESULT hr_ = _tbl.Fields().Append(fld_);
			if (FAILED(hr_)){
				_err = hr_; break;
			}
		}

		return _err;
	}

	TErrorRef CDataSchema_CreateIndex(const CXmlNode& _node, CDataTable& _tbl, CSysError& _err) {
		_err.Module(_T(__FUNCTION__));
		_err = S_OK;

		const CXmlNode& xml_nodes = _node.Child(_T("indices"));
		if (xml_nodes.IsValid()) {
			const TXmlNodes& xml_array = xml_nodes.Children();
			for (size_t i_ = 0; i_ < xml_array.size(); i_++) {

				const CXmlNode& ndx_node = xml_array[i_];

				HRESULT hr_ = _tbl.Indices().Append(
					ndx_node.Attribute(_T("name")), ndx_node.Attribute(_T("field")), _tbl.Name()
					);
				if (FAILED(hr_)) {
					_err.SetState(
						(DWORD)ERROR_INVALID_DATA, _T("Field index is not valid")
						);
					return _err;
				}
			}
		}
		return _err;
	}

	TErrorRef CDataSchema_CreateTable(const CXmlNode& _node, CDataTable& _tbl, CSysError& _err) {
		_err.Module(_T(__FUNCTION__));
		_err = S_OK;

		if (!_node.IsValid()) {
			_err.SetState(
				(DWORD)ERROR_INVALID_DATA, _T("XML <table> node is invalid")
				);
			return _err;
		}
		_tbl.Name(_node.Attribute(_T("name")));

		HRESULT hr_ = S_OK;
		
		hr_ = CDataSchema_CreateField(_node, _tbl, _err);
		if (FAILED(hr_))
			return _err;

		hr_ = CDataSchema_CreateIndex(_node, _tbl, _err);

		return _err;
	}
}}}}}

/////////////////////////////////////////////////////////////////////////////

CDataSchema::CDataSchema(LPCTSTR lpszName) : TBase(lpszName) {
	m_error.Source(_T("CDataSchema"));
	m_error.Module(_T(__FUNCTION__));
	m_error = OLE_E_BLANK;
}

CDataSchema::~CDataSchema(void) {}

/////////////////////////////////////////////////////////////////////////////

bool          CDataSchema::IsValid(void)const { return (!m_error && !m_tables.empty() && TBase::IsValid()); }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDataSchema::Append(const CDataTable& _tbl) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!_tbl.IsValid()) {
		m_error.SetState(
				(DWORD)ERROR_INVALID_DATA, _T("Table being provided is not valid")
			);
		return m_error;
	}

	if (-1 != this->Find(_tbl.Name())) {
		m_error.SetState(
				(DWORD)ERROR_ALREADY_EXISTS, _T("Table [%s] is already appended"), _tbl.Name()
			);
		return m_error;
	}

	try {
		m_tables.push_back(_tbl);
	}
	catch(::std::bad_alloc&) {
		m_error = E_OUTOFMEMORY;
	}
	return m_error;
}

HRESULT       CDataSchema::Create(LPCTSTR lpszXmlStream)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (CDataBase::Invalidate(lpszXmlStream))
		return (m_error = (DWORD)ERROR_INVALID_DATA);
	else
		m_source = lpszXmlStream;

	CXmlDataProvider prov_;
	HRESULT hr_ = prov_.Create(lpszXmlStream);
	if (FAILED(hr_))
		return (m_error = prov_.Error());

	const CXmlNode& root_ = prov_.Root();
	if (!root_.IsValid()) {
		m_error.SetState(
			(DWORD)ERROR_INVALID_DATA, _T("Root element is not defined")
			);
		return m_error;
	}
	this->m_name = root_.Attribute(_T("name"));
	
	const TXmlNodes& xml_tables = root_.Children();
	for (size_t i_ = 0; i_ < xml_tables.size(); i_++) {

		const CXmlNode& xml_item = xml_tables[i_];
		CDataTable table_;
		hr_ = details::CDataSchema_CreateTable(xml_item, table_, m_error);
		if (FAILED(hr_))
			break;

		hr_ = this->Append(table_);
		if (FAILED(hr_))
			break;
	}
	
	if (m_tables.empty()) {
		m_error.SetState(
			(DWORD)ERROR_INVALID_DATA, _T("No table is defined.")
		);
	}

	return m_error;
}

VOID          CDataSchema::Clear (void) {
		if (false == m_tables.empty()) m_tables.clear();

		m_error.Module(_T(__FUNCTION__));
		m_error = OLE_E_BLANK;            // it is required for pre-defined state when calling IsValid() property;
}

TErrorRef     CDataSchema::Error(void)const { return m_error; }

INT           CDataSchema::Find (LPCTSTR lpszTableName)const {
	INT result_= -1;
	for (size_t i_ = 0; i_ < m_tables.size(); i_++) {
		const CDataTable& tbl_ = m_tables[i_];

		if (tbl_.IsIdentifiable(lpszTableName))
			return (result_ = static_cast<INT>(i_));
	}
	return result_;
}

HRESULT       CDataSchema::Remove (LPCTSTR lpszTable) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (CDataBase::Invalidate(lpszTable))
		return (m_error = E_INVALIDARG);

	const INT nIndex = this->Find(lpszTable);
	if (-1 == nIndex)
		return (m_error = TYPE_E_ELEMENTNOTFOUND);

	m_tables.erase(m_tables.begin() + nIndex);

	if (m_tables.empty()) {
		m_error = OLE_E_BLANK;
	}

	return m_error;
}

const TDataTableEnum&
              CDataSchema::Tables(void)const  { return m_tables; }

const
CDataTable&   CDataSchema::TableOf (const INT _index)const {
	if (0 > _index || _index > static_cast<INT>(m_tables.size()) - 1) {
		static CDataTable invalid_;
		return invalid_;
	}
	else
		return m_tables[_index];
}

LPCTSTR       CDataSchema::ToString(void)const {
	return m_source.GetString();
}

/////////////////////////////////////////////////////////////////////////////

CDataSchemaLocator::CDataSchemaLocator(void) {
	m_error.Source(_T("CDataSchemaLocator"));
}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR       CDataSchemaLocator::Data (void)const   { return m_data.GetString(); }
TErrorRef     CDataSchemaLocator::Error(void)const   { return m_error; }
HRESULT       CDataSchemaLocator::LoadFromFile(LPCTSTR lpszPath) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (CDataBase::Invalidate(lpszPath))
		return (m_error = E_INVALIDARG);

	CGenericFile file_(lpszPath);

	HRESULT hr_ = file_.ReadAsText(m_data);
	if (FAILED(hr_))
		return (m_error = file_.Error());

	return m_error;
}

HRESULT       CDataSchemaLocator::LoadFromResource(const UINT nResId) {
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!nResId)
		return (m_error = E_INVALIDARG);

	CGenericResourceLoader loader_;

	CRawData buffer_;
	HRESULT hr_ = loader_.LoadRcDataAsUtf8(nResId, buffer_);
	if (FAILED(hr_))
		return (m_error = loader_.Error());

	CAtlStringA cs_data;
	hr_ = buffer_.ToStringUtf8(cs_data);
	if (FAILED(hr_))
		return (m_error = loader_.Error());
	else
		m_data = cs_data;

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CDataSchemata::CDataSchemata(const DWORD _ops){

	m_error.Source(_T("CDataSchemata"));
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (CDataSchemaOpt::eLoadDefault != _ops)
		return;
	CDataSchemaLocator locator_;
	HRESULT hr_ = locator_.LoadFromResource(IDR_FG_DB_LOC_SCHEMA_DEFAULT);
	if (SUCCEEDED(hr_))
		m_default.Create(locator_.Data());
	else
		m_error = locator_.Error();
}

CDataSchemata::~CDataSchemata(void) {}

/////////////////////////////////////////////////////////////////////////////

const
TDataSchemata& CDataSchemata::All(void)const { return m_schemas; }

HRESULT        CDataSchemata::Append(const CDataSchema& _schema) {

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!_schema.IsValid())
		return (m_error = E_INVALIDARG);

	try {
		m_schemas.push_back(_schema);
	} catch (::std::bad_alloc&) {
		m_error = E_OUTOFMEMORY;
	}

	return  m_error;
}

const
CDataSchema&   CDataSchemata::Default(void)const   { return m_default; }
CDataSchema&   CDataSchemata::Default(void)        { return m_default; }
TErrorRef      CDataSchemata::Error  (void)const   { return m_error;   }

const
CDataSchema&   CDataSchemata::SchemaOf(const INT nIndex)const {
	if (0 > nIndex || nIndex > (INT)m_schemas.size()) {
		static CDataSchema invalid_(_T("#n/a"));
		return invalid_;
	}
	else
		return m_schemas[nIndex];
}

CDataSchema&   CDataSchemata::SchemaOf(const INT nIndex) {
	if (0 > nIndex || nIndex > (INT)m_schemas.size()) {
		static CDataSchema invalid_(_T("#n/a"));
		return invalid_;
	}
	else
		return m_schemas[nIndex];
}

/////////////////////////////////////////////////////////////////////////////

CDbObjectBase::CDbObjectBase(const PVOID& _p_conn) : m_con_ref(_p_conn) { TBase::m_error.Source(_T("CDbObjectBase")); }

/////////////////////////////////////////////////////////////////////////////

const PVOID   CDbObjectBase::Connection(void)const    { return m_con_ref; }
bool          CDbObjectBase::IsConnected(void)const   { return (NULL != m_con_ref); }

/////////////////////////////////////////////////////////////////////////////

TErrorRef     CDbObjectBase_simplified::Error (void)const       { return m_error;   }

/////////////////////////////////////////////////////////////////////////////

CDbObject::CDbObject(const DWORD _opts) : TBase(global::conn_ref), m_schemata(_opts) {}
CDbObject::~CDbObject(void) {}

/////////////////////////////////////////////////////////////////////////////

const
CDataSchemata&  CDbObject::Schemata(void)const { return m_schemata; }
CDataSchemata&  CDbObject::Schemata(void)      { return m_schemata; }