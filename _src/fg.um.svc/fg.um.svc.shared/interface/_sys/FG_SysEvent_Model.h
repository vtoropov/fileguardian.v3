#ifndef _FGDATMODEL_H_45697F72_081D_4dda_9FBF_0059334E4E3B_INCLUDED
#define _FGDATMODEL_H_45697F72_081D_4dda_9FBF_0059334E4E3B_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jan-2016 at 9:42:39pm, GMT+7, Phuket, Rawai, Monday;
	This is File Guardian background service shared library data model defenition(s) declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 11:53:29a, UTC+7, Phuket, Rawai, Thursday;
*/
#include "Shared_SystemError.h"
#include "Shared_GenericSyncObject.h"
#include "Shared_FS_CommonDefs.h"

#include <queue>
#include "FG_FsFilterIface.Defs.h"
#include "FG_FsBridgeDefs.h"

namespace fg { namespace common { namespace data
{
	using fg::common::filter::CEventItem;

	struct CSystemEvent
	{
		eFsEventType::_e    _id;
		CAtlString          _name;
		bool                _valid;
		CSystemEvent(const bool __valid = true) : _id(eFsEventType::eFsEventUnknown), _valid(__valid) {}
	};

	CAtlString  SystemEventIdToText(const eFsEventType::_e);

	typedef ::std::vector<CSystemEvent> TSystemEventCollection;
	typedef ::std::map<eFsEventType::_e, INT> TOperateTotals;

	class CSystemEventEnum
	{
	private:
		static
		TSystemEventCollection  m_events;
	public:
		CSystemEventEnum(void);
	public:
		INT            Count(void)const;
		const
		TSystemEventCollection& Events(void)const;
		const
		CSystemEvent&  Item (const INT nIndex)const;
	};

	struct CSystemEventPair {
		LPCTSTR   lpsz_source;
		LPCTSTR   lpsz_target;
		INT       n_action;
		INT       n_status;
	public:
		CSystemEventPair(void) : lpsz_source(NULL), lpsz_target(NULL), n_action(0), n_status(0) {}
		CSystemEventPair(LPCTSTR _lpsz_src) : lpsz_source(_lpsz_src), lpsz_target(NULL), n_action(0), n_status(0) {}
		CSystemEventPair(LPCTSTR _lpsz_src, LPCTSTR _lpsz_tgt) : lpsz_source(_lpsz_src), lpsz_target(_lpsz_tgt), n_action(0), n_status(0) {} 
		CSystemEventPair(LPCTSTR _lpsz_src, LPCTSTR _lpsz_tgt, const INT _act, const INT _status) :
			lpsz_source(_lpsz_src), lpsz_target(_lpsz_tgt), n_action(_act), n_status(_status) {}
	};

	using shared::lite::common::CSysError;
	using shared::ntfs::CObjectType;

	class CRecord
	{
	public:
		enum {
			eStatusNone  = -1,
			eStatusUnreported =  0,
			eStatusReported   =  1,
		};
	protected:
		time_t         m_timestamp;  // timestamp that is based on seconds;
		LONGLONG       m_time_ex;    // timestamp that is based on nanoseconfs;
		CAtlString     m_source;
		CAtlString     m_target;
		INT            m_action;     // actually, kernel mode driver event identifier
		INT            m_status;
		bool           m_loc_time;   // indicates local time if true;
		CAtlString     m_process;    // path of a process, which initiated file event;
		CObjectType::_e
		               m_type;
	public:
		CRecord(void);
		CRecord(const CRecord&);
		CRecord(const CEventItem&);
		CRecord(const FILETIME&   _timestamp, const CSystemEventPair&);
		CRecord(const SYSTEMTIME& _timestamp, const CSystemEventPair&);
		CRecord(const time_t      _timestamp, const CSystemEventPair&);
	public:
		INT            Action   (void)const;
		VOID           Action   (const INT);      // input argument must be synchronous with eFsEventType::_e enumeration;
		CAtlString     ActionAsText(void)const;
		bool           IsLocalTime (void)const;
		VOID           IsLocalTime (const bool);
		LPCTSTR        Process  (void)const;
		VOID           Process  (LPCTSTR);
		LPCTSTR        Source   (void)const;
		VOID           Source   (LPCTSTR)  ;
		INT            Status   (void)const;
		VOID           Status   (const INT);
		CAtlString     StatusAsText(void)const;
		LPCTSTR        Target   (void)const;
		VOID           Target   (LPCTSTR)  ;
		time_t         Timestamp(void)const;
		VOID           Timestamp(const FILETIME&);
		VOID           Timestamp(const SYSTEMTIME&);
		VOID           Timestamp(const time_t, const bool bLocal = false);
		CAtlString     TimestampAsText(const bool bFormatted = true)const; // gets timestamp as unix time;
		LONGLONG       TimestampEx(void)const;
		VOID           TimestampEx(const LONGLONG);
		CObjectType::_e
		               Type(void)const;
		VOID           Type(const CObjectType::_e);
		VOID           TypeAsLong(const LONG);
		CAtlString     TypeAsText(void)const;
	public:
		CRecord&       operator= (const CRecord&);
		CRecord&       operator= (const CEventItem&);
	};

	typedef ::std::vector<CRecord>    TRecords;

	using shared::lite::sync::CGenericSyncObject;
	using shared::ntfs::TFolderList;

	class CRecordCache
	{
		typedef ::std::queue<CRecord> TCacheData;
	private:
		CGenericSyncObject m_sync_obj;
		TCacheData         m_data;
	public:
		CRecordCache(void);
	public:
		VOID          Clear(void);
		CRecord       Get(void);
		bool          IsEmpty(void)const;
		HRESULT       Put(const CRecord&);
		INT           Size(void)const;
	};

	class CRecordLimit {
	private:
		LONG          m_offset;
		LONG          m_limit;
	public:
		CRecordLimit(void);
		CRecordLimit(const LONG _off, const LONG _lmt);
		CRecordLimit(const CRecordLimit&);
	public:
		bool          IsValid(void)const;
		LONG          Limit(void)const;
		HRESULT       Limit(const LONG);
		LONG          Offset(void)const;
		HRESULT       Offset(const LONG);
	public:
		CRecordLimit& operator=(const CRecordLimit&);
	};

	class CRecordRange_simplified {
	protected:
		LONG          m_max;
		LONG          m_min;
	public:
		CRecordRange_simplified(void);
		CRecordRange_simplified(const LONG _min, const LONG _max);
	public:
		virtual bool  IsValid(void)const;
	public:
		LONG          Max  (void)const;
		VOID          Max  (const LONG);
		LONG          Min  (void)const;
		VOID          Min  (const LONG);
	};

	class CRecordRange : public CRecordRange_simplified {

		typedef CRecordRange_simplified TBase;

	private:
		LONG          m_value;
	public:
		CRecordRange(void);
		CRecordRange(const LONG _val, const LONG _min, const LONG _max);
	public:
		bool          IsValid(void)const override sealed;
		LONG          Value(void) const;
		HRESULT       Value(const LONG);
		HRESULT       Value(LPCTSTR lpszValueAsText);
		CAtlString    ValueAsFormattedText(void)const;
		CAtlString    ValueAsText(void)const;
	public:
		operator LONG(void)const;
	};
}}}

#endif/*_FGDATMODEL_H_45697F72_081D_4dda_9FBF_0059334E4E3B_INCLUDED*/