#ifndef _FGDATFILTER_H_4B5EB4BD_A4C5_4891_BB10_89CC77443F0D_INCLUDED
#define _FGDATFILTER_H_4B5EB4BD_A4C5_4891_BB10_89CC77443F0D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Jan-2016 at 8:24:29pm, GMT+7, Phuket, Rawai, Tuesday;
	This is File Guardian Background Service System Events Data Filter class declaration file.
*/
#include "Shared_DateTime.h"
#include "FG_SysEvent_Model.h"

namespace fg { namespace common { namespace data
{
	using shared::lite::data::CUnixTime;

	class CTimeSpan
	{
	private:
		CUnixTime      m_from;
		CUnixTime      m_upto;
		CAtlString     m_field;
		bool           m_enabled;
	public:
		CTimeSpan(void);
		CTimeSpan(const CTimeSpan&);
	public:
		VOID           BindTo(const CAtlString& _field);
		VOID           Clear(void);
		bool           Enabled(void)const;
		VOID           Enabled(const bool);
		const
		CUnixTime&     From(void)const;
		CUnixTime&     From(void);
		SYSTEMTIME     FromAsSysTime(void)const;
		CAtlString     FromAsText(void)const;
		bool           IsValid(void)const;
		VOID           Normalize(void);
		CAtlString     ToString(void)const;
		const
		CUnixTime&     Upto(void)const;
		CUnixTime&     Upto(void);
		SYSTEMTIME     UptoAsSysTime(void)const;
		CAtlString     UptoAsText(void)const;
	public:
		CTimeSpan&     operator=(const CTimeSpan&);
	};

	class CFilterExtension // actually is not a filter from SQL point of view (not under WHERE clause)
	{
	public:
		enum {
			eDefaultRowLimit = 100,
		};
	private:
		bool           m_reverse;   // recent first
		DWORD          m_limit;     // rows limit
		CAtlString     m_reverse_bind;
	public:
		CFilterExtension(void);
		CFilterExtension(const CFilterExtension&);
	public:
		CAtlString     ReverseBinging(void)const;
		VOID           ReverseBinding(LPCTSTR pszFieldName);
		bool           Reversed(void)const;
		VOID           Reversed(const bool);
		CAtlString     ReversedAsQuery(void)const;
		DWORD          RowLimit(void)const;
		HRESULT        RowLimit(const INT);
		CAtlString     RowLimitAsQuery(void)const;
	public:
		CFilterExtension& operator=(const CFilterExtension&);
	};

	class CFilter : public CRecord
	{
		typedef CRecord TBase;
	private:
		mutable
		CSysError      m_error;
		CTimeSpan      m_tm_span;
		bool           m_enabled;
		INT            m_watched;            // an index of watched folder, otherwise, -1
	private:
		CFilterExtension m_extension;
	public:
		CFilter(void);
		CFilter(const CFilter&);
	public:
		bool           Enabled(void)const;
		VOID           Enabled(const bool);
		TErrorRef      Error(void)const;
		VOID           Reset(void);          // clears the filter and turns it off
		VOID           ResetToDefault(void); // resets the filter to default values and turns it on
		const
		CTimeSpan&     TimeSpan(void)const;
		CTimeSpan&     TimeSpan(void);
		CAtlString     ToString(void)const;
		bool           Validate(void)const;
		INT            Watched (void)const;
		VOID           Watched (const INT);
	public:
		const
		CFilterExtension& Extension(void)const;
		CFilterExtension& Extension(void);
	public:
		CFilter&       operator=(const CFilter&);
	};

#include "Shared_SystemError.h"

	using shared::lite::common::CSysError;

	class CFilterPersistent : public CFilter
	{
	private:
		CSysError      m_error;
	public:
		CFilterPersistent(void);
	public:
		TErrorRef      Error(void)const;
		HRESULT        Load (void);
		HRESULT        Save (void);
	};
}}}

#endif/*_FGDATFILTER_H_4B5EB4BD_A4C5_4891_BB10_89CC77443F0D_INCLUDED*/