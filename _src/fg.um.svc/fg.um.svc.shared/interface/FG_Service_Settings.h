#ifndef _FGSVCSETTINGS_H_D3D62E43_1A4F_47a2_B540_EA91C8ABBC8F_INCLUDED
#define _FGSVCSETTINGS_H_D3D62E43_1A4F_47a2_B540_EA91C8ABBC8F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Apr-2016 at 2:47:39pm, GMT+7, Phuket, Rawai, Monday;
	This is File Guardian background service configuration class(es) declaration file.
*/
#include "Shared_SystemError.h"
#include "FG_SysEvent_Model.h"

namespace fg { namespace common { namespace data
{
	using shared::lite::common::CSysError;

	class CWatchedType
	{
	public:
		enum _e{
			eIncluded  = 0x0,   // the folder is watched for file system events
			eExcluded  = 0x1,   // the folder is not watched, i.e. is excluded
		};
	};

	class CWatchedFolders
	{
	protected:
		mutable
		CSysError         m_error;
		TFolderList       m_folders;
		CWatchedType::_e  m_type;
	public:
		CWatchedFolders(const CWatchedType::_e);
	public:
		HRESULT           Append(const CAtlString& _folder);
		INT               Count (void) const;
		TErrorRef         Error (void) const;
		bool              IsEmpty (void) const;
		bool              IsExcluded(void)const;
		bool              IsRelative(const CAtlString& _folder)const;
		CAtlString        ItemOf(const INT nIndex) const;
		const
		TFolderList&      List  (void) const;
		HRESULT           Remove(const CAtlString& _folder);
		CWatchedType::_e  Type  (void) const;
	public:
		operator const TFolderList&(void)const;
		CWatchedFolders& operator= (const TFolderList&);
	};

	class CWatchedPersistent : public CWatchedFolders
	{
		typedef CWatchedFolders TBase;
	public:
		CWatchedPersistent(const CWatchedFolders&);
		CWatchedPersistent(const CWatchedType::_e);
	public:
		HRESULT           Load(void);
		HRESULT           Save(void);
	public:
		CWatchedPersistent& operator= (const CWatchedFolders&);
	};

	class CServiceSettings
	{
	protected:
		mutable
		CSysError         m_error;
		CWatchedFolders   m_included;
		CWatchedFolders   m_excluded;
		bool              m_bRemoval;        // if true, auto-track removal devices
	public:
		CServiceSettings(void);
	public:
		TErrorRef         Error(void)const;
		const
		CWatchedFolders&  ExcludedFolders(void)const;
		CWatchedFolders&  ExcludedFolders(void);
		const
		CWatchedFolders&  IncludedFolders(void)const;
		CWatchedFolders&  IncludedFolders(void);
		bool              TrackRemoval(void)const;
		VOID              TrackRemoval(const bool);
	};

	class CServiceSettingPersBase : public CServiceSettings
	{
		typedef CServiceSettings TBase;
	public:
		CServiceSettingPersBase(void);
	public:
		HRESULT           Load(void);
		HRESULT           Save(void);
	};

	class CServiceSettingPersistent : public CServiceSettingPersBase
	{
		typedef CServiceSettingPersBase TBase;
	private:
		bool              m_bInitialized;
	public:
		CServiceSettingPersistent(void);
	public:
		HRESULT           AppendFolder(const CAtlString& _folder, const CWatchedType::_e);
		bool              IsInitialized(void)const;
		HRESULT           RemoveFolder(const CAtlString& _folder, const CWatchedType::_e);
	public:
		HRESULT           Load(void);
		HRESULT           Save(const bool bIncludeBaseOpts = false);
	};

	CServiceSettingPersistent&  GetServiceSettingsRef(void); // returns singleton object reference

	class CServiceFolderComparator
	{
	private:
		mutable
		CSysError         m_error;
		const
		CWatchedFolders&  m_included;
		const
		CWatchedFolders&  m_excluded;
	public:
		CServiceFolderComparator(const CWatchedFolders& _inc, const CWatchedFolders& _exc);
	public:
		HRESULT           CheckIncluded(LPCTSTR lpszPath) const;
		HRESULT           CheckExcluded(LPCTSTR lpszPath) const;
		TErrorRef         Error(void)const;
	private:
		HRESULT           CheckInput(LPCTSTR lpszPath) const;
		HRESULT           CheckCoincident(LPCTSTR lpszPath) const;
	};
}}}

#endif/*_FGSVCSETTINGS_H_D3D62E43_1A4F_47a2_B540_EA91C8ABBC8F_INCLUDED*/