#ifndef _FGSERVICELICMONITOR_H_0A1B50C4_D21F_46fb_B246_5434C028463B_INCLUDED
#define _FGSERVICELICMONITOR_H_0A1B50C4_D21F_46fb_B246_5434C028463B_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Apr-2016 at 11:35:45pm, GMT+7, Phuket, Rawai, Monday;
	This is File Guardian Background Service License Monitor class declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_SystemThreadPool.h"

#include "FG_License_Info.h"

namespace fg { namespace service
{
	using shared::lite::common::CSysError;
	using shared::lite::common::CSysErrorSafe;
	using shared::lite::sys_core::CThreadBase;

	using fg::common::lic::CLicenseState;

	interface ILicenseCallback
	{
		virtual HRESULT ILicenseCallback_OnChanged(const CLicenseState&) PURE;
	};

	class CLicenseMonitor : public CThreadBase
	{
		typedef CThreadBase TBase;
	private:
		ILicenseCallback& m_sink;
		CLicenseState     m_last_state;
	public:
		CLicenseMonitor(ILicenseCallback&);
	public:
		HRESULT           Start(void) override sealed;
	public:
		CLicenseState     LastState(void)const;
		HRESULT           Initialize(void);
	private:
		VOID              LastState(const CLicenseState&);
		VOID              ThreadFunction(void) override sealed;
	};
}}

#endif/*_FGSERVICELICMONITOR_H_0A1B50C4_D21F_46fb_B246_5434C028463B_INCLUDED*/