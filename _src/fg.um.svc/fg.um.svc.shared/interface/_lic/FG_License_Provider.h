#ifndef _FGLICPROVIDER_H_A9147EE9_F6BF_4790_B8DE_3B03F3979641_INCLUDED
#define _FGLICPROVIDER_H_A9147EE9_F6BF_4790_B8DE_3B03F3979641_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Apr-2016 at 8:59:39pm, GMT+7, Phuket, Rawai;
	This is File Guardian License Data Provider class declaration file.
*/
#include "Shared_SystemError.h"
#include "FG_License_Info.h"

namespace fg { namespace common { namespace lic
{
	using shared::lite::common::CSysError;

	class CLicenseStorage {

	private:
		CSysError      m_error;
	public:
		CLicenseStorage(void);
	public:
		TErrorRef      Error(void)const;
		HRESULT        Load (CLicenseInfo&);
	};

	class CLicenseProvider
	{
	private:
		mutable
		CSysError      m_error;
		CLicenseStorage
		               m_storage;
	public:
		CLicenseProvider(void);
		~CLicenseProvider(void);
	public:
		HRESULT        CheckLicense(CLicenseInfo& _lic);       // checks current state of the license and gains expiration date;
		HRESULT        CheckLicense(CLicenseState& _state);    // checks current state of the license; this method is called from thread function of the background service
		TErrorRef      Error(void)const;
		bool           IsNewVersionAvailable(CLicenseInfo&)const; // checks license version (received from web service) against this executable one
		HRESULT        RegisterLicense(CLicenseInfo& _lic);    // this method is called from installer to register license by web license service, and allows to return installation identifier to caller;
		HRESULT        RemoveLicense(void);                    // this method is called from uninstaller to remove license from web license database
	public:
		const
		CLicenseStorage& Storage(void)const;
		CLicenseStorage& Storage(void);
	};
}}}

#endif/*_FGLICPROVIDER_H_A9147EE9_F6BF_4790_B8DE_3B03F3979641_INCLUDED*/