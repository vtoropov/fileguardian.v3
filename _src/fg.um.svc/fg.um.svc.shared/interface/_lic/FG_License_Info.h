#ifndef _FGLICINFO_H_07A6812E_350E_4d1c_BAA4_F2CECDE586F1_INCLUDED
#define _FGLICINFO_H_07A6812E_350E_4d1c_BAA4_F2CECDE586F1_INCLUDED
/*
	Created by Tech_dog (VToropov) on 23-Apr-2016 at 11:34:47am, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian common library license information related class(es) declaration file.
*/
#include "Shared_SystemError.h"

namespace fg { namespace common { namespace lic
{
	using shared::lite::common::CSysError;

	class CLicenseState
	{
	public:
		enum _e{
			eUnknown  = 0,  // a license state is unknown, i.e. no licensing has been before;
			eLicensed = 1,  // a license is valid and in active state
			eExpired  = 2,  // a license is expired
			eRemoved  = 3,  // a software has been uninstalled; this state is used by setup program;
			ePending  = 4,  // license info request in the pending state; waiting web service response;
			eError    = 5,  // getting license info is failed; this state is mainly used by license monitor;
		};
	private:
		_e             m_id;
	public:
		CLicenseState(void);
		CLicenseState(const CLicenseState::_e);
	public:
		_e             Identifier(void)const;
		CAtlString     ToString(void)const;
	public:
		CLicenseState& operator= (const CLicenseState::_e);
	public:
		static
		CLicenseState::_e StringToState(LPCTSTR lpszState);
	};

	class CLicVersion
	{
	public:
		enum _e{
			eNumberOfGroups = 4
		};
	private:
		typedef ::std::vector<UINT> TVerParts;
	private:
		CSysError      m_error;
		TVerParts      m_value;  // in format [0-9].[0-9].[0-9].[0-9]
	public:
		CLicVersion(LPCTSTR lpszVersion = NULL);
	public:
		TErrorRef      Error(void)const;
		HRESULT        InitializeFromGlobalDef(void);// initializes version info from global definition of desktop app current version
		HRESULT        InitializeFromResource(void); // initializes version info from executable resource section;
		LONGLONG       ToLong(void)const;
		CAtlString     Value(void)const;
		HRESULT        Value(LPCTSTR);
	public:
		bool operator> (const CLicVersion&)const;
		bool operator< (const CLicVersion&)const;
		bool operator==(const CLicVersion&)const;
	};

	class CLicenseInfo
	{
	public:
		enum _e{
			eSerialNumberLen = 16,
			eUserIdLen       = 20,
		};
	private:
		CLicenseState  m_state;
		time_t         m_expire;
		CAtlString     m_serial;
		CAtlString     m_userId;
		CAtlString     m_desc;     // optional description of the workstation or any other info;
		CLicVersion    m_version;
	public:
		CLicenseInfo(void);
	public:
		CAtlString     DaysLeftAsText(void)const;
		CAtlString     Description(void)const;
		VOID           Description(LPCTSTR);
		time_t         ExpireDate(void)const;
		VOID           ExpireDate(const time_t);    // sets expiration date from unix timestamp
		VOID           ExpireDate(LPCTSTR);         // sets expiration date from string
		CAtlString     ExpireDateAsText(void)const; // return formatted expiration date
		bool           IsActive(void)const;         // the license is active and allows to use full functionality of FG
		CAtlString     Serial(void)const;           // gets a serial number
		HRESULT        Serial(LPCTSTR);             // sets a serial number from string
		CAtlString     SerialFormatted(void)const;  // gets a serial number formatted for better readability
		const
		CLicenseState& State(void)const;
		CLicenseState& State(void);
#if defined(_DEBUG)
		CAtlString     ToString(LPCTSTR lpszSeparator = NULL)const;
#endif
		CAtlString     UserId(void)const;
		HRESULT        UserId(LPCTSTR);
		const
		CLicVersion&   Version(void)const;
		CLicVersion&   Version(void);
	};

	class CLicenseInfoPersistent : public CLicenseInfo
	{
		typedef CLicenseInfo  TBase;
	private:
		CSysError      m_error;
	public:
		CLicenseInfoPersistent(void);
		CLicenseInfoPersistent(const CLicenseInfo&);
	public:
		HRESULT        Clear(void);      // removes data from registry
		TErrorRef      Error(void)const;
		HRESULT        Load(void);
		HRESULT        Save(void);
	public:
		CLicenseInfoPersistent& operator= (const CLicenseInfo&);
	};

	class CLicenseUser
	{
	private:
		CSysError      m_error;
	public:
		CLicenseUser(void);
	public:
		TErrorRef      Error(void)const;
		HRESULT        GenerateId(CAtlString& _uid);
	};
}}}

#endif/*_FGLICINFO_H_07A6812E_350E_4d1c_BAA4_F2CECDE586F1_INCLUDED*/