#ifndef _FGSERVICEBRIDGE_H_E8F4E062_357D_4205_9A2D_A339E92460C0_INCLUDED
#define _FGSERVICEBRIDGE_H_E8F4E062_357D_4205_9A2D_A339E92460C0_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Dec-2017 at 0:09:59am, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian background service filter connector class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 4-Jun-2018 at 10:17:46p, UTC+7, Phuket, Rawai, Monday;
*/
//#include "FG_Service_CommonDefs.h"
#include "FG_FsBridgeIface.h"

#include "Shared_FS_GenericDrive.Monitor.h"

namespace fg { namespace service
{
	using fg::common::filter::IDriverEventAdoptedSink;
	using fg::common::filter::CDriverBridge;
	using fg::common::filter::CDriverInstaller;

	using shared::ntfs::CRemovableDriveMonitor;
	using shared::ntfs::IRemovableDriveEventSink;
	using shared::ntfs::TDriveList;

	class CDrvConnector :
		public  IRemovableDriveEventSink
	{
	private:
		IDriverEventAdoptedSink&
		                       m_evt_sink;
		CDriverInstaller       m_drv_installer;
		CDriverBridge          m_drv_adapter;
		CRemovableDriveMonitor m_rem_dev;
	public:
		CDrvConnector(IDriverEventAdoptedSink&);
		~CDrvConnector(void);

	private: // IRemovableDriveEventSink
		virtual HRESULT IRemovableDrive_BeingAdded(const TDriveList&) override sealed;
		virtual HRESULT IRemovableDrive_BeingEjected(const TDriveList&) override sealed;

	public:
		HRESULT Connect(void);      // if filter driver is not installed, it will be made automatically;
		HRESULT Disconnect(void);   // uninstalls the filter;
	};
}}

#endif/*_FGSERVICEBRIDGE_H_E8F4E062_357D_4205_9A2D_A339E92460C0_INCLUDED*/