#ifndef _FGSVCMANAGER_H_D465BA32_9E31_44b4_BCE0_96ADD39FB95F_INCLUDED
#define _FGSVCMANAGER_H_D465BA32_9E31_44b4_BCE0_96ADD39FB95F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Feb-2016 at 10:12:57pm, GMT+7, Phuket, Rawai, Tuesday;
	This is File Guardian Background Service Manager class declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_SystemService.h"
#include "Shared_SystemServiceMan.h"

#include "FG_Service_CrtData.h"

namespace fg { namespace common
{
	using shared::lite::common::CSysError;

	using shared::service::CService;
	using shared::service::CServiceState;
	using shared::service::CServiceCrtData;
	using shared::service::CServiceCrtOption;
	using shared::service::CServiceManager;

	class CFwServiceManager
	{
	private:
		CSysError         m_error;
		CServiceManager   m_mgr;
		CServiceCrtData   m_crt;
		CService          m_svc;
	public:
		CFwServiceManager(void);
		CFwServiceManager(const CAtlString& _exe_path);   // this constructor for installation procedure;
	public:
		TErrorRef         Error(void) const;
		bool              InstallService  (void);
		bool              IsInstalled     (void);
		const
		CService&         GetServiceObject(void);
		bool              StartService    (void);
		bool              StopService     (void);
		bool              UninstallService(void);
	};
}}

#endif/*_FGSVCMANAGER_H_D465BA32_9E31_44b4_BCE0_96ADD39FB95F_INCLUDED*/