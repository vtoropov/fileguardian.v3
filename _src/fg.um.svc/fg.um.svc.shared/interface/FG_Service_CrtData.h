#ifndef _FGSVCCRTDATA_H_500390B1_5C1D_4882_9801_47F4CA2F89DF_INCLUDED
#define _FGSVCCRTDATA_H_500390B1_5C1D_4882_9801_47F4CA2F89DF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Jan-2016 at 5:59:07pm, GMT+7, Phuket, Rawai, Monday;
	This is File Guardian background service create data class(es) declaration file.
*/
#include "Shared_SystemService.h"

namespace fg { namespace common { namespace data
{
	using shared::service::CServiceCrtData;
	using shared::service::CServiceCrtOption;

	HRESULT  InitializeServiceCrtData(CServiceCrtData&);
}}}

#endif/*_FGSVCCRTDATA_H_500390B1_5C1D_4882_9801_47F4CA2F89DF_INCLUDED*/