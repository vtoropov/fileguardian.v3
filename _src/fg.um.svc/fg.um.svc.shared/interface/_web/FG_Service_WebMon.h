#ifndef _FGWEBPMONITOR_H_4362D00F_96E8_4379_9AFC_DD0768EF7DED_INCLUDED
#define _FGWEBPMONITOR_H_4362D00F_96E8_4379_9AFC_DD0768EF7DED_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jan-2016 at 3:21:00pm, GMT+7, Phuket, Rawai, Monday;
	This is File Guardian background service common library web monitor class declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_GenericSyncObject.h"
#include "Shared_SystemThreadPool.h"

#include "FG_SqlData_Provider.h"

namespace fg { namespace common { namespace data
{
	using shared::lite::common::CSysError;
	using shared::lite::common::CSysErrorSafe;
	using shared::lite::sync::CGenericSyncObject;

	using shared::lite::sys_core::CThreadCrtData;
	using shared::lite::sys_core::CThreadState;

	using fg::common::data::local::CDbProvider;

	class CWebMonOpt {
	public:
		enum _e {
			eLogUpload     = 0x10, // this value must not coincident with 'CThreadState' values;
		};
	};

	class CWebMonitor
	{
	private:
		CThreadCrtData      m_crt;
		CGenericSyncObject  m_sync_obj;
		CDbProvider&        m_provider;
	private:
		volatile
		DWORD               m_state;
		CSysErrorSafe       m_error;
	public:
		CWebMonitor(CDbProvider&);
		~CWebMonitor(void);
	public:
		CSysError           Error(void)const;
		bool                IsRunning(void)const;
		HRESULT             Start(void);
		DWORD               State(void)const;
		HRESULT             State(const CWebMonOpt::_e, const bool _set);
		HRESULT             Stop (void);
	private:
		VOID                MonitorWorkerThread(void);
	};
}}}

#endif/*_FGWEBPMONITOR_H_4362D00F_96E8_4379_9AFC_DD0768EF7DED_INCLUDED*/