#ifndef _FGWEBMONITORCFG_H_089E7720_7325_41ac_B6DF_6CC7D5FD6530_INCLUDED
#define _FGWEBMONITORCFG_H_089E7720_7325_41ac_B6DF_6CC7D5FD6530_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-May-2016 at 9:52:55pm, GMT+7, Phuket, Rawai, Tuesday;
	This is File Guardian background service web reporting service monitor configuration class declaration file.
*/
#include "Shared_SystemError.h"

namespace fg { namespace common { namespace data
{
	using shared::lite::common::CSysError;

	class CWebMonitorDefaults
	{
	public:
		static bool   Enabled(void);
	};

	class CWebMonitorSettings
	{
	protected:
		mutable
		CSysError     m_error;
	private:
		bool          m_enabled;
	public:
		CWebMonitorSettings(const bool bInitToDefaults = false);
	public:
		VOID          Default(void);             // resets to default settings
		bool          Enabled(void)const;
		VOID          Enabled(const bool);
		TErrorRef     Error(void)const;
	};

	class CWebMonitorSettingPersistent : public CWebMonitorSettings
	{
		typedef CWebMonitorSettings TBase;
	public:
		CWebMonitorSettingPersistent(const bool bInitToDefaults);
	public:
		HRESULT       Load(void);
		HRESULT       Save(void);
	};
}}}

#endif/*_FGWEBMONITORCFG_H_089E7720_7325_41ac_B6DF_6CC7D5FD6530_INCLUDED*/