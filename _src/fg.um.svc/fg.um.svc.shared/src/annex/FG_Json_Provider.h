#ifndef _FGJSONPROVIDER_H_420142DE_F6BB_4523_AE17_297AC8179173_INCLUDED
#define _FGJSONPROVIDER_H_420142DE_F6BB_4523_AE17_297AC8179173_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Apr-2016 at 12:01:41pm, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian common library JSON data provider class declaration file.
*/
#include "Shared_SystemError.h"
#include "FG_License_Provider.h"
#include "FG_SysEvent_Model.h"

namespace fg { namespace common { namespace data
{
	using shared::lite::common::CSysError;

	using fg::common::lic::CLicenseInfo;

	class CJsonDataProvider
	{
	private:
		CSysError     m_error;
	public:
		CJsonDataProvider(void);
	public:
		CAtlString    CreateBatchUpdate(const CAtlString& _uid, const TRecords _recs);
		CAtlString    CreateLicRequest (const CAtlString& _sn , const CAtlString& _uid, const CAtlString& _desc = _T(""), const CAtlString& _ver = _T(""));
		CAtlString    CreateLicRequest (const CLicenseInfo&);
		HRESULT       ParseLicResponse (const CAtlString& _response, CLicenseInfo&);
		TErrorRef     Error(void)const;
	};

}}}

#endif/*_FGJSONPROVIDER_H_420142DE_F6BB_4523_AE17_297AC8179173_INCLUDED*/