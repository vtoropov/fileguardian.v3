/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Apr-2016 at 12:13:36pm, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian common library JSON data provider class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 11:09:21p, UTC+7, Phuket, Rawai, Thursday;
*/
#include "StdAfx.h"
#include "FG_Json_Provider.h"

using namespace fg::common::data;
using namespace fg::common::lic;

#include "document.h"
#include "error/en.h"
using namespace rapidjson;

typedef GenericDocument<UTF16<>> DocumentW;
typedef GenericValue<UTF16<>, MemoryPoolAllocator<>> TGenericValueW;
typedef GenericMemberIterator<true, UTF16<>, MemoryPoolAllocator<>> TConstMemberIterW;
/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data { namespace details
{
	bool JsonDataProvider_FindError(DocumentW& _doc)
	{
		bool bFound = false;
		for (TConstMemberIterW it_ = _doc.MemberBegin(); it_ != _doc.MemberEnd(); ++it_) {
			CAtlString js_name = it_->name.GetString();
			bFound = (0 == js_name.CompareNoCase(_T("error")));
			if (bFound)
				break;
		}
		return bFound;
	}

	bool JsonDataProvider_ParseError(DocumentW& _doc, CSysError& _err)
	{
		if (!JsonDataProvider_FindError(_doc))
			return false;

		const TGenericValueW& js_error = _doc[_T("error")];
		if (!js_error.IsObject())
			return false;

		CAtlString cs_code = _T("#n/a");
		const TGenericValueW& js_code = js_error[_T("code")];
		if (js_code.IsString())
			cs_code = js_code.GetString();

		CAtlString cs_details = _T("#n/a");
		const TGenericValueW& js_details = js_error[_T("details")];
		if (js_details.IsString())
			cs_details = js_details.GetString();

		_err.SetState(
				(DWORD)ERROR_EXCEPTION_IN_SERVICE,
				_T("License: code=%s; details=%s"),
				(LPCTSTR)cs_code,
				(LPCTSTR)cs_details
			);

		return true;
	}

	VOID JsonDataProvider_ValidateString(::ATL::CAtlString& cs_val)
	{
		if (-1 != cs_val.Find(_T("\'"))) cs_val.Replace(_T("\'"), _T("\\\'"));
		if (-1 != cs_val.Find(_T("\""))) cs_val.Replace(_T("\""), _T("\\\""));
		if (-1 != cs_val.Find(_T("\\"))) cs_val.Replace(_T("\\"), _T("\\\\"));
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CJsonDataProvider::CJsonDataProvider(void)
{
	m_error.Source(_T("CJsonDataProvider"));
}

/////////////////////////////////////////////////////////////////////////////

CAtlString    CJsonDataProvider::CreateBatchUpdate(const CAtlString& _uid, const TRecords _recs)
{
	CAtlString cs_json;
	const size_t count_ = _recs.size();

	cs_json.Format(
			_T("{ \"installation_id\" : \"%s\", \"events\" : ["),
			_uid.GetString()
		);

	for (size_t i_ = 0; i_ < count_; i_++)
	{
		const CRecord& rec_ = _recs[i_];
		const bool last_ = (count_ - 1 == i_);

		CAtlString cs_path = rec_.Target();
		details::JsonDataProvider_ValidateString(cs_path);

		CAtlString cs_entry;
		cs_entry.Format(
				_T("{\"timestamp\" : %s, \"file_path\" : \"%s\", \"action\" : \"%s\"}"),
				(LPCTSTR)rec_.TimestampAsText(false), // do not format timestamp, use it as long integer!!!
				(LPCTSTR)cs_path,
				(LPCTSTR)rec_.ActionAsText()
			);

		cs_json += cs_entry;
		if (!last_)
			cs_json += _T(",");
		cs_json += _T("\r\n");
	}
	cs_json += _T("]}");

	return cs_json;
}

CAtlString    CJsonDataProvider::CreateLicRequest(const CAtlString& _sn, const CAtlString& _uid, const CAtlString& _desc, const CAtlString& _ver)
{
	CAtlString cs_json;
	cs_json.Format(
			_T("{\"serial\":\"%s\", \"installation_id\":\"%s\", \"description\":\"%s\", \"version\":\"%s\"}"),
			(LPCTSTR)_sn   ,
			(LPCTSTR)_uid  ,
			(LPCTSTR)_desc ,
			(LPCTSTR)_ver
		);
	return cs_json;
}

CAtlString    CJsonDataProvider::CreateLicRequest(const CLicenseInfo& _lic)
{
	return this->CreateLicRequest(
			_lic.Serial(),
			_lic.UserId(),
			_lic.Description(),
			_lic.Version().Value()
		);
}

HRESULT       CJsonDataProvider::ParseLicResponse(const CAtlString& _response, CLicenseInfo& _lic)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	DocumentW js_doc;

	rapidjson::ParseResult result_ = js_doc.Parse(_response);
	if (result_.IsError())
	{
		m_error.SetState(
			(DWORD)ERROR_INVALID_DATA,
			_T("JSON parse error: %s (%u)"), CAtlString(rapidjson::GetParseError_En(result_.Code())).GetString(), result_.Offset()
		);
		return m_error;
	}

	if (details::JsonDataProvider_ParseError(js_doc, m_error))
		return m_error;

	CAtlString cs_state = "#n/a";
	const TGenericValueW& js_state = js_doc[_T("lic_status")];
	if (js_state.IsString())
		cs_state = js_state.GetString();

	CLicenseState::_e state_ = CLicenseState::StringToState(cs_state);
	CLicenseState  lic_stat_(state_);

	LONGLONG ll_expire = 0;
	const TGenericValueW& js_expire = js_doc[_T("exp_date")];
	if (js_expire.IsInt64())
		ll_expire = js_expire.GetInt64();

	_lic.State() = lic_stat_;
	_lic.ExpireDate(static_cast<time_t>(ll_expire));

	HRESULT hr_ = _lic.Version().InitializeFromGlobalDef(); // sets the current version by default
	if (FAILED(hr_))
		return (m_error = _lic.Version().Error());

	//
	// TODO: version node can be missed; it is necessary to check its presence;
	//
	if (js_doc.HasMember(_T("version"))) {
		const TGenericValueW& js_version = js_doc[_T("version")];
		if (js_version.IsString()){
			CAtlString cs_version = js_version.GetString();
			if (!cs_version.IsEmpty())
				hr_ = _lic.Version().Value(cs_version);
		}
	}
	if (FAILED(hr_))
		return (m_error = _lic.Version().Error());

	return m_error;
}

TErrorRef     CJsonDataProvider::Error(void)const
{
	return m_error;
}