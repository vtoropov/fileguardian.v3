#ifndef _FGCOMMONSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED
#define _FGCOMMONSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 2:51:35am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian Background Service Shared Library precompiled headers declaration file.
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <atlrx.h>              // atl server lite regular expressions
#include <comdef.h>
#include <vector>
#include <map>
#include <queue>

#include <time.h>

#include "Shared_LogJournal.h"

using shared::log::CEventJournal;

#endif/*_FGCOMMONSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED*/