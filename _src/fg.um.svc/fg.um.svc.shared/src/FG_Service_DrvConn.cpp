/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Dec-2017 at 0:16:54am, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian background service filter connector class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 4-Jun-2018 at 10:16:54p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "FG_Service_DrvConn.h"

using namespace fg::service;
using namespace fg::common::filter;

#include "FG_Service_Settings.h"

using namespace fg::common::data;

/////////////////////////////////////////////////////////////////////////////

CDrvConnector::CDrvConnector(IDriverEventAdoptedSink& _evt_snk) : m_evt_sink(_evt_snk), m_drv_adapter(_evt_snk), m_rem_dev(*this) { }
CDrvConnector::~CDrvConnector(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT CDrvConnector::Connect(void) {

	HRESULT hr_ = S_OK;

	const bool bIgonreIfInstalled = true;

	hr_ = m_drv_installer.Install(bIgonreIfInstalled);

	if (FAILED(hr_)){
		m_evt_sink.IDriverEventAdopted_OnError(m_drv_installer.Error());
		return hr_;
	}

	const bool bIgnoreIfConnected = true;

	hr_ = m_drv_adapter.Connect(bIgnoreIfConnected);
	if (FAILED(hr_)){
		m_evt_sink.IDriverEventAdopted_OnError(m_drv_adapter.Error());
	}
	else {
		CServiceSettingPersBase pers_;
		pers_.Load();
		if (pers_.TrackRemoval())
			m_rem_dev.Start();
	}

	return hr_;
}

HRESULT CDrvConnector::Disconnect(void)
{
	HRESULT hr_ = m_drv_adapter.Disconnect();

	if (FAILED(hr_)){
		m_evt_sink.IDriverEventAdopted_OnError(m_drv_adapter.Error());
	}

	const bool bIgonreIfUninstalled = true;

	hr_ = m_drv_installer.Uninstall(bIgonreIfUninstalled);
	if (m_rem_dev.IsRunning())
		m_rem_dev.Stop();

	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT CDrvConnector::IRemovableDrive_BeingAdded(const TDriveList& _list)
{
	HRESULT hr_ = S_OK;

	for (size_t i_ = 0; i_ < _list.size(); ++i_){
	
		m_drv_adapter.Removable().Append(_list[i_].GetString());
	}
	return  hr_;
}

HRESULT CDrvConnector::IRemovableDrive_BeingEjected(const TDriveList& _list)
{
	HRESULT hr_ = S_OK;

	for (size_t i_ = 0; i_ < _list.size(); ++i_){
	
		m_drv_adapter.Removable().Delete(_list[i_].GetString());
	}
	return  hr_;
}