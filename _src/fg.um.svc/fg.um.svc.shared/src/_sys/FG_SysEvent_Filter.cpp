/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Jan-2016 at 8:34:53pm, GMT+7, Phuket, Rawai, Tuesday;
	This is File Guardian System Events Data Filter class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 12:37:18p, UTC+7, Phuket, Rawai, Thursday;
*/
#include "StdAfx.h"
#include "FG_SysEvent_Filter.h"
#include "FG_Service_Settings.h"

using namespace fg::common::data;

#include "Shared_Registry.h"
#include "Shared_DateTime.h"

using namespace shared::lite::data;
using namespace shared::registry;

/////////////////////////////////////////////////////////////////////////////

CTimeSpan::CTimeSpan(void): m_from(true), m_upto(true), m_enabled(false)
{
	m_from.Current();
	m_upto.Current();
}

CTimeSpan::CTimeSpan(const CTimeSpan& _span): m_from(true), m_upto(true), m_enabled(false)
{
	*this = _span;
}

/////////////////////////////////////////////////////////////////////////////

VOID         CTimeSpan::BindTo(const CAtlString& _field) { m_field = _field;     }
VOID         CTimeSpan::Clear(void)                      { m_from  = m_upto = 0; }
bool         CTimeSpan::Enabled(void)const               { return m_enabled;     }
VOID         CTimeSpan::Enabled(const bool _enabled)     { m_enabled = _enabled; }
const
CUnixTime&   CTimeSpan::From(void)const                  { return m_from;        }
CUnixTime&   CTimeSpan::From(void)                       { return m_from;        }
SYSTEMTIME   CTimeSpan::FromAsSysTime(void)const
{
	CSystemTime sys_(m_from.IsLocal()); sys_ = m_from;
	return sys_;
}

CAtlString   CTimeSpan::FromAsText(void)const
{
	CAtlString cs_from;
	m_from.ToString(false, cs_from);
	return cs_from;
}

bool         CTimeSpan::IsValid(void)const
{
	return (m_from.IsValid() && m_upto.IsValid());
}

VOID         CTimeSpan::Normalize(void)
{
	if (!this->IsValid())
		return;
	if (m_from > m_upto)
	{
		const time_t tm_ = m_from;
		m_from = m_upto;
		m_upto = tm_;
	}
}

CAtlString   CTimeSpan::ToString(void)const
{
	CAtlString cs_text;
	cs_text.Format(
			_T("(%s between %s and %s)"),
			(LPCTSTR)m_field,
			(LPCTSTR)this->FromAsText(),
			(LPCTSTR)this->UptoAsText()
		);
	return cs_text;
}

const
CUnixTime&   CTimeSpan::Upto(void)const                  { return m_upto;        }
CUnixTime&   CTimeSpan::Upto(void)                       { return m_upto;        }
SYSTEMTIME   CTimeSpan::UptoAsSysTime(void)const
{
	CSystemTime sys_(m_upto.IsLocal()); sys_ = m_upto;
	return sys_;
}

CAtlString   CTimeSpan::UptoAsText(void)const
{
	CAtlString cs_upto;
	m_upto.ToString(false, cs_upto);
	return cs_upto;
}

/////////////////////////////////////////////////////////////////////////////

CTimeSpan&   CTimeSpan::operator=(const CTimeSpan& _span){
	this->m_from    = _span.m_from;
	this->m_upto    = _span.m_upto;
	this->m_field   = _span.m_field;
	this->m_enabled = _span.m_enabled;

	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CFilterExtension::CFilterExtension(void) : m_limit(0), m_reverse(false), m_reverse_bind(_T("time"))
{
}

CFilterExtension::CFilterExtension(const CFilterExtension& _ext) : m_limit(0), m_reverse(false), m_reverse_bind(_T("time"))
{
	*this = _ext;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString   CFilterExtension::ReverseBinging(void)const            { return m_reverse_bind;         }
VOID         CFilterExtension::ReverseBinding(LPCTSTR pszFieldName) { m_reverse_bind = pszFieldName; }
bool         CFilterExtension::Reversed(void)const                  { return m_reverse;              }
VOID         CFilterExtension::Reversed(const bool _reverse)        { m_reverse = _reverse;          }
CAtlString   CFilterExtension::ReversedAsQuery(void)const
{
	CAtlString cs_reversed;

	if (m_reverse)
		cs_reversed.Format(
				_T("%s desc"), (LPCTSTR)m_reverse_bind
			);
	else
		cs_reversed.Format(
				_T("%s asc") , (LPCTSTR)m_reverse_bind
			);
	return cs_reversed;
}

DWORD        CFilterExtension::RowLimit(void)const                  { return m_limit;                }
HRESULT      CFilterExtension::RowLimit(const INT _limit) {

	if (_limit < 0)
	{
		m_limit = 0; // sets to unlimit value
		return E_INVALIDARG;
	}
	else
	{
		m_limit = _limit;
		return S_OK;
	}
}

CAtlString   CFilterExtension::RowLimitAsQuery(void)const
{
	CAtlString cs_limit;
	if (m_limit > 0)
		cs_limit.Format(
				_T(" limit %d"),
				m_limit
			);
	return cs_limit;
}

/////////////////////////////////////////////////////////////////////////////

CFilterExtension& CFilterExtension::operator=(const CFilterExtension& _ext) {
	this->m_reverse       = _ext.m_reverse;
	this->m_limit         = _ext.m_limit;
	this->m_reverse_bind  = _ext.m_reverse_bind;

	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CFilter::CFilter(void) : m_enabled(false), m_watched(CRecord::eStatusNone)
{
	m_error.Source(_T("CFilter"));
	m_tm_span.BindTo(_T("time"));
	TBase::m_status = CRecord::eStatusNone; // record class constructor creates another value;
}

CFilter::CFilter(const CFilter& _flt) : m_enabled(false), m_watched(CRecord::eStatusNone)
{
	*this = _flt;
}

/////////////////////////////////////////////////////////////////////////////

bool         CFilter::Enabled(void)const                            { return m_enabled;              }
VOID         CFilter::Enabled(const bool _enabled)                  { m_enabled = _enabled;          }
TErrorRef    CFilter::Error(void)const                              { return m_error;                }
VOID         CFilter::Reset(void)
{
	//
	// TODO: is a reset of timespan required?
	//
	m_extension.Reversed(false);
	m_extension.RowLimit(0);

	m_tm_span.Clear();

	if (!m_source.IsEmpty())m_source.Empty();
	if (!m_target.IsEmpty())m_target.Empty();

	m_action    = 0;
	m_status    = CRecord::eStatusNone;
	TBase::m_timestamp = 0;
	m_enabled   = false;
	m_watched   = CRecord::eStatusNone;;
}

VOID         CFilter::ResetToDefault(void)
{
	// TODO: is a reset of timespan to default values required?
	//
	// clears all
	this->Reset();

	m_extension.Reversed(true);
	m_extension.RowLimit(CFilterExtension::eDefaultRowLimit);
	m_extension.ReverseBinding(_T("time"));

	m_status = CRecord::eStatusUnreported;
}

const
CTimeSpan&   CFilter::TimeSpan(void)const                           { return m_tm_span;              }
CTimeSpan&   CFilter::TimeSpan(void)                                { return m_tm_span;              }
CAtlString   CFilter::ToString(void)const
{
	CAtlString cs_filter;
	if (!this->Enabled())
		return cs_filter;

	if (m_tm_span.Enabled() && m_tm_span.IsValid())
		cs_filter += m_tm_span.ToString();

	CAtlString cs_pattern = m_target;

	if (m_watched > -1)
	{
		CWatchedPersistent pers_(CWatchedType::eIncluded);
		HRESULT hr_ = pers_.Load();
		if (!FAILED(hr_))
		{
			const INT count_ = pers_.Count();
			if (m_watched < count_)
				cs_pattern = pers_.ItemOf(m_watched);
		}
	}
	//
	// TODO: field name(s) must be taken from schema definition;
	//       the hard code must be removed;
	//
	if (!cs_pattern.IsEmpty())
	{
		if (!cs_filter.IsEmpty()) cs_filter += _T(" and ");

		cs_filter += _T("target like '");
		cs_filter += cs_pattern;
		cs_filter += _T("%'");
	}

	if (m_action)
	{
		if (!cs_filter.IsEmpty()) cs_filter += _T(" and ");
		
		CAtlString cs_action;
		cs_action.Format(
				_T("action_id = %d"),
				m_action
			);

		cs_filter += cs_action;
	}

	if (m_status > CRecord::eStatusNone)
	{
		if (!cs_filter.IsEmpty()) cs_filter += _T(" and ");
		
		CAtlString cs_status;
		cs_status.Format(
				_T("status = %d"),
				m_status
			);

		cs_filter += cs_status;
	}

	return cs_filter;
}

bool         CFilter::Validate(void)const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (m_tm_span.Enabled() == false 
			&& m_target.IsEmpty()
				&& !m_action
					&& m_status < 0
						&& m_watched < 0)
		m_error.SetState(
				OLE_E_BLANK,
				_T("No filter criterion is defined")
			);
	return (!m_error);
}

INT          CFilter::Watched (void)const                           { return m_watched;              }
VOID         CFilter::Watched (const INT _watched)                  { m_watched = _watched;          }

/////////////////////////////////////////////////////////////////////////////

const
CFilterExtension& CFilter::Extension(void)const                     { return m_extension;            }
CFilterExtension& CFilter::Extension(void)                          { return m_extension;            }

/////////////////////////////////////////////////////////////////////////////

CFilter&     CFilter::operator=(const CFilter& _flt)
{
	this->m_error    = _flt.m_error;
	this->m_tm_span  = _flt.m_tm_span;
	this->m_enabled  = _flt.m_enabled;
	this->m_watched  = _flt.m_watched;

	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CFilterPersistent::CFilterPersistent(void)
{
	m_error.Source(_T("CFilterPersistent"));
}

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data { namespace details
{
	class  CFilterPersistent_Registry
	{
	private:
		CRegistryStorage  m_stg;
	public:
		CFilterPersistent_Registry(void) : m_stg(HKEY_CURRENT_USER)
		{
		}
	public:
		HRESULT           Load(CFilter& _filter)
		{
			HRESULT hr_ = S_OK;
			LONG lValue = 0;

			hr_ = m_stg.Load(
					this->_FolderName(),
					this->_NamedValueAction(),
					lValue
				);
			if (S_OK == hr_)
				_filter.Action(static_cast<INT>(lValue));

			hr_ = m_stg.Load(
					this->_FolderName(),
					this->_NamedValueEnabled(),
					lValue
				);
			if (S_OK == hr_)
				_filter.Enabled(!!lValue);

			CAtlString cs_value;

			hr_ = m_stg.Load(
					this->_FolderName(),
					this->_NamedValueDateFrom(),
					cs_value
				);
			if (S_OK == hr_)
			{
				const time_t date_ = ::_tstoi64(cs_value);
				_filter.TimeSpan().From() = date_;
			}

			hr_ = m_stg.Load(
					this->_FolderName(),
					this->_NamedValueDateUpto(),
					cs_value
				);
			if (S_OK == hr_)
			{
				const time_t date_ = ::_tstoi64(cs_value);
				_filter.TimeSpan().Upto() = date_;
			}

			hr_ = m_stg.Load(
					this->_FolderName(),
					this->_NamedValueDateEnabled(),
					lValue
				);
			if (S_OK == hr_)
				_filter.TimeSpan().Enabled(!!lValue);

			hr_ = m_stg.Load(
					this->_FolderName(),
					this->_NamedValueFilePattern(),
					cs_value
				);
			if (S_OK == hr_)
				_filter.Target(cs_value);

			hr_ = m_stg.Load(
					this->_FolderName(),
					this->_NamedValueReverse(),
					lValue
				);
			if (S_OK == hr_)
				_filter.Extension().Reversed(!!lValue);

			hr_ = m_stg.Load(
					this->_FolderName(),
					this->_NamedValueRowLimit(),
					lValue
				);
			if (S_OK == hr_)
				_filter.Extension().RowLimit(static_cast<INT>(lValue));

			hr_ = m_stg.Load(
					this->_FolderName(),
					this->_NamedValueUnreported(),
					lValue
				);
			if (S_OK == hr_)
				_filter.Status(static_cast<INT>(lValue));

			hr_ = m_stg.Load(
					this->_FolderName(),
					this->_NamedValueWatched(),
					lValue
				);
			if (S_OK == hr_)
				_filter.Watched(static_cast<INT>(lValue));

			if (hr_ ==S_FALSE)
				hr_ = S_OK;

			return  hr_;
		}
		HRESULT           Save(const CFilter& _filter)
		{
			HRESULT hr_ = S_OK;

			hr_ = m_stg.Save(
					this->_FolderName(),
					this->_NamedValueAction(),
					static_cast<LONG>(_filter.Action())
				);

			hr_ = m_stg.Save(
					this->_FolderName(),
					this->_NamedValueEnabled(),
					static_cast<LONG>(_filter.Enabled())
				);

			hr_ = m_stg.Save(
					this->_FolderName(),
					this->_NamedValueDateFrom(),
					_filter.TimeSpan().FromAsText()
				);

			hr_ = m_stg.Save(
					this->_FolderName(),
					this->_NamedValueDateUpto(),
					_filter.TimeSpan().UptoAsText()
				);

			hr_ = m_stg.Save(
					this->_FolderName(),
					this->_NamedValueDateEnabled(),
					static_cast<LONG>(_filter.TimeSpan().Enabled())
				);

			hr_ = m_stg.Save(
					this->_FolderName(),
					this->_NamedValueFilePattern(),
					_filter.Target()
				);

			hr_ = m_stg.Save(
					this->_FolderName(),
					this->_NamedValueReverse(),
					static_cast<LONG>(_filter.Extension().Reversed())
				);

			hr_ = m_stg.Save(
					this->_FolderName(),
					this->_NamedValueRowLimit(),
					static_cast<LONG>(_filter.Extension().RowLimit())
				);

			hr_ = m_stg.Save(
					this->_FolderName(),
					this->_NamedValueUnreported(),
					static_cast<LONG>(_filter.Status())
				);

			hr_ = m_stg.Save(
					this->_FolderName(),
					this->_NamedValueWatched(),
					static_cast<LONG>(_filter.Watched())
				);
			return  hr_;
		}
	private:
		CAtlString   _FolderName(void)            const { return CAtlString(_T("DataFilter"));  }
		CAtlString   _NamedValueAction(void)      const { return CAtlString(_T("Action"));      }
		CAtlString   _NamedValueEnabled(void)     const { return CAtlString(_T("Enabled"));     }
		CAtlString   _NamedValueDateFrom(void)    const { return CAtlString(_T("DateFrom"));    }
		CAtlString   _NamedValueDateUpto(void)    const { return CAtlString(_T("DateUpto"));    }
		CAtlString   _NamedValueDateEnabled(void) const { return CAtlString(_T("DateEnabled")); }
		CAtlString   _NamedValueFilePattern(void) const { return CAtlString(_T("FilePattern")); }
		CAtlString   _NamedValueReverse(void)     const { return CAtlString(_T("Reverse"));     }
		CAtlString   _NamedValueRowLimit(void)    const { return CAtlString(_T("RowLimit"));    }
		CAtlString   _NamedValueUnreported(void)  const { return CAtlString(_T("Unreported"));  } 
		CAtlString   _NamedValueWatched(void)     const { return CAtlString(_T("Watched"));     } 
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

TErrorRef    CFilterPersistent::Error(void)const
{
	return m_error;
}

HRESULT      CFilterPersistent::Load (void)
{
	m_error.Module(_T(__FUNCTION__));

	details::CFilterPersistent_Registry reg_;
	m_error = reg_.Load(*this);

	return m_error;
}

HRESULT      CFilterPersistent::Save (void)
{
	m_error.Module(_T(__FUNCTION__));

	details::CFilterPersistent_Registry reg_;
	m_error = reg_.Save(*this);

	return m_error;
}