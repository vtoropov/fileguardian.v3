/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Jan-2016 at 10:12:24am, GMT+7, Phuket, Rawai, Saturday;
	This is File Watcher background service share4d library data model defenition(s) implementation file.
*/
#include "StdAfx.h"
#include "FG_SysEvent_Model.h"

using namespace fg::common::data;

#include "Shared_DateTime.h"

using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data
{
	CAtlString  SystemEventIdToText(const eFsEventType::_e _id)
	{
		CAtlString event_;

		switch (_id)
		{
		case eFsEventType::eFsEventAltered : event_ = _T("changed") ; break;
		case eFsEventType::eFsEventCopied  : event_ = _T("copied")  ; break;
		case eFsEventType::eFsEventCreated : event_ = _T("created") ; break;
		case eFsEventType::eFsEventDeleted : event_ = _T("deleted") ; break;
		case eFsEventType::eFsEventRenamed : event_ = _T("renamed") ; break;
		case eFsEventType::eFsEventMoved   : event_ = _T("moved")   ; break;
		case eFsEventType::eFsEventReplaced: event_ = _T("replaced"); break;
		case eFsEventType::eFsEventMovedToRecycle: event_ = _T("recycled"); break;
		case eFsEventType::eFsEventRestoredFromRecycle: event_ = _T("restored"); break;
		default:
			 event_ = _T("unknown");
		}

		return event_;
	}
}}}

TSystemEventCollection CSystemEventEnum::m_events;
/////////////////////////////////////////////////////////////////////////////

CSystemEventEnum::CSystemEventEnum(void)
{
	if (!m_events.empty())
		return;

	eFsEventType::_e evtId[] = {
			eFsEventType::eFsEventCreated,
			eFsEventType::eFsEventCopied ,
			eFsEventType::eFsEventAltered,
			eFsEventType::eFsEventRenamed,
			eFsEventType::eFsEventDeleted,
			eFsEventType::eFsEventMoved  ,
			eFsEventType::eFsEventReplaced,
			eFsEventType::eFsEventMovedToRecycle,
			eFsEventType::eFsEventRestoredFromRecycle
		};
	for (INT i_ = 0; i_ < _countof(evtId); i_++)
	{
		CAtlString cs_evt = SystemEventIdToText(evtId[i_]);
		CSystemEvent evt_;

		evt_._id    = evtId[i_];
		evt_._name  = cs_evt;
		evt_._valid = true;

		try
		{
			m_events.push_back(evt_);
		}
		catch(::std::bad_alloc&)
		{
			break;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////

INT      CSystemEventEnum::Count(void)const
{
	return static_cast<INT>(m_events.size());
}

const
TSystemEventCollection& CSystemEventEnum::Events(void)const
{
	return m_events;
}

const
CSystemEvent& CSystemEventEnum::Item (const INT nIndex)const
{
	static CSystemEvent evt_(false);
	if (nIndex < 0 || nIndex > this->Count() - 1)
		return evt_;
	else
		return m_events[nIndex];
}

/////////////////////////////////////////////////////////////////////////////

CRecord::CRecord(void) : 
	m_timestamp(0), m_action(0), m_status(CRecord::eStatusUnreported), m_loc_time(false), m_time_ex(0), m_type(CObjectType::eUnknown) { }
CRecord::CRecord(const CRecord& _rec) :
	m_timestamp(0), m_action(0), m_status(CRecord::eStatusUnreported), m_loc_time(false), m_time_ex(0), m_type(CObjectType::eUnknown) { *this = _rec; }
CRecord::CRecord(const FILETIME& _timestamp, const CSystemEventPair& _pair) :
	m_timestamp(0),
	m_source(_pair.lpsz_source),
	m_target(_pair.lpsz_target),
	m_action(_pair.n_action)   ,
	m_status(_pair.n_status)   , m_loc_time(false), m_time_ex(0), m_type(CObjectType::eUnknown) {
	CUnixTime nx_(true); nx_ = _timestamp; m_timestamp = nx_;
}

CRecord::CRecord(const SYSTEMTIME& _timestamp, const CSystemEventPair& _pair) :
	m_timestamp(0),
	m_source(_pair.lpsz_source),
	m_target(_pair.lpsz_target),
	m_action(_pair.n_action)   ,
	m_status(_pair.n_status)   , m_loc_time(false), m_time_ex(0), m_type(CObjectType::eUnknown) {
	CUnixTime nx_(true); nx_ = _timestamp; m_timestamp = nx_;
}

CRecord::CRecord(const time_t _timestamp,  const CSystemEventPair& _pair) :
	m_timestamp(_timestamp),
	m_source(_pair.lpsz_source),
	m_target(_pair.lpsz_target),
	m_action(_pair.n_action)   ,
	m_status(_pair.n_status)   , m_loc_time(false), m_time_ex(0), m_type(CObjectType::eUnknown) {
}

CRecord::CRecord(const CEventItem& _evt) :
	m_timestamp(0), m_action(0), m_status(CRecord::eStatusUnreported), m_loc_time(false), m_time_ex(0), m_type(CObjectType::eUnknown) {

	*this = _evt;
}

/////////////////////////////////////////////////////////////////////////////

INT          CRecord::Action   (void)const           { return m_action;  }
VOID         CRecord::Action   (const INT _val)      { m_action = _val;  }

CAtlString   CRecord::ActionAsText(void)const
{
	eFsEventType::_e id_ = eFsEventType::eFsEventUnknown;

	switch (m_action)
	{
	case 0x0001: id_ = eFsEventType::eFsEventCreated;   break;
	case 0x0002: id_ = eFsEventType::eFsEventCopied;    break;
	case 0x0004: id_ = eFsEventType::eFsEventAltered;   break;
	case 0x0008: id_ = eFsEventType::eFsEventRenamed;   break;
	case 0x0010: id_ = eFsEventType::eFsEventDeleted ;  break;
	case 0x0020: id_ = eFsEventType::eFsEventMoved ;    break;
	case 0x0040: id_ = eFsEventType::eFsEventReplaced ; break;
	case 0x0080: id_ = eFsEventType::eFsEventMovedToRecycle ; break;
	case 0x0100: id_ = eFsEventType::eFsEventRestoredFromRecycle ; break;
	}
	return SystemEventIdToText(id_);
}

bool         CRecord::IsLocalTime(void)const         { return m_loc_time; }
VOID         CRecord::IsLocalTime(const bool _v)     { m_loc_time = _v;   }
LPCTSTR      CRecord::Process  (void)const           { return m_process.GetString();}
VOID         CRecord::Process  (LPCTSTR _val)        { m_process = _val;  }
LPCTSTR      CRecord::Source   (void)const           { return m_source.GetString(); }
VOID         CRecord::Source   (LPCTSTR _val)        { m_source = _val;   }
INT          CRecord::Status   (void)const           { return m_status;   }
VOID         CRecord::Status   (const INT _val)
{
	if (false){}
	else if (_val < CRecord::eStatusNone)     m_status  = CRecord::eStatusNone;
	else if (_val > CRecord::eStatusReported) m_status  = CRecord::eStatusReported;
	else
		m_status  = _val;
}

CAtlString   CRecord::StatusAsText(void)const
{
	CAtlString cs_status;
	switch (m_status)
	{
	case CRecord::eStatusUnreported: cs_status = _T("Unreported"); break;
	case CRecord::eStatusReported  : cs_status = _T("Reported");   break;
	default:
		cs_status = _T("Undefined");
	}
	return cs_status;
}

LPCTSTR      CRecord::Target  (void)const   { return m_target.GetString(); }
VOID         CRecord::Target  (LPCTSTR _val){ m_target = _val;    }

time_t       CRecord::Timestamp(void)const  { return m_timestamp; }
VOID         CRecord::Timestamp(const FILETIME& _val) { CUnixTime nx_(true); nx_ = _val; m_timestamp = nx_; }
VOID         CRecord::Timestamp(const SYSTEMTIME& _val) { CUnixTime nx_(true); nx_ = _val; m_timestamp = nx_; }
VOID         CRecord::Timestamp(const time_t _val, const bool bLocal) { m_timestamp = _val; m_loc_time = bLocal; }
LONGLONG     CRecord::TimestampEx(void)const { return m_time_ex; }
VOID         CRecord::TimestampEx(const LONGLONG _val) { m_time_ex = _val; } 

CAtlString   CRecord::TimestampAsText(const bool bFormatted)const
{
	CAtlString cs_time;
	CUnixTime nx_(true); nx_ = m_timestamp; nx_.ToString(bFormatted, cs_time);
	return cs_time;
}

CObjectType::_e
             CRecord::Type(void)const   { return m_type; }
VOID         CRecord::Type(const CObjectType::_e _type) { m_type = _type; }
VOID         CRecord::TypeAsLong(const LONG _val) {
	if (CObjectType::eUnknown == _val) m_type = CObjectType::eUnknown;
	if (CObjectType::eFolder  == _val) m_type = CObjectType::eFolder;
	if (CObjectType::eFile    == _val) m_type = CObjectType::eFile;
}

CAtlString   CRecord::TypeAsText(void)const {

	CAtlString cs_type;
	switch (m_type) {
	case CObjectType::eFile  :  cs_type = _T("File"); break;
	case CObjectType::eFolder:  cs_type = _T("Folder"); break;
	default:
	cs_type = _T("");
	}

	return cs_type;
}

/////////////////////////////////////////////////////////////////////////////

CRecord&     CRecord::operator=(const CRecord& _rec)
{
	this->m_timestamp = _rec.m_timestamp;
	this->m_source    = _rec.m_source;
	this->m_target    = _rec.m_target;
	this->m_action    = _rec.m_action;
	this->m_status    = _rec.m_status;
	this->m_time_ex   = _rec.m_time_ex;
	this->m_process   = _rec.m_process;
	this->m_type      = _rec.m_type;

	return *this;
}

CRecord&     CRecord::operator= (const CEventItem& _evt)
{
	this->m_timestamp = _evt.Time().Completion();
	this->m_source    = _evt.Source();
	this->m_target    = _evt.Target();
	this->m_action    = _evt.Action();
	this->m_status    = 0;
	this->m_time_ex   = _evt.TimeEx();
	this->m_process   = _evt.Process().Path();
	this->m_type      = _evt.Type();
	return *this;
}
/////////////////////////////////////////////////////////////////////////////

CRecordCache::CRecordCache(void) {}

/////////////////////////////////////////////////////////////////////////////

VOID         CRecordCache::Clear(void)
{
	SAFE_LOCK(m_sync_obj);
	if (m_data.empty() == false) {
		while(!m_data.empty()) m_data.pop();
	}
}

CRecord      CRecordCache::Get(void)
{
	if (this->IsEmpty())
		return CRecord();

	CRecord rec_; {
		SAFE_LOCK(m_sync_obj);
		rec_ = m_data.front();
		m_data.pop();
	}
	return rec_;
}

bool         CRecordCache::IsEmpty(void)const
{
	bool bEmpty = true;
	{
		SAFE_LOCK(m_sync_obj);
		bEmpty = m_data.empty();
	}
	return bEmpty;
}

HRESULT      CRecordCache::Put(const CRecord& _rec)
{
	SAFE_LOCK(m_sync_obj);
	try {
		m_data.push(_rec);
	}
	catch(::std::bad_alloc&) {
		return E_OUTOFMEMORY;
	}
	return S_OK;
}

INT          CRecordCache::Size(void)const
{
	INT nSize = 0;
	{
		SAFE_LOCK(m_sync_obj);
		nSize = static_cast<INT>(m_data.size());
	}
	return nSize;
}

/////////////////////////////////////////////////////////////////////////////

CRecordLimit::CRecordLimit(void) : m_offset(0), m_limit(0) {}
CRecordLimit::CRecordLimit(const LONG _off, const LONG _lmt) : m_offset(_off), m_limit(_lmt) {}
CRecordLimit::CRecordLimit(const CRecordLimit& _rec_lmt) {
	*this = _rec_lmt;
}

/////////////////////////////////////////////////////////////////////////////

bool          CRecordLimit::IsValid(void)const     { return (-1 < m_offset && 0 < m_limit); }
LONG          CRecordLimit::Limit(void)const       { return m_limit; }
HRESULT       CRecordLimit::Limit(const LONG _val) { if (_val < 1) return E_INVALIDARG; m_limit = _val;  return S_OK; }
LONG          CRecordLimit::Offset(void)const      { return m_offset; }
HRESULT       CRecordLimit::Offset(const LONG _val){ if (_val < 1) return E_INVALIDARG; m_offset = _val; return S_OK; }

/////////////////////////////////////////////////////////////////////////////

CRecordLimit& CRecordLimit::operator=(const CRecordLimit& _rec_lmt) {
	this->Limit(_rec_lmt.Limit());
	this->Offset(_rec_lmt.Offset());
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CRecordRange_simplified::CRecordRange_simplified(void) : m_max(0), m_min(0) {}
CRecordRange_simplified::CRecordRange_simplified(const LONG _min, const LONG _max) : m_max(_max), m_min(_min) {}

/////////////////////////////////////////////////////////////////////////////

bool  CRecordRange_simplified::IsValid(void)const {
	if (!m_min && !m_max) return false;
	if ( m_min  > m_max)  return false;
	return true;
}

/////////////////////////////////////////////////////////////////////////////

LONG       CRecordRange_simplified::Max  (void)const       { return m_max; }
VOID       CRecordRange_simplified::Max  (const LONG _val) { m_max = _val; }
LONG       CRecordRange_simplified::Min  (void)const       { return m_min; }
VOID       CRecordRange_simplified::Min  (const LONG _val) { m_min = _val; }

/////////////////////////////////////////////////////////////////////////////

CRecordRange::CRecordRange(void) : m_value(0) { }
CRecordRange::CRecordRange(const LONG _val, const LONG _min, const LONG _max) : m_value(_val), TBase(_min, _max) {}

/////////////////////////////////////////////////////////////////////////////

bool         CRecordRange::IsValid(void)const {
	if (!TBase::IsValid())
		return false;
	if ( m_value < m_min || m_value > m_max)
		return false;
	else
		return true;
}

LONG         CRecordRange::Value(void) const { return m_value; }
HRESULT      CRecordRange::Value(const LONG _value) {
	m_value = _value;

	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT      CRecordRange::Value(LPCTSTR lpszValueAsText) {
	const LONG lValue = ::_tstol(lpszValueAsText);
	HRESULT hr_ = this->Value(lValue);
	return  hr_;
}

CAtlString   CRecordRange::ValueAsFormattedText(void)const {
	CAtlString cs_value = this->ValueAsText();

	TCHAR buffer_[1024] = {0};
	::GetNumberFormat(
			LOCALE_USER_DEFAULT,
			0,
			cs_value,
			NULL,
			buffer_,
			_countof(buffer_)
		);
	return (cs_value = buffer_);
}

CAtlString   CRecordRange::ValueAsText(void)const {
	CAtlString cs_value;
	cs_value.Format(
			_T("%d"),
			m_value
		);
	return (cs_value);
}

/////////////////////////////////////////////////////////////////////////////

CRecordRange::operator LONG(void)const { return m_value; }