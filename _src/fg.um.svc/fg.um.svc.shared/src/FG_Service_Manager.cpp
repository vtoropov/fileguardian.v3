/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Feb-2016 at 10:27:11pm, GMT+7, Phuket, Rawai, Tuesday;
	This is File Guardian Background Service Manager class implementation file.
*/
#include "StdAfx.h"
#include "FG_Service_Manager.h"

using namespace fg::common;

#include "FG_Service_CrtData.h"

using namespace fg::common::data;

using namespace shared::service;

/////////////////////////////////////////////////////////////////////////////

CFwServiceManager::CFwServiceManager(void)
{
	m_error.Source(_T("CFwServiceManager"));
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = InitializeServiceCrtData(m_crt);
	if (FAILED(hr_))
		m_error = (hr_);
	else
	{
		hr_ = m_mgr.Initialize(CServiceManagerParams::eOpenForService);
		if (FAILED(hr_))
			m_error = m_mgr.Error();
	}
}

CFwServiceManager::CFwServiceManager(const CAtlString& _exe_path)
{
	m_error.Source(_T("CFwServiceManager"));
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = InitializeServiceCrtData(m_crt);
	if (FAILED(hr_))
		m_error = (hr_);
	else
	{
		m_crt.Path() = _exe_path;
		hr_ = m_mgr.Initialize(CServiceManagerParams::eOpenForService);
		if (FAILED(hr_))
			m_error = m_mgr.Error();
	}
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef CFwServiceManager::Error(void) const
{
	return m_error;
}

bool      CFwServiceManager::InstallService(void)
{
	if (!m_mgr.IsValid())
		return false;

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	HRESULT hr_ = m_mgr.Create(m_crt, m_svc);
	if (FAILED(hr_))
		m_error = m_mgr.Error();

	return (!m_error);
}

bool      CFwServiceManager::IsInstalled(void)
{
	if (!m_mgr.IsValid())
		return false;
	CService svc_;
	HRESULT hr_ = m_mgr.Open(m_crt, CServiceAccessLevel::eQueryStatus, svc_);
	return !!SUCCEEDED(hr_);
}

const
CService& CFwServiceManager::GetServiceObject(void)
{
	HRESULT hr_ = m_mgr.Open(m_crt, CServiceAccessLevel::eQueryStatus, m_svc);
	if (SUCCEEDED(hr_))
		hr_ = m_svc.UpdateCurrentState();

	if (!m_svc.State().Error())
	     m_svc.State().Error() = hr_;

	if (FAILED(hr_))
		m_svc.State().Error().Source(_T(__FUNCTION__));
	
	return m_svc;
}

bool      CFwServiceManager::StartService(void)
{
	if (!m_mgr.IsValid())
		return false;

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const CAtlString cs_name = m_crt.Name();

	HRESULT hr_ = m_mgr.Start(cs_name);
	if (FAILED(hr_))
		m_error = m_mgr.Error();

	return (!m_error);
}

bool      CFwServiceManager::StopService(void)
{
	if (!m_mgr.IsValid())
		return false;

	const CAtlString cs_name = m_crt.Name();

	HRESULT hr_ = m_mgr.Stop(cs_name);
	if (FAILED(hr_))
		m_error = m_mgr.Error();

	return (!m_error);
}

bool      CFwServiceManager::UninstallService(void)
{
	if (!m_mgr.IsValid())
		return false;

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const CAtlString cs_name = m_crt.Name();

	HRESULT hr_ = m_mgr.Remove(cs_name);
	if (FAILED(hr_))
		m_error = m_mgr.Error();

	return (!m_error);
}