/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Apr-2016 at 9:11:02pm, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian License Data Provider class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 11:50:01a, UTC+7, Phuket, Rawai, Thursday
*/
#include "StdAfx.h"
#include "FG_License_Provider.h"

using namespace fg::common::lic;

#include "Shared_Registry.h"
#include "Shared_GenericAppObject.h"
#include "Shared_NetHttpProvider.h"

using namespace shared::registry;
using namespace shared::user32;
using namespace shared::net;

#include "FG_Generic_Encrypt.h"
#include "FG_Json_Provider.h"

using namespace fg::common::data;

#include "FG_Generic_defs.h"
/////////////////////////////////////////////////////////////////////////////

CLicenseStorage::CLicenseStorage(void) {
	m_error.Source(_T("CLicenseStorage"));
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef      CLicenseStorage::Error(void)const { return m_error; }
HRESULT        CLicenseStorage::Load (CLicenseInfo& _lic)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CLicenseInfoPersistent pers_(_lic);
	HRESULT hr_ = pers_.Load();
	if (FAILED(hr_))
		m_error = pers_.Error();
	else
		_lic = (CLicenseInfo)pers_;

	return  m_error;
}

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace lic { namespace details
{
	const CCryptoDataCrt&
	      LicenseProvider_GetCryptoCrtData(void)
	{
		static CCryptoDataCrt crt_;
		static bool bInitialized = false;
		if (!bInitialized)
		{
			bInitialized = true;
			crt_.Algorithm(_FW_LIC_CRYPTO_ALGO);
			crt_.Key      (_FW_LIC_CRYPTO_KEY );
			crt_.Mode     (_FW_LIC_CRYPTO_MODE);
			crt_.Vector   (_FW_LIC_CRYPTO_VECTOR);
		}
		return crt_;
	}

	HRESULT  LicenseProvider_DecodeData(CLicenseInfo& _lic, const CAtlString& _response, CSysError& _error)
	{
		// (1) decrypts response data
		CAtlString decoded_;
		{
			CCryptoDataProviderBase cry_(details::LicenseProvider_GetCryptoCrtData());
			HRESULT hr_ = cry_.Decrypt(_response, decoded_);
			if (FAILED(hr_))
			{
				_error.SetState(
						hr_,
						_T("Encryption protocol error")
					);
				return (_error);
			}
		}
		// (2) extracts license data from the response
#if (0)
		decoded_ = _T("{ \"error\" : { \"code\" : \"20\" , \"details\" : \"Provided serial number data is invalid\"}}");
#endif
		{
			CJsonDataProvider json_;
			HRESULT hr_ = json_.ParseLicResponse(decoded_, _lic);
			if (FAILED(hr_))
			{
				_error.SetState(
						hr_,
						json_.Error().GetDescription()
					);
				return _error;
			}
		}
		return (_error = S_OK);
	}

	HRESULT  LicenseProvider_EncodeData(const CLicenseInfo& _lic, CAtlString& _pst_data, CSysError& _error)
	{
		// (1) generates json data for sending to web service
		{
			CJsonDataProvider json_;
			_pst_data = json_.CreateLicRequest(_lic);
		}
		// (2) encrypts json data and completes post data
		{
			CAtlString encoded_;
			CCryptoDataProviderBase cry_(details::LicenseProvider_GetCryptoCrtData());
			HRESULT hr_ = cry_.Encrypt(_pst_data, encoded_);
			if (FAILED(hr_))
				(_error = cry_.Error());
			else
			{
				_pst_data.Format(
						_T("data='%s'"),
						(LPCTSTR)encoded_
					);
			}
		}
		return _error;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CLicenseProvider::CLicenseProvider(void)
{
	m_error.Source(_T("CLicenseProvider"));
}

CLicenseProvider::~CLicenseProvider(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CLicenseProvider::CheckLicense(CLicenseInfo& _lic)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	_lic.State() = CLicenseState::eError; // beforehand assignment

	CAtlString pst_dat_;
	// (1) prepares post data for sending
	HRESULT hr_ = details::LicenseProvider_EncodeData(_lic, pst_dat_, m_error);
	if (FAILED(hr_))
		return m_error;
	// (2) sends data to a web service and waits for a response
	CAtlString response_;
	{
		CHttpProvider web_;
		hr_ = web_.Initialize();
		if (FAILED(hr_))
			return (m_error = web_.Error());

		hr_ = web_.DoPostRequest(_FW_LIC_WEB_CHECK_QUERY, response_, pst_dat_);
		if (FAILED(hr_))
			return (m_error = web_.Error());
	}
	// (3) extracts license data from the response
	hr_ = details::LicenseProvider_DecodeData(_lic, response_, m_error);

	return m_error;
}

HRESULT       CLicenseProvider::CheckLicense(CLicenseState& _current)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CLicenseInfoPersistent lic_;
	HRESULT hr_ = lic_.Load();
	if (FAILED(hr_))
	{
		_current = CLicenseState::eError;
		return (m_error = lic_.Error());
	}

	this->CheckLicense(lic_);

	_current = lic_.State();
	return m_error;
}

TErrorRef     CLicenseProvider::Error(void)const
{
	return m_error;
}

bool          CLicenseProvider::IsNewVersionAvailable(CLicenseInfo& _lic)const
{
	CLicVersion current_;
	current_.InitializeFromGlobalDef();
	return (_lic.Version() > current_);
}

HRESULT       CLicenseProvider::RegisterLicense(CLicenseInfo& _lic)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString uid_;

	CLicenseUser user_;
	HRESULT hr_ = user_.GenerateId(uid_);

	_lic.UserId(uid_);

	CAtlString pst_dat_;
	// (1) prepares post data for sending
	hr_ = details::LicenseProvider_EncodeData(_lic, pst_dat_, m_error);
	if (FAILED(hr_))
		return m_error;
	// (2) sends data to a web service and waits for a response
	CAtlString response_;
	{
		CHttpProvider web_;
		hr_ = web_.Initialize();
		if (FAILED(hr_))
			return (m_error = web_.Error());

		hr_ = web_.DoPostRequest(_FW_LIC_WEB_REGISTER_QUERY, response_, pst_dat_);
		if (FAILED(hr_))
			return (m_error = web_.Error());
	}
	// (3) extracts license data from the response
	hr_ = details::LicenseProvider_DecodeData(_lic, response_, m_error);
	if (FAILED(hr_))
		return (m_error);

	// (4) analyzes the state of the license
	if (CLicenseState::eUnknown == _lic.State().Identifier())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_DATA,
				_T("License service returned unknown license status.")
			);
	} else
	if (CLicenseState::eLicensed != _lic.State().Identifier())
	{
		m_error.SetState(
				CLASS_E_NOTLICENSED,
				_T("License status: %s"), (LPCTSTR)_lic.State().ToString()
			);
	}
	else // saves a serial number in case of success
	{
		CLicenseInfoPersistent pers_ = _lic;
		hr_ = pers_.Save();
		if (FAILED(hr_))
			m_error = pers_.Error();
	}

	return m_error;
}

HRESULT       CLicenseProvider::RemoveLicense(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CLicenseInfoPersistent lic_;
	HRESULT hr_ = lic_.Load();
	if (FAILED(hr_))
		return (m_error = lic_.Error());

	CAtlString pst_dat_;
	// (1) prepares post data for sending
	hr_ = details::LicenseProvider_EncodeData(lic_, pst_dat_, m_error);
	if (FAILED(hr_))
		return m_error;
	// (2) sends data to a web service and waits for a response
	CAtlString response_;
	{
		CHttpProvider web_;
		hr_ = web_.Initialize();
		if (FAILED(hr_))
			return (m_error = web_.Error());

		hr_ = web_.DoPostRequest(_FW_LIC_WEB_REMOVE_QUERY, response_, pst_dat_);
		if (FAILED(hr_))
			return (m_error = web_.Error());

	}
	// (3) extracts license data from the response
	hr_ = details::LicenseProvider_DecodeData(lic_, response_, m_error);
	{
		hr_ = lic_.Clear();
	}
	return m_error;
}

const
CLicenseStorage& CLicenseProvider::Storage(void)const { return m_storage; }
CLicenseStorage& CLicenseProvider::Storage(void)      { return m_storage; }