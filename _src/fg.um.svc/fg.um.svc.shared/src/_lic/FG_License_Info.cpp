/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Apr-2016 at 11:50:49am, GMT+7, Phuket, Rawai, Saturday;
	This is File Guardian common library license information related class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 10:59:53a, UTC+7, Phuket, Rawai, Thursday;
*/
#include "StdAfx.h"
#include "FG_License_Info.h"
#include "FG_Generic_Encrypt.h"

using namespace fg::common::lic;
using namespace fg::common::data;

#include "Shared_Registry.h"
#include "Shared_SystemInfo.h"
#include "wmi.service.deprecated.h"
#include "Shared_GenericAppObject.h"

using namespace shared::lite::data;
using namespace shared::lite::common;
using namespace shared::user32;
using namespace shared::registry;

#include "FG_Generic_Defs.h"
#include "FG_Generic_version.h"
/////////////////////////////////////////////////////////////////////////////

CLicenseState::CLicenseState(void) : m_id(CLicenseState::eUnknown)
{
}

CLicenseState::CLicenseState(const CLicenseState::_e _id) : m_id(_id)
{
}

/////////////////////////////////////////////////////////////////////////////

CLicenseState::_e CLicenseState::Identifier(void)const
{
	return m_id;
}

CAtlString        CLicenseState::ToString(void)const
{
	CAtlString cs_title;
	switch (m_id)
	{
	case CLicenseState::eExpired :  cs_title = _T("Expired");  break;
	case CLicenseState::eLicensed:  cs_title = _T("Licensed"); break;
	case CLicenseState::eRemoved :  cs_title = _T("Removed");  break;
	case CLicenseState::ePending :  cs_title = _T("Pending");  break;
	case CLicenseState::eError   :  cs_title = _T("#Error");   break;
	case CLicenseState::eUnknown :
	default:
		cs_title = _T("Unknown");
	}
	return cs_title;
}

/////////////////////////////////////////////////////////////////////////////

CLicenseState& CLicenseState::operator= (const CLicenseState::_e _state)
{
	m_id = _state;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CLicenseState::_e CLicenseState::StringToState(LPCTSTR lpszState)
{
	CLicenseState::_e state_ = CLicenseState::eUnknown;

	CAtlString cs_state(lpszState);
	if (cs_state.IsEmpty())
		return state_;
	else if (0 == cs_state.CompareNoCase(_T("licensed"))) state_ = CLicenseState::eLicensed;
	else if (0 == cs_state.CompareNoCase(_T("expired" ))) state_ = CLicenseState::eExpired;

	return state_;
}

/////////////////////////////////////////////////////////////////////////////

CLicVersion::CLicVersion(LPCTSTR lpszVersion)
{
	m_error.Source(_T("CLicVersion"));
	if (lpszVersion)
		this->Value(lpszVersion);
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef      CLicVersion::Error(void)const
{
	return m_error;
}

HRESULT        CLicVersion::InitializeFromGlobalDef(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_version(FG_PRODUCT_VER);

	this->Value(cs_version);

	return m_error;
}

HRESULT        CLicVersion::InitializeFromResource(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const CApplication& the_app = GetAppObjectRef();
	CAtlString cs_version = the_app.Version().FileVersionFixed();
	if (cs_version.IsEmpty()
		|| 0 == cs_version.CompareNoCase(_T("#n/a"))){
			m_error.SetState(
					(DWORD)ERROR_INVALID_DATA,
					_T("The executable file does not contain file version info.")
				);
	}
	else
		this->Value(cs_version.GetString());
	return m_error;
}

LONGLONG       CLicVersion::ToLong(void)const
{
	LONGLONG l_ver = 0;
	CAtlString cs_ver;
	switch (m_value.size()){
		case 4:
			{
				cs_ver.Format(
					_T("%.5u%.5u%.5u%.5u"), m_value[0], m_value[1], m_value[2], m_value[3]
				);
				l_ver = ::_tstoi64(cs_ver);
			} break;
	}
	return l_ver;
}

CAtlString     CLicVersion::Value(void)const
{
	CAtlString cs_ver;
	for (size_t j_ = 0; j_ < m_value.size(); j_++){
		CAtlString cs_part;
		cs_part.Format(
				_T("%u"), m_value[j_]
			);
		cs_ver += cs_part;
		if (j_ < m_value.size() - 1)
			cs_ver += _T(".");
	}
	return cs_ver;
}

HRESULT        CLicVersion::Value(LPCTSTR lpszVersion)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == lpszVersion || ::_tcslen(lpszVersion) < 1)
		return (m_error = E_INVALIDARG);

	if (m_value.empty() == false)
		m_value.clear();

	CAtlRegExp<> regex;
	const REParseError status = regex.Parse(_T("^{\\z*}.{\\z*}.{\\z*}.{\\z*}$"));
	if (REPARSE_ERROR_OK != status){
		m_error.SetState(
				(DWORD)ERROR_INTERNAL_ERROR,
				_T("Internal error in regular expression.")
			);
		return m_error;
	}

	CAtlREMatchContext<> match;

	if (!regex.Match(lpszVersion, &match)){
		m_error.SetState(
				(DWORD)(ERROR_INVALID_DATA),
				_T("Version number must be in format [0-9].[0-9].[0-9].[0-9]")
			);
		return m_error;
	}
	if (match.m_uNumGroups != CLicVersion::eNumberOfGroups){
		m_error.SetState(
				(DWORD)(ERROR_INVALID_DATA),
				_T("Version number must contain 4 (four) digit groups")
			);
		return m_error;
	}
	LPCTSTR start_ = NULL;
	LPCTSTR finish_ = NULL;

	for (UINT j_ = 0; j_ < CLicVersion::eNumberOfGroups; j_++){
		match.GetMatch(j_, &start_, &finish_);
		try {
			m_value.push_back(static_cast<UINT>(::_tstol(start_)));
		}
		catch(::std::bad_alloc&){
			return (m_error = E_OUTOFMEMORY);
		}
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

bool CLicVersion::operator> (const CLicVersion& _lic)const
{
	return this->ToLong() > _lic.ToLong();
}

bool CLicVersion::operator< (const CLicVersion& _lic)const
{
	return this->ToLong() < _lic.ToLong();
}

bool CLicVersion::operator==(const CLicVersion& _lic)const
{
	return this->ToLong() == _lic.ToLong();
}

/////////////////////////////////////////////////////////////////////////////

CLicenseInfo::CLicenseInfo(void) : m_expire(0)
{
}

/////////////////////////////////////////////////////////////////////////////

CAtlString     CLicenseInfo::DaysLeftAsText(void)const
{
	if (!m_expire)
		return CAtlString(_T("#na"));

	time_t rawtime;
	::time(&rawtime);

	double left_time = 0.0;
	left_time = ::difftime(m_expire, rawtime);
	left_time = left_time/(60.*60.*24); // converts to days

	CAtlString cs_days;

	const INT days_left = static_cast<INT>(::floor(left_time));

	if (false){}
	else if (days_left < 1 && left_time > 0.0)
		cs_days = _T("less than a day");
	else if (days_left > 0)
		cs_days.Format(_T("%d"), days_left);
	else
		cs_days = _T("0");

	return cs_days;
}

CAtlString     CLicenseInfo::Description(void)const
{
	return m_desc;
}

VOID           CLicenseInfo::Description(LPCTSTR lpszDesc)
{
	m_desc = lpszDesc;
}

time_t         CLicenseInfo::ExpireDate(void)const
{
	return m_expire;
}

VOID           CLicenseInfo::ExpireDate(const time_t _expire)
{
	m_expire = _expire;
}

VOID           CLicenseInfo::ExpireDate(LPCTSTR lpszExpireDate)
{
	m_expire = static_cast<time_t>(::_tstoi64(lpszExpireDate));
}

CAtlString     CLicenseInfo::ExpireDateAsText(void)const
{
	TCHAR buf_[_MAX_PATH] = {0};

	struct tm tm_ = {0};
	::localtime_s(&tm_, &m_expire);

	::_tcsftime(
			buf_,
			_countof(buf_),
			_T("%Y-%m-%d %H:%M:%S"),
			&tm_
		);
	CAtlString cs_expire = buf_;
	return cs_expire;
}

bool           CLicenseInfo::IsActive(void)const
{
	return CLicenseState::eLicensed == m_state.Identifier();
}

CAtlString     CLicenseInfo::Serial(void)const
{
	return m_serial;
}

HRESULT        CLicenseInfo::Serial(LPCTSTR lpszSerial)
{
	if (!lpszSerial || ::_tcslen(lpszSerial) != CLicenseInfo::eSerialNumberLen)
		return E_INVALIDARG;

	m_serial = lpszSerial;

	HRESULT hr_ = S_OK;
	return  hr_;
}

CAtlString     CLicenseInfo::SerialFormatted(void)const
{
	CAtlString cs_fmt_sn;

	for (INT i_ = 0; i_ < m_serial.GetLength(); i_++)
	{
		cs_fmt_sn.AppendChar(
				m_serial.GetAt(i_)
			);
		const bool bLast = (i_ == m_serial.GetLength() - 1);
		if (!((i_ + 1)% 4) && !bLast)
			cs_fmt_sn.AppendChar('-');
	}

	return cs_fmt_sn;
}

const
CLicenseState& CLicenseInfo::State(void)const
{
	return m_state;
}

CLicenseState& CLicenseInfo::State(void)
{
	return m_state;
}
#if defined(_DEBUG)
CAtlString     CLicenseInfo::ToString(LPCTSTR lpszSeparator)const
{
	LPCTSTR lpszFormat = _T("UserId:%s;serial:%s;expire:%s;days:%s;state:%s;version:%s;desc:%s");

	CAtlString cs_format;
	if (NULL != lpszSeparator) {
		cs_format = lpszFormat;
		cs_format.Replace(
				_T(";"), lpszSeparator
			);
	}

	CAtlString cs_lic;
	cs_lic.Format(
		(LPCTSTR)cs_format,
		(LPCTSTR)this->UserId(),
		(LPCTSTR)this->SerialFormatted(),
		(LPCTSTR)this->ExpireDateAsText(),
		(LPCTSTR)this->DaysLeftAsText(),
		(LPCTSTR)this->State().ToString(),
		(LPCTSTR)this->Version().Value(),
		(LPCTSTR)this->Description()
		);
	return cs_lic;
}
#endif
CAtlString     CLicenseInfo::UserId(void)const
{
	return m_userId;
}

HRESULT        CLicenseInfo::UserId(LPCTSTR lpszUserId)
{
	if (!lpszUserId || ::_tcslen(lpszUserId) != CLicenseInfo::eUserIdLen)
		return E_INVALIDARG;

	m_userId = lpszUserId;

	HRESULT hr_ = S_OK;
	return  hr_;
}
const
CLicVersion&   CLicenseInfo::Version(void)const
{
	return m_version;
}

CLicVersion&   CLicenseInfo::Version(void)
{
	return m_version;
}

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace lic { namespace details
{
	const CCryptoDataCrt&
	      CLicenseInfo_GetCryptoCrtData(void)
	{
		static CCryptoDataCrt crt_;
		static bool bInitialized = false;
		if (!bInitialized)
		{
			bInitialized = true;
			crt_.Algorithm(_T("rijndael-128"));
			crt_.Key      (_T("BZHS3Lu8PjyGLeBKDnDfr75T1WPvONHMRsLjnfkg"));
			crt_.Mode     (_T("cbc"));
			crt_.Vector   (_T("WWN7Gq5NeYFktsXhaubqbqP1UH1LbkQEIMBwZKJp"));
		}
		return crt_;
	}

	class CLicenseInfo_Encryptor : public CCryptoDataProviderBase
	{
		typedef CCryptoDataProviderBase TBase;
	public:
		  CLicenseInfo_Encryptor(void) : TBase(details::CLicenseInfo_GetCryptoCrtData())
		{
			m_error.Source(_T("CLicenseInfo_Encryptor"));
		}
	};

	class CLicenseInfo_Registry : private CRegistryPathFinder
	{
		typedef CRegistryPathFinder TBase;
	private:
		CRegistryStorage m_stg;
	public:
		CLicenseInfo_Registry(void) : m_stg(TBase::CService::SettingsRoot(), CRegistryOptions::eDoNotModifyPath)
		{
		}
	public:
		HRESULT      Clear(void)
		{
			HRESULT hr_ = m_stg.Remove(
					this->_GetRegPath(),
					this->_GetSerialNamedValue()
				);
			return hr_;
		}

		HRESULT      Load (CAtlString& _lic_encoded)
		{
			HRESULT hr_ = m_stg.Load(
					this->_GetRegPath(),
					this->_GetSerialNamedValue(),
					_lic_encoded
				);
			return  hr_;
		}

		HRESULT      Save (const CAtlString& _lic_encoded)
		{
			HRESULT hr_ = m_stg.Save(
					this->_GetRegPath(),
					this->_GetSerialNamedValue(),
					_lic_encoded
				);
			return hr_;
		}
	private:
		CAtlString  _GetRegPath(void)const
		{
			CAtlString path_= TBase::CService::SettingsPath();
			path_ += _T("\\License");
			return path_;
		}
		CAtlString  _GetDescNamedValue(void)const { return CAtlString(_T("Description")); }
		CAtlString  _GetSerialNamedValue(void)const { return CAtlString(_T("SerialNumber")); }
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CLicenseInfoPersistent::CLicenseInfoPersistent(void)
{
	m_error.Source(_T("CLicenseInfoPersistent"));
}

CLicenseInfoPersistent::CLicenseInfoPersistent(const CLicenseInfo& _lic)
{
	*this = _lic;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT        CLicenseInfoPersistent::Clear(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	details::CLicenseInfo_Registry reg_;
	HRESULT hr_ = reg_.Clear();
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}

TErrorRef      CLicenseInfoPersistent::Error(void)const
{
	return m_error;
}

HRESULT        CLicenseInfoPersistent::Load(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	details::CLicenseInfo_Registry reg_;

	CAtlString encoded_;
	HRESULT hr_ = reg_.Load(encoded_);
	if (FAILED(hr_))
		return (m_error = hr_);

	if (encoded_.IsEmpty())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_DATA,
				_T("Serial number is not set")
			);
		return m_error;
	}

	CAtlString cs_decoded;

	details::CLicenseInfo_Encryptor cry_;
	hr_ = cry_.Decrypt(encoded_, cs_decoded);
	if (FAILED(hr_))
		m_error = cry_.Error();
	else
	{
		const int pos_ = cs_decoded.Find(_T("\f"));
		if (-1 == pos_)
		{
			this->Serial(cs_decoded);
			this->Description(NULL);
		}
		else
		{
			this->Serial(cs_decoded.Left(pos_));
			this->Description(cs_decoded.Mid(pos_ + 1));
		}

		CAtlString uid_;
		CLicenseUser user_;
		hr_ = user_.GenerateId(uid_);
		if (FAILED(hr_))
			m_error = user_.Error();
		else
			this->UserId(uid_);
	}
	return m_error;
}

HRESULT        CLicenseInfoPersistent::Save(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString decoded_;
	decoded_ += this->Serial();
	decoded_ += _T("\f");
	decoded_ += this->Description();

	CAtlString encoded_;

	details::CLicenseInfo_Encryptor cry_;
	HRESULT hr_ = cry_.Encrypt(decoded_, encoded_);
	if (FAILED(hr_))
		m_error = cry_.Error();

	details::CLicenseInfo_Registry reg_;
	hr_ = reg_.Save(encoded_);
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CLicenseInfoPersistent& CLicenseInfoPersistent::operator= (const CLicenseInfo& _lic)
{
	this->ExpireDate(_lic.ExpireDate());
	this->Serial(_lic.Serial());
	this->State() = _lic.State();
	this->UserId(_lic.UserId());
	this->Description(_lic.Description());
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace lic { namespace details
{
	CSysError LicenseInfo_GetMediaAccCtrl  (CAtlString& _mac)
	{
		CSysError err_obj(_T(__FUNCTION__));
		err_obj = S_OK;

		// Tech_dog commented on 8-Nov-2016 at 12:18:33a, Phuket, Rawai, Tuesday;
		//-----------------------------------------------------------------------------
		// on some PCs we can have multiple network adapters, and we need to find the adapter with valid MAC,
		// the first adapter can have MAC property empty;

		LPCTSTR lpszEnsureProperty =_T("MACAddress");

		CWMI_NetworkProvider net_(lpszEnsureProperty);
		_mac = net_.GetMacAddress();

		if (net_.Error())
			err_obj = net_.Error();
		else
			_mac.Replace(_T(":"), _T(""));

		return err_obj;
	}

	CSysError LicenseInfo_GetSerialNumber  (CAtlString& _serial)
	{
		CSysError err_obj(_T(__FUNCTION__));
		err_obj = S_OK;
		_serial = CSysInfoDataProvider::WindowsDriveId();

		if (_serial.IsEmpty())
		{
			err_obj.Module(_T(__FUNCTION__));
			err_obj.SetState(
					(DWORD)ERROR_INVALID_DATA,
					_T("Cannot retreive a device serial number")
				);
		}

		return err_obj;
	}

	CSysError LicenseInfo_GetUniqueSequence(CAtlString& _seq)
	{
		CSysError err_obj(_T(__FUNCTION__));
		err_obj = S_OK;

		CAtlString cs_mac;
		err_obj = LicenseInfo_GetMediaAccCtrl(cs_mac);
		if (err_obj)
			return err_obj;

		CAtlString cs_serial;
		err_obj = LicenseInfo_GetSerialNumber(cs_serial);
		if (err_obj)
			return err_obj;

		_seq.Format(
				_T("%s%s"),
			(LPCTSTR)cs_mac,
			(LPCTSTR)cs_serial
			);

		return err_obj;
	}

}}}}

/////////////////////////////////////////////////////////////////////////////

CLicenseUser::CLicenseUser(void)
{
	m_error.Source(_T("CLicenseUser"));
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef      CLicenseUser::Error(void)const
{
	return m_error;
}

HRESULT        CLicenseUser::GenerateId(CAtlString& _uid)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = details::LicenseInfo_GetUniqueSequence(_uid);
	
	return m_error;
}