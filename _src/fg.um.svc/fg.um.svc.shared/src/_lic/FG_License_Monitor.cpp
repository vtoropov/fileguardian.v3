/*
	Created by Tech_dog (ebontrop@gmail.com) on 26-Apr-2016 at 0:04:38am, GMT+7, Phuket, Rawai, Tuesday;
	This is File Guardian Background Service License Monitor class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jun-2018 at 6:28:15p, UTC+7, Phuket, Rawai, Friday;
*/
#include "StdAfx.h"
#include "FG_License_Monitor.h"

using namespace fg::service;

#include "FG_License_Provider.h"

using namespace fg::common::lic;

#include "Shared_SystemCore.h"
#include "Shared_GenericSyncObject.h"
#include "Shared_GenericEvent.h"

using namespace shared::lite::sys_core;
using namespace shared::runnable;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace service { namespace details
{
	CGenericSyncObject&   LicenseMonitor_GetSyncObjectRef(void)
	{
		static CGenericSyncObject obj_;
		return obj_;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CLicenseMonitor::CLicenseMonitor(ILicenseCallback& _sink) : m_sink(_sink), m_last_state(CLicenseState::eUnknown)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT        CLicenseMonitor::Start(void)
{
	HRESULT hr_ = TBase::Start();
	if (FAILED(hr_))
		LastState(CLicenseState::eUnknown);
	else
		LastState(CLicenseState::ePending); // it's required to allow service main thread work till the first attempt of the license check;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CLicenseState  CLicenseMonitor::LastState(void)const
{
	CLicenseState state_;
	{
		SAFE_LOCK(details::LicenseMonitor_GetSyncObjectRef());
		state_ = m_last_state;
	}
	return state_;
}

HRESULT        CLicenseMonitor::Initialize(void)
{
	this->LastState(CLicenseState::ePending);
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

VOID           CLicenseMonitor::LastState(const CLicenseState& _state)
{
	SAFE_LOCK(details::LicenseMonitor_GetSyncObjectRef());
	m_last_state = _state;
}

VOID           CLicenseMonitor::ThreadFunction(void)
{
	CCoInitializer com_core(false);
#if defined(_DEBUG)
	CGenericWaitCounter wait_(10, 10000 * 01); // 10 sec(s)
#else
	CGenericWaitCounter wait_(10, 10000 * 60); // 10 min(s)
#endif

	CEventJournal::LogInfo(_T("License monitor has started"));

	CLicenseProvider prov_;

	while (!m_crt.IsStopped())
	{
		wait_.Wait();
		if (wait_.IsElapsed())
			wait_.Reset();
		else
			continue;

		CLicenseState state_;
		HRESULT hr_ = prov_.CheckLicense(state_);
		this->LastState(state_);

		if (FAILED(hr_)) {
			CEventJournal::LogError(
					prov_.Error()
				);
			break;
		}

		if (CLicenseState::eLicensed != state_.Identifier()) {

			CSysError err_obj(prov_.Error());
			err_obj.SetState(
					CLASS_E_NOTLICENSED,
					_T("License status: %s"), (LPCTSTR)state_.ToString()
				);
			break;
		}

		this->m_sink.ILicenseCallback_OnChanged(state_);
	}
	CEventJournal::LogInfo(_T("License monitor has stopped"));
	::SetEvent(m_crt.EventObject());
}