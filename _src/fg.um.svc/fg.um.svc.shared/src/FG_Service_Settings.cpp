/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Apr-2016 at 6:16:12pm, GMT+7, Phuket, Rawai, Monday;
	This is File Guardian background service configuration class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 1:1:1p, UTC+7, Phuket, Rawai, Thursday;
*/
#include "StdAfx.h"
#include "FG_Service_Settings.h"
#include "FG_Generic_Defs.h"

using namespace fg::common::data;

#include "Shared_Registry.h"
#include "Shared_FS_CommonDefs.h"

using namespace shared::registry;
using namespace shared::ntfs;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data { namespace details
{
	class CWatchedFolder_RegBase : protected CRegistryPathFinder
	{
		typedef CRegistryPathFinder TBase;
	protected:
		CRegistryStorage m_stg;
	public:
		CWatchedFolder_RegBase(void): m_stg(TBase::CService::SettingsRoot(), CRegistryOptions::eDoNotModifyPath){
		}
	public:
		bool         TrackRemovable(void)const
		{
			LONG lValue = 0;
			m_stg.Load(
					this->_GetRegPathBase(),
					this->_GetRemovableNamedValue(),
					lValue
				);
			if (0 == lValue) lValue = 1;
			return !!lValue;
		}
		HRESULT      TrackRemovable(const bool _track)
		{
			HRESULT hr_ = m_stg.Save(
					this->_GetRegPathBase(),
					this->_GetRemovableNamedValue(),
					static_cast<LONG>(_track)
				);
			return hr_;
		}
	protected:
		CAtlString  _GetRegPathBase(void)const
		{
			CAtlString path_= TBase::CService::SettingsPath();
			path_ += _T("\\Folders");

			return path_;
		}
	private:
		CAtlString  _GetRemovableNamedValue(void)const{ return CAtlString(_T("TrackRemovable"));}
	};

	class CWatchedFolder_Registry : private CWatchedFolder_RegBase
	{
		typedef CWatchedFolder_RegBase TBase;
	private:
		CWatchedType::_e m_type;
	public:
		CWatchedFolder_Registry(const CWatchedType::_e _type) : m_type(_type)
		{
		}
	public:
		HRESULT      Load(TFolderList& _list)const
		{
			LONG lCount = 0;
			HRESULT hr_ = m_stg.Load(
					this->_GetRegPath(),
					this->_GetCountNamedValue(),
					lCount
				);
			if (FAILED(hr_))
				return hr_;

			if (lCount < 1)
				return hr_;

			for (LONG i_ = 0; i_ < lCount; i_++)
			{
				CAtlString cs_item;
				cs_item.Format(
						this->_GetItemNamedValue(),
						i_
					);
				CAtlString cs_value;
				hr_ = m_stg.Load(
					this->_GetRegPath(),
					cs_item,
					cs_value
				);

				if (S_OK != hr_)
					break;
				if (cs_value.IsEmpty())
					continue;

				CAtlString cs_unc;

				hr_ = CGenericPath::ConvertUncToDos(cs_value.GetString(), cs_unc);
				if (FAILED(hr_))
					continue;

				try
				{
					_list.push_back( cs_unc );
				}
				catch(::std::bad_alloc&){ break; }
			}

			return  hr_;
		}

		HRESULT      Save(const TFolderList& _list)
		{
			const DWORD lCount = static_cast<DWORD>(_list.size());

			HRESULT hr_ = m_stg.Save(
					this->_GetRegPath(),
					this->_GetCountNamedValue(),
					lCount
				);

			for (DWORD i_ = 0; i_ < lCount; i_++)
			{
				CAtlString cs_item;
				cs_item.Format(
						this->_GetItemNamedValue(),
						i_
					);

				CAtlString cs_unc;

				hr_ = CGenericPath::ConvertDosToUnc(_list[i_].GetString(), cs_unc);
				if (FAILED(hr_))
					continue;

				hr_ = m_stg.Save(
					this->_GetRegPath(), cs_item, cs_unc
				);
				if (S_OK != hr_)
					break;
			}

			return hr_;
		}

	private:
		CAtlString  _GetRegPath(void)const
		{
			CAtlString path_= TBase::_GetRegPathBase();

			if (CWatchedType::eExcluded == m_type) path_ += _T("\\Excluded");
			if (CWatchedType::eIncluded == m_type) path_ += _T("\\Included");

			return path_;
		}
		CAtlString  _GetCountNamedValue(void)const    { return CAtlString(_T("Count")); }
		CAtlString  _GetItemNamedValue (void)const    { return CAtlString(_T("Item_%.4d"));}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CWatchedFolders::CWatchedFolders(const CWatchedType::_e _type) : m_type(_type)
{
	m_error.Source(_T("CWatchedFolders"));
}

/////////////////////////////////////////////////////////////////////////////

HRESULT           CWatchedFolders::Append(const CAtlString& _folder)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const bool bRelated = this->IsRelative(_folder);
	if (bRelated)
		return m_error;

	try
	{
		m_folders.push_back(_folder);
	}
	catch (::std::bad_alloc&)
	{
		m_error = E_OUTOFMEMORY;
	}
	return m_error;
}

INT               CWatchedFolders::Count  (void) const { return (static_cast<INT>(m_folders.size())); }
TErrorRef         CWatchedFolders::Error  (void) const { return m_error; }
bool              CWatchedFolders::IsEmpty(void) const { return m_folders.empty(); }
bool              CWatchedFolders::IsExcluded(void)const  { return (CWatchedType::eExcluded == m_type); }

bool              CWatchedFolders::IsRelative(const CAtlString& _folder)const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const INT len_ = _folder.GetLength();
	if (len_ < 1)
	{
		m_error = E_INVALIDARG;
		return true;
	}

	const INT count_ = this->Count();
	if (count_ < 1)
		return false;

	for (INT i_ = 0; i_ < count_; i_++)
	{
		const CAtlString& ref_ = m_folders[i_];
		if (len_ > ref_.GetLength())
		{
			if (0 == _folder.Find(ref_))
			{
				m_error.SetState(
						E_INVALIDARG,
						_T("The parent folder is registered: %s"),
						(LPCTSTR)ref_
					);
				return true;
			}
		}
		else if (len_ < ref_.GetLength())
		{
			if (0 == ref_.Find(_folder))
			{
				m_error.SetState(
						E_INVALIDARG,
						_T("Remove the child folder first: %s"),
						(LPCTSTR)ref_
					);
				return true;
			}
		}
		else
		{
			if (0 == ref_.Find(_folder))
			{
				m_error.SetState(
						E_INVALIDARG,
						_T("The folder is already registered: %s"),
						(LPCTSTR)ref_
					);
				return true;
			}
		}
	}
	return false;
}

CAtlString        CWatchedFolders::ItemOf(const INT nIndex) const
{
	if (nIndex < 0 || nIndex > this->Count() - 1)
		return CAtlString();
	else
		return m_folders[nIndex];
}

const
TFolderList&      CWatchedFolders::List  (void) const { return m_folders; }

HRESULT           CWatchedFolders::Remove(const CAtlString& _folder)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (_folder.IsEmpty())
		return (m_error = E_INVALIDARG);

	const INT count_ = this->Count();
	for ( INT i_ = 0; i_ < count_; i_++)
	{
		if (0 == m_folders[i_].CompareNoCase(_folder))
		{
			m_folders.erase(m_folders.begin() + i_);
			return m_error;
		}
	}
	m_error.SetState(
			E_INVALIDARG,
			_T("Specified folder is not found")
		);
	return m_error;
}

CWatchedType::_e  CWatchedFolders::Type  (void) const { return m_type; }

/////////////////////////////////////////////////////////////////////////////

CWatchedFolders::operator const TFolderList&(void)const { return m_folders; }

CWatchedFolders& CWatchedFolders::operator= (const TFolderList& _list)
{
	m_folders = _list;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CWatchedPersistent::CWatchedPersistent(const CWatchedFolders& _folders) : TBase(_folders.Type())
{
	this->m_folders = _folders;
}

CWatchedPersistent::CWatchedPersistent(const CWatchedType::_e _type) : TBase(_type)
{
	m_error.Source(_T("CWatchedPersistent"));
}

/////////////////////////////////////////////////////////////////////////////

HRESULT           CWatchedPersistent::Load(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	details::CWatchedFolder_Registry reg_(this->Type());
	HRESULT hr_ = reg_.Load(TBase::m_folders);
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}

HRESULT           CWatchedPersistent::Save(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	details::CWatchedFolder_Registry reg_(this->Type());
	HRESULT hr_ = reg_.Save(TBase::m_folders);
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CWatchedPersistent& CWatchedPersistent::operator= (const CWatchedFolders& _folders)
{
	this->m_type    = _folders.Type();
	this->m_folders = _folders;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CServiceSettings::CServiceSettings(void) :
	m_included(CWatchedType::eIncluded),
	m_excluded(CWatchedType::eExcluded),
	m_bRemoval(true)
{
	m_error.Source(_T("CServiceSettings"));
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef         CServiceSettings::Error(void)const           { return m_error; }
const
CWatchedFolders&  CServiceSettings::ExcludedFolders(void)const { return m_excluded; }
CWatchedFolders&  CServiceSettings::ExcludedFolders(void)      { return m_excluded; }
const
CWatchedFolders&  CServiceSettings::IncludedFolders(void)const { return m_included; }
CWatchedFolders&  CServiceSettings::IncludedFolders(void)      { return m_included; }
bool              CServiceSettings::TrackRemoval(void)const    { return m_bRemoval; }
VOID              CServiceSettings::TrackRemoval(const bool _track) { m_bRemoval = _track; }

/////////////////////////////////////////////////////////////////////////////

CServiceSettingPersBase::CServiceSettingPersBase(void) { m_error.Source(_T("CServiceSettingPersBase")); }

/////////////////////////////////////////////////////////////////////////////

HRESULT           CServiceSettingPersBase::Load(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;
	{
		details::CWatchedFolder_RegBase reg_;
		this->TrackRemoval(
				reg_.TrackRemovable()
			);
	}
	return m_error;
}

HRESULT           CServiceSettingPersBase::Save(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;
	{
		details::CWatchedFolder_RegBase reg_;
		reg_.TrackRemovable(
				this->TrackRemoval()
			);
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CServiceSettingPersistent::CServiceSettingPersistent(void) : m_bInitialized(false)
{
	m_error.Source(_T("CServiceSettingPersistent"));
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CServiceSettingPersistent::AppendFolder(const CAtlString& _folder, const CWatchedType::_e _type)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsInitialized())
		return (m_error = OLE_E_BLANK);

	CWatchedPersistent pers_(_type);
	switch (_type)
	{
	case CWatchedType::eExcluded: pers_ = m_excluded; break;
	case CWatchedType::eIncluded: pers_ = m_included; break;
	default:
		{
			m_error = (DWORD)ERROR_UNSUPPORTED_TYPE;
			return m_error;
		}
	}

	HRESULT hr_ = pers_.Append(_folder);
	if (FAILED(hr_))
		return (m_error = pers_.Error());
	
	hr_ = pers_.Save();
	if (FAILED(hr_))
		return (m_error = pers_.Error());

	switch (_type)
	{
	case CWatchedType::eExcluded: m_excluded = (TFolderList)pers_; break;
	case CWatchedType::eIncluded: m_included = (TFolderList)pers_; break;
	}

	return m_error;
}

bool      CServiceSettingPersistent::IsInitialized(void)const
{
	return m_bInitialized;
}

HRESULT   CServiceSettingPersistent::RemoveFolder(const CAtlString& _folder, const CWatchedType::_e _type)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsInitialized())
		return (m_error = OLE_E_BLANK);

	CWatchedPersistent pers_(_type);
	switch (_type)
	{
	case CWatchedType::eExcluded: pers_ = m_excluded; break;
	case CWatchedType::eIncluded: pers_ = m_included; break;
	default:
		{
			m_error = (DWORD)ERROR_UNSUPPORTED_TYPE;
			return m_error;
		}
	}

	HRESULT hr_ = pers_.Remove(_folder);
	if (FAILED(hr_))
		return (m_error = pers_.Error());

	hr_ = pers_.Save();
	if (FAILED(hr_))
		return (m_error = pers_.Error());

	switch (_type)
	{
	case CWatchedType::eExcluded: m_excluded = (TFolderList)pers_; break;
	case CWatchedType::eIncluded: m_included = (TFolderList)pers_; break;
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CServiceSettingPersistent::Load(void)
{
	TBase::Load();

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;
	{
		CWatchedPersistent pers_(CWatchedType::eExcluded);
		HRESULT hr_ = pers_.Load();
		if (FAILED(hr_))
			m_error = pers_.Error();
		else
			m_excluded = (CWatchedFolders)pers_;
	}
	{
		CWatchedPersistent pers_(CWatchedType::eIncluded);
		HRESULT hr_ = pers_.Load();
		if (FAILED(hr_))
			m_error = pers_.Error();
		else
			m_included = (CWatchedFolders)pers_;
	}
	m_bInitialized = !m_error;
	return m_error;
}

HRESULT   CServiceSettingPersistent::Save(const bool bIncludeBaseOpts)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsInitialized())
		return (m_error = OLE_E_BLANK);
	if (bIncludeBaseOpts)
		TBase::Save();
	{
		CWatchedPersistent pers_(m_excluded);
		HRESULT hr_ = pers_.Save();
		if (FAILED(hr_))
			m_error = pers_.Error();
	}
	{
		CWatchedPersistent pers_(m_included);
		HRESULT hr_ = pers_.Save();
		if (FAILED(hr_))
			m_error = pers_.Error();
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data
{
	CServiceSettingPersistent&  GetServiceSettingsRef(void)
	{
		static CServiceSettingPersistent pers_;
		return pers_;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CServiceFolderComparator::CServiceFolderComparator(const CWatchedFolders& _inc, const CWatchedFolders& _exc) : m_included(_inc), m_excluded(_exc)
{
	m_error.Source(_T("CServiceFolderComparator"));
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CServiceFolderComparator::CheckIncluded(LPCTSTR lpszPath) const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (FAILED(this->CheckInput(lpszPath)))
		return m_error;

	if (m_included.IsEmpty())
		return m_error;

	if (!this->CheckCoincident(lpszPath))
		return m_error;

	// (1) checks for included folders first;
	{
		CWatchedFolders included_(CWatchedType::eIncluded);
		included_ = m_included;

		if (included_.IsRelative(lpszPath))
			return (m_error = included_.Error());
	}
	// (2) checks for excluded folders secondly;
	{
		CWatchedFolders excluded_(CWatchedType::eExcluded);
		excluded_ = m_excluded;

		if (excluded_.IsRelative(lpszPath))
			return (m_error = excluded_.Error());
	}

	return m_error;
}

HRESULT   CServiceFolderComparator::CheckExcluded(LPCTSTR lpszPath) const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (FAILED(this->CheckInput(lpszPath)))
		return m_error;

	if (!this->CheckCoincident(lpszPath))
		return m_error;

	// (1) checks for excluded folders secondly;
	{
		CWatchedFolders excluded_(CWatchedType::eExcluded);
		excluded_ = m_excluded;

		if (excluded_.IsRelative(lpszPath))
			return (m_error = excluded_.Error());
	}

	// (2) checks for included folders first;
	{
		CWatchedFolders included_(CWatchedType::eIncluded);
		included_ = m_included;

		if (!included_.IsRelative(lpszPath)) {
			m_error.SetState(
					(DWORD)ERROR_INVALID_DATA, _T("Excluded folder is not related to any included one.")
				);
		}
	}

	return m_error;
}

TErrorRef CServiceFolderComparator::Error(void)const
{
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CServiceFolderComparator::CheckInput(LPCTSTR lpszPath) const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == lpszPath || ::_tcslen(lpszPath) < _countof(_T("a:")))
		return (m_error = E_INVALIDARG);

	return m_error;
}

HRESULT   CServiceFolderComparator::CheckCoincident(LPCTSTR lpszPath) const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;
	// (1) checks against included folders
	{
		const TFolderList& list_ = m_included.List();
		
		for (size_t i_ = 0; i_ < list_.size(); i_++) {

			const CAtlString& cs_folder = list_[i_];

			if (0 == cs_folder.CompareNoCase(lpszPath)){
				m_error.SetState(
						(DWORD)ERROR_INVALID_DATA, _T("The folder is already registered as watched one.")
					);
				break;
			}
		}
	}
	// (2) checks agains excluded folders
	{
		const TFolderList& list_ = m_excluded.List();
		
		for (size_t i_ = 0; i_ < list_.size(); i_++) {

			const CAtlString& cs_folder = list_[i_];

			if (0 == cs_folder.CompareNoCase(lpszPath)){
				m_error.SetState(
						(DWORD)ERROR_INVALID_DATA, _T("The folder is already registered as excluded one.")
					);
				break;
			}
		}
	}

	return m_error;
}