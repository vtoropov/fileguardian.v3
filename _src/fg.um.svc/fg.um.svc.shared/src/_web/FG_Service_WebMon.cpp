/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jan-2016 at 3:49:42pm, GMT+7, Phuket, Rawai, Monday;
	This is File Guardian background service shared library web monitor class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 31-May-2018 at 11:00:03p, UTC+7, Phuket, Rawai, Thursday;
*/
#include "StdAfx.h"
#include "FG_Service_WebMon.h"
#include "FG_Service_WebCfg.h"
#include "FG_License_Info.h"
#include "FG_Generic_Encrypt.h"
#include "FG_Json_Provider.h"

#include "FG_Generic_Defs.h"

using namespace fg::common::lic;
using namespace fg::common::data;

#include "Shared_SystemCore.h"
#include "Shared_GenericEvent.h"
#include "Shared_DateTime.h"
#include "Shared_NetHttpProvider.h"

using namespace shared::lite::sys_core;
using namespace shared::runnable;
using namespace shared::lite::data;
using namespace shared::net;

#include <Wininet.h>
/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data { namespace details
{
	const CCryptoDataCrt& WebMonitor_GetCryptoCrtData(void)
	{
		static CCryptoDataCrt crt_;
		static bool bInitialized = false;
		if (!bInitialized)
		{
			bInitialized = true;
			crt_.Algorithm(_FW_LIC_CRYPTO_ALGO);
			crt_.Key      (_FW_LIC_CRYPTO_KEY );
			crt_.Mode     (_FW_LIC_CRYPTO_MODE);
			crt_.Vector   (_FW_LIC_CRYPTO_VECTOR);
		}
		return crt_;
	}

	CAtlString WebMonitor_GetQueryText(const CRecord& rec_)
	{
		static CAtlString cs_pattern;
		if (cs_pattern.IsEmpty())
		{
			cs_pattern += _T("http://www.initec.net/copytest.php");
			cs_pattern += _T("?eventId=10000&clientId=10000");
			cs_pattern += _T("&instanceId=1000&eventCode=%d&timeStamp=%s&fileName=%s");
		}
		CSystemTime sys_(rec_.Timestamp());

		CAtlString cs_time;    sys_.ToString(cs_time, CDateTimeStyle::eUnspecified);
		CAtlString cs_query;

		cs_query.Format(
				cs_pattern,
				rec_.Action(),
				(LPCTSTR)cs_time,
				(LPCTSTR)rec_.Target()
			);

		TCHAR sz_canon[INTERNET_MAX_URL_LENGTH] = {0};
		DWORD dw_size = _countof(sz_canon);

		HRESULT hr_ = ::UrlCanonicalize(
							cs_query,
							sz_canon,
							&dw_size,
							URL_ESCAPE_UNSAFE
						);
		if (!FAILED(hr_))
			cs_query = sz_canon;
		return cs_query;
	}

	class WebMonitor_Encryptor : public CCryptoDataProviderBase
	{
		typedef CCryptoDataProviderBase TBase;
	public:
		WebMonitor_Encryptor(void) : TBase(details::WebMonitor_GetCryptoCrtData())
		{
			m_error.Source(_T("WebMonitor_Encryptor"));
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CWebMonitor::CWebMonitor(CDbProvider& _prov) : m_error(m_sync_obj), m_state(CThreadState::eStopped), m_provider(_prov)
{
	m_error.Source(_T("CWebMonitor"));
}

CWebMonitor::~CWebMonitor(void) {}

/////////////////////////////////////////////////////////////////////////////

CSysError     CWebMonitor::Error(void)const
{
	CSysError err_obj;
	{
		SAFE_LOCK(m_sync_obj);
		err_obj = m_error;
	}
	return err_obj;
}

bool          CWebMonitor::IsRunning(void)const
{
	bool bRunning = false;
	{
		SAFE_LOCK(m_sync_obj);
		bRunning = !!(CThreadState::eWorking & m_state);
	}
	return bRunning;
}

HRESULT       CWebMonitor::Start(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (this->IsRunning())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("Monitor is already running")
			);
		return m_error;
	}

	m_crt.IsStopped(false);

	if (!CThreadPool::QueueUserWorkItem(&CWebMonitor::MonitorWorkerThread, this))
		m_error = ::GetLastError();

	if (m_error) {
		m_state &= ~(CThreadState::eStopped);
		m_state |=   CThreadState::eError;
	}
	else {
		m_state &= ~(CThreadState::eError   | CThreadState::eStopped);
		m_state |=   CThreadState::eWorking;
	}

	return m_error;
}

DWORD         CWebMonitor::State(void)const
{
	DWORD state_ = 0;
	{
		SAFE_LOCK(m_sync_obj);
		state_ = m_state;
	}
	return state_;
}

HRESULT       CWebMonitor::State(const CWebMonOpt::_e _opt, const bool _set) {

	SAFE_LOCK(m_sync_obj);

	if (_set) m_state |=  _opt;
	else      m_state &= ~_opt;

	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT       CWebMonitor::Stop (void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsRunning())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("Monitor is already stopped")
			);
		return m_error;
	}
	m_crt.IsStopped(true);

	const DWORD dwResult = ::WaitForSingleObject(m_crt.EventObject(), INFINITE);

	if (dwResult != WAIT_OBJECT_0)
		m_error = ::GetLastError();

	if (m_error)
	{
		SAFE_LOCK(m_sync_obj);
		m_state |=  CThreadState::eError;
	}
	else
	{
		SAFE_LOCK(m_sync_obj);
		m_state |=  CThreadState::eStopped;
		m_state &= ~CThreadState::eWorking;
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

VOID          CWebMonitor::MonitorWorkerThread(void)
{
	CCoInitializer com_core(false);

	CGenericWaitCounter wait_(10, 1000);

	CHttpProvider web_;
	CAtlString    uid_;
	CLicenseUser  lic_;

	HRESULT hr_ = web_.Initialize();
	if (FAILED(hr_))
	{
		CEventJournal::LogError(web_.Error());
		goto __clean_up__;
	}
	else
		CEventJournal::LogInfo(
				_T("%s"),
				_T("Web provider is initialized")
			);

	hr_ = lic_.GenerateId(uid_);
	if (FAILED(hr_))
	{
		CEventJournal::LogError(lic_.Error());
		goto __clean_up__;
	}

	while (!m_crt.IsStopped())
	{
		wait_.Wait();
		if (wait_.IsElapsed())
			wait_.Reset();
		else
			continue;
		if (m_provider.IsLocked())
		{
			::Sleep(10);
			continue;
		}
		const INT state_ =   0;
		const INT limit_ = 100;
		TRecords recs_ = m_provider.Reader().Records(state_, limit_);  // gets records with 0 status, that means not uploaded data;
		//
		// TODO: it's necessary to re-view error handling in this worker thread;
		//       only debug mode handles a possible error for the time being;
		//
#if defined(_DEBUG)
		if ( m_provider.Reader().Error()) {
			CEventJournal::LogError(
				m_provider.Reader().Error()
			);
			goto __clean_up__;
		}
#endif
		if (!recs_.size())
			continue;

		CJsonDataProvider json_;
		CAtlString cs_json = json_.CreateBatchUpdate(uid_, recs_);

		CAtlString encoded_;
		details::WebMonitor_Encryptor cry_;
		cry_.Encrypt(cs_json, encoded_);

		if (m_provider.IsLocked())
			continue;

		if (m_crt.IsStopped())
				break;

		const CRecord empty_;
		CAtlString cs_query = _FW_DAT_WEB_UPLOAD_QUERY;
		CAtlString cs_reply;

		CAtlString pst_dat_;
		pst_dat_.Format(
						_T("data='%s'"),
						(LPCTSTR)encoded_
					);

		hr_ = web_.DoPostRequest(cs_query, cs_reply, pst_dat_);
		if (FAILED(hr_))
			continue;
		if (m_provider.IsLocked())
			continue;

		const LONG  repId = ::_tstol(cs_reply);
		if (1000 != repId)
			continue;

		hr_ = m_provider.Writer().UpdateStatus(recs_, 1);
		if (FAILED(hr_))
			CEventJournal::LogError(
					m_provider.Error()
				);
		else if (0 != (this->m_state & CWebMonOpt::eLogUpload)) {
			CEventJournal::LogInfo(
				_T("Web service returned success: %u records are accepted."), recs_.size()
			);
		}
	}
	hr_ = web_.Terminate();
	if (FAILED(hr_))
		CEventJournal::LogError(web_.Error());
	else
		CEventJournal::LogInfo(
				_T("%s"),
				_T("Web provider has been stopped.")
			);

__clean_up__:
	::SetEvent(m_crt.EventObject());
}