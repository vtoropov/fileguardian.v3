/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-May-2016 at 10:20:40pm, GMT+7, Phuket, Rawai, Tuesday;
	This is File Guardian background service web reporting service monitor configuration class implementation file.
*/
#include "StdAfx.h"
#include "FG_Service_WebCfg.h"
#include "FG_Generic_Defs.h"

using namespace fg::common::data;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"

using namespace shared::lite::common;
using namespace shared::registry;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data { namespace details
{
	class CWebMonitor_Registry : private CRegistryPathFinder
	{
		typedef CRegistryPathFinder TBase;
	private:
		CRegistryStorage m_stg;
	public:
		CWebMonitor_Registry(void) : 
		m_stg(TBase::CService::SettingsRoot(), CRegistryOptions::eDoNotModifyPath)
		{
		}
	public:
		HRESULT      Load(CWebMonitorSettings& _cfg)
		{
			LONG lValue = 0;
			m_stg.Load(
					this->_GetRegPath(),
					this->_GetEnableNamedValue(),
					lValue,
					static_cast<LONG>(CWebMonitorDefaults::Enabled())
				);
			_cfg.Enabled(!!lValue);

			HRESULT hr_ = S_OK;
			return  hr_;
		}

		HRESULT      Save(const CWebMonitorSettings& _cfg)
		{
			m_stg.Save(
					this->_GetRegPath(),
					this->_GetEnableNamedValue(),
					static_cast<LONG>(_cfg.Enabled())
				);

			HRESULT hr_ = S_OK;
			return  hr_;
		}
	private:
		CAtlString  _GetRegPath(void)const
		{
			CAtlString path_= TBase::CService::SettingsPath();
			path_ += _T("\\WebMonitor");
			return path_;
		}
		CAtlString  _GetEnableNamedValue   (void)const    { return CAtlString(_T("Enabled")); }
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

bool   CWebMonitorDefaults::Enabled(void)
{
	return true;
}

/////////////////////////////////////////////////////////////////////////////

CWebMonitorSettings::CWebMonitorSettings(const bool bInitToDefaults):m_enabled(false)
{
	m_error.Source(_T("CWebMonitorSettings"));
	if (bInitToDefaults)
		this->Default();
}

/////////////////////////////////////////////////////////////////////////////

VOID          CWebMonitorSettings::Default(void)
{
	m_enabled = CWebMonitorDefaults::Enabled();
}

bool          CWebMonitorSettings::Enabled(void)const
{
	return m_enabled;
}

VOID          CWebMonitorSettings::Enabled(const bool _enabled)
{
	m_enabled = _enabled;
}

TErrorRef     CWebMonitorSettings::Error(void)const
{
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CWebMonitorSettingPersistent::CWebMonitorSettingPersistent(const bool bInitToDefaults):TBase(bInitToDefaults)
{
	m_error.Source(_T("CWebMonitorSettingPersistent"));
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CWebMonitorSettingPersistent::Load(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	details::CWebMonitor_Registry reg_;
	m_error = reg_.Load(*this);

	return m_error;
}

HRESULT       CWebMonitorSettingPersistent::Save(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	details::CWebMonitor_Registry reg_;
	m_error = reg_.Save(*this);

	return m_error;
}