/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Jan-2016 at 6:05:33pm, GMT+7, Phuket, Rawai, Monday;
	This is File Guardian background service create data class(es) implementation file.
*/
#include "StdAfx.h"
#include "FG_Service_CrtData.h"
#include "FG_Generic_Defs.h"

using namespace fg::common::data;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace common { namespace data
{

	HRESULT    InitializeServiceCrtData(CServiceCrtData& _crt)
	{
		CServiceNames service_;

		_crt.Account()     = _T("NT AUTHORITY\\LocalSystem");
		_crt.Description() = _T("Initec system file monitor makes a user aware about all file operations");
		_crt.DisplayName() = _T("Initec System Protection File Guardian");
		_crt.Name()        = service_.Name();
		_crt.Path()        = service_.Executable();
		_crt.Options()     = (CServiceCrtOption::eStoppable | CServiceCrtOption::eCanShutdown);

		return  service_.LastResult();
	}
}}}