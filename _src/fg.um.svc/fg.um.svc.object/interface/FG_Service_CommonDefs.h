#ifndef _FGSERVICECOMMONDEFS_H_F6E3B79B_9A0A_43de_BB20_D459205D8C9A_INCLUDED
#define _FGSERVICECOMMONDEFS_H_F6E3B79B_9A0A_43de_BB20_D459205D8C9A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 2:34:36am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian background service common definitions declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 4-Jun-2018 at 9:53:19p, UTC+7, Phuket, Rawai, Monday;
*/
#include "Shared_SystemService.h"
#include "Shared_SystemError.h"
#include "Shared_SystemThreadPool.h"

#include "Shared_FS_CommonDefs.h"
#include "FG_SysEvent_Model.h"

namespace fg { namespace service {  namespace defs
{
	using shared::service::CServiceCrtData;
	using shared::service::CServiceCrtOption;

	using shared::lite::common::CSysError;
	using shared::lite::sys_core::CThreadCrtData;

	class CWorkedThreadCrtData : public CThreadCrtData { };

	using shared::ntfs::TFolderList;
	using shared::ntfs::COperateType;
}}}

#endif/*_FGSERVICECOMMONDEFS_H_F6E3B79B_9A0A_43de_BB20_D459205D8C9A_INCLUDED*/