#ifndef _FGSERVICEPRECOMPILEDHEADER_H_BC66510F_407B_4447_88A9_5C36BEF3454C_INCLUDED
#define _FGSERVICEPRECOMPILEDHEADER_H_BC66510F_407B_4447_88A9_5C36BEF3454C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 2:02:59am, GMT+7, Phuket, Rawai, friday;
	This is File Guardian Background Service Application precompiled header declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 1-Jun-2018 at 5:46:57p, UTC+7, Phuket, Rawai, Friday;
*/

#define  WINVER         0x0600
#define _WIN32_WINNT    0x0601
#define _WIN32_IE       0x0700
#define _RICHEDIT_VER   0x0200

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <conio.h>
#include <comutil.h>
#include <vector>
#include <map>
#include <typeinfo>

#pragma warning(disable: 4481) // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996) // security warning: function or variable may be unsafe

#include "Shared_GenericAppObject.h"
#include "Shared_LogJournal.h"

using shared::log::CEventJournal;

namespace global
{
	bool   IsDebugMode(void);
	class _out : public shared::log::CEventJournal{};
}

#pragma warning(disable: 4099) // mcrypt.lib: linking object as if no debug info

#pragma comment(lib, "__shared.lite_v15.lib")
#pragma comment(lib, "_crypto_v15.lib")
#pragma comment(lib, "_generic.stg_v15.lib")
#pragma comment(lib, "_log_v15.lib")
#pragma comment(lib, "_net_v15.lib")
#pragma comment(lib, "_ntfs_v15.lib")
#pragma comment(lib, "_registry_v15.lib")
#pragma comment(lib, "_runnable_v15.lib")
#pragma comment(lib, "_service_v15.lib")
#pragma comment(lib, "_user.32_v15.lib")
#pragma comment(lib, "_wmi.service_v15.lib")
#pragma comment(lib, "_xml_v15.lib")
#pragma comment(lib, "fg.generic.shared_v15.lib")
#pragma comment(lib, "fg.service.db.local_v15.lib")
#pragma comment(lib, "fg.service.shared_v15.lib")
#pragma comment(lib, "fg.um.filter.bridge_v15.lib")
#pragma comment(lib, "lib.mcrypt.adopted_v15.lib")

#endif/*_FGSERVICEPRECOMPILEDHEADER_H_BC66510F_407B_4447_88A9_5C36BEF3454C_INCLUDED*/