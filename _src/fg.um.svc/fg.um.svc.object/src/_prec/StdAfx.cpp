/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 2:05:26am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian Background Service Application precompiled header implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 4-Jun-2018 at 11:33:50p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"

using namespace shared::user32;

namespace global
{
	bool    IsDebugMode(void)
	{
		static volatile bool bDebugMode  = false;
		static volatile bool bInitialized = false;
		if (bInitialized == false)
		{
			bInitialized  = true;
			const TCommandLine& cmd_line = GetAppObjectRef().CommandLine();
			bDebugMode = cmd_line.Has(_T("debug"));
		}
		return bDebugMode;
	}
}