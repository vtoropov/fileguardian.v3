/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 3:46:16am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian background service shared resource class(es) implementation file.
*/
#include "StdAfx.h"
#include "FG_service_SharedRes.h"

using namespace fg::service;

#include "Shared_GenericSyncObject.h"

using namespace shared::lite::sync;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace service { namespace details
{
	CGenericSyncObject& ReadDirChangesEx_GetSyncObjRef(void)
	{
		static CGenericSyncObject obj_;
		return obj_;
	}
}}}

#define SAFE_LOCK_THIS() SAFE_LOCK(details::ReadDirChangesEx_GetSyncObjRef())
/////////////////////////////////////////////////////////////////////////////

CDirEventMonitor::CDirEventMonitor(void):
	m_dwFlags(FILE_NOTIFY_CHANGE_LAST_WRITE|FILE_NOTIFY_CHANGE_CREATION|FILE_NOTIFY_CHANGE_FILE_NAME),
	m_bTerminated(false)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID     CDirEventMonitor::AddDrives(const TDriveList& _list)
{
	SAFE_LOCK_THIS();
	if (m_bTerminated)
		return;
	for (size_t i_ = 0; i_ < _list.size(); i_++)
	{
		CAtlString cs_drive = _list[i_];
	}
}

VOID     CDirEventMonitor::AddFolders(const TFolderList& _list)
{
	SAFE_LOCK_THIS();
	if (m_bTerminated)
		return;
	for (size_t i_ = 0; i_ < _list.size(); i_++)
	{
		CAtlString cs_folder = _list[i_];
	}
}

VOID     CDirEventMonitor::GetEvent(DWORD& _action, CAtlString& _file)const
{
	_action; _file;
}

HANDLE   CDirEventMonitor::GetWaitHandle(void)const
{
	return NULL;
}

VOID     CDirEventMonitor::Initialize(void)
{
	SAFE_LOCK_THIS();
	m_bTerminated = false;
}

bool     CDirEventMonitor::IsOverflow(void)const
{
	return true;
}

VOID     CDirEventMonitor::RemoveDrives(const TDriveList& _list)
{
	SAFE_LOCK_THIS();
	if (m_bTerminated)
		return;
	for (size_t i_ = 0; i_ < _list.size(); i_++)
	{
		CAtlString cs_drive = _list[i_];
	}
}

VOID     CDirEventMonitor::Terminate(void)
{
	SAFE_LOCK_THIS();
	if (m_bTerminated)
		return;
	m_bTerminated = true;
}

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace service
{
	CDirEventMonitor& GetDirEvtMonitorObjRef(void)
	{
		static CDirEventMonitor dir_obj;
		return dir_obj;
	}
}}