/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 2:30:37am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian background service entry point implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 4-Jun-2018 at 10:28:32p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "Shared_SystemCore.h"
#include "Shared_GenericConObject.h"
#include "Shared_DateTime.h"
#include "Shared_FS_CommonDefs.h"

using namespace shared::ntfs;
using namespace shared::user32;
using namespace shared::lite::sys_core;
using namespace shared::lite::data;

#include "FG_Service_CommonDefs.h"
#include "FG_Service_Impl.h"
#include "FG_Service_ManagerImpl.h"

using namespace fg::service;
using namespace fg::service::defs;

#include "FG_SqlData_Provider.h"
#include "FG_Service_Settings.h"
#include "FG_Service_WebMon.h"
#include "FG_Service_WebCfg.h"
#include "FG_ArcData_Manager.h"
#include "FG_Service_CrtData.h"

using namespace fg::common::data;
using namespace fg::common::data::local;

#include "FG_Service_Resource.h"
#include "FG_FsBridgeCache.h"

using namespace fg::common::filter;

class CSvcCacheSink : public ICacheCallback
{
private:
	bool            m_inited;
	CDbProvider     m_sql_provider;
	CWebMonitor     m_web_monitor;
	CArchiveManager m_arc_manager;
public:
	CSvcCacheSink(void) : m_web_monitor(m_sql_provider), m_arc_manager(m_sql_provider), m_inited(false) {}
	~CSvcCacheSink(void){}

private: // ICacheCallback
	HRESULT ICacheCallback_OnError(TErrorRef _error) override sealed {

		CEventJournal::LogError(
				_error
			);

		HRESULT hr_ = S_OK;
		return  hr_;
	}

	HRESULT ICacheCallback_OnItemArrive( const CEventItem& _event ) override sealed {

		SYSTEMTIME complete_ = {0};
		_event.Time().CompletionAsSysTime(complete_);

		CRecord rec_(
				_event
			);
		rec_.Timestamp(complete_);
		rec_.IsLocalTime(true);
		HRESULT hr_ = m_sql_provider.WriterCached().Insert(rec_);
		if (FAILED(hr_))
			CEventJournal::LogError(
					m_sql_provider.Error()
				);
#if defined(_DEBUG)
		else if (global::IsDebugMode()) {

			static INT count_ = 0;

			global::_out::LogInfo(
				_T("Event #%04d:\n\t\tCompleted: %s\n\t\tTarget: %s\n\t\tAction: %s\n\t\tStatus: %s"),
				++count_ ,
				(LPCTSTR)rec_.TimestampAsText(),
				(LPCTSTR)rec_.Target()         ,
				(LPCTSTR)rec_.ActionAsText()   ,
				(LPCTSTR)rec_.StatusAsText()
			);
		}
#endif
		return  hr_;
	}

public:
	HRESULT   Initialize(void)
	{
		HRESULT hr_ = m_sql_provider.Accessor().Open(NULL);
		if (!FAILED(hr_)) {
			global::_out::LogInfo(
					_T("Service database '%s' is initialized"), m_sql_provider.Accessor().Locator().Path()
				);
			hr_ = m_sql_provider.WriterCached().Suspend(false);
		}

		if (FAILED(hr_)){
			global::_out::LogError( m_sql_provider.Accessor().Error() );
			return hr_;
		}

		hr_ = m_arc_manager.Start();
		if (FAILED(hr_)) {
			global::_out::LogError( m_arc_manager.Error() );
			return hr_;
		}
		else {
			global::_out::LogInfo(_T("Archive manager has started"));
		}

		CWebMonitorSettingPersistent pers_(true);
		hr_ = pers_.Load();
		if (!pers_.Enabled()) {
			global::_out::LogWarn(_T("Web monitor is not enabled."));
		}
		else {
			hr_ = m_web_monitor.State(CWebMonOpt::eLogUpload, true);
			hr_ = m_web_monitor.Start();
			if (FAILED(hr_))
				global::_out::LogError(m_web_monitor.Error());
			else
				global::_out::LogInfo(_T("Web monitor has started"));
		}
		m_inited = (S_OK == hr_);
		if (!m_inited) {
			if (m_web_monitor.IsRunning()) m_web_monitor.Stop();
		}
		return hr_;
	}

	bool      IsInited(void) const { return m_inited; }

	HRESULT   Terminate (void)
	{
		m_inited = false;
		HRESULT hr_ = m_web_monitor.Stop();
		if (FAILED(hr_))
			global::_out::LogError( m_web_monitor.Error() );
		else
			global::_out::LogInfo( _T("Web monitor has stopped") );

		hr_ = m_arc_manager.Stop();
		if (FAILED(hr_))
			global::_out::LogError(m_arc_manager.Error());
		else
			global::_out::LogInfo(_T("Archive manager has stopped"));

		hr_ = m_sql_provider.Accessor().Close();
		if (FAILED(hr_))
			global::_out::LogError(m_sql_provider.Accessor().Error());
		else
		{
			//
			// TODO: clearing write cache must be made by an accessor object on its close event;
			//
			m_sql_provider.WriterCached().Suspend(true);
			m_sql_provider.WriterCached().Cache().Clear();

			CEventJournal::LogInfo(
					_T("Service database '%s' is closed"), m_sql_provider.Accessor().Locator().Path()
				);
		}
		return hr_;
	}
};

INT _tmain(VOID)
{
	CCoApartmentThreaded com_core;
	if (!com_core.IsSuccess()) {
		return 1;
	}

	const bool debug_ = global::IsDebugMode();
	const TCommandLine& cmd_line = GetAppObjectRef().CommandLine();

	if (debug_)
	{
		global::_out::OutputToConsole(debug_);
		global::_out::VerboseMode(debug_);
		global::_out::LogInfoFromResource(
				IDS_FG_SERVICE_RUN_STANDALONE
			);
		global::_out::LogInfoFromResource(
				IDS_FG_SERVICE_CLS_WINDOW
			);
	}

	if (0 == cmd_line.Count()) // it very looks like the module is loaded by system service manager
	{
		CSvcCacheSink evt_sink_;
		CEventCache cache_( evt_sink_);

		HRESULT hr_ = evt_sink_.Initialize();
		if (!FAILED(hr_))
		{
			CServiceCrtData crt_data;
			hr_ = InitializeServiceCrtData(crt_data);
			if (!FAILED(hr_))
			{
				CFgService svc_(crt_data, cache_);

				const BOOL bResult = CFgService::Run(svc_);
				if (!bResult)
				{
					CSysError err_obj(::GetLastError());
					global::_out::LogError(
							_T("Service failed to run: %s"), err_obj.GetFormattedDetails().GetString()
						);
				}
			}
		}
		evt_sink_.Terminate();
	}
	else
	{
		CSvcCacheSink evt_sink_;
		CEventCache cache_( evt_sink_);
		CEventJournal::OutputToConsole(true);

		HRESULT hr_ = S_OK;
		if (debug_)
			hr_ = evt_sink_.Initialize();

		CConsoleWindow console_;
		console_.SetIcon(IDR_RT_MAINICON);
		::SetConsoleOutputCP(CP_UTF8);
		//
		// filter will be connected by service worker thread; 
		//
		if (SUCCEEDED(hr_))
		{
			CFgManager mgr_(cache_);

			hr_ = mgr_.Process(cmd_line);
			if (FAILED(hr_))
				CEventJournal::LogError(
						mgr_.Error()
					);
		}
		if (debug_)
			evt_sink_.Terminate();
	}

	if (debug_) {
		CEventJournal::LogEmptyLine();
		CEventJournal::LogInfo(
				_T("File Guardian service is exiting. Press any key or click [x] button")
			);
		::_gettch();
	}

	return 0;
}