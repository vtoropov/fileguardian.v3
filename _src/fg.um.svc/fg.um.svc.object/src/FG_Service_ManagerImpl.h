#ifndef _FGSERVICEMGR_H_9A94A259_C36B_43d2_B806_C62B29F0DF20_INCLUDED
#define _FGSERVICEMGR_H_9A94A259_C36B_43d2_B806_C62B29F0DF20_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jan-2016 at 1:10:50pm, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian background service manager class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 4-Jun-2018 at 11:40:44p, UTC+7, Phuket, Rawai, Monday;
*/
#include "Shared_SystemError.h"
#include "FG_FsBridgeDefs.h"

namespace fg { namespace service
{
	using shared::lite::common::CSysError;
	using fg::common::filter::IDriverEventAdoptedSink;

	class CFgManager
	{
	private:
		CSysError             m_error;
		IDriverEventAdoptedSink&
		                      m_fs_sink;
	public:
		CFgManager(IDriverEventAdoptedSink&);
		~CFgManager(void);
	public:
		TErrorRef     Error(void)const;
		HRESULT       Process(const TCommandLine&);
	};
}}

#endif/*_FGSERVICEMGR_H_9A94A259_C36B_43d2_B806_C62B29F0DF20_INCLUDED*/