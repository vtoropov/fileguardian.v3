#ifndef _FGSERVICESHAREDRES_H_5F5178AB_07F2_409e_8F36_3FD0027F6C5F_INCLUDED
#define _FGSERVICESHAREDRES_H_5F5178AB_07F2_409e_8F36_3FD0027F6C5F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 3:43:28am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian background service shared resource class(es) declaration file.
*/
#include "Shared_FS_GenericDrive.h"

namespace fg { namespace service
{
	using shared::ntfs::TFolderList;
	using shared::ntfs::TDriveList;

	class CDirEventMonitor
	{
	private:
		bool        m_bTerminated;
		const DWORD m_dwFlags;      // directory change notification options;
	public:
		CDirEventMonitor(void);
	public:
		VOID     AddDrives(const TDriveList&);
		VOID     AddFolders(const TFolderList&);
		VOID     GetEvent(DWORD& _action, CAtlString& _file)const;
		HANDLE   GetWaitHandle(void)const;
		VOID     Initialize(void);
		bool     IsOverflow(void)const;
		VOID     RemoveDrives(const TDriveList&);
		VOID     Terminate(void);
	};

	CDirEventMonitor& GetDirEvtMonitorObjRef(void);
}}

#endif/*_FGSERVICESHAREDRES_H_5F5178AB_07F2_409e_8F36_3FD0027F6C5F_INCLUDED*/