#ifndef _FGSERVICEIMPL_H_81C2D44B_F26F_433f_B79B_90DCEF0283B2_INCLUDED
#define _FGSERVICEIMPL_H_81C2D44B_F26F_433f_B79B_90DCEF0283B2_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 2:40:18am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian background service main class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 4-Jun-2018 at 8:57:22p, UTC+7, Phuket, Rawai, Monday;
*/
#include "Shared_SystemServiceImpl.h"
#include "Shared_SystemError.h"
#include "Shared_GenericSyncObject.h"

#include "Shared_FS_GenericDrive.Monitor.h"

#include "FG_Service_CommonDefs.h"
#include "FG_License_Monitor.h"

#include "FG_Generic_Launcher.h"
#include "FG_FsBridgeDefs.h"

namespace fg { namespace service
{
	using shared::lite::common::CSysError;
	using shared::lite::common::CSysErrorSafe;

	using shared::lite::sync::CGenericSyncObject;

	using shared::service::CServiceCrtData;
	using shared::service::CServiceBaseImpl;
	using shared::service::CServiceBaseImplEx;

	using fg::service::defs::CWorkedThreadCrtData;
//	using fg::service::defs::IFileSystemEventSink;
	using fg::common::CDesktopLauncher;

	using fg::common::filter::IDriverEventAdoptedSink;

	using shared::ntfs::CRemovableDriveMonitor;
	using shared::ntfs::IRemovableDriveEventSink;
	using shared::ntfs::TDriveList;

	class CFgService :
		public  CServiceBaseImplEx,
		public  ILicenseCallback
	{
		typedef CServiceBaseImplEx TBase;
	private:
		IDriverEventAdoptedSink&
		                       m_fs_sink;
		CWorkedThreadCrtData   m_crt;
		CGenericSyncObject     m_sync_obj;
		CSysErrorSafe          m_error;
		CDesktopLauncher       m_launcher;
		CLicenseMonitor        m_lic_mgr;

	public:
		CFgService(const CServiceCrtData&, IDriverEventAdoptedSink&);
		~CFgService(void);

	private: // CServiceBaseImpl
		virtual VOID           OnStart(const DWORD dwArgc, PWSTR* pszArgv) override sealed;
		virtual VOID           OnStop(VOID) override sealed;

	private: // CServiceBaseImplEx
		virtual VOID           OnDeviceEvent(const DWORD dwEventType, LPVOID const lpEventData) override sealed;

	private: // ILicenseCallback
		virtual HRESULT        ILicenseCallback_OnChanged(const CLicenseState&) override sealed;

	public:
		CSysError  Error(void)const;
	private:
		VOID       ServiceWorkerThread(void);
	};
}}

#endif/*_FGSERVICEIMPL_H_81C2D44B_F26F_433f_B79B_90DCEF0283B2_INCLUDED*/