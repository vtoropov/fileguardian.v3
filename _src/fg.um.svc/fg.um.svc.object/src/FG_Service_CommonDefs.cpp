/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 2:36:13am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian background service common definitions implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 4-Jun-2018 at 9:54:42p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "FG_Service_CommonDefs.h"