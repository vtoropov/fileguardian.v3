/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 3:25:44am, GMT+7, Phuket, Rawai, Friday;
	This is File Guardian background service main class implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 4-Jun-2018 at 10:21:04p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "FG_Service_Impl.h"
#include "FG_Service_SharedRes.h"
#include "FG_Service_Settings.h"

using namespace fg::service;
using namespace fg::service::defs;
using namespace fg::common::data;

#include "Shared_SystemCore.h"
#include "Shared_SystemThreadPool.h"
#include "Shared_GenericAppObject.h"
#include "Shared_GenericEvent.h"

using namespace shared::lite::sys_core;
using namespace shared::lite::common;
using namespace shared::user32;
using namespace shared::runnable;

#include "FG_Service_DrvConn.h"

using namespace fg::common::filter;

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace service { namespace details
{
	bool       FgService_CheckExclusions (const TFolderList& _exclude, const CAtlString _folder)
	{
		for (size_t i_ = 0; i_ < _exclude.size(); i_++)
		{
			const CAtlString& excluded_ = _exclude[i_];
			if (0 == _folder.Find(excluded_))
				return true;
		}
		return false;
	}

	VOID       FgService_DumpWatchFolders(const TFolderList& _folders, const bool _exclude)
	{
		if (_folders.empty())
		{
			if (_exclude)
				CEventJournal::LogInfo(
						_T("Exclude folder: %s"),
						_T("empty")
					);
			else
				CEventJournal::LogWarn(
						_T("Include folder: %s"),
						_T("empty")
					);
		}

		for (size_t i_ = 0; i_ < _folders.size(); i_++)
		{
			if (_exclude)
				CEventJournal::LogInfo(
						_T("Exclude folder: %s"),
						_folders[i_].GetString()
					);
			else
				CEventJournal::LogInfo(
						_T("Include folder: %s"),
						_folders[i_].GetString()
					);
		}
	}

}}}

/////////////////////////////////////////////////////////////////////////////

CFgService::CFgService(
			const CServiceCrtData& crt_data,
			IDriverEventAdoptedSink& _snk
		) : TBase(crt_data), m_error(m_sync_obj), m_fs_sink(_snk), m_lic_mgr(*this)
{
	m_error.Source(_T("CFgService"));
}

CFgService::~CFgService(void)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID CFgService::OnStart(const DWORD dwArgc, PWSTR* pszArgv)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	TBase::OnStart(dwArgc, pszArgv);

	if (m_last_error != NO_ERROR) {}

	if (m_lic_mgr.IsRunning() == false)
		m_lic_mgr.Initialize(); // sets license to pending state

	if (!CThreadPool::QueueUserWorkItem(&CFgService::ServiceWorkerThread, this))
		m_error = ::GetLastError();
	else
	{
		// the code below is temporarily disabled due to the error:
		// code= 0x80070534; desc=No mapping between account names and security IDs was done;
		if (m_launcher.IsRunning() == false && false)
			m_launcher.Start();
		if (m_lic_mgr.IsRunning() == false)
			m_lic_mgr.Start();

		m_error = S_OK;
	}
}

VOID CFgService::OnStop(VOID)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	m_crt.IsStopped(true);

	TBase::OnStop();

	if (m_launcher.IsRunning())
		m_launcher.Stop();

	if (m_lic_mgr.IsRunning())
		m_lic_mgr.Stop();

	if (::WaitForSingleObject(m_crt.EventObject(), INFINITE) != WAIT_OBJECT_0)
		m_error = ::GetLastError();

}

/////////////////////////////////////////////////////////////////////////////

VOID CFgService::OnDeviceEvent(const DWORD dwEventType, LPVOID const lpEventData)
{
	dwEventType; lpEventData;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CFgService::ILicenseCallback_OnChanged(const CLicenseState& _state)
{
	_state;
	if (global::IsDebugMode()) {
		global::_out::LogInfo(_T("License state: %s"), (LPCTSTR)_state.ToString());
	}
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CSysError CFgService::Error(void)const {

	CSysError err_obj;
	{
		SAFE_LOCK(m_sync_obj);
		err_obj = m_error;
	}
	return err_obj;
}

/////////////////////////////////////////////////////////////////////////////

void CFgService::ServiceWorkerThread(void)
{
	CCoInitializer com_core(false);

	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CGenericWaitCounter waiter_(50, 500);

	CDrvConnector drv_connect(m_fs_sink);

	HRESULT hr_ = drv_connect.Connect();
	if (FAILED(hr_)) {
		goto __clean_up__;
	}

	while (!m_crt.IsStopped())
	{
		waiter_.Wait();
		if (waiter_.IsElapsed())
			waiter_.Reset();
		else
			continue;

		const CLicenseState state_ = m_lic_mgr.LastState();

		if (CLicenseState::eLicensed != state_.Identifier() &&
			CLicenseState::ePending  != state_.Identifier())
		{
#if !defined(_DEBUG)
			CEventJournal::LogError(
					_T("The license status is %s, the service is forced to stop monitoring a file system."),
					state_.ToString().GetString()
				);
			this->Exit();
			break;
#endif
		}
	}

__clean_up__:

	drv_connect.Disconnect();

	if (m_error)
		m_fs_sink.IDriverEventAdopted_OnError(m_error);

	::SetEvent(m_crt.EventObject());
}