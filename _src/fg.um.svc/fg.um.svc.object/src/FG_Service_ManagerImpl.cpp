/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jan-2016 at 1:18:50pm, GMT+7, Phuket, Rawai, Wednesday;
	This is File Guardian background service manager class implementation file.
*/
#include "StdAfx.h"
#include "FG_Service_ManagerImpl.h"
#include "FG_Service_Impl.h"
#include "FG_Service_CrtData.h"

using namespace fg::service;
using namespace fg::service::defs;
using namespace fg::common::data;

#include "Shared_SystemService.h"
#include "Shared_SystemServiceMan.h"

using namespace shared::service;

#include "FG_Service_Resource.h"

/////////////////////////////////////////////////////////////////////////////

namespace fg { namespace service { namespace details
{
	class CFgManager_CmdLineHandler
	{
	private:
		const
		TCommandLine& m_cmd_ln;
		CSysError     m_error;
	public:
		CFgManager_CmdLineHandler(const TCommandLine& cmd_ln_ref) : m_cmd_ln(cmd_ln_ref)
		{
			m_error.Source(_T("CFgManager_CmdLineHandler"));
		}
		~CFgManager_CmdLineHandler(void){}
	public:
		TErrorRef     Error(void)const        { return m_error; }
		bool          IsCmdDebug (void)const  { return m_cmd_ln.Has(_T("debug"));   } // deprecated; global proprty can be used;
		bool          IsCmdCreate(void)const  { return m_cmd_ln.Has(_T("install")); }
		bool          IsCmdRemove(void)const  { return m_cmd_ln.Has(_T("remove"));  }
		bool          IsCmdStart (void)const  { return m_cmd_ln.Has(_T("start"));   }
		bool          IsCmdStop  (void)const  { return m_cmd_ln.Has(_T("stop"));    }

		HRESULT       Validate(void)
		{
			m_error.Module(_T(__FUNCTION__));

			if (this->IsCmdCreate() ||
				this->IsCmdRemove() ||
				this->IsCmdStart () ||
				this->IsCmdStop  () ||
				this->IsCmdDebug ())
				m_error.Clear();
			else
				m_error.SetState(
						E_INVALIDARG,
						IDS_FG_SERVICE_CMDLINE_ERROR
					);
			return m_error;
		}
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CFgManager::CFgManager(IDriverEventAdoptedSink& snk_ref) : m_fs_sink(snk_ref) { m_error.Source(_T("CFwManager")); }
CFgManager::~CFgManager(void) { }

/////////////////////////////////////////////////////////////////////////////

TErrorRef  CFgManager::Error(void)const { return m_error; }

HRESULT    CFgManager::Process(const TCommandLine& cmd_ln_ref)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	details::CFgManager_CmdLineHandler handler_(cmd_ln_ref);
	HRESULT hr_ = handler_.Validate();

	if (FAILED(hr_))
		return (m_error = handler_.Error());

	if (global::IsDebugMode())
	{
		CServiceCrtData crt_data;
		hr_ = InitializeServiceCrtData(crt_data);
		if (FAILED(hr_))
			return (m_error = hr_);

		CFgService svc_(crt_data, m_fs_sink);

		svc_.Start(0, NULL);
		::_getch();
		svc_.Stop();

		return m_error;
	}

	CServiceManager mgr_;
	CServiceCrtData crt_;

	hr_ = InitializeServiceCrtData(crt_);
	if (FAILED(hr_))
		return (m_error = hr_);

	hr_ = mgr_.Initialize(CServiceManagerParams::eOpenForService);
	if (FAILED(hr_))
		return (m_error = mgr_.Error());

	if (false){}
	else if (handler_.IsCmdCreate())
	{
		CEventJournal::LogInfoFromResource(IDS_FG_SERVICE_CREATE_START);

		CService svc_;
		hr_ = mgr_.Create(crt_, svc_);

		if (FAILED(hr_))
			m_error = mgr_.Error();
		else
			CEventJournal::LogInfoFromResource(IDS_FG_SERVICE_CREATE_SUCCESS);
	}
	else if (handler_.IsCmdRemove())
	{
		CEventJournal::LogInfoFromResource(IDS_FG_SERVICE_REMOVE_START);

		hr_ = mgr_.Remove(crt_.Name());
		if (FAILED(hr_))
			m_error = mgr_.Error();
		else
			CEventJournal::LogInfoFromResource(IDS_FG_SERVICE_REMOVE_SUCCESS);
	}
	else if (handler_.IsCmdStart())
	{
		CEventJournal::LogInfoFromResource(IDS_FG_SERVICE_RUN_START);

		hr_ = mgr_.Start(crt_.Name());
		if (FAILED(hr_))
			m_error = mgr_.Error();
		else
			CEventJournal::LogInfoFromResource(IDS_FG_SERVICE_RUN_SUCCESS);
	}
	else if (handler_.IsCmdStop())
	{
		CEventJournal::LogInfoFromResource(IDS_FG_SERVICE_STOP_STARTED);

		hr_ = mgr_.Stop(crt_.Name());
		if (FAILED(hr_))
			m_error = mgr_.Error();
		else
			CEventJournal::LogInfoFromResource(IDS_FG_SERVICE_STOP_SUCCESS);
	}
	return m_error;
}