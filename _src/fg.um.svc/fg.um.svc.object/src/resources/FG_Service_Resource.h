//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ resource include file.
// Used by FG_Service_Resource.rc
//
// Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2017 at 2:20:00am, GMT+7, Phuket,Rawai, Friday
// This is File Guardian Background Service Application Resource Declaration file.
//

#define IDR_RT_MANIFEST                                     1
#define IDR_RT_MAINICON                                     3

#define IDS_FG_SERVICE_CMDLINE_ERROR                     1001
#define IDS_FG_SERVICE_CREATE_START                      1003
#define IDS_FG_SERVICE_CREATE_SUCCESS                    1005
#define IDS_FG_SERVICE_REMOVE_START                      1007
#define IDS_FG_SERVICE_REMOVE_SUCCESS                    1009
#define IDS_FG_SERVICE_RUN_START                         1011
#define IDS_FG_SERVICE_RUN_SUCCESS                       1013
#define IDS_FG_SERVICE_RUN_STANDALONE                    1015
#define IDS_FG_SERVICE_CLS_WINDOW                        1017
#define IDS_FG_SERVICE_STOP_STARTED                      1019
#define IDS_FG_SERVICE_STOP_SUCCESS                      1021


