#ifndef _UIXDEFINITION_H_BD496C2A_6531_4ad2_A7F0_7129A8F8FEED_INCLUDED
#define _UIXDEFINITION_H_BD496C2A_6531_4ad2_A7F0_7129A8F8FEED_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Nov-2014 at 3:42:04a, GMT+3, Taganrog, Saturday;
	This is UIX Web Browser library common interface declaration file.
*/
#include <dispex.h>
#include <map>
#include <vector>
#include <Exdisp.h>
#include <Exdispid.h>

#include "Shared_SystemError.h"

namespace ex_ui
{
	using shared::lite::common::CSysError;

	class eBrowserResourceType {
	public:
		enum _e {
			eHostExecutable     =     0,   // html resource is located in the host of the browser; 
		};
	};

	class CBrowserResource {
	private:
		CSysError     m_error;
		eBrowserResourceType::_e m_type;
	public:
		CBrowserResource(const eBrowserResourceType::_e);
	public:
		TErrorRef     Error(void)const;
		HRESULT       URL(LPCTSTR lpszPattern, CAtlString& _result);
	};

	class eBrowserEmulationVersion
	{
	public:
		enum _e {
			eDefault            =     0,
			eVersion7           =  7000,
			eVersion8           =  8000,
			eVersion8Standards  =  8888,
			eVersion9           =  9000,
			eVersion9Standards  =  9999,
			eVersion10          = 10000,
			eVersion10Standards = 10001,
			eVersion11          = 11000,
			eVersion11Edge      = 11001
		};
	};

	class CBrowserEmulateMan
	{
	private:
		mutable
		CSysError          m_error;
		CAtlString         m_host_name;              // application executable name (without full path) that hosts the web browser control;
	public:
		CBrowserEmulateMan(LPCTSTR lpszHost = NULL); // if not provided, the current executable name is used;
	public:
		HRESULT            ApplyLatestVersion(void);
		TErrorRef          Error(void)const;
		bool               IsEnabled(const eBrowserEmulationVersion::_e)const;
		eBrowserEmulationVersion::_e LatestAvailableVersion(void)const;
		eBrowserEmulationVersion::_e Mode(void)const;
		HRESULT            Mode(const eBrowserEmulationVersion::_e);
	};

	interface IWebBrowserEventHandler
	{
		virtual HRESULT    BrowserEvent_BeforeNavigate  (LPCTSTR lpszUrl)                           PURE;
		virtual HRESULT    BrowserEvent_DocumentComplete(LPCTSTR lpszUrl, const bool bStreamObject) PURE;
		virtual HRESULT    BrowserEvent_OnMouseMessage  (const MSG&) { return E_NOTIMPL; }
	};

	interface IHtmlElementEventCallback
	{
		virtual HRESULT    HtmlEvent_OnGeneric(LPCTSTR lpszElement, LPCTSTR lpszEvent) { lpszElement; lpszEvent; return S_OK; }
	};
}

#endif/*_UIXDEFINITION_H_BD496C2A_6531_4ad2_A7F0_7129A8F8FEED_INCLUDED*/