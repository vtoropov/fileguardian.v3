#ifndef _BROWSERLIBRARYPRECOMPILEDHEADER_H_CEE6902E_18A4_43c5_8D69_A7BC5024D1C7_INCLUDED
#define _BROWSERLIBRARYPRECOMPILEDHEADER_H_CEE6902E_18A4_43c5_8D69_A7BC5024D1C7_INCLUDED
/*
	Created by Tech_dog (VToropov) on 29-Nov-2014 at 03:42:04a, GMT+3, Taganrog, Saturday;
	This is UIX Browser library precompiled headers definition file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 8:53:33p, UTC+7, Phuket, Rawai, Monday;
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>
#include <comutil.h>

/*
	VERY IMPORTANT NOTES:
	-------------------------------------------------------------------------------
	in order to get the httprequest.h header file you have to perform the following:
	1) Run as administrator "Visual Studio 2008 Command Prompt";
	2) cd {Path_to_Windows_sdk}\Include\;
	3) midl httprequest.idl;
*/
//
// the comment above is remained for an information only;
// the required file is included from __shared.3rd.party.libs\ms-win-http\include folder;
//
#include <httprequest.h>

#define _use_IE6_compatible_CB5829FE_6DE5_4fe2_B8D8_32996330F16B_

#if !defined(_use_sys_xml_service_26F58581_8525_4e1e_983B_F32F5D73565C_)
	 #define _use_sys_xml_service_26F58581_8525_4e1e_983B_F32F5D73565C_
#endif

#endif/*_BROWSERLIBRARYPRECOMPILEDHEADER_H_CEE6902E_18A4_43c5_8D69_A7BC5024D1C7_INCLUDED*/