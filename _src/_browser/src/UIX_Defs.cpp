/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Apr-2017 at 11:05:46p, GMT+7, Phuket, Rawai, Saturday;
	This is UIX Web Browser library common definition implementation file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 9:27:08p, UTC+7, Phuket, Rawai, Monday;
*/
#include "StdAfx.h"
#include "UIX_Defs.h"

using namespace ex_ui;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"

using namespace shared::user32;
using namespace shared::registry;

/////////////////////////////////////////////////////////////////////////////

CBrowserResource::CBrowserResource(const eBrowserResourceType::_e _type) : m_type(_type)
{
	m_error.Source(_T("CBrowserResource"));
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef     CBrowserResource::Error(void)const {
	return m_error;
}

HRESULT       CBrowserResource::URL(LPCTSTR lpszPattern, CAtlString& _result)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	switch (m_type) {

	case eBrowserResourceType::eHostExecutable:
		{
			CAtlString cs_path;
			HRESULT hr_ = GetAppObjectRef().GetFullPath(cs_path);
			if (SUCCEEDED(hr_))
			{
				_result.Format(
					lpszPattern, cs_path.GetString()
				);
			}
			else
				m_error = hr_;
		} break;
	default:
		m_error = DISP_E_TYPEMISMATCH;
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace details
{
	class CBrowserEmulateRegistry
	{
	private:
		mutable
		CRegistryStorage m_stg;
		CAtlString       m_named_value;
	public:
		CBrowserEmulateRegistry(LPCTSTR lpszNamedValue) :
			m_stg(
				HKEY_CURRENT_USER, CRegistryOptions::eDoNotModifyPath
			),
			m_named_value(lpszNamedValue){}
	public:
		HRESULT   Emulation(eBrowserEmulationVersion::_e& _mode)
		{
			LONG lValue = 0;
			HRESULT hr_ = m_stg.Load(
					this->_EmulateRegFolder(), m_named_value, lValue, static_cast<DWORD>(eBrowserEmulationVersion::eDefault)
				);
			if (FAILED(hr_))
				return hr_;

			_mode = static_cast<eBrowserEmulationVersion::_e>(lValue);

			return hr_;
		}

		HRESULT   Emulation(const eBrowserEmulationVersion::_e& _mode)
		{
			HRESULT hr_ = m_stg.Save(
					this->_EmulateRegFolder(), m_named_value, static_cast<DWORD>(_mode)
				);
			return  hr_;
		}

		HRESULT   Version(LONG& _major)
		{
			CAtlString cs_version;
			HRESULT hr_ = m_stg.Load(
					this->_VersionRegFolder(), this->_VersionRegValue_New(), cs_version
				);
			if (cs_version.IsEmpty())
				hr_ = m_stg.Load(
					this->_VersionRegFolder(), this->_VersionRegValue_Old(), cs_version
				);
			if (SUCCEEDED(hr_))
				_major = ::_tstol(cs_version);
			return hr_;
		}
	private:
		LPCTSTR  _EmulateRegFolder(void) const
		{
			static CAtlString cs_folder;
			if (cs_folder.IsEmpty()){
				cs_folder += this->_VersionRegFolder();
				cs_folder +=_T("\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION");
			}
			return cs_folder;
		}

		LPCTSTR  _VersionRegFolder(void) const
		{
			static CAtlString cs_folder(_T("SOFTWARE\\Microsoft\\Internet Explorer"));
			return cs_folder;
		}

		LPCTSTR  _VersionRegValue_New(void)const
		{
			static CAtlString cs_ver(_T("svcVersion"));
			return cs_ver;
		}

		LPCTSTR  _VersionRegValue_Old(void)const
		{
			static CAtlString cs_ver(_T("Version"));
			return cs_ver;
		}
	};
}}

/////////////////////////////////////////////////////////////////////////////

CBrowserEmulateMan::CBrowserEmulateMan(LPCTSTR lpszHost)
{
	m_error.Source(_T("CBrowserEmulateMan"));
	m_error.Module(_T(__FUNCTION__));

	m_host_name = lpszHost;
	if (m_host_name.IsEmpty())
	{
		const CApplication& the_app = GetAppObjectRef();
		m_host_name = the_app.GetFileName();
	}
	if (m_host_name.IsEmpty())
		m_error.SetState(
				E_FAIL,
				_T("Cannot evaluate executable file name.")
			);
	else
		m_error = S_OK;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CBrowserEmulateMan::ApplyLatestVersion(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	eBrowserEmulationVersion::_e the_latest = this->LatestAvailableVersion();
	this->Mode(the_latest);

	return m_error;
}

TErrorRef    CBrowserEmulateMan::Error(void)const
{
	return m_error;
}

bool         CBrowserEmulateMan::IsEnabled(const eBrowserEmulationVersion::_e _mode)const
{
	const eBrowserEmulationVersion::_e eMode = this->Mode();
	return (_mode <= eMode);
}

eBrowserEmulationVersion::_e
             CBrowserEmulateMan::LatestAvailableVersion(void)const
{
	eBrowserEmulationVersion::_e the_latest = eBrowserEmulationVersion::eVersion11;
	return the_latest;
#if(0)
	details::CBrowserEmulateRegistry reg_(m_host_name);
	LONG major_ = 0;
	HRESULT hr_ = reg_.Version(major_);
	if (FAILED(hr_))
		return the_latest;

	if (major_ > 11)
		major_ = 11;
	switch (major_)
	{
	case 11: the_latest = eBrowserEmulationVersion::eVersion11; break;
	case 10: the_latest = eBrowserEmulationVersion::eVersion10; break;
	case  9: the_latest = eBrowserEmulationVersion::eVersion9;  break;
	case  8: the_latest = eBrowserEmulationVersion::eVersion8;  break;
	}
	return the_latest;
#endif
}

eBrowserEmulationVersion::_e 
             CBrowserEmulateMan::Mode(void)const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	eBrowserEmulationVersion::_e eMode = eBrowserEmulationVersion::eDefault;
	details::CBrowserEmulateRegistry reg_(m_host_name);

	HRESULT hr_ = reg_.Emulation(eMode);
	if (FAILED(hr_)) {
		eMode = eBrowserEmulationVersion::eDefault;
		m_error = hr_;
	}
	return eMode;
}

HRESULT      CBrowserEmulateMan::Mode(const eBrowserEmulationVersion::_e _mode)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	details::CBrowserEmulateRegistry reg_(m_host_name);
	HRESULT hr_ = reg_.Emulation(_mode);
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}